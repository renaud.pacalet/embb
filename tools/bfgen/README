
Introduction
============

The tool can be used to deal with fields of bits in computer program
(software) and hardware description language. There is often a need for
a common register or packet bit-level format to enable communication.
The proposed tool can generate code in various formats and languages to
access bit packed data from a generic bit fields description. When
developing a hardware device along with associated drivers, this allows
keeping hardware, software and specifications consistent.

The tool is able to generate software code for a specific configuration
of processor endianess and access width from the generic bit field
description. This makes the tool useful for both network packet and
binary file formats.

The input format is parsed, checked for common errors (like fields
overlap) and padded with padding fields if needed. The output format is
written to the standard output by a backend module. Some backends are
provided but it is really simple to develop a new one which can come
along with your application. The bfgen script can easily be embedded
along with the few required backends within your project source tree.

Input format
============

The bitfield input format can describe multiple registers containing
an arbitrary number of fields. Each register declaration start with a
single '%' character followed by the register name:

  %myregister

Register fields are declared using two '%' character:

    %%firstfield

A special field name can be used to explicitly insert padding fields:
'__padding__'.  Padding fields will be automatically generated to fill
holes between fields.

Both registers and fields can have attributes. Attributes have a name
and a value separated with a colon:

  %myregister
    width: 64
    swap: 0b111000

    %%firstfield
      width: 4
    %%secondfield
      width: 12

As an alternate syntax, the width field attribute may be written
directly on the field name line:

  [ %%field_name: width ]

    %%firstfield: 4
    %%secondfield: 12

A bits range syntax is also available:

  [ %%field_name: first_bit-last_bit ]

    %%firstfield: 0-3
    %%secondfield: 4-15

Acceptable register and field values can be specified with a short
descriptive string:

    %%firstfield
      width: 4
      0b0000: Add op
      0b0011: Div op
      0b1011: No op

The given string will be used for documentation and generation of code
constant names. Separate content for these two purposes may be
provided with an additional colon:

    %%firstfield
      width: 4
      0b0000: Add op : The addition operation
      0b0011: Div op : The division operation
      0b1011: No op : No operation performed

Numbers can be in decimal, hexadecimal (0x prefix) or binary format
(0b) prefix.

Package attributes
------------------

Package attributes can be specified either at start of the input,
before first register definition, or on the command line.

  * expand: force expansion for all registers and fields with a count
    attribute greater than 1 in the package.

Available attributes depend on selected output backend, see below.

Register attributes
-------------------

The following optional attributes are available for registers:

  * width: the total register width. When this attribute is not
    present, the total width is computed from field width with largest
    position.

  * address: address of the register in the bank. Registers must be
    sorted in ascending order in the input file. When this attribute
    is not present, register addresses are automatically incremented.
    The special value 'union' can be used to declare a register at the
    same location as the previous register.

  * swap: swapping directive for this register, see below.

  * bound: field boundary width, see below.

  * longname: long explicit field name used for documentation and
    comments.

  * doc: field documentation, multiple doc attributes will be
    concatenated.

  * count: specify a repeat count to instanciate multiple registers
    with the same format.

  * stride: specify an address increment to use when count > 1.

  * expand: force multiple registers expansion for this register.

  * index_start and index_step: specify numeric indexes to use for the
    repeated registers (count > 1).

  * condition: specify a condition for the register definition to be
    considered. see below

Field attributes
----------------

The following optional attributes are available for fields:

  * width: set the field width. When this attribute
    is not present the field width is assumed to be 1.

  * position: set the bit position of the field. When this attribute
    is not present the field is placed next to the previous field.

  * range: set the valid values range. default is 0 to 2^width-1.

  * default: set default field value, 0 if not present.

  * direction: 'r', 'w' or 'rw'. Defaults to 'rw' if not specified.

  * longname: long explicit field name used for documentation and
    comments.

  * doc: field documentation, multiple doc attributes will be
    concatenated.

  * count: specify a repeat count to instanciate multiple fields
    with the same format.

  * stride: specify a bit position increment to use when count > 1.

  * expand: force multiple registers expansion for this field when count > 1.

  * index_start and index_step: specify numeric indexes to use for the
    repeated fields (count > 1).

  * condition: specify a condition for the field definition to be
    considered. see below

Examples
--------

# Foo device registers definition

%command: 32
  doc: This is the device command register.
  doc: bla bla ...

  %%start
    position: 4
    direction: w

  %%status
    width: 4
    position: 8
    direction: r

%irqmask: 32
  # FIXME this register needs some doc

  %%recvirq
  %%sendirq

%irqstate
  address: union
  width: 32

  %%recvirq
  %%sendirq

%gpiostate: 32

  %%pin: 1
    count: 32

%powersave: 32
  condition: CHIP_REVISION > 5

Command line
============

The tool read the generic bitfield description on the standard input
and write the generated code on standard output.

    -h  --help         Display help

    -o  --backend      Select output backend.
    -l  --attr-list    Display attributes list for selected backend.

    -I  --input        Set input file (default is stdin).
    -O  --output       Set output file (default is stdout).

    -e  --endian       Set endianess of target processor (little* or big).

Package attributes can be specified on the command line as value
assignments:

$ bfgen -I foo.bf -O foo.c -o caccessor swap=0b11000 align=16 bound=32

Available package attributes depend on selected backend; they can be
listed with the -l option:

$ bfgen -o caccessor -l

Bit swapping and alignment
==========================

Bit swapping
------------

Software with memory-mapped access to hardware registers must take care
of bit ordering. Processor endianess, device registers endianess as well
as other swapping may occur.

Bit swapping can be expressed as an integer with each bit specifying
swapping of bit packets with a fixed width in a word. If set, bit 0 then
forces the swapping of pairs of bits, bit 1 forces the swapping of
groups of 2 bits, and so on. Example: the value 0b11000 forces byte and
half-word swaps, that is, a complete byte swap on 32 bits words.

When multiple swap transformations are performed along the path from
software to hardware, the resulting swapping can be expressed by xoring
multiple integer swap directives.

Such swap directives can be specified at different locations:

  * Using the 'swap' package attribute to apply a swapping directive.

  * On a per register basis using the register 'swap' attribute.

Processor endianess specified using the -e command line flag also
contributes to bit swapping when generating memory accesses. Extra bit
swapping is applyed when endianess is little; swap directive depends
on memory access width.

Alignment
---------

Memory alignment attribute can constraint width of memory
accesses. The 'align' package attribute can be used to set the
alignment constraint width.

Boundary
--------

It is sometime important to prevent fields to span across a boundary
inside register. Furthermore some output backend impose boundary
restrictions due to bit swapping: bit swapping can not always be
performed if it would split a field into several parts. That's why you
may specify a boundary width which will be enforced by the tool on input
bitfield description.

The default boundary is 8 bits unless specified with the 'bound'
package or register attributes.

Repeated registers and fields expansion
=======================================

If the count attributes is greater than 1 and expansion is used, the
register or field format is instanciated multiple times and a
numerical suffix is appended to the name. The count and associated
attributes of is removed and the back-end only see the expanded set of
registers/fields as if they were unrolled in the input.

If the expansion is not used, it's up to the back-end to handle the
count parameter properly. Some back-end may force the expansion if
they are not able to handle the count parameter in a special way.

Conditional entries
===================

Registers and fields can be conditionally defined. The condition use a
C pre-processor #if syntax. Multiple condition lines for the same entry
are ored together.

The -D and -U command line options can be used to set values used by
condition evaluation. Register and fields with a condition that
evaluate to false will be discarded before calling the output backend.

Some backends are able generate output containing conditional
directives. In this case entries with a condition which evaluate to
false are retained and passed to the backend. The keep_conditions
package attribute control this behavior.

Available backends
==================

bitfield backend
----------------

The bitfield backend writes the parsed input in the same format.  This
is mainly for test and debug purpose. All fields will be shown including
inserted padding.

vhdl backends
-------------

The vhdl backend writes several useful vhdl declarations for each
register found in the input:

   * A constant with register width
   * A constant with padding bits mask
   * A record which contains all fields except padding
   * A slice function to convert a raw std_ulogic_vector to the record type
   * A concat function to convert the record type to a raw std_ulogic_vector

The vhdl2 backend writes similar code but concatenates all registers
in a single record.

cbitfield backend
-----------------

The cstruct backend writes a C struct for each register found in the
input, along with an additional struct containing the whole register
bank.

Bitfield endianess of the target processor can be specified with the
-e options.

Remember that when using field boundary greater than 8, C bitfields
can only be generated when no byte swapping is needed. So it's not
portable as it may not work depending on target processor endianess.

cacessor backend
----------------

Generate C functions for reading and writing register
fields. Generated code is optimized for requested alignment (memory
access width), bit swapping, processor endianess and other package
attributes.

Address space access expressions can be customized. Register bank can
be accessed through a generated struct or using direct register
address.

latex backend
-------------

Generate register table, field tables and register layout drawing.
The generated output requires some LaTeX packages to
compile. longtable and bytefield are used for tables and register
layouts.

The output consists of definition of new latex commands which can be
invoked to insert the various generated tables.

html backend
------------

Generate register table, field tables and register layout drawing
using xhtml tables.

mkdoc backend
-------------

Generate register table, field tables and register layout drawing
for inclusion in mkdoc documents.

Custom backends
===============

Backends are searched in bfgen script directory and in
directories listed in the BFGEN_PATH environment variable.

You can write your own backend in perl from scratch or start from one
of the existing backend found in the installation directory. You just
have to name your perl module like this: bfgen_*.pm .

