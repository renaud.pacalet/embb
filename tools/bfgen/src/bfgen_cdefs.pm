#    This file is part of bfgen.
#
#    bfgen is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bfgen is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bfgen.  If not, see <http://www.gnu.org/licenses/>.
#
#    Copyright (C) 2011, Alexandre Becoulet <alexandre.becoulet@free.fr>
#    Copyright (c) 2011, Institut Telecom / Telecom ParisTech

package bfgen_cdefs;

use strict;
use List::Util qw[min max];
use bigint;

our $pack = $main::pack;

sub init
{
    $main::known_pack_attrs{cdefs_use_reg_mask} = "Generate register used bits mask macros (0=off, 1=on)";
    $main::known_pack_attrs{cdefs_use_field_mask} = "Generate field mask macros (0=off, 1=on, 2=if single bit)";
    $main::known_pack_attrs{cdefs_use_field_shift} = "Generate field shift amount macros (0=off, 1=on)";
    $main::known_pack_attrs{cdefs_use_field_shifted_mask} = "Generate field shifted bit mask macros (0=off, 1=on, 2=if single bit and no value)";
    $main::known_pack_attrs{cdefs_use_field_shifter} = "Generate field value left shifting macros (0=off, 1=on, 2=if multi-bits or values)";
    $main::known_pack_attrs{cdefs_use_field_get} = "Generate field value extraction macros (0=off, 1=on, 2=if multi-bits or values)";
    $main::known_pack_attrs{cdefs_use_field_or} = "Generate field value masking macros (0=off, 1=on, 2=if multi-bits or values)";
    $main::known_pack_attrs{cdefs_use_field_orval} = "Generate alternate masking macros which dont use the value prefix (0=off, 1=on)";
    $main::known_pack_attrs{cdefs_use_field_set} = "Generate field value assignment macros (0=off, 1=on, 2=if multi-bits or values)";
    $main::known_pack_attrs{cdefs_use_field_setval} = "Generate alternate value assignment macros which don use the value prefix (0=off, 1=on)";
    $main::known_pack_attrs{cdefs_use_setter_value_prefix} = "Field setter macros paste the value name prefix (0=off, 1=on)";

    $pack->{cdefs_use_reg_mask} = 0;
    $pack->{cdefs_use_field_mask} = 0;
    $pack->{cdefs_use_field_shift} = 0;
    $pack->{cdefs_use_field_shifted_mask} = 2;
    $pack->{cdefs_use_field_shifter} = 2;
    $pack->{cdefs_use_field_get} = 2;
    $pack->{cdefs_use_field_or} = 0;
    $pack->{cdefs_use_field_orval} = 0;
    $pack->{cdefs_use_field_set} = 2;
    $pack->{cdefs_use_field_setval} = 0;
    $pack->{cdefs_use_setter_value_prefix} = 1;

    $main::known_pack_attrs{cdefs_sfx_reg_mask} = "Register mask macro suffix";
    $main::known_pack_attrs{cdefs_sfx_field_mask} = "Bit mask macro suffix";
    $main::known_pack_attrs{cdefs_sfx_field_shift} = "Shift amount macro suffix";
    $main::known_pack_attrs{cdefs_sfx_field_shifted_mask} = "Shifted bit mask macro suffix";
    $main::known_pack_attrs{cdefs_sfx_field_shifter} = "Shifting macro suffix";
    $main::known_pack_attrs{cdefs_sfx_field_get} = "Value extraction macro suffix";
    $main::known_pack_attrs{cdefs_sfx_field_or} = "Value masking macro suffix";
    $main::known_pack_attrs{cdefs_sfx_field_orval} = "Alternate value masking macro suffix";
    $main::known_pack_attrs{cdefs_sfx_field_set} = "Value assignment macro suffix";
    $main::known_pack_attrs{cdefs_sfx_field_setval} = "Alternate value assignment macro suffix";

    $pack->{cdefs_sfx_reg_mask} = "_MASK";
    $pack->{cdefs_sfx_field_mask} = "_MASK";
    $pack->{cdefs_sfx_field_shift} = "_SHIFT";
    $pack->{cdefs_sfx_field_shifted_mask} = "";
    $pack->{cdefs_sfx_field_shifter} = "";
    $pack->{cdefs_sfx_field_get} = "_GET";
    $pack->{cdefs_sfx_field_or} = "_OR";
    $pack->{cdefs_sfx_field_orval} = "_ORVAL";
    $pack->{cdefs_sfx_field_set} = "_SET";
    $pack->{cdefs_sfx_field_setval} = "_SETVAL";

    $main::known_pack_attrs{cdefs_use_reg_values} = "Generate macros for possible register values (0=off, 1=on)";
    $main::known_pack_attrs{cdefs_use_field_values} = "Generate macros for possible field values (0=off, 1=on)";
    $main::known_pack_attrs{cdefs_use_shifted_values} = "Register and field values are shifted (0=off, 1=on)";

    $pack->{cdefs_use_reg_values} = 1;
    $pack->{cdefs_use_field_values} = 1;
    $pack->{cdefs_use_shifted_values} = 0;

    $main::known_pack_attrs{cdefs_use_reg_comment} = "Generate comments for registers (0=off, 1=doc, 2=longnames, 3=both, 5=either)";
    $main::known_pack_attrs{cdefs_use_field_comment} = "Generate comments for fields (0=off, 1=doc, 2=longnames, 3=both, 5=either)";
    $main::known_pack_attrs{cdefs_use_value_comment} = "Generate comments for values (0=off, 1=doc, 2=longnames, 3=both)";

    $pack->{cdefs_use_reg_comment} = 1;
    $pack->{cdefs_use_field_comment} = 1;
    $pack->{cdefs_use_value_comment} = 1;

    $pack->{keep_conditions} = 1;
}

sub help
{
    print STDERR "Write C macros for registers address and fields masking\n";
}

sub print_values
{
    my ( $obj, $type, $prn ) = @_;

    return if !$obj->{_values};

    print '/** Specify acceptable values for the @em{'.lc($obj->{longname})."} $type. \@multiple */\n"
	if ($pack->{cdefs_use_value_comment} & 2);

    foreach my $k ( sort { $a <=> $b } keys %{$obj->{_values}} ) {
	my $v = $obj->{_values}->{$k};
	my $i = $v;
	if ( $v =~ /^([^:]+?)\s*:\s*(.*)$/ ) {
	    $i = $1;
	    $v = $2;
	    print "/** $v */\n" if ($pack->{cdefs_use_value_comment} & 1);
	}
	$i =~ s/\W+/_/g;
	$k = $k << $obj->{position} if ($pack->{cdefs_use_shifted_values});
	$prn->($i, $k);
    }
}

sub wrap80
{
    my ( $str, $prefix1, $prefix2 ) = @_;
    
    my $w;
    my $line = $prefix1;

    while ( $str =~ s/^\s*(\S+)//g ) {
	if ( (length $line) + (length $1) > 80 ) {
	    $w .= $line."\n";
	    $line = $prefix2.$1;
	} else {
	    $line .= " ".$1;
	}
    }

    $w .= $line.$str;

    return $w;
}

sub write
{
    my ( $p, $regs ) = @_;

    print 
"/***************************************
* Auto generated by BFGen, do not edit *
***************************************/

/*
$main::cmd_line
*/

#ifndef _".uc($pack->{name})."_BFGEN_DEFS_
#define _".uc($pack->{name})."_BFGEN_DEFS_

";

    my $if_indent = 1;

    foreach my $r ( @$regs ) {

	print "#".(" " x $if_indent++)."if $r->{condition}\n" if (defined $r->{condition});

	my $doc;

	if (($pack->{cdefs_use_reg_comment} & 1) && defined $r->{doc}) {
	    $doc = " $r->{doc}";
	}
	if ((($pack->{cdefs_use_reg_comment} & 4) && !defined $r->{doc}) ||
	    ($pack->{cdefs_use_reg_comment} & 2)) {
	    $doc .= ' Defines the address of the @em{'.lc($r->{longname})."} $r->{width} bits register.";
	}

	print wrap80("$doc \@multiple */\n", "/**", "   ") if defined $doc;

	my $rn = uc($pack->{name}."_".$r->{_name});
	$rn = $pack->{reg_prefix}."_".$rn if ($pack->{reg_prefix});

	if (int $r->{count} > 1) {
	    my $i = '(ridx)';
	    $i = "$i * $r->{index_step}" if ( $r->{index_step} != 1 );
	    $i = "($i + $r->{index_start})" if ( $r->{index_start} != 0 );
	    $i = "$i * $r->{stride}" if ( $r->{stride} != 1 );
	    printf "#define %-44s (0x%08x + $i)\n", $rn."_ADDR(ridx)", $r->{address};
	    printf "#define %-44s %u\n", $rn.'_COUNT', $r->{count};
	} else {
	    printf "#define %-44s 0x%08x\n", $rn."_ADDR", $r->{address};
	}

	printf "#define %-44s 0x%08x\n", $rn.$pack->{cdefs_sfx_reg_mask}, $r->{_padmask}
	  if ($pack->{cdefs_use_reg_mask});

	my $fmt = sprintf("%%0%ix", $r->{width} / 4);

	print_values( $r, "register", sub {
	    my ($i, $k) = @_;
	    printf "  #define %-40s 0x%08x\n", $rn."_".uc($i), $k;
	} ) if ($pack->{cdefs_use_reg_values});

	my $cur_fcond;

	# register fields
	foreach my $f ( @{$r->{_fields}} ) {
	    next if ($f->{_name} eq '__padding__');
	    my $n = uc($pack->{name}."_".$r->{_name}."_".$f->{_name});
	    $n = $pack->{field_prefix}."_".$n if ($pack->{field_prefix});

	    my $fid_arg;
	    my $fid_arg2;

	    if (int $r->{count} > 1) {
		$fid_arg = "(fid)";
		$fid_arg = "fid, ";
	    }

	    my $cond = $f->{condition};
	    if ($cur_fcond ne $cond) {
		print "#".(" " x --$if_indent)."endif\n" if defined $cur_fcond;
		print "#".(" " x $if_indent++)."if $f->{condition}\n" if defined $cond;
		$cur_fcond = $cond;
	    }

	    my $doc;

	    if (($pack->{cdefs_use_field_comment} & 1) && defined $f->{doc}) {
		$doc = " $f->{doc}";
	    }
	    if ((($pack->{cdefs_use_field_comment} & 4) && !defined $f->{doc}) ||
		($pack->{cdefs_use_field_comment} & 2)) {
		$doc .= ' Defines the @em{'.lc($f->{longname}).'} field of the @em{'.lc($r->{longname})."} register.";
	    }

	    print wrap80("$doc \@multiple */\n", "/**", "   ") if defined $doc;

	    my $ull = $r->{width} > 32 ? "ULL" : "";
	    my $has_values = $f->{_values} && $pack->{cdefs_use_field_values};
	    my $has_shifted_values = $has_values && $pack->{cdefs_use_shifted_values};

	    printf "  #define %-40s 0x${fmt}$ull\n", $n.$pack->{cdefs_sfx_field_mask},
	           ((1 << $f->{width}) - 1)
		if ($pack->{cdefs_use_field_mask} == 1 ||
		    ($pack->{cdefs_use_field_mask} == 2 && $f->{width} == 1));

	    my $fval_prefix = $pack->{cdefs_use_setter_value_prefix} && $has_values ? $n.'_##' : '';

	    my $use_shifted_mask = ($pack->{cdefs_use_field_shifted_mask} == 1 ||
				    ($pack->{cdefs_use_field_shifted_mask} == 2 && $f->{width} == 1) && !$has_values);

	    my $smask = ((1 << $f->{width}) - 1) << $f->{position};

	    if (int $f->{count} > 1) {
		my $i = '(fidx)';
		$i = "$i * $f->{index_step}" if ( $f->{index_step} != 1 );
		$i = "($i + $f->{index_start})" if ( $f->{index_start} != 0 );
		$i = "$i * $f->{stride}" if ( $f->{stride} != 1 );

		printf "  #define %-40s %u\n", $n.'_COUNT', $f->{count};

		printf "  #define %-40s (0x${fmt}$ull << ($i))\n", $n.$pack->{cdefs_sfx_field_shifted_mask}.'(fidx)', $smask
		    if ( $use_shifted_mask );

	        printf "  #define %-40s ($i + %u)\n", $n.$pack->{cdefs_sfx_field_shift}.'(fidx)',
	               $f->{position}
	            if ($pack->{cdefs_use_field_shift});
	    
		if ( !$has_shifted_values ) {
	            printf "  #define %-40s ((${fval_prefix}v) << ($i + %u))\n", $n.$pack->{cdefs_sfx_field_shifter}.'(fidx, v)',
	                   $f->{position}
	                if ($pack->{cdefs_use_field_shifter} == 1 ||
	    	        ($pack->{cdefs_use_field_shifter} == 2 && ($f->{width} != 1 || $has_values)));
	    	    
		    if ($pack->{cdefs_use_field_or} == 1 ||
	    	        ($pack->{cdefs_use_field_or} == 2 && ($f->{width} != 1 || $has_values))) {
			printf "  #define %-40s (((x) & ~(0x%x$ull << ($i))) | ((${fval_prefix}v) << ($i + %u)))\n", $n.$pack->{cdefs_sfx_field_orval}.'(fidx, x, v)',
			    $smask, $f->{position};

			printf "  #define %-40s (((x) & ~(0x%x$ull << ($i))) | ((v) << ($i + %u)))\n", $n.$pack->{cdefs_sfx_field_orval}.'(fidx, x, v)',
			    $smask, $f->{position} if (${fval_prefix} && $pack->{cdefs_use_field_orval});
		    }

		    if ($pack->{cdefs_use_field_set} == 1 ||
	    	        ($pack->{cdefs_use_field_set} == 2 && ($f->{width} != 1 || $has_values))) {
			printf "  #define %-40s do { (x) = (((x) & ~(0x%x$ull << ($i))) | ((${fval_prefix}v) << ($i + %u))); } while(0)\n", $n.$pack->{cdefs_sfx_field_set}.'(fidx, x, v)',
	                    $smask, $f->{position};

			printf "  #define %-40s do { (x) = (((x) & ~(0x%x$ull << ($i))) | ((v) << ($i + %u))); } while(0)\n", $n.$pack->{cdefs_sfx_field_setval}.'(fidx, x, v)',
	                    $smask, $f->{position} if (${fval_prefix} && $pack->{cdefs_use_field_setval});
		    }

	            printf "  #define %-40s (((x) >> ($i + %u)) & 0x%x$ull)\n", $n.$pack->{cdefs_sfx_field_get}.'(fidx, x)',
                           $f->{position}, ((1 << $f->{width}) - 1)
	    	    if ($pack->{cdefs_use_field_get} == 1 ||
	    	        ($pack->{cdefs_use_field_get} == 2 && ($f->{width} != 1 || $has_values)));
		}

		if ( $pack->{cdefs_use_field_values} ) {
		    if ( $pack->{cdefs_use_shifted_values} ) {
			print_values( $f, "field", sub {
			    my ($j, $k) = @_;
			    print "  " if ( $use_shifted_mask );
			    printf "  #define %-40s (0x%08x << ($i))\n", $n."_".uc($j).'(fidx)', $k;
			} );
		    } else {
			print_values( $f, "field", sub {
			    my ($i, $k) = @_;
			    print "  " if ( $use_shifted_mask );
			    printf "  #define %-40s 0x%08x\n", $n."_".uc($i), $k;
			} ) ;
		    }
		}

	    } else {
		printf "  #define %-40s 0x${fmt}$ull\n", $n.$pack->{cdefs_sfx_field_shifted_mask}, $smask
		    if ( $use_shifted_mask );

	        printf "  #define %-40s %u\n", $n.$pack->{cdefs_sfx_field_shift},
	               $f->{position}
	            if ($pack->{cdefs_use_field_shift});
	    
		if ( !$has_shifted_values ) {
	            printf "  #define %-40s ((${fval_prefix}v) << %u)\n", $n.$pack->{cdefs_sfx_field_shifter}.'(v)',
	                   $f->{position}
	                if ($pack->{cdefs_use_field_shifter} == 1 ||
	    	        ($pack->{cdefs_use_field_shifter} == 2 && ($f->{width} != 1 || $has_values)));
	    	    
		    if ($pack->{cdefs_use_field_or} == 1 ||
	    	        ($pack->{cdefs_use_field_or} == 2 && ($f->{width} != 1 || $has_values))) {
			printf "  #define %-40s (((x) & ~0x%x$ull) | ((${fval_prefix}v) << %u))\n", $n.$pack->{cdefs_sfx_field_or}.'(x, v)',
			    $smask, $f->{position};

			printf "  #define %-40s (((x) & ~0x%x$ull) | ((v) << %u))\n", $n.$pack->{cdefs_sfx_field_orval}.'(x, v)',
			    $smask, $f->{position} if (${fval_prefix} && $pack->{cdefs_use_field_orval});
		    }

		    if ($pack->{cdefs_use_field_set} == 1 ||
	    	        ($pack->{cdefs_use_field_set} == 2 && ($f->{width} != 1 || $has_values))) {
			printf "  #define %-40s do { (x) = (((x) & ~0x%x$ull) | ((${fval_prefix}v) << %u)); } while(0)\n", $n.$pack->{cdefs_sfx_field_set}.'(x, v)',
	                    $smask, $f->{position};

			printf "  #define %-40s do { (x) = (((x) & ~0x%x$ull) | ((v) << %u)); } while(0)\n", $n.$pack->{cdefs_sfx_field_setval}.'(x, v)',
	                    $smask, $f->{position} if (${fval_prefix} && $pack->{cdefs_use_field_setval});
		    }
		    
	            printf "  #define %-40s (((x) >> %u) & 0x%x$ull)\n", $n.$pack->{cdefs_sfx_field_get}.'(x)',
                           $f->{position}, ((1 << $f->{width}) - 1)
	    	    if ($pack->{cdefs_use_field_get} == 1 ||
	    	        ($pack->{cdefs_use_field_get} == 2 && ($f->{width} != 1 || $has_values)));
		}

		if ( $pack->{cdefs_use_field_values} ) {
		    my $indent = $has_shifted_values ? "  " : "    ";
		    print_values( $f, "field", sub {
			my ($i, $k) = @_;
			printf $indent."#define %-40s 0x%08x\n", $n."_".uc($i), $k;
		    } );
		}

	    }
	}

	print "#".(" " x --$if_indent)."endif\n" if (defined $cur_fcond);

	print "#".(" " x --$if_indent)."endif\n" if (defined $r->{condition});

	print "\n";
    }

    print "#endif

";
}

1;

