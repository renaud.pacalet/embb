#    This file is part of bfgen.
#
#    bfgen is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bfgen is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bfgen.  If not, see <http://www.gnu.org/licenses/>.
#
#    Copyright (C) 2010, Alexandre Becoulet <alexandre.becoulet@free.fr>
#    Copyright (c) 2010, Institut Telecom / Telecom ParisTech

package bfgen_caccessor;

use strict;
use List::Util qw[min max];
use bigint;

our $pack = $main::pack;

sub init
{
    $main::known_pack_attrs{caccessor_param_type} = "Type name of pointer to struct holding registers";
    $main::known_pack_attrs{caccessor_extra_field} = "Additional fields to access registers in struct";
    $main::known_pack_attrs{caccessor_use_reg_name} = "Use register name in accessor functions name";
    $main::known_pack_attrs{caccessor_decl_macro} = "Put C declarations inside preprocessor macros";

    $main::known_pack_attrs{caccessor_read_expr} = "Memory read template expression";
    $main::known_pack_attrs{caccessor_write_expr} = "Memory write template expression";
    $main::known_pack_attrs{caccessor_no_struct} = "Do not use struct, reference by base address";
    $main::known_pack_attrs{caccessor_use_volatile} = "Use volatile keyword for memory accesses";
    $main::known_pack_attrs{caccessor_optimize} = "Discard memory accesses for masked out bits";
    $main::known_pack_attrs{caccessor_attributes} = "Set functions attributes, default is `static inline'";
    $main::known_pack_attrs{caccessor_includes} = "Comma separated list of C header files to include";
    $main::known_pack_attrs{caccessor_reg_func} = "Generate register wide accessor functions even if some fields are defined";
    $main::known_pack_attrs{caccessor_checking} = "Use assert to check bounds in generated code";
    $main::known_pack_attrs{caccessor_merge_idx_params} = "Merge field and register index parameters into a single parameter";

    $pack->{caccessor_includes} = "stdint.h";
    $pack->{caccessor_use_reg_name} = 1;
    $pack->{caccessor_optimize} = 1;
    $pack->{caccessor_attributes} = 'static inline';
    $pack->{caccessor_read_expr}  = '@p[@i]';
    $pack->{caccessor_write_expr} = '@p[@i] = @v';
    $pack->{caccessor_reg_func} = 0;
    $pack->{caccessor_checking} = 0;
    $pack->{caccessor_merge_idx_params} = 0;
}

sub help
{
    print STDERR "Write C register access functions\n";
}

our $struct;
our $param_type;
our $extra_field;

our $buf;

sub apply_subst
{
    my ( $expr, $i, $s, $addr, $width, $awidth, $reg ) = @_;

    my $idx = int(($i ^ $s) / $awidth);
    my $a = $addr->as_hex();

    if (int $reg->{count} > 1) {
	my $r = '__ridx';
	my $w;

	$r = "$r * $reg->{index_step}" if ( $reg->{index_step} != 1 );
	$r = "($r + $reg->{index_start})" if ( $reg->{index_start} != 0 );

	$w = $reg->{stride} / ($awidth / 8);
	$idx = ($w != 1) ? "($r * $w + $idx)" : "($r + $idx)";

	$w = $reg->{stride};
	$a = ($w != 1) ? "($r * $w + $a)" : "($r + $a)";
    }

    $expr =~ s/\@i/$idx/g;
    $expr =~ s/\@a/$a/ge;
    $expr =~ s/\@w/$awidth/ge;
    return $expr;
}

sub print_fshift
{
    my ( $f ) = @_;

    $buf .= "    assert(__fidx >= 0 && __fidx < $f->{count});\n" if ( $pack->{caccessor_checking} );

    my $fs = '__fidx';
    $fs = "$fs * $f->{index_step}" if ( $f->{index_step} != 1 );
    $fs = "($fs + $f->{index_start})" if ( $f->{index_start} != 0 );
    $fs = "$fs * $f->{stride}" if ($f->{stride} != 1);
    $buf .= "    __value <<= $fs;\n";
}

sub print_load
{
    my ( $var, $addr, $rname, $swap, $width, $awidth, $mask, $r ) = @_;

    $mask = (1 << $width) - 1 if (!$pack->{caccessor_optimize});

    my $type1 = "uint${width}_t";
    my $type2 = "uint${awidth}_t";

    my $s = ($swap ^ 0b1111000) % $width;

    my @load;
    my $use_array;

    for ( my $i = 0; $i < $width; $i += $awidth ) {
	if ((((1 << $awidth) - 1) << $i) & $mask) {
	    my $re = apply_subst($pack->{caccessor_read_expr}, $i, $s, $addr, $width, $awidth, $r);
	    $use_array |= $re =~ s/\@p/__r/g;

	    if ($i) {
		push @load, "((uint${width}_t)$re << ".$i.")";
	    } else {
		push @load, "(uint${width}_t)$re";
	    }
	}
    }

    die if !@load;

    my $v = $pack->{caccessor_use_volatile} ? 'volatile ' : '';
    if ( $use_array == 1) {
	if ( $pack->{caccessor_no_struct} ) {
	    $buf .= "    ${v}const $type2 *__r = (const $type2*)(__base + ".$r->{address}->as_hex()."ULL);\n";
	} else {
	    $buf .= "    ${v}const $type2 *__r = (const $type2*)&__base->$pack->{caccessor_extra_field}$rname;\n";
	}
    }
    $buf .= "    $type1 $var = ";
    $buf .= join(' | ', @load).";\n";
}

sub print_store
{
    my ( $var, $addr, $rname, $swap, $width, $awidth, $mask, $r ) = @_;

    $mask = (1 << $width) - 1 if (!$pack->{caccessor_optimize});

    my $type1 = "uint${width}_t";
    my $type2 = "uint${awidth}_t";

    my $s = ($swap ^ 0b1111000) % $width;
    my $use_array;
    my $v = $pack->{caccessor_use_volatile} ? 'volatile ' : '';

    for ( my $i = 0; $i < $width; $i += $awidth ) {
	if ((((1 << $awidth) - 1) << $i) & $mask) {
	    my $we = apply_subst($pack->{caccessor_write_expr}, $i, $s, $addr, $width, $awidth, $r);
	    $use_array += $we =~ s/\@p/__w/g;
	    if ($i) {
		$we =~ s/\@v/$var >> $i/g;
	    } else {
		$we =~ s/\@v/$var/g;
	    }

	    if ( $use_array == 1) {
		if ( $pack->{caccessor_no_struct} ) {
		    $buf .= "    $v$type2 *__w = ($type2*)(__base + ".$r->{address}->as_hex()."ULL);\n";
		} else {
		    $buf .= "    $v$type2 *__w = ($type2*)&__base->$pack->{caccessor_extra_field}$rname;\n";
		}
	    }
	    $buf .= "    $we;\n";
	}
    }
}

our @swmask = (
    0x55555555555555555555555555555555,
    0x33333333333333333333333333333333,
    0x0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f,
    0x00ff00ff00ff00ff00ff00ff00ff00ff,
    0x0000ffff0000ffff0000ffff0000ffff,
    0x00000000ffffffff00000000ffffffff,
    0x0000000000000000ffffffffffffffff
    );

sub print_swap_shift
{
    my ( $var, $swap, $width, $mask, $pre_rshift, $post_rshift ) = @_;

    my $flush_shift = sub
    {
	if ($pre_rshift > 0) {
	    $buf .= "    $var >>= $pre_rshift;\n";
	    $mask >>= $pre_rshift;
	} elsif ($pre_rshift < 0) {
	    $buf .= "    $var <<= ".(-$pre_rshift).";\n";
	    $mask <<= -$pre_rshift;
	}
	$pre_rshift = 0;
    };

    if (!$pack->{caccessor_optimize}) {
	$flush_shift->();
	$mask = (1 << $width) - 1;
    }

    for ( my ($i, $j) = (64, 6); $i >= 1; $j--, $i >>= 1 ) {
	next if ( ( $i >= $width ) || not ( $i & $swap ) );

	if ($mask & ~@swmask[$j]) {
	    if ($mask & @swmask[$j]) {
		$flush_shift->();
		$buf .= "    static const uint${width}_t __mask$j = ".(@swmask[$j] % (1<<$width))->as_hex()."ULL;\n";
		$buf .= "    $var = (($var & ~__mask$j) >> $i) | (($var & __mask$j) << $i);\n";
		$mask = (($mask & ~@swmask[$j]) >> $i) | (($mask & @swmask[$j]) << $i);
	    } else {
		$pre_rshift += $i;
	    }
	} else {
	    if ($mask & @swmask[$j]) {
		$pre_rshift -= $i;
	    } else {
		die $mask;
	    }
	}
    }

    $flush_shift->() if !$pack->{caccessor_optimize};

    $pre_rshift += $post_rshift;

    $flush_shift->();
}

sub get_swap_mask
{
    my ( $swap, $f, $width ) = @_;
    my $m1 = 0;

    if ( int $f->{count} > 1 ) {
	for ( my $i = 0; $i < $f->{count}; $i++ ) {
	    $m1 |= ((1 << $f->{width}) - 1) << ($f->{position} + $i * $f->{stride});
	}
    } else {
	$m1 = ((1 << $f->{width}) - 1) << $f->{position};
    }

    my $m2 = $m1;

    for ( my ($i, $j) = (1, 0); $i < $width; $j++, $i *= 2 ) {
	next if ( not ( $i & $swap ) );
	$m2 = (($m2 & ~@swmask[$j]) >> $i) | (($m2 & @swmask[$j]) << $i);
    }

    return ( $m1, $m2 );
}

sub print_values
{
    my ( $obj, $tmpl, $type ) = @_;

    return if !$obj->{_values};

    $buf .= "
/** Specify acceptable values for $obj->{longname} $type. */
enum ${tmpl}_e {
";

    while (my ( $k, $v ) = each( %{$obj->{_values}} ) ) {
	my $i = $v;
	if ( $v =~ /^([^:]+?)\s*:\s*(.*)$/ ) {
	    $i = $1;
	    $v = $2;
	}
	$i =~ s/\W+/_/g;
	$buf .= "  /** $v */
  ".uc($tmpl)."_".uc($i)." = $k,\n";
    }

    $buf .= "};
"
}

sub flush_macro
{
    my ( $name ) = @_;

    if ( $pack->{caccessor_decl_macro} ) {
	$buf =~ s/\n/ \\\n/g;
	print "#define $name \\\n$buf\n";
	$buf = "";
    } else {
	print $buf;
	$buf = "";
    }
}

sub write
{
    my ( $p, $regs ) = @_;

    return main::error( "the keep_conditions attribute is not supported by this backend" )
	if ( $pack->{keep_conditions} );

    $struct = $pack->{'name'}.'_regs';

    if ($pack->{caccessor_no_struct}) {
	$pack->{caccessor_param_type} ||= 'uintptr_t';
	$pack->{caccessor_read_expr}  ||= '*(uint@w_t*)(s + @a)';
	$pack->{caccessor_write_expr} ||= '*(uint@w_t*)(s + @a) = @v';
    } else {
	$pack->{caccessor_param_type} ||= "struct $struct *";
	$pack->{caccessor_read_expr}  ||= '@p[@i]';
	$pack->{caccessor_write_expr} ||= '@p[@i] = @v';
    }

    print 
"/****************************************
** Auto generated by BFGen, do not edit
****************************************/

/*
$main::cmd_line

   Configuration:
    memory alignment    : $pack->{align} bits
    cpu endianess       : $main::endian
    hardware swap       : ".sprintf("0x%x", $pack->{swap})."
*/

";

    print  "#include \"$_\"\n"
	foreach split /\s*,\s*/, $pack->{caccessor_includes};

    print  "#include <assert.h>\n"
	if ( $pack->{caccessor_checking} );

    print "
";

    if (!$pack->{caccessor_no_struct}) {
	my $addr = 0;

	my $sdoc = $pack->{inlinedoc};
	$sdoc ||= "Packed struct containing registers of $pack->{longname}";

	$buf .= "/** $sdoc */
struct $struct
\{
";

	my $ustate;
	for ( my $i = 0; $i < scalar @$regs; $i++ ) {
	    my $r = $regs->[$i];
	    my $nr = $regs->[$i+1];

	    if ($r->{address} > $addr) {
		$buf .= sprintf "  uint8_t _pad_$addr\[0x%x\];\n", ($r->{address} - $addr);
	    }

	    if ( !$ustate && $nr && $r->{address} == $nr->{address} ) {
		$buf .= "  union {\n";
		$ustate = 1;
	    }

	    $addr = max( $addr, $r->{address} + int($r->{width} / 8) );

	    my $rname = $main::regpfix.$r->{_name}.$main::regsfix;
	    $buf .= "/** $r->{inlinedoc} */\n" if ( $r->{inlinedoc} );
	    $buf .= "  uint$r->{width}_t $rname";
	    $buf .= "[$r->{count}]" if int $r->{count} > 1;
	    $buf .= ";\n";

	    if ( $ustate && ( !$nr || $r->{address} != $nr->{address} ) ) {
		$buf .= "  };\n";
		$ustate = 0;
	    }
	}

	$buf .= 
	    "} __attribute__ ((packed));
";
    }

    flush_macro( uc($pack->{'name'}."_STRUCT_DECL") );

    foreach my $r ( @$regs ) {
	my $type = "uint$r->{width}_t";
        my $rname = $main::regpfix.$r->{_name}.$main::regsfix;
	my $width = $r->{width};

	# memory access width
	my $awidth = $r->{access_width};

	# hardware bitswaping of device register/memory content
	my $swap = $pack->{swap} ^ $r->{swap};

	# processor endianness does swap bytes order within accessed
	# word but must not change words order.
	$swap ^= 0b111111000 % $awidth if ( $main::endian eq 'little' );

	# swapping within memory access width
	my $wordswap = $swap & ( $awidth - 1 );

	if ( $width > 128 || ( $width & ( $width - 1 ) ) ) {
	    main::error( "caccessor: '$r->{_name}' has bad register width." );
	    next;
	}

	if ($r->{_nofield} || $pack->{caccessor_reg_access}) {
	    my $rfname = $pack->{caccessor_use_reg_name} ? '_'.$rname : "";
	    print_values( $r, "$pack->{name}$rfname", "register" );

	    my $mask = (1 << $width) - 1;

	    ##################################################

	    if ( $r->{direction} =~ /r/ ) {

		$buf .= "
/** This function reads the \@ref ${struct}::$r->{_name} register. */
$pack->{caccessor_attributes} $type $pack->{'name'}_get_${rname}(const $pack->{caccessor_param_type} __base";
		$buf .= ", intptr_t __ridx" if (int $r->{count} > 1);
		$buf .= ")
\{
";
		$buf .= "    assert(__ridx >= 0 && __ridx < $r->{count});\n"
		    if ( $pack->{caccessor_checking} && int $r->{count} > 1);
		print_load( "__value", $r->{address}, $rname, $swap, $width, $awidth, $mask, $r );
		print_swap_shift( "__value", $wordswap, $width, $mask, 0, 0 );
		$buf .= "    return __value;\n";
    $buf .= "}
";
	    }

	    ##################################################

	    if ( $r->{direction} =~ /w/ ) {

		$buf .= "
/** This function writes the \@ref ${struct}::$r->{_name} register. */
$pack->{caccessor_attributes} void $pack->{'name'}_set_${rname}($pack->{caccessor_param_type} __base";
		$buf .= ", intptr_t __ridx" if (int $r->{count} > 1);
		$buf .= ", $type __value)
\{
";
		if ( $pack->{caccessor_checking} ) {
		    $buf .= "    assert(__ridx >= 0 && __ridx < $r->{count});\n" if (int $r->{count} > 1);
		    $buf .= "    assert(__value >= $r->{_range_min});\n" if (defined $r->{_range_min});
		    $buf .= "    assert(__value <= $r->{_range_max});\n" if (defined $r->{_range_max});
		}
		print_swap_shift( "__value", $wordswap, $width, $mask, 0, 0 );
		print_store( "__value", $r->{address}, $rname, $swap, $width, $awidth, $mask, $r );
    $buf .= "}
";
	    }

	}

	    ##################################################

	if (!$r->{_nofield} && $r->{direction} =~ /w/) {

	    my $fcnt = 0;
	    foreach my $f ( @{$r->{_fields}} ) {
		$fcnt++ if ($f->{_name} ne '__padding__');

		if (int $f->{count} > 1) {
		    $fcnt = 0;
		    last;         # FIXME no setall function if repeated field found
		}
	    }

	    if ($fcnt > 1) {
		$buf .= "
/** This function writes all fields of the \@ref ${struct}::$r->{_name} register. */
$pack->{caccessor_attributes} void $pack->{'name'}_setall_${rname}($pack->{caccessor_param_type} __base";
		$buf .= ", intptr_t __ridx" if (int $r->{count} > 1);

		my $mask = 0;

		foreach my $f ( @{$r->{_fields}} ) {
		    next if ($f->{_name} eq '__padding__');
		    $mask |= ((1 << $f->{width}) - 1) << $f->{position};
		    $buf .= ", $type $f->{_name}"
			
		}
		$buf .= ")
\{
";
		$buf .= "    $type __value = ";
		my $or = "";
		foreach my $f ( @{$r->{_fields}} ) {
		    next if ($f->{_name} eq '__padding__');
		    $buf .= $or."($f->{_name} << $f->{position})";
		    $or = " | ";
		}
		$buf .= ";
";

		if ( $pack->{caccessor_checking} ) {
		    $buf .= "    assert(__ridx >= 0 && __ridx < $r->{count});\n" if (int $r->{count} > 1);
		    foreach my $f ( @{$r->{_fields}} ) {
			next if ($f->{_name} eq '__padding__');
			$buf .= "    assert($f->{_name} >= $f->{_range_min});\n" if (defined $f->{_range_min});
			$buf .= "    assert($f->{_name} <= $f->{_range_max});\n" if (defined $f->{_range_max});
		    }
		}

		print_swap_shift( "__value", $wordswap, $width, $mask, 0, 0 );
		print_store( "__value", $r->{address}, $rname, $swap, $width, $awidth, $mask, $r );
    $buf .= "}
";
	    }

	}

	    ##################################################

	if ( !$r->{_nofield} ) {

	    foreach my $f ( @{$r->{_fields}} ) {
		next if ($f->{_name} eq '__padding__');
		my $fname = $main::fieldpfix.$f->{_name}.$main::fieldsfix;
		my $rfname = $pack->{caccessor_use_reg_name} ? '_'.$rname : "";

		my $mask = (1 << $f->{width}) - 1;
		my ( $m1, $m2 ) = get_swap_mask( $wordswap, $f, $width );

		my $merge_idx = $pack->{caccessor_merge_idx_params} && int $r->{count} > 1 && int $f->{count} > 1;

		if ( $f->{direction} =~ /r/ ) {

		    $buf .= "
/** This function reads the $f->{_name} ($f->{longname}) field of the \@ref ${struct}::$r->{_name} register. */
$pack->{caccessor_attributes} $type $pack->{'name'}_get${rfname}_$fname(const $pack->{caccessor_param_type} __base";
		    if ( $merge_idx ) {
			$buf .= ", intptr_t __idx";
		    } else {
			$buf .= ", intptr_t __ridx" if (int $r->{count} > 1);
			$buf .= ", intptr_t __fidx" if (int $f->{count} > 1);
		    }
		    $buf .= ")
\{
";

		    if ( $merge_idx ) {
			$buf .= "    intptr_t __ridx = __idx / $f->{count};\n";
			$buf .= "    intptr_t __fidx = __idx % $f->{count};\n";
		    }

		    if ( $pack->{caccessor_checking} ) {
			$buf .= "    assert(__ridx >= 0 && __ridx < $r->{count});\n" if (int $r->{count} > 1);
		    }
		    print_load( "__value", $r->{address}, $rname, $swap, $width, $awidth, $m2, $r );
		    print_swap_shift( "__value", $wordswap, $width, $m2, 0, $f->{position} );
		    print_fshift( $f ) if ( int $f->{count} > 1 );

		    $buf .= "    return __value & ".$mask->as_hex()."ULL;\n";
		    $buf .= "}
";
		}

	    ##################################################

		if ( $f->{direction} =~ /w/ ) {

		    $buf .= "
/** This function writes the $f->{_name} ($f->{longname}) field in the \@ref ${struct}::$r->{_name} register. */
$pack->{caccessor_attributes} void $pack->{'name'}_set${rfname}_$fname($pack->{caccessor_param_type} __base";
		    if ( $merge_idx ) {
			$buf .= ", intptr_t __idx";
		    } else {
			$buf .= ", intptr_t __ridx" if (int $r->{count} > 1);
			$buf .= ", intptr_t __fidx" if (int $f->{count} > 1);
		    }
		    $buf .= ", $type __value)
\{
";

		    if ( $merge_idx ) {
			$buf .= "    intptr_t __ridx = __idx / $f->{count};\n";
			$buf .= "    intptr_t __fidx = __idx % $f->{count};\n";
		    }

		    if ( $pack->{caccessor_checking} ) {
			$buf .= "    assert(__ridx >= 0 && __ridx < $r->{count});\n" if (int $r->{count} > 1);
			$buf .= "    assert(__value >= $f->{_range_min});\n" if (defined $f->{_range_min});
			$buf .= "    assert(__value <= $f->{_range_max});\n" if (defined $f->{_range_max});
		    }

		    if ( int $r->{_fcount} > 1 || int $f->{count} > 1 ) {

			$buf .= "    static const uint${width}_t __mask = ".$m2->as_hex()."ULL;\n";
			print_fshift( $f ) if ( int $f->{count} > 1 );
			print_swap_shift( "__value", $wordswap, $width, $m1, -$f->{position}, 0 );
			if ($f->{position} % $awidth == 0 && $f->{width} % $awidth == 0) {
			    $buf .= "    uint${width}_t __x = (__value & __mask);\n";
			} else {
			    print_load( "__x", $r->{address}, $rname, $swap, $width, $awidth, $m2, $r );
			    $buf .= "    __x = (__x & ~__mask) | (__value & __mask);\n";
			}
			print_store( "__x", $r->{address}, $rname, $swap, $width, $awidth, $m2, $r );

		    } else {
			$buf .= "    static const uint${width}_t __mask = ".$mask->as_hex()."ULL;\n";
			$buf .= "    __value &= __mask;\n";
			print_swap_shift( "__value", $wordswap, $width, $m1, -$f->{position}, 0 );
			print_store( "__value", $r->{address}, $rname, $swap, $width, $awidth, $m2, $r );
		    }
		    $buf .= "}
";
		}

	    ##################################################

		print_values( $f, "$pack->{name}${rfname}_${fname}", "field" );
	    }
	}
    }

    flush_macro( uc($pack->{'name'}."_FUNC_DECL") );
}

1;

