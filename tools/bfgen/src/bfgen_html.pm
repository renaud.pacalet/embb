#    This file is part of bfgen.
#
#    bfgen is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bfgen is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bfgen.  If not, see <http://www.gnu.org/licenses/>.
#
#    Copyright (C) 2010, Alexandre Becoulet <alexandre.becoulet@free.fr>
#    Copyright (c) 2010, Institut Telecom / Telecom ParisTech

package bfgen_html;

use strict;
use List::Util qw[min max];

our $pack = $main::pack;

sub _esc
{
    my ( $str ) = @_;

    my %escmap = ( '<' => '&lt;',
		   '>' => '&gt;',
		   '&' => '&amp;',
		   '"' => '&quot;',
		   "\n" => "<br />\n" );

    $str =~ s/((?!\&\[a-z]+;)[<>&\n])/$escmap{$1}/ges;

    while ($str =~ s/  /&nbsp; /g) {}

    return $str;
}

sub init
{
    $pack->{html_table_class} = "bfgen_table";
    $pack->{html_bitfield_class} = "bfgen_bitfield";
    $pack->{html_bitfield_link} = 1;
    $pack->{doc_split_width} = 32;

    $main::known_pack_attrs{html_table_class} = "Html table css class name";
    $main::known_pack_attrs{html_bitfield_class} = "Html bitfield layout table css class name";
    $main::known_pack_attrs{html_use_bit_ranges} = "Html bitfield table header use ranges instead of single bit cells";
    $main::known_pack_attrs{html_bitfield_link} = "Use html links for field names in bitfield tables";

    $pack->{expand} = 1;
}

sub help
{
    print STDERR "Write xhtml document\n";
}

sub print_bitfield_map
{
    my ( $r ) = @_;

    print "<table class=\"".$pack->{html_bitfield_class}."\">\n";

    my @fs = main::fields_split( $r, $pack->{doc_split_width} );
    my $mw = min( $pack->{doc_split_width}, $r->{width} );
    my ( $i, $j ) = ( 0, 0 );
    my $f;

    do {
	my $th;
	my $td;

	for (; $f = @fs[$i]; ) {

	    my $w = $f->{_lastpos} - $f->{position} + 1;
	    my $tmp;

	    if ( $pack->{html_use_bit_ranges} ) {
		my $hw = 100 / $w * $mw;
		if ($w > 1) {
		    $tmp = "<th colspan=\"$w\" style=\"width: $hw%;\">$f->{position}&nbsp;-&nbsp;$f->{_lastpos}</th>";
		} else {
		    $tmp = "<th style=\"width: $hw%;\">$f->{position}</th>";
		}
		$th = $pack->{doc_lsb_on_left} ? $th.$tmp : $tmp.$th;

	    } else {
		my $hw = 100 / $mw;
		for ( my $k = 0; $k < $w; $k++ ) {
		    $tmp = "<th style=\"width: $hw%; text-align:center;\">".($k + $f->{position})."</th>";
		    $th = $pack->{doc_lsb_on_left} ? $th.$tmp : $tmp.$th;
		}
	    }

	    if ($f->{_name} eq '__padding__') {
		$tmp = "<td colspan=\"$f->{width}\" style=\"background:gray; text-align:center;\"><em>unused</em></td>";
	    } else {
		my $name = $f->{_name};
		if ( $pack->{html_bitfield_link} ) {
		    $name = "<a href=\"#bitfield_$r->{_name}_$name\" id=\"table_$r->{_name}_$name\">$name</a>";
		}
		$tmp = "<td colspan=\"$f->{width}\" style=\"text-align:center;\">$name</td>";
	    }

	    $td = $pack->{doc_lsb_on_left} ? $td.$tmp : $tmp.$td;

	    $i++;
	    if ( ($f->{_lastpos} + 1) % $mw == 0 ) {
		print "  <tr>$th</tr>\n";
		print "  <tr>$td</tr>\n";
		last;
	    }
	}

    } while ( $f );

    print "</table>\n";
}

sub print_fields_table
{
    my ( $r ) = @_;

    print "<table class=\"".$pack->{html_table_class}."\"><tr><th>Name</th><th>Width</th>";
    print "<th>Direction</th>" if ( $pack->{doc_field_direction_column} );
    print "<th>Long name</th>" if ( $pack->{doc_field_longname_column} );
    print "<th>Description</th>" if ( $pack->{doc_field_doc_column} );
    print "</tr>\n";

    my $tbl = "";

    foreach my $f ( @{$r->{_fields}} ) {
	next if ($f->{_name} eq '__padding__');

	my $name = $f->{_name};
	if ( $pack->{html_bitfield_link} ) {
	    $name = "<a href=\"#table_$r->{_name}_$name\" id=\"bitfield_$r->{_name}_$name\">$name</a>";
	}
	$tbl .= "  <tr><td><code>$name</code></td>\n";
	$tbl .= "      <td>$f->{width} bits</td>\n";
	$tbl .= "      <td>".$f->{direction}."</td>\n" if ( $pack->{doc_field_direction_column} );
	$tbl .= "      <td>"._esc($f->{longname})."</td>\n" if ( $pack->{doc_field_longname_column} );
	$tbl .= "      <td>"._esc($f->{doc})."</td>" if ( $pack->{doc_field_doc_column} );
	$tbl .= "</tr>\n";
    }

    print $tbl;
    print "</table>\n";

    return $tbl;
}

sub print_regs_table
{
    my ( $regs ) = @_;

    print "<table class=\"".$pack->{html_table_class}."\"><tr><th>Name</th>";
    print "<th>Direction</th>" if ( $pack->{doc_reg_direction_column} );
    print "<th>Address</th>" if ( $pack->{doc_reg_address_column} );
    print "<th>Long name</th>" if ( $pack->{doc_reg_longname_column} );
    print "<th>Description</th>" if ( $pack->{doc_reg_doc_column} );
    print "</tr>\n";

    foreach my $r ( @$regs ) {
	my $name = $r->{_name};
	if ( $pack->{html_bitfield_link} && !$r->{_nofield} ) {
	    $name = "<a href=\"#table_fields_$r->{_name}\" id=\"table_regs_$r->{_name}\">$name</a>";
	}
	print "  <tr><td><code>$name</code></td>\n";
	print "      <td>".$r->{direction}."</td>\n" if ( $pack->{doc_reg_direction_column} );
	printf "      <td>0x%0x</td>\n", $r->{address} if ( $pack->{doc_reg_address_column} );
	print "      <td>"._esc($r->{longname})."</td>\n" if ( $pack->{doc_reg_longname_column} );
	print "      <td>"._esc($r->{doc})."</td></tr>\n" if ( $pack->{doc_reg_doc_column} );
	print "</tr>\n";
    }

    print "</table>\n";
}

sub write
{
    my ( $p, $regs ) = @_;

    return main::error( "the keep_conditions attribute is not supported by this backend" )
	if ( $pack->{keep_conditions} );

    my $all_fields_rows;

    print "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
 <!-- 
$main::cmd_line
 -->
 <title>BFGen HTML output</title>
 <link rel=\"stylesheet\" href=\"bfgen.css\" />
 <meta name=\"Generator\" content=\"bfgen\" />
</head>
<body>\n";

    print "<h3>"._esc($pack->{longname})." registers table</h3>";
    print_regs_table( $regs );

    foreach my $r ( @$regs ) {

	next if ($r->{_nofield});
	print "<a id=\"table_fields_$r->{_name}\"></a>" if ( $pack->{html_bitfield_link} );
	print "<h3>"._esc($r->{longname})." register</h3>";

	print "<h4>Register layout</h4>";
	print_bitfield_map( $r );

	print "<h4>Register fields</h4>";
	$all_fields_rows .= print_fields_table( $r );
    }

####################################################

    {

	print "<h3>"._esc($pack->{longname})." fields from all registers</h3>";
	print "<table class=\"".$pack->{html_table_class}."\"><tr><th>Name</th><th>Width</th>";
	print "<th>Direction</th>" if ( $pack->{doc_field_direction_column} );
	print "<th>Long name</th>" if ( $pack->{doc_field_longname_column} );
	print "<th>Description</th>" if ( $pack->{doc_field_doc_column} );
	print "</tr>\n";
	print $all_fields_rows;
	print "</table>\n";

    }

####################################################

    print "</body></html>\n\n";
}

1;

