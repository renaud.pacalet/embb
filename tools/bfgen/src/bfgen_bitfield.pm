#    This file is part of bfgen.
#
#    bfgen is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bfgen is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bfgen.  If not, see <http://www.gnu.org/licenses/>.
#
#    Copyright (C) 2010, Alexandre Becoulet <alexandre.becoulet@free.fr>
#    Copyright (c) 2010, Institut Telecom / Telecom ParisTech

package bfgen_bitfield;

use strict;

our $pack = $main::pack;

sub init
{
    $main::known_pack_attrs{bitfield_dump_internals} = "Include internal reg/field attributes in output";

    $pack->{bitfield_dump_internals} = 0;

    $pack->{keep_conditions} = 1;
}

sub help
{
    print STDERR "Write in same format as input\n";
}

sub write
{
    my ( $p, $regs ) = @_;

    # package attributes
    while (my ($k, $v) = each(%$pack)) {
	next if $k =~ /^_/;
	print "$k: $v\n";
    }

    foreach my $r ( @$regs ) {
	print "\n%$r->{_name}\n";

	# register attributes
	while (my ($k, $v) = each(%$r)) {
	    next if $k =~ /^_/ && !$pack->{bitfield_dump_internals};
	    print "  $k: $v\n";
	}

	# register fields
	foreach my $f ( @{$r->{_fields}} ) {
	    print "\n  %%$f->{_name}\n";

	    # field attributes
	    while (my ($k, $v) = each(%$f)) {
		next if $k =~ /^_/ && !$pack->{bitfield_dump_internals};
		print "    $k: $v\n";
	    }
	}
    }
}

1;

