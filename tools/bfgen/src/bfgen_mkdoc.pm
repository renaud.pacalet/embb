#    This file is part of bfgen.
#
#    bfgen is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    bfgen is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with bfgen.  If not, see <http://www.gnu.org/licenses/>.
#
#    Copyright (C) 2010, Alexandre Becoulet <alexandre.becoulet@free.fr>
#    Copyright (c) 2010, Institut Telecom / Telecom ParisTech

package bfgen_mkdoc;

use strict;

require bfgen_html;
require bfgen_latex;

our $pack = $main::pack;

sub _esc
{
    my ( $str ) = @_;

    $str =~ s/([\@\{\}])/@\1/g;
    return $str;
}

sub init
{
    bfgen_html::init();
    bfgen_latex::init();

    delete $main::known_pack_attrs{html_bitfield_link};
    $pack->{html_bitfield_link} = 0;

    $pack->{html_table_class} = "table";
    $pack->{html_bitfield_class} = "table";

    $pack->{expand} = 1;
}

sub help
{
    print STDERR "Write mkdoc documentation file\n";
}

sub write
{
    my ( $p, $regs ) = @_;

    return main::error( "the keep_conditions attribute is not supported by this backend" )
	if ( $pack->{keep_conditions} );

    my $all_fields_rows;
    my $cmd_line = $main::cmd_line;
    $cmd_line =~ s/^/\@c/gm;

	print 
"\@c cccccccccccccccccccccccccccccccccccc
\@c Auto generated by BFGen, do not edit
\@c cccccccccccccccccccccccccccccccccccc

$cmd_line

\@c Latex source must use:
\@c  \\usepackage{color}
\@c  \\usepackage{rotating}
\@c  \\usepackage{bytefield}
\@c  \\providecommand{\\setfirstbitindex}[1][0]{\\hspace{#1\\bitwidth}}
";

    foreach my $r ( @$regs ) {

	my $name = ucfirst($main::pack->{'name'}).ucfirst($r->{_name});

	print "
\@c $r->{_name} register layout

\@macro bf$name
  \@raw html
<div class=\"table_frame\"><div class=\"table_caption\">$pack->{'longname'} $r->{_name} register layout</div>
";

	bfgen_html::print_bitfield_map( $r );

	print "
</div>
  \@raw latex
  \\begin{center}
\\makebox[.8\\textwidth]{$pack->{'longname'} \\texttt{".bfgen_latex::_esc($r->{_name})."} register layout}
\\framebox[.8\\textwidth]{
";
	bfgen_latex::print_bitfield_map( $r );
	print "
    }
    \\end{center}
  \@raw mkdoc
    Missing bfgen backend for this documentation format.
  \@end raw
\@end macro

";
    }

####################################################

    my @head;

    foreach my $r ( @$regs ) {

	my $name = ucfirst($main::pack->{'name'}).ucfirst($r->{_name});

	@head = ( "    \@item Name" );
	push @head, " \@item Width";
	push @head, " \@item Direction" if ( $pack->{doc_field_direction_column} );
	push @head, " \@item Long name" if ( $pack->{doc_field_longname_column} );
	push @head, " \@item Description" if ( $pack->{doc_field_doc_column} );

	print "
\@c $r->{_name} register fields table

\@macro bfTable$name
  \@table ".(scalar @head)." {$pack->{'longname'} $r->{_name} register fields}
".(join ' ', @head)."\n";

	my $tbl = "";

	print "\n";

	foreach my $f ( @{$r->{_fields}} ) {
	    next if ($f->{_name} eq '__padding__');

	    $tbl .= "    \@item \@tt $f->{_name} \@item $f->{width} bits";
	    $tbl .= " \@item ".$f->{direction} if ( $pack->{doc_field_direction_column} );
	    $tbl .= " \@item "._esc($f->{longname}) if ( $pack->{doc_field_longname_column} );
	    $tbl .= " \@item "._esc($f->{doc}) if ( $pack->{doc_field_doc_column} );
	    $tbl .= "\n";
	}

	$all_fields_rows .= $tbl;
	print "$tbl";

print "  \@end table
\@end macro

";

####################################################

	print "

\@c $r->{_name} register doc text

\@macro bfDoc$name
"._esc($r->{doc})."
\@end macro
";

    }

####################################################

    {
	my $name = ucfirst($main::pack->{'name'});

	print "
\@c all fields in one table

\@macro bfTable$name
  \@table ".(scalar @head)." {$pack->{longname} fields from all registers}
".(join ' ', @head)."\n";

	my $tbl = "";

	print "    \@item Name \@item Width";
	print " \@item Direction" if ( $pack->{doc_field_direction_column} );
	print " \@item Long name" if ( $pack->{doc_field_longname_column} );
	print " \@item Description"  if ( $pack->{doc_field_doc_column} );
	print "\n";

	print "$all_fields_rows";

	print "  \@end table
\@end macro

";
    }

####################################################

    {
	@head = ( "    \@item Name" );
	push @head, " \@item Address" if ( $pack->{doc_reg_address_column} );
	push @head, " \@item Direction" if ( $pack->{doc_reg_direction_column} );
	push @head, " \@item Long name" if ( $pack->{doc_reg_longname_column} );
	push @head, " \@item Description" if ( $pack->{doc_reg_doc_column} );

	print "
\@c all registers table

\@macro bfRegsTable".ucfirst($main::pack->{'name'})."
  \@table ".(scalar @head)." {$pack->{longname} registers table}
".(join ' ', @head)."\n";

	foreach my $r ( @$regs ) {

	    print "    \@item \@tt $r->{_name}";
	    printf " \@item 0x%0x", $r->{address} if ( $pack->{doc_reg_address_column} );
	    print " \@item ".$r->{direction} if ( $pack->{doc_reg_direction_column} );
	    print " \@item "._esc($r->{longname}) if ( $pack->{doc_reg_longname_column} );
	    print " \@item "._esc($r->{doc})  if ( $pack->{doc_reg_doc_column} );
	    print "\n";

	}

	print "  \@end table
\@end macro

";

####################################################

    }

}

