name: efm32_timer
longname: EFM32 Timer/Counter

bound: 32
align: 32

%ctrl: 32
  longname: Control Register
  direction: rw
  %%mode: 0-1
    doc: These bit set the counting mode for the Timer. Note, when Quadrature Decoder Mode is selected (MODE = 'b11), the CLKSEL is
    doc: don't care. The Timer is clocked by the Decoder Mode clock output.
    0: UP
    1: DOWN
    2: UPDOWN
    3: QDEC
  %%sync: 3-3
    doc: When this bit is set, the Timer is started/stopped/reloaded by start/stop/reload commands in the other timers.
    0: NONE
    1: SYNC
  %%osmen: 4-4
    doc: Thisble/disable one shot mode.
  %%qdm: 5-5
    doc: This bit sets the mode for the quadrature decoder.
    0: X2
    1: X4
  %%debugrun: 6-6
    doc: Set this bit to enable timer to run in debug mode.
    0: FROZEN
    1: DEBUG
  %%dmaclract: 7-7
    doc: When this bit is set, the DMA requests are cleared when the corresponding DMA channel is active. This enables the timer DMA
    doc: requests to be cleared without accessing the timer.
  %%risea: 8-9
    doc: These bits select the action taken in the counter when a rising edge occurs on the input.
    0: NONE
    1: START
    2: STOP
    3: RELOADSTART
  %%falla: 10-11
    doc: These bits select the action taken in the counter when a falling edge occurs on the input.
    0: NONE
    1: START
    2: STOP
    3: RELOADSTART
  %%x2cnt: 13-13
    doc: Enable 2x count mode.
  %%clksel: 16-17
    doc: These bits select the clock source for the timer.
    0: PRESCHFPERCLK
    1: CC1
    2: TIMEROUF
  %%presc: 24-27
    doc: These bits select the prescaling factor.
    0: DIV1
    1: DIV2
    2: DIV4
    3: DIV8
    4: DIV16
    5: DIV32
    6: DIV64
    7: DIV128
    8: DIV256
    9: DIV512
   10: DIV1024
  %%ati: 28-28
    doc: Enable ATI makes CCPOL always track the polarity of the inputs.
  %%rsscoist: 29-29
    doc: When enabled, compare output is set to COIST value at Reload-Start event.
%cmd: 32
  longname: Command Register
  direction: w
  %%start: 0-0
    doc: Write a 1 to this bit to start timer. 
  %%stop: 1-1
    doc: Write a 1 to this bit to stop timer. 

%status: 32
  longname: Status Register
  direction: r
  %%running: 0-0
    doc: Indicates if timer is running or not. 
  %%dir: 1-1
    doc: Indicates count direction.
    0: UP
    1: DOWN
  %%topbv: 2-2
    doc: This indicates that TIMERn_TOPB contains valid data that has not been written to TIMERn_TOP. This bit is also cleared when
    doc: TIMERn_TOP is written. 
  %%ccvbv: 8-8
    count: 3
    doc: This field indicates that the TIMERn_CCn_CCVB registers contain data which have not been written to TIMERn_CCn_CCV. These
    doc: bits are only used in output compare/pwm mode and are cleared when CCMODE is written to 0b00 (Off). 
    0: INVALID
    1: VALID
  %%icv: 16-16
    count: 3
    doc: This bit indicates that TIMERn_CCn_CCV contains a valid capture value. These bits are only used in input capture mode and are
    doc: cleared when CCMODE is written to 0b00 (Off).
    0: INVALID
    1: VALID
  %%ccpol: 24-24
    count: 3
    doc: In Input Capture mode, this bit indicates the polarity of the edge that triggered capture in TIMERn_CCn_CCV. In Compare/PWM
    doc: mode, this bit indicates the polarity of the selected input to CC channel n. These bits are cleared when CCMODE is written to 0b00
    doc: (Off).
    0: INVALID
    1: VALID

%ien: 32
  longname: Interrupt Enable Register
  direction: rw
  %%of: 0-0
    doc: Enable/disable overflow interrupt.
  %%uf: 1-1
    doc: Enable/disable underflow interrupt.
  %%cc: 4-4
    count: 3
    doc: Enable/disable Compare/Capture ch n interrupt.
  %%icbof: 8-8
    count: 3
    doc: Enable/disable Compare/Capture ch n input capture buffer overflow interrupt.

%if: 32
  longname: Interrupt Flag Register
  direction: r
  %%of: 0-0
    doc: This bit indicates that there has been an overflow.
  %%uf: 1-1
    doc: This bit indicates that there has been an underflow.
  %%cc: 4-4
    count: 3
    doc: This bit indicates that there has been an interrupt event on Compare/Capture channel n.
  %%icbof: 8-8
    count: 3
    doc: This bit indicates that a new capture value has pushed an unread value out of the TIMERn_CCn_CCV/TIMERn_CCn_CCVB register
    doc: pair.

%ifs: 32
  longname: Interrupt Flag Set Register
  direction: w
  %%of: 0-0
    doc: Writing a 1 to this bit will set the overflow interrupt flag.
  %%uf: 1-1
    doc: Writing a 1 to this bit will set the underflow interrupt flag.
  %%cc: 4-4
    count: 3
    doc: Writing a 1 to this bit will set Compare/Capture channel n interrupt flag.
  %%icbof: 8-8
    count: 3
    doc: Writing a 1 to this bit will set Compare/Capture channel n input capture buffer overflow interrupt flag.

%ifc: 32
  longname: Interrupt Flag Clear Register
  direction: w
  %%of: 0-0
    doc: Writing a 1 to this bit will clear the overflow interrupt flag.
  %%uf: 1-1
    doc: Writing a 1 to this bit will clear the underflow interrupt flag.
  %%cc: 4-4
    count: 3
    doc: Writing a 1 to this bit will clear Compare/Capture channel n interrupt flag.
  %%icbof: 8-8
    count: 3
    doc: Writing a 1 to this bit will clear Compare/Capture channel n input capture buffer overflow interrupt flag.

%top: 32
  longname: Counter Top Value Register
  direction: rw
  %%topb: 0-15
    doc: These bits hold the TOP value.

%topb: 32
  longname: Counter Top Value Buffer Register
  direction: rw
  %%topb: 0-15
    doc: These bits hold the TOP buffer value.

%cnt: 32
  longname: Counter Value Register
  direction: rw
  %%cnt: 0-15
    doc: These bits hold the counter value.

%route: 32
  longname: I/O Routing Register
  direction: rw
  %%ccpen: 0-0
    count: 3
    doc: Enable/disable CC Channel n output/input connection to pin.
  %%location: 16-18
    doc: Decides the location of the CC pins.
    0: L0C0
    1: L0C1
    2: L0C2
    3: L0C3
    4: L0C4
    5: L0C5

%cc_ctrl: 32
  longname: CC Channel Control Register
  address: 0x30
  direction: rw
  count: 3
  stride: 4
  %%mode: 0-1
    doc: These bits select the mode for Compare/Capture channel.
    0: OFF
    1: INPUTCAPTURE
    2: OUTPUTCOMPARE
    3: PWM
  %%outinv: 2-2
    doc:  Setting this bit inverts the output from the CC channel (Output compare,PWM).
  %%coist: 4-4
    doc:  This bit is only used in Output Compare and PWM mode. When this bit is set in compare mode,the output is set high when the counter
    doc: is disabled. When counting resumes, this value will represent the initial value for the output. If the bit is cleared, the output will be
    doc: cleared when the counter is disabled. In PWM mode, the output will always be low when disabled, regardless of this bit. However,
    doc: this bit will represent the initial value of the output, once it is enabled.
  %%cmoa: 8-9
    doc: Select output action on compare match.
    0: NONE
    1: TOGGLE
    2: CLEAR
    3: UP
  %%cofoa: 10-11
    doc: Select output action on counter overflow.
    0: NONE
    1: TOGGLE
    2: CLEAR
    3: UP
  %%cufoa: 12-13
    doc: Select output action on counter underflow.
    0: NONE
    1: TOGGLE
    2: CLEAR
    3: UP
  %%prssel: 16-17
    doc: Select PRS input channel for Compare/Capture channel.
    0: PRSCH0
    1: PRSCH1
    2: PRSCH2
    3: PRSCH3
  %%insel: 20-20
    doc: Select Compare/Capture channel input.
    0: PIN
    1: PRS
  %%filt: 21-21
    doc: Enable digital filter.
    0: DISBALED
    1: ENABLED
  %%icedge: 24-25
    doc: These bits control which edges the edge detector triggers on. The output is used for input capture and external clock input.
    0: RISING
    1: FALLING
    2: BOTH
    3: NONE
  %%icevctrl: 26-27
    doc: These bits control when a Compare/Capture PRS output pulse, interrupt flag and DMA request is set.
    0: EVERYEDGE
    1: EVERYSECONDEDGE
    2: RISING
    3: FALLING
  %%prsconf: 28-28
    doc: Select PRS pulse or level.
    0: PULSE
    1: LEVEL

%cc_ccv: 32
  longname: CC Channel Value Register
  direction: rw
  count: 3
  stride: 4
  %%ccv: 0-15
    doc: In input capture mode, this field holds the first unread capture value. When reading this register in input capture mode, then contents
    doc: of the TIMERn_CCx_CCVB register will be written to TIMERn_CCx_CCV in the next cycle. In compare mode, this fields holds the
    doc: compare value.

%cc_ccvp: 32
  longname: CC Channel Value Peek Register
  direction: r
  count: 3
  stride: 4
  %%ccvp: 0-15
    doc: This field is used to read the CC value without pulling data through the FIFO in capture mode.

%cc_ccvb: 32
  longname: CC Channel Buffer Register
  direction: rw
  count: 3
  stride: 4
  %%ccvb: 0-15
    doc: In Input Capture mode, this field holds the last capture value if the TIMERn_CCx_CCV register already contains an earlier unread
    doc: capture value. In Output Compare or PWM mode, this field holds the CC buffer value which will be written to TIMERn_CCx_CCV
    doc: on an update event if TIMERn_CCx_CCVB contains valid data.










