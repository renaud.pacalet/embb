name: efm32_aes
longname: EFM32 AES

bound: 32
align: 32

%ctrl: 32
  longname: Control Register
  direction: rw
  %%decrypt: 0-0
    doc: Select encryption or decryption.
    0: ENCRYPTION
    1: DECRYPTION
  %%datastart: 4-4
    doc: Set this bit to start encryption/decryption when DATA3 is written through AES_DATA.
  %%xorstart: 5-5
    doc: Set this bit to start encryption/decryption when DATA3 is written through AES_XORDATA.
  %%byteorder: 6-6
    doc: When set, the byte orders in the data and key registers are swapped before and after encryption/decryption.

%cmd: 32
  longname: Command Register
  direction: w
  %%start: 0-0
    doc: Set to start encryption/decryption.
  %%stop: 1-1
    doc: Set to stop encryption/decryption.

%status: 32
  longname: Status Register
  direction: r
  %%running: 0-0
    doc: This bit indicates that the AES module is running an encryption/decryption.

%ien: 32
  longname: Interrupt Enable Register
  direction: rw
  %%done: 0-0
    doc: Enable/disable interrupt on encryption/decryption done.

%if: 32
  longname: Interrupt Flag Register
  direction: r
  %%done: 0-0
    doc: Set when an encryption/decryption has finished.

%ifs: 32
  longname: Interrupt Flag Set Register
  direction: w
  %%done: 0-0
    doc: Write to 1 to set encryption/decryption done interrupt flag.

%ifc: 32
  longname: Interrupt Flag Clear Register
  direction: w
  %%done: 0-0
    doc: Write to 1 to clear encryption/decryption done interrupt flag.

%data: 32
  longname: DATA Register
  direction: rw
  %%data: 0-31
    doc: Access data through this register.

%xordata: 32
  longname: XOR DATA Register
  direction: rw
  %%xordata: 0-31
    doc: Access data with XOR function through this register.

%keyl: 32
  longname: KEY Low Register
  direction: rw
  count: 4
  %%keyl: 0-31
    doc: Access the low key words through this register.
