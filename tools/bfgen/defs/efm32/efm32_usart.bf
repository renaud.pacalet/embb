name: efm32_usart
longname: EFM32 Universal Synchronous Asynchronous Receiver/Transmitter

bound: 32
align: 32

%ctrl: 32
  longname: Control Register
  direction: rw
  %%sync: 0-0
    doc: Determines whether the USART is operating in asynchronous or synchronous mode.
    0: ASYNC
    1: SYNC
  %%loopbk: 1-1
    doc: Allows the receiver to be connected directly to the USART transmitter for loopback and half duplex communication.
    0: RX
    1: TX
  %%ccen: 2-2
    doc: Enables collision checking on data when operating in half duplex modus.
  %%mpm: 3-3
    doc: Multi-processor mode uses the 9th bit of the USART frames to tell whether the frame is an address frame or a data frame.
    0:NONE
    1:LOADED
  %%mpab: 4-4
    doc: Defines the value of the multi-processor address bit. An incoming frame with its 9th bit equal to the value of this bit marks the frame
    doc: as a multi-processor address frame.
  %%ovs: 5-6
    doc: Sets the number of clock periods in a UART bit-period. More clock cycles gives better robustness, while less clock cycles gives
    doc: better performance.
    0: X16
    1: X8
    2: X6
    3: X4
  %%clkpol: 8-8
    doc: Determines the clock polarity of the bus clock used in synchronous mode.
    0: IDLE_LOW
    1: IDLE_HIGH
  %%clkpha: 9-9
    doc: Determines where data is set-up and sampled according to the bus clock when in synchronous mode.
    0: SAMPLE_LEADING
    1: SAMPLE_TRAILING
  %%msbf: 10-10
    doc: Decides whether data is sent with the least significant bit first, or the most significant bit first.
    0: LSB_FIRST
    1: MSB_FIRST
  %%csma: 11-11
    doc: This register determines the action to be performed when slave-select is configured as an input and driven low while in master mode.
    0: NO_ACTION
    1: SLAVE_MODE
  %%txbil: 12-12
    doc: Determines the interrupt and status level of the transmit buffer.
    0: EMPTY
    1: HALF_FULLD
  %%rxinv: 13-13
    doc: Setting this bit will invert the input to the USART receiver.
    0: NONE
    1: INVERTED
  %%txinv: 14-14
    doc: The output from the USART transmitter can optionally be inverted by setting this bit.
    0: NONE
    1: INVERTED
  %%csinv: 15-15
    doc: Default value is active low. This affects both the selection of external slaves, as well as the selection of the microcontroller as a slave.
    0: ACTIVE_LOW
    1: ACTIVE_HIGH
  %%autocs: 16-16
    doc: When enabled, the output on USn_CS will be activated one baud-period before transmission starts, and deactivated when
    doc: transmission ends.
  %%autori: 17-17
    doc: When enabled, TXTRI is set by hardware whenever the transmitter is idle, and TXTRI is cleared by hardware when transmission starts.
  %%scmode: 18-18
    doc: Use this bit to enable or disable SmartCard mode.
  %%scretrans: 19-19
    doc: When in SmartCard mode, a NACK'ed frame will be kept in the shift register and retransmitted if the transmitter is still enabled.
  %%skipperrf: 20-20
    doc: When set, the receiver discards frames with parity errors (asynchronous mode only). The PERR interrupt flag is still set.
  %%bit8dv: 21-21
    doc: The default value of the 9th bit. If 9-bit frames are used, and an 8-bit write operation is done, leaving the 9th bit unspecified, the
    doc: 9th bit is set to the value of BIT8DV.
  %%errsdma: 22-22
    doc: When set, DMA requests will be cleared on framing and parity errors (asynchronous mode only).
    0: NO_EFFECT
    1: BLOCKED
  %%errsrx: 23-23
    doc: When set, the receiver is disabled on framing and parity errors (asynchronous mode only).
    0: NO_EFFECT
    1: DISABLE_RX
  %%errstx: 24-24
    doc: When set, the transmitter is disabled on framing and parity errors (asynchronous mode only) in the receiver.
    0: NO_EFFECT
    1: DISABLE_TX
  %%ssearly: 25-25
    doc: Setup data on sample edge in synchronous slave mode to improve MOSI setup time.
  %%txdelay: 26-27
    doc: Configurable delay before new transfers. Frames sent back-to-back are not delayed.
    0: NONE
    1: SINGLE
    2: DOUBLE
    3: TRIPLE
  %%byteswap: 28-28
    doc: Set to switch the order of the bytes in double accesses.
    0: NORMAL
    1: SWAPPED
  %%autotx: 29-29
    doc: Transmits as long as RX is not full. If TX is empty, underflows are generated.
  %%mvdis: 30-30
    doc: Disable majority vote for 16x, 8x and 6x oversampling modes.
  %%smsdelay: 31-31
    doc: Delay Synchronous Master sample point to the next setup edge to improve timing and allow communication at higher speeds.

%frame: 32
  longname: Frame Format Register
  direction: rw
  %%databits: 0-3
    doc: This register sets the number of data bits in a USART frame.
    1: FOUR
    2: FIVE
    3: SIX
    4: SEVEN
    5: EIGHT
    6: NINE
    7: TEN
    8: ELEVEN
    9: TWELVE
    10: THIRTEEN
    11: FOURTEEN
    12: FIFTEEN
    13: SIXTEEN
  %%parity: 8-9
    doc: Determines whether parity bits are enabled, and whether even or odd parity should be used. Only available in asynchronous mode.
    0: NONE
    2: EVEN
    3: ODD
  %%stopbits: 12-13
    doc: Determines the number of stop-bits used.
    0: HALF
    1: ONE
    2: ONEADNHALF
    3: TWO

%trigctrl: 32
  longname: Trigger Control Register
  direction: rw
  %%autotxten: 6-6
    doc: When set, AUTOTX is enabled as long as the PRS channel selected by TSEL has a high value.
  %%txten: 5-5
    doc: When set, the PRS channel selected by TSEL sets TXEN, enabling the transmitter on positive trigger edges.
  %%rxten: 4-4
    doc: When set, the PRS channel selected by TSEL sets RXEN, enabling the receiver on positive trigger edges.
  %%tsel: 1-0
    doc: Select USART PRS trigger channel. The PRS signal can enable RX and/or TX, depending on the setting of RXTEN and TXTEN.
    0: PRSCH0
    1: PRSCH1
    2: PRSCH2
    3: PRSCH3

%cmd: 32
  longname: Command Register
  direction: w
  %%rxen: 0-0
    doc: Set to activate data reception on U(S)n_RX
  %%rxdis: 1-1
    doc: Set to disable data reception. If a frame is under reception when the receiver is disabled, the incoming frame is discarded.
  %%txen: 2-2
    doc: Set to enable data transmission.
  %%txdis: 3-3
    doc: Set to disable transmission.
  %%masteren: 4-4
    doc: Set to enable master mode, setting the MASTER status bit. Master mode should not be enabled while TXENS is set to 1. To enable
    doc: both master and TX mode, write MASTEREN before TXEN, or enable them both in the same write operation.
  %%masterdis: 5-5
    doc: Set to disable master mode, clearing the MASTER status bit and putting the USART in slave mode.
  %%rxblockden: 6-6
    doc: Set to set RXBLOCK, resulting in all incoming frames being discarded.
  %%rxblockdis: 7-7
    doc: Set to clear RXBLOCK, resulting in all incoming frames being loaded into the receive buffer.
  %%txtrien: 8-8
    doc: Tristates the transmitter output.
  %%txtridis: 9-9
    doc: Disables tristating of the transmitter output.
  %%cleartx: 10-10
    doc: Set to clear receive buffer and the RX shift register.
  %%clearrx: 11-11
    doc: Set to clear receive buffer and the RX shift register.

%status: 32
  longname: Status Register
  direction: r
  %%rxens: 0-0
    doc: Set when the receiver is enabled.
  %%txens: 1-1
    doc: Set when the transmitter is enabled.
  %%master: 2-2
    doc: Set when the USART operates as a master. Set using the MASTEREN command and clear using the MASTERDIS command.
  %%rxblock: 3-3
    doc: When set, the receiver discards incoming frames. An incoming frame will not be loaded into the receive buffer if this bit is set at the
    doc: instant the frame has been completely received.
  %%txtri:4-4
    doc: Set when the transmitter is tristated, and cleared when transmitter output is enabled. If AUTOTRI in USARTn_CTRL is set this bit
    doc: is always read as 0.
  %%txc: 5-5
    doc: Set when a transmission has completed and no more data is available in the transmit buffer and shift register. Cleared when data
    doc: is written to the transmit buffer.
  %%txbl: 6-6
    doc: Indicates the level of the transmit buffer. If TXBIL is cleared, TXBL is set whenever the transmit buffer is empty, and if TXBIL is set,
    doc: TXBL is set whenever the transmit buffer is half-full or empty.
  %%rxdatav: 7-7
    doc: Set when data is available in the receive buffer. Cleared when the receive buffer is empty.
  %%rxfull: 8-8
    doc: Set when the RXFIFO is full. Cleared when the receive buffer is no longer full. When this bit is set, there is still room for one more
    doc: frame in the receive shift register.
  %%txbdright: 9-9
    doc: When set, the TX buffer expects double right data. Else it may expect a single right data or left data. Only used in I2S mode.
  %%txbsright: 10-10
    doc: When set, the TX buffer expects at least a single right data. Else it expects left data. Only used in I2S mode.
  %%rxdatavright: 11-11
    doc: When set, reading RXDATA or RXDATAX gives right data. Else left data is read. Only used in I2S mode.
  %%rxfullright: 12-12
    doc: When set, the entire RX buffer contains right data. Only used in I2S mode.

%clkdiv: 32
  longname: Clock Control Register
  direction: rw
  %%div: 6-20
    doc: Specifies the fractional clock divider for the USART

%rxdatax: 32
  longname: Rx buffer data Extended Register
  direction: r
  %%rxdata: 0-8
    doc: Use this register to access data read from the USART. Buffer is cleared on read access.
  %%perr: 14-14
    doc: Set if data in buffer has a parity error (asynchronous mode only).
  %%ferr: 15-15
    doc: Set if data in buffer has a framing error. Can be the result of a break condition.

%rxdata: 32
  longname: RX Buffer Data Register
  direction: r
  %%rxdata: 0-7
    doc: Use this register to access data read from USART. Buffer is cleared on read access. Only the 8 LSB can be read using this register.

%rxdoublex: 32
  longname: RX Buffer Double Data Extended Register
  direction: r
  %%rxdata0: 0-8
    doc: First frame read from buffer.
  %%perr0: 14-14
    doc: Set if data in buffer has a parity error (asynchronous mode only).
  %%ferr0: 15-15
    doc: Set if data in buffer has a framing error. Can be the result of a break condition.
  %%rxdata1: 24-16
    doc: Second frame read from buffer.
  %%perr1: 30-30
    doc: Set if data in buffer has a parity error (asynchronous mode only).
  %%ferr1: 31-31
    doc: Set if data in buffer has a framing error. Can be the result of a break condition.

%rxdouble: 32
  longname: RX FIFO Double Data Register
  direction: r
  %%rxdata0: 0-7
    doc: First frame read from buffer.
  %%rxdata1: 8-15
    doc: Second frame read from buffer.

%rxdataxp: 32
  longname: RX Buffer Data Extended Peek Register
  direction: r
  %%rxdatap: 0-8
    doc: Use this register to access data read from the USART.
  %%perr: 14-14
    doc: Set if data in buffer has a parity error (asynchronous mode only).
  %%ferr: 15-15
    doc: Set if data in buffer has a framing error. Can be the result of a break condition.
  %%rxdata1: 24-16
    doc: Second frame read from buffer.
  %%perr1: 30-30
    doc: Set if data in buffer has a parity error (asynchronous mode only).
  %%ferr1: 31-31
    doc: Set if data in buffer has a framing error. Can be the result of a break condition.

%rxdoublexp: 32
  longname: RX Buffer Double Data Extended Peek Register
  direction: r
  %%rxdatap0: 0-8
    doc: First frame read from FIFO.
  %%perrp0: 14-14
    doc: Set if data in buffer has a parity error (asynchronous mode only).
  %%ferrp0: 15-15
    doc: Set if data in buffer has a framing error. Can be the result of a break condition.
  %%rxdata1: 24-16
    doc: Second frame read from FIFO.
  %%perr1p: 30-30
    doc: Set if data in buffer has a parity error (asynchronous mode only).
  %%ferr1p: 31-31
    doc: Set if data in buffer has a framing error. Can be the result of a break condition.

%txdatax: 32
  longname: TX Buffer Data Extended Register
  direction: w
  %%txdatax: 0-8
    doc: Use this register to write data to the USART. If TXEN is set, a transfer will be initiated at the first opportunity.
  %%ubrxat: 11-11
    doc: Set clear RXBLOCK after transmission, unblocking the receiver.
  %%txtriat: 12-12
    doc: Set to tristate transmitter by setting TXTRI after transmission.
  %%txbreak: 13-13
    doc: Set to send data as a break. Recipient will see a framing error or a break condition depending on its configuration and the value
    doc: of WDATA.
  %%txdiasat: 14-14
    doc: Set to disable transmitter and release data bus directly after transmission.
  %%rxenat: 15-15
    doc: Set to enable reception after transmission.

%txdata: 32
  longname: TX Buffer Data Register
  direction: w
  %%txdata: 0-7
    doc: This frame will be added to TX buffer. Only 8 LSB can be written using this register. 9th bit and control bits will be cleared.

%txdoublex: 32
  longname: TX Buffer Double Data Extended Register
  direction: w
  %%txdata0: 0-8
    doc: First frame to write to buffer.
  %%ubrxat0: 11-11
    doc: Set clear RXBLOCK after transmission, unblocking the receiver.
  %%txtriat0: 12-12
    doc: Set clear RXBLOCK after transmission, unblocking the receiver.
  %%txbreak0: 13-13
    doc: Set to send data as a break. Recipient will see a framing error or a break condition depending on its configuration and the value
    doc: of WDATA.
  %%txdiasat0: 14-14
    doc: Set to disable transmitter and release data bus directly after transmission.
  %%rxenat0: 15-15
    doc: Set to enable reception after transmission.
  %%txdata1: 16-24
    doc: Second frame to write to FIFO.
  %%ubrxat1: 27-27
    doc: Set clear RXBLOCK after transmission, unblocking the receiver.
  %%txtriat1: 28-28
    doc: Set clear RXBLOCK after transmission, unblocking the receiver.
  %%txbreak1: 29-29
    doc: Set to send data as a break. Recipient will see a framing error or a break condition depending on its configuration and the value
    doc: of WDATA.
  %%txdiasat1: 30-30
    doc: Set to disable transmitter and release data bus directly after transmission.
  %%rxenat1: 31-31
    doc: Set to enable reception after transmission.

%txdouble: 32
  longname: TX Buffer Double Data Register
  direction: w
  %%txdata0: 0-7
    doc: First frame to write to buffer.
  %%txdata1: 8-15
    doc: Second frame to write to buffer.

%if: 32
  longname: Interrupt Flag Register
  direction: r
  %%txc: 0-0
    doc: TX Complete Interrupt Flag
  %%txbl: 1-1
    doc: Set when buffer becomes empty if TXBIL is set, or when buffer goes from full to half-full if TXBIL is cleared.
  %%rxdatav: 2-2
    doc: Set when data becomes available in the receive buffer.
  %%rxfull: 3-3
    doc: Set when the receive buffer becomes full.
  %%rxof: 4-4
    doc: Set when data is incoming while the receive shift register is full. The data previously in the shift register is lost.
  %%rxuf: 5-5
    doc: Set when trying to read from the receive buffer when it is empty.
  %%txof: 6-6
    doc: Set when a write is done to the transmit buffer while it is full. The data already in the transmit buffer is preserved.
  %%txuf: 7-7
    doc: Set when operating as a synchronous slave, no data is available in the transmit buffer when the master starts transmission of a
    doc: new frame.
  %%perr: 8-8
    doc: Set when a frame with a parity error (asynchronous mode only) is received while RXBLOCK is cleared.
  %%ferr: 9-9
    doc: Set when a frame with a framing error is received while RXBLOCK is cleared.
  %%mpaf: 10-10
    doc: Set when a multi-processor address frame is detected.
  %%ssm: 11-11
    doc: Set when the device is selected as a slave when in master mode.
  %%ccf: 12-12
    doc: Set when a collision check notices an error in the transmitted data.

%ifs: 32
  longname: Interrupt Flag Set Register
  direction: w
  %%txc: 0-0
    doc: Write to 1 to set the TXC interrupt flag.
  %%rxfull: 3-3
    doc: Write to 1 to set the RXFULL interrupt flag.
  %%rxof: 4-4
    doc: Write to 1 to set the RXOF interrupt flag.
  %%rxuf: 5-5
    doc: Write to 1 to set the RXUF interrupt flag.
  %%txof: 6-6
    doc: Write to 1 to set the TXOF interrupt flag.
  %%txuf: 7-7
    doc: Write to 1 to set the TXUF interrupt flag.
  %%perr: 8-8
    doc: Write to 1 to set the PERR interrupt flag.
  %%ferr: 9-9
    doc: Write to 1 to set the FERR interrupt flag.
  %%mpaf: 10-10
    doc: Write to 1 to set the MPAF interrupt flag.
  %%ssm: 11-11
    doc: Write to 1 to set the SSM interrupt flag.
  %%ccf: 12-12
    doc: Write to 1 to set the CCF interrupt flag.

%ifc: 32
  longname: Interrupt Flag Clear Register
  direction: w
  %%txc: 0-0
    doc: Write to 1 to clear the TXC interrupt flag.
  %%rxfull: 3-3
    doc: Write to 1 to clear the RXFULL interrupt flag.
  %%rxof: 4-4
    doc: Write to 1 to clear the RXOF interrupt flag.
  %%rxuf: 5-5
    doc: Write to 1 to clear the RXUF interrupt flag.
  %%xof: 6-6
    doc: Write to 1 to clear the TXOF interrupt flag.
  %%txuf: 7-7
    doc: Write to 1 to clear the TXUF interrupt flag.
  %%perr: 8-8
    doc: Write to 1 to clear the PERR interrupt flag.
  %%ferr: 9-9
    doc: Write to 1 to clear the FERR interrupt flag.
  %%mpaf: 10-10
    doc: Write to 1 to clear the MPAF interrupt flag.
  %%ssm: 11-11
    doc: Write to 1 to clear the SSM interrupt flag.
  %%ccf: 12-12
    doc: Write to 1 to clear the CCF interrupt flag.

%ien: 32
  longname: Interrupt Enable Register
  direction: rw
  %%txc: 0-0
    doc: Enable interrupt on TX complete.
  %%txbl: 1-1
    doc: Enable interrupt on TX buffer level.
  %%rxdatav: 2-2
    doc: Enable interrupt on RX data.
  %%rxfull: 3-3
    doc: Enable interrupt on RX Buffer full.
  %%rxof: 4-4
    doc: Enable interrupt on RX overflow.
  %%rxuf: 5-5
    doc: Enable interrupt on RX underflow.
  %%txof: 6-6
    doc: Enable interrupt on TX overflow.
  %%txuf: 7-7
    doc: Enable interrupt on TX underflow.
  %%perr: 8-8
    doc: Enable interrupt on parity error (asynchronous mode only).
  %%ferr: 9-9
    doc: Enable interrupt on framing error.
  %%mpaf: 10-10
    doc: Enable interrupt on multi-processor address frame.
  %%ssm: 11-11
    doc: Enable interrupt on slave-select in master mode.
  %%ccf: 12-12
    doc: Enable interrupt on collision check error detected.

%irctrl: 32
  longname: IrDA Control Register
  direction: rw
  %%iren: 0-0
    doc: Enable IrDA module and rout USART signals through it.
  %%irpw: 1-2
    doc: Configure the pulse width generated by the IrDA modulator as a fraction of the configured USART bit period.
    0: ONE
    1: TWO
    2: THREE
    3: FOUR
  %%irfilt: 3-3
    doc: Set to enable filter on IrDA demodulator.
  %%irprssel: 4-5
    doc: A PRS can be used as input to the pulse modulator instead of TX. This value selects the channel to use.
    0: PRSCH0 
    1: PRSCH1 
    2: PRSCH2 
    3: PRSCH3 
  %%irprsen: 7-7
    doc: Enable the PRS channel selected by IRPRSSEL as input to IrDA module instead of TX.

%route: 32
  longname: I/O Routing Register
  direction: rw
  %%rxpen: 0-0
    doc: When set, the RX/MISO pin of the USART is enabled.
  %%txpen: 1-1
    doc: When set, the TX/MOSI pin of the USART is enabled.
  %%cspen: 2-2
    doc: When set, the CS pin of the USART is enabled.
  %%clkpen: 3-3
    doc: When set, the Clk pin of the USART is enabled.
  %%location: 8-10
    doc: Decides the location of the USART I/O pins.
    0:LOC0
    1:LOC1
    2:LOC2
    3:LOC3

%input: 32
  longname: USART Input Register
  direction: rw
  %%rxprssel: 0-1
    doc: Select PRS channel as input to RX.
    0: PRSCH0 
    1: PRSCH1 
    2: PRSCH2 
    3: PRSCH3 
  %%rxprs: 4-4
    doc: When set, the PRS channel selected as input to RX.

%i2sctrl: 32
  longname: I2S Control Register
  direction: rw
  %%en: 0-0
    doc: Set the U(S)ART in I2S mode.
  %%mono: 1-1
    doc: Switch between stereo and mono mode. Set for mono
  %%justify: 2-2
    doc: Determines whether the I2S data is left or right justified
    0: LEFT
    1: RIGHT
  %%dmasplit: 3-3
    doc: When set DMA requests for right-channel data are put on the TXBLRIGHT and RXDATAVRIGHT DMA requests.
  %%delay: 4-4
    doc: Set to add a one-cycle delay between a transition on the word-clock and the start of the I2S word. Should be set for standard I2S format
