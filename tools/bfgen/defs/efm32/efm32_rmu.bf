name: efm32_rmu
longname: EFM32 Reset Management Unit

bound: 32
align: 32

%ctrl: 32
  longname: Control Register
  direction: rw
  %%lockuprdis: 0-0
    doc: Set this bit to disable the LOCKUP signal (from the Cortex) from resetting the device.

%rstcause: 32
  longname: Reset Cause Register
  direction: r
  %%ports: 0-0
    doc: Set if a power on reset has been performed. Must be cleared by software.
  %%bodunregrst: 1-1
    doc: Set if a unregulated domain brown out detector reset has been performed. Must be cleared by software.
  %%bodregrst: 2-2
    doc: Set if a regulated domain brown out detector reset has been performed. Must be cleared by software.
  %%extrst: 3-3
    doc: Set if an external pin reset has been performed. Must be cleared by software.
  %%wdogrst: 4-4
    doc: Set if a watchdog reset has been performed. Must be cleared by software.
  %%lockuprst: 5-5
    doc: Set if a LOCKUP reset has been requested. Must be cleared by software.
  %%sysreqrst: 6-6
    doc: Set if a system request reset has been performed. Must be cleared by software.
  %%em4rst: 7-7
    doc: Set if the system has been in EM4. Must be cleared by software.
  %%em4wurst: 8-8
    doc: Set if the system has been woken up from EM4 from a reset request from pin. Must be cleared by software.
  %%bodavdd0: 9-9
    doc: Set if analog power domain 0 brown out detector reset has been performed. Must be cleared by software.
  %%bodavdd1: 10-10
    doc: Set if analog power domain 1 brown out detector reset has been performed. Must be cleared by software.

%cmd: 32
  longname: Command Register
  direction: w
  %%rcclr: 0-0
    doc: Set this bit to clear the LOCKUPRST and SYSREQRST bits in the RMU_RSTCAUSE register. Use the HRCCLR bit in the
    doc: EMU_AUXCTRL register to clear the remaining bits.

