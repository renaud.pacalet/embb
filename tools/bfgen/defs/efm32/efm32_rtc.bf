name: efm32_rtc
longname: EFM32 Real Time Counter

bound: 32
align: 32

%ctrl: 32
  longname: Control Register
  direction: rw
  %%en: 0-0
    doc: When this bit is set, the RTC is enabled and counts up. When cleared, the counter register CNT is reset.
    0: RESET
    1: COUNT
  %%debugrun: 1-1
    doc: Set this bit to enable the RTC to keep running in debug
    0: FROZEN
    1: RUNNING
  %%comp0top: 2-2
    doc: When set, the counter is cleared in the clock cycle after a compare match with compare channel 0
    0: TOPMAX
    1: TOPCOMP0

%cnt: 32
  longname: Counter Value Register
  direction: rw
  %%cnt: 0-23
    doc: Gives access to the counter value of the RTC.

%comp0: 32
  longname: Compare Value Register 0
  direction: rw
  %%val: 0-23
    doc: A compare match event occurs when CNT is equal to this value. This event sets the COMP0 interrupt flag, and can be used to start
    doc: the LETIMER. It is also available as a PRS signal.

%comp1: 32
  longname: Compare Value Register 1
  direction: rw
  %%val: 0-23
    doc: A compare match event occurs when CNT is equal to this value. This event sets the COMP1 interrupt flag, and can be used to start
    doc: the LETIMER. It is also available as a PRS signal.


%if: 32
  longname: Interrupt Flag Register
  direction: r
  %%of: 0-0
    doc: Set on a CNT value overflow.
  %%comp0: 1-1
    doc: Set on a compare match between CNT and COMP0.
  %%comp1: 2-2
    doc: Set on a compare match between CNT and COMP1.

%ifs: 32
  longname: Interrupt Flag Set Register
  direction: r
  %%of: 0-0
    doc: Write to 1 to set the OF interrupt flag.
  %%comp0: 1-1
    doc: Write to 1 to set the COMP0 interrupt flag.
  %%comp1: 2-2
    doc: Write to 1 to set the COMP1 interrupt flag.

%ifc: 32
  longname: Interrupt Flag Clear Register
  direction: r
  %%of: 0-0
    doc: Write to 1 to clear the OF interrupt flag.
  %%comp0: 1-1
    doc: Write to 1 to clear the COMP0 interrupt flag.
  %%comp1: 2-2
    doc: Write to 1 to clear the COMP1 interrupt flag.

%ien: 32
  longname: Interrupt Enable Register
  direction: r
  %%of: 0-0
    doc: Enable interrupt on overflow.
  %%comp0: 1-1
    doc: Enable interrupt on compare match 0.
  %%comp1: 2-2
    doc: Enable interrupt on compare match 1.

%freeze: 32
  longname: Freeze Register
  direction: rw
  %%regfreeze: 0-0
    doc: When set, the update of the RTC is postponed until this bit is cleared. Use this bit to update several registers simultaneously.
    0: UPDATE
    1: FREEZE

%syncbusy: 32
  longname: Synchronization Busy Register
  direction: r
  %%ctrl: 0-0 
    doc: Set when the value written to CTRL is being synchronized.
  %%comp0: 1-1 
    doc: Set when the value written to COMP0 is being synchronized.
  %%comp1: 2-2 
    doc: Set when the value written to COMP1 is being synchronized.









