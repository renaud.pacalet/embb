name: efm32_emu
longname: EFM32 Energy Management Unit

bound: 32
align: 32

%ctrl: 32
  longname: Control Register
  direction: rw
  %%emvreg: 0-0
    doc: Control the voltage regulator in low energy modes 2 and 3.
    0: REDUCED
    1: FULL
  %%em2block: 1-1
    doc: This bit is used to prevent the MCU to enter Energy Mode 2 or lower.
  %%em4ctrl: 2-3
    doc: This register is used to enter Energy Mode 4, in which the device only wakes up from an external pin reset, from a power cycle,
    doc: Backup RTC interrupt, or EM4 wakeup reset request. Energy Mode 4 is entered when the EM4 sequence is written to this bitfield.

%lock: 32
  longname: Configuration Lock Register
  direction: rw
  %%key: 0-15
    doc: Write any other value than the unlock code to lock all EMU registers, except the interrupt registers, from editing. Write the unlock
    doc: code to unlock. When reading the register, bit 0 is set when the lock is enabled.
    0: LOCK
    0xADE8: UNLOCK

%auxctrl: 32
  longname: Auxiliary Control Register
  direction: rw
  %%hrcclr: 0-0
    doc: Write to 1 and then 0 to clear the POR, BOD and WDOG reset cause register bits. See also the Reset Management Unit (RMU).

