name: efm32_usb
longname: EFM32 USB controller

bound: 32
align: 32

%ctrl: 32
  longname: System Control Register
  direction: rw
  address: 0
  %%vbusenap: 0-0
    doc: Use this bit to select the active polarity of the USB_VBUSEN pin.
    0: LOW
    1: HIGH
  %%dmpuap: 1-1
    doc: Use this bit to select the active polarity of the USB_DMPU pin.
    0: LOW
    1: HIGH
  %%vregdis:16-16
    doc: Set this bit to disable the voltage regulator.
  %%vregosen:17-17
    doc: Set this bit to enable USB_VREGO voltage level sensing.
  %%biasprogem01:20-21 
    doc: Regulator bias current setting in EM0/1 (i.e. while USB active).
  %%biasprogem23:24-25 
    doc: Regulator bias current setting in EM2/3 (i.e. while USB in suspend).

%status: 32
  longname: System Status Register
  direction: rw
  %%vregos:0-0
    doc: USB_VREGO Voltage Sense output. 0 when no USB_VREGO voltage, 1 when USB_VREGO above approximately 1.8 V. Always
    doc: 0 when VREGOSEN in USB_CTRL is 0.

%if: 32
  longname:  interrupt flag register
  direction: rw
  %%vregosh:0-0
    doc: set when usb_vrego goes above approximately 1.8 v.
  %%vregosl:1-1
    doc: set when usb_vrego drops below approximately 1.8 v.

%ifs: 32
  longname:  interrupt flag set register
  direction: rw
  %%vregosh:0-0
    doc: Write to 1 to set the VREGO Sense High Interrupt Flag.
  %%vregosl:1-1
    doc: Write to 1 to set the VREGO Sense Low Interrupt Flag.

%ifc: 32
  longname:  interrupt flag clear register
  direction: rw
  %%vregosh:0-0
    doc: Write to 1 to clear the VREGO Sense High Interrupt Flag.
  %%vregosl:1-1
    doc: Write to 1 to clear the VREGO Sense Low Interrupt Flag.

%ien: 32
  longname:  interrupt enable register
  direction: rw
  %%vregosh:0-0
    doc: Enable interrupt on VREGO Sense High.
  %%vregosl:1-1
    doc: Enable interrupt on VREGO Sense Low.

%route: 32
  longname: I/O Routing Register
  direction: rw
  %%phypen: 0-0
    doc: When set, the USB PHY and USB pins are enabled. The USB_DP and USB_DM are changed from regular GPIO pins to USB pins.
  %%vbusenpen: 1-1
    doc: When set, the USB_VBUSEN pin is enabled.
  %%dmpupen: 2-2
    doc: When set, the USB_DMPU pin is enabled.

%gotgctl: 32
  longname: OTG Control and Status Register
  address: 0x3c000
  direction: rw
  %%sesreqscs:0-0
    doc: The core sets this bit when a session request initiation is successful.
  %%sesreq: 1-1
    doc: The application sets this bit to initiate a session request on the USB. The application can clear this bit by writing a 0 when the Host
    doc: Negotiation Success Status Change bit in the OTG Interrupt register (USB_GOTGINT.HSTNEGSUCSTSCHNG) is set. The core
    doc: clears this bit when the HSTNEGSUCSTSCHNG bit is cleared. The application must wait until the VBUS discharges to 0.2 V, after the
    doc: B-Session Valid bit in this register (USB_GOTGCTL.BSESVLD) is cleared. This discharge time can be obtained from the datasheet.
  %%vbvalidoven:2-2
    doc: This bit is used to enable/disable the software to override the vbusvalid signal using the USB_GOTGCTL.VBVALIDOVVAL. When
    doc: set, vbusvalid received from the PHY is overridden with USB_GOTGCTL.VBVALIDOVVAL.
  %%vbvalidoval:3-3
    doc: This bit is used to set Override value for vbusvalid signal when USB_GOTGCTL.VBVALIDOVEN is set.
  %%bvalidoven:4-4
    doc: This bit is used to enable/disable the software to override the Bvalid signal using the USB_GOTGCTL.BVALIDOVVAL. When set
    doc:Bvalid received from the PHY is overridden with USB_GOTGCTL.BVALIDOVVAL.
  %%bvalidoval:5-5
    doc: This bit is used to set Override value for Bvalid signal when USB_GOTGCTL.BVALIDOVEN is set.
  %%avalidoven:6-6
    doc: This bit is used to enable/disable the software to override the Avalid signal using the USB_GOTGCTL.AVALIDOVVAL. When set
    doc: Avalid received from the PHY is overridden with USB_GOTGCTL.AVALIDOVVAL.
  %%avalidoval:7-7
    doc: This bit is used to set Override value for Avalid signal when USB_GOTGCTL.AVALIDOVEN is set.
  %%hstnegscs:8-8
    doc: The core sets this bit when host negotiation is successful. The core clears this bit when the HNP Request (HNPREQ) bit in this
    doc: register is set.
  %%hnpreq:9-9
    doc: The application sets this bit to initiate an HNP request to the connected USB host. The application can clear this bit by writing a 0
    doc: when the Host Negotiation Success Status Change bit in the OTG Interrupt register (USB_GOTGINT.HSTNEGSUCSTSCHNG) is
    doc: set. The core clears this bit when the HSTNEGSUCSTSCHNG bit is cleared.
  %%hstsethnpen:10-10
    doc: The application sets this bit when it has successfully enabled HNP (using the SetFeature.SetHNPEnable command) on the connected
    doc: device.
  %%devhnpen:11-11 
    doc: The application sets this bit when it successfully receives a SetFeature.SetHNPEnable command from the connected USB host.
  %%conidsts:16-16 
    doc: Indicates the connector ID status on a connect event.
    0: A_MODE
    1: B_MODE
  %%dbnctime:17-17 
    doc: Indicates the debounce time of a detected connection.
    0: LONG
    1: SHORT
  %%asesvld:18-18 
    doc: Indicates the Host mode transceiver status for A-session valid.
  %%bsesvld:19-19 
    doc: Indicates the Device mode transceiver status for B-session valid. In OTG mode, you can use this bit to determine if the device is
    doc: connected or disconnected.
  %%otgver:20-20 
    doc: Indicates the OTG revision.
    0: OTG13 
    1: OTG20 

%gotgint: 32
  longname: OTG Interrupt Register
  direction: rw
  %%sesenddet:2-2 
    doc: The core sets this bit when VBUS is in the range 0.8V - 2.0V. This bit can be set only by the core and the application should write
    doc: 1 to clear it.
  %%sesreqsucstschng:8-8
    doc: The core sets this bit on the success or failure of a session request. The application must read the Session Request Success bit
    doc: in the OTG Control and Status register (USB_GOTGCTL.SESREQSCS) to check for success or failure. This bit can be set only by
    doc: the core and the application should write 1 to clear it.
  %%hstnegsucstschng:9-9 
    doc: The core sets this bit on the success or failure of a USB host negotiation request. The application must read the Host Negotiation
    doc: Success bit of the OTG Control and Status register (USB_GOTGCTL.HSTNEGSCS) to check for success or failure. This bit can be
    doc: set only by the core and the application should write 1 to clear it.
  %%hstnegdet:17-17
    doc: The core sets this bit when it detects a host negotiation request on the USB. This bit can be set only by the core and the application
    doc: should write 1 to clear it.
  %%adevtoutchg:18-18
    doc: the core sets this bit to indicate that the a-device has timed out while waiting for the b-device to connect. this bit can be set only
    doc: by the core and the application should write 1 to clear it.
  %%dbncedone:19-19
    doc: The core sets this bit when the debounce is completed after the device connect. The application can start driving USB reset after
    doc: seeing this interrupt. This bit is only valid when the HNP Capable or SRP Capable bit is set in the Core USB Configuration register
    doc: (USB_GUSBCFG.HNPCAP or USB_GUSBCFG.SRPCAP, respectively). This bit can be set only by the core and the application
    doc: should write 1 to clear it.

%gahbcfg: 32
  longname: AHB Configuration Register
  direction: rw
  %%glblintrmsk:0-0
    doc: The application uses this bit to mask or unmask the interrupt line assertion to itself. Irrespective of this bit's setting, the interrupt status
    doc: registers are updated by the core. Set to unmask.
  %%hbstlen:1-4
    doc: This field is used in DMA mode.
    0: SINGLE
    1: INCR
    3: INCR4
    5: INCR8
    7: INCR16
  %%dmaen:5-5 
    doc: When set to 0 the core operates in Slave mode. When set to 1 the core operates in a DMA mode.
  %%nptxfemplvl:7-7
    doc: This bit is used only in Slave mode. In host mode this bit indicates when the Non-Periodic TxFIFO Empty Interrupt bit in the Core
    doc: Interrupt register (USB_GINTSTS.NPTXFEMP) is triggered. In device mode, this bit indicates when IN endpoint Transmit FIFO empty
    doc: interrupt (USB_DIEP0INT/USB_DIEPx_INT.TXFEMP) is triggered.
    0: HALFEMPTY
    1: EMPTY
  %%ptxfemplvl:8-8
    doc: Indicates when the Periodic TxFIFO Empty Interrupt bit in the Core Interrupt register (USB_GINTSTS.PTXFEMP) is triggered. This
    doc:  bit is used only in Slave mode.
    0: HALFEMPTY
    1: EMPTY
  %%remmemsupp:21-21
    doc: This bit is programmed to enable the functionality to wait for the system DMA Done Signal for the DMA Write Transfers. When set, the
    doc: int_dma_req output signal is asserted when HSOTG DMA starts write transfer to the external memory. When the core is done with the
    doc: Transfers it asserts int_dma_done signal to flag the completion of DMA writes from HSOTG. The core then waits for sys_dma_done
    doc: signal from the system to proceed further and complete the Data Transfer corresponding to a particular Channel/Endpoint. When
    doc: cleared, the int_dma_req and int_dma_done signals are not asserted and the core proceeds with the assertion of the XferComp
    doc: interrupt as soon as the DMA write transfer is done at the HSOTG Core Boundary and it doesn't wait for the sys_dma_done signal
    doc: to complete the DATA.
  %%notialldmawrit:22-22
    doc: This bit is programmed to enable the System DMA Done functionality for all the DMA write Transactions corresponding to the Channel/
    doc: Endpoint. This bit is valid only when USB_GAHBCFG.REMMEMSUPP is set to 1. When set, the core asserts int_dma_req for all the
    doc: DMA write transactions on the AHB interface along with int_dma_done, chep_last_transact and chep_number signal informations.
    doc: The core waits for sys_dma_done signal for all the DMA write transactions in order to complete the transfer of a particular Channel/
    doc: Endpoint. When cleared, the core asserts int_dma_req signal only for the last transaction of DMA write transfer corresponding to a
    doc: particular Channel/Endpoint. Similarly, the core waits for sys_dma_done signal only for that transaction of DMA write to complete
    doc: the transfer of a particular Channel/Endpoint.

%gusbcfg: 32
  longname: USB Configuration Register
  direction: rw
  %%toutcal:0-2
    doc: Always write this field to 0.
  %%fsintf:5-5
    doc: Always write this field to 0.
  %%srpcap:8-8
    doc: The application uses this bit to control the core's SRP capabilities. If the core operates as a non-SRP-capable B-device, it cannot
    doc: request the connected A-device (host) to activate VBUS and start a session. Set to enable SRP capability.
  %%hnpcap:9-9
    doc: The application uses this bit to control the core's HNP capabilities. Set to enable HNP capability.
  %%usbtrdtim:10-13 
    doc: Sets the turnaround time in PHY clocks. Specifies the response time For a MAC request to the Packet FIFO Controller (PFC) to fetch
    doc: data from the DFIFO (SPRAM). Always write this field to 5.
  %%termseldlpulse:22-22 
    doc: This bit selects utmi_termselect to drive data line pulse during SRP.
    0: TXVALID
    1: TERMSEL
  %%txendelay:28-28 
    doc: Writing 1 to this bit enables the core to follow the TxEndDelay timings as per UTMI+ specification 1.05 section 4.1.5 for opmode
    doc: signal during remote wakeup.
  %%forcehstmode:29-29
    doc: Writing a 1 to this bit forces the core to host mode irrespective of the state of the ID pin. After setting the force bit, the application
    doc: must wait at least 65 ms before the change to take effect.
  %%forcedevmode:30-30
    doc: Writing a 1 to this bit forces the core to device mode irrespective of the state of the ID pin. After setting the force bit, the application
    doc: must wait at least 25 ms before the change to take effect.
  %%corrupttxpkt:31-31
    doc: This bit is for debug purposes only. Never Set this bit to 1. The application should always write 0 to this bit.

%grstctl:32
  longname: Reset Register
  direction: rw
  %%csftrst:0-0
    doc: Resets the core by clearing the interrupts and all the CSR registers except the following register bits:
    doc: USB_PCGCCTL.RSTPDWNMODULE, USB_PCGCCTL.GATEHCLK, USB_PCGCCTL.PWRCLMP, USB_GUSBCFG.FSINTF,
    doc: USB_HCFG.FSLSPCLKSEL, USB_DCFG.DEVSPD.
    doc: All module state machines (except the AHB Slave Unit) are reset to the IDLE state, and all the transmit FIFOs and the receive FIFO
    doc: are flushed. Any transactions on the AHB Master are terminated as soon as possible, after gracefully completing the last data phase
    doc: of an AHB transfer. Any transactions on the USB are terminated immediately. The application can write to this bit any time it wants
    doc: to reset the core. This is a self-clearing bit and the core clears this bit after all the necessary logic is reset in the core, which can
    doc: take several clocks, depending on the current state of the core. Once this bit is cleared software must wait at least 3 clock cycles
    doc: before doing any access to the core. Software must also must check that bit 31 of this register is 1 (AHB Master is IDLE) before
    doc: starting any operation.
  %%frmcntrrst:2-2 
    doc: The application writes this bit to reset the frame number counter inside the core. When the frame counter is reset, the subsequent
    doc: SOF sent out by the core has a frame number of 0. When application writes 1 to the bit, it might not be able to read back the value
    doc: as it will get cleared by the core in a few clock cycles.
  %%rxfflsh:4-4
    doc: The application can flush the entire RxFIFO using this bit, but must first ensure that the core is not in the middle of a transaction. The
    doc: application must only write to this bit after checking that the core is neither reading from the RxFIFO nor writing to the RxFIFO. The
    doc: application must wait until the bit is cleared before performing any other operations. This bit requires 8 clocks to clear.
  %%txfflsh:5-5
    doc: This bit selectively flushes a single or all transmit FIFOs, but cannot do so if the core is in the midle of a transaction. The application
    doc: must write this bit only after checking that the core is neither writing to the TxFIFO nor reading from the TxFIFO. NAK Effective
    doc: Interrupt ensures the core is not reading from the FIFO. USB_GRSTCTL.AHBIDLE ensures the core is not writing anything to the
    doc: FIFO. Flushing is normally recommended when FIFOs are reconfigured. FIFO flushing is also recommended during device endpoint
    doc: disable. The application must wait until the core clears this bit before performing any operations. This bit takes eight clocks to clear
  %%txfnum:6-10
    doc: This is the FIFO number that must be flushed using the TxFIFO Flush bit. This field must not be changed until the core clears the
    doc: TxFIFO Flush bit.
    0: FIFO0
    1: FIFO1
    2: FIFO2
    3: FIFO3
    4: FIFO4
    5: FIFO5
    6: FIFO6
    16: ALLFIF0
  %%dmareq:30-30
    doc: Indicates that the DMA request is in progress. Used for debug
  %%ahbidle:31-31
    doc: Indicates that the AHB Master State Machine is in the IDLE condition.

%gintsts:32
  longname: Interrupt Register
  direction: rw
  address: 0x3C014
  %%curmode:0-0
    doc: Indicates the current mode.
  %%modemis:1-1
    doc: The core sets this bit when the application is trying to access a Host mode register, when the core is operating in Device mode or when
    doc: the application accesses a Device mode register, when the core is operating in Host mode. The register access is ignored by the core
    doc: internally and does not affect the operation of the core. This bit can be set only by the core and the application should write 1 to clear it.
  %%otgint: 2-2
    doc: The core sets this bit to indicate an OTG protocol event. The application must read the OTG Interrupt Status (USB_GOTGINT) register
    doc: to determine the exact event that caused this interrupt. The application must clear the appropriate status bit in the USB_GOTGINT
    doc: register to clear this bit.
  %%sof: 3-3
    doc: In Host mode, the core sets this bit to indicate that an SOF (FS) or Keep-Alive (LS) is transmitted on the USB. The application must
    doc: write a 1 to this bit to clear the interrupt.
    doc: In Device mode, in the core sets this bit to indicate that an SOF token has been received on the USB. The application can read the
    doc: Device Status register to get the current frame number. This interrupt is seen only when the core is operating at full-speed (FS). This
    doc: bit can be set only by the core and the application should write 1 to clear it.
  %%rxflvl:4-4
    doc: Indicates that there is at least one packet pending to be read from the RxFIFO.
  %%nptxfemp:5-5
    doc: This interrupt is asserted when the Non-periodic TxFIFO is either half or completely empty, and there is space for at least one entry
    doc: to be written to the Non-periodic Transmit Request Queue. The half or completely empty status is determined by the Non-periodic
    doc: TxFIFO Empty Level bit in the Core AHB Configuration register (USB_GAHBCFG.NPTXFEMPLVL).
  %%ginnakeff:6-6
    doc: Indicates that the Set Global Non-periodic IN NAK bit in the Device Control register (USB_DCTL.SGNPINNAK), set by the application,
    doc: has taken effect in the core. That is, the core has sampled the Global IN NAK bit set by the application. This bit can be cleared by
    doc: clearing the Clear Global Non-periodic IN NAK bit in the Device Control register (USB_DCTL.CGNPINNAK). This interrupt does not
    doc: necessarily mean that a NAK handshake is sent out on the USB. The STALL bit takes precedence over the NAK bit.
  %%goutnakeff:7-7
    doc: Indicates that the Set Global OUT NAK bit in the Device Control register (USB_DCTL.SGOUTNAK), set by the application,
    doc: has taken effect in the core. This bit can be cleared by writing the Clear Global OUT NAK bit in the Device Control register
    doc: (USB_DCTL.CGOUTNAK).
  %%erlysusp:10-10
    doc: The core sets this bit to indicate that an Idle state has been detected on the USB for 3 ms.
  %%usbsusp:11-11
    doc: The core sets this bit to indicate that a suspend was detected on the USB. The core enters the Suspended state when there is no
    doc: activity on the bus for an extended period of time.
  %%usbrst:12-12
    doc: The core sets this bit to indicate that a reset is detected on the USB.
  %%enumdone:13-13
    doc: The core sets this bit to indicate that speed enumeration is complete. The application must read the Device Status (USB_DSTS)
    doc: register to obtain the enumerated speed.
  %%isooutdrop:14-14
    doc: The core sets this bit when it fails to write an isochronous OUT packet into the RxFIFO because the RxFIFO does not have enough
    doc: space to accommodate a maximum packet size packet for the isochronous OUT endpoint.
  %%iepint:18-18
    doc: The core sets this bit to indicate that an interrupt is pending on one of the IN endpoints of the core (in Device mode). The application
    doc: must read the Device All Endpoints Interrupt (USB_DAINT) register to determine the exact number of the IN endpoint on Device IN
    doc: Endpoint-x Interrupt (USB_DIEP0INT/USB_DIEPx_INT) register to determine the exact cause of the interrupt. The application must
    doc: clear the appropriate status bit in the corresponding USB_DIEP0INT/USB_DIEPx_INT register to clear this bit.
  %%oepint:19-19
    doc: The core sets this bit to indicate that an interrupt is pending on one of the OUT endpoints of the core (in Device mode). The application
    doc: must read the Device All Endpoints Interrupt (USB_DAINT) register to determine the exact number of the OUT endpoint on which
    doc: the interrupt occurred, and then read the corresponding Device OUT Endpoint-x Interrupt (USB_DOEP0INT/USB_DOEPx_INT)
    doc: register to determine the exact cause of the interrupt. The application must clear the appropriate status bit in the corresponding
    doc: USB_DOEP0INT/USB_DOEPx_INT register to clear this bit.
  %%incompisoin:20-20
    doc: The core sets this interrupt to indicate that there is at least one isochronous IN endpoint on which the transfer is not completed in
    doc: the current frame.
  %%incomplp:21-21
    doc: In Host mode, the core sets this interrupt bit when there are incomplete periodic transactions still pending which are scheduled for the
    doc: current frame. In Device mode, the core sets this interrupt to indicate that there is at least one isochronous OUT endpoint on which
    doc: the transfer is not completed in the current frame. This bit can be set only by the core and the application should write 1 to clear it.
  %%fetsusp:22-22
    doc: This interrupt is valid only in DMA mode. This interrupt indicates that the core has stopped fetching data for IN endpoints due to the
    doc: unavailability of TxFIFO space or Request Queue space. This interrupt is used by the application for an endpoint mismatch algorithm.
    doc: For example, after detecting an endpoint mismatch, the application: Sets a Global non-periodic IN NAK handshake, Disables In
    doc: endpoints, Flushes the FIFO, Determines the token sequence from the IN Token Sequence, Re-enables the endpoints, Clears the
    doc: Global non-periodic IN NAK handshake.
    doc: If the Global non-periodic IN NAK is cleared, the core has not yet fetched data for the IN endpoint, and the IN token is received: the
    doc: core generates an IN Token Received when FIFO Empty interrupt. The OTG then sends the host a NAK response. To avoid this
    doc: scenario, the application can check the USB_GINTSTS.FETSUSP interrupt, which ensures that the FIFO is full before clearing a
    doc: Global NAK handshake. Alternatively, the application can mask the IN Token Received when FIFO Empty interrupt when clearing
    doc: a Global IN NAK handshake.
  %%resetdet:23-23
    doc: This interrupt is valid only in DMA mode. This interrupt indicates that the core has stopped fetching data for IN endpoints due to the
    doc: unavailability of TxFIFO space or Request Queue space. This interrupt is used by the application for an endpoint mismatch algorithm.
  %%prtint:24-24
    doc: The core sets this bit to indicate a change in port status in Host mode. The application must read the Host Port Control and Status
    doc: (USB_HPRT) register to determine the exact event that caused this interrupt. The application must clear the appropriate status bit
    doc: in the Host Port Control and Status register to clear this bit.
  %%hchint:25-25
    doc: The core sets this bit to indicate that an interrupt is pending on one of the channels of the core (in Host mode). The application must
    doc: read the Host All Channels Interrupt (USB_HAINT) register to determine the exact number of the channel on which the interrupt
    doc: occurred, and then read the corresponding Host Channel-x Interrupt (USB_HCx_INT) register to determine th
  %%ptxfemp:26-26 
    doc: This interrupt is asserted when the Periodic Transmit FIFO is either half or completely empty and there is space for at least one entry
    doc: to be written in the Periodic Request Queue. The half or completely empty status is determined by the Periodic TxFIFO Empty Level
    doc: bit in the Core AHB Configuration register (USB_GAHBCFG.PTXFEMPLVL).
  %%conidstschng:28-28
    doc: The core sets this bit when there is a change in connector ID status. This bit can be set only by the core and the application should
    doc: write 1 to clear it.
  %%disconnint:29-29 
    doc: Asserted when a device disconnect is detected. This bit can be set only by the core and the application should write 1 to clear it.
  %%sessreqint:30-30
    doc: In Host mode, this interrupt is asserted when a session request is detected from the device. In Device mode, this interrupt is asserted
    doc: when the VBUS voltage reaches the session-valid level. This bit can be set only by the core and the application should write 1 to clear.
  %%wkupint:31-31 
    doc: Wakeup Interrupt during Suspend state. In Device mode this interrupt is asserted only when Host Initiated Resume is detected on
    doc: USB. In Host mode this interrupt is asserted only when Device Initiated Remote Wakeup is detected on USB. This bit can be set
    doc: only by the core and the application should write 1 to clear.

%gintmsk: 32
  longname: Interrupt Mask Register
  direction: rw
  %%modemismsk:1-1
    doc: Set to 1 to unmask MODEMISINT interrupt.
  %%otgintmsk: 2-2
    doc: Set to 1 to unmask OTGINTINT interrupt.
  %%sofmsk: 3-3
    doc: Set to 1 to unmask SOFINT interrupt.
  %%rxflvlmsk:4-4
    doc: Set to 1 to unmask RXFLVLINT interrupt.
  %%nptxfempmsk:5-5
    doc: Set to 1 to unmask NPTXFEMPINT interrupt.
  %%ginnakeffmsk:6-6
    doc: Set to 1 to unmask GINNAKEFFINT interrupt.
  %%goutnakeffmsk:7-7
    doc: Set to 1 to unmask GOUTNAKEFFMS interrupt.
  %%erlysuspmsk:10-10
    doc: Set to 1 to unmask ERLYSUSPINT interrupt.
  %%usbsuspmsk:11-11
    doc: Set to 1 to unmask USBSUSPINT interrupt.
  %%usbrstmsk:12-12
    doc: Set to 1 to unmask USBRSTINT interrupt.
  %%enumdonemsk:13-13
    doc: Set to 1 to unmask ENUMDONEINT interrupt.
  %%isooutdropmsk:14-14
    doc: Set to 1 to unmask ISOOUTDROPINT interrupt.
  %%iepintmsk:18-18
    doc: Set to 1 to unmask IEPINTINT interrupt.
  %%oepintmsk:19-19
    doc: Set to 1 to unmask OEPINTINT interrupt.
  %%incompisoinmsk:20-20
    doc: Set to 1 to unmask INCOMPISOININT interrupt.
  %%incomplpmsk:21-21
    doc: Set to 1 to unmask INCOMPLPINT interrupt.
  %%fetsuspmsk:22-22
    doc: Set to 1 to unmask FETSUSPINT interrupt.
  %%resetdetmsk:23-23
    doc: Set to 1 to unmask RESETDETINT interrupt.
  %%prtintmsk:24-24
    doc: Set to 1 to unmask PRTINTINT interrupt.
  %%hchintmsk:25-25
    doc: Set to 1 to unmask HCHINTINT interrupt.
  %%ptxfempmsk:26-26 
    doc: Set to 1 to unmask PTXFEMPINT interrupt.
  %%conidstschngmsk:28-28
    doc: Set to 1 to unmask CONIDSTSCHNGINT interrupt.
  %%disconnintmsk:29-29 
    doc: Set to 1 to unmask DISCONNINTINT interrupt.
  %%sessreqintmsk:30-30
    doc: Set to 1 to unmask SESSREQINTINT interrupt.
  %%wkupintmsk:31-31 
    doc: Set to 1 to unmask WKUPINTINT interrupt.

%grxstsr:32
  longname: Receive Status Debug Read Register
  address: 0x3C01C
  direction: rw
  %%chepnum:0-3 
    doc: Host mode: Indicates the channel number to which the current received packet belongs.
    doc: Device mode: Indicates the endpoint number to which the current received packet belongs.
  %%bcnt:4-14
    doc: Host mode: Indicates the byte count of the received IN data packet.
    doc: Device mode: Indicates the byte count of the received data packet.
  %%dpid:15-16
    doc: Host mode: Indicates the Data PID of the received packet. Device mode: Indicates the Data PID of the received OUT data packet.
    0: DATA0
    1: DATA1
    2: DATA2
    3: MDATA
  %%pktsts:17-20
    doc: Indicates the status of the received packet.
    1: GOUTNAK 
    2: PKTRCV 
    3: XFERCOMPL 
    4: SETUPCOMPL 
    5: TGLERR 
    6: SETUPRCV 
    7: CHLT 
  %%fn:24-27
    doc: This is the least significant 4 bits of the Frame number in which the packet is received on the USB.

%grxstsp:32
  longname: Receive Status Read and Pop Register
  address: 0x3C020
  direction: rw
  %%chepnum:0-3 
    doc: Host mode: Indicates the channel number to which the current received packet belongs.
    doc: Device mode: Indicates the endpoint number to which the current received packet belongs.
  %%bcnt:4-14
    doc: Host mode: Indicates the byte count of the received IN data packet.
    doc: Device mode: Indicates the byte count of the received data packet.
  %%dpid:15-16
    doc: Host mode: Indicates the Data PID of the received packet. Device mode: Indicates the Data PID of the received OUT data packet.
    0: DATA0
    1: DATA1
    2: DATA2
    3: MDATA
  %%pktsts:17-20
    doc: Indicates the status of the received packet.
    1: GOUTNAK 
    2: PKTRCV 
    3: XFERCOMPL 
    4: SETUPCOMPL 
    5: TGLERR 
    6: SETUPRCV 
    7: CHLT 
  %%fn:21-24
    doc: This is the least significant 4 bits of the Frame number in which the packet is received on the USB.

%grxfsize:32
  longname: Receive FIFO Size Register
  address: 0x3C024
  direction: rw
  %%chepnum:0-9 
    doc: This value is in terms of 32-bit words. Minimum value is 16. Maximum value is 512.

%gnptxfsize:32
  longname: Non-periodic Transmit FIFO Size Register
  address: 0x3C028
  direction: rw
  %%nptxfstaddr:0-9 
    doc: This field contains the memory start address for Non-periodic Transmit FIFO RAM. Programmed values must not exceed the reset
    doc: value.
  %%nptxfineptxf0dep:16-31 
    doc: This value is in terms of 32-bit words. Minimum value is 16. Maximum value is 512.

%gnptxsts:32
  longname: Non-periodic Transmit FIFO/Queue Status
  address: 0x3C02C
  direction: rw
  %%nptxfspcavail:0-15
    doc: Indicates the amount of free space available in the Non-periodic TxFIFO. Values are in terms of 32-bit words.
  %%nptxqspcavail:16-23
    doc: ndicates the amount of free space (locations) available in the Non-periodic Transmit Request Queue. This queue holds both IN and
    doc: OUT requests in Host mode. Device mode has only IN requests.
  %%nptxqtop:24-30 
    doc: Entry in the Non-periodic Tx Request Queue that is currently being processed by the MAC.
    doc: Bits [6:3]: Channel/endpoint number.
    doc: Bits [2:1]: 00: IN/OUT token, 01: Zero-length transmit packet (device IN/host OUT), 10: Unused, 11: Channel halt command.
    doc: Bit [0]: Terminate (last Entry for selected channel/endpoint).

%gdfifocfg:32
  longname: Global DFIFO Configuration Register
  address: 0x3C05C
  direction: rw
  %%gdfifocfg:0-15
    doc: This field is for dynamic programming of the DFIFO Size. This value takes effect only when the application programs a non zero
    doc: value to this register. The core does not have any corrective logic if the FIFO sizes are programmed incorrectly.
  %%epinfobaseaddr:16-31 
    doc: This field provides the start address of the EP info controller.

%hptxfsize:32
  longname: Host Periodic Transmit FIFO Size Register
  address: 0x3C100
  direction: rw
  %%gdfifocfg:0-10
    doc: This field contains the memory start address for Host Periodic TxFIFO.
  %%ptxfsize:16-25
    doc: This value is in terms of 32-bit words. Minimum value is 16. Maximum value is 512.

%dieptxf:32
  longname: Device IN Endpoint Transmit FIFO Size Register
  address: 0x3C104
  count: 6
  direction: rw
  %%inepntxfdep:16-25 
    doc: This value is in terms of 32-bit words. Minimum value is 16. Maximum value is 512.
  %%inepntxfstaddr:0-10 
    doc: This field contains the memory start address for IN endpoint Transmit FIFO.

%hcfg:32
  longname: Host Configuration Register
  address: 0x3C400
  direction: rw
  %%fslspclksel:0-1
    doc: Use this field to set the internal PHY clock frequency. Set to 48 MHz in FS Host mode and 6 MHz in LS Host mode. When you select
    doc: a 6 MHz clock during LS mode, you must do a soft reset.
    1: DIV1
    2: DIV8
  %%fslssupp: 2-2
    doc: The application uses this bit to control the core's enumeration speed. Using this bit, the application can make the core enumerate as
    doc: a FS host, even If the connected device supports HS traffic. Do not make changes to this field after initial programming.
  %%ena32khzs: 7-7
    doc: When this bit is set the core expects that the clock to the core during Suspend is switched from 48 MHz to 32 KHz.
  %%resvalid: 8-15
    doc: This field is effective only when USB_HCFG.ENA32KHZS is set. It will control the resume period when the core resumes from
    doc: suspend. The core counts for RESVALID number of clock cycles to detect a valid resume when USB_HCFG.ENA32KHZS is set.
  %%modechtimen: 31-31 
    doc: This bit is used to enable/disable the Host core to wait 200 clock cycles at the end of Resume before changing the PHY opmode to
    doc: normal operation. When set to 0 the Host core waits for either 200 PHY clock cycles or a linestate of SE0 at the end of resume to
    doc: the change the PHY opmode to noxmal operation. When set to 1 the Host core waits only for a linstate of SE0 at the end of resume
    doc: to change the PHY opmode to normal operation.

%hfir:32
  longname: Host Frame Interval Register
  address: 0x3C404
  direction: rw
  %%frint:0-15
    doc: The value that the application programs to this field specifies the interval between two consecutive SOFs (FS) or Keep-Alive tokens
    doc: (LS). This field contains the number of PHY clocks that constitute the required frame interval. The application can write a value to
    doc: this register only after the Port Enable bit of the Host Port Control and Status register (USB_HPRT.PRTENA) has been set. If no
    doc: value is programmed, the core calculates the value based on the PHY clock specified in the FS/LS PHY Clock Select field of the
    doc: Host Configuration register (USB_HCFG.FSLSPCLKSEL). Do not change the value of this field after the initial configuration. Set to
    doc: 48000 (1 ms at 48 MHz) for FS and 6000 (1 ms at 6 MHz) for LS.
  %%hfirrldctrl: 16-16
    doc: This bit allows dynamic reloading of the HFIR register during run time. This bit needs to be programmed during initial configuration
    doc: and its value should not be changed during runtime.

%hfnum:32
  longname: Host Frame Number
  address: 0x3C408
  direction: rw
  %%frnum:0-15
    doc: This field increments when a new SOF is transmitted on the USB, and is reset to 0 when it reaches 0x3FFF.
  %%frrem:16-31
    doc: indicates the amount of time remaining in the current Frame, in terms of PHY clocks. This field decrements on each PHY clock. When
    doc: it reaches zero, this field is reloaded with the value in the Frame Interval register and a new SOF is transmitted on the USB.

%hfptxsts:32
  longname: Host Periodic Transmit FIFO/Queue Status Resgister
  address: 0x3C410
  direction: r
  %%ptxfspcavail: 0-15
    doc: Indicates the number of free locations available to be written to in the Periodic TxFIFO. Values are in terms of 32-bit words.
  %%ptxqspcavail: 16-23
    doc: Indicates the number of free locations available to be written in the Periodic Transmit Request Queue. This queue holds both IN
    doc: and OUT requests.
  %%ptxqtop: 24-31 
    doc: This indicates the Entry in the Periodic Tx Request Queue that is currently being processes by the MAC. This register is used for
    doc: debugging.
    doc: Bit [7]: Odd/Even Frame. 0: send in even Frame, 1: send in odd Frame.
    doc: Bits [6:3]: Channel/endpoint number.
    doc: Bits [2:1]: Type. 00: IN/OUT, 01: Zero-length packet, 10: Unused, 11: Disable channel command.
    doc: Bit [0]: Terminate (last Entry for the selected channel/endpoint).

%haint:32
  longname: Host All Channels Interrupt Register
  address: 0x3C414
  direction: r
  %%haint: 0-13 
    doc: When the interrupt bit for a channel x set, one or more of the interrupt flags in the USB_HCx_INT are set.

%haintmsk:32
  longname: Host All Channels Interrupt Mask Register
  address: 0x3C418
  direction: rw
  %%haintmsk: 0-13 
    doc: Set bit n to unmask channel n interrupts.

%hprt:32
  longname: Host Port Control and Status Register
  address: 0x3C440
  direction: rw
  %%prtconnsts: 0-0
    doc: When this bit is 1 a device is attached to the port.
  %%prtconndet: 1-1
    doc: The core sets this bit when a device connection is detected to trigger an interrupt to the application using the Host Port Interrupt
    doc: bit of the Core Interrupt register (USB_GINTSTS.PRTINT). This bit can be set only by the core and the application should write 1 to 
    doc: clear it.The application must write a 1 to this bit to clear the interrupt.
  %%prtena:2-2
    doc: A port is enabled only by the core after a reset sequence, and is disabled by an overcurrent condition, a disconnect condition, or by
    doc: the application clearing this bit. The application cannot set this bit by a register write. It can only clear it to disable the port by writing
    doc: 1. This bit does not trigger any interrupt to the application.
  %%prtenchng:3-3
    doc: The core sets this bit when the status of the Port Enable bit[2] of this register changes. This bit can be set only by the core and the
    doc: application should write 1 to clear it.
  %%prtovrcurract:4-4
    doc: Indicates the overcurrent condition of the port. When there is an overcurrent condition this bit is 1.
  %%prtovrcurrchng:5-5
    doc: The core sets this bit when the status of the Port Overcurrent Active bit (bit 4) in this register changes. This bit can be set only by
    doc: the core and the application should write 1 to clear it.
  %%prtres:6-6
    doc: The application sets this bit to drive resume signaling on the port. The core continues to drive the resume signal until the application
    doc: clears this bit. If the core detects a USB remote wakeup sequence, as indicated by the Port Resume/Remote Wakeup Detected
    doc: Interrupt bit of the Core Interrupt register (USB_GINTSTS.WKUPINT), the core starts driving resume signaling without application
    doc: intervention and clears this bit when it detects a disconnect condition. The read value of this bit indicates whether the core is currently
    doc: driving resume signaling.
  %%prtsusp:7-7
    doc: The application sets this bit to put this port in Suspend mode. The core only stops sending SOFs when this is set. To stop the PHY
    doc: clock, the application must set USB_PCGCCTL.STOPPCLK, which puts the PHY into suspend mode. The read value of this bit
    doc: reflects the current suspend status of the port. This bit is cleared by the core after a remote wakeup signal is detected or the application
    doc: sets the Port Reset bit or Port Resume bit in this register or the Resume/Remote Wakeup Detected Interrupt bit or Disconnect
    doc: Detected Interrupt bit in the Core Interrupt register (USB_GINTSTS.WKUPINT or USB_GINTSTS.DISCONNINT respectively). This
    doc: bit is cleared by the core even if there is no device connected to the Host.
  %%prtrst:8-8 
    doc: When the application sets this bit, a reset sequence is started on this port. The application must time the reset period and clear this
    doc: bit after the reset sequence is complete. The application must leave this bit set for at least 10 ms to start a reset on the port. The
    doc: application can leave it set for another 10 ms in addition to the required minimum duration, before clearing the bit, even though there
    doc: is no maximum limit set by the USB standard.
  %%prtlnsts:10-11
    doc: Indicates the current logic level USB data lines. Bit [0]: Logic level of D+. Bit [1]: Logic level of D-.
  %%prtpwr:12-12
    doc: The application uses this field to control power to this port. The core can clear this bit on an over current condition.
    0: OFF
    1: ON
  %%prttstctl:13-16
    doc: The application writes a nonzero value to this field to put the port into a Test mode, and the corresponding pattern is signaled on
    doc: the port.
    0: DISABLE
    1: J
    2: K
    3: SE0NAK
    4: PACKET
    5: FORCE
  %%prtspd:17-18 
    doc: Indicates the speed of the device attached to this port.
    0: HS
    1: FS
    2: LS

%hc_char:32
  longname: Host Channel Characteristics Register
  address: 0x3C500
  direction: rw
  count: 14
  stride: 0x20
  %%mps:0-10
    doc: Indicates the maximum packet size of the associated endpoint.
  %%epnum:11-14
    doc: Indicates the maximum packet size of the associated endpoint.
  %%epdir:15-15
    doc: Indicates whether the transaction is IN or OUT.
    0: OUT
    1: IN
  %%lspddev:17-17
    doc: This field is set by the application to indicate that this channel is communicating to a low-speed device.
  %%eptype:18-19
    doc: Indicates the transfer type selected.
    0: CONTROL
    1: ISO
    2: BULK
    3: INT
  %%mc:20-21
    doc: For periodic transfers this field indicates to the host the number of transactions that must be executed per frame for this periodic
    doc: endpoint. For non-periodic transfers, this field is used only in DMA mode, and specifies the number packets to be fetched for this
    doc: channel before the internal DMA engine changes arbitration.
  %%devaddr:22-28
    doc: This field selects the specific device serving as the data source or sink.
  %%oddfrm:29-29
    doc: This field selects the specific device serving as the data source or sink.
  %%chdis: 30-30
    doc: The application sets this bit to stop transmitting/receiving data on a channel, even before the transfer for that channel is complete.
    doc: The application must wait for the Channel Disabled interrupt before treating the channel as disabled.
  %%chena: 31-31
    doc: This field is set by the application and cleared by the core. The state of this bit reflects the channel enable status.
    doc: el is complete.

%hc_int:32
  longname: Host Channel Interrupt Register
  address: 0x3C508
  direction: rw
  count: 14
  stride: 0x20
  %%xfercompl: 0-0
    doc: Transfer completed normally without any errors. This bit can be set only by the core and the application should write 1 to clear it.
  %%chhltd:1-1
    doc: In DMA mode this bit indicates the transfer completed abnormally either because of any USB transaction error or in response to
    doc: disable request by the application or because of a completed transfer.
  %%ahberr:2-2
    doc: This is generated only in DMA mode when there is an AHB error during AHB read/write. The application can read the corresponding
    doc:  channel's DMA address register to get the error address.
  %%stall:3-3
    doc:This bit can be set only by the core and the application should write 1 to clear it.
  %%nak:4-4
    doc: This bit can be set only by the core and the application should write 1 to clear it.
  %%ack:5-5
    doc: This bit can be set only by the core and the application should write 1 to clear it.
  %%xacterr:7-7
    doc: Indicates one of the following errors occurred on the USB: CRC check failure, Timeout, Bit stuff error or False EOP. This bit can be
    doc: set only by the core and the application should write 1 to clear it.
  %%bblerr:8-8
    doc: This bit can be set only by the core and the application should write 1 to clear it.
  %%frmovrun:9-9
    doc: This bit can be set only by the core and the application should write 1 to clear it.
  %%datatglerr:10-10
    doc: This bit can be set only by the core and the application should write 1 to clear it.

%hc_intmsk: 32
  longname: Host Channel Interrupt Mask Register
  address: 0x3C50C
  direction: rw
  count: 14
  stride: 0x20
  %%xfercomplmsk:0-0
    doc: Set to unmask XFERCOMPL interrupt.
  %%chhltdmsk:1-1
    doc: Set to unmask CHHLTD interrupt.
  %%ahberrmsk:2-2
    doc: Set to unmask AHBERR interrupt.
  %%stallmsk:3-3
    doc: Set to unmask STALL interrupt.
  %%nakmsk:4-4
    doc: Set to unmask NAK interrupt.
  %%ackmsk:5-5
    doc: Set to unmask ACK interrupt.
  %%xacterrmsk:7-7
    doc: Set to unmask XACTERR interrupt.
  %%bblerrmsk:8-8
    doc: Set to unmask BBLERR interrupt.
  %%frmovrunmsk:9-9
    doc: Set to unmask FRMOVRUN interrupt.
  %%datatglerrmsk:10-10
    doc: Set to unmask DATATGLERR interrupt.

%hc_tsize: 32
  longname: Host Channel x Transfer Size Register
  address: 0x3C510
  direction: rw
  count: 14
  stride: 0x20
  %%xfersize:0-18
    doc: For an OUT, this field is the number of data bytes the host sends during the transfer. For an IN, this field is the buffer size that the
    doc: application has reserved for the transfer. The application is expected to program this field as an integer multiple of the maximum
    doc: packet size for IN transactions (periodic and non-periodic).
  %%pktcnt:19-28
    doc: This field is programmed by the application with the expected number of packets to be transmitted (OUT) or received (IN). The
    doc: host decrements this count on every successful transmission or reception of an OUT/IN packet. Once this count reaches zero, the
    doc: application is interrupted to indicate normal completion.
  %%pid:29-30
    doc: The application programs this field with the packet ID type to use for the initial transaction. The host maintains this field for the rest
    doc: of the transfer.
    0: DATA0
    1: DATA2
    2: DATA1
    3: MDATA

%hc_dmaaddr: 32
  longname: Host Channel DMA Address Register
  address: 0x3C514
  direction: rw
  count: 14
  stride: 0x20
  %%val:0-31
    doc: This field holds the start address in the external memory from which the data for the endpoint must be fetched or to which it must
    doc: be stored. This register is incremented on every AHB transaction. The data for this register field is stored in RAM. Thus, the reset
    doc: value is undefined (X).

%dcfg: 32
  longname: Device Configuration Register
  address: 0x3C800
  direction: rw
  %%devspd:0-1
    doc: Indicates the speed at which the application requires the core to enumerate, or the maximum speed the application can support.
    doc: However, the actual bus speed is determined only after the chirp sequence is completed, and is based on the speed of the USB
    doc: host to which the core is connected.
    2: 6MHZ
    3: 48MHZ
  %%nzstsouthshk:2-2
    doc: The application can use this field to select the handshake the core sends on receiving a nonzero-length data packet during the OUT
    doc: transaction of a control transfer's Status stage. When set to 1 send a STALL handshake on a nonzero-length status OUT transaction
    doc: and do not send the received OUT packet to the application. When set to 0 send the received OUT packet to the application (zerolength
    doc: or nonzero-length) and send a handshake based on the NAK and STALL bits for the endpoint in the Device Endpoint Control register.
  %%ena32khzsusp:3-3
    doc: When this bit is set, the core expects that the PHY clock during Suspend is switched from 48 MHz to 32 KHz.
  %%devaddr:4-10
    doc: The application must program this field after every SetAddress control command.
  %%perfrint:11-12 
    doc: Indicates the time within a frame at which the application must be notified using the End Of Periodic Frame Interrupt. This can be
    doc: used to determine if all the isochronous traffic for that frame is complete.
    0: 80PCNT
    1: 85PCNT
    2: 90PCNT
    3: 95PCNT
  %%resvalid:26-31 
    doc: This field is effective only when USB_DCFG.ENA32KHZSUSP is set. It will control the resume period when the core resumes from
    doc: suspend. The core counts for RESVALID number of clock cycles to detect a valid resume when USB_DCFG.ENA32KHZSUSP is set.

%dctl: 32
  longname: Device Control Register
  address: 0x3C804
  direction: rw
  %%rmtwkupsig: 0-0
    doc: When the application sets this bit, the core initiates remote signaling to wake up the USB host. The application must set this bit
    doc: to instruct the core to exit the Suspend state. As specified in the USB 2.0 specification, the application must clear this bit 1-15 ms
    doc: after setting it.
  %%sftdiscon:1-1
    doc: The application uses this bit to signal the core to do a soft disconnect. As long as this bit is set, the host does not see that the device is
    doc: connected, and the device does not receive signals on the USB. The core stays in the disconnected state until the application clears
    doc: this bit. When suspended, the minimum duration for which the core must keep this bit set is 1 ms + 2.5 us. When IDLE or performing
    doc: transactions, the minimum duration for which the core must keep this bit set is 2.5 us.
  %%gnpinnaksts: 2-2
    doc: When this bit is 0 a handshake is sent out based on the data availability in the transmit FIFO. When this bit is 1 a NAK handshake
    doc: is sent out on all non-periodic IN endpoints, irrespective of the data availability in the transmit FIFO.
  %%goutnaksts:3-3
    doc: When this bit is 0 a handshake is sent based on the FIFO Status and the NAK and STALL bit settings. When this bit is 1 no data
    doc: is written to the RxFIFO, irrespective of space availability. Sends a NAK handshake on all packets, except on SETUP transactions.
    doc: All isochronous OUT packets are dropped.
  %%tstctl:4-6
    doc: Set to a non-zero value to enable test control.
    0: DISABLE
    1: J
    2: K
    3: SE0NAK
    4: PACKET
    5: FORCE
  %%sgnpinnak:7-7
    doc: A write to this field sets the Global Non-periodic IN NAK. The application uses this bit to send a NAK handshake on all non-periodic IN
    doc: endpoints. The application must set this bit only after making sure that the Global IN NAK Effective bit in the Core Interrupt Register
    doc: (USB_GINTSTS.GINNAKEFF) is cleared.
  %%cgnpinnak:8-8
    doc: A write to this field clears the Global Non-periodic IN NAK.
  %%sgoutnak:9-9
    doc: A write to this field sets the Global OUT NAK. The application uses this bit to send a NAK handshake on all OUT endpoints.
    doc: The application must set this bit only after making sure that the Global OUT NAK Effective bit in the Core Interrupt Register
    doc: (USB_GINTSTS.GOUTNAKEFF) is cleared.
  %%cgoutnak: 10-10
    doc: A write to this field clears the Global OUT NAK.
  %%pwronprgdone: 11-11
    doc: The application uses this bit to indicate that register programming is completed after a wake-up from Power Down mode.
  %%ignrfrmnum: 15-15
    doc: When set to 0 the core transmits the packets only in the frame number in which they are intended to be transmitted. When set to 1
    doc: the core ignores the frame number, sending packets immediately as the packets are ready.
  %%nakonbble: 16-16
    doc: Set NAK automatically on babble. The core sets NAK automatically for the endpoint on which babble is received.

%dsts: 32
  longname: Device Status Register
  address: 0x3C808
  direction: r
  %%suspsts: 0-0
    doc: In Device mode, this bit is set as long as a Suspend condition is detected on the USB. The core enters the Suspended state when
    doc: there is no activity on the bus for an extended period of time. The core comes out of the suspend when there is any activity on the
    doc: bus or when the application writes to the Remote Wakeup Signaling bit in the Device Control register (USB_DCTL.RMTWKUPSIG).
  %%enumspd:1-2
    doc: Indicates the speed at which the core has come up after speed detection through a chirp sequence.
  %%errticerr:3-3
    doc: The core sets this bit to report any erratic errors (PHY error) Due to erratic errors, the core goes into Suspended state and an interrupt
    doc: is generated to the application with Early Suspend bit of the Core Interrupt register (USB_GINTSTS.ERLYSUSP). If the early suspend
    doc: is asserted due to an erratic error, the application can only perform a soft disconnect recover.
  %%soffn:8-21
    doc: This field contains a Frame number. This field may return a non zero value if read immediately after power on reset. In case the
    doc: register bits reads non zero immediately after power on reset it does not indicate that SOF has been received from the host. The
    doc: read value of this interrupt is valid only after a valid connection between host and device is established.
    doc: he core sets this bit to report any erratic errors (PHY error) Due to erratic errors, the core goes into Suspended state and an interrupt

%diepmsk: 32
  longname: Device IN Endpoint Common Interrupt Mask Register
  address: 0x3C810
  direction: rw
  %%xfercomplmsk:0-0
    doc: Set to 1 to unmask XFERCOMPL Interrupt.
  %%epdisbldmsk:1-1
    doc: Set to 1 to unmask EPDISBLD Interrupt.
  %%ahberrmsk:2-2
    doc: Set to 1 to unmask AHBERR Interrupt.
  %%timeoutmsk:3-3
    doc: Set to 1 to unmask Interrupt TIMEOUT. Applies to Non-isochronous endpoints.
  %%intkntxfempmsk:4-4 
    doc: Set to 1 to unmask INTKNTXFEMP Interrupt.
  %%inepnakeffmsk:5-5 
    doc: Set to 1 to unmask INEPNAKEFF Interrupt.
  %%txfifoundrnmsk:8-8
    doc: Set to 1 to unmask TXFIFOUNDRN Interrupt.
  %%nakmsk:13-13
    doc: Set to 1 to unmask NAK Interrupt.

%doepmsk: 32
  longname: Device OUT Endpoint Common Interrupt Mask Register
  address: 0x3C814
  direction: rw
  %%xfercomplmsk:0-0
    doc: Set to 1 to unmask XFERCOMPL Interrupt.
  %%epdisbldmsk:1-1
    doc: Set to 1 to unmask EPDISBLD Interrupt.
  %%ahberrmsk:2-2
    doc: Set to 1 to unmask AHBERR Interrupt.
  %%setupmsk:3-3
    doc: Set to 1 to unmask SETUP Interrupt. Applies to control endpoints only.
  %%outtkntxfempmsk:4-4 
    doc: Set to 1 to unmask OUTTKNTXFEMP Interrupt.
  %%back2backsetup:6-6 
    doc: Set to 1 to unmask BACK2BACKSETUP Interrupt. Applies to control OUT endpoints only.
  %%outpkterrmsk:8-8
    doc: Set to 1 to unmask OUTPKTERR Interrupt.
  %%bbleerrmsk:12-12
    doc: Set to 1 to unmask NAK Interrupt.
  %%nakmsk:13-13
    doc: Set to 1 to unmask BBLEERR Interrupt.

%daint: 32
  longname: Device All Endpoints Interrupt Register
  address: 0x3C818
  direction: r
  %%inepint:0-0
    count: 7
    doc: This bit is set when one or more of the interrupt flags in USB_DIEP0INT are set.
  %%outepint:16-16
    count: 7
    doc: This bit is set when one or more of the interrupt flags in USB_DOEP0INT are set.

%daintmsk: 32
  longname: Device All Endpoints Interrupt Msk Register
  address: 0x3C81C
  direction: rw
  %%inepmsk:0-0
    count: 7
    doc: Set to 1 to unmask USB_DAINT.INEPINTX.
  %%outepmsk:16-16
    count: 7
    doc: Set to 1 to unmask USB_DAINT.OUTEPINTX.

%dvbusdis: 32
  longname: Device VBUS Discharge Time Register
  address: 0x3C828
  direction: rw
  %%val: 0-15
    doc: Specifies the VBUS discharge time after VBUS pulsing during SRP. This value equals VBUS discharge time in PHY clocks / 1024.
    doc: Depending on your VBUS load, this value can need adjustment.

%dvbuspulse: 32
  longname: Device VBUS Pulsing Time Register
  address: 0x3C82C
  direction: rw
  %%val: 0-15
    doc: Specifies the VBUS pulsing time during SRP. This value equals VBUS pulsing time in PHY clocks / 1024.
    doc: e in PHY clocks / 1024.

%diepempmsk: 32
  longname: Device IN Endpoint FIFO Empty Interrupt Mask Register
  address: 0x3C834
  direction: rw
  %%val: 0-15
    doc: These bits acts as mask bits for USB_DIEP0INT.TXFEMP/USB_DIEPx_INT.TXFEMP interrupt. One bit per IN Endpoint: Bit 0 for
    doc: IN EP 0, bit 6 for IN EP 6.

%diepctl: 32
  longname: Device IN Endpoint Control Register
  address: 0x3C900
  count: 7
  stride: 0x20
  direction: rw
  %%mps: 0-10 
    doc: The application must program this field with the maximum packet size for the current logical endpoint.
  %%usbactep: 15-15 
    doc: This bit is always 1, indicating that control endpoint 0 is always active in all configurations and interfaces.
  %%dpideof: 16-16 
    doc: For interrupt/bulk endpoints this field contains the PID of the packet to be received or transmitted on this endpoint. The application
    doc: must program the PID of the first packet to be received or transmitted on this endpoint, after the endpoint is activated. The applications
    doc: use the SETD1PIDOF and SETD0PIDEF fields of this register to program either DATA0 or DATA1 PID. For isochronous endpoints,
    doc: this field indicates the frame number in which the core transmits/receives isochronous data for this endpoint. The application must
    doc: program the even/odd frame number in which it intends to transmit/receive isochronous data for this endpoint using the SETD0PIDEF
    doc: and SETD1PIDOF fields in this register.
    0: EVEN
    1: ODD
  %%naksts: 17-17 
    doc: When this bit is 0 the core is transmitting non-NAK handshakes based on the FIFO status. When this bit is 1 the core is transmitting
    doc: NAK handshakes on this endpoint. When either the application or the core sets this bit the core stops receiving any data on an OUT
    doc: endpoint, even if there is space in the RxFIFO to accommodate the incoming packet. For non-isochronous IN endpoints the core
    doc: stops transmitting any data on an IN endpoint, even if there data is available in the TxFIFO. For isochronous IN endpoints the core
    doc: sends out a zero-length data packet, even if there data is available in the TxFIFO. Irrespective of this bit's setting, the core always
    doc: responds to SETUP data packets with an ACK handshake.
  %%etype: 18-19 
    doc: This is the transfer type supported by this logical endpoint.
    0: CONTROL
    1: ISO
    2: BULK
    3: INT
  %%stall: 21-21 
    doc: For bulk and interrupt endpoints: The application sets this bit to stall all tokens from the USB host to this endpoint. If a NAK bit, Global
    doc: Non-periodic IN NAK, or Global OUT NAK is set along with this bit, the STALL bit takes priority. In this case only the application
    doc: can clear this bit, never the core.
    doc: When control endpoint: The application can only set this bit, and the core clears it, when a SETUP token is received for this endpoint.
    doc: If a NAK bit, Global Non-periodic IN NAK, or Global OUT NAK is set along with this bit, the STALL bit takes priority. Irrespective of
    doc: this bit's setting, the core always responds to SETUP data packets with an ACK handshake.
  %%txfnum: 22-25 
    doc: These bits specify the FIFO number associated with this endpoint. Each active IN endpoint must be programmed to a separate FIFO
    doc: number. This field is valid only for IN endpoints.
  %%cnak: 26-26
    doc: A write to this bit clears the NAK bit for the endpoint.
  %%snak: 27-27 
    doc: A write to this bit sets the NAK bit for the endpoint. Using this bit, the application can control the transmission of NAK handshakes
    doc: on an endpoint. The core can also set this bit for an endpoint after a SETUP packet is received on that endpoint.
  %%setd0pidef: 28-28 
    doc: For bulk and interrupt endpoints writing this field sets the Endpoint Data PID / Even or Odd Frame (DPIDEOF) field in this register
    doc: to DATA0EVEN.
    doc: For isochronous endpoints writing this field sets the Endpoint Data PID / Even or Odd Frame (DPIDEOF) field to odd (DATA0EVEN).
  %%setd1pidof: 29-29 
    doc: For bulk and interrupt endpoints writing this field sets the Endpoint Data PID / Even or Odd Frame (DPIDEOF) field in this register
    doc: to DATA1ODD.
    doc: For isochronous endpoints writing this field sets the Endpoint Data PID / Even or Odd Frame (DPIDEOF) field to odd (DATA1ODD).
  %%epdis:30-30
    doc: The application sets this bit to stop transmitting data on an endpoint, even before the transfer for that endpoint is complete. The
    doc: application must wait for the Endpoint Disabled interrupt before treating the endpoint as disabled. The core clears this bit before
    doc: setting the Endpoint Disabled Interrupt. The application must set this bit only if Endpoint Enable is already set for this endpoint.
  %%epena:31-31
    doc: In DMA mode for IN endpoints, this bit indicates that data is ready to be transmitted on the endpoint. The core clears this bit before
    doc: setting any of the following interrupts on this endpoint: SETUP Phase Done, Endpoint Disabled, Transfer Completed. For control
    doc: endpoints in DMA mode, this bit must be set to be able to transfer SETUP data packets in memory.

%diepint: 32
  longname: Device IN Endpoint Interrupt Register
  address: 0x3C908
  count: 7
  stride: 0x20
  direction: rw
  %%xfercompl:0-0
    doc: This field indicates that the programmed transfer is complete on the AHB as well as on the USB, for this endpoint.
  %%epdisbld:1-1
    doc: This bit indicates that the endpoint is disabled per the application's request.
  %%ahberr:2-2
    doc: This is generated in DMA mode when there is an AHB error during an AHB read/write. The application can read the corresponding
    doc: endpoint DMA address register to get the error address.
  %%timeout:3-3
    doc: Applies only to Control IN endpoints. Indicates that the core has detected a timeout condition on the USB for the last IN token on
    doc: this endpoint.
  %%intkntxfemp: 4-4 
    doc: Applies to non-periodic IN endpoints only. Indicates that an IN token was received when the associated TxFIFO (periodic/non-
    doc: periodic) was empty. This interrupt is asserted on the endpoint for which the IN token was received.
  %%inepnakeff:5-5
    doc: Applies to periodic IN endpoints only. This bit can be cleared when the application clears the IN endpoint NAK by writing to
    doc: USB_DIEP0CTL.CNAK. This interrupt indicates that the core has sampled the NAK bit set (either by the application or by the core).
    doc: The interrupt indicates that the IN endpoint NAK bit set by the application has taken effect in the core. This interrupt does not guarantee
    doc: that a NAK handshake is sent on the USB. A STALL bit takes priority over a NAK bit.
  %%txfemp:7-7 
    doc: This interrupt is asserted when the TxFIFO for this endpoint is either half or completely empty. The half or completely empty status
    doc: is determined by the TxFIFO Empty Level bit in the Core AHB Configuration register (USB_GAHBCFG.NPTXFEMPLVL).
  %%pktdrpsts:11-11
    doc: This bit indicates to the application that an ISO OUT packet has been dropped. This bit does not have an associated mask bit and
    doc: does not generate an interrupt.
  %%bblerr:12-12
    doc: The core generates this interrupt when babble is received for the endpoint.
  %%nakintrpt:13-13 
    doc: The core generates this interrupt when a NAK is transmitted or received by the device. In case of isochronous IN endpoints the
    doc:interrupt gets generated when a zero length packet is transmitted due to un-availability of data in the TXFifo.

%dieptsize: 32
  longname: Device IN Endpoint Transfer Size Register
  address: 0x3C910
  direction: rw
  count: 7
  stride: 0x20
  %%xfersize: 0-6 
    doc: Indicates the transfer size in bytes for endpoint 0. The core interrupts the application only after it has exhausted the transfer size
    doc: amount of data. The transfer size can be set to the maximum packet size of the endpoint, to be interrupted at the end of each packet.
    doc: The core decrements this field every time a packet from the external memory is written to the TxFIFO.
  %%pktcnt: 19-28 
    doc: Indicates the total number of USB packets that constitute the Transfer Size amount of data for endpoint 0. This field is decremented
    doc: every time a packet (maximum size or short packet) is read from the TxFIFO.
  %%mc: 29-30 
    doc: For periodic IN endpoints, this field indicates the number of packets that must be transmitted per frame on the USB. The core uses
    doc: this field to calculate the data PID for isochronous IN endpoints.

%diepdmaaddr: 32
  longname: Device IN Endpoint DMA Address Register
  address: 0x3C914
  direction: rw
  count: 7
  stride: 0x20
  %%val: 0-31 
    doc: Holds the start address of the external memory for fetching endpoint data. For control endpoints, this field stores control OUT data
    doc: packets as well as SETUP transaction data packets. When more than three SETUP packets are received back-to-back, the SETUP
    doc: data packet in the memory is overwritten. This register is incremented on every AHB transaction. The application can give only a
    doc: DWORD-aligned address. The data for this register field is stored in RAM. Thus, the reset value is undefined (X).

%dieptxfsts: 32
  longname: Device IN Endpoint Transmit FIFO Status Register
  address: 0x3C918
  direction: rw
  count: 7
  stride: 0x20
  %%spcavail: 0-15 
    doc: Indicates the amount of free space available in the Endpoint TxFIFO. Values are in terms of 32-bit words.

%doepctl: 32
  longname: Device OUT Endpoint Control Register
  address: 0x3CB00
  count: 7
  stride: 0x20
  direction: rw
  %%mps: 0-10 
    doc: The application must program this field with the maximum packet size for the current logical endpoint. This value is in bytes.
  %%usbactep: 15-15 
    doc: Indicates whether this endpoint is active in the current configuration and interface. The core clears this bit for all endpoints after
    doc: detecting a USB reset. After receiving the SetConfiguration and SetInterface commands, the application must program endpoint
    doc: registers accordingly and set this bit.
  %%dpideof: 16-16 
    doc: For interrupt/bulk endpoints: Contains the PID of the packet to be received or transmitted on this endpoint. The application must
    doc: program the PID of the first packet to be received or transmitted on this endpoint, after the endpoint is activated. The application use
    doc: the SETD1PIDOF and SETD0PIDEF fields of this register to program either DATA0 or DATA1 PID.
    doc: For isochronous endpoints: Indicates the frame number in which the core transmits/receives isochronous data for this endpoint. The
    doc: application must program the even/odd frame number in which it intends to transmit/receive isochronous data for this endpoint using
    doc: the SETD1PIDOF and SETD0PIDEF fields in this register.
    0: EVEN
    1: ODD
  %%naksts: 17-17 
    doc: When this bit is 0 the core is transmitting non-NAK handshakes based on the FIFO status. When this bit is 1 the core is transmitting
    doc: NAK handshakes on this endpoint. When this bit is set, either by the application or core, the core stops transmitting data, even if
    doc: there is data available in the TxFIFO. Irrespective of this bit's setting, the core always responds to SETUP data packets with an
    doc: ACK handshake.
  %%etype: 18-19 
    doc: This is the transfer type supported by this logical endpoint.
    0: CONTROL
    1: ISO
    2: BULK
    3: INT
  %%snp: 20-20 
    doc: This bit configures the endpoint to Snoop mode. In Snoop mode, the core does not check the correctness of OUT packets before
    doc: transferring them to application memory.
  %%stall: 21-21 
    doc: For non-control, non-isochronous endpoints: The application sets this bit to stall all tokens from the USB host to this endpoint. If a
    doc: NAK bit, Global Non-periodic IN NAK, or Global OUT NAK is set along with this bit, the STALL bit takes priority. Only the application
    doc: can clear this bit, never the core.
    doc: For control endpoints: The application can only set this bit, and the core clears it, when a SETUP token is received for this endpoint.
    doc: If a NAK bit, Global Non-periodic IN NAK, or Global OUT NAK is set along with this bit, the STALL bit takes priority. Irrespective of
    doc: this bit's setting, the core always responds to SETUP data packets with an ACK handshake.
  %%cnak: 26-26
    doc: A write to this bit clears the NAK bit for the endpoint.
  %%snak: 27-27 
    doc: A write to this bit sets the NAK bit for the endpoint. Using this bit, the application can control the transmission of NAK handshakes
    doc: on an endpoint. The core can also set this bit for an endpoint after a SETUP packet is received on that endpoint.
  %%setd0pidef: 28-28 
    doc: For bulk and interrupt endpoints writing this field sets the Endpoint Data PID / Even or Odd Frame (DPIDEOF) field in this register
    doc: to DATA0EVEN.
    doc: For isochronous endpoints writing this field sets the Endpoint Data PID / Even or Odd Frame (DPIDEOF) field to odd (DATA0EVEN).
  %%setd1pidof: 29-29 
    doc: For bulk and interrupt endpoints writing this field sets the Endpoint Data PID / Even or Odd Frame (DPIDEOF) field in this register
    doc: to DATA1ODD.
    doc: For isochronous endpoints writing this field sets the Endpoint Data PID / Even or Odd Frame (DPIDEOF) field to odd (DATA1ODD).
  %%epdis:30-30
    doc: The application sets this bit to stop transmitting/receiving data on an endpoint, even before the transfer for that endpoint is complete.
    doc: The application must wait for the Endpoint Disabled interrupt before treating the endpoint as disabled. The core clears this bit before
    doc: setting the Endpoint Disabled interrupt. The application must set this bit only if Endpoint Enable is already set for this endpoint.
  %%epena:31-31
    doc: In DMA mode this bit indicates that the application has allocated the memory to start receiving data from the USB. The core clears
    doc: this bit before setting any of the following interrupts on this endpoint: SETUP Phase Done, Endpoint Disabled, Transfer Completed.
    doc: In DMA mode, this bit must be set for the core to transfer SETUP data packets into memory.

%doepint: 32
  longname: Device OUT Endpoint Interrupt Register
  address: 0x3CB08
  count: 7
  stride: 0x20
  direction: rw
  %%xfercompl:0-0
    doc: This field indicates that the programmed transfer is complete on the AHB as well as on the USB, for this endpoint.
  %%epdisbld:1-1
    doc: This bit indicates that the endpoint is disabled per the application's request.
  %%ahberr:2-2
    doc: This is generated in DMA mode when there is an AHB error during an AHB read/write. The application can read the corresponding
    doc: endpoint DMA address register to get the error address.
  %%setup:3-3
    doc: Applies to control OUT endpoints only. Indicates that the SETUP phase for the control endpoint is complete and no more back-
    doc: to-back SETUP packets were received for the current control transfer. On this interrupt, the application can decode the received
    doc: SETUP data packet.
  %%outtknepdis:4-4 
    doc: Applies only to control OUT endpoints. Indicates that an OUT token was received when the endpoint was not yet enabled. This
    doc: interrupt is asserted on the endpoint for which the OUT token was received.
  %%back2backsetup:6-6
    doc: Applies to Control OUT endpoints only. This bit indicates that the core has received more than three back-to-back SETUP packets
    doc: for this particular endpoint.
  %%pktdrpsts:11-11
    doc: This bit indicates to the application that an ISO OUT packet has been dropped. This bit does not have an associated mask bit and
    doc: does not generate an interrupt.
  %%bblerr:12-12
    doc: The core generates this interrupt when babble is received for the endpoint.
  %%nakintrpt:13-13 
    doc: The core generates this interrupt when a NAK is transmitted or received by the device. In case of isochronous IN endpoints the
    doc:interrupt gets generated when a zero length packet is transmitted due to un-availability of data in the TXFifo.

%doeptsize: 32
  longname: Device OUT Endpoint Transfer Size Register
  address: 0x3CB10
  direction: rw
  count: 7
  stride: 0x20
  %%xfersize: 0-6 
    doc: Indicates the transfer size in bytes for endpoint 0. The core interrupts the application only after it has exhausted the transfer size
    doc: amount of data. The transfer size can be set to the maximum packet size of the endpoint, to be interrupted at the end of each packet.
    doc: The core decrements this field every time a packet from the external memory is written to the TxFIFO.
  %%pktcnt: 19-28
    doc: This field is decremented to zero after a packet is written into the RxFIFO.
  %%rxdpidsupcnt: 29-30
    doc: For isochronous OUT endpoints: This is the data PID received in the last packet for this endpoint.
    doc: For control OUT Endpoints: This field specifies the number of back-to-back SETUP data packets the endpoint can receive.
    0: DATA0
    1: DATA2
    2: DATA1
    3: MDATA

%doepdmaaddr: 32
  longname: Device OUT Endpoint DMA Address Register
  address: 0x3CB14
  direction: rw
  count: 7
  stride: 0x20
  %%val: 0-31 
    doc: Holds the start address of the external memory for fetching endpoint data. For control endpoints, this field stores control OUT data
    doc: packets as well as SETUP transaction data packets. When more than three SETUP packets are received back-to-back, the SETUP
    doc: data packet in the memory is overwritten. This register is incremented on every AHB transaction. The application can give only a
    doc: DWORD-aligned address. The data for this register field is stored in RAM. Thus, the reset value is undefined (X).

%pcgcctl: 32
  longname: Power and Clock Gating Control Register
  address: 0x3CE00
  direction: rw
  %%stoppclk: 0-0 
    doc: The application sets this bit to stop the PHY clock when the USB is suspended, the session is not valid, or the device is disconnected.
    doc: The application clears this bit when the USB is resumed or a new session starts.
  %%gatehclk: 1-1 
    doc: The application sets this bit to gate the clock (HCLK) to modules other than the AHB Slave and Master and wakeup logic when the
    doc: USB is suspended or the session is not valid. The application clears this bit when the USB is resumed or a new session starts.
  %%pwrclmp: 2-2 
    doc: The application sets this bit before the power is turned off to clamp the signals between the power-on modules and the power-off
    doc: modules of the USB core. The application clears the bit to disable the clamping.
  %%rstpdwnmodule: 3-3
    doc: The application sets this bit to reset the part of the USB that is powered down during EM2. The application clears this bit to release
    doc: reset after an waking up from EM2 when the PHY clock is back at 48/6 MHz. Accessing core registers is possible only when this
    doc: bit is set to 0.
  %%physleep: 6-6 
    doc: Indicates that the PHY is in Sleep State.
  %%resetaftersusp: 8-8 
    doc: When exiting EM2, this bit needs to be set in host mode before clamp is removed if the host needs to issue reset after suspend. If
    doc: this bit is not set, then the host issues resume after suspend. This bit is not applicable in device mode and when EM2 is not used.

%fifo: 32
  longname: Device EP/Host Channel FIFO
  direction: rw
  address: 0x3D000
  count: 14
  stride: 0x1000
  %%data: 0-31
    doc: This register, available in both Host and Device modes, is used to read or write the FIFO space for
    doc: endpoint 0 or channel 0, in a given direction. If a host channel is of type IN, the FIFO can only be read
    doc: on the channel. Similarly, if a host channel is of type OUT, the FIFO can only be written on the channel.

%fiforam: 32
  longname: Direct Access to Data FIFO RAM for Debugging
  direction: rw
  address: 0x5C000
  %%data: 0-31
    doc: Direct Access to Data FIFO RAM for Debugging (2 KB)
