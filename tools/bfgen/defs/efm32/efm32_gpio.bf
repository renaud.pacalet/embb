name: efm32_gpio
longname: EFM32 General purpose Input Output

bound: 32
align: 32

%ctrl: 32
  longname: Port Control Register
  direction: rw
  address: 0
  count: 6
  stride: 0x24
  %%drivemode: 0-1
    doc: Select drive mode for all pins on port configured with alternate drive strength
    0: STANDART
    1: LOWEST
    2: HIGHT
    3: LOW

%model: 32
  longname: Port Pin Mode Low Register
  direction: rw
  address: 0x4
  count: 6
  stride: 0x24
  %%mode: 0-3
    count: 8
    0:  DISABLED
    1:  INPUT
    2:  INPUTPULL
    3:  INPUTPULLFILTER
    4:  PUSHPULL
    5:  PUSHPULLDRIVE
    6:  WIREDOR
    7:  WIREDORPULLDOWN
    8:  WIREDAND
    9:  WIREDANDFILTER
    10: WIREDANDPULLUP
    11: WIREDANDPULLUPFILTER
    12: WIREDANDDRIVE
    13: WIREDANDDRIVEFILTER
    14: WIREDANDDRIVEPULLUP
    15: WIREDANDDRIVEPULLUPFILTER

%modeh: 32
  longname: Port Pin Mode high Register
  direction: rw
  address: 0x8
  count: 6
  stride: 0x24
  %%mode: 0-3
    count: 8
    0:  DISABLED
    1:  INPUT
    2:  INPUTPULL
    3:  INPUTPULLFILTER
    4:  PUSHPULL
    5:  PUSHPULLDRIVE
    6:  WIREDOR
    7:  WIREDORPULLDOWN
    8:  WIREDAND
    9:  WIREDANDFILTER
    10: WIREDANDPULLUP
    11: WIREDANDPULLUPFILTER
    12: WIREDANDDRIVE
    13: WIREDANDDRIVEFILTER
    14: WIREDANDDRIVEPULLUP
    15: WIREDANDDRIVEPULLUPFILTER

%dout: 32
  longname: Port Data Out Register
  direction: rw
  address: 0xC
  count: 6
  stride:  0x24
  %%dout: 0-0
    count: 16
    doc: Data out on port n.

%doutset: 32
  longname: Port Data Out Set Register
  direction: w
  address: 0x10
  count: 6
  stride:  0x24
  %%doutset: 0-0
    count: 16
    doc: Write bits to 1 to set corresponding bits in GPIO_Px_DOUT. Bits written to 0 will have no effect.

%doutclr: 32
  longname: Port Data Out Clear Register
  direction: w
  address: 0x14
  count: 6
  stride:  0x24
  %%doutclr: 0-0
    count: 16
    doc: Write bits to 1 to clear corresponding bits in GPIO_Px_DOUT. Bits written to 0 will have no effect.

%douttgl: 32
  longname: Port Data Out Toggle Register
  direction: w
  address: 0x18
  count: 6
  stride:  0x24
  %%douttgl: 0-0
    count: 16
    doc: Write bits to 1 to toggle corresponding bits in GPIO_Px_DOUT. Bits written to 0 will have no effect

%din: 32
  longname: Port Data In Register
  direction: r
  address: 0x1C
  count: 6
  stride:  0x24
  %%din: 0-0
    count: 16
    doc: Port Data Input

%pinlockn: 32
  longname: Port Unlock Pin Register
  direction: rw
  address: 0x20
  count: 6
  stride:  0x24
  %%pinlockn: 0-0
    count: 16
    doc: Shows unlocked pins in the port. To lock pin n, clear bit n. The pin is then locked until reset.

%extipsell: 32
  longname: External Interrupt Port Select Low Register
  direction: rw
  address: 0x100
  %%ext: 0-2
    count: 8
    stride: 4
    doc: Select input port for external low interrupt (0-7).
    0: PORT A
    1: PORT B
    2: PORT C
    3: PORT D
    4: PORT E
    5: PORT F

%extipselh: 32
  longname: External Interrupt Port Select high Register
  direction: rw
  address: 0x104
  %%ext: 0-2
    count: 8
    stride: 4
    doc: Select input port for external high interrupt (8-15).
    0: PORT A
    1: PORT B
    2: PORT C
    3: PORT D
    4: PORT E
    5: PORT F

%extirise: 32
  longname: External Interrupt Rising Edge Trigger Register
  direction: rw
  address: 0x108
  %%ext: 0-0
    count: 16
    doc: Set bit n to enable triggering of external interrupt n on rising edge

%extifall: 32
  longname: External Interrupt Falling Edge Trigger Register
  direction: rw
  address: 0x10C
  %%ext: 0-0
    count: 16
    doc: Set bit n to enable triggering of external interrupt n on falling edge.

%ien: 32
  longname: Interrupt Enable Register
  direction: rw
  address: 0x110
  %%ext: 0-0
    count: 16
    doc: Set bit n to enable external interrupt from pin n.

%if: 32
  longname: Interrupt Flag Register
  direction: r
  address: 0x114
  %%ext: 0-0
    count: 16
    doc: Pin n external interrupt flag.

%ifs: 32
  longname: Interrupt Flag Set Register
  direction: w
  address: 0x118
  %%ext: 0-0
    count: 16
    doc: Write bit n to 1 to set interrupt flag n

%ifc: 32
  longname: Interrupt Flag Clear Register
  direction: w
  address: 0x11C
  %%ext: 0-0
    count: 16
    doc: Write bit n to 1 to clear external interrupt flag n.

%route: 32
  longname: I/O routing register
  direction: rw
  address: 0x120
  %%swclkpen: 0-0
    default: 1
    doc: Enable Serial Wire Clock connection to pin. WARNING: When this pin is disabled, the device can no longer be accessed by
    doc: a debugger. A reset will set the pin back to a default state as enabled. If you disable this pin, make sure you have at least
    doc: a 3 second timeout at the start of you program code before you disable the pin. This way, the debugger will have time to halt
    doc: the device after a reset before the pin is disabled.
  %%swdiopen: 1-1
    default: 1
    doc: Enable Serial Wire Data connection to pin. WARNING: When this pin is disabled, the device can no longer be accessed by a
    doc: debugger. A reset will set the pin back to a default state as enabled. If you disable this pin, make sure you have at least
    doc: a 3 second timeout at the start of you program code before you disable the pin. This way, the debugger will have time to 
    doc: halt the device after a reset before the pin is disabled.

%insense: 32
  longname: Input Sense Register
  direction: rw
  address: 0x124
  %%int: 0-0
    doc: Set this bit to enable input sensing for interrupts.
  %%prs: 1-1
    doc: Set this bit to enable input sensing for PRS.

%lock: 32
  longname: Configuration Lock Register
  direction: rw
  address: 0x128
  %%lockkey: 0-15
    doc: Write any other value than the unlock code to lock MODEL, MODEH, CTRL, PINLOCKN, EPISELL, EIPSELH, INSENSE and
    doc: SWDPROUTE from editing. Write the unlock code to unlock. When reading the register, bit 0 is set when the lock is enabled.
    0xA534: UNLOCK
    0: LOCK

%pctrl: 32
  longname: GPIO Control Register
  direction: rw
  address: 0x12C
  %%em4ret: 0-0
    doc: Set to enable EM4 retention of output enable, output value and pull enable.

%cmd: 32
  longname: GPIO Command Register
  direction: w
  address: 0x130
  %%em4wuclr: 0-0
    doc: Write 1 to clear all wake-up requests.

%em4wuen: 32
  longname: EM4 Wake-up Enable Register
  direction: rw
  address: 0x134
  %%em4wuen: 0-5
    doc: Write 1 to enable wake-up request, write 0 to disable wake-up request.
    0x01:A0
    0x04:C9
    0x08:F1
    0x10:F2
    0x20:E13
  
%em4wupol: 32
  longname: EM4 Wake-up Polarity Register
  direction: rw
  address: 0x138
  %%em4wupol: 0-5
    doc: Write bit n to 1 for high wake-up request. Write bit n to 0 for low wake-up request
    0x01:A0
    0x04:C9
    0x08:F1
    0x10:F2
    0x20:E13

%em4wucause: 32
  longname: EM4 Wake-up Cause Register
  direction: r
  address: 0x13C
  %%em4wucause: 0-5
    doc: Bit n indicates which pin the wake-up request occurred.
    0x01:A0
    0x04:C9
    0x08:F1
    0x10:F2
    0x20:E13
