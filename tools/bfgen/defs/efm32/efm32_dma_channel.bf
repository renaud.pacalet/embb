name: dma_channel
longname: Pl230 DMA channel description

bound: 32
align: 32

%src_data_end: 32
  longname: Source data end pointer
  direction: rw
  %%add: 0-31
    doc: Pointer to the end address of the source data
    
%dst_data_end: 32
  longname: Destination data end pointer
  direction: rw
  %%add: 0-31
    doc: Pointer to the end address of the destination data

%cfg: 32
  longname: Control data configuration
  direction: rw
  %%cycle_ctr: 0-2
    doc: The operating mode of the DMA cycle.
    0: STOP
    1: BASIC
    2: AUTOREQUEST
    3: PINGPONG
    4: MEM_SG_PRI
    5: MEM_SG_ALT
    6: PER_SG_PRI
    7: PER_SG_ALT
  %%next_useburst: 3-3
    doc: Controls if the chnl_useburst_set [C] bit is set to a 1, when the controller is performing a
    doc: peripheral scatter-gather and is completing a DMA cycle that uses the alternate data structure
  %%n_minus_1: 4-13
    doc: Prior to the DMA cycle commencing, these bits represent the total number of DMA transfers
    doc: that the DMA cycle contains. You must set these bits according to the size of DMA cycle that
    doc: you require. The 10-bit value indicates the number of DMA transfers, minus one.
  %%r_power: 14-17
    doc: Set these bits to control how many DMA transfers can occur before the controller rearbitrates.
    0: AFTER1
    1: AFTER2
    2: AFTER4
    3: AFTER8
    4: AFTER16
    5: AFTER32
    6: AFTER64
    7: AFTER128
    8: AFTER256
    9: AFTER512
   10: AFTER1024
  %%src_prot_ctrl: 18-20
    doc: Set the bits to control the state of HPROT[3:1] when the controller reads the source data.
  %%dst_prot_ctrl: 21-23
    doc: Set the bits to control the state of HPROT[3:1] when the controller writes the destination.
  %%src_size: 24-25
    doc: Set the bits to match the size of the source data:
    0: BYTE
    1: HALFWORD
    2: WORD
  %%src_inc: 26-27
    doc: Set the bits to control the source address increment. The address increment depends on the
    doc: source data width.
    0: BYTE
    1: HALFWORD
    2: WORD
    3: NOINC
  %%dst_size: 28-29
    doc: Set the bits to match the size of the destonation data:
    0: BYTE
    1: HALFWORD
    2: WORD
  %%dst_inc: 30-31
    doc: Set the bits to control the destination address increment. The address increment depends on the
    doc: destination data width.
    0: BYTE
    1: HALFWORD
    2: WORD
    3: NOINC

%unused: 32



