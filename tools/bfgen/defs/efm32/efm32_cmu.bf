name: efm32_cmu
longname: EFM32 Clock Management Unit

bound: 32
align: 32

required_regs: hfcoreclken0 hfperclken0 lfaclken0 lfbclken0 lfapresc0 lfbpresc0

%ctrl: 32
  longname: Control Register
  direction: rw
  address: 0x000
  %%hfxomode: 0-1
    doc: Set this to configure the external source for the HFXO. The oscillator setting takes effect when 1 is written to HFXOEN in
    doc: CMU_OSCENCMD. The oscillator setting is reset to default when 1 is written to HFXODIS in CMU_OSCENCMD.
    0: XTAL
    1: BUFEXTCLK
    2: DIGEEXTCLK
  %%hfxoboost: 2-3
    doc: Used to adjust start-up boost current for HFXO.
    0: 50PCENT
    1: 70PCENT
    2: 80PCENT
    3: 100PCENT
  %%hfxobufcur: 5-6
    doc: This value has been set during calibration and should not be changed.
  %%hfxoglitchdeten: 7-7
    doc: This bit enables the glitch detector which is active as long as the start-up ripple-counter is counting. A detected glitch will reset the
    doc: ripple-counter effectively increasing the start-up time. Once the ripple-counter has timed-out, glitches will not be detected.
  %%hfxotimeout: 9-10
    doc: Configures the start-up delay for HFXO.
    0: 8
    1: 256
    2: 1K
    3: 16K
  %%lxomode: 11-12
    doc: Set this to configure the external source for the LFXO. The oscillator setting takes effect when 1 is written to LFXOEN in
    doc: CMU_OSCENCMD. The oscillator setting is reset to default when 1 is written to LFXODIS in CMU_OSCENCMD.
    0: XTAL
    1: BUFEXTCLK 
    2: DIGEXTCLK 
  %%lfxoboost: 13-13
    doc: Adjusts start-up boost current for LFXO.
    0: 70PCENT
    1: 100PCENT
  %%hfclkdiv: 14-16
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: Use to divide HFCLK frequency by (hfclkdiv + 1)
  %%lfxobufcur: 17-17
    doc: This value has been updated to the correct level during calibration and should not be changed.
  %%lfxotimeout: 18-19
    doc: Configures the start-up delay for LFXO.
    0: 8
    1: 1K
    2: 16K
    3: 32K
  %%clkoutsel0: 20-22
    doc: Controls the clock output multiplexer. To actually output on the pin, set CLKOUT0PEN in CMU_ROUTE.
    0:HFRCO
    1:HFX0
    2:HFCLK2
    3:HFCLK4
    4:HFCLK8
    5:HFCLK16
    6:ULFRCO
    7:AUXHFRC0
  %%clkoutsel1: 23-23
    doc: Controls the clock output multiplexer. To actually output on the pin, set CLKOUT1PEN in CMU_ROUTE.
    condition: defined(CONFIG_EFM32_GECKO)
    0:LFRCO
    1:LFX0
  %%clkoutsel1: 23-25
    doc: Controls the clock output multiplexer. To actually output on the pin, set CLKOUT1PEN in CMU_ROUTE.
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO) || defined(CONFIG_EFM32_ZERO_GECKO)
    0:LFRCO
    1:LFX0
    2:HFCLK
    3:LFXOQ
    4:HFXOQ
    5:LFRCOQ
    6:HFRCOQ
    7:AUXHFCOQ
  %%dbgclk: 28-28
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: Select clock used for the debug system
    0: AUXHFRCO
    1: HFCLK
  %%hfle: 30-30
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: Set to allow access to LE peripheral when running at frequencis higher than 24Mhz.

%hfcoreclkdiv: 32
  longname: high frequency core clock division register
  direction: rw
  address: 0x004
  %%hfcoreclkdiv: 0-3
    doc: Specifies the clock divider for the HFPERCLK. 
    0: HFCLK
    1: HFCLK2
    2: HFCLK4
    3: HFCLK8
    4: HFCLK16
    5: HFCLK32
    6: HFCLK64
    7: HFCLK128
    8: HFCLK256
    9: HFCLK512
  %%hfperclken: 8-8
    doc: Set to enable the HFPERCLK. 

%hfperclkdiv: 32
  longname: High Frequency Peripheral Clock Division Register
  direction: rw
  address: 0x008
  %%hfcoreclkdiv: 0-3
    doc: Specifies the clock divider for the HFPERCLK. 
    0: HFCLK
    1: HFCLK2
    2: HFCLK4
    3: HFCLK8
    4: HFCLK16
    5: HFCLK32
    6: HFCLK64
    7: HFCLK128
    8: HFCLK256
    9: HFCLK512
  %%hfperclken: 8-8
    doc: Set to enable the HFPERCLK. 

%hfrcoctrl: 32
  longname: HFRCO Control Register
  direction: rw
  address: 0x00c
  %%tunning: 0-7
    doc: Writing this field adjusts the HFRCO frequency (the higher value, the higher frequency). This field is updated with the production
    doc: calibrated value for the 14 MHz band during reset, and the reset value might therefore vary between devices.
  %%band: 8-10
    doc: Write this field to set the frequency band in which the HFRCO is to operate. When changing this setting there will be no glitches on
    doc: the HFRCO output, hence it is safe to change this setting even while the system is running on the HFRCO. To ensure an accurate
    doc: frequency, the HFTUNING value should also be written when changing the frequency band. The calibrated tuning value for the
    doc: different bands can be read from the Device Information page.
    0: 1MHZ
    1: 7MHZ
    2: 11MHZ
    3: 14MHZ
    4: 21MHZ
    5: 28MHZ : not available on all devices
  %%sudelay: 12-16
    doc: Always write this field to 0.

%lfrcoctrl: 32
  longname: LFRCO Control Register
  direction: rw
  address: 0x010
  %%tunning: 0-6
    doc: Writing this field adjusts the LFRCO frequency (the higher value, the higher frequency). This field is updated with the production
    doc: calibrated value during reset, and the reset value might therefore vary between devices.

%auxhfrcoctrl: 32
  longname: HFRCO Control Register
  direction: rw
  address: 0x014
  %%tunning: 0-7
    doc: Writing this field adjusts the AUXHFRCO frequency (the higher value, the higher frequency). This field is updated with the production
    doc: calibrated value during reset, and the reset value might therefore vary between devices.
  %%band: 8-10
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO) || defined(CONFIG_EFM32_ZERO_GECKO)
    doc: Write this field to set the frequency band in which the HFRCO is to operate. When changing this setting there will be no glitches on
    doc: the AUXHFRCO output, hence it is safe to change this setting even while the system is running on the AUXHFRCO. To ensure an accurate
    doc: frequency, the AUXTUNING value should also be written when changing the frequency band. The calibrated tuning value for the
    doc: different bands can be read from the Device Information page. Flash erase and write use this clock. If it is changed to another
    doc: value than the default, MSC_TIMEBASE must also be configured to ensure correct flash erase and write operation.
    0: 14MHZ
    1: 11MHZ
    2: 7MHZ
    3: 1MHZ
    6: 28MHZ : not available on all devices
    7: 21MHZ

%calctrl: 32
  longname: Calibration Up-counter Select
  direction: rw
  address: 0x018
  %%upsel: 0-2
    doc: Selects clock source for the calibration up-counter. 
    0: HFXO
    1: LFXO
    2: HFRCO
    3: LFRCO
    4: AUXHFRCO
  %%downsel: 3-5
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO) || defined(CONFIG_EFM32_ZERO_GECKO)
    doc: Selects clock source for the calibration down-counter.
    0: HFCLK
    1: HFXO
    2: LFXO
    3: HFRCO
    4: LFRCO
    5: AUXHFRCO
  %%cont: 6-6
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO) || defined(CONFIG_EFM32_ZERO_GECKO)
    doc: Set this bit to enable continuous calibration.

%calcnt: 32
  longname: Calibration Counter Register
  direction: rw
  address: 0x01c
  %%calcnt: 0-19
    doc: Write top value before calibration. Read calibration result from this register when Calibration Ready flag has been set.

%oscencmd: 32
  longname: Oscillator Enable/Disable Command Register
  direction: w
  address: 0x020
  %%hfrcoen: 0-0
    doc: Enables the HFRCO.
  %%hfrcodis: 1-1
    doc: Disables the HFRCO. HFRCOEN has higher priority if written simultaneously. WARNING: Do not disable the HFRCO if this oscillator
    doc: is selected as the source for HFCLK.
  %%hfxoen: 2-2
    doc: Enables the HFXO.
  %%hfxodis: 3-3
    doc: Disables the HFXO. HFXOEN has higher priority if written simultaneously. WARNING: Do not disable the HFRXO if this oscillator
    doc: is selected as the source for HFCLK.
  %%auxhfrcoen: 4-4
    doc: Enables the AUXHFRCO.
  %%auxhfrcodis: 5-5
    doc: Disables the AUXHFRCO. AUXHFRCOEN has higher priority if written simultaneously. WARNING: Do not disable this clock during
    doc: a flash erase/write operation.
  %%lfrcoen: 6-6
    doc: Enables the LFRCO.
  %%lfrcodis: 7-7
    doc: Disables the LFRCO. LFRCOEN has higher priority if written simultaneously.
  %%lfxoen: 8-8
    doc: Enables the LFXOEN.
  %%lfxodis: 9-9
    doc: Disables the LFXO. LFXOEN has higher priority if written simultaneously.

%cmd: 32
  longname: Command Register
  direction: w
  address: 0x024
  %%hfclksel: 0-2
    doc: Selects the clock source for HFCLK. Note that selecting an oscillator that is disabled will cause the system clock to stop. Check the
    doc: status register and confirm that oscillator is ready before switching.
    1: HFRCO
    2: HFXO
    3: LFRCO
    4: LFXO
  %%calstart: 3-3
    doc: Starts the calibration, effectively loading the CMU_CALCNT into the down-counter and start decrementing.
  %%calstop: 4-4
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO) || defined(CONFIG_EFM32_ZERO_GECKO)
    doc: Stops the calibration counters.
  %%usbcclksel: 5-6
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: Selects the clock for HFCORECLKUSBC. Thes tatus register is updated chen the clock switch has taken effect.
    1: HFCLKNODIV
    2: LFXO
    3: LFRCO

%lfclksel: 32
  longname: Low Frequency Clock Select Register
  direction: rw
  address: 0x028
  %%lfa: 0-1
    doc: Selects the clock source for LFACLK. Tied to LFAE.
    0: DISABLED_OR_ULFRCO
    1: LFRCO
    2: LFXO
    3: HFCORECLKLEDIV2 
  %%lfb: 2-3
    doc: Selects the clock source for LFBCLK. Tied to LFBE.
    0: DISABLED_OR_ULFRCO
    1: LFRCO
    2: LFXO
    3: HFCORECLKLEDIV2 
  %%lfae: 16-16
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO) || defined(CONFIG_EFM32_ZERO_GECKO)
    doc: This bit redefines the meaning of the LFA field.
    0: DISABLED
    1: ULFRCO
  %%lfbe: 20-20
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO) || defined(CONFIG_EFM32_ZERO_GECKO)
    doc: This bit redefines the meaning of the LFB field.
    0: DISABLED
    1: ULFRCO

%status: 32
  longname: Status Register
  direction: r
  address: 0x02c
  %%hfrcoens: 0-0
    doc: HFRCO is enabled.
  %%hfrcordy: 1-1
    doc: HFRCO is enabled and start-up time has exceeded.
  %%hfxoens: 2-2
    doc: HFXO is enabled.
  %%hfxordy: 3-3
    doc: HFXO is enabled and start-up time has exceeded.
  %%auxhfrcoens: 4-4
    doc: AUXHFRCO is enabled.
  %%auxhfrcordy: 5-5
    doc: AUXHFRCO is enabled and start-up time has exceeded.
  %%lfrcoens: 6-6
    doc: LFRCO is enabled.
  %%lfrcordy: 7-7
    doc: LFRCO is enabled and start-up time has exceeded.
  %%lfxoens: 8-8
    doc: LFXO is enabled.
  %%lfxordy: 9-9
    doc: LFXO is enabled and start-up time has exceeded.
  %%HFRCOSEL: 10-10
    doc: HFRCO is selected as HFCLK clock source.
  %%HFXOSEL: 11-11
    doc: HFXO is selected as HFCLK clock source.
  %%LFRCOSEL: 12-12
    doc: LFRCO is selected as HFCLK clock source.
  %%LFXOSEL: 13-13
    doc: LFXO is selected as HFCLK clock source.
  %%CALBSY: 14-14
    doc: Calibration is on-going.
  %%usbchfclksel: 15-15
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: HFCLK is selected (and active) as HFCORECLKUSBC
  %%usbclfxosel: 16-16
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: LFXO is selected (and active) as HFCORECLKUSBC
  %%usbclfrcosel: 17-17
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: LFRCO is selected (and active) as HFCORECLKUSBC

%if: 32
  longname: Interrupt Flag Register
  direction: r
  address: 0x030
  %%HFRCORDY: 0-0
    doc: Set when HFRCO is ready (start-up time exceeded).
  %%HFXORDY: 1-1
    doc: Set when HFXO is ready (start-up time exceeded).
  %%LFRCORDY: 2-2
    doc: Set when LFRCO is ready (start-up time exceeded).
  %%LFXORDY: 3-3
    doc: Set when LFXO is ready (start-up time exceeded).
  %%AUXHFRCORDY: 4-4
    doc: Set when AUXHFRCO is ready (start-up time exceeded).
  %%CALRDY: 5-5
    doc: Set when calibration is completed.
  %%CALOF: 6-6
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO) || defined(CONFIG_EFM32_ZERO_GECKO)
    doc: Set when calibration overflow has occurred
  %%usbchfclksel: 7-7
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: set when HFCLK is selected as HFCORECLKUSBC

%ifs: 32
  longname: Interrupt Flag Set Register
  direction: w
  address: 0x034
  %%HFRCORDY: 0-0
    doc: Write to 1 to set the HFRCO Ready Interrupt Flag.
  %%HFXORDY: 1-1
    doc: Write to 1 to set the HFXO Ready Interrupt Flag.
  %%LFRCORDY: 2-2
    doc: Write to 1 to set the LFRCO Ready Interrupt Flag.
  %%LFXORDY: 3-3
    doc: Write to 1 to set the LFXO Ready Interrupt Flag.
  %%AUXHFRCORDY: 4-4
    doc: Write to 1 to set the AUXHFRCO Ready Interrupt Flag.
  %%CALRDY: 5-5
    doc: Write to 1 to set the Calibration Ready(completed) Interrupt Flag.
  %%CALOF: 6-6
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO) || defined(CONFIG_EFM32_ZERO_GECKO)
    doc: Write to 1 to set the Calibration Overflow Interrupt Flag.
  %%usbchfclksel: 7-7
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: set when HFCLK is selected as HFCORECLKUSBC

%ifc: 32
  longname: Interrupt Flag Clear Register
  direction: w
  address: 0x038
  %%HFRCORDY: 0-0
    doc: Write to 1 to clear the HFRCO Ready Interrupt Flag.
  %%HFXORDY: 1-1
    doc: Write to 1 to clear the HFXO Ready Interrupt Flag.
  %%LFRCORDY: 2-2
    doc: Write to 1 to clear the LFRCO Ready Interrupt Flag.
  %%LFXORDY: 3-3
    doc: Write to 1 to clear the LFXO Ready Interrupt Flag.
  %%AUXHFRCORDY: 4-4
    doc: Write to 1 to clear the AUXHFRCO Ready Interrupt Flag.
  %%CALRDY: 5-5
    doc: Write to 1 to clear the Calibration Ready(completed) Interrupt Flag.
  %%CALOF: 6-6
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO) || defined(CONFIG_EFM32_ZERO_GECKO)
    doc: Write to 1 to clear the Calibration Overflow Interrupt Flag.
  %%usbchfclksel: 7-7
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: set when HFCLK is selected as HFCORECLKUSBC

%ien: 32
  longname: Interrupt Flag Enable Register
  direction: rw
  address: 0x03c
  %%HFRCORDY: 0-0
    doc: Set to enable the HFRCO Ready Interrupt.
  %%HFXORDY: 1-1
    doc: Set to enable the HFXO Ready Interrupt.
  %%LFRCORDY: 2-2
    doc: Set to enable the LFRCO Ready Interrupt.
  %%LFXORDY: 3-3
    doc: Set to enable the LFXO Ready Interrupt.
  %%AUXHFRCORDY: 4-4
    doc: Set to enable the AUXHFRCO Ready Interrupt.
  %%CALRDY: 5-5
    doc: Set to enable the Calibration Ready(completed) Interrupt.
  %%CALOF: 6-6
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO) || defined(CONFIG_EFM32_ZERO_GECKO)
    doc: Set to enable the Calibration Overflow Interrupt.
  %%usbchfclksel: 7-7
    condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: set when HFCLK is selected as HFCORECLKUSBC

%hfcoreclken0:32
  condition: defined(CONFIG_EFM32_GECKO)
  longname: High Frequency Core Clock Enable Register 0
  direction: rw
  address: 0x040
  %%aes: 0-0
    doc: Set to enable the clock for AES.
  %%dma: 1-1
    doc: Set to enable the clock for DMA.
  %%le: 2-2
    doc: Set to enable the clock for LE. Interface used for bus access to Low Energy peripherals.
  %%ebi: 3-3
    doc: Set to enable the clock for EBI.

%hfcoreclken0:32
  condition: defined(CONFIG_EFM32_ZERO_GECKO)
  longname: High Frequency Core Clock Enable Register 0
  direction: rw
  address: 0x040
  %%aes: 0-0
    doc: Set to enable the clock for AES.
  %%dma: 1-1
    doc: Set to enable the clock for DMA.
  %%le: 2-2
    doc: Set to enable the clock for LE. Interface used for bus access to Low Energy peripherals.

%hfcoreclken0:32
  condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
  longname: High Frequency Core Clock Enable Register 0
  direction: rw
  address: 0x040
  %%dma: 0-0
    doc: Set to enable the clock for DMA.
  %%aes: 1-1
    doc: Set to enable the clock for AES.
  %%usbc: 2-2
    doc: Set to enable the clock for USBC.
  %%usb: 3-3
    doc: Set to enable the clock for USB.
  %%le: 4-4
    doc: Set to enable the clock for LE. Interface used for bus access to Low Energy peripherals.
  %%ebi: 5-5
    doc: Set to enable the clock for EBI.

%hfperclken0:32
  condition: defined(CONFIG_EFM32_GECKO)
  longname: High Frequency Peripheral Clock Enable Register 0
  direction: rw
  address: 0x044
  %%usart0: 0-0
    doc: Set to enable the clock for USART1.
  %%usart1: 1-1
    doc: Set to enable the clock for USART1.
  %%usart2: 2-2
    doc: Set to enable the clock for USART1.
  %%uart0: 3-3
    doc: Set to enable the clock for USART1.
  %%timer0: 4-4
    doc: Set to enable the clock for TIMER0.
  %%timer1: 5-5
    doc: Set to enable the clock for TIMER1.
  %%timer2: 6-6
    doc: Set to enable the clock for TIMER1.
  %%acmp0: 7-7
    doc: Set to enable the clock for ACMP0.
  %%acmp1: 8-8
    doc: Set to enable the clock for ACMP0.
  %%prs: 10-10
    doc: Set to enable the clock for PRS.
  %%dac0: 11-11
    doc: Set to enable the clock for DAC0.
  %%gpio: 12-12
    doc: Set to enable the clock for GPIO.
  %%vcmp: 13-13
    doc: Set to enable the clock for VCMP.
  %%adc0: 14-14
    doc: Set to enable the clock for ADC0.
  %%i2c0: 15-15
    doc: Set to enable the clock for I2C0.

%hfperclken0:32
  condition: defined(CONFIG_EFM32_ZERO_GECKO)
  longname: High Frequency Peripheral Clock Enable Register 0
  direction: rw
  address: 0x044
  %%timer0: 0-0
    doc: Set to enable the clock for TIMER0.
  %%timer1: 1-1
    doc: Set to enable the clock for TIMER1.
  %%acmp0: 2-2
    doc: Set to enable the clock for ACMP0.
  %%usart1: 3-3
    doc: Set to enable the clock for USART1.
  %%prs: 4-4
    doc: Set to enable the clock for PRS.
  %%idac0: 6-6
    doc: Set to enable the clock for IDAC0.
  %%gpio: 7-7
    doc: Set to enable the clock for GPIO.
  %%vcmp: 8-8
    doc: Set to enable the clock for VCMP.
  %%ADC0: 10-10
    doc: Set to enable the clock for ADC0.
  %%i2c0: 11-11
    doc: Set to enable the clock for I2C0.

%hfperclken0:32
  condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
  longname: High Frequency Peripheral Clock Enable Register 0
  direction: rw
  address: 0x044
  %%usart0: 0-0
    doc: Set to enable the clock for USART0.
  %%usart1: 1-1
    doc: Set to enable the clock for USART1.
  %%usart2: 2-2
    doc: Set to enable the clock for USART2.
  %%uart0: 3-3
    doc: Set to enable the clock for UART0.
  %%uart1: 4-4
    doc: Set to enable the clock for UART1.
  %%timer0: 5-5
    doc: Set to enable the clock for timer0.
  %%timer1: 6-6
    doc: Set to enable the clock for timer1.
  %%timer2: 7-7
    doc: Set to enable the clock for timer2.
  %%timer3: 8-8
    doc: Set to enable the clock for timer3.
  %%acmp0: 9-9
    doc: Set to enable the clock for ACMP0.
  %%acmp1: 10-10
    doc: Set to enable the clock for ACMP0.
  %%i2c0: 11-11
    doc: Set to enable the clock for I2C0.
  %%i2c1: 12-12
    doc: Set to enable the clock for I2C1.
  %%gpio: 13-13
    doc: Set to enable the clock for GPIO.
  %%vcmp: 14-14
    doc: Set to enable the clock for VCMP.
  %%prs: 15-15
    doc: Set to enable the clock for PRS.
  %%adc0: 16-16
    doc: Set to enable the clock for ADC0.
  %%dac0: 17-17
    doc: Set to enable the clock for DAC0.

%syncbusy:32
  longname: Synchronization Busy Register
  direction: r
  address: 0x050
  %%lfaclken0: 0-0
    doc: Used to check the synchronization status of CMU_LFACLKEN0.
    0: READY
    1: BUSY
  %%lfapresc0: 2-2
    doc: Used to check the synchronization status of CMU_LFAPRESC0.
    0: READY
    1: BUSY
  %%lfbclken0: 4-4
    doc: Used to check the synchronization status of CMU_LFBCLKEN0.
    0: READY
    1: BUSY
  %%lfbpresc0: 5-5
    doc: Used to check the synchronization status of CMU_LFBPRESC0.
    0: READY
    1: BUSY

%freeze:32
  longname: Synchronization Busy Register
  direction: rw
  address: 0x054
  %%regfreeze: 0-0
    doc: When set, the update of the Low Frequency clock control registers is postponed until this bit is cleared. Use this bit to update several
    doc: registers simultaneously.
    0: UPDATE
    1: FREEZE

%lfaclken0:32
  condition: defined(CONFIG_EFM32_GECKO)
  longname: Low Frequency A Clock Enable Register 0 (Async Reg)
  direction: rw
  address: 0x058
  %%rtc: 0-0
    doc: Set to enable the clock for RTC.
  %%letimer0: 1-1
    doc: Set to enable the clock for LETIMER0.
  %%lcd: 2-2
    doc: Set to enable the clock for LCD.

%lfaclken0:32
  condition: defined(CONFIG_EFM32_ZERO_GECKO)
  longname: Low Frequency A Clock Enable Register 0 (Async Reg)
  direction: rw
  address: 0x058
  %%rtc: 0-0
    doc: Set to enable the clock for RTC.

%lfaclken0:32
  condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
  longname: Low Frequency A Clock Enable Register 0 (Async Reg)
  direction: rw
  address: 0x058
  %%lesense: 0-0
    doc: Set to enable the clock for LESENSE.
  %%rtc: 1-1
    doc: Set to enable the clock for RTC.
  %%letimer0: 2-2
    doc: Set to enable the clock for LETIMER0.
  %%lcd: 3-3
    doc: Set to enable the clock for LCD.

%lfbclken0:32
  condition: defined(CONFIG_EFM32_ZERO_GECKO)
  longname: Low Frequency B Clock Enable Register 0 (Async Reg)
  address: 0x060
  direction: rw
  %%leuart0: 0-0
    doc: Set to enable the clock for LEUART0.

%lfbclken0:32
  condition: defined(CONFIG_EFM32_GECKO) || defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
  longname: Low Frequency B Clock Enable Register 0 (Async Reg)
  address: 0x060
  direction: rw
  %%leuart0: 0-0
    doc: Set to enable the clock for LEUART0.
  %%leuart1: 1-1
    doc: Set to enable the clock for LEUART1.

%lfapresc0: 32
  condition: defined(CONFIG_EFM32_GECKO)
  longname: Low Frequency A Prescaler Register 0 (Async Reg)
  direction: rw
  address: 0x68
  %%rtc: 0-3
    doc: Configure Real-Time Counter prescaler
    0: DIV1
    1: DIV2
    2: DIV4
    3: DIV8
    4: DIV16
    5: DIV32
    6: DIV64
    7: DIV128
    8: DIV256
    9: DIV512
   10: DIV1024
   11: DIV2048
   12: DIV4096
   13: DIV8192
   14: DIV16384
   15: DIV32768
  %%letimer0: 4-7
    doc: Configure Real-Time Counter prescaler
    0: DIV1
    1: DIV2
    2: DIV4
    3: DIV8
    4: DIV16
    5: DIV32
    6: DIV64
    7: DIV128
    8: DIV256
    9: DIV512
   10: DIV1024
   11: DIV2048
   12: DIV4096
   13: DIV8192
   14: DIV16384
   15: DIV32768
  %%lcd: 8-9
    doc: Configure LCD controller prescaler
    0: DIV16
    1: DIV32
    2: DIV64
    3: DIV128

%lfapresc0: 32
  condition: defined(CONFIG_EFM32_ZERO_GECKO)
  longname: Low Frequency A Prescaler Register 0 (Async Reg)
  direction: rw
  address: 0x68
  %%rtc: 0-3
    doc: Configure Real-Time Counter prescaler
    0: DIV1
    1: DIV2
    2: DIV4
    3: DIV8
    4: DIV16
    5: DIV32
    6: DIV64
    7: DIV128
    8: DIV256
    9: DIV512
   10: DIV1024
   11: DIV2048
   12: DIV4096
   13: DIV8192
   14: DIV16384
   15: DIV32768

%lfapresc0: 32
  condition: defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
  longname: Low Frequency A Prescaler Register 0 (Async Reg)
  direction: rw
  address: 0x68
  %%lesense: 0-1
    doc: Configure Low Energy Interface prescaler
    0: DIV1
    1: DIV2
    2: DIV4
    3: DIV8
  %%rtc: 4-7
    doc: Configure Real-Time Counter prescaler
    0: DIV1
    1: DIV2
    2: DIV4
    3: DIV8
    4: DIV16
    5: DIV32
    6: DIV64
    7: DIV128
    8: DIV256
    9: DIV512
   10: DIV1024
   11: DIV2048
   12: DIV4096
   13: DIV8192
   14: DIV16384
   15: DIV32768
  %%letimer0: 8-11
    doc: Configure Real-Time Counter prescaler
    0: DIV1
    1: DIV2
    2: DIV4
    3: DIV8
    4: DIV16
    5: DIV32
    6: DIV64
    7: DIV128
    8: DIV256
    9: DIV512
   10: DIV1024
   11: DIV2048
   12: DIV4096
   13: DIV8192
   14: DIV16384
   15: DIV32768
  %%lcd: 12-14
    doc: Configure LCD controller prescaler
    0: DIV16
    1: DIV32
    2: DIV64
    3: DIV128

%lfbpresc0: 32
  condition: defined(CONFIG_EFM32_ZERO_GECKO)
  longname: Low Frequency B Prescaler Register 0 (Async Reg)
  direction: rw
  address: 0x70
  %%leuart0: 0-1
    doc: Configure LEUART0 prescaler
    0: DIV1
    1: DIV2
    2: DIV4
    3: DIV8

%lfbpresc0: 32
  condition: defined(CONFIG_EFM32_GECKO) || defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
  longname: Low Frequency B Prescaler Register 0 (Async Reg)
  direction: rw
  address: 0x70
  %%leuart0: 0-1
    doc: Configure LEUART0 prescaler
    0: DIV1
    1: DIV2
    2: DIV4
    3: DIV8
  %%leuart1: 4-5
    doc: Configure LEUART1 prescaler
    0: DIV1
    1: DIV2
    2: DIV4
    3: DIV8

%pcntctrl: 32
  longname: PCNT Control Register
  direction: rw
  address: 0x78
  %%pcnt0clken: 0-0
    doc: This bit enables/disables the clock to the PCNT.
  %%pcnt0clksel: 1-1
    doc: This bit controls which clock that is used for the PCNT.
    0: LFACLK
    1: PCNT0S0
  %%pcnt1clken: 2-2
    condition: defined(CONFIG_EFM32_GECKO) || defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: This bit enables/disables the clock to the PCNT.
  %%pcnt1clksel: 3-3
    condition: defined(CONFIG_EFM32_GECKO) || defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: This bit controls which clock that is used for the PCNT.
    0: LFACLK
    1: PCNT1S0
  %%pcnt2clken: 4-4
    condition: defined(CONFIG_EFM32_GECKO) || defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: This bit enables/disables the clock to the PCNT.
  %%pcnt2clksel: 5-5
    condition: defined(CONFIG_EFM32_GECKO) || defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: This bit controls which clock that is used for the PCNT.
    0: LFACLK
    1: PCNT2S0

%lcdctrl: 32
  condition: defined(CONFIG_EFM32_GECKO) || defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
  longname: LCD Control register
  direction: rw
  address: 0x7c
  %%fdiv: 0-2
    doc: These bits controls the framerate according to this formula:
    doc: LFACLKLCD = LFACLKLCDpre / (1 + FDIV). Do not change this value while
    doc: the LCD bit in LFACLKEN0 is set to 1.
  %%vboosten: 3-3
    doc: This bit enables/disables the VBOOST function.
  %%vbfdiv: 4-6
    doc: These bits control the voltage boost update frequency division.
    doc: Voltage Boost update Frequency = LFACLK / DIV.
    0: DIV1
    1: DIV2
    2: DIV4
    3: DIV8
    4: DIV16
    5: DIV32

%route: 32
  longname: I/O Routing Register
  direction: rw
  address: 0x80
  %%clkout0pen: 0-0
    doc: When set, the CLKOUT0 pin is enabled.
  %%clkout1pen: 1-1
    doc: When set, the CLKOUT1 pin is enabled.
  %%location: 2-2
    condition: defined(CONFIG_EFM32_GECKO)
    doc: Decides the location of the CMU I/O pins.
    0: LOC0
  %%location: 2-4
    condition: defined(CONFIG_EFM32_ZERO_GECKO) || defined(CONFIG_EFM32_LEOPARD_GECKO) || defined(CONFIG_EFM32_WONDER_GECKO)
    doc: Decides the location of the CMU I/O pins.
    0: LOC0
    1: LOC1
    2: LOC2

%lock: 32
  longname: I/O Routing Register
  direction: rw
  address: 0x084
  %%lockkey: 0-15
    doc: Write any other value than the unlock code to lock CMU_CTRL, CMU_HFCORECLKDIV,
    doc: CMU_HFPERCLKDIV, CMU_HFRCOCTRL, CMU_LFRCOCTRL, CMU_AUXHFRCOCTRL, CMU_OSCENCMD, CMU_CMD,
    doc: CMU_LFCLKSEL, CMU_HFCORECLKEN0, CMU_HFPERCLKEN0, CMU_LFACLKEN0, CMU_LFBCLKEN0, CMU_LFAPRESC0,
    doc: CMU_LFBPRESC0, and CMU_PCNTCTRL from editing. Write the unlock code to unlock. When reading the register, bit 0 is set
    doc: when the lock is enabled.
    0: UNLOCKED
    1: LOCKED
    0x580E: UNLOCK
 

