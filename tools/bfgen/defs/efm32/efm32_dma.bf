name: efm32_dma
longname: EFM32 DMA

bound: 32
align: 32

%status: 32
  longname: DMA Status Register
  direction: r
  %%en: 0-0
    doc: When this bit is 1, the DMA is enabled.
  %%state: 4-7
    doc: State can be one of the following. Higher values (11-15) are undefined.
    0: IDLE
    1: RDCHCTRLDATA
    2: RDSRCENDPTR
    3: RDDSTENDPTR
    4: RDSRCDATA
    5: WRDSTDATA
    6: WAITREQCLR
    7: WRCHCTRLDATA
    8: STALLED
    9: DONE
   10: PERSCATTRANS
  %%chnum: 16-20
    doc: Number of available DMA channels minus one.
    default: 0x3

%config: 32
  longname: DMA Configuration Register
  direction: w
  %%en: 0-0
    doc: Set this bit to enable the DMA controller.
  %%chprot: 5-5
    doc: Control whether accesses done by the DMA controller are privileged or not. When CHPROT = 1 then HPROT is HIGH and the access
    doc: is privileged. When CHPROT = 0 then HPROT is LOW and the access is non-privileged.

%ctrlbase: 32
  longname: Channel Control Data Base Pointer Register
  direction: rw
  %%base: 0-31
    doc: The base pointer for a location in system memory that holds the channel control data structure. This register must be written to point
    doc: to a location in system memory with the channel control data structure before the DMA can be used. Note that ctrl_base_ptr[8:0]
    doc: must be 0.

%altctrlbase: 32
  longname: Channel Alternate Control Data Base Pointer Register
  direction: r
  %%base: 0-31
    default: 0x40
    doc: The base address of the alternate data structure. This register will read as DMA_CTRLBASE + 0x40.
 
%chwaitstatus: 32
  longname: Channel Wait on Request Status Register
  direction: r
  %%ch: 0-0
    count: 12
    default: 1
    doc: Status for wait on request for channel n.

%chswreq: 32
  longname: Channel Software Request Register
  direction: w
  %%ch: 0-0
    count: 12
    default: 1
    doc: Write 1 to this bit to generate a DMA request for this channel.

%chusebursts: 32
  longname: Channel Useburst Set Register
  direction: rw
  %%ch: 0-0
    count: 12
    doc: Write to 1 to enable the useburst setting for this channel. Reading returns the useburst status. After the penultimate 2^R transfer
    doc: completes, if the number of remaining transfers, N, is less than 2^R then the controller resets the chnl_useburst_set bit to 0.
    doc: This enables you to complete the remaining transfers using dma_req[] or dma_sreq[]. In peripheral scatter-gather mode, if the
    doc: next_useburst bit is set in channel_cfg then the controller sets the chnl_useburst_set[C] bit to a 1, when it completes the DMA cycle
    doc: that uses the alternate data structure.

%chuseburstc: 32
  longname: Channel Useburst Clear Register
  direction: w
  %%ch: 0-0
    count: 12
    doc: Write to 1 to disable useburst setting for this channel.

%chreqmasks: 32
  longname: Channel Request Mask Set Register
  direction: rw
  %%ch: 0-0
    count: 12
    doc: Write to 1 to disable peripheral requests for this channel.

%chreqmaskc: 32
  longname: Channel Request Mask Clear Register
  direction: rw
  %%ch: 0-0
    count: 12
    doc: Write to 1 to enable peripheral requests for this channel.

%chens: 32
  longname: Channel Enable Set Register
  direction: rw
  %%ch: 0-0
    count: 12
    doc: Write to 1 to enable this channel. Reading returns the enable status of the channel.

%chenc: 32
  longname: Channel Enable Clear Register
  direction: w
  count: 12
  %%ch: 0-31
    doc: Write to 1 to disable this channel. Note that the controller disables a channel, by setting the appropriate bit, when either it completes
    doc: the DMA cycle, or it reads a channel_cfg memory location which has cycle_ctrl = b000, or an ERROR occurs on the AHB-Lite bus.
    doc: A read from this field returns the value of CH0ENS from the DMA_CHENS register.

%chalts: 32
  longname: Channel Alernate Set Register
  direction: rw
  %%ch: 0-0
    count: 12 
    doc: Write to 1 to select the alternate structure for this channel.

%chaltc: 32
  longname: Channel Alernate Clear Register
  direction: rw
  %%ch: 0-0
    count: 12
    doc: Write to 1 to select the Primary structure for this channel.

%chpris: 32
  longname: Channel Priority Set Register
  direction: rw
  %%ch: 0-0
    count: 12
    doc: Write to 1 to obtain high priority for this channel. Reading returns the channel priority status.

%chpric: 32
  longname: Channel Priority Clear Register
  direction: w
  %%ch: 0-0
    count: 12
    doc: Write to 1 to clear high priority for this channel.

%errorc: 32
  longname: Bus Error Clear Register
  direction: rw
  address: 0x4c
  %%errorc: 0-0
    doc: This bit is set high if an AHB bus error has occurred. Writing a 1 to this bit will clear the bit. If the error is deasserted at the same time
    doc: as an error occurs on the bus, the error condition takes precedence and ERRORC remains asserted.

%chreqstatus: 32
  longname: Channel Request Status
  direction: r
  address: 0xE10
  %%ch: 0-0
    count: 12
    doc: When this bit is 1, it indicates that the peripheral connected as the input to this DMA channel is requesting the controller to service
    doc: the DMA channel. The controller services the request by performing the DMA cycle using 2^R DMA transfers.

%chsreqstatus: 32
  longname: Channel Single Request Status
  direction: r
  %%ch: 0-0
    count: 12
    doc: When this bit is 1, it indicates that the peripheral connected as the input to this DMA channel is requesting the controller to service
    doc: the DMA channel. The controller services the request by performing the DMA cycle using single DMA transfers. 

%if: 32
  longname: Interrupt Flag Register
  direction: r
  address: 0x1000
  %%done: 0-0
    count: 12
    doc: Set when the DMA channel has completed its transfer. If the channel is disabled, the flag is set when there is a request for the channel.
  %%err: 31-31
    doc: This flag is set when an error has occurred on the AHB bus.

%ifs: 32
  longname: Interrupt Flag Set Register
  direction: w
  %%done: 0-0
    count: 12
    doc: Write to 1 to set the corresponding DMA channel complete interrupt flag.
  %%err: 31-31
    doc: Set to 1 to set DMA error interrupt flag.

%ifc: 32
  longname: Interrupt Flag Clear Register
  direction: w
  %%done: 0-0
    count: 12
    doc: Write to 1 to clear the corresponding DMA channel complete interrupt flag.
  %%err: 31-31
    doc: Set to 1 to clear DMA error interrupt flag. Note that if an error happened, the Bus Error Clear Register must be used to clear the DMA.

%ien: 32
  longname: Interrupt Enable Register
  direction: rw
  %%done: 0-0
    count: 12
    doc: Write to 1 to enable complete interrupt on this DMA channel. Clear to disable the interrupt.
  %%err: 31-31
    doc: Set this bit to enable interrupt on AHB bus error.

%ctrl: 32
  longname: DMA Control Register
  direction: rw
  address: 0x1010
  %%descrect: 0-0
    doc: Word 4 in dma descriptor specifies WIDTH, HEIGHT and SRCSTRIDE for rectangle copies. WIDTH is given by bits 9:0, HEIGHT
    doc: is given by bits 19:10, and SRCSTRIDE is given by bits 30:20
  %%prdu: 1-1
    doc: Allows the reuse of a rect descriptor. When active CH0 and no others can have RDS set
    
%rds: 32
  longname: DMA Retain Descriptor State
  direction: rw
  %%rdsch: 0-0
    count: 12
    doc: Speed up execution of consequtive DMA requests from the same channel by not reading descriptor at the start of every arbitration
    doc: cycle if the next channel is the same as the previous

%loop0: 32
  longname: DMA Channel 0 Loop Register
  address: 0x1020
  direction: rw
  %%width: 0-9
    doc: Reload value for N_MINUS_1 when loop is enabled
  %%en: 16-16
    doc: Reload value for N_MINUS_1 when loop is enabled

%loop1: 32
  longname: DMA Channel 1 Loop Register
  direction: rw
  %%width: 0-9
    doc: Reload value for N_MINUS_1 when loop is enabled
  %%en: 16-16
    doc: Reload value for N_MINUS_1 when loop is enabled

%rect0: 32
  longname: DMA Channel 0 Rectangle Register
  address: 0x1060
  direction: rw
  %%height: 0-9
    doc: Number of lines when doing rectangle copy. Set to the number of lines - 1.
  %%srcstride: 10-20
    doc: Space between start of lines in source rectangle
  %%dststride: 21-31
    doc: Space between start of lines in destination rectangle

%ch_ctrl: 32
  longname: Channel Control Register
  direction: rw
  address: 0x1100
  count: 12
  %%sigsel: 0-3
    doc: Select input signal to DMA channel.
  %%sourcesel: 16-21
    doc: Select input source to DMA channel.
    0: NONE
    8: ADC0
   13: USART1
   16: LEUART0
   20: I2C0
   24: TIMER0
   25: TIMER1
   48: MSC
   49: AES
    
  %%err: 31-31
    doc: Set this bit to enable interrupt on AHB bus error.
