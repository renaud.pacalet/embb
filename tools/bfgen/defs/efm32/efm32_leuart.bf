name: efm32_leuart
longname: EFM32 Low Energy Universal Asynchronous Receiver/Transmitter

bound: 32
align: 32

%ctrl: 32
  longname: Control Register
  direction: rw
  %%autotri: 0-0
    doc: When set, LEUn_TX is tristated whenever the transmitter is inactive.
    0: HIGH
    1: TRISTATED
  %%databits: 1-1
    doc: This register sets the number of data bits.
    0: EIGHT
    1: NINE
  %%parity: 2-3
    doc: Determines whether parity bits are enabled, and whether even or odd parity should be used.
    0: NONE
    2: EVEN
    3: ODD
  %%stopbits: 4-4
    doc: Determines the number of stop-bits used. Only used when transmitting data. The receiver only verifies that one stop bit is present.
    0: ONE
    1: TWO
  %%inv: 5-5
    doc: Set to invert the output on LEUn_TX and input on LEUn_RX.
    0:H1L0
    1:H0L1
  %%errsdma: 6-6
    doc: When set, RX DMA requests will be cleared on framing and parity errors.
  %%loopbk: 7-7
    doc: Set to connect receiver to LEUn_TX instead of LEUn_RX.
  %%SFUBRX: 8-8
    doc: Clears RXBLOCK when the start-frame is found in the incoming data. The start-frame is loaded into the receive buffer.
    0: NONE
    1: CLEAR
  %%mpm: 9-9
    doc: Set to enable multi-processor mode.
    0: NONE
    1: INTERRUPT
  %%mpab: 10-10
    doc: Defines the value of the multi-processor address bit. An incoming frame with its 9th bit equal to the value of this bit marks the frame
    doc: as a multi-processor address frame.
  %%bit8dv: 11-11
    doc: When 9-bit frames are transmitted, the default value of the 9th bit is given by BIT8DV. If TXDATA is used to write a frame, then the
    doc: value of BIT8DV is assigned to the 9th bit of the outgoing frame. If a frame is written with TXDATAX however, the default value is
    doc: overridden by the written value.
  %%rxdmawu: 12-12
    doc: Set to wake the DMA controller up when in EM2 and data is available in the receive buffer.
    0: NONE
    1: WAKE
  %%txdmawu: 13-13
    doc: Set to wake the DMA controller up when in EM2 and space is available in the transmit buffer.
    0: NONE
    1: WAKE
  %%txdelay: 14-15
    doc: Configurable delay before new transfers. Frames sent back-to-back are not delayed.
    0: NONE
    1: SINGLE
    2: DOUBLE

%cmd: 32
  longname: Command Register
  direction: w
  %%rxen: 0-0
    doc: Set to activate data reception on LEUn_RX
  %%rxdis: 1-1
    doc: Set to disable data reception. If a frame is under reception when the receiver is disabled, the incoming frame is discarded.
  %%txen: 2-2
    doc: Set to enable data transmission.
  %%txdis: 3-3
    doc: Set to disable transmission.
  %%rxblocken: 4-4
    doc: Set to set RXBLOCK, resulting in all incoming frames being discarded.
  %%rxblockdis: 5-5
    doc: Set to clear RXBLOCK, resulting in all incoming frames being loaded into the receive buffer.
  %%cleartx: 6-6
    doc: Set to clear receive buffer and the RX shift register.
  %%clearrx: 7-7
    doc: Set to clear receive buffer and the RX shift register.

%status: 32
  longname: Status Register
  direction: r
  %%rxens: 0-0
    doc: Set when the receiver is enabled. The receiver must be enabled for start frames, signal frames, and multi-processor address bit
    doc: detection.
  %%txens: 1-1
    doc: Set when the transmitter is enabled.
  %%rxblock: 2-2
    doc: When set, the receiver discards incoming frames. An incoming frame will not be loaded into the receive buffer if this bit is set at the
    doc: instant the frame has been completely received.
  %%txc: 3-3
    doc: Set when a transmission has completed and no more data is available in the transmit buffer. Cleared when a new transmission starts.
  %%txbl: 4-4
    doc: Indicates the level of the transmit buffer. Set when the transmit buffer is empty, and cleared when it is full.
  %%rxdatav: 5-5
    doc: Set when data is available in the receive buffer. Cleared when the receive buffer is empty.

%clkdiv: 32
  longname: Clock Control Register
  direction: rw
  %%div: 3-14
    doc: Specifies the fractional clock divider for the LEUART

%startframe: 32
  longname: Start Frame Register
  direction: rw
  %%startframe: 0-8
    doc: When a frame matching STARTFRAME is detected by the receiver, STARTF interrupt flag is set, and if SFUBRX is set, RXBLOCK
    doc: is cleared. The start-frame is be loaded into the RX buffer.

%sigframe: 32
  longname: Start Frame Register
  direction: rw
  %%sigframe: 0-8
    doc: When a frame matching SIGFRAME is detected by the receiver, SIGF interrupt flag is set.

%rxdatax: 32
  longname: Receive Buffer Data Extended Register
  direction: r
  %%rxdata: 0-8
    doc: Use this register to access data read from the LEUART. Buffer is cleared on read access.
  %%perr: 14-14
    doc: Set if data in buffer has a parity error.
  %%ferr: 15-15
    doc: Set if data in buffer has a framing error. Can be the result of a break condition.

%rxdata: 32
  longname: Receive Buffer Data Register
  direction: r
  %%rxdata: 0-7
    doc: Use this register to access data read from LEUART. Buffer is cleared on read access. Only the 8 LSB can be read using this register.

%rxdataxp: 32
  longname: Receive Buffer Data Extended Peek Register
  direction: r
  %%rxdatap: 0-8
    doc: Use this register to access data read from the LEUART.
  %%perr: 14-14
    doc: Set if data in buffer has a parity error.
  %%ferr: 15-15
    doc: Set if data in buffer has a framing error. Can be the result of a break condition.

%txdatax: 32
  longname: Transmit Buffer Data Extended Register
  direction: w
  %%txdata: 0-8
    doc: Use this register to write data to the LEUART. If the transmitter is enabled, a transfer will be initiated at the first opportunity.
  %%txbreak: 13-13
    doc: Set to send data as a break. Recipient will see a framing error or a break condition depending on its configuration and the value
    doc: of TXDATA.
    0:NONE
    1:BREAK
  %%txdiasat: 14-14
    doc: Set to disable transmitter directly after transmission has competed.
    0:NONE
    1:DISABLED
  %%rxenat: 15-15
    doc: Set to enable reception after transmission.

%txdata: 32
  longname: Transmit Buffer Data Register
  direction: w
  %%txdata: 0-7
    doc: This frame will be added to TX buffer. Only 8 LSB can be written using this register. 9th bit and control bits will be cleared.


%if: 32
  longname: Interrupt Flag Register
  direction: r
  %%txc: 0-0
    doc: Set after a transmission when both the TX buffer and shift register are empty.
  %%txbl: 1-1
    doc: Set when space becomes available in the transmit buffer for a new frame.
  %%rxdatav: 2-2
    doc: Set when data becomes available in the receive buffer.
  %%rxof: 3-3
    doc: Set when data is incoming while the receive shift register is full. The data previously in shift register is overwritten by the new data.
  %%rxuf: 4-4
    doc: Set when trying to read from the receive buffer when it is empty.
  %%txof: 5-5
    doc: Set when a write is done to the transmit buffer while it is full. The data already in the transmit buffer is preserved.
  %%perr: 6-6
    doc: Set when a frame with a parity error is received while RXBLOCK is cleared.
  %%ferr: 7-7
    doc: Set when a frame with a framing error is received while RXBLOCK is cleared.
  %%mpaf: 8-8
    doc: Set when a multi-processor address frame is detected.
  %%startf: 9-9
    doc: Set when a start frame is detected.
  %%sigf: 10-10
    doc: Set when a signal frame is detected.

%ifs: 32
  longname: Interrupt Flag Set Register
  direction: w
  %%txc: 0-0
    doc: Write to 1 to set the TXC interrupt flag.
  %%rxof: 3-3
    doc: Write to 1 to set the RXOF interrupt flag.
  %%rxuf: 4-4
    doc: Write to 1 to set the RXUF interrupt flag.
  %%txof: 5-5
    doc: Write to 1 to set the TXOF interrupt flag.
  %%perr: 6-6
    doc: Write to 1 to set the PERR interrupt flag.
  %%ferr: 7-7
    doc: Write to 1 to set the FERR interrupt flag.
  %%mpaf: 8-8
    doc: Write to 1 to set the MPAF interrupt flag.
  %%startf: 9-9
    doc: Write to 1 to set the STARTF interrupt flag.
  %%sigf: 10-10
    doc: Write to 1 to set the SIGF interrupt flag.

%ifc: 32
  longname: Interrupt Flag Clear Register
  direction: w
  %%txc: 0-0
    doc: Write to 1 to clear the TXC interrupt flag.
  %%rxof: 3-3
    doc: Write to 1 to clear the RXOF interrupt flag.
  %%rxuf: 4-4
    doc: Write to 1 to clear the RXUF interrupt flag.
  %%txof: 5-5
    doc: Write to 1 to clear the TXOF interrupt flag.
  %%perr: 6-6
    doc: Write to 1 to clear the PERR interrupt flag.
  %%ferr: 7-7
    doc: Write to 1 to clear the FERR interrupt flag.
  %%mpaf: 8-8
    doc: Write to 1 to clear the MPAF interrupt flag.
  %%startf: 9-9
    doc: Write to 1 to clear the STARTF interrupt flag.
  %%sigf: 10-10
    doc: Write to 1 to clear the SIGF interrupt flag.

%ien: 32
  longname: Interrupt Enable Register
  direction: w
  %%txc: 0-0
    doc: Enable interrupt on TX complete.
  %%txbl: 1-1
    doc: Enable interrupt on TX buffer level.
  %%rxdatav: 2-2
    doc: Enable interrupt on RX data.
  %%rxof: 3-3
    doc: Enable interrupt on RX overflow.
  %%rxuf: 4-4
    doc: Enable interrupt on RX underflow.
  %%txof: 5-5
    doc: Enable interrupt on TX overflow.
  %%perr: 6-6
    doc: Enable interrupt on parity error.
  %%ferr: 7-7
    doc: Enable interrupt on framing error.
  %%mpaf: 8-8
    doc: Enable interrupt on multi-processor address frame.
  %%startf: 9-9
    doc: Enable interrupt on start frame.
  %%sigf: 10-10
    doc: Enable interrupt on signal frame.

%pulsectrl: 32
  longname: Pulse Control Register
  direction: rw
  %%pulsew: 0-3
    doc: Configure the pulse width of the pulse generator as a number of 32.768 kHz clock cycles.
  %%pulseen: 4-4
    doc: Filter LEUART output through pulse generator and the LEUART input through the pulse extender.
  %%pulsefilt: 5-5
    doc: Enable a one-cycle pulse filter for pulse extender

%freeze: 32
  longname: Freeze Register
  direction: rw
  %%regfreeze: 0-0
    doc: When set, the update of the LEUART is postponed until this bit is cleared. Use this bit to update several registers simultaneously.
    0: UPDATE
    1: FREEZE

%syncbusy: 32
  longname: Synchronization Busy Register
  direction: r
  %%ctrl: 0-0
    doc: Set when the value written to CTRL is being synchronized.
  %%cmd: 1-1
    doc: Set when the value written to CMD is being synchronized.
  %%clkdiv: 2-2
    doc: Set when the value written to CLKDIV is being synchronized.
  %%startframe: 3-3
    doc: Set when the value written to STARTFRAME is being synchronized.
  %%sigframe: 4-4
    doc: Set when the value written to SIGFRAME is being synchronized.
  %%txdatax: 5-5
    doc: Set when the value written to TXDATAX is being synchronized.
  %%txdata: 6-6
    doc: Set when the value written to TXDATA is being synchronized.
  %%pulsectrl: 7-7
    doc: Set when the value written to PULSECTRL is being synchronized.

%route: 32
  longname: I/O Routing Register
  direction: rw
  address: 0x54
  %%rxpen: 0-0
    doc: When set, the RX pin of the LEUART is enabled.
  %%txpen: 1-1
    doc: When set, the TX pin of the LEUART is enabled.
  %%location: 8-10
    doc: Decides the location of the LEUART I/O pins.
    0:LOC0
    1:LOC1
    2:LOC2
    3:LOC3
    4:LOC4

%input: 32
  longname: LEUART Input Register
  direction: rw
  address: 0xAC
  %%rxprssel: 0-1
    doc: Select PRS channel as input to RX.
    0: PRSCH0 
    1: PRSCH1 
    2: PRSCH2 
    3: PRSCH3 
  %%rxprs: 4-4
    doc: When set, the PRS channel selected as input to RX.

