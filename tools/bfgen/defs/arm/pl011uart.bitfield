name: bcm2835_pl011
doc: PrimeCell pl011 UART registers

bound: 32
align: 32

%dr: 32
  longname: data register
  address: 0x0

  %%data: 0-7
    longname: receive data character or transmit data character

  %%fe: 8-8
    longname: framing error

  %%pe: 9-9
    longname: parity error

  %%be: 10-10
    longname: break error

  %%oe: 11-11
    longname: overrun error

%rsrecr: 32
  longname: receive status register
  address: 0x4

  %%fe: 0-0
    longname: framing error

  %%pe: 1-1
    longname: parity error

  %%be: 2-2
    longname: break error

  %%oe: 3-3
    longname: overrun error

%fr: 32
  longname: flag register
  address: 0x18

  %%cts: 0-0
    longname: clear to send

  %%busy: 3-3
    longname: uart busy

  %%rxfe: 4-4
    longname: receive FIFO empty

  %%txff: 5-5
    longname: transmit FIFO full

  %%rxff: 6-6
    longname: receive FIFO full

  %%txfe: 7-7
    longname: transmit FIFO empty

%ibrd: 32
  longname: integer baud rate divisor
  address: 0x24

  %%ibrd: 0-15
    longname: the integer baud rate divisor

%fbrd: 32
  longname: fractional baud rate divisor
  address: 0x28

  %%fbrd: 0-5
    longname: the fractional baud rate divisor

%lcrh: 32
  longname: line control register
  address: 0x2c

  %%brk: 0-0
    longname: send break

  %%pen: 1-1
    longname: parity enable
    0: disabled: parity is disabled
    1: enabled: parity checking

  %%eps: 2-2
    longname: even parity select
    0: odd: odd parity
    1: even: even parity

  %%stp2: 3-3
    longname: two stop bits select

  %%fen: 4-4
    longname: enable FIFOs
    0: nofifo: FIFOs are disabled
    1: usefifo: transmit and receive FIFO buffers are enabled

  %%wlen:5-6
    longname: word length
    0b00: 5 bits
    0b01: 6 bits
    0b10: 7 bits
    0b11: 8 bits

  %%sps: 7-7
    longname: stick parity select
    0: disabled: stick parity is disabled
    1: enabled: if the EPS bit is 0 then the parity bit is transmitted and checked as 1, if the EPS bit is 1 then the parity bit is transmitted and checked as a 0

%cr: 32
  longname: control register
  address: 0x30

  %%uarten: 0-0
    longname: UART enable
    0: disabled: UART is disabled
    1: enabled: UART is enabled

  %%lbe: 7-7
    longname: loopback enable

  %%txe: 8-8
    longname: transmit enable

  %%rxe: 9-9
    longname: receive enable

  %%rts: 11-11
    longname: request to send

  %%rtsen: 14-14
    longname: RTS hardware flow control enable

  %%ctsen: 15-15
    longname: CTS hardware flow control enable

%ifls: 32
  longname: interrupt FIFO level select register
  address: 0x34

  %%rxiflsel: 3-5
    longname: receive interrupt FIFO level select
    0b000: 1/8: irq on receive FIFO 1/8 full
    0b001: 1/4: irq on receive FIFO 1/4 full
    0b010: 1/2: irq on receive FIFO 1/2 full
    0b011: 3/4: irq on receive FIFO 3/4 full
    0b100: 7/8: irq on receive FIFO 7/8 full

  %%txiflsel: 0-2
    longname: transmit interrupt FIFO level select
    0b000: 1/8: irq on transmit FIFO 1/8 full
    0b001: 1/4: irq on transmit FIFO 1/4 full
    0b010: 1/2: irq on transmit FIFO 1/2 full
    0b011: 3/4: irq on transmit FIFO 3/4 full
    0b100: 7/8: irq on transmit FIFO 7/8 full

%imsc: 32
  longname: interrupt mask set clear register
  address: 0x3c

  %%ctsmim: 1-1
    longname: nUARTCTS modem interrupt mask

  %%rxim: 4-4
    longname: receive interrupt mask

  %%txim: 5-5
    longname: transmit interrupt mask

  %%rtim: 6-6
    longname: receive timeout interrupt mask

  %%feim: 7-7
    longname: framing error interrupt mask

  %%peim: 8-8
    longname: parity error interrupt mask

  %%beim: 9-9
    longname: break error interrupt mask

  %%oeim: 10-10
    longname: overrun error interrupt mask

%ris: 32
  longname: raw interrupt status register
  address: 0x40

  %%ctsrmis: 1-1
    longname: nUARTCTS modem interrupt status

  %%rxris: 4-4
    longname: receive interrupt status

  %%txris: 5-5
    longname: transmit inerrupt status

  %%rtris: 6-6
    longname: receive timeout interrupt status

  %%feris: 7-7
    longname: frawing error interrupt status

  %%peris: 8-8
    longname: parity error interrupt status

  %%beris: 9-9
    longname: break error interrupt status

  %%oeris: 10-10
    longname: overrun error interrupt status

%mis: 32
  longname: masked interrupt status register
  address: 0x40

  %%ctsmmis: 1-1
    longname: nUARTCTS modem masked interrupt status

  %%rxmis: 4-4
    longname: receive masked interrupt status

  %%txmis: 5-5
    longname: transmit masked interrupt status

  %%rtmis: 6-6
    longname: receive timeout masked interrupt status

  %%femis: 7-7
    longname: framing error masked interrupt status

  %%pemis: 8-8
    longname: parity error masked interrupt status

  %%bemis: 9-9
    longname: break error masked interrupt status

  %%oemis: 10-10
    longname: overrun error masked interrupt status

%icr: 32
  longname: interrupt clear register
  address: 0x44

  %%ctsmic: 1-1
    longname: nUARTCTS modem masked interrupt status

  %%rxic: 4-4
    longname: receive masked interrupt status

  %%txic: 5-5
    longname: transmit interrupt clear

  %%rtic: 6-6
    longname: receive timeout interrupt clear

  %%feic: 7-7
    longname: framing error interrupt clear

  %%peic: 8-8
    longname: parity error interrupt clear

  %%beic: 9-9
    longname: break error interrupt clear

  %%oeic: 10-10
    longname: overrun error interrupt clear

%itcr: 32
  longname: test control register
  address: 0x80

  %%itcr0: 0-0
    longname: integration test enable

  %%ictr1: 1-1
    longname: test FIFO enable

%itip: 32
  longname: integration test input reg
  address: 0x84

  %%itip0: 0-0
    longname: UARTRXD primary input
    doc: reads return the value of the UARTRXD primary input

  %%itip3: 3-3
    longname: nUARTCTS primary input
    doc: reads return the value of the nUARTCTS primary input

%itop: 32
  longname: integration test output reg
  address: 0x88

  %%itip0: 0-0
    longname: UARTTXD primary output
    doc: Set the value to drive on UARTTXD on write

  %%itip3: 3-3
    longname: nUARTRTS primary output
    doc: Set the value to drive on nUARTRTS on write

  %%itip6: 6-6
    longname: UARTINTR intra-chip output
    doc: Set the value to drive on UARTINTR on write

  %%itop7: 7-7
    longname: UARTEINTR intra-chip output
    doc: Set the value to drive on UARTEINTR on write

  %%itop8: 8-8
    longname: UARTRTINTR intra-chip output
    doc: Set the value to drive on UARTRTINTR on write

  %%itop9: 9-9
    longname: UARTTXINTR intra-chip output
    doc: Set the value to drive on UARTTXINTR on write

  %%itop10: 10-10
    longname: UARTRXINTR intra-chip output
    doc: Set the value to drive on UARTRXINTR on write

  %%itop11: 11-11
    longname: UARTMSINTR intra-chip output
    doc: Set the value to drive on UARTMSINTR on write

%tdr: 32
  longname: test data reg
  address: 0x8c

  %%tdr10_0: 0-10
    doc: when the ITCR1 bit is set to 1, data is written into the receive FIFO and read out of the transmit FIFO
