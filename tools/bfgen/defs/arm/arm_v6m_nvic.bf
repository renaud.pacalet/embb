name: v6m_nvic
longname: ARM v6-M Nested Vectored Interrupt Controller

bound: 32
align: 32

%nvic_iser: 32
  longname: Interrupt Set-Enable Register
  direction: rw
  address: 0xE000E100 
  %%setena: 0-0
    count: 32 
    doc: Enables, or reads the enabled state of one or more interrupts. Each bit corresponds to the same
    doc: numbered interrupt.
    
%nvic_icer: 32
  address: 0xE000E180 
  longname: Interrupt Clear-Enable Register
  direction: rw
  %%clrena: 0-0
    count: 32 
    doc: Disables, or reads the enabled state of one or more interrupts. Each bit corresponds to the
    doc: same numbered interrupt:

%nvic_ispr: 32
  longname: Interrupt Set-Pending Register
  address: 0xE000E200 
  direction: rw
  %%setpend: 0-0
    count: 32 
    doc: On writes, sets the status of one or more interrupts to pending. On reads, shows the
    doc: pending status of the interrupts.

%nvic_icpr: 32
  longname: Interrupt Clear-Pending Register
  address: 0xE000E280 
  direction: rw
  %%clrpend: 0-0
    count: 32
    doc: On writes, clears the status of one or more interrupts to pending. On reads, shows
    doc: the pending status of the interrupts.
 
%nvic_ipr: 32
  longname: Interrupt Priority Registers
  direction: rw
  address: 0xE000E400 
  count: 8
  %%ipr: 6-7
    count: 4
    stride: 8
    doc: Eight NVIC_IPRs are implemented, supporting up to 32 interrupts. These registers
    doc: are always implemented.
    doc: If an interrupt is not implemented, the corresponding PRI_Nx field can be RAZ/WI.
