
name: ipv4
longname: Internet Protocol version 4
bound: 16

%iv: 8
	address: 0

    %%version: 4-7
	longname: IP version
	4: version 4

    %%ihl: 0-3
        longname: Internet Header Length

%tos: 8
	address: 1
	longname: Type of Service

    %%r: 2-2
	longname: high reliability

    %%t: 3-3
	longname: High throughput

    %%d: 4-4
	longname: Low delay

    %%prec: 5-7
	longname: Precedence
	0b111: Network Control
	0b110: Internetwork Control
	0b101: CRITIC/ECP
	0b100: Flash Override
	0b011: Flash
	0b010: Immediate
	0b001: Priority
	0b000: Routine

%diffserv: 8
	address: union
	longname: Differentiated Services Code Point

    %%ecn: 0-1
	longname: Explicit congestion notification
	0b00: Non-EC : Non ECN-Capable Transport
	0b10: ECT(0) : ECN Capable Transport
	0b01: ECT(1) : ECN Capable Transport
	0b11: CE : Congestion Encountered

    %%dscp: 2-7
	longname: Differentiated services codepoint

%len: 16
	address: 2
	longname: Total Length

%id: 16
	address: 4
	longname: Identification

%frag: 16
	address: 6

    %%off: 0-12
	longname: Fragment offset

    %%df: 13-13
	longname: Dont fragment flag

    %%mf: 14-14
	longname: More fragment flag

%ttl:8
	address: 8
	longname: Time To Live

%protocol: 8
	address: 9
	longname: Protocol
	1: icmp
	2: igmp
	6: tcp
	17: udp

%sum: 16
	address: 10
	longname: Checksum

%src: 32
	address: 12
	longname: Source IP

%dst: 32
	address: 16
	longname: Destination IP

