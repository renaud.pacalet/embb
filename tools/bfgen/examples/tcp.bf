
# Documentation from wikipedia

name: tcp
longname: TCP protocol header
bound: 8

%sport: 16
	longname: Source port
	doc: Identifies the sending port

%dport: 16
	longname: Destination port
	doc: Identifies the receiving port

%seq: 32
	longname: Sequence number

	doc: If the SYN flag is set (1), then this is the initial
	doc: sequence number. The sequence number of the actual first data
	doc: byte and the acknowledged number in the corresponding ACK are
	doc: then this sequence number plus 1.

	doc: If the SYN flag is clear (0), then this is the accumulated
	doc: sequence number of the first data byte of this packet for the
	doc: current session.

%ack: 32
	longname: Acknowledgement number

	doc: If the ACK flag is set then the value of this field is the
	doc: next sequence number that the receiver is expecting. This
	doc: acknowledges receipt of all prior bytes (if any). The first
	doc: ACK sent by each end acknowledges the other end's initial
	doc: sequence number itself, but no data.

%off: 8
    %%off: 4-7
	longname: Data offset
		
	doc: Specifies the size of the TCP header in 32-bit
	doc: words. The minimum size header is 5 words and the
	doc: maximum is 15 words thus giving the minimum size of 20
	doc: bytes and maximum of 60 bytes, allowing for up to 40
	doc: bytes of options in the header. This field gets its
	doc: name from the fact that it is also the offset from the
	doc: start of the TCP segment to the actual data.

%flags: 8
    %%fin
	longname: Fin flag

	doc: No more data is available from sender

    %%syn
	longname: Synchronize flag

	doc: Synchronize sequence numbers. Only the first
	doc: packet sent from each end should have this flag
	doc: set. Some other flags change meaning based on this
	doc: flag, and some are only valid for when it is set, and
	doc: others when it is clear.

    %%rst
	longname: Reset flag

	doc: Reset the connection

    %%push
	longname: Push flag

	doc: Push function. Asks to push the buffered data to
	doc: the receiving application.

    %%ack
	longname: Acknowledgment flag

	doc: Indicates that the Acknowledgment field is
	doc: significant. All packets after the initial SYN packet
	doc: sent by the client should have this flag set.

    %%urg
	longname: Urgent flag

	doc: Indicates that the Urgent pointer field is significant

    %%ece
	longname: ECN-Echo indicates

	doc: If the SYN flag is set (1), that the TCP peer is ECN
	doc: capable.

	doc: If the SYN flag is clear (0), that a packet with
	doc: Congestion Experienced flag in IP header set is
	doc: received during normal transmission.

    %%cwr
	longname: CWR flag

	doc: Congestion Window Reduced (CWR) flag is set by the
	doc: sending host to indicate that it received a TCP
	doc: segment with the ECE flag set and had responded in
	doc: congestion control mechanism

%win: 16
	longname: Window size

	doc: The size of the receive window, which specifies the
	doc: number of bytes (beyond the sequence number in the
	doc: acknowledgment field) that the receiver is currently willing
	doc: to receive

%sum: 16
	longname: Checksum

	doc: The 16-bit checksum field is used for error-checking of
	doc: the header and data

%urp: 16
	longname: Urgent pointer

	doc: If the URG flag is set, then this 16-bit field is an
	doc: offset from the sequence number indicating the last urgent
	doc: data byte

