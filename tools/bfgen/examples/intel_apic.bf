
name: apic
longname: Intel Local APIC
spec_title: Intel 64 and IA-32 Architecture Software Developer's Manual, Volume 3A
spec_version: April 2011

bound: 32
align: 128

# little endian access
swap: 0b111000

%id: 32
	longname: Local APIC ID
	address: 0x20

	%%apic_id: 24-31

%version: 32
	longname: Local APIC Version
	address: 0x30
	direction: r

	%%version: 0-7
		longname: Apic Version

	%%maxlvt: 16-23
		longname: Number of LVT entries minus 1

	%%eoibs: 24-24
		longname: Support for EOI-broadcast suppression

%tpr: 32
	longname: Task Priority
	address: 0x80
	default: 0

	%%tp: 4-7
		longname: Task Priority

	%%tpsub: 0-3
		longname: Task Priority Sub-Class

%apr: 32
	longname: Arbitration Priority
	address: 0x90
	direction: r
	default: 0

	%%ap: 4-7
		longname: Arbitration Priority

	%%apsub: 0-3
		longname: Arbitration Priority Sub-Class

%ppr: 32
	longname: Processor Priority
	address: 0xa0
	direction: r
	default: 0

	%%pp: 4-7
		longname: Processor Priority

	%%ppsub: 0-3
		longname: Processor Priority Sub-Class

%eoi: 32
	longname: End Of Interrupt
	address: 0xb0
	default: 0
	direction: w

%rrd: 32
	longname: Remote Read
	address: 0xc0
	direction: r

%ldr: 32
	longname: Logical Direction
	address: 0xd0
	default: 0

	%%apic_id: 24-31
		longname: Logical APIC ID

%dfr: 32
	longname: Direction Format
	address: 0xe0
	default: 0xffffffff
	reserved: 0x0fffffff

	%%model: 28-31
		longname: Model
		0b1111: Flat model
		0b0000: Cluster model

%svr: 32
	longname: Spurious Interrupt Vector
	address: 0xf0
	default: 0xff

	%%vector: 0-7
 		longname: Spurious Vector

	%%softenable: 8-8
		longname: APIC software Enable/Disable

	%%focus: 9-9
		longname: Focus processor checking
		0: Enabled
		1: Disabled

	%%eoibs: 12-12
		longname: EOI-Broadcast Suppression
		0: Enabled
		1: Disabled

%isr31_0: 32
	longname: In-Service; bits 31:0
	address: 0x100
	direction: r
	default: 0

%isr63_32: 32
	longname: In-Service; bits 63:32
	address: 0x110
	direction: r
	default: 0

%isr95_64: 32
	longname: In-Service; bits 95:64
	address: 0x120
	direction: r
	default: 0

%isr127_96: 32
	longname: In-Service; bits 127:96
	address: 0x130
	direction: r
	default: 0

%isr159_128: 32
	longname: In-Service; bits 159:128
	address: 0x140
	direction: r
	default: 0

%isr191_160: 32
	longname: In-Service; bits 191:160
	address: 0x150
	direction: r
	default: 0

%isr223_192: 32
	longname: In-Service; bits 223:192
	address: 0x160
	direction: r
	default: 0

%isr255_224: 32
	longname: In-Service; bits 255:224
	address: 0x170
	direction: r
	default: 0

%tmr31_0: 32
	longname: Trigger Mode; bits 31:0
	address: 0x180
	direction: r
	default: 0

%tmr63_32: 32
	longname: Trigger Mode; bits 63:32
	address: 0x190
	direction: r
	default: 0

%tmr95_64: 32
	longname: Trigger Mode; bits 95:64
	address: 0x1a0
	direction: r
	default: 0

%tmr127_96: 32
	longname: Trigger Mode; bits 127:96
	address: 0x1b0
	direction: r
	default: 0

%tmr159_128: 32
	longname: Trigger Mode; bits 159:128
	address: 0x1c0
	direction: r
	default: 0

%tmr191_160: 32
	longname: Trigger Mode; bits 191:160
	address: 0x1d0
	direction: r
	default: 0

%tmr223_192: 32
	longname: Trigger Mode; bits 223:192
	address: 0x1e0
	direction: r
	default: 0

%tmr255_224: 32
	longname: Trigger Mode; bits 255:224
	address: 0x1f0
	direction: r
	default: 0

%irr31_0: 32
	longname: Interrupt Request; bits 31:0
	address: 0x200
	direction: r
	default: 0

%irr63_32: 32
	longname: Interrupt Request; bits 63:32
	address: 0x210
	direction: r
	default: 0

%irr95_64: 32
	longname: Interrupt Request; bits 95:64
	address: 0x220
	direction: r
	default: 0

%irr127_96: 32
	longname: Interrupt Request; bits 127:96
	address: 0x230
	direction: r
	default: 0

%irr159_128: 32
	longname: Interrupt Request; bits 159:128
	address: 0x240
	direction: r
	default: 0

%irr191_160: 32
	longname: Interrupt Request; bits 191:160
	address: 0x250
	direction: r
	default: 0

%irr223_192: 32
	longname: Interrupt Request; bits 223:192
	address: 0x260
	direction: r
	default: 0

%irr255_224: 32
	longname: Interrupt Request; bits 255:224
	address: 0x270
	direction: r
	default: 0

%esr: 32
	longname: Error Status
	address: 0x280
	direction: r
	default: 0

	%%scsum: 0-0
		longname: Send Checksum Error
	%%rcsum: 1-1
		longname: Receive Checksum Error
	%%sae: 2-2
		longname: Send Accept Error
	%%rae: 3-3
		longname: Receive Accept Error
	%%ripi: 4-4
		longname: Redirectable IPI
	%%siv: 5-5
		longname: Send Illegal Vector
	%%riv: 6-6
		longname: Receive Illegal Vector
	%%ira: 7-7
		longname: Illegal Register Address

%lvt_cmci: 32
	longname: LVT CMCI
	address: 0x2f0
	default: 0x00010000

	%%vector: 0-7
		longname: Vector Number

	%%dmode: 8-10
		longname: Delivery mode
		0b000: Fixed
		0b010: SMI
		0b100: NMI
		0b111: ExtINT
		0b101: INIT

	%%dstatus: 12-12
		longname: Delivery Status
		0: Idle
		1: Send pending

	%%mask: 16-16
		longname: Interrupt mask
		0: Not masked
		1: Masked


%icr0_31: 32
	longname: Interrupt Command; bits 0-31
	address: 0x300
	default: 0

	%%vector: 0-7
 		longname: Vector Number

	%%dmode: 8-10
		longname: Delivery mode
		0b000: Fixed
		0b001: Lowest Priority
		0b010: SMI
		0b100: NMI
		0b101: INIT
		0b110: Start Up

	%%dstmode: 11-11
		longname: Destination mode
		0: Physical
		1: Logical

	%%dstatus: 12-12
		longname: Delivery Status
		0: Idle
		1: Send pending

	%%level: 14-14
		longname: Level
		0: De-assert
		1: Assert

	%%trmode: 15-15
		longname: Trigger mode
		0: Edge
		1: Level

	%%dstshrt: 18-19
		longname: Destination Shorthand
		0b00: No shorthand
		0b01: Self
		0b10: All including self
		0b11: All excluding self

%icr32_63: 32
	longname: Interrupt Command; bits 32-63
	address: 0x310
	default: 0

	%%dest: 24-31
		longname: Destination field

%lvt_timer: 32
	longname: LVT Timer
	address: 0x320
	default: 0x00010000

	%%vector: 0-7
 		longname: Vector Number

	%%dstatus: 12-12
		longname: Delivery Status
		0: Idle
		1: Send pending

	%%mask: 16-16
		longname: Interrupt mask
		0: Not masked
		1: Masked

	%%tmode: 17-18
		longname: Timer mode
		0b00: One-shot
		0b01: Periodic
		0b10: TSC-Deadline

%lvt_tsr: 32
	longname: LVT Thermal Sensor
	address: 0x330
	default: 0x00010000

	%%vector: 0-7
		longname: Vector Number

	%%dmode: 8-10
		longname: Delivery mode
		0b000: Fixed
		0b010: SMI
		0b100: NMI
		0b111: ExtINT
		0b101: INIT

	%%dstatus: 12-12
		longname: Delivery Status
		0: Idle
		1: Send pending

	%%mask: 16-16
		longname: Interrupt mask
		0: Not masked
		1: Masked

%lvt_pmcr: 32
	longname: LVT Performance Monitoring Counters
	address: 0x340
	default: 0x00010000

	%%vector: 0-7
		longname: Vector Number

	%%dmode: 8-10
		longname: Delivery mode
		0b000: Fixed
		0b010: SMI
		0b100: NMI
		0b111: ExtINT
		0b101: INIT

	%%dstatus: 12-12
		longname: Delivery Status
		0: Idle
		1: Send pending

	%%mask: 16-16
		longname: Interrupt mask
		0: Not masked
		1: Masked

%lvt_lint0: 32
	longname: LVT LINT0
	address: 0x350
	default: 0x00010000

	%%vector: 0-7
		longname: Vector Number

	%%dmode: 8-10
		longname: Delivery mode
		0b000: Fixed
		0b010: SMI
		0b100: NMI
		0b111: ExtINT
		0b101: INIT

	%%dstatus: 12-12
		longname: Delivery Status
		0: Idle
		1: Send pending

	%%pol: 13-13
		longname: Pin polarity

	%%rirr: 14-14
		longname: Remote IRR

	%%tmode: 15-15
		longname: Trigger mode
		0: Edge
		1: Level

	%%mask: 16-16
		longname: Interrupt mask
		0: Not masked
		1: Masked

%lvt_lint1: 32
	longname: LVT LINT0
	address: 0x360
	default: 0x00010000

	%%vector: 0-7
		longname: Vector Number

	%%dmode: 8-10
		longname: Delivery mode
		0b000: Fixed
		0b010: SMI
		0b100: NMI
		0b111: ExtINT
		0b101: INIT

	%%dstatus: 12-12
		longname: Delivery Status
		0: Idle
		1: Send pending

	%%pol: 13-13
		longname: Pin polarity

	%%rirr: 14-14
		longname: Remote IRR

	%%tmode: 15-15
		longname: Trigger mode
		0: Edge
		1: Level

	%%mask: 16-16
		longname: Interrupt mask
		0: Not masked
		1: Masked

%lvt_error: 32
	longname: LVT Error
	address: 0x370
	default: 0x00010000

	%%vector: 0-7
		longname: Vector Number

	%%dstatus: 12-12
		longname: Delivery Status
		0: Idle
		1: Send pending

	%%mask: 16-16
		longname: Interrupt mask
		0: Not masked
		1: Masked

%timer_icr: 32
	longname: Timer Initial Count
	address: 0x380
	default: 0

%timer_ccr: 32
	longname: Timer Current Count
	address: 0x390
	direction: r
	default: 0

%timer_div: 32
	longname: Timer Divide Configuration
	address: 0x3e0
	default: 0

	%%dvalue: 0-3
		longname: Divide Value
		0b0000: Divide by 2
		0b0001: Divide by 4
		0b0010: Divide by 8
		0b0011: Divide by 16
		0b1000: Divide by 32
		0b1001: Divide by 64
		0b1010: Divide by 128
		0b1011: Divide by 1

