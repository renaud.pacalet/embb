--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Crossbar mapping package
--*
--*  Defines a set of constants to configure the crossbar:
--* - The number of requesters (initiators),
--* - The number of targets,
--* - Their names and unique IDs (the two ID spaces are independent and an initiator can have the same ID as a target),
--* - The base address and address mask of each target (xtag_len bits each, these are used to decode the requests and route them to the right target). An
--*   address a match a target with base address ba and address mask msk iff:
--*
--*     a(vci_n-1 downto vci_n-xtag_len) and msk = ba
--*
--* - The initiator to target connections and the fixed priorities between incoming requests and responses.
--*
--* Usage:
--* In order to configure a crossbar, search for "UPDATE THIS" and follow the instructions:
--* - First indicate a number of initiators (NI constant) and a number of targets
--* (NT constant).
--* - Chose a short nickname for each of them.
--* - Declare a constant of type "natural range 0 to NI - 1" for each initiator
--* (name it with a short but meaningfull nickname) and assign it a unique ID. Do
--* the same for each target, with type "natural range 0 to NT - 1" (the two ID
--* spaces are independent and an initiator can have the same ID as a target).
--* - Declare and assign a constant of type TMAPV. This constant is a vector of
--* NT target maps, one per target. Each definition comprise  a target ID, a
--* target base address BA and an address mask MSK. The base address and the mask
--* are used to decode addresses, using their XTAG_LEN MSBs: the XTAG_LEN MSBs A
--* of an address match a target iff
--*
--*   A and MSK = BA and MSK
--* 
--* - Declare and assign a constant of type TDEFV. This constant is a vector of
--* NT target definitions, one per target. Each definition comprise a number N of
--* connected initiators and a list I of NI initiators IDs, in decreasing order
--* of requests priorities. The N first elements of the list are taken into
--* account for the routing. The NI-N last are ignored and their values can be
--* anything valid.
--* - Symmetrically define and assign a constant of type IDEFV. It is a vector of
--* NI initiators definitions, one per initiator. Each comprise an ID, a number N
--* of connected targets and a list T of target IDs, in decreasing order of
--* responses priorities. The N first elements of the list are taken into account
--* for the routing. The NT-N last are ignored and their values can be anything
--* valid.
--* - Finally, for debugging, adapt the TRGT_NAME and INIT_NAME function bodies
--* to your specific system.

library ieee;
use ieee.std_logic_1164.all;

library crossbar_lib;
use crossbar_lib.crossbar_pkg.all;
use crossbar_lib.plugplay_0_pkg.all;

package crossbar_map is

  -- UPDATE THIS
  
  constant ADC_ENABLE : integer := 0;
  constant LIME_ENABLE : integer := 0;
  constant PRE_PROCESSOR_ENABLE : integer := 0;
  constant ADAIF_ENABLE : integer := 0;
  constant CHANNEL_DECODER_ENABLE : integer := 0;
  constant RAM_ENABLE : integer := 1;
  constant FEP_ENABLE : integer := 1;
  constant MAPPER_ENABLE : integer := 1;
  constant INTL_ENABLE : integer := 1;

  constant icross : natural range 0 to INS_MAX - 1 := 0;                                --* Instance number:Crossbar
  constant ibri   : natural range 0 to INS_MAX - 1 := 1;                                --* Instance number:Bridge
  constant ifep   : natural range 0 to INS_MAX - 1 := ibri   + FEP_ENABLE;              --* Instance number:Front eINS_MAX processor
  constant iram   : natural range 0 to INS_MAX - 1 := ifep   + RAM_ENABLE;              --* Instance number:VCI RAM
  constant iintl  : natural range 0 to INS_MAX - 1 := iram   + INTL_ENABLE;             --* Instance number:Interleaver
  constant imapp  : natural range 0 to INS_MAX - 1 := iintl  + MAPPER_ENABLE;           --* Instance number:Mapper
  constant iadc   : natural range 0 to INS_MAX - 1 := imapp  + ADC_ENABLE;              --* Instance number:SPI controller interface for adc
  constant ichdc  : natural range 0 to INS_MAX - 1 := iadc   + CHANNEL_DECODER_ENABLE;  --* Instance number:Channel decoder
  constant ipp    : natural range 0 to INS_MAX - 1 := ichdc  + PRE_PROCESSOR_ENABLE;    --* Instance number:Preprocessor
  constant iadaif : natural range 0 to INS_MAX - 1 := ipp    + ADAIF_ENABLE;            --* Instance number:ADA interface

-- EXEMPLES
-- constant mapp  : instance := (ven => TELECOM_PARISTECH, d => MAPPER,   c => WD & WU & WC, ver => 0, nirq => 3, msk => X"FFF", addr => X"206", nt => 3, ni => 1, t => (ifep, iintl, others => 0),   i => (ibri, others => 0));
-- constant ram   : instance := (ven => TELECOM_PARISTECH, d => RAM,      c => WD & NU & NC, ver => 0, nirq => 1, msk => X"FFF", addr => X"201", nt => 2, ni => 2, t => (ifep, iadaif, others => 0),  i => (ibri, iadaif, others => 0));
-- constant adc   : instance := (ven => EURECOM,           d => ADCSPI,   c => ND & NU & WC, ver => 0, nirq => 1, msk => X"FFF", addr => X"204", nt => 0, ni => 1, t => (others => 0),                i => (ibri, others => 0));
-- constant adaif : instance := (ven => TELECOM_PARISTECH, d => ADAIF,    c => WD & WU & WC, ver => 0, nirq => 11,msk => X"FFF", addr => X"203", nt => 1, ni => 1, t => (others => ifep),             i => (ibri, others => 0));
-- constant ram   : instance := (ven => TELECOM_PARISTECH, d => RAM,      c => WD & NU & NC, ver => 0, nirq => 1, msk => X"FFF", addr => X"201", nt => 1, ni => 1, t => (ibri, others => 0),          i => (ibri, others => 0));
-- constant intl  : instance := (ven => TELECOM_PARISTECH, d => INTL,     c => WD & WU & WC, ver => 0, nirq => 3, msk => X"FFF", addr => X"201", nt => 1, ni => 1, t => (ibri, others => 0),          i => (ibri, others => 0));
-- constant mapp  : instance := (ven => TELECOM_PARISTECH, d => MAPPER,   c => WD & WU & WC, ver => 0, nirq => 3, msk => X"FFF", addr => X"203", nt => 1, ni => 1, t => (ibri, others => 0),          i => (ibri, others => 0));

-- For ZigBee TX:
--   MAPPER receives data from CPU or (using internal DMA) from DDR through bridge. Sends data to INTL.
--   INTL receives data from MAPPER. INTL sends data to FEP.
--   FEP receives data from INTL. FEP sends data to ADAIF.
--   ADAIF receives data from FEP.
--   All can receive/send data from/to bridge.
  constant zynq_cross : instance := (ven => TELECOM_PARISTECH, d => CROSSBAR, c => ND & NU & NC, ver => 0, nirq =>  1, msk => X"FFF", addr => X"400", nt => 0, ni => 1,
    t => (others => 0),
    i => (ibri, others => 0));
  constant zynq_fep   : instance := (ven => TELECOM_PARISTECH, d => FEP,      c => WD & WU & WC, ver => 0, nirq =>  3, msk => X"FFF", addr => X"402", nt => 4, ni => 4,
    t => (ibri, iintl, imapp, iram, others => 0),
    i => (ibri, iintl, imapp, iram, others => 0));
  constant zynq_intl  : instance := (ven => TELECOM_PARISTECH, d => INTL,     c => WD & WU & WC, ver => 0, nirq =>  3, msk => X"FFF", addr => X"404", nt => 4, ni => 4,
    t => (ibri, ifep, imapp, iram, others => 0),
    i => (ibri, ifep, imapp, iram, others => 0));
  constant zynq_mapper: instance := (ven => TELECOM_PARISTECH, d => MAPPER,   c => WD & WU & WC, ver => 0, nirq =>  3, msk => X"FFF", addr => X"405", nt => 4, ni => 4,
    t => (ibri, ifep, iintl, iram, others => 0),
    i => (ibri, ifep, iintl, iram, others => 0));
  constant zynq_ram: instance := (ven => TELECOM_PARISTECH, d => RAM,   c => WD & NU & NC, ver => 0, nirq =>  1, msk => X"FFF", addr => X"403", nt => 4, ni => 4,
    t => (ibri, ifep, iintl, imapp, others => 0),
    i => (ibri, ifep, iintl, imapp, others => 0));
  constant zynq_bri   : instance := (ven => TELECOM_PARISTECH, d => BRIDGE,   c => ND & NU & NC, ver => 0, nirq =>  0, msk => X"E00", addr => X"000", nt => 5, ni => 4,
    t => (icross, ifep, iintl, imapp, iram, others => 0),
    i => (ifep, iintl, imapp, iram, others => 0));

-- EXEMPLES
--  constant instances : vinstance := (
--   iram => ram,
--   iadc => adc,
--   iadaif => adaif,
--   ifep => fep,
--   iram => ram,
--   imapp  => mapp,
--   iintl  => intl,
--   others => no_instance);

  constant zynq_instances : vinstance := (
    icross  => zynq_cross,
    ifep    => zynq_fep,
    iintl   => zynq_intl,
    imapp   => zynq_mapper,
    iram    => zynq_ram,
    ibri    => zynq_bri,
    others  => no_instance
  );

  -- Assign the definition corresponding to your target hardware platform
  constant instances: vinstance := zynq_instances;
  
  -- END UPDATE

end package crossbar_map;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
