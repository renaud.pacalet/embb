#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

# Vivado synthesis script.

proc usage {} {
    puts "\
usage: vivado <options> \[-mode batch\] -source  <script> -tclargs \"<helper>\"
  <options>:  precision command line options
  <script>:   TCL synthesis script
  <helper>:   helper TCL script"
}

if { [llength $argv] == 1 } {
    set HELPER [lindex $argv 0]
} else {
    usage
    exit -1
}

# Source helper script
puts ${HELPER}
source ${HELPER}

set period 20.0
set frequency [expr 1000.0 / $period]
set part "xc7z020clg484-1"
set board "em.avnet.com:zed:part0:1.3"
array set ios {
	"ledsmode[0]"	{ "F22" "LVCMOS25" }
	"ledsmode[1]"	{ "G22" "LVCMOS25" }
	"led[0]"	{ "T22" "LVCMOS25" }
	"led[1]"	{ "T21" "LVCMOS25" }
	"led[2]"	{ "U22" "LVCMOS25" }
	"led[3]"	{ "U21" "LVCMOS25" }
	"led[4]"	{ "V22" "LVCMOS25" }
	"led[5]"	{ "W22" "LVCMOS25" }
	"led[6]"	{ "U19" "LVCMOS25" }
	"led[7]"	{ "U14" "LVCMOS25" }
}

puts "*********************************************"
puts "summary of build parameters"
puts "*********************************************"
puts "board:  $board"
puts "part:   $part"
puts "period: $period"
puts "*********************************************"

set ip embb_wrapper
set lib EMBB
set vendor www.telecom-paristech.fr

#############
# Create IP #
#############
create_project -part $part -force $ip $ip
add_files ../../pr/fmir4zynq_lib.embb_wrapper.last/embb_wrapper.v
import_files -force -norecurse
ipx::package_project -root_dir $ip -vendor $vendor -library $lib -force $ip
close_project
puts "$ip IP created"

##################
## Create design #
##################

set top top
create_project -part $part -force $top .
set_property TARGET_LANGUAGE VHDL [current_project]
set_property board_part $board [current_project]
set_property ip_repo_paths ./$ip [current_fileset]
update_ip_catalog
create_bd_design "$top"
set embb [create_bd_cell -type ip -vlnv [get_ipdefs *$vendor:$lib:$ip:*] embb]
set ps [create_bd_cell -type ip -vlnv [get_ipdefs *xilinx.com:ip:processing_system7:*] ps]
apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" } $ps
set_property -dict [list CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ $frequency] $ps
set_property -dict [list CONFIG.PCW_USE_M_AXI_GP0 {1}] $ps
set_property -dict [list CONFIG.PCW_M_AXI_GP0_ENABLE_STATIC_REMAP {1}] $ps
set_property -dict [list CONFIG.PCW_USE_S_AXI_HP0 {1}] $ps
set_property -dict [list CONFIG.PCW_USE_FABRIC_INTERRUPT {1}] $ps
set_property -dict [list CONFIG.PCW_CORE0_FIQ_INTR {0}] $ps
set_property -dict [list CONFIG.PCW_IRQ_F2P_INTR {1}] $ps

# Interconnections
# Primary IOs
create_bd_port -dir O -from 7 -to 0 led
connect_bd_net [get_bd_pins $embb/led] [get_bd_ports led]
create_bd_port -dir I -from 1 -to 0 ledsmode 
connect_bd_net [get_bd_pins $embb/ledsmode] [get_bd_ports ledsmode]
# ps - embb
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config [list Master $ps/M_AXI_GP0 Clk "Auto"]  [get_bd_intf_pins $embb/s_axi]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config [list Master $embb/m_axi Clk "Auto"]  [get_bd_intf_pins $ps/S_AXI_HP0]
connect_bd_net [get_bd_pins $embb/irq] [get_bd_pins $ps/IRQ_F2P]

# Addresses ranges
set_property range 1G [get_bd_addr_segs $ps/*embb*]
set_property offset 0x40000000 [get_bd_addr_segs $ps/*embb*]
set_property range 512M [get_bd_addr_segs $embb/*ps*]
set_property offset 0x00000000 [get_bd_addr_segs $embb/*ps*]

# Synthesis
validate_bd_design
set files [get_files *$top.bd]
generate_target all $files
add_files -norecurse -force [make_wrapper -files $files -top]
save_bd_design
set run [get_runs synth*]
set_property STEPS.SYNTH_DESIGN.ARGS.FLATTEN_HIERARCHY full $run
launch_runs $run
wait_on_run $run
open_run $run

# IOs
foreach io [ array names ios ] {
	set pin [ lindex $ios($io) 0 ]
	set std [ lindex $ios($io) 1 ]
	set_property package_pin $pin [get_ports $io]
	set_property iostandard $std [get_ports [list $io]]
}

# Timing constraints
set clock [get_clocks]
set_false_path -from $clock -to [get_ports {led[*]}]
set_false_path -from [get_ports {ledsmode[*]}] -to $clock

# Implementation
save_constraints
set run [get_runs impl*]
reset_run $run
set_property STEPS.OPT_DESIGN.ARGS.DIRECTIVE RuntimeOptimized $run
set_property STEPS.PLACE_DESIGN.ARGS.DIRECTIVE RuntimeOptimized $run
set_property STEPS.ROUTE_DESIGN.ARGS.DIRECTIVE RuntimeOptimized $run
set_property STEPS.WRITE_BITSTREAM.ARGS.BIN_FILE true $run
launch_runs -to_step write_bitstream $run
wait_on_run $run

# Messages
set rundir [pwd]/$top.runs/$run
puts ""
puts "\[VIVADO\]: done"
puts "*********************************************"
puts "summary of build parameters"
puts "*********************************************"
puts "board:  $board"
puts "part:   $part"
puts "period: $period"
puts "*********************************************"
puts "bitstream in $rundir/${top}_wrapper.bit"
puts "resource utilization report in $rundir/${top}_wrapper_utilization_placed.rpt"
puts "timing report in $rundir/${top}_wrapper_timing_summary_routed.rpt"
puts "*********************************************"

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
