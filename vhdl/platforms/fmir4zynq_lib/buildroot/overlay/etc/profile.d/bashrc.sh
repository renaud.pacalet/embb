#!/usr/bin/env bash

#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

alias ll="ls -algs"

if [ -d /root/bin ]; then
	export PATH=$PATH:/root/bin
fi

export PS1='\u@Embb> '

cat <<!
                                                                                    
                                             bbbbbbbb           bbbbbbbb            
EEEEEEEEEEEEEEEEEEEEEE                       b::::::b           b::::::b            
E::::::::::::::::::::E                       b::::::b           b::::::b            
E::::::::::::::::::::E                       b::::::b           b::::::b            
EE::::::EEEEEEEEE::::E                        b:::::b            b:::::b            
  E:::::E       EEEEEE   mmmmmmm    mmmmmmm   b:::::bbbbbbbbb    b:::::bbbbbbbbb    
  E:::::E              mm:::::::m  m:::::::mm b::::::::::::::bb  b::::::::::::::bb  
  E::::::EEEEEEEEEE   m::::::::::mm::::::::::mb::::::::::::::::b b::::::::::::::::b 
  E:::::::::::::::E   m::::::::::::::::::::::mb:::::bbbbb:::::::bb:::::bbbbb:::::::b
  E:::::::::::::::E   m:::::mmm::::::mmm:::::mb:::::b    b::::::bb:::::b    b::::::b
  E::::::EEEEEEEEEE   m::::m   m::::m   m::::mb:::::b     b:::::bb:::::b     b:::::b
  E:::::E             m::::m   m::::m   m::::mb:::::b     b:::::bb:::::b     b:::::b
  E:::::E       EEEEEEm::::m   m::::m   m::::mb:::::b     b:::::bb:::::b     b:::::b
EE::::::EEEEEEEE:::::Em::::m   m::::m   m::::mb:::::bbbbbb::::::bb:::::bbbbbb::::::b
E::::::::::::::::::::Em::::m   m::::m   m::::mb::::::::::::::::b b::::::::::::::::b 
E::::::::::::::::::::Em::::m   m::::m   m::::mb:::::::::::::::b  b:::::::::::::::b  
EEEEEEEEEEEEEEEEEEEEEEmmmmmm   mmmmmm   mmmmmmbbbbbbbbbbbbbbbb   bbbbbbbbbbbbbbbb   
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
(C) Telecom ParisTech
https://embb-paristech.fr/

username / password
  root       embb
!

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
