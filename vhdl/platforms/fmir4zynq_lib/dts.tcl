#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

if { $argc != 3 } {
	puts "usage: hsi -mode batch -quiet -notrace -source xzdt.tcl -tclargs <hardware-description-file> <path-to-device-tree-xlnx> <local-device-tree-directory>"
} else {
	set hdf [lindex $argv 0]
	set xdts [lindex $argv 1]
	set ldts [lindex $argv 2]
	open_hw_design $hdf
	set_repo_path $xdts
	create_sw_design device-tree -os device_tree -proc ps7_cortexa9_0
	generate_target -dir $ldts
}

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
