--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.axi_pkg_m8.all;

entity embb_top_sim is
  generic(
    na: positive range 2 to 32 := 30 -- Useful bit-width of address bus
  );
end entity embb_top_sim;

architecture sim of embb_top_sim is

  signal aclk : std_ulogic;
  signal aresetn : std_ulogic;

    -----------------------------------------------------------------------   
    -- AXI4 lite 32 bits slave interface                                                    
    -----------------------------------------------------------------------  

    -- Write Address Channel                                         
  signal s_axi_awaddr : std_ulogic_vector(na - 1 downto 0);          
  signal s_axi_awprot : std_ulogic_vector(2  downto 0);          
  signal s_axi_awvalid : std_ulogic;    
  signal s_axi_awready : std_ulogic;          

    -- Write Data Channel                                            
  signal s_axi_wstrb : std_ulogic_vector(3 downto 0);
  signal s_axi_wdata : std_ulogic_vector(31 downto 0);
  signal s_axi_wvalid : std_ulogic;    
  signal s_axi_wready : std_ulogic;           

    -- Write Response Channel                             
  signal s_axi_bresp : std_ulogic_vector(1 downto 0); 
  signal s_axi_bvalid : std_ulogic;
  signal s_axi_bready : std_ulogic;

    -- Read Address Channel           
  signal s_axi_araddr : std_ulogic_vector(na - 1 downto 0);          
  signal s_axi_arprot : std_ulogic_vector(2  downto 0);          
  signal s_axi_arvalid : std_ulogic;    
  signal s_axi_arready : std_ulogic;         

    -- Read Data Channel          
  signal s_axi_rdata : std_ulogic_vector(31 downto 0);
  signal s_axi_rresp : std_ulogic_vector(1 downto 0);
  signal s_axi_rvalid : std_ulogic;
  signal s_axi_rready : std_ulogic;

    -----------------------------------------------------------------------      
    -- AXI4 64 bits master interface                                                   
    -----------------------------------------------------------------------     

    -- Write Address Channel                                         
  signal m_axi_awid : std_ulogic_vector(axi_i - 1 downto 0);          
  signal m_axi_awaddr : std_ulogic_vector(axi_a - 1 downto 0);          
  signal m_axi_awlen : std_ulogic_vector(axi_l - 1  downto 0);          
  signal m_axi_awsize : std_ulogic_vector(axi_s - 1  downto 0);          
  signal m_axi_awburst : std_ulogic_vector(axi_b - 1  downto 0);          
  signal m_axi_awlock : std_ulogic_vector(axi_o - 1  downto 0);          
  signal m_axi_awcache : std_ulogic_vector(axi_c - 1  downto 0);          
  signal m_axi_awprot : std_ulogic_vector(axi_p - 1  downto 0);          
  signal m_axi_awqos : std_ulogic_vector(axi_q - 1  downto 0);          
  signal m_axi_awvalid : std_ulogic;    
  signal m_axi_awready : std_ulogic;          

    -- Write Data Channel                                            
  signal m_axi_wstrb : std_ulogic_vector(axi_m - 1 downto 0);
  signal m_axi_wdata : std_ulogic_vector(axi_d - 1 downto 0);
  signal m_axi_wlast : std_ulogic;
  signal m_axi_wvalid : std_ulogic;    
  signal m_axi_wready : std_ulogic;           

    -- Write Response Channel                             
  signal m_axi_bid : std_ulogic_vector(axi_i - 1 downto 0); 
  signal m_axi_bresp : std_ulogic_vector(axi_r - 1 downto 0); 
  signal m_axi_bvalid : std_ulogic;
  signal m_axi_bready : std_ulogic;

    -- Read Address Channel           
  signal m_axi_arid : std_ulogic_vector(axi_i - 1 downto 0);          
  signal m_axi_araddr : std_ulogic_vector(axi_a - 1 downto 0);          
  signal m_axi_arlen : std_ulogic_vector(axi_l - 1  downto 0);          
  signal m_axi_arsize : std_ulogic_vector(axi_s - 1  downto 0);          
  signal m_axi_arburst : std_ulogic_vector(axi_b - 1  downto 0);          
  signal m_axi_arlock : std_ulogic_vector(axi_o - 1  downto 0);          
  signal m_axi_arcache : std_ulogic_vector(axi_c - 1  downto 0);          
  signal m_axi_arprot : std_ulogic_vector(axi_p - 1  downto 0);          
  signal m_axi_arqos : std_ulogic_vector(axi_q - 1  downto 0);          
  signal m_axi_arvalid : std_ulogic;    
  signal m_axi_arready : std_ulogic;         

    -- Read Data Channel          
  signal m_axi_rid : std_ulogic_vector(axi_i - 1 downto 0);          
  signal m_axi_rdata : std_ulogic_vector(axi_d - 1 downto 0);
  signal m_axi_rresp : std_ulogic_vector(axi_r - 1 downto 0);
  signal m_axi_rlast : std_ulogic;
  signal m_axi_rvalid : std_ulogic;
  signal m_axi_rready : std_ulogic;

    -----------------------------------------------------------------------   
    -- Interrupt, emulated mode indicator
    -----------------------------------------------------------------------  

  signal irq : std_ulogic;

    -----------------------------------------------------------------------   
    -- Debug
    -----------------------------------------------------------------------  

  signal ledsmode : std_ulogic_vector(1 downto 0);
  signal led : std_ulogic_vector(7 downto 0);

  signal eos: boolean := false;

begin

  process
  begin
    aclk <= '0';
    wait for 1 ns;
    aclk <= '1';
    wait for 1 ns;
    if eos then
      wait;
    end if;
  end process;

  process
  begin
    aresetn       <= '0';
    s_axi_awaddr  <= (others => '0');
    s_axi_awprot  <= (others => '0');
    s_axi_awvalid <= '0';
    s_axi_wstrb   <= (others => '0');
    s_axi_wdata   <= (others => '0');
    s_axi_wvalid  <= '0';
    s_axi_bready  <= '0';
    s_axi_araddr  <= (others => '0');
    s_axi_arprot  <= (others => '0');
    s_axi_arvalid <= '0';
    s_axi_rready  <= '0';
    m_axi_arready <= '0';
    m_axi_awready <= '0';
    m_axi_wready  <= '0';
    m_axi_rid     <= (others => '0');
    m_axi_rdata   <= (others => '0');
    m_axi_rresp   <= (others => '0');
    m_axi_rlast   <= '0';
    m_axi_rvalid  <= '0';
    m_axi_bid     <= (others => '0');
    m_axi_bresp   <= (others => '0');
    m_axi_bvalid  <= '0';
    ledsmode      <= (others => '0');
    for i in 1 to 10 loop
      wait until rising_edge(aclk);
    end loop;
    aresetn <= '1';

    s_axi_awaddr <= "00" & X"00fff00";
    s_axi_awprot <= "000";
    s_axi_awvalid <= '1';
    s_axi_wstrb <= X"f";
    s_axi_wdata <= X"ffffffff";
    s_axi_wvalid <= '1';
    loop
      wait until rising_edge(aclk);
      if s_axi_awready = '1' then
        s_axi_awvalid <= '0';
      end if;
      if s_axi_wready = '1' then
        s_axi_wvalid <= '0';
      end if;
      if s_axi_bvalid = '1' then
        s_axi_bready <= '1';
      end if;
      if s_axi_bready = '1' then
        s_axi_bready <= '0';
        exit;
      end if;
    end loop;

    s_axi_araddr <= "00" & X"00fff00";
    s_axi_arprot <= "000";
    s_axi_arvalid <= '1';

    loop
      wait until rising_edge(aclk);
      if s_axi_arready = '1' then
        s_axi_arvalid <= '0';
      end if;
      if s_axi_rvalid = '1' then
        s_axi_rready <= '1';
      end if;
      if s_axi_rready = '1' then
        s_axi_rready <= '0';
        exit;
      end if;
    end loop;
    eos <= true;
    wait;
  end process;

  iembb_top: entity work.embb_top
  port map(
    aclk                   => aclk,
    aresetn                => aresetn,

    -----------------------------------------------------------------------   
    -- AXI4 lite 32 bits slave interface                                                    
    -----------------------------------------------------------------------  

    -- Write Address Channel                                         
    s_axi_awaddr           => s_axi_awaddr,
    s_axi_awprot           => s_axi_awprot,
    s_axi_awvalid          => s_axi_awvalid,
    s_axi_awready          => s_axi_awready,

    -- Write Data Channel                                            
    s_axi_wstrb            => s_axi_wstrb,
    s_axi_wdata            => s_axi_wdata,
    s_axi_wvalid           => s_axi_wvalid,
    s_axi_wready           => s_axi_wready,

    -- Write Response Channel                             
    s_axi_bresp            => s_axi_bresp,
    s_axi_bvalid           => s_axi_bvalid,
    s_axi_bready           => s_axi_bready,

    -- Read Address Channel           
    s_axi_araddr           => s_axi_araddr,
    s_axi_arprot           => s_axi_arprot,
    s_axi_arvalid          => s_axi_arvalid,
    s_axi_arready          => s_axi_arready,

    -- Read Data Channel          
    s_axi_rdata            => s_axi_rdata,
    s_axi_rresp            => s_axi_rresp,
    s_axi_rvalid           => s_axi_rvalid,
    s_axi_rready           => s_axi_rready,

    -----------------------------------------------------------------------      
    -- AXI4 64 bits master interface                                                   
    -----------------------------------------------------------------------     

    -- Write Address Channel                                         
    m_axi_awid             => m_axi_awid,
    m_axi_awaddr           => m_axi_awaddr,
    m_axi_awlen            => m_axi_awlen,
    m_axi_awsize           => m_axi_awsize,
    m_axi_awburst          => m_axi_awburst,
    m_axi_awlock           => m_axi_awlock,
    m_axi_awcache          => m_axi_awcache,
    m_axi_awprot           => m_axi_awprot,
    m_axi_awqos            => m_axi_awqos,
    m_axi_awvalid          => m_axi_awvalid,
    m_axi_awready          => m_axi_awready,

    -- Write Data Channel                                            
    m_axi_wstrb            => m_axi_wstrb,
    m_axi_wdata            => m_axi_wdata,
    m_axi_wlast            => m_axi_wlast,
    m_axi_wvalid           => m_axi_wvalid,
    m_axi_wready           => m_axi_wready,

    -- Write Response Channel                             
    m_axi_bid              => m_axi_bid,
    m_axi_bresp            => m_axi_bresp,
    m_axi_bvalid           => m_axi_bvalid,
    m_axi_bready           => m_axi_bready,

    -- Read Address Channel           
    m_axi_arid             => m_axi_arid,
    m_axi_araddr           => m_axi_araddr,
    m_axi_arlen            => m_axi_arlen,
    m_axi_arsize           => m_axi_arsize,
    m_axi_arburst          => m_axi_arburst,
    m_axi_arlock           => m_axi_arlock,
    m_axi_arcache          => m_axi_arcache,
    m_axi_arprot           => m_axi_arprot,
    m_axi_arqos            => m_axi_arqos,
    m_axi_arvalid          => m_axi_arvalid,
    m_axi_arready          => m_axi_arready,

    -- Read Data Channel          
    m_axi_rid              => m_axi_rid,
    m_axi_rdata            => m_axi_rdata,
    m_axi_rresp            => m_axi_rresp,
    m_axi_rlast            => m_axi_rlast,
    m_axi_rvalid           => m_axi_rvalid,
    m_axi_rready           => m_axi_rready,

    -----------------------------------------------------------------------   
    -- Interrupt, emulated mode indicator
    -----------------------------------------------------------------------  

    irq                    => irq,

    -----------------------------------------------------------------------   
    -- Debug
    -----------------------------------------------------------------------  

    ledsmode               => ledsmode,
    led                    => led
  );

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
