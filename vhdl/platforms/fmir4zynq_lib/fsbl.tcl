#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

if { $argc != 2 } {
	puts "usage: hsi -mode batch -quiet -notrace -source xzfsbl.tcl -tclargs <hardware-description-file> <fsbl-build-directory>"
} else {
	set hdf [lindex $argv 0]
	set dir [lindex $argv 1]
	set design [ open_hw_design ${hdf} ]
	generate_app -hw $design -os standalone -proc ps7_cortexa9_0 -app zynq_fsbl -sw fsbl -dir ${dir}
}

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
