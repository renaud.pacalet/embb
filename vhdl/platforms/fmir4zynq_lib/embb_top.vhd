--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

    -----------------------------------------------------------------------   
    -- Debug
    -----------------------------------------------------------------------  

    -- LEDs can be activity indicators (ledsmode=0), driven by a custom 64 bits parameter register of the FEP (1), events indicators (2), internal general purpose register of the AXI-VCI bridge (3)
    -- ledsmode = 0:
    --   0:   Switches every period of 2**22 aclk cycles
    --   1-7: 0
    -- ledsmode = 1:
    --   led(i) = or_reduce(FEP.R3(8 * i + 7 downto 8 * i))
    -- ledsmode = 2:
    --   led(i) illuminates if the corresponding events occur:
    --   0: Crossbar interrupt
    --   1: FEP DMA interrupt
    --   2: FEP PSS interrupt
    --   3: INTL DMA interrupt
    --   4: INTL PSS interrupt
    --   5: MAPPER DMA interrupt
    --   6: MAPPER PSS interrupt
    --   7: 0
    -- ledsmode = 3:
    --   0-3: or_reduce(SLAVE_BRIDGE.GPR(8 * i + 7 downto 8 * i))
    --   4-7: 0
    -----------------------------------------------------------------------     

library ieee;
use ieee.std_logic_1164.all;

entity embb_top is
	generic(
		na:	positive range 2 to 32		:= 30;	-- Useful bit-width of AXI slave address bus
		ba:	std_ulogic_vector(31 downto 30)	:= "01"	-- Base address of AXI slave interface
	);
	port(
		aclk:		in	std_ulogic;
		aresetn:	in	std_ulogic;
		s_axi_awaddr:	in	std_ulogic_vector(na - 1 downto 0);          
		s_axi_awprot:	in	std_ulogic_vector(2  downto 0);          
		s_axi_awvalid:	in	std_ulogic;    
		s_axi_awready:	out	std_ulogic;          
		s_axi_wstrb:	in	std_ulogic_vector(3 downto 0);
		s_axi_wdata:	in	std_ulogic_vector(31 downto 0);
		s_axi_wvalid:	in	std_ulogic;    
		s_axi_wready:	out	std_ulogic;           
		s_axi_bresp:	out	std_ulogic_vector(1 downto 0); 
		s_axi_bvalid:	out	std_ulogic;
		s_axi_bready:	in	std_ulogic;
		s_axi_araddr:	in	std_ulogic_vector(na - 1 downto 0);          
		s_axi_arprot:	in	std_ulogic_vector(2  downto 0);          
		s_axi_arvalid:	in	std_ulogic;    
		s_axi_arready:	out	std_ulogic;         
		s_axi_rdata:	out	std_ulogic_vector(31 downto 0);
		s_axi_rresp:	out	std_ulogic_vector(1 downto 0);
		s_axi_rvalid:	out	std_ulogic;
		s_axi_rready:	in	std_ulogic;
		m_axi_awid:	out	std_ulogic_vector(5 downto 0);          
		m_axi_awaddr:	out	std_ulogic_vector(31 downto 0);          
		m_axi_awlen:	out	std_ulogic_vector(7  downto 0);          
		m_axi_awsize:	out	std_ulogic_vector(2  downto 0);          
		m_axi_awburst:	out	std_ulogic_vector(1  downto 0);          
		m_axi_awlock:	out	std_ulogic_vector(0  downto 0);          
		m_axi_awcache:	out	std_ulogic_vector(3  downto 0);          
		m_axi_awprot:	out	std_ulogic_vector(2  downto 0);          
		m_axi_awqos:	out	std_ulogic_vector(3  downto 0);          
		m_axi_awvalid:	out	std_ulogic;    
		m_axi_awready:	in	std_ulogic;          
		m_axi_wstrb:	out	std_ulogic_vector(7 downto 0);
		m_axi_wdata:	out	std_ulogic_vector(63 downto 0);
		m_axi_wlast:	out	std_ulogic;
		m_axi_wvalid:	out	std_ulogic;    
		m_axi_wready:	in	std_ulogic;           
		m_axi_bid:	in	std_ulogic_vector(5 downto 0); 
		m_axi_bresp:	in	std_ulogic_vector(1 downto 0); 
		m_axi_bvalid:	in	std_ulogic;
		m_axi_bready:	out	std_ulogic;
		m_axi_arid:	out	std_ulogic_vector(5 downto 0);          
		m_axi_araddr:	out	std_ulogic_vector(31 downto 0);          
		m_axi_arlen:	out	std_ulogic_vector(7  downto 0);          
		m_axi_arsize:	out	std_ulogic_vector(2  downto 0);          
		m_axi_arburst:	out	std_ulogic_vector(1  downto 0);          
		m_axi_arlock:	out	std_ulogic_vector(0  downto 0);          
		m_axi_arcache:	out	std_ulogic_vector(3  downto 0);          
		m_axi_arprot:	out	std_ulogic_vector(2  downto 0);          
		m_axi_arqos:	out	std_ulogic_vector(3  downto 0);          
		m_axi_arvalid:	out	std_ulogic;    
		m_axi_arready:	in	std_ulogic;         
		m_axi_rid:	in	std_ulogic_vector(5 downto 0);          
		m_axi_rdata:	in	std_ulogic_vector(63 downto 0);
		m_axi_rresp:	in	std_ulogic_vector(1 downto 0);
		m_axi_rlast:	in	std_ulogic;
		m_axi_rvalid:	in	std_ulogic;
		m_axi_rready:	out	std_ulogic;
		irq:		out	std_ulogic;
		ledsmode:	in	std_ulogic_vector(1 downto 0);
		led:		out	std_ulogic_vector(7 downto 0)
	);
end entity embb_top;

architecture rtl of embb_top is

	component embb_wrapper is
		port(
			aclk:		in	std_ulogic;
			aresetn:	in	std_ulogic;
			s_axi_awaddr:	in	std_ulogic_vector(na - 1 downto 0);          
			s_axi_awprot:	in	std_ulogic_vector(2  downto 0);          
			s_axi_awvalid:	in	std_ulogic;    
			s_axi_awready:	out	std_ulogic;          
			s_axi_wstrb:	in	std_ulogic_vector(3 downto 0);
			s_axi_wdata:	in	std_ulogic_vector(31 downto 0);
			s_axi_wvalid:	in	std_ulogic;    
			s_axi_wready:	out	std_ulogic;           
			s_axi_bresp:	out	std_ulogic_vector(1 downto 0); 
			s_axi_bvalid:	out	std_ulogic;
			s_axi_bready:	in	std_ulogic;
			s_axi_araddr:	in	std_ulogic_vector(na - 1 downto 0);          
			s_axi_arprot:	in	std_ulogic_vector(2  downto 0);          
			s_axi_arvalid:	in	std_ulogic;    
			s_axi_arready:	out	std_ulogic;         
			s_axi_rdata:	out	std_ulogic_vector(31 downto 0);
			s_axi_rresp:	out	std_ulogic_vector(1 downto 0);
			s_axi_rvalid:	out	std_ulogic;
			s_axi_rready:	in	std_ulogic;
			m_axi_awid:	out	std_ulogic_vector(5 downto 0);          
			m_axi_awaddr:	out	std_ulogic_vector(31 downto 0);          
			m_axi_awlen:	out	std_ulogic_vector(7  downto 0);          
			m_axi_awsize:	out	std_ulogic_vector(2  downto 0);          
			m_axi_awburst:	out	std_ulogic_vector(1  downto 0);          
			m_axi_awlock:	out	std_ulogic_vector(0  downto 0);          
			m_axi_awcache:	out	std_ulogic_vector(3  downto 0);          
			m_axi_awprot:	out	std_ulogic_vector(2  downto 0);          
			m_axi_awqos:	out	std_ulogic_vector(3  downto 0);          
			m_axi_awvalid:	out	std_ulogic;    
			m_axi_awready:	in	std_ulogic;          
			m_axi_wstrb:	out	std_ulogic_vector(7 downto 0);
			m_axi_wdata:	out	std_ulogic_vector(63 downto 0);
			m_axi_wlast:	out	std_ulogic;
			m_axi_wvalid:	out	std_ulogic;    
			m_axi_wready:	in	std_ulogic;           
			m_axi_bid:	in	std_ulogic_vector(5 downto 0); 
			m_axi_bresp:	in	std_ulogic_vector(1 downto 0); 
			m_axi_bvalid:	in	std_ulogic;
			m_axi_bready:	out	std_ulogic;
			m_axi_arid:	out	std_ulogic_vector(5 downto 0);          
			m_axi_araddr:	out	std_ulogic_vector(31 downto 0);          
			m_axi_arlen:	out	std_ulogic_vector(7  downto 0);          
			m_axi_arsize:	out	std_ulogic_vector(2  downto 0);          
			m_axi_arburst:	out	std_ulogic_vector(1  downto 0);          
			m_axi_arlock:	out	std_ulogic_vector(0  downto 0);          
			m_axi_arcache:	out	std_ulogic_vector(3  downto 0);          
			m_axi_arprot:	out	std_ulogic_vector(2  downto 0);          
			m_axi_arqos:	out	std_ulogic_vector(3  downto 0);          
			m_axi_arvalid:	out	std_ulogic;    
			m_axi_arready:	in	std_ulogic;         
			m_axi_rid:	in	std_ulogic_vector(5 downto 0);          
			m_axi_rdata:	in	std_ulogic_vector(63 downto 0);
			m_axi_rresp:	in	std_ulogic_vector(1 downto 0);
			m_axi_rlast:	in	std_ulogic;
			m_axi_rvalid:	in	std_ulogic;
			m_axi_rready:	out	std_ulogic;
			irq:		out	std_ulogic;
			ledsmode:	in	std_ulogic_vector(1 downto 0);
			led:		out	std_ulogic_vector(7 downto 0)
		);
	end component embb_wrapper;

	-- pragma translate_off
	for all: embb_wrapper use entity work.embb_wrapper(rtl);
	-- pragma translate_on

begin
 
	u0: embb_wrapper
		port map(
			aclk		=>	aclk,
			aresetn		=>	aresetn,
			s_axi_awaddr	=>	s_axi_awaddr,
			s_axi_awprot	=>	s_axi_awprot,
			s_axi_awvalid	=>	s_axi_awvalid,
			s_axi_awready	=>	s_axi_awready,
			s_axi_wstrb	=>	s_axi_wstrb,
			s_axi_wdata	=>	s_axi_wdata,
			s_axi_wvalid	=>	s_axi_wvalid,
			s_axi_wready	=>	s_axi_wready,
			s_axi_bresp	=>	s_axi_bresp,
			s_axi_bvalid	=>	s_axi_bvalid,
			s_axi_bready	=>	s_axi_bready,
			s_axi_araddr	=>	s_axi_araddr,
			s_axi_arprot	=>	s_axi_arprot,
			s_axi_arvalid	=>	s_axi_arvalid,
			s_axi_arready	=>	s_axi_arready,
			s_axi_rdata	=>	s_axi_rdata,
			s_axi_rresp	=>	s_axi_rresp,
			s_axi_rvalid	=>	s_axi_rvalid,
			s_axi_rready	=>	s_axi_rready,
			m_axi_awid	=>	m_axi_awid,
			m_axi_awaddr	=>	m_axi_awaddr,
			m_axi_awlen	=>	m_axi_awlen,
			m_axi_awsize	=>	m_axi_awsize,
			m_axi_awburst	=>	m_axi_awburst,
			m_axi_awlock	=>	m_axi_awlock,
			m_axi_awcache	=>	m_axi_awcache,
			m_axi_awprot	=>	m_axi_awprot,
			m_axi_awqos	=>	m_axi_awqos,
			m_axi_awvalid	=>	m_axi_awvalid,
			m_axi_awready	=>	m_axi_awready,
			m_axi_wstrb	=>	m_axi_wstrb,
			m_axi_wdata	=>	m_axi_wdata,
			m_axi_wlast	=>	m_axi_wlast,
			m_axi_wvalid	=>	m_axi_wvalid,
			m_axi_wready	=>	m_axi_wready,
			m_axi_bid	=>	m_axi_bid,
			m_axi_bresp	=>	m_axi_bresp,
			m_axi_bvalid	=>	m_axi_bvalid,
			m_axi_bready	=>	m_axi_bready,
			m_axi_arid	=>	m_axi_arid,
			m_axi_araddr	=>	m_axi_araddr,
			m_axi_arlen	=>	m_axi_arlen,
			m_axi_arsize	=>	m_axi_arsize,
			m_axi_arburst	=>	m_axi_arburst,
			m_axi_arlock	=>	m_axi_arlock,
			m_axi_arcache	=>	m_axi_arcache,
			m_axi_arprot	=>	m_axi_arprot,
			m_axi_arqos	=>	m_axi_arqos,
			m_axi_arvalid	=>	m_axi_arvalid,
			m_axi_arready	=>	m_axi_arready,
			m_axi_rid	=>	m_axi_rid,
			m_axi_rdata	=>	m_axi_rdata,
			m_axi_rresp	=>	m_axi_rresp,
			m_axi_rlast	=>	m_axi_rlast,
			m_axi_rvalid	=>	m_axi_rvalid,
			m_axi_rready	=>	m_axi_rready,
			irq		=>	irq,
			ledsmode	=>	ledsmode,
			led		=>	led
		);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
