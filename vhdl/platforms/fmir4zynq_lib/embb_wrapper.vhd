--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.axi_pkg_m4.all;
use global_lib.axi_pkg_m8.all;

entity embb_wrapper is
	generic(
		na:	positive range 2 to 32		:= 30;	-- Useful bit-width of AXI slave address bus
		ba:	std_ulogic_vector(31 downto 30)	:= "01"	-- Base address of AXI slave interface
	);
	port(
		aclk:		in	std_ulogic;
		aresetn:	in	std_ulogic;
		s_axi_awaddr:	in	std_ulogic_vector(na - 1 downto 0);          
		s_axi_awprot:	in	std_ulogic_vector(2  downto 0);          
		s_axi_awvalid:	in	std_ulogic;    
		s_axi_awready:	out	std_ulogic;          
		s_axi_wstrb:	in	std_ulogic_vector(3 downto 0);
		s_axi_wdata:	in	std_ulogic_vector(31 downto 0);
		s_axi_wvalid:	in	std_ulogic;    
		s_axi_wready:	out	std_ulogic;           
		s_axi_bresp:	out	std_ulogic_vector(1 downto 0); 
		s_axi_bvalid:	out	std_ulogic;
		s_axi_bready:	in	std_ulogic;
		s_axi_araddr:	in	std_ulogic_vector(na - 1 downto 0);          
		s_axi_arprot:	in	std_ulogic_vector(2  downto 0);          
		s_axi_arvalid:	in	std_ulogic;    
		s_axi_arready:	out	std_ulogic;         
		s_axi_rdata:	out	std_ulogic_vector(31 downto 0);
		s_axi_rresp:	out	std_ulogic_vector(1 downto 0);
		s_axi_rvalid:	out	std_ulogic;
		s_axi_rready:	in	std_ulogic;
		m_axi_awid:	out	std_ulogic_vector(5 downto 0);          
		m_axi_awaddr:	out	std_ulogic_vector(31 downto 0);          
		m_axi_awlen:	out	std_ulogic_vector(7  downto 0);          
		m_axi_awsize:	out	std_ulogic_vector(2  downto 0);          
		m_axi_awburst:	out	std_ulogic_vector(1  downto 0);          
		m_axi_awlock:	out	std_ulogic_vector(0  downto 0);          
		m_axi_awcache:	out	std_ulogic_vector(3  downto 0);          
		m_axi_awprot:	out	std_ulogic_vector(2  downto 0);          
		m_axi_awqos:	out	std_ulogic_vector(3  downto 0);          
		m_axi_awvalid:	out	std_ulogic;    
		m_axi_awready:	in	std_ulogic;          
		m_axi_wstrb:	out	std_ulogic_vector(7 downto 0);
		m_axi_wdata:	out	std_ulogic_vector(63 downto 0);
		m_axi_wlast:	out	std_ulogic;
		m_axi_wvalid:	out	std_ulogic;    
		m_axi_wready:	in	std_ulogic;           
		m_axi_bid:	in	std_ulogic_vector(5 downto 0); 
		m_axi_bresp:	in	std_ulogic_vector(1 downto 0); 
		m_axi_bvalid:	in	std_ulogic;
		m_axi_bready:	out	std_ulogic;
		m_axi_arid:	out	std_ulogic_vector(5 downto 0);          
		m_axi_araddr:	out	std_ulogic_vector(31 downto 0);          
		m_axi_arlen:	out	std_ulogic_vector(7  downto 0);          
		m_axi_arsize:	out	std_ulogic_vector(2  downto 0);          
		m_axi_arburst:	out	std_ulogic_vector(1  downto 0);          
		m_axi_arlock:	out	std_ulogic_vector(0  downto 0);          
		m_axi_arcache:	out	std_ulogic_vector(3  downto 0);          
		m_axi_arprot:	out	std_ulogic_vector(2  downto 0);          
		m_axi_arqos:	out	std_ulogic_vector(3  downto 0);          
		m_axi_arvalid:	out	std_ulogic;    
		m_axi_arready:	in	std_ulogic;         
		m_axi_rid:	in	std_ulogic_vector(5 downto 0);          
		m_axi_rdata:	in	std_ulogic_vector(63 downto 0);
		m_axi_rresp:	in	std_ulogic_vector(1 downto 0);
		m_axi_rlast:	in	std_ulogic;
		m_axi_rvalid:	in	std_ulogic;
		m_axi_rready:	out	std_ulogic;
		irq:		out	std_ulogic;
		ledsmode:	in	std_ulogic_vector(1 downto 0);
		led:		out	std_ulogic_vector(7 downto 0)
	);
end entity embb_wrapper;

----------------------------------------------------
-- Architecture
----------------------------------------------------

architecture rtl of embb_wrapper is

  signal s_axi_m2s:	global_lib.axi_pkg_m4.axilite_m2s;	-- AXI inputs from master
  signal s_axi_s2m:	global_lib.axi_pkg_m4.axilite_s2m;	-- AXI outputs to master
  signal m_axi_m2s:	global_lib.axi_pkg_m8.axi_m2s;		-- AXI outputs to slave
  signal m_axi_s2m:	global_lib.axi_pkg_m8.axi_s2m;		-- AXI inputs from slave

begin
 
	u0: entity work.embb(rtl)
		port map(
			aclk		=> aclk,
			aresetn		=> aresetn,
			s_axi_m2s	=> s_axi_m2s,
			s_axi_s2m	=> s_axi_s2m,
			m_axi_m2s	=> m_axi_m2s,
			m_axi_s2m	=> m_axi_s2m,
			irq		=> irq,
			ledsmode	=> ledsmode,
			led		=> led
		);

	s_axi_m2s.awaddr  <= ba & std_ulogic_vector(s_axi_awaddr);
	s_axi_m2s.awprot  <= std_ulogic_vector(s_axi_awprot);
	s_axi_m2s.awvalid <= std_ulogic(s_axi_awvalid);
	s_axi_m2s.araddr  <= ba & std_ulogic_vector(s_axi_araddr);
	s_axi_m2s.arprot  <= std_ulogic_vector(s_axi_arprot);
	s_axi_m2s.arvalid <= std_ulogic(s_axi_arvalid);
	s_axi_m2s.rready  <= std_ulogic(s_axi_rready);
	s_axi_m2s.wdata   <= std_ulogic_vector(s_axi_wdata);
	s_axi_m2s.wstrb   <= std_ulogic_vector(s_axi_wstrb);
	s_axi_m2s.wvalid  <= std_ulogic(s_axi_wvalid);
	s_axi_m2s.bready  <= std_ulogic(s_axi_bready);

	s_axi_arready     <= std_ulogic(s_axi_s2m.arready);
	s_axi_awready     <= std_ulogic(s_axi_s2m.awready);
	s_axi_rdata       <= std_ulogic_vector(s_axi_s2m.rdata);
	s_axi_rresp       <= std_ulogic_vector(s_axi_s2m.rresp);
	s_axi_rvalid      <= std_ulogic(s_axi_s2m.rvalid);
	s_axi_wready      <= std_ulogic(s_axi_s2m.wready);
	s_axi_bresp       <= std_ulogic_vector(s_axi_s2m.bresp);
	s_axi_bvalid      <= std_ulogic(s_axi_s2m.bvalid);

	m_axi_awid        <= std_ulogic_vector(m_axi_m2s.awid);
	m_axi_awaddr      <= std_ulogic_vector(m_axi_m2s.awaddr);
	m_axi_awlen       <= std_ulogic_vector(m_axi_m2s.awlen);
	m_axi_awsize      <= std_ulogic_vector(m_axi_m2s.awsize);
	m_axi_awburst     <= std_ulogic_vector(m_axi_m2s.awburst);
	m_axi_awlock      <= std_ulogic_vector(m_axi_m2s.awlock);
	m_axi_awcache     <= std_ulogic_vector(m_axi_m2s.awcache);
	m_axi_awprot      <= std_ulogic_vector(m_axi_m2s.awprot);
	m_axi_awqos       <= std_ulogic_vector(m_axi_m2s.awqos);
	m_axi_awvalid     <= std_ulogic(m_axi_m2s.awvalid);
	m_axi_arid        <= std_ulogic_vector(m_axi_m2s.arid);
	m_axi_araddr      <= std_ulogic_vector(m_axi_m2s.araddr);
	m_axi_arlen       <= std_ulogic_vector(m_axi_m2s.arlen);
	m_axi_arsize      <= std_ulogic_vector(m_axi_m2s.arsize);
	m_axi_arburst     <= std_ulogic_vector(m_axi_m2s.arburst);
	m_axi_arlock      <= std_ulogic_vector(m_axi_m2s.arlock);
	m_axi_arcache     <= std_ulogic_vector(m_axi_m2s.arcache);
	m_axi_arprot      <= std_ulogic_vector(m_axi_m2s.arprot);
	m_axi_arqos       <= std_ulogic_vector(m_axi_m2s.arqos);
	m_axi_arvalid     <= std_ulogic(m_axi_m2s.arvalid);
	m_axi_rready      <= std_ulogic(m_axi_m2s.rready);
	m_axi_wdata       <= std_ulogic_vector(m_axi_m2s.wdata);
	m_axi_wstrb       <= std_ulogic_vector(m_axi_m2s.wstrb);
	m_axi_wlast       <= std_ulogic(m_axi_m2s.wlast);
	m_axi_wvalid      <= std_ulogic(m_axi_m2s.wvalid);
	m_axi_bready      <= std_ulogic(m_axi_m2s.bready);

	m_axi_s2m.arready <= std_ulogic(m_axi_arready);
	m_axi_s2m.awready <= std_ulogic(m_axi_awready);
	m_axi_s2m.rid     <= std_ulogic_vector(m_axi_rid);
	m_axi_s2m.rdata   <= std_ulogic_vector(m_axi_rdata);
	m_axi_s2m.rresp   <= std_ulogic_vector(m_axi_rresp);
	m_axi_s2m.rlast   <= std_ulogic(m_axi_rlast);
	m_axi_s2m.rvalid  <= std_ulogic(m_axi_rvalid);
	m_axi_s2m.wready  <= std_ulogic(m_axi_wready);
	m_axi_s2m.bid     <= std_ulogic_vector(m_axi_bid);
	m_axi_s2m.bresp   <= std_ulogic_vector(m_axi_bresp);
	m_axi_s2m.bvalid  <= std_ulogic(m_axi_bvalid);
 
end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
