<!--
Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
Copyright (C) - Telecom ParisTech
Contacts: contact-embb@telecom-paristech.fr

Embb is governed by the CeCILL license under French law and abiding by the rules
of distribution of free software. You can use, modify and/ or redistribute the
software under the terms of the CeCILL license. You should have received a copy
of the CeCILL license along with this program; if not, you can access it online
at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
-->

This repository and its sub-directories contain the VHDL source code, VHDL simulation environment, simulation, synthesis scripts and companion example software for Embb. It can be ported on any board based on Xilinx Zynq cores but has been specifically designed for the ZebBoard.

All provided instructions are for a host computer running a GNU/Linux operating system and have been tested on a Debian 8 (jessie) distribution. Porting to other GNU/Linux distributions should be very easy. If you are working under Microsoft Windows or Apple Mac OS X, installing a virtualization framework and running a Debian OS in a virtual machine is probably the easiest path.

Please signal errors and send suggestions for improvements to renaud.pacalet@telecom-paristech.fr.

# Table of content
* [License](#License)
* [Content](#Content)
* [Notations](#Notations)
* [Links](#Links)
* [Description](#Description)
* [Install from the archive](#Archive)
* [Test Embb on the ZedBoard](#Run)
* [Build everything from scratch](#Build)
    * [Downloads](#BuildDownloads)
    * [Hardware synthesis](#BuildSynthesis)
    * [Build a root file system](#BuildRootFs)
    * [Build the Linux kernel](#BuildKernel)
    * [Build the Embb software](#BuildEmbbSoftware)
    * [Build U-Boot](#BuildUboot)
    * [Build the kernel device tree](#BuildDTS)
    * [Build the First Stage Boot Loader (FSBL)](#BuildFSBL)
    * [Build the Zynq boot image](#BuildBootImg)
    * [Prepare the SD card](#BuildSDCard)

# <a name="License"></a>License

Copyright Telecom ParisTech  
Copyright Renaud Pacalet (renaud.pacalet@telecom-paristech.fr)

Licensed uder the CeCILL license, Version 2.1 of
2013-06-21 (the "License"). You should have
received a copy of the License. Else, you may
obtain a copy of the License at:

http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt

# <a name="Content"></a>Content

TODO

# <a name="Notations"></a>Notations

Example code blocks use different prompts for different contexts:

* `Host>`: regular user on host PC.
* `Embb>`: root user on ZedBoard.

# <a name="Links"></a>Links

* [Buildroot](https://buildroot.org/)
* [U-Boot](http://www.denx.de/wiki/U-Boot)

# <a name="Description"></a>Description

**Embb** is a generic Digital Signal Processor (DSP) dedicated to demanding and hetegoreneous Digital Signal Processing applications. More information can be found on the dedicated web site [http://embb.telecom-paristech.fr/](http://embb.telecom-paristech.fr/).

The system clock frequency of the FPGA part is 50 MZz (20.0 ns clock period). It can probably be increased a bit (see the embb_wrapper.psyn.tcl and embb_top.vsyn.tcl synthesis scripts) but do not expect much. The maximum is somewhere between 80 and 90 MHz.

## Debugging

Slide switches SW1 (MSB) and SW0 (LSB) form the ledsmode 2-bits selector. LEDs can be activity indicators (ledsmode=0), driven by a custom 64 bits parameter register of the FEP (1), events indicators (2) or driven by the internal general purpose register of the AXI-VCI bridge (3):

- ledsmode = 0:
  - 0:   Switches every `2**27` system clock cycles (can be used to confirm system clock frequency)
  - 1-7: 0
- ledsmode = 1:
  - led(i) = `or_reduce(FEP.R3(8 * i + 7 downto 8 * i))`
- ledsmode = 2:
  - led(i) illuminates if the corresponding events occur:
  - 0: Crossbar interrupt
  - 1: FEP DMA interrupt
  - 2: FEP PSS interrupt
  - 3: INTL DMA interrupt
  - 4: INTL PSS interrupt
  - 5: MAPPER DMA interrupt
  - 6: MAPPER PSS interrupt
  - 7: RAM DMA interrupt
- ledsmode = 3:
  - 0-3: `or_reduce(SLAVE_BRIDGE.GPR(8 * i + 7 downto 8 * i))`
  - 4-7: 0

## The Embb instance comprises

- Slave AXI bridge (connected to CPU master)
- Crossbar
- Master AXI bridge (connected to DDR slave)
- FEP (with DMA, UC and PSS)
- RAM (64 kB, with DMA)
- INTL (with DMA, UC and PSS)
- MAPPER (with DMA, UC and PSS)

The slave AXI bridge has a 32 bits slave AXI lite interface through which it receives and serves AXI requests from the CPU. It converts all AXI requests to 64 bits VCI requests and routes them to the crossbar, except the requests to its own internal registers (`[0x4010_0000..0x4020_0000[`).

The crossbar routes the VCI requests it receives from the slave AXI bridge to the corresponding units, except the requests to its own internal ROMs and registers (`[0x4000_0000..0x4010_0000[`).

The master AXI bridge has a 32 bits master AXI interface to the DDR (through the processing system). It routes all requests it receives (`[0x0000_0000..0x2000_0000[`) to the DDR.

The other units have their slave interfaces at addresses:
- FEP    `[0x4020_0000..0x4020_0000[`
- RAM    `[0x4030_0000..0x4040_0000[`
- INTL   `[0x4040_0000..0x4050_0000[`
- MAPPER `[0x4050_0000..0x4060_0000[`

Each of FEP, RAM, INTL and MAPPER is also a master for the others (they are all equipped with a DMA engine and can thus send data to the others or read data from them) and for the master AXI bridge (they can peform read and write accesses to the DDR).

# <a name="Archive"></a>Install from the archive

TODO

# <a name="Run"></a>Test Embb on the ZedBoard

* Plug the SD card in the ZedBoard and connect the USB cable.
* Check the position of the jumper that selects the boot medium (SD card).
* Power on. A new character device should show up (`/dev/ttyACM0` by default) on the host PC. It correspons to the serial link with the ZedBoard.
* Launch a terminal emulator (picocom, minicom...) and attach it to the new character device, with a 115200 baudrate, no flow control, no parity, 8 bits characters, no port reset and no port locking: `picocom -b115200 -fn -pn -d8 -r -l /dev/ttyACM0`.
* Wait until Linux boots, log in as root (password `embb`) and start interacting with Embb.

<!-- -->
    Host> picocom -b115200 -fn -pn -d8 -r -l /dev/ttyACM0
    ...
    Welcome to Embb (c) Telecom ParisTech
    embb login: root
    Embb> insmod dsp.ko
    Embb> insmod embb.ko
    Embb> ./demo_perf

### <a name="RunHalt"></a>Halt the system

Always halt properly before switching the power off:

    Embb> poweroff
    Embb> Stopping network...Saving random seed... done.
    Stopping logging: OK
    The system is going down NOW!
    Sent SIGTERM to all processes
    Sent SIGKILL to all processes
    Requesting system poweroff
    reboot: System halted

# <a name="Build"></a>Build everything from scratch

---

**Note**: Mentor Graphics tools (Precision RTL) and Xilinx tools (Vivado and its companion SDK) are needed. In the following we assume that they are properly installed. Several free and open source software are also needed. Some steps can be run in parallel because they do not depend on the results of other steps. Some steps cannot be started before others finish and this will be indicated.

---

**Note**: the Linux repository, the Buildroot repository and the directories in which some software components are built occupy several GB of disk space each.

---

## <a name="BuildDownloads"></a>Downloads

Define environment variables for the git clones:

    Host> EMBB=<some-path>/embb
    Host> XLINUX=<some-path>/linux-xlnx
    Host> XUBOOT=<some-path>/u-boot-xlnx
    Host> XDTS=<some-path>/device-tree-xlnx
    Host> BUILDROOT=<some-path>/buildroot

Clone the git repositories:

    Host> git clone https://<user>:<password>@git.telecom-paristech.fr/embb $EMBB
    Host> git clone https://github.com/Xilinx/linux-xlnx.git $XLINUX
    Host> git clone https://github.com/Xilinx/u-boot-xlnx.git $XUBOOT
    Host> git clone http://github.com/Xilinx/device-tree-xlnx.git $XDTS
    Host> git clone http://git.buildroot.net/git/buildroot.git $BUILDROOT

Create the build directory for the software stack:

    Host> BUILDS=<some-path>/zed_embb               # Build directory (several GB!!!)
    Host> mkdir -p $BUILDS

## <a name="BuildSynthesis"></a>Hardware synthesis

    Host> DESIGN=$EMBB/hardware/vhdl/src/fmir4zynq  # Source directory of top-level VHDL design
    Host> cd $DESIGN
    Host> make embb_wrapper.psyn                    # Precision RTL synthesis
    Host> make embb_top.vsyn                        # Vivado synthesis, place, route and bitstream generation

Copy the generated bitstream and the system definition file to the build directory:

    Host> cd $DESIGN/embb_top.vv-syn/top.runs/impl_1
    Host> cp top_wrapper.bit top_wrapper.sysdef $BUILDS

---

## <a name="BuildRootFs"></a>Build the root file system

    Host> export BR2_EXTERNAL=$DESIGN/buildroot       # Buildroot configuration directory for Embb
    Host> export BR2_CCACHE_DIR=<some-path>           # Cache directory used by ccache
    Host> export BR2_DL_DIR=<some-path>               # Buildroot download directory
    Host> OVERLAY=$BR2_EXTERNAL/overlay               # Buildroot overlay directory
    Host> BUILDROOT_BUILD=$BUILDS/buildroot           # Buildroot build directory
    Host> mkdir -p $BR2_CCACHE_DIR $BR2_DL_DIR $BUILDROOT_BUILD

In order to connect to the ZedBoard using ssh, generate a key-pair (or use an existing one) and install the public key in the buildroot overlay:

    Host> ssh-keygen -f ~/.ssh/id_rsa_embb -N ''
    Host> mkdir -p $OVERLAY/root/.ssh
    Host> cp ~/.ssh/id_rsa_embb.pub $OVERLAY/root/.ssh/authorized_keys

Initialize the buildroot build directory and build:

    Host> cd $BUILDROOT
    Host> make O=$BUILDROOT_BUILD zed_embb_defconfig
    Host> cd $BUILDROOT_BUILD
    Host> make

**Note**: If needed, before building, customize the default configuration and save it:

    Host> cd $BUILDROOT_BUILD
    Host> make menuconfig
    Host> make savedefconfig

**Note**: the first build takes some time, especially because Buildroot must first build the toolchain for ARM targets, but most of the work will not have to be redone if we later change the configuration and rebuild (unless we change the configuration of the Buildroot tool chain, in which case we will have to rebuild everything).

---

Add the directory of the host tools (cross-compiler, debugger...) to the path and define the `CROSS_COMPILE` environment variable (note the trailing hyphen):

    Host> export PATH=$PATH:$BUILDROOT_BUILD/host/usr/bin
    Host> export CROSS_COMPILE=arm-buildroot-linux-gnueabi-

## <a name="BuildKernel"></a>Build the Linux kernel

---

**Note**: Do not start this part before the toolchain is built.

---

Download the `PREEMPT RT` patch, checkout the `xilinx-v2016.2` tag of the Linux kernel, apply the patch, configure the kernel:

    Host> LINUX_BUILD=$BUILDS/linux
    Host> mkdir -p $LINUX_BUILD
    Host> cd $XLINUX
    Host> git checkout xilinx-v2016.2
    Host> wget -P /tmp https://www.kernel.org/pub/linux/kernel/projects/rt/4.4/older/patch-4.4-rt3.patch.gz
    Host> gunzip -c /tmp/patch-4.4-rt3.patch.gz | patch -p1
    Host> make O=$LINUX_BUILD ARCH=arm xilinx_zynq_defconfig

Change the kernel configuration for full preemption, build the kernel, the modules, install the modules in the buildroot overlay and copy the U-Boot image of the kernel to the build directory:

    Host> cd $LINUX_BUILD
    Host> make ARCH=arm menuconfig
        Kernel Features -> Preemption Model -> Fully Preemptible Kernel (RT)
    Host> make -j24 ARCH=arm LOADADDR=0x8000 uImage
    Host> cp arch/arm/boot/uImage $BUILDS
    Host> make -j24 ARCH=arm modules
    Host> make ARCH=arm modules_install INSTALL_MOD_PATH=$OVERLAY

## <a name="BuildEmbbSoftware"></a>Build the Embb software

Build the Linux drivers for Embb and the example application, generates the input data file, install them in the buildroot overlay directory, rebuild the root file system and copy it to the build directory:

    Host> cd $EMBB/software/linux
    Host> make -C embbDrivers KDIR=$LINUX_BUILD CROSS_COMPILE=$CROSS_COMPILE ultraclean all
    Host> make -C applications CROSS_COMPILE=$CROSS_COMPILE ultraclean demo_perf data
    Host> cp embbDrivers/*.ko applications/demo_perf applications/input.dat $OVERLAY/root
    Host> cd $BUILDROOT_BUILD
    Host> make
    Host> cp images/rootfs.cpio.uboot $BUILDS/uramdisk.image.gz

## <a name="BuildUboot"></a>Build U-Boot

---

**Note**: Do not start this part before the [toolchain](#BuildRootFs) is built.

---

Run the U-Boot configurator to create a default U-Boot configuration, build and copy the U-Boot ELF to the build directory:

    Host> UBOOT_BUILD=$BUILDS/uboot
    Host> mkdir -p $UBOOT_BUILD
    Host> cd $XUBOOT
    Host> make O=$UBOOT_BUILD zynq_zed_defconfig
    Host> cd $UBOOT_BUILD
    Host> make -j24
    Host> cp u-boot $BUILDS/u-boot.elf

**Note**: If needed, before building, customize the default configuration:

    Host> cd $UBOOT_BUILD
    Host> make menuconfig
    Host> make -j24

## <a name="BuildDTS"></a>Linux kernel device tree

Do not start this part before the [hardware synthesis finishes](#BuildSynthesis) finishes and the [toolchain](#BuildRootFs) is built.

Generate the default device tree sources, patch it for Embb and build the device tree blob:

    Host> cd $BUILDS
    Host> hsi -mode batch -quiet -notrace -source $DESIGN/dts.tcl -tclargs top_wrapper.sysdef $XDTS dts
    Host> patch -p0 < $DESIGN/dts.patch
    Host> dtc -I dts -O dtb -o devicetree.dtb dts/system.dts

## <a name="BuildFSBL"></a>First Stage Boot Loader (FSBL)

Do not start this part before the [hardware synthesis finishes](#BuildSynthesis) finishes.

Generate the FSBL sources and compile them:

    Host> cd $BUILDS
    Host> hsi -mode batch -quiet -notrace -source $DESIGN/fsbl.tcl -tclargs top_wrapper.sysdef fsbl
    Host> make -C fsbl
    Host> cp fsbl/executable.elf fsbl.elf

## <a name="BuildBootImg"></a>Zynq boot image

Generate the Zynq boot image:

    Host> cd $BUILDS
    Host> bootgen -w -image $DESIGN/boot.bif -o boot.bin

## <a name="BuildSDCard"></a>Prepare the SD card

Finally, prepare a SD card with a FAT32 first primary partition (8MB minimum), mount it on your host PC, and copy the different components to it:

    Host> SDCARD=<path-to-mounted-sd-card>
    Host> cd $BUILDS
    Host> cp boot.bin devicetree.dtb uImage uramdisk.image.gz $SDCARD
    Host> sync
    Host> umount $SDCARD

Eject the SD card, plug it in the ZedBoard and power on.

<!-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0: -->
