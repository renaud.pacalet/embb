#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

gh-IGNORE	+= fmir4zynq_lib.crossbar fmir4zynq_lib.embb fmir4zynq_lib.embb_wrapper fmir4zynq_lib.embb_top fmir4zynq_lib.embb_top_sim

fmir4zynq_lib.crossbar: \
	global_lib.global \
	global_lib.utils \
	crossbar_lib.crossbar_pkg \
	crossbar_lib.plugplay_0_pkg \
	crossbar_lib.qarbiter \
	crossbar_lib.parbiter \
	crossbar_lib.tgtif \
	fmir4zynq_lib.plugplay_1_pkg \
	fmir4zynq_lib.crossbar_def

fmir4zynq_lib.crossbar_def: \
	crossbar_lib.crossbar_pkg \
	fmir4zynq_lib.plugplay_1_pkg \
	fmir4zynq_lib.crossbar_map

fmir4zynq_lib.crossbar_map: \
	crossbar_lib.plugplay_0_pkg \
	crossbar_lib.crossbar_pkg

fmir4zynq_lib.embb: \
	global_lib.global \
	global_lib.axi_pkg_m4 \
	global_lib.axi_pkg_m8 \
	global_lib.bvci64_pkg \
	axi_vci_bridge_lib.axi4lite32_slave_to_bvci64_initiator \
	axi_vci_bridge_lib.tvci2maxi \
	axi_vci_bridge_lib.axi_vci_bridge_pkg \
	fep_lib.fep \
	fep_lib.fep_pkg \
	intl_lib.intl \
	mapper_lib.mapper \
	vci_ram_lib.vci_ram \
	fmir4zynq_lib.crossbar_map \
	fmir4zynq_lib.crossbar_def \
	fmir4zynq_lib.plugplay_1_pkg \
	fmir4zynq_lib.crossbar

fmir4zynq_lib.embb_wrapper: \
	global_lib.axi_pkg_m4 \
	global_lib.axi_pkg_m8 \
	fmir4zynq_lib.embb

fmir4zynq_lib.embb_top: \
	fmir4zynq_lib.embb_wrapper

fmir4zynq_lib.embb_top_sim: \
	global_lib.axi_pkg_m8 \
	fmir4zynq_lib.embb_top

fmir4zynq_lib.plugplay_1_pkg: \
	crossbar_lib.plugplay_0_pkg \
	crossbar_lib.crossbar_pkg \
	fmir4zynq_lib.crossbar_map

vv-syn.fmir4zynq_lib.embb_top: pr-syn.fmir4zynq_lib.embb_wrapper

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
