--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.axi_pkg_m4.all;
use global_lib.axi_pkg_m8.all;
use global_lib.bvci64_pkg.all;

library axi_vci_bridge_lib;

library mapper_lib;

library fep_lib;

library intl_lib;

library vci_ram_lib;

use work.plugplay_1_pkg.all;
use work.crossbar_map.all;
use work.crossbar_def.all;

entity embb is
	port(
		aclk:		in	std_ulogic;
		aresetn:	in	std_ulogic;
		s_axi_m2s:	in	global_lib.axi_pkg_m4.axilite_m2s;	-- AXI inputs from master
		s_axi_s2m:	out	global_lib.axi_pkg_m4.axilite_s2m;	-- AXI outputs to master
		m_axi_m2s:	out	global_lib.axi_pkg_m8.axi_m2s;		-- AXI outputs to slave
		m_axi_s2m:	in	global_lib.axi_pkg_m8.axi_s2m;		-- AXI inputs from slave
		irq:		out	std_ulogic;
		ledsmode:	in	std_ulogic_vector(1 downto 0);
		led:		out	std_ulogic_vector(7 downto 0)
	);
end entity embb;

architecture rtl of embb is

	signal i2x_v:		vci_i2t_vector(0 to ni - 1);
	signal x2i_v:		vci_t2i_vector(0 to ni - 1);
	signal t2x_v:		vci_t2i_vector(0 to nt - 1);
	signal x2t_v:		vci_i2t_vector(0 to nt - 1);

	signal xirq:		std_ulogic_vector(nirq - 1 downto 0);

	signal srstns:		std_ulogic_vector(nins - 1 downto 0);
	signal ce:		std_ulogic_vector(nins - 1 downto 0);
	signal hirq:		std_ulogic_vector(nins - 1 downto 0);

	signal t_bvci64_i2t:	bvci64_i2t_type;
	signal t_bvci64_t2i:	bvci64_t2i_type;
	signal t_vci_i2t:	vci_i2t_type;
	signal t_vci_t2i:	vci_t2i_type;
	signal i_vci_i2t:	vci_i2t_type;
	signal i_vci_t2i:	vci_t2i_type;

	signal bridgegpo:	std_ulogic_vector(31 downto 0);
	signal events:		std_ulogic_vector(7 downto 0);
	signal fepgpo:		std_ulogic_vector(63 downto 0);

	signal leds0:		std_ulogic_vector(7 downto 0);
	signal leds1:		std_ulogic_vector(7 downto 0);
	signal leds2:		std_ulogic_vector(7 downto 0);
	signal leds3:		std_ulogic_vector(7 downto 0);

begin

	i2x_v(iidt(ibri))	<= t_vci_i2t;
	t_vci_t2i		<= x2i_v(iidt(ibri));
	i_vci_i2t		<= x2t_v(tidt(ibri));
	t2x_v(tidt(ibri))	<= i_vci_t2i;
	t_vci_i2t		<= bvci64_i2t_to_vci_i2t(t_bvci64_i2t);
	t_bvci64_t2i		<= vci_t2i_to_bvci64_t2i(t_vci_t2i);

	slave_bridge: entity axi_vci_bridge_lib.axi4lite32_slave_to_bvci64_initiator(rtl)
		generic map(
			base_address	=> X"401"
		)
		port map(
			clk	=> aclk,
			srstn	=> aresetn,
			vci_i2t	=> t_bvci64_i2t,
			vci_t2i	=> t_bvci64_t2i,
			axi_s2m	=> s_axi_s2m,
			axi_m2s	=> s_axi_m2s,
			evnt	=> '0',
			gpo	=> bridgegpo
		);

	master_bridge: entity axi_vci_bridge_lib.tvci2maxi(rtl)
		port map(
			clk		=> aclk,
			srstn		=> aresetn,
			vci_i2t		=> i_vci_i2t,
			vci_t2i		=> i_vci_t2i,
			m_axi_s2m	=> m_axi_s2m,
			m_axi_m2s	=> m_axi_m2s
		);

	j_crossbar: entity work.crossbar
		generic map(
			idefs		=> idefs,
			tmaps		=> tmaps,
			tdefs		=> tdefs,
			rom		=> rom,
			nirq		=> nirq,
			nins		=> nins)
		port map(
			clk		=> aclk,
			srstn		=> aresetn,
			ce		=> ce,
			srstnout	=> srstns,
			i2x		=> i2x_v,
			x2i		=> x2i_v,
			t2x		=> t2x_v,
			x2t		=> x2t_v,
			tvci_in		=> x2t_v(tidt(icross)),
			tvci_out	=> t2x_v(tidt(icross)),
			irqi		=> xirq,
			hirq		=> hirq,
			cirq		=> xirq(icross),
			irq		=> irq
		);

	mapper_0: if MAPPER_ENABLE = 1 generate
		u_mapper: entity mapper_lib.mapper
			port map(
				clk		=> aclk,
				srstn		=> srstns(imapp),
				ce		=> ce(imapp),
				hirq		=> hirq(imapp),
				dirq		=> xirq(irqidt(imapp)(0)),
				pirq		=> xirq(irqidt(imapp)(1)),
				uirq		=> xirq(irqidt(imapp)(2)),
				tvci_in		=> x2t_v(tidt(imapp)),
				tvci_out	=> t2x_v(tidt(imapp)),
				ivci_in		=> x2i_v(iidt(imapp)),
				ivci_out	=> i2x_v(iidt(imapp))
			);
	end generate mapper_0;
	
	fep_0: if FEP_ENABLE = 1 generate
		u_fep: entity fep_lib.fep(rtl)
			port map(
				clk		=> aclk,
				srstn		=> srstns(ifep),
				ce		=> ce(ifep),
				hirq		=> hirq(ifep),
				dirq		=> xirq(irqidt(ifep)(0)),
				pirq		=> xirq(irqidt(ifep)(1)),
				uirq		=> xirq(irqidt(ifep)(2)),
				tvci_in		=> x2t_v(tidt(ifep)),
				tvci_out	=> t2x_v(tidt(ifep)),
				ivci_in		=> x2i_v(iidt(ifep)),
				ivci_out	=> i2x_v(iidt(ifep)),
				gpo		=> fepgpo
			);
	end generate;

	intl_0: if INTL_ENABLE = 1 generate
		u_intl: entity intl_lib.intl(rtl)
			port map(
				clk		=> aclk,
				srstn		=> srstns(iintl),
				ce		=> ce(iintl),
				hirq		=> hirq(iintl),
				dirq		=> xirq(irqidt(iintl)(0)),
				pirq		=> xirq(irqidt(iintl)(1)),
				uirq		=> xirq(irqidt(iintl)(2)),
				tvci_in		=> x2t_v(tidt(iintl)),
				tvci_out	=> t2x_v(tidt(iintl)),
				ivci_in		=> x2i_v(iidt(iintl)),
				ivci_out	=> i2x_v(iidt(iintl))
			);
	end generate;
	
	ram_0: if RAM_ENABLE = 1 generate
		u_vci_ram: entity vci_ram_lib.vci_ram 
			generic map(
				na		=> 13
			)
			port map(
				clk		=> aclk,
				srstn		=> srstns(iram),
				ce		=> ce(iram),
				dirq		=> xirq(irqidt(iram)(0)),
				tvci_in		=> x2t_v(tidt(iram)),
				tvci_out	=> t2x_v(tidt(iram)),
				ivci_in		=> x2i_v(iidt(iram)),
				ivci_out	=> i2x_v(iidt(iram))
			);
	end generate;

	events(0) <= xirq(icross);
	events(1) <= xirq(irqidt(ifep)(0));
	events(2) <= xirq(irqidt(ifep)(1));
	events(3) <= xirq(irqidt(iintl)(0));
	events(4) <= xirq(irqidt(iintl)(1));
	events(5) <= xirq(irqidt(imapp)(0));
	events(6) <= xirq(irqidt(imapp)(1));
	events(7) <= xirq(irqidt(iram)(0));

	led <=	leds0 when ledsmode = "00" else
		leds1 when ledsmode = "01" else
		leds2 when ledsmode = "10" else
		leds3 when ledsmode = "11" else
		(others => '0');

	process(aclk)
		variable cnt: u_unsigned(27 downto 0);
	begin 
		if rising_edge(aclk) then
			if aresetn = '0' then
				cnt := (others => '0');
			else
				cnt := cnt + 1;
			end if;
			leds0 <= (0 => cnt(27), others => '0');
		end if;
	end process;

	gleds1: for i in 0 to 7 generate
		leds1(i) <=	'0' when fepgpo(8 * i + 7 downto 8 * i) = X"00" else
				'1';
	end generate gleds1;

	process(aclk)
		variable leds20, leds21: std_ulogic_vector(7 downto 0);
	begin
		if rising_edge(aclk) then
			if aresetn = '0' then
				leds2  <= (others => '0');
				leds20 := (others => '0');
				leds21 := (others => '0');
			else
				leds2  <= leds20;
				leds20 := leds21;
				leds21 := events;
			end if;
		end if;
	end process;

	gleds3: for i in 0 to 3 generate
		leds3(i) <=	'0' when bridgegpo(8 * i + 7 downto 8 * i) = X"00" else
				'1';
	end generate gleds3;
	leds3(7 downto 4) <= "0000";

end architecture;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
