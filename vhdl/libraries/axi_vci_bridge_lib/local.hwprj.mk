#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

gh-IGNORE	+= axi_vci_bridge_lib.tvci2maxi axi_vci_bridge_lib.saxi2ivci axi_vci_bridge_lib.maxi2tvci_tb
gh-IGNORE	+= axi_vci_bridge_lib.maxi2saxi_tb axi_vci_bridge_lib.ivci2tvci_tb axi_vci_bridge_lib.ivci2saxi_tb
gh-IGNORE	+= axi_vci_bridge_lib.axi4lite32_slave_to_bvci64_initiator_sim

axi_vci_bridge_lib.axi_vci_bridge_pkg: \
	global_lib.axi_pkg_m8 \
	global_lib.global

axi_vci_bridge_lib.axi4lite32_slave_to_bvci64_initiator: \
	global_lib.axi_pkg_m4 \
	global_lib.utils \
	global_lib.bvci64_pkg

axi_vci_bridge_lib.axi4lite32_slave_to_bvci64_initiator_sim: \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils \
	global_lib.axi_pkg_m4 \
	global_lib.axi4lite32_master \
	global_lib.bvci64_target \
	axi_vci_bridge_lib.axi4lite32_slave_to_bvci64_initiator

axi_vci_bridge_lib.ivci2saxi_tb: \
	global_lib.axi_pkg_m8 \
	global_lib.vci_master \
	axi_vci_bridge_lib.tvci2maxi \
	global_lib.global \
	memories_lib.axi_memory

axi_vci_bridge_lib.ivci2tvci_tb: \
	global_lib.global \
	global_lib.vci_master \
	memories_lib.vci_memory

axi_vci_bridge_lib.maxi2saxi_tb: \
	global_lib.global \
	global_lib.axi_pkg_m8 \
	global_lib.axi_master \
	memories_lib.axi_memory

axi_vci_bridge_lib.maxi2tvci_tb: \
	global_lib.global \
	global_lib.axi_pkg_m8 \
	global_lib.axi_master \
	memories_lib.vci_memory \
	axi_vci_bridge_lib.saxi2ivci

axi_vci_bridge_lib.saxi2ivci: \
	global_lib.fifo \
	axi_vci_bridge_lib.axi_vci_bridge_pkg \
	global_lib.utils \
	global_lib.global

axi_vci_bridge_lib.tvci2maxi: \
	axi_vci_bridge_lib.axi_vci_bridge_pkg \
	global_lib.utils \
	global_lib.sim_utils \
	global_lib.fifo \
	global_lib.axi_pkg_m8 \
	global_lib.global

# Simulation

axi_vci_bridge_lib_maxicmdfile := $(hwprj_db_path.axi_vci_bridge_lib)/maxi.cmd
axi_vci_bridge_lib_ivcicmdfile := $(hwprj_db_path.axi_vci_bridge_lib)/ivci.cmd

ms-sim.axi_vci_bridge_lib.maxi2tvci_tb: MSSIMFLAGS += -do 'run -all; quit' -t ps -Gcmdfile="$(axi_vci_bridge_lib_maxicmdfile)"
ms-sim.axi_vci_bridge_lib.maxi2saxi_tb: MSSIMFLAGS += -do 'run -all; quit' -t ps -Gcmdfile="$(axi_vci_bridge_lib_maxicmdfile)"
ms-sim.axi_vci_bridge_lib.ivci2saxi_tb: MSSIMFLAGS += -do 'run -all; quit' -t ps -Gcmdfile="$(axi_vci_bridge_lib_ivcicmdfile)"
ms-sim.axi_vci_bridge_lib.ivci2tvci_tb: MSSIMFLAGS += -do 'run -all; quit' -t ps -Gcmdfile="$(axi_vci_bridge_lib_ivcicmdfile)"
ms-sim.axi_vci_bridge_lib: $(addprefix ms-sim.axi_vci_bridge_lib.,maxi2tvci_tb maxi2saxi_tb ivci2saxi_tb ivci2tvci_tb)
ms-sim: ms-sim.axi_vci_bridge_lib

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
