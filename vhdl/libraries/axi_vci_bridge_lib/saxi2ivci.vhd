--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;
use global_lib.axi_pkg_m8.all;

use work.axi_vci_bridge_pkg.all;

entity saxi2ivci is
  port (
      clk        : in std_logic;
      srstn      : in std_logic;
      vci_i2t    : out vci_i2t_type;
      vci_t2i    : in vci_t2i_type;
      s_axi_s2m  : out axi_s2m_split;
      s_axi_m2s  : in axi_m2s_split
    );
end entity saxi2ivci;
  
  
architecture rtl of saxi2ivci is

type state_type is (idle_t, process_t);

signal dra : axi_m2s_a;
signal dwa : axi_m2s_a;
signal drd : axi_s2m_r;
signal dwd : axi_m2s_w;
signal dwr : axi_s2m_b;
  
signal frai : ra_fifo_in;
signal frao : ra_fifo_out;
signal fwai : wa_fifo_in;
signal fwao : wa_fifo_out;
signal frdi : rd_fifo_in;
signal frdo : rd_fifo_out;
signal fwdi : wd_fifo_in;
signal fwdo : wd_fifo_out;
signal fwri : wr_fifo_in;
signal fwro : wr_fifo_out;
signal freqi : req_fifo_in;
signal freqo : req_fifo_out;
signal frspi : rsp_fifo_in;
signal frspo : rsp_fifo_out;


type address_type is record 
  start  : std_ulogic_vector(31 downto 0);
  align  : std_ulogic_vector(31 downto 0);
  wrap   : std_ulogic_vector(31 downto 0);
  burst  : std_ulogic_vector(31 downto 0);
  border : std_ulogic_vector(31 downto 0);
end record;

type p_type is record 
  addr    : address_type;
  size    : std_ulogic_vector(2 downto 0);
  len     : std_ulogic_vector(axi_l - 1 downto 0);
  wrap    : std_ulogic;
  incr    : std_ulogic;
  err     : std_ulogic;
  rrb     : std_ulogic;
  rnw     : std_ulogic;
  state   : state_type;
  reqcnt  : natural;
  vci_i2t : vci_request_type;
  cmdack  : std_ulogic;
end record;

signal r, rin : p_type;

begin

    -- VCI fifo

    fifo_req: entity global_lib.fifo 
    generic map (
      T      =>  std_ulogic_vector(vci_request_length - 1 downto 0),
      depth  =>  2)
    port map (
      srstn =>  srstn,
      clk   =>  clk,
      read_en  =>  freqi.enr,
      write_en =>  freqi.enw, 
      dout  =>  freqo.do,
      din   =>  freqi.di,
      empty =>  freqo.empty,
      full  =>  freqo.full
    );

    fifo_rsp: entity global_lib.fifo 
    generic map (
      T      =>  std_ulogic_vector(vci_response_length - 1 downto 0),
      depth  =>  2)
    port map (
      srstn =>  srstn,
      clk   =>  clk,
      read_en  =>  frspi.enr,
      write_en =>  frspi.enw, 
      dout  =>  frspo.do,
      din   =>  frspi.di,
      empty =>  frspo.empty,
      full  =>  frspo.full
    );
    
    fifo_ra: entity global_lib.fifo 
    generic map (
      T      =>  std_ulogic_vector(read_address_length - 1 downto 0),
      depth  =>  1)
    port map (
      srstn =>  srstn,
      clk   =>  clk,
      read_en  =>  frai.enr,
      write_en =>  frai.enw, 
      dout  =>  frao.do,
      din   =>  frai.di,
      empty =>  frao.empty,
      full  =>  frao.full
    );

    fifo_rd: entity global_lib.fifo 
    generic map (
      T      =>  std_ulogic_vector(read_data_length - 1 downto 0),
      depth  =>  2)
    port map (
      srstn =>  srstn, 
      clk   =>  clk,
      read_en  =>  frdi.enr,
      write_en =>  frdi.enw, 
      dout  =>  frdo.do,
      din   =>  frdi.di,
      empty =>  frdo.empty,
      full  =>  frdo.full
    );

    fifo_wa: entity global_lib.fifo 
    generic map (
      T      =>  std_ulogic_vector(write_address_length - 1 downto 0),
      depth  =>  1)
    port map (
      srstn =>  srstn, 
      clk   =>  clk,
      read_en  =>  fwai.enr,
      write_en =>  fwai.enw, 
      dout  =>  fwao.do,
      din   =>  fwai.di,
      empty =>  fwao.empty,
      full  =>  fwao.full
    );

    fifo_wd: entity global_lib.fifo 
    generic map (
      T      =>  std_ulogic_vector(write_data_length - 1 downto 0),
      depth  =>  2)
    port map (
      srstn =>  srstn, 
      clk   =>  clk,
      read_en  =>  fwdi.enr,
      write_en =>  fwdi.enw, 
      dout  =>  fwdo.do,
      din   =>  fwdi.di,
      empty =>  fwdo.empty,
      full  =>  fwdo.full
    );

    fifo_wr: entity global_lib.fifo 
    generic map (
      T      =>  std_ulogic_vector(write_response_length - 1 downto 0),
      depth  =>  1)
    port map (
      srstn =>  srstn, 
      clk   =>  clk,
      read_en  =>  fwri.enr,
      write_en =>  fwri.enw, 
      dout  =>  fwro.do,
      din   =>  fwri.di,
      empty =>  fwro.empty,
      full  =>  fwro.full
    );

    dra <= to_axi_m2s_a(frao.do) when frao.empty = '0' else axi_m2s_a_none;
    dwa <= to_axi_m2s_a(fwao.do) when fwao.empty = '0' else axi_m2s_a_none;
    drd <= to_axi_s2m_r(frdo.do) when frdo.empty = '0' else axi_s2m_r_none;
    dwd <= to_axi_m2s_w(fwdo.do) when fwdo.empty = '0' else axi_m2s_w_none;
    dwr <= to_axi_s2m_b(fwro.do) when fwro.empty = '0' else axi_s2m_b_none;

    frai.enw <= not frao.full  and s_axi_m2s.ar.arvalid;
    frai.di  <= to_std_ulogic_vector(s_axi_m2s.ar);
  
    fwai.enw <= not fwao.full  and s_axi_m2s.aw.awvalid;
    fwai.di  <= to_std_ulogic_vector(s_axi_m2s.aw);

    fwdi.enw <= not fwdo.full  and s_axi_m2s.w.wvalid;
    fwdi.di  <= to_std_ulogic_vector(s_axi_m2s.w);

    s_axi_s2m.b <= dwr;
    s_axi_s2m.r <= drd;
    
    s_axi_s2m.aw.awready <= not fwao.full;
    s_axi_s2m.ar.arready <= not frao.full;
    s_axi_s2m.w.wready  <= not fwdo.full;

  process(clk)

    begin

    if rising_edge(clk) then
      if srstn = '0' then 
        r.state   <= idle_t;
        r.rrb     <= AXI_WRITE;
        r.err     <= '0';
        r.reqcnt  <= 0;
        r.vci_i2t <= vci_request_none;
      else
        r <= rin;
      end if;
    end if;

  end process;

  axi2vci_req_p : process(dwa, dwd, dra, r, frao, fwdo, fwao, vci_t2i, freqo)

    variable v       : p_type; 
    variable da      : axi_m2s_a;
    variable reqcnt1 : natural;
    variable rd_ra   : std_ulogic; 
    variable rd_wa   : std_ulogic; 
    variable rd_wd   : std_ulogic; 
    variable rval    : std_ulogic;
    variable wval    : std_ulogic;
    variable waval   : std_ulogic;
    variable wdval   : std_ulogic;
    variable err     : std_ulogic;

  
    begin
  
      rval  := dra.avalid and not frao.empty;
      wdval := dwd.wvalid and not fwdo.empty;
      waval := dwa.avalid and not fwao.empty;
      wval  := wdval and waval;
  
      da   :=  axi_m2s_a_none;

      v := r;
     
      v.vci_i2t.eop      := '0';
      v.vci_i2t.cmdval   := '0';
      v.vci_i2t.pktid(1) := '0';
      v.vci_i2t.be       := (others => '0');

      v.cmdack := vci_t2i.cmdack;
      
      rd_ra  := '0';
      rd_wa  := '0';
      rd_wd  := '0'; 

      reqcnt1 := r.reqcnt + 1;
      
      if freqo.full = '0' then
 
        case r.state is 
    
          when idle_t =>
  
            -- Arbiter
           
            v.rnw := '0'; 
            if rval = '1' and wval = '0' then
              da := dra;
              rd_ra := '1';
              v.rnw := '1'; 
            elsif wval = '1' and rval = '0' then 
              da := dwa;
              rd_wa := '1'; 
            elsif rval = '1' and wval = '1' then 
            -- Conflict
              v.rrb := not r.rrb;
              if r.rrb = AXI_READ then 
              -- Round Robin arbiter select read interface
                da    := dra;
                rd_ra  := '1'; 
                v.rnw := '1'; 
              else
              -- Round Robin arbiter select write interface
                da    := dwa;
                rd_wa  := '1'; 
              end if;
            end if;
 
            -- End arbiter

            if da.aburst(1) = '1' then 
              -- Wrapping burst
-- pragma translate_off
              v.err :=  check_wrap_parameters(da.aaddr, da.asize, da.alen); 
-- pragma translate_on
              v.addr.wrap := get_wrap_addr(da.aaddr, da.asize, da.alen);
            end if;
 
            v.len             := da.alen;
            v.wrap            := da.aburst(1);
            v.incr            := or_reduce(da.aburst);
            v.size            := da.asize;
            v.addr.start      := da.aaddr;
            v.addr.align      := get_align_addr(da.aaddr, da.asize);
            v.addr.border     := get_border_addr(v.addr.wrap, v.size, da.alen);
            v.addr.burst      := get_next_addr(v.addr.align, v.size);
    
            if rd_ra = '1' then 
    
              -- New read operation
  
              v.vci_i2t.cmd      := "01";
              v.vci_i2t.be       := X"FF";
              v.vci_i2t.pktid(0) := '1';
     
            elsif rd_wa = '1' then 
              
              -- New write operation
              
              v.vci_i2t.be       := swap_strb(dwd.wstrb);
              v.vci_i2t.cmd      := "10";
              v.vci_i2t.pktid(0) := '0';
     
            end if; 
    
            if rd_ra = '1' or rd_wa = '1' then 
 
              v.state           := process_t;
              v.vci_i2t.cmdval  := '1'; 
              v.vci_i2t.eop     := '0'; 
              v.vci_i2t.address := da.aaddr;
              v.vci_i2t.trdid   := (others => '0');
              if vci_t <= axi_i then 
                v.vci_i2t.trdid   := da.aid(vci_t - 1 downto 0);
              else 
                v.vci_i2t.trdid(axi_i - 1 downto 0) := da.aid;
              end if;
 
              if u_unsigned(v.len) = 0 then  
                v.vci_i2t.eop := '1';
                v.state := idle_t;
              end if;
 
            end if;
    
          when process_t =>
            
            if r.rnw = '1' then
            -- Processing READ
              v.vci_i2t.cmdval  := '1';
              v.vci_i2t.be      := X"FF";
            else
            -- Processing WRITE
              rd_wd := '1'; 
              v.vci_i2t.cmdval  := wdval; 
              v.vci_i2t.be      := swap_strb(dwd.wstrb);
            end if;
 
            v.vci_i2t.address := r.addr.burst;
       
            if r.reqcnt = u_unsigned(r.len) then
              v.vci_i2t.eop  := '1'; 
            end if;
 
            if v.vci_i2t.cmdval = '1' then
            -- Valid request
           
              if r.reqcnt = u_unsigned(r.len) then
                v.state := idle_t;
              end if;
 
              if r.wrap = '1' and r.addr.burst = r.addr.border then 
              -- Jump to wrapping address when border address is reached
                v.addr.burst  := r.addr.wrap;
                if v.vci_i2t.eop = '0' then 
                  v.vci_i2t.pktid(1) := '1';
                end if;
                v.vci_i2t.eop := '1'; 
              elsif r.incr = '1' then 
                v.addr.burst := get_next_addr(r.addr.burst, r.size);
              end if;
           
            end if;
     
        end case;    
       
        v.vci_i2t.pktid(3 downto 2):= v.size(1 downto 0);
        v.vci_i2t.wdata := swap_data(dwd.wdata);
        v.vci_i2t.address(2 downto 0) := (others => '0'); 
            
        if v.vci_i2t.cmdval = '1' then
          if r.reqcnt = u_unsigned(v.len) then
            v.reqcnt := 0;
          else
            v.reqcnt := reqcnt1;
          end if;
        end if;

      end if;

      freqi.di  <= vci_req_to_vector(v.vci_i2t);
      freqi.enw <= v.vci_i2t.cmdval and not freqo.full;  
      freqi.enr <= vci_t2i.cmdack and not freqo.empty;  
      vci_i2t.req <= vci_request_none;
      if freqo.empty = '0' then  
        vci_i2t.req <= vector_to_vci_req(freqo.do);
      end if;

      frai.enr <= not frao.empty and not freqo.full and rd_ra;
      fwai.enr <= not fwao.empty and not freqo.full and rd_wa;
      fwdi.enr <= not fwdo.empty and not freqo.full and (rd_wd or rd_wa);

      rin <= v;
  
  end process axi2vci_req_p;

  vci2axi_rsp_p : process(s_axi_m2s, vci_t2i, frdo, fwro, r, frspo)

    variable err     : std_ulogic;
    variable rspo    : vci_response_type;
    variable axi_s2m_rec : axi_s2m_split;
 
  begin
      
  rspo := vci_response_none;
  if frspo.empty = '0' then 
    rspo := vector_to_vci_rsp(frspo.do);
  end if;

  err := or_reduce(rspo.rerror) or r.err;
  
  axi_s2m_rec.b.bvalid := '1';
  axi_s2m_rec.b.bresp  := err & '0';
  axi_s2m_rec.b.bid    := (others => '0');
  if axi_i <= vci_s then 
    axi_s2m_rec.b.bid  := rspo.rtrdid(axi_i - 1 downto 0);
  else
    axi_s2m_rec.b.bid(vci_t - 1 downto 0) := rspo.rtrdid;
  end if;

  axi_s2m_rec.r.rvalid := '1';
  axi_s2m_rec.r.rresp  := err & '0';
  axi_s2m_rec.r.rdata  := swap_data(rspo.rdata);
  axi_s2m_rec.r.rlast  := rspo.reop and not rspo.rpktid(1);
  axi_s2m_rec.r.rid    := (others => '0');
  if axi_i <= vci_t then 
    axi_s2m_rec.r.rid  := rspo.rtrdid(axi_i - 1 downto 0); 
  else
    axi_s2m_rec.r.rid(vci_s - 1 downto 0) := rspo.rtrdid;
  end if;

  fwri.di  <= to_std_ulogic_vector(axi_s2m_rec.b);
  frdi.di  <= to_std_ulogic_vector(axi_s2m_rec.r);
  fwri.enr <= s_axi_m2s.b.bready and not fwro.empty;
  frdi.enr <= s_axi_m2s.r.rready and not frdo.empty;


  frspi.enr <= '0';
  frdi.enw <= '0';
  fwri.enw <= '0';

  if rspo.rspval = '1' then
    if rspo.rpktid(0) = '1' then 
    -- Reception of a rdata
      if frdo.full = '0' then 
        frdi.enw <= '1';
        frspi.enr <= '1';
      end if;
    else
    -- Write done
      if fwro.full = '0' then 
        frspi.enr <= '1';
        if rspo.reop = '1' then
        -- End of write transaction
          if rspo.rpktid(1) = '0' then 
          -- Not a wrap boundary
            fwri.enw <= '1';
          end if;
        end if;
      end if;
    end if;
  end if;

  frspi.enw <= vci_t2i.rsp.rspval and not frspo.full;
  frspi.di <= vci_rsp_to_vector(vci_t2i.rsp);
  vci_i2t.rspack <= not frspo.full; 
 
  end process vci2axi_rsp_p;

  
end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
