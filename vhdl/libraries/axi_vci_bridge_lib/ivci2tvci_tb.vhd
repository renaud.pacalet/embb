--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.sim_utils.all;

library memories_lib;

entity ivci2tvci_tb is
  generic(cmdfile: string);
end entity ivci2tvci_tb;
 
architecture rtl of ivci2tvci_tb is
 
  signal vci_i2t   : vci_i2t_type;  
  signal vci_t2i   : vci_t2i_type;  
  signal clk       : std_ulogic;  
  signal srstn     : std_ulogic;  
  signal hst2tvci : vci_i2t_type;
  signal tvci2hst : vci_t2i_type;
  signal eos       : boolean;

begin

  vci_master0 : entity global_lib.vci_master(arc)
  generic map(cmdfile => cmdfile)
  port map(
    clk        => clk,
    srstn      => srstn,
    eos        => eos,
    hst2tvci   => vci_i2t,
    tvci2hst   => vci_t2i
  );
  
  vci_memory_0: entity memories_lib.vci_memory(rtl)
    port map(
      clk       => clk,
      srstn     => srstn,
      vci_out   => vci_t2i,
      vci_in    => vci_i2t
    );

  process
  begin
    clk <= '0';
    wait for 7 ns;
    clk <= '1';
    wait for 7 ns;
    if eos then -- If end of simulation
      print("End of simulation, regression test passed.");
      wait; -- Wait forever (simulation should end gracefully)
    end if;
  end process;
  
end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
