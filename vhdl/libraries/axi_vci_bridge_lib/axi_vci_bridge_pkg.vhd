--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.axi_pkg_m8.all;

package axi_vci_bridge_pkg is

  constant AXI_READ  : std_ulogic := '1';
  constant AXI_WRITE : std_ulogic := '0';

  function swap_data(din : std_ulogic_vector(axi_d - 1 downto 0)) return std_ulogic_vector;
  
  function swap_strb(din : std_ulogic_vector(axi_m - 1 downto 0)) return std_ulogic_vector;
  
  type ra_fifo_in is record
    enr : std_ulogic;
    enw : std_ulogic;
    di  : std_ulogic_vector(read_address_length - 1 downto 0);
  end record;

  type ra_fifo_out is record
    empty : std_ulogic;
    full  : std_ulogic;
    do    : std_ulogic_vector(read_address_length - 1 downto 0);
  end record;

  type wa_fifo_in is record
    enr : std_ulogic;
    enw : std_ulogic;
    di  : std_ulogic_vector(write_address_length - 1 downto 0);
  end record;

  type wa_fifo_out is record
    empty : std_ulogic;
    full  : std_ulogic;
    do    : std_ulogic_vector(write_address_length - 1 downto 0);
  end record;

  type rd_fifo_in is record
    enr : std_ulogic;
    enw : std_ulogic;
    di  : std_ulogic_vector(read_data_length - 1 downto 0);
  end record;

  type rd_fifo_out is record
    empty : std_ulogic;
    full  : std_ulogic;
    do    : std_ulogic_vector(read_data_length - 1 downto 0);
  end record;

  type wd_fifo_in is record
    enr : std_ulogic;
    enw : std_ulogic;
    di  : std_ulogic_vector(write_data_length - 1 downto 0);
  end record;

  type wd_fifo_out is record
    empty : std_ulogic;
    full  : std_ulogic;
    do    : std_ulogic_vector(write_data_length - 1 downto 0);
  end record;

  type wr_fifo_in is record
    enr : std_ulogic;
    enw : std_ulogic;
    di  : std_ulogic_vector(write_response_length - 1 downto 0);
  end record;

  type wr_fifo_out is record
    empty : std_ulogic;
    full  : std_ulogic;
    do    : std_ulogic_vector(write_response_length - 1 downto 0);
  end record;

  type rsp_fifo_in is record
    enr : std_ulogic;
    enw : std_ulogic;
    di  : std_ulogic_vector(vci_response_length - 1 downto 0);
  end record;

  type rsp_fifo_out is record
    empty : std_ulogic;
    full  : std_ulogic;
    do    : std_ulogic_vector(vci_response_length - 1 downto 0);
  end record;

  type req_fifo_in is record
    enr : std_ulogic;
    enw : std_ulogic;
    di  : std_ulogic_vector(vci_request_length - 1 downto 0);
  end record;

  type req_fifo_out is record
    empty : std_ulogic;
    full  : std_ulogic;
    do    : std_ulogic_vector(vci_request_length - 1 downto 0);
  end record;

end package axi_vci_bridge_pkg;

package body axi_vci_bridge_pkg is
  
  function swap_strb(din : std_ulogic_vector(axi_m - 1 downto 0)) return std_ulogic_vector is

    variable d : std_ulogic_vector(axi_m - 1 downto 0);

  begin
    
    for i in 0 to 7 loop 
      d(7 - i) := din(i);   
    end loop;

    return d;

  end swap_strb;

  function swap_data(din : std_ulogic_vector(axi_d - 1 downto 0)) return std_ulogic_vector is

    variable d : std_ulogic_vector(axi_d - 1 downto 0);

  begin
    
    for i in 0 to 7 loop 
      d(63 - i * 8 downto 56 - i * 8) := din(7 + i * 8 downto 0 + i * 8);   
    end loop;

    return d;

  end swap_data;


end package body axi_vci_bridge_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
