--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.axi_pkg_m8.all;
use global_lib.sim_utils.all;

library memories_lib;

entity maxi2tvci_tb is
  generic(cmdfile: string);
end entity maxi2tvci_tb;

architecture rtl of maxi2tvci_tb is

  signal axi_m2s   : axi_m2s_split;
  signal axi_s2m   : axi_s2m_split;
  signal clk       : std_ulogic;
  signal srstn     : std_ulogic;
  signal b2v_vci_i2t : vci_i2t_type;
  signal v2b_vci_t2i : vci_t2i_type;
  signal eos       : boolean;

begin

  axi_master_0 : entity global_lib.axi_master(arc)
  generic map(cmdfile => cmdfile)
  port map(
    clk        => clk,
    srstn      => srstn,
    eos        => eos,
    hst2saxi   => axi_m2s,
    saxi2hst   => axi_s2m
  );

  saxi2ivci_0 : entity work.saxi2ivci(rtl)
  port map(
    clk        => clk,
    srstn      => srstn,
    vci_i2t    => b2v_vci_i2t,
    vci_t2i    => v2b_vci_t2i,
    s_axi_s2m  => axi_s2m,
    s_axi_m2s  => axi_m2s
  );

  vci_memory_0: entity memories_lib.vci_memory(rtl)
    port map(
      clk     => clk,
      srstn   => srstn,
      vci_in  => b2v_vci_i2t,
      vci_out => v2b_vci_t2i
    );

  process
  begin
    clk <= '0';
    wait for 7 ns;
    clk <= '1';
    wait for 7 ns;
    if eos then -- If end of simulation
      print("End of simulation, regression test passed.");
      wait; -- Wait forever (simulation should end gracefully)
    end if;
  end process;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
