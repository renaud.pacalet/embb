--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.axi_pkg_m4.all;
use global_lib.bvci64_pkg.all;
use global_lib.utils.all;

entity axi4lite32_slave_to_bvci64_initiator is
  generic(
    base_address: std_ulogic_vector(axi_a - 1 downto 20)
  );
  port(
    clk:     in  std_ulogic;
    srstn:   in  std_ulogic; -- synchronous active low reset
    evnt:    in  std_ulogic; -- external events signalling: cnt_reg internal register increments on every evnt change
    vci_i2t: out bvci64_i2t_type; -- Basic VCI 64 bits initiator-to-target output signals
    vci_t2i: in  bvci64_t2i_type; -- Basic VCI 64 bits target-to-initiator input signals
    axi_m2s: in  axilite_m2s; -- AXI4 lite, 32 bits, master-to-slave input signals
    axi_s2m: out axilite_s2m; -- AXI4 lite, 32 bits, slave-to-master output signals
    gpo:     out std_ulogic_vector(axi_d - 1 downto 0)
  );
end entity axi4lite32_slave_to_bvci64_initiator;

architecture rtl of axi4lite32_slave_to_bvci64_initiator is

  signal axi_m2s_r: axilite_m2s;           -- Input register for AXI requests and response acknowledges
  signal axi_s2m_l: axilite_s2m;
  signal vci_t2i_r: bvci64_t2i_type;               -- Input register for VCI responses and request acknowledges
  signal vci_i2t_l: bvci64_i2t_type;

  type states is (idle, local_write_wait_state, vci_write_wait_vci_response, local_read_wait_state, vci_read_wait_vci_response,
                  wait_axi_write_response_acknowledge, wait_axi_read_response_acknowledge);
  signal state: states;

  -- Internal registers
  signal swap_reg: std_ulogic_vector(axi_d - 1 downto 0);   -- drive byte swapping
  signal gpr_reg: std_ulogic_vector(axi_d - 1 downto 0);    -- general purpose
  signal rdatah_reg: std_ulogic_vector(axi_d - 1 downto 0); -- RDATA 32 MSBs of input register for VCI responses and request acknowledges
  signal rdatal_reg: std_ulogic_vector(axi_d - 1 downto 0); -- RDATA 32 LSBs of input register for VCI responses and request acknowledges
  signal flags_reg: std_ulogic_vector(axi_d - 1 downto 0);  -- CMDACK, RSPVAL, RERROR flags of input register for VCI responses and request acknowledges
  signal cnt_reg: std_ulogic_vector(axi_d - 1 downto 0);    -- events counter: increments on every evnt change

  -- Addresses of internal registers
  constant swap_reg_add:   std_ulogic_vector(19 downto 0) := X"00000";
  constant gpr_reg_add:    std_ulogic_vector(19 downto 0) := X"00004";
  constant rdatah_reg_add: std_ulogic_vector(19 downto 0) := X"00008";
  constant rdatal_reg_add: std_ulogic_vector(19 downto 0) := X"0000c";
  constant flags_reg_add:  std_ulogic_vector(19 downto 0) := X"00010";
  constant cnt_reg_add:    std_ulogic_vector(19 downto 0) := X"00014";

begin

  vci_i2t    <= vci_i2t_l;
  axi_s2m    <= axi_s2m_l;

  rdatah_reg <= vci_t2i_r.rdata(63 downto 32);
  rdatal_reg <= vci_t2i_r.rdata(31 downto 0);
  flags_reg(axi_d - 1 downto 3) <= (others => '0');
  flags_reg(2)                  <= vci_t2i_r.cmdack;
  flags_reg(1)                  <= vci_t2i_r.rspval;
  flags_reg(0)                  <= vci_t2i_r.rerror;

  -- Unconditionally sample VCI responses and request acknowledges
  process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        axi_m2s_r <= axilite_m2s_none;
        vci_t2i_r <= bvci64_t2i_none;
      else
        axi_m2s_r <= axi_m2s;
        vci_t2i_r <= vci_t2i;
      end if;
    end if;
  end process;

  gpo    <= gpr_reg;

  process(clk)
    variable addr: std_ulogic_vector(axi_a - 1 downto 0);
    variable addrl: std_ulogic_vector(19 downto 0);
    variable strb: std_ulogic_vector(axi_m - 1 downto 0);
    variable data: std_ulogic_vector(axi_d - 1 downto 0);
    variable evnt_pipe: std_ulogic_vector(2 downto 0);
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        gpr_reg   <= (others => '0');
        swap_reg  <= (others => '0');
        axi_s2m_l <= axilite_s2m_none;
        vci_i2t_l <= bvci64_i2t_none;
        state     <= idle;
        evnt_pipe := (others => '0');
      else
        vci_i2t_l.rspack  <= '0'; -- By default, do not aknowledge AXI requests and VCI responses
        axi_s2m_l.awready <= '0';
        axi_s2m_l.wready  <= '0';
        axi_s2m_l.arready <= '0';
        if axi_m2s.bready = '1' then -- De-assert AXI write responses when acknowledged
          axi_s2m_l.bvalid <= '0';
        end if;
        if axi_m2s.rready = '1' then -- De-assert AXI read responses when acknowledged
          axi_s2m_l.rvalid <= '0';
        end if;
        if vci_t2i.cmdack = '1' then -- De-assert VCI requests when acknowledged
          vci_i2t_l.cmdval <= '0';
        end if;
        case state is
          when idle =>
            -- If AXI input register contains write request
            if axi_m2s_r.awvalid = '1' and axi_m2s_r.wvalid = '1' then
              addr := axi_m2s_r.awaddr;
              addrl := addr(19 downto 0);
              strb := axi_m2s_r.wstrb;
              data := axi_m2s_r.wdata;
              if addr(axi_a - 1 downto 20) = base_address then -- Write to bridge local registers
                case addrl is -- Do write
                  when swap_reg_add => -- swap register
                    swap_reg <= mask_bytes(swap_reg, data, strb);
                  when gpr_reg_add =>  -- general purpose register
                    gpr_reg <= mask_bytes(gpr_reg, data, strb);
                  when others => null;
                end case;
                state <= local_write_wait_state; -- Need one wait state to avoid re-sampling the same AXI request
              else -- Write request VCI world
                vci_i2t_l.cmdval  <= '1'; -- Submit VCI write request
                vci_i2t_l.address <= addr(axi_a - 1 downto 3) & "000";
                vci_i2t_l.cmd     <= bvci64_cmd_write;
                -- By default (swap_reg = 0):
                --   address 0 <=> least significant (right) half of 64 bits dwords 
                --   address 4 <=> most significant (left) half of 64 bits dwords 
                --   no half-word swaps in 32-bits words
                --   no byte swaps in 16-bits half-words
                vci_i2t_l.be      <= vector_swap(strb & X"0", (addr(2) xor swap_reg(2)) & swap_reg(1 downto 0));
                vci_i2t_l.wdata   <= vector_swap(data & data, (addr(2) xor swap_reg(2)) & swap_reg(1 downto 0));
                state             <= vci_write_wait_vci_response;
              end if;
              axi_s2m_l.awready <= '1'; -- Acknowledge AXI write request
              axi_s2m_l.wready  <= '1';
              axi_s2m_l.bvalid  <= '1'; -- Submit AXI write response
                                        -- If AXI input register contains read request
            elsif axi_m2s_r.arvalid = '1' then
              addr := axi_m2s_r.araddr;
              if addr(axi_a - 1 downto 20) = base_address then -- Read in bridge local registers
                axi_s2m_l.rvalid  <= '1'; -- Submit AXI read response
                case addrl is -- Do read
                  when swap_reg_add => -- swap register
                    axi_s2m_l.rdata <= swap_reg;
                  when gpr_reg_add =>  -- general purpose register
                    axi_s2m_l.rdata <= gpr_reg;
                  when rdatah_reg_add => -- most significant (left) half of vci_t2i_r.rdata
                    axi_s2m_l.rdata <= rdatah_reg;
                  when rdatal_reg_add => -- least significant (right) half of vci_t2i_r.rdata
                    axi_s2m_l.rdata <= rdatal_reg;
                  when flags_reg_add => -- 0^29 & vci_t2i_r.{cmdack,rspval,rerror}
                    axi_s2m_l.rdata <= flags_reg;
                  when cnt_reg_add =>  -- event counter
                    axi_s2m_l.rdata <= cnt_reg;
                  when others =>
                    axi_s2m_l.rdata <= (others => '0');
                end case;
                axi_s2m_l.arready <= '1'; -- Acknowledge AXI read request
                axi_s2m_l.rvalid  <= '1'; -- Submit AXI read response
                state             <= local_read_wait_state; -- Need one wait state to avoid re-sampling the same AXI request
              else -- Read request VCI world
                vci_i2t_l.cmdval  <= '1'; -- Submit VCI read request
                vci_i2t_l.address <= addr(axi_a - 1 downto 3) & "000";
                vci_i2t_l.cmd     <= bvci64_cmd_read;
                -- By default (swap_reg = 0):
                --   address 0 <=> least significant (right) half of 64 bits dwords 
                --   address 4 <=> most significant (left) half of 64 bits dwords 
                --   no half-word swaps in 32-bits words
                --   no byte swaps in 16-bits half-words
                vci_i2t_l.be      <= vector_swap(X"F0", (addr(2) xor swap_reg(2)) & swap_reg(1 downto 0));
                state             <= vci_read_wait_vci_response;
              end if;
              axi_s2m_l.arready <= '1'; -- Acknowledge AXI read request
            end if;
          when local_write_wait_state =>
            state <= wait_axi_write_response_acknowledge;
          when wait_axi_write_response_acknowledge => -- Wait until AXI write response is acknowledged
            if axi_s2m_l.bvalid = '0' or axi_m2s.bready = '1' then -- If AXI write response (already) acknowledged
              state <= idle;
            end if;
          when vci_write_wait_vci_response =>
            if vci_t2i_r.rspval = '1' then
              vci_i2t_l.rspack <= '1'; -- Acknowledge VCI write response
              state            <= wait_axi_write_response_acknowledge;
            end if;
          when local_read_wait_state =>
            state <= wait_axi_read_response_acknowledge;
          when wait_axi_read_response_acknowledge => -- Wait until AXI read response is acknowledged
            if axi_s2m_l.rvalid = '0' or axi_m2s.rready = '1' then -- If AXI read response acknowledged
              state            <= idle;
            end if;
          when vci_read_wait_vci_response =>
            if vci_t2i_r.rspval = '1' then
              vci_i2t_l.rspack  <= '1'; -- Acknowledge VCI read response
              if (addr(2) xor swap_reg(2)) = '1' then
                data := vci_t2i_r.rdata(31 downto 0);
              else
                data := vci_t2i_r.rdata(63 downto 32);
              end if;
              data := vector_swap(data, swap_reg(1 downto 0));
              axi_s2m_l.rvalid <= '1';  -- Submit AXI read response
              axi_s2m_l.rdata  <= data;
              state            <= wait_axi_read_response_acknowledge;
            end if;
        end case;
        if(evnt_pipe(2) /= evnt_pipe(1)) then
          cnt_reg <= std_ulogic_vector(u_unsigned(cnt_reg) + 1);
        end if;
        evnt_pipe := evnt_pipe(1 downto 0) & evnt;
      end if;
    end if;
  end process;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
