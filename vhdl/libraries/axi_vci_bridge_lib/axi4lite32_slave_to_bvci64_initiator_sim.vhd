--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.bvci64_pkg.all;
use global_lib.axi_pkg_m4.all;
use global_lib.sim_utils.all;

entity axi4lite32_slave_to_bvci64_initiator_sim is
  generic(
    na:    positive range 2 to 16 := 10; -- Bit-width of address bus of virtual RAM
    cc:    natural := 1000000;
    debug: boolean := false
  );
  port(
    clk:         out std_ulogic;
    srstn:       out std_ulogic;
    vci_i2t:     out bvci64_i2t_type;
    vci_t2i:     out bvci64_t2i_type;
    axi_m2s:     out axilite_m2s;
    axi_s2m:     out axilite_s2m
  );
end entity axi4lite32_slave_to_bvci64_initiator_sim;

architecture sim of axi4lite32_slave_to_bvci64_initiator_sim is

  signal clk_l:     std_ulogic;
  signal srstn_l:   std_ulogic;
  signal vci_i2t_l: bvci64_i2t_type;
  signal vci_t2i_l: bvci64_t2i_type;
  signal axi_m2s_l: axilite_m2s;
  signal axi_s2m_l: axilite_s2m;
  signal eos:       boolean := false;

begin

  vci_i2t <= vci_i2t_l;
  vci_t2i <= vci_t2i_l;
  axi_m2s <= axi_m2s_l;
  axi_s2m <= axi_s2m_l;
  clk     <= clk_l;
  srstn   <= srstn_l;

  process
  begin
    clk_l <= '0';
    wait for 1 ns;
    clk_l <= '1';
    wait for 1 ns;
    if eos then
      wait;
    end if;
  end process;

  process
  begin
    srstn_l <= '0';
    for i in 1 to 10 loop
      wait until rising_edge(clk_l);
    end loop;
    srstn_l <= '1';
    for i in 1 to cc loop
      wait until rising_edge(clk_l);
      if i mod (cc / 100) = 0 then
        progress(i, cc);
      end if;
    end loop;
    eos <= true;
    report "Non-regression test passed, end of simulation.";
    wait;
  end process;

  iaxi4lite32_master: entity global_lib.axi4lite32_master(rtl)
  generic map(
    na    => na,
    debug => debug
  )
  port map(
    clk     => clk_l,
    srstn   => srstn_l,
    axi_m2s => axi_m2s_l,
    axi_s2m => axi_s2m_l
  );

  axi4lite32_slave_to_bvci64_initiator: entity work.axi4lite32_slave_to_bvci64_initiator(rtl)
  generic map(base_address => (others => '0'))
  port map(
    clk     => clk_l,
    srstn   => srstn_l,
    evnt    => '0',
    axi_m2s => axi_m2s_l,
    axi_s2m => axi_s2m_l,
    vci_i2t => vci_i2t_l,
    vci_t2i => vci_t2i_l,
    gpo     => open
  );

  ibvci64_target: entity global_lib.bvci64_target(rtl)
  generic map(
    na    => na,
    debug => debug
  )
  port map(
    clk     => clk_l,
    srstn   => srstn_l,
    vci_i2t => vci_i2t_l,
    vci_t2i => vci_t2i_l
  );

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
