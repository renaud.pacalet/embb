--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;
-- pragma translate_off
use global_lib.sim_utils.all;
-- pragma translate_on
use global_lib.axi_pkg_m8.all;

use work.axi_vci_bridge_pkg.all;

entity tvci2maxi is
  port (
      clk        : in std_logic;
      srstn      : in std_logic;
      vci_i2t    : in vci_i2t_type;
      vci_t2i    : out vci_t2i_type;
      m_axi_s2m  : in axi_s2m;
      m_axi_m2s  : out axi_m2s
    );
end entity tvci2maxi;
  
architecture rtl of tvci2maxi is

type state_type is (idle_t, write_t, read_t, wait_rsp_t);

signal rrb : std_ulogic;

signal m_axi_s2m_local: axi_s2m_split;
signal m_axi_m2s_local: axi_m2s_split;

signal drd : axi_s2m_r;
signal dwr : axi_s2m_b;
  
signal frai : ra_fifo_in;
signal frao : ra_fifo_out;
signal fwai : wa_fifo_in;
signal fwao : wa_fifo_out;
signal frdi : rd_fifo_in;
signal frdo : rd_fifo_out;
signal fwdi : wd_fifo_in;
signal fwdo : wd_fifo_out;
signal fwri : wr_fifo_in;
signal fwro : wr_fifo_out;

-- signal axi_m2s   : axi_m2s_type;
signal vci_t2i_s : vci_t2i_type;


begin

	m_axi_s2m_local <= to_axi_s2m_split(m_axi_s2m);
	m_axi_m2s <= to_axi_m2s(m_axi_m2s_local);

    vci_t2i <= vci_t2i_s;

    fifo_ra: entity global_lib.fifo 
    generic map (
      T      =>  std_ulogic_vector(address_length - 1 downto 0),
      depth  =>  2)
    port map (
      srstn =>  srstn,
      clk   =>  clk,
      read_en  =>  frai.enr,
      write_en =>  frai.enw, 
      dout  =>  frao.do,
      din   =>  frai.di,
      empty =>  frao.empty,
      full  =>  frao.full
    );

    fifo_rd: entity global_lib.fifo 
    generic map (
      T      =>  std_ulogic_vector(read_data_length - 1 downto 0),
      depth  =>  2)
    port map (
      srstn =>  srstn, 
      clk   =>  clk,
      read_en  =>  frdi.enr,
      write_en =>  frdi.enw, 
      dout  =>  frdo.do,
      din   =>  frdi.di,
      empty =>  frdo.empty,
      full  =>  frdo.full
    );

    fifo_wa: entity global_lib.fifo 
    generic map (
      T      =>  std_ulogic_vector(address_length - 1 downto 0),
      depth  =>  2)
    port map (
      srstn =>  srstn, 
      clk   =>  clk,
      read_en  =>  fwai.enr,
      write_en =>  fwai.enw, 
      dout  =>  fwao.do,
      din   =>  fwai.di,
      empty =>  fwao.empty,
      full  =>  fwao.full
    );

    fifo_wd: entity global_lib.fifo 
    generic map (
      T      =>  std_ulogic_vector(write_data_length - 1 downto 0),
      depth  =>  2)
    port map (
      srstn =>  srstn, 
      clk   =>  clk,
      read_en  =>  fwdi.enr,
      write_en =>  fwdi.enw, 
      dout  =>  fwdo.do,
      din   =>  fwdi.di,
      empty =>  fwdo.empty,
      full  =>  fwdo.full
    );

    fifo_wr: entity global_lib.fifo 
    generic map (
      T      =>  std_ulogic_vector(write_response_length - 1 downto 0),
      depth  =>  2)
    port map (
      srstn =>  srstn, 
      clk   =>  clk,
      read_en  =>  fwri.enr,
      write_en =>  fwri.enw, 
      dout  =>  fwro.do,
      din   =>  fwri.di,
      empty =>  fwro.empty,
      full  =>  fwro.full
    );
 
    -- Axi interface with fifos

    m_axi_m2s_local.ar <= to_axi_m2s_ar(frao.do) when frao.empty = '0' else axi_m2s_ar_none;
    m_axi_m2s_local.aw <= to_axi_m2s_aw(fwao.do) when fwao.empty = '0' else axi_m2s_aw_none;
    m_axi_m2s_local.w <= to_axi_m2s_w(fwdo.do) when fwdo.empty = '0' else axi_m2s_w_none;

    fwri.di  <= to_std_ulogic_vector(m_axi_s2m_local.b);
    fwri.enw <= m_axi_s2m_local.b.bvalid and not fwro.full;

    frdi.di  <= to_std_ulogic_vector(m_axi_s2m_local.r);
    frdi.enw <= m_axi_s2m_local.r.rvalid and not frdo.full;

    m_axi_m2s_local.b.bready <= not fwro.full;
    m_axi_m2s_local.r.rready <= not frdo.full;

    frai.enr <= not frao.empty and m_axi_s2m_local.ar.arready;
    fwai.enr <= not fwao.empty and m_axi_s2m_local.aw.awready;
    fwdi.enr <= not fwdo.empty and m_axi_s2m_local.w.wready;

    -- Interface with AVCI

    drd <= to_axi_s2m_r(frdo.do);
    dwr <= to_axi_s2m_b(fwro.do);

  process(clk)

    begin

    if rising_edge(clk) then
      if srstn = '0' then 
        rrb <= axi_read;
      else
        if vci_t2i_s.rsp.rspval = '1' and vci_i2t.rspack = '1' then 
          rrb <= not rrb;
        end if;
      end if;
    end if;

  end process;

  rsp_p : process(drd, dwr, rrb, fwro, frdo, vci_i2t)

    variable  vci_t2i_read_response : vci_response_type;
    variable  vci_t2i_write_response : vci_response_type;

    begin

      frdi.enr <= '0';
      fwri.enr <= '0';

      vci_t2i_read_response.rsrcid    := (others => '0');
      vci_t2i_write_response.rsrcid   := (others => '0');

      vci_t2i_read_response.rspval    := drd.rvalid;
      vci_t2i_read_response.rdata     := swap_data(drd.rdata);
      vci_t2i_read_response.rerror    := "000" & drd.rresp;
      vci_t2i_read_response.reop      := '1';
      vci_t2i_read_response.rpktid    := std_ulogic_vector(to_unsigned(1, vci_p));
      vci_t2i_read_response.rpktid(0) := '1';
      vci_t2i_read_response.rtrdid    := (others => '0');
      if vci_s <= axi_i then 
        vci_t2i_read_response.rsrcid    := drd.rid(vci_s - 1 downto 0);
      else
        vci_t2i_read_response.rsrcid(axi_i - 1 downto 0) := drd.rid;
      end if;

      vci_t2i_write_response.rspval    := dwr.bvalid;
      vci_t2i_write_response.rdata     := (others => '0');
      vci_t2i_write_response.rerror    := "000" & dwr.bresp;
      vci_t2i_write_response.reop      := '1';
      vci_t2i_write_response.rpktid    := (others => '0');
      vci_t2i_write_response.rtrdid    := (others => '0');
      if vci_s <= axi_i then 
        vci_t2i_write_response.rsrcid    := dwr.bid(vci_s - 1 downto 0);
      else
        vci_t2i_write_response.rsrcid(axi_i - 1 downto 0) := dwr.bid;
      end if;

      vci_t2i_s.rsp <= vci_response_none;

      if fwro.empty = '0' and frdo.empty = '1' then 
      -- Only write response
        vci_t2i_s.rsp <= vci_t2i_write_response;
        fwri.enr      <= vci_i2t.rspack;
      elsif fwro.empty = '1' and frdo.empty = '0' then 
      -- Only read response
        vci_t2i_s.rsp <= vci_t2i_read_response;
        frdi.enr      <= vci_i2t.rspack;
      elsif fwro.empty = '0' and frdo.empty = '0' then 
      -- Read and write responses available
        if rrb = axi_read then 
        -- Round robin arbiter selects read
          vci_t2i_s.rsp <= vci_t2i_read_response;
          frdi.enr      <= vci_i2t.rspack;
        else
        -- Round robin arbiter selects write
          vci_t2i_s.rsp <= vci_t2i_write_response;
          fwri.enr      <= vci_i2t.rspack;
        end if;
      end if;
      
  end process rsp_p;

  process(vci_i2t, fwdo, frao, fwao)

    variable ac      : axi_m2s_a;
    variable wd      : axi_m2s_w;
    variable cmdack  : std_ulogic;

    begin

      cmdack   := '0';
      frai.enw <= '0';
      fwai.enw <= '0';
      fwdi.enw <= '0';

      ac := axi_m2s_a_none;
      wd := axi_m2s_w_none;

      if vci_i2t.req.cmdval = '1' then 
      -- New request

        ac.aaddr  := vci_i2t.req.address;
        ac.asize  := "011";
        ac.aburst := "01";
        ac.avalid := '1';
        ac.alen   := (others => '0');
        ac.alock  := (others => '0');
        ac.acache := (others => '0');
        ac.aprot  := (others => '0');
        if vci_s <= axi_i then 
          ac.aid(vci_s - 1 downto 0) := vci_i2t.req.srcid;
        else
          ac.aid := vci_i2t.req.srcid(axi_i - 1 downto 0);
        end if;
        
        wd.wdata  := swap_data(vci_i2t.req.wdata);
        wd.wstrb  := swap_strb(vci_i2t.req.be);
        wd.wlast  := '1';
        wd.wvalid := '1';

        if vci_i2t.req.cmd = "01" then 
        -- Read
          cmdack   := not frao.full;
          frai.enw <= cmdack;
        else
          cmdack   := not fwao.full and not fwdo.full;
          fwai.enw <= cmdack;
          fwdi.enw <= cmdack;
        end if;
      end if;

      frai.di  <= to_std_ulogic_vector(ac);
      fwai.di  <= to_std_ulogic_vector(ac);
      fwdi.di  <= to_std_ulogic_vector(wd);

      vci_t2i_s.cmdack <= cmdack;
        
  end process;
  
end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
