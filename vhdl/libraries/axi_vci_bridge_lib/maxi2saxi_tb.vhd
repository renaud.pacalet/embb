--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.sim_utils.all;
use global_lib.axi_pkg_m8.all;

library memories_lib;

entity maxi2saxi_tb is
  generic(cmdfile: string);
end entity maxi2saxi_tb;
 
architecture rtl of maxi2saxi_tb is
 
  signal axi_m2s   : axi_m2s_split;  
  signal axi_s2m   : axi_s2m_split;  
  signal clk       : std_ulogic;  
  signal srstn     : std_ulogic;  
  signal hst2tvci  : vci_i2t_type;
  signal tvci2hst  : vci_t2i_type;
  signal eos       : boolean;

begin

  axi_master0 : entity global_lib.axi_master(arc)
  generic map(cmdfile => cmdfile)
  port map(
    clk        => clk,
    srstn      => srstn,
    eos        => eos,
    hst2saxi   => axi_m2s,
    saxi2hst   => axi_s2m
  );
  
  axi_memory_0: entity memories_lib.axi_memory(rtl)
    port map(
      clk       => clk,
      srstn     => srstn,
      s_axi_s2m => axi_s2m,
      s_axi_m2s => axi_m2s
    );

  process
  begin
    clk <= '0';
    wait for 7 ns;
    clk <= '1';
    wait for 7 ns;
    if eos then -- If end of simulation
      print("End of simulation, regression test passed.");
      wait; -- Wait forever (simulation should end gracefully)
    end if;
  end process;
  
end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
