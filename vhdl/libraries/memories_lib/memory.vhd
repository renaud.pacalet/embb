--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

library memories_lib;
use memories_lib.ram_pkg.all;

entity memory is
  generic(registered: boolean := true;
          na   : integer := 10; --bit-width of address busses
          nb   : integer := 8   -- byte-width of data busses
  );
  port(clk   : in std_ulogic;
       run   : in std_ulogic;
       en    : in std_ulogic;                              -- enable
       rnw   : in std_ulogic;                              -- read not write
       be    : in std_ulogic_vector(nb - 1 downto 0);      -- byte enable
       add   : in std_ulogic_vector(na - 1 downto 0);      -- address bus
       wdata : in std_ulogic_vector(nb * 8 - 1 downto 0);  -- write data
       gnt   : out std_ulogic_vector(nb - 1 downto 0);     -- grant (one bit per byte)
       rdata : out std_ulogic_vector(nb * 8 - 1 downto 0)  -- read data
      );       
end entity memory;

architecture rtl of memory is
  
  type di_in_type is record
    en: std_ulogic; -- Active high enables
    we: std_ulogic_vector(nb - 1 downto 0); -- Active high write enables
    add: std_ulogic_vector(na - 1 downto 0); -- Address busses
    din: std_ulogic_vector(nb * 8 - 1 downto 0); -- Input data busses
  end record;

  constant di_in_none : di_in_type := (en => '0', we => (others => '0'), add => (others => '0'), din => (others => '0'));

  type di_out_type is record
    dout: std_ulogic_vector(nb * 8 - 1 downto 0); -- Output data busses
  end record;
  
  signal di_in: di_in_type;
  signal di_out: di_out_type;

  signal g : std_ulogic_vector(nb - 1 downto 0);

  begin

  spram_0 : entity memories_lib.spram
  generic map (registered => registered,
               na   => na, -- bit-width of address busses
               nb   => nb   -- byte-width of data busses
  )
  port map (
   en => di_in.en,
   clk => clk,
   add => di_in.add,
   din => di_in.din,
   we => di_in.we,
   dout => di_out.dout);

 process(clk)

   variable rndv : std_ulogic;

   begin

   if (clk'event and clk = '1') then

     rndv := std_ulogic_rnd;

     if rndv = '1' then 
       g <= (others => '1');
     else
	     g <= std_ulogic_vector_rnd(nb);
     end if;

   end if;
 end process;

  
 arbiter: process(all)

   variable di_in_v: di_in_type;

  begin

    di_in_v := di_in_none;

    if en = '1' then 

      di_in_v.en    := '1';
      di_in_v.we    := (others => not rnw);
      di_in_v.add   := add;
      di_in_v.din   := wdata;
      di_in_v.we    := di_in_v.we and be;

      if run = '0' then  
        gnt <= be;
      else
        gnt <= (g and be);
      end if;

    end if;

    di_in    <= di_in_v;
    
  end process arbiter;

  rdata <= di_out.dout;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
