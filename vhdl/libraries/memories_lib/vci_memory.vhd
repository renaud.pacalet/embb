--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;


entity vci_memory is
  generic(na   : integer := 10; --bit-width of address busses
          nb   : integer := 8   -- byte-width of data busses
  );
  port (
    clk:     in std_ulogic;      -- master clock
    srstn:   in std_ulogic;      -- synchronous active low reset
    vci_in:  in vci_i2t_type;    -- AVCI target input port
    vci_out: out vci_t2i_type    -- AVCI target output port
  );
end entity vci_memory;

architecture rtl of vci_memory is
  
type v_type is record
  en    : std_ulogic;
  rnw   : std_ulogic;
  last  : std_ulogic;
  ack   : std_ulogic;
  err   : std_ulogic;
  pktid : std_ulogic_vector(vci_p - 1 downto 0);
  srcid : std_ulogic_vector(vci_s - 1 downto 0);
  trdid : std_ulogic_vector(vci_t - 1 downto 0);
end record;

constant v_none : v_type := (en => '0', rnw => '0', last => '0', ack=> '0', err => '1', pktid => (others => '0'), srcid => (others => '0'), trdid => (others => '0'));

type pipe_type is array(natural range <>) of v_type;

constant depth : natural := 1;

signal pipe : pipe_type(0 to depth);

type rsp_fifo_in is record
  enr : std_ulogic;
  enw : std_ulogic;
  di  : std_ulogic_vector(vci_response_length - 1 downto 0);
end record;

type rsp_fifo_out is record
  empty : std_ulogic;
  full  : std_ulogic;
  do    : std_ulogic_vector(vci_response_length - 1 downto 0);
end record;

type vci_type is record 
  en      : std_ulogic;
  rnw     : std_ulogic;
  ack     : std_ulogic;
  be      : std_ulogic_vector(vci_b - 1 downto 0);
  last    : std_ulogic;
  wdata   : std_ulogic_vector(vci_b * 8 - 1 downto 0);
  add     : std_ulogic_vector(vci_n - 1 downto 0);
  pktid   : std_ulogic_vector(vci_p - 1 downto 0);
  srcid   : std_ulogic_vector(vci_s - 1 downto 0);
  trdid   : std_ulogic_vector(vci_t - 1 downto 0);
  vci_t2i : vci_t2i_type;
end record;

type memory_in_type is record
  en    : std_ulogic;
  rnw   : std_ulogic;
  be    : std_ulogic_vector(nb - 1 downto 0);
  add   : std_ulogic_vector(na - 1 downto 0);
  wdata : std_ulogic_vector(nb * 8 - 1 downto 0);
end record;

type memory_out_type is record
  gnt   : std_ulogic_vector(nb - 1 downto 0);    
  rdata : std_ulogic_vector(nb * 8 - 1 downto 0);
end record;

signal min : memory_in_type;
signal mout : memory_out_type;

signal r, rin  : vci_type;

signal d : vci_response_type;

signal fi : rsp_fifo_in;
signal fo : rsp_fifo_out;

signal rspack : std_ulogic;
signal cmdack : std_ulogic;

begin

  memory_0 : entity work.memory 
  port map (
    clk   =>  clk,
    run   =>  '1',
    en    =>  min.en,   
    rnw   =>  min.rnw,  
    be    =>  min.be,   
    add   =>  min.add,  
    wdata =>  min.wdata,
    gnt   =>  mout.gnt,  
    rdata =>  mout.rdata
  );

  fifo_rsp: entity global_lib.fifo 
  generic map (
    T      =>  std_ulogic_vector(vci_response_length - 1 downto 0),
    depth  =>  3)
  port map (
    srstn =>  srstn, 
    clk   =>  clk,
    read_en  =>  fi.enr,
    write_en =>  fi.enw, 
    dout  =>  fo.do,
    din   =>  fi.di,
    empty =>  fo.empty,
    full  =>  fo.full
  );

  d <= vector_to_vci_rsp(fo.do) when fo.empty = '0' else vci_response_none;

  rspack <= not(rin.vci_t2i.rsp.rspval and not vci_in.rspack);
  cmdack <= not(vci_in.req.cmdval      and not rin.vci_t2i.cmdack);

  process(all)
  
    variable v       : vci_type;
    variable ack     : std_ulogic;

  begin

    v := r; 
      
    v.en  := '0'; 
    v.ack := '0'; 

    v.vci_t2i.cmdack     := '0';

    v.pktid  := vci_in.req.pktid;
    v.srcid  := vci_in.req.srcid;
    v.trdid  := vci_in.req.trdid;
    v.last   := vci_in.req.eop;
    v.rnw    := vci_in.req.cmd(0);
    v.be     := vci_in.req.be;
    v.add    := vci_in.req.address;
    v.wdata  := vci_in.req.wdata;
    v.en     := vci_in.req.cmdval and rspack;

    if v.en = '1' then 
      if v.be = mout.gnt then
        v.vci_t2i.cmdack := '1';
        v.ack := '1';
      end if;
    end if;

    if r.en = '1' and r.ack = '0' then
      min.en    <= r.en; 
      min.add   <= r.add(3 + na - 1 downto 3); 
      min.wdata <= r.wdata; 
      min.be    <= r.be; 
      min.rnw   <= r.rnw;
    else
      min.en    <= v.en; 
      min.wdata <= v.wdata; 
      min.add   <= v.add(3 + na - 1 downto 3); 
      min.be    <= v.be; 
      min.rnw   <= v.rnw;
    end if; 

    v.vci_t2i.rsp := d; 

    rin <= v;
    
  end process;

  process(all)

    variable rsp : vci_response_type;

  begin

    rsp := vci_response_none;

    if pipe(depth).en = '1' then
      if pipe(depth).ack = '1' then 
      -- New r/w operation finished 
        rsp.rsrcid    := pipe(depth).srcid;
        rsp.rpktid    := pipe(depth).pktid;
        rsp.rtrdid    := pipe(depth).trdid;
        rsp.reop      := pipe(depth).last;
        rsp.rerror(0) := pipe(depth).err;
        rsp.rdata     := mout.rdata;
        rsp.rspval    := '1';
      end if;
    end if;

    fi.di  <= vci_rsp_to_vector(rsp);
    fi.enr <= rspack and not fo.empty;
    fi.enw <= rsp.rspval; 

  end process;

  vci_out <= rin.vci_t2i;

  process(clk)

  begin
    if (clk'event and clk = '1') then
      if srstn = '0' then
        r.en  <= '0'; 
        r.ack <= '0'; 
        for i in 0 to depth loop
          pipe(i) <= v_none;
        end loop;
      else

        r <= rin;

        pipe(0).en    <= rin.en;
        pipe(0).srcid <= rin.srcid;
        pipe(0).pktid <= rin.pktid;
        pipe(0).trdid <= rin.trdid;
        pipe(0).ack   <= rin.ack;
        pipe(0).last  <= rin.last;
        pipe(0).rnw   <= rin.rnw;
        pipe(0).err   <= '0';
        
        for i in 0 to depth - 1 loop
          pipe(i + 1)  <= pipe(i);
        end loop;

      end if;
     end if;
   end process;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
