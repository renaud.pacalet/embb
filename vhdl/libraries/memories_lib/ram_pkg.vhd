--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Utility package

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

package ram_pkg is

	-- decode_we translates a read_not_write (rnw) indicator and the A LSBs of an
	-- address (add) into a 2^A bits write enable signal for a 2^A bytes wide ram.
	-- When rnw is high (read operation) it outputs "0..0". When rnw is low (write
	-- operation) it outputs "10..0" when add="0..0" (write the leftmost byte),
	-- "010..0" when add="0..01", ... and "0..01" when add="1..1" (write rightmost
	-- byte).
  function decode_we(rnw: std_ulogic; add: std_ulogic_vector) return std_ulogic_vector;
  function decode_we(rnw: std_ulogic; add: std_ulogic) return std_ulogic_vector;

  type clients is (pss, uc, dma, vci); -- the 4 clients of a memory subsystem

  subtype ports is natural range 0 to 1;
--  type ports is (a, b); -- two ports per physical block ram

  type in_port_4kx8 is record -- type of an input port of a 4kx8 ram
    en:    std_ulogic; -- enable
    we:    std_ulogic_vector(0 downto 0); -- write enable (active high)
    add:   std_ulogic_vector(11 downto 0); -- address bus
    wdata: word8; -- write data
  end record;

  -- type of inputs (2 ports) of a 4kx8 ram
  type in_ram_4kx8 is array(ports) of in_port_4kx8;

  -- type of all input requests from all the clients of a 4kx8 ram
  type in_reqs_4kx8 is array(clients, ports) of in_port_4kx8;

  -- default value for an input port of a 4kx8 ram
  constant in_port_4kx8_default: in_port_4kx8 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  type in_port_128x8 is record -- type of an input port of a 128x8 ram
    en:    std_ulogic; -- enable
    we:    std_ulogic_vector(0 downto 0); -- write enable (active high)
    add:   std_ulogic_vector(6 downto 0); -- address bus
    wdata: word8; -- write data
  end record;

  -- type of all input requests from all the clients of a 128x8 ram
  type in_reqs_128x8 is array(clients, ports) of in_port_128x8;

  -- default value for an input port of a 128x8 ram
  constant in_port_128x8_default: in_port_128x8 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  type in_port_4kx16 is record -- type of an input port of a 4kx16 ram
    en:    std_ulogic; -- enable
    we:    std_ulogic_vector(1 downto 0); -- write enable (active high)
    add:   std_ulogic_vector(11 downto 0); -- address bus
    wdata: word16; -- write data
  end record;

  -- type of inputs (2 ports) of a 4kx16 ram
  type in_ram_4kx16 is array(ports) of in_port_4kx16;

  -- type of all input requests from all the clients of a 4kx16 ram
  type in_reqs_4kx16 is array(clients, ports) of in_port_4kx16;

  -- default value for an input port of a 4kx16 ram
  constant in_port_4kx16_default: in_port_4kx16 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  type in_port_2kx8 is record -- type of an input port of a 2kx8 ram
    en:    std_ulogic; -- enable
    we:    std_ulogic_vector(0 downto 0); -- write enable (active high)
    add:   std_ulogic_vector(10 downto 0); -- address bus
    wdata: word8; -- write data
  end record;

  -- type of inputs (2 ports) of a 2kx8 ram
  type in_ram_2kx8 is array(ports) of in_port_2kx8;

  -- type of all input requests from all the clients of a 2kx8 ram
  type in_reqs_2kx8 is array(clients, ports) of in_port_2kx8;

  -- default value for an input port of a 2kx8 ram
  constant in_port_2kx8_default: in_port_2kx8 := (
    en => '0', we => (others => '0'), add => (others => '0'), wdata => (others => '0'));

  type in_port_8kx8 is record -- type of an input port of a 8kx8 ram
    en:    std_ulogic; -- enable
    we:    std_ulogic_vector(0 downto 0); -- write enable (active high)
    add:   std_ulogic_vector(12 downto 0); -- address bus
    wdata: word8; -- write data
  end record;

  -- type of inputs (2 ports) of a 8kx8 ram
  type in_ram_8kx8 is array(ports) of in_port_8kx8;

  -- type of all input requests from all the clients of a 8kx8 ram
  type in_reqs_8kx8 is array(clients, ports) of in_port_8kx8;

  -- default value for an input port of a 8kx8 ram
  constant in_port_8kx8_default: in_port_8kx8 := (
    en => '0', we => (others => '0'), add => (others => '0'), wdata => (others => '0'));

  -- type of outputs (2 ports) of a x8 ram
  type out_ram_x8 is array(ports) of word8;
  
  type in_port_1kx27 is record -- type of an input port of a 1kx27 ram
    en:    std_ulogic; -- enable
    we:    std_ulogic_vector(2 downto 0);  -- write enable (active high)
    add:   std_ulogic_vector(9 downto 0);  -- address bus
    wdata: std_ulogic_vector(26 downto 0); -- write data
  end record;

  constant in_port_1kx27_default: in_port_1kx27 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  type in_port_1kx16 is record -- type of an input port of a 1kx16 ram
    en:    std_ulogic; -- enable
    we:    std_ulogic_vector(1 downto 0); -- write enable (active high)
    add:   std_ulogic_vector(9 downto 0); -- address bus
    wdata: word16; -- write data
  end record;

  -- type of inputs (2 ports) of a 1kx16 ram
  type in_ram_1kx16 is array(ports) of in_port_1kx16;

  -- type of all input requests from all the clients of a 1kx16 ram
  type in_reqs_1kx16 is array(clients, ports) of in_port_1kx16;

  -- default value for an input port of a 1kx16 ram
  constant in_port_1kx16_default: in_port_1kx16 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  -- type of outputs (2 ports) of a x16 ram
  type out_ram_x16 is array(ports) of word16;

  type in_port_2kx16 is record -- type of an input port of a 2kx16 ram
    en:    std_ulogic; -- enable
    we:    std_ulogic_vector(1 downto 0); -- write enable (active high)
    add:   std_ulogic_vector(10 downto 0); -- address bus
    wdata: word16; -- write data
  end record;

  -- type of inputs (2 ports) of a 2kx16 ram
  type in_ram_2kx16 is array(ports) of in_port_2kx16;

  -- type of all input requests from all the clients of a 2kx16 ram
  type in_reqs_2kx16 is array(clients, ports) of in_port_2kx16;

  -- default value for an input port of a 2kx16 ram
  constant in_port_2kx16_default: in_port_2kx16 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  type in_port_8kx16 is record -- type of an input port of a 8kx16 ram
    en:    std_ulogic; -- enable
    we:    std_ulogic_vector(1 downto 0); -- write enable (active high)
    add:   std_ulogic_vector(12 downto 0); -- address bus
    wdata: word16; -- write data
  end record;

  -- default value for an input port of a 8kx16 ram
  constant in_port_8kx16_default: in_port_8kx16 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  type in_port_16kx16 is record -- type of an input port of a 16kx16 ram
    en:    std_ulogic; -- enable
    we:    std_ulogic_vector(1 downto 0); -- write enable (active high)
    add:   std_ulogic_vector(13 downto 0); -- address bus
    wdata: word16; -- write data
  end record;

  -- type of inputs (2 ports) of a 16kx16 ram
  type in_ram_16kx16 is array(ports) of in_port_16kx16;

  -- type of all input requests from all the clients of a 16kx16 ram
  type in_reqs_16kx16 is array(clients, ports) of in_port_16kx16;

  -- default value for an input port of a 16kx16 ram
  constant in_port_16kx16_default: in_port_16kx16 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  type in_port_1kx18 is record -- type of an input port of a 1kx18 ram
    en:    std_ulogic; -- enable
    we:    std_ulogic_vector(1 downto 0); -- write enable (active high)
    add:   std_ulogic_vector(9 downto 0); -- address bus
    wdata: word18; -- write data
  end record;

  -- type of inputs (2 ports) of a 1kx18 ram
  type in_ram_1kx18 is array(ports) of in_port_1kx18;

  -- type of all input requests from all the clients of a 1kx18 ram
  type in_reqs_1kx18 is array(clients, ports) of in_port_1kx18;

  -- default value for an input port of a 1kx18 ram
  constant in_port_1kx18_default: in_port_1kx18 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  -- type of outputs (2 ports) of a x18 ram
  type out_ram_x18 is array(ports) of word18;

  type in_port_1kx24 is record -- type of an input port of a 1kx24 ram
    en:    std_ulogic; -- enable
    we:    std_ulogic_vector(2 downto 0); -- write enable (active high)
    add:   std_ulogic_vector(9 downto 0); -- address bus
    wdata: word24; -- write data
  end record;

  -- type of inputs (2 ports) of a 1kx24 ram
  type in_ram_1kx24 is array(ports) of in_port_1kx24;

  -- type of all input requests from all the clients of a 1kx24 ram
  type in_reqs_1kx24 is array(clients, ports) of in_port_1kx24;

  -- default value for an input port of a 1kx24 ram
  constant in_port_1kx24_default: in_port_1kx24 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  -- type of outputs (2 ports) of a x24 ram
  type out_ram_x24 is array(ports) of word24;

  type in_port_1kx32 is record -- type of an input port of a 1kx32 ram
    en: std_ulogic; -- enable
    we: std_ulogic_vector(3 downto 0); -- write enable (active high)
    add: std_ulogic_vector(9 downto 0); -- address bus
    wdata: word32; -- write data
  end record;

  -- type of inputs (2 ports) of a 1kx32 ram
  type in_ram_1kx32 is array(ports) of in_port_1kx32;

  -- type of all input requests from all the clients of a 1kx32 ram
  type in_reqs_1kx32 is array(clients, ports) of in_port_1kx32;

  -- default value for an input port of a 1kx32 ram
  constant in_port_1kx32_default: in_port_1kx32 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  -- type of outputs (2 ports) of a x32 ram
  type out_ram_x32 is array(ports) of word32;

  type in_port_2kx32 is record -- type of an input port of a 2kx32 ram
    en: std_ulogic; -- enable
    we: std_ulogic_vector(3 downto 0); -- write enable (active high)
    add: std_ulogic_vector(10 downto 0); -- address bus
    wdata: word32; -- write data
  end record;

  type in_port_2kx24 is record -- type of an input port of a 2kx24 ram
    en: std_ulogic; -- enable
    we: std_ulogic_vector(2 downto 0); -- write enable (active high)
    add: std_ulogic_vector(10 downto 0); -- address bus
    wdata: word24; -- write data
  end record;

  -- default value for an input port of a 2kx24 ram
  constant in_port_2kx24_default: in_port_2kx24 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  -- type of inputs (2 ports) of a 2kx32 ram
  type in_ram_2kx32 is array(ports) of in_port_2kx32;

  -- type of all input requests from all the clients of a 2kx32 ram
  type in_reqs_2kx32 is array(clients, ports) of in_port_2kx32;

  -- default value for an input port of a 2kx32 ram
  constant in_port_2kx32_default: in_port_2kx32 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  type in_port_1kx54 is record -- type of an input port of a 1kx54 ram
    en: std_ulogic; -- enable
    we: std_ulogic_vector(5 downto 0); -- write enable (active high)
    add: std_ulogic_vector(9 downto 0); -- address bus
    wdata: word54; -- write data
  end record;

  -- type of inputs (2 ports) of a 1kx54 ram
  type in_ram_1kx54 is array(ports) of in_port_1kx54;

  -- type of all input requests from all the clients of a 1kx54 ram
  type in_reqs_1kx54 is array(clients, ports) of in_port_1kx54;

  -- default value for an input port of a 1kx54 ram
  constant in_port_1kx54_default: in_port_1kx54 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  -- type of outputs (2 ports) of a x54 ram
  type out_ram_x54 is array(ports) of word54;

  type in_port_1kx64 is record -- type of an input port of a 1kx64 ram
    en: std_ulogic; -- enable
    we: std_ulogic_vector(7 downto 0); -- write enable (active high)
    add: std_ulogic_vector(9 downto 0); -- address bus
    wdata: word64; -- write data
  end record;

  -- type of inputs (2 ports) of a 1kx64 ram
  type in_ram_1kx64 is array(ports) of in_port_1kx64;

  -- type of all input requests from all the clients of a 1kx64 ram
  type in_reqs_1kx64 is array(clients, ports) of in_port_1kx64;

  -- default value for an input port of a 1kx64 ram
  constant in_port_1kx64_default: in_port_1kx64 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  -- type of outputs (2 ports) of a x64 ram
  type out_ram_x64 is array(ports) of word64;

  type in_port_1kx128 is record -- type of an input port of a 1kx128 ram
    en: std_ulogic; -- enable
    we: std_ulogic_vector(15 downto 0); -- write enable (active high)
    add: std_ulogic_vector(9 downto 0); -- address bus
    wdata: word128; -- write data
  end record;

  -- type of inputs (2 ports) of a 1kx128 ram
  type in_ram_1kx128 is array(ports) of in_port_1kx128;

  -- type of all input requests from all the clients of a 1kx128 ram
  type in_reqs_1kx128 is array(clients, ports) of in_port_1kx128;

  -- default value for an input port of a 1kx128 ram
  constant in_port_1kx128_default: in_port_1kx128 := (
    en => '0', we => (others => '0'), add => (others => '0'),
    wdata => (others => '0'));

  -- type of outputs (2 ports) of a x128 ram
  type out_ram_x128 is array(ports) of word128;

  type in_port_128kx64 is record -- type of an input port of a 128kx64 ram
    en: std_ulogic; -- enable
    we: std_ulogic_vector(7 downto 0); -- write enable (active high)
    add: std_ulogic_vector(16 downto 0); -- address bus
    wdata: word64; -- write data
  end record;

  -- default value for an input port of a 1kx128 ram
  constant in_port_128kx64_default: in_port_128kx64 := (en => '0', we => (others => '0'), add => (others => '0'), wdata => (others => '0'));

-- Masking function.
  -- mask takes a N bits word (wdata) and a N/8 bits word (we) and returns a N
  -- bits word. each byte of the results is either a copy of the input byte at
  -- the same position in wdata or a null byte, depending on the value of the
  -- correponding bit in we (null if we(i) = '1').
  function mask(wdata: std_ulogic_vector; data: std_ulogic_vector; we: std_ulogic) return
    std_ulogic_vector;
  function mask(wdata: std_ulogic_vector; data: std_ulogic_vector; we: std_ulogic_vector) return
    std_ulogic_vector;

end package ram_pkg;

package body ram_pkg is

	function decode_we(rnw: std_ulogic; add: std_ulogic_vector) return std_ulogic_vector is
  begin
    return band((not rnw), decode(add));
	end function decode_we;

	function decode_we(rnw: std_ulogic; add: std_ulogic) return std_ulogic_vector is
    variable a: std_ulogic_vector(0 to 0);
  begin
		a(0) := add;
		return decode_we(rnw, a);
	end function decode_we;

  function mask(wdata: std_ulogic_vector; data: std_ulogic_vector; we: std_ulogic) return
    std_ulogic_vector is
    variable tmp: std_ulogic_vector(wdata'length - 1 downto 0) := wdata;
    variable wel: std_ulogic := we;
  begin
-- pragma translate_off
    assert wdata'length = 8 or wdata'length = 9
      report "mask: illegal parameters lengths"
      severity warning;
    assert wdata'length = data'length
      report "mask: illegal parameters lengths"
      severity warning;
-- pragma translate_on
    if we = '0' then -- the byte is not written
      tmp := data;
    end if;
    return tmp;
  end function mask;

  function mask(wdata: std_ulogic_vector; data: std_ulogic_vector; we: std_ulogic_vector) return
    std_ulogic_vector is
    variable tmp1: std_ulogic_vector(wdata'length - 1 downto 0) := wdata;
    variable tmp2: std_ulogic_vector(data'length - 1 downto 0) := data;
    variable wel: std_ulogic_vector(we'length - 1 downto 0) := we;
  begin
-- pragma translate_off
    assert wdata'length = data'length
      report "mask: illegal parameters lengths"
      severity warning;
-- pragma translate_on
    if wdata'length mod 8 = 0 then
-- pragma translate_off
      assert 8 * wel'length = wdata'length
        report "mask: illegal parameters lengths"
        severity warning;
-- pragma translate_on
      for i in wel'length - 1 downto 0 loop
        if wel(i) = '0' then -- the byte is not written
          tmp1(8 * i + 7 downto 8 * i) := (others => '0');
        else -- the byte is written
          tmp2(8 * i + 7 downto 8 * i) := (others => '0');
        end if;
      end loop;
    elsif wdata'length mod 9 = 0 then
-- pragma translate_off
      assert 9 * wel'length = wdata'length
        report "mask: illegal parameters lengths"
        severity warning;
-- pragma translate_on
      for i in wel'length - 1 downto 0 loop
        if wel(i) = '0' then -- the byte is not written
          tmp1(9 * i + 8 downto 9 * i) := (others => '0');
        else -- the byte is written
          tmp2(9 * i + 8 downto 9 * i) := (others => '0');
        end if;
      end loop;
-- pragma translate_off
    else
      assert false
        report "mask: unsupported byte width"
        severity warning;
-- pragma translate_on
    end if;
    return tmp1 or tmp2;
  end function mask;

end package body ram_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
