#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

gh-IGNORE	+= memories_lib.dctdpram memories_lib.dctdpram_sim memories_lib.tdpram memories_lib.tdpram_sim
gh-IGNORE	+= memories_lib.axi_memory memories_lib.vci_memory memories_lib.sdpram

memories_lib.dctdpram: \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils

memories_lib.dctdpram_sim: \
	random_lib.rnd \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils \
	memories_lib.dctdpram

memories_lib.ram_pkg: \
	global_lib.global \
	global_lib.utils

memories_lib.ram_sim_pkg: \
	global_lib.sim_utils

memories_lib.sdpram: \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils

memories_lib.spram: \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils

memories_lib.tdpram: \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils

memories_lib.tdpram_sim: \
	random_lib.rnd \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils \
	memories_lib.tdpram

memories_lib.memory: \
  global_lib.utils \
  global_lib.global \
  memories_lib.spram \
  memories_lib.ram_pkg \
  random_lib.rnd

memories_lib.vci_memory: \
  global_lib.global \
  global_lib.utils \
  global_lib.fifo \
  memories_lib.memory

memories_lib.ahb_memory: \
  global_lib.global \
  global_lib.ahb_pkg \
  global_lib.utils \
  ahb_vci_bridge_lib.ahb_vci_bridge_pkg \
  memories_lib.memory

memories_lib.axi_memory: \
  global_lib.global \
  global_lib.utils \
  global_lib.fifo \
  global_lib.axi_pkg_m8 \
  memories_lib.memory

MS-SIM-TESTS	= dctdpram_sim tdpram_sim

%.hex:
	@[[ "$@" =~ ([0-9]+)kx([0-9]+).hex ]]; \
	if [ $$? -ne 0 ]; then \
	  echo "$@ is not a valid name for a simulation data file"; \
	  exit -1; \
	fi; \
	d=$${BASH_REMATCH[1]}; \
	w=$${BASH_REMATCH[2]}; \
	awk --assign d=$$d --assign w=$$w 'END {m = w % 4; w = w - m; for(i = 0; i < d * 1024; i++) {if(m != 0) printf("%x", rand() * (2^m)); for(j = 0; j < w / 4; j++) printf("%x", rand() * (2^4)); printf("\n");}}' /dev/null > $@

$(addprefix ms-sim.memories_lib.,tdpram_sim dctdpram_sim): 1kx56.hex
$(addprefix ms-sim.memories_lib.,tdpram_sim dctdpram_sim): MSSIMFLAGS += -Gna=10 -Gnb=7 -Ginit_file=\"1kx56.hex\" -Gsi=0 -Gn=0 -do 'run -all; quit'
ms-sim.memories_lib: $(addprefix ms-sim.memories_lib.,tdpram_sim dctdpram_sim)
ms-sim: ms-sim.memories_lib

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
