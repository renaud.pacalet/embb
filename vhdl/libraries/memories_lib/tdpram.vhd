--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief True dual port RAM
--*
--*  a dual port synchronous RAM model that should be compatible with the Xilinx RAM Blocks with the following parameters:
--* - The RAM Block is a True Dual Port one (two ports that can be each used either for read or write)
--* - The RAM Block mode is read first
--* - The RAM Block is clocked on positive edges of a single clock
--* - Individual byte write enables
--* - Ouput registers are optional (registered generic parameter)

-- pragma translate_off
use std.textio.all;
-- pragma translate_on
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- use work.ram_pkg.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;
-- pragma translate_off
use global_lib.sim_utils.all;
-- pragma translate_on

entity tdpram is
  generic(
-- pragma translate_off
          debug: boolean := true;
          -- Name of initialization file if any. The initialization file is
          -- ASCII and contains -- u_unsigned hexadecimal values, one per
          -- address, in addresses increasing -- order, separated by spaces
          -- (spaces, tabs or CR).
          init_file: string := "";
          -- Address of the first word in RAM of the first data in
          -- initialization file. If this parameter is not 0, then some low
          -- addresses are uninitialized.
          si: natural := 0;
          instance_name: string := "tdpram"; -- Name of the RAM instance.
-- pragma translate_on
          registered: boolean := true; -- control output registers
          na: natural := 10; -- bit-width of address busses
          nb: positive := 4); -- byte-width of data busses
  port(clk: in  std_ulogic; -- Clock. The RAM samples its inputs on the rising edge of clk.
       ena, enb: in std_ulogic; -- Active high enables
       wea, web: in std_ulogic_vector(nb - 1 downto 0); -- Active high write enables
       adda, addb: in std_ulogic_vector(na - 1 downto 0); -- Address busses
       dina, dinb: in std_ulogic_vector(nb * 8 - 1 downto 0); -- Input data busses
       douta, doutb: out std_ulogic_vector(nb * 8 - 1 downto 0)); -- Output data busses
end entity tdpram;

architecture arc of tdpram is

  constant ww: positive := nb * 8;

  subtype word is std_ulogic_vector(ww - 1 downto 0);
  type word_array is array(natural range <>) of word;
  subtype tmem is word_array(0 to 2**na - 1);

-- pragma translate_off
  type shared_word is protected
    impure function get return word;
    procedure set(val: word);
  end protected shared_word;

  type shared_word is protected body
    variable w: word := (others => '0');
    impure function get return word is
    begin
      return w;
    end function get;
    procedure set(val: word) is
    begin
      w := val;
    end procedure set;
  end protected body shared_word;

  impure function init_mem return tmem is
    variable l: line;
    variable mem: tmem; -- The variable used to build the returned value.
    file f: text; -- File descriptor for initialization file.
    constant nbx4: positive := 4 * ((ww + 3) / 4);
    subtype wordx4 is std_ulogic_vector(nbx4 - 1 downto 0);
    constant zero: std_ulogic_vector(nbx4 - 1 downto ww) := (others => '0');
    variable val: wordx4;
    variable i: natural; -- Address index.
  begin
    if init_file'length = 0 then -- If no initialization file.
      return mem; -- tdpram array uninitialized: (others => (others => 'U'))
    end if;
    file_open(f, init_file); -- Else, open the file.
    i := si; -- Address of first initialized word in the RAM.
    while not endfile(f) loop -- Parse the file.
      readline(f, l); -- Read a new line.
      while l'length /= 0 loop -- Parse the line.
        hread(l, val); -- Hex-read a value.
        assert i < 2**na
          report "tdpram: out of range address during ASCII initialization: " &
            integer'image(i) & " (instance " & instance_name & ")"
          severity failure;
        assert or_reduce(val(nbx4 - 1 downto ww)) = '0'
          report "tdpram: out of range value during ASCII initialization: " &
            integer'image(to_integer(u_unsigned(val))) & " (instance " & instance_name & ")"
          severity failure;
        mem(i) := val(ww - 1 downto 0); -- Assign the value.
        i := i + 1; -- Next address.
      end loop;
    end loop;
    file_close(f); -- Close the file.
    return mem; -- Return initialized tdpram array.
  end function init_mem;
-- pragma translate_on

  shared variable mem: tmem
-- pragma translate_off
                            := init_mem
-- pragma translate_on
                                       ; -- DO NOT DELETE THIS LINE
-- pragma translate_off
  shared variable lock: shared_boolean;
  shared variable data: shared_word;
-- pragma translate_on

  signal douta1, doutb1: word;
  signal ena1, enb1: std_ulogic;

begin

  porta: process(clk)
    variable add: natural range 0 to 2**na - 1;
  begin
    if rising_edge(clk) then
      ena1 <= ena;
      if registered and ena1 = '1' then
        douta <= douta1;
      end if;
      if ena = '1' then
        add := to_integer(u_unsigned(adda));
        douta1 <= mem(add);
        if not registered then
          douta <= mem(add);
        end if;
-- pragma translate_off
        if not lock.get then
          data.set(mem(add));
        else
          if enb = '1' and add = to_integer(u_unsigned(addb)) then
            douta1 <= data.get;
            if not registered then
              douta <= data.get;
            end if;
          end if;
        end if;
-- pragma translate_on
        for b in nb - 1 downto 0 loop
          if wea(b) = '1' then
            mem(add)(8 * b + 8 - 1 downto 8 * b) := dina(8 * b + 8 - 1 downto 8 * b);
-- pragma translate_off
            if enb = '1' and web(b) = '1' and add = to_integer(u_unsigned(addb)) then
              mem(add)(8 * b + 8 - 1 downto 8 * b) := (others => 'X');
              assert not debug
                report instance_name & ": write conflict @" & integer'image(add) & " byte #" & integer'image(b)
                severity warning;
            end if;
-- pragma translate_on
          end if;
        end loop;
      end if;
-- pragma translate_off
      lock.flip;
-- pragma translate_on
    end if;
  end process porta;

  portb: process(clk)
    variable add: natural range 0 to 2**na - 1;
  begin
    if rising_edge(clk) then
      enb1 <= enb;
      if registered and enb1 = '1' then
        doutb <= doutb1;
      end if;
      if enb = '1' then
        add := to_integer(u_unsigned(addb));
        doutb1 <= mem(add);
        if not registered then
          doutb <= mem(add);
        end if;
-- pragma translate_off
        if not lock.get then
          data.set(mem(add));
        else
          if ena = '1' and add = to_integer(u_unsigned(adda)) then
            doutb1 <= data.get;
            if not registered then
              doutb <= data.get;
            end if;
          end if;
        end if;
-- pragma translate_on
        for b in nb - 1 downto 0 loop
          if web(b) = '1' then
            mem(add)(8 * b + 8 - 1 downto 8 * b) := dinb(8 * b + 8 - 1 downto 8 * b);
-- pragma translate_off
            if ena = '1' and wea(b) = '1' and add = to_integer(u_unsigned(adda)) then
              mem(add)(8 * b + 8 - 1 downto 8 * b) := (others => 'X');
              assert not debug
                report instance_name & ": write conflict @" & integer'image(add) & " byte #" & integer'image(b)
                severity warning;
            end if;
-- pragma translate_on
          end if;
        end loop;
      end if;
-- pragma translate_off
      lock.flip;
-- pragma translate_on
    end if;
  end process portb;

end architecture arc;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
