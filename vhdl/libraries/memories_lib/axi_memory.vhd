--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.utils.all;
use global_lib.global.all;
use global_lib.axi_pkg_m8.all;

entity axi_memory is
  generic(na   : integer := 10; --bit-width of address busses
          nb   : integer := 8   -- byte-width of data busses
  );
  port (
    clk       : in std_ulogic;     -- master clock
    srstn     : in std_ulogic;     -- synchronous active low reset
    s_axi_s2m : out axi_s2m_split;
    s_axi_m2s : in axi_m2s_split
  );
end entity axi_memory;

architecture rtl of axi_memory is

type state_type is (idle_t, write_t, read_t);

type memory_in_type is record
  en    : std_ulogic;
  rnw   : std_ulogic;
  be    : std_ulogic_vector(nb - 1 downto 0);
  add   : std_ulogic_vector(na - 1 downto 0);
  wdata : std_ulogic_vector(nb * 8 - 1 downto 0);
end record;

type memory_out_type is record
  gnt   : std_ulogic_vector(nb - 1 downto 0);    
  rdata : std_ulogic_vector(nb * 8 - 1 downto 0);
end record;

signal min : memory_in_type;
signal mout : memory_out_type;

type v_type is record
  en    : std_ulogic;
  rnw   : std_ulogic;
  last  : std_ulogic;
  ack   : std_ulogic;
  err   : std_ulogic;
  id    : std_ulogic_vector(axi_i - 1 downto 0);
end record;

constant v_none : v_type := (en => '0', err => '0',  rnw => '0', last => '0', ack=> '0', id => (others => '0'));

type pipe_type is array(natural range <>) of v_type;

constant depth : natural := 1;

signal pipe : pipe_type(0 to depth);

type address_type is record 
  start  : std_ulogic_vector(axi_a - 1 downto 0);
  align  : std_ulogic_vector(axi_a - 1 downto 0);
  wrap   : std_ulogic_vector(axi_a - 1 downto 0);
  burst  : std_ulogic_vector(axi_a - 1 downto 0);
  border : std_ulogic_vector(axi_a - 1 downto 0);
end record;

type axi_type is record 
  en      : std_ulogic;
  rnw     : std_ulogic;
  ack     : std_ulogic;
  be      : std_ulogic_vector(axi_m - 1 downto 0);
  last    : std_ulogic;
  wdata   : std_ulogic_vector(axi_d - 1 downto 0);
  id      : std_ulogic_vector(axi_i - 1 downto 0);
  add     : std_ulogic_vector(axi_a - 1 downto 0);
  addr    : address_type;
  size    : std_ulogic_vector(axi_s - 1 downto 0);
  len     : std_ulogic_vector(axi_l - 1 downto 0);
  wrap    : std_ulogic;
  incr    : std_ulogic;
  rrb     : std_ulogic;
  err     : std_ulogic;
  state   : state_type;
  reqcnt  : natural;
  axi_s2m : axi_s2m_split;
end record;

constant AXI_READ  : std_ulogic := '1';
constant AXI_WRITE : std_ulogic := '0';

signal r, rin  : axi_type;
signal wrack   : std_ulogic;
signal rdack   : std_ulogic;

signal drd : axi_s2m_r;
signal dwr : axi_s2m_b;

type rd_fifo_in is record
  enr : std_ulogic;
  enw : std_ulogic;
  di  : std_ulogic_vector(read_data_length - 1 downto 0);
end record;

type rd_fifo_out is record
  empty : std_ulogic;
  full  : std_ulogic;
  do    : std_ulogic_vector(read_data_length - 1 downto 0);
end record;

type wr_fifo_in is record
  enr : std_ulogic;
  enw : std_ulogic;
  di  : std_ulogic_vector(write_response_length - 1 downto 0);
end record;

type wr_fifo_out is record
  empty : std_ulogic;
  full  : std_ulogic;
  do    : std_ulogic_vector(write_response_length - 1 downto 0);
end record;

signal frdi : rd_fifo_in;
signal frdo : rd_fifo_out;
signal fwri : wr_fifo_in;
signal fwro : wr_fifo_out;

begin

  memory_0 : entity work.memory 
  port map (
    clk   =>  clk,
    run   =>  '1',
    en    =>  min.en,   
    rnw   =>  min.rnw,  
    be    =>  min.be,   
    add   =>  min.add,  
    wdata =>  min.wdata,
    gnt   =>  mout.gnt,  
    rdata =>  mout.rdata
  );
        
  fifo_rd: entity global_lib.fifo 
  generic map (
    T      =>  std_ulogic_vector(read_data_length - 1 downto 0),
    depth  =>  3)
  port map (
    srstn =>  srstn, 
    clk   =>  clk,
    read_en  =>  frdi.enr,
    write_en =>  frdi.enw, 
    dout  =>  frdo.do,
    din   =>  frdi.di,
    empty =>  frdo.empty,
    full  =>  frdo.full
  );
  
  fifo_wr: entity global_lib.fifo 
  generic map (
    T      =>  std_ulogic_vector(write_response_length - 1 downto 0),
    depth  =>  3)
  port map (
    srstn =>  srstn, 
    clk   =>  clk,
    read_en  =>  fwri.enr,
    write_en =>  fwri.enw, 
    dout  =>  fwro.do,
    din   =>  fwri.di,
    empty =>  fwro.empty,
    full  =>  fwro.full
  );

  wrack <= not(rin.axi_s2m.b.bvalid and not s_axi_m2s.b.bready);
  rdack <= not(rin.axi_s2m.r.rvalid and not s_axi_m2s.r.rready); 

  drd <= to_axi_s2m_r(frdo.do) when frdo.empty = '0' else axi_s2m_r_none;
  dwr <= to_axi_s2m_b(fwro.do) when fwro.empty = '0' else axi_s2m_b_none;

  process(all)
  
    variable da      : axi_m2s_a;
    variable rval    : std_ulogic;
    variable wval    : std_ulogic;
    variable waval   : std_ulogic;
    variable wdval   : std_ulogic;
    variable ropv    : std_ulogic; 
    variable wopv    : std_ulogic; 
    variable v       : axi_type;
    variable ack     : std_ulogic;
    variable err     : std_ulogic;

  begin

    v := r; 
      
    ack := wrack and rdack;

    rval  := s_axi_m2s.ar.arvalid;
    wdval := s_axi_m2s.w.wvalid;
    waval := s_axi_m2s.aw.awvalid;
    wval  := wdval and waval;
  
    ropv := '0'; 
    wopv := '0'; 

    da   :=  axi_m2s_a_none;

    v.en   := '0'; 
    v.last := '0'; 
    v.ack  := '0'; 

    v.axi_s2m.ar.arready := '0';
    v.axi_s2m.aw.awready := '0';
    v.axi_s2m.w.wready  := '0';
    v.axi_s2m.r.rvalid  := '0';
    v.axi_s2m.b.bvalid  := '0';

    if r.en = '1' and r.ack = '1' then
 
      v.add := r.addr.burst;
      if r.wrap = '1' and r.addr.burst = r.addr.border then 
      -- Jump to wrapping address when border address is reached
        v.addr.burst  := r.addr.wrap;
      elsif r.incr = '1' then 
        v.addr.burst := get_next_addr(r.addr.burst, r.size);
      end if;

    end if;

    case r.state is 
  
      when idle_t =>
    
        if rval = '1' and wval = '0' then
          da   := to_axi_m2s_a(s_axi_m2s.ar);
          ropv := '1'; 
        elsif wval = '1' and rval = '0' then 
          da   := to_axi_m2s_a(s_axi_m2s.aw);
          wopv := '1'; 
        elsif rval = '1' and wval = '1' then 
        -- Conflict
          if r.rrb = axi_read then 
          -- Round Robin arbiter select read interface
            da     := to_axi_m2s_a(s_axi_m2s.ar);
            ropv   := '1'; 
            v.rrb := axi_write;
          else
          -- Round Robin arbiter select write interface
            da     := to_axi_m2s_a(s_axi_m2s.aw);
            wopv   := '1'; 
            v.rrb := axi_read;
          end if;
        end if;

        if da.aburst(1) = '1' then
        -- Wrap mode
-- pragma translate_off
          v.err :=  check_wrap_parameters(da.aaddr, da.asize, da.alen); 
-- pragma translate_on
          v.addr.wrap := get_wrap_addr(da.aaddr, da.asize, da.alen);
        end if;

        v.len             := da.alen;
        v.wrap            := da.aburst(1);
        v.incr            := or_reduce(da.aburst);
        v.size            := da.asize;
        v.addr.start      := da.aaddr;
        v.addr.align      := get_align_addr(da.aaddr, da.asize);
        v.addr.border     := get_border_addr(v.addr.wrap, v.size, da.alen);
        v.addr.burst      := get_next_addr(v.addr.align, v.size);

        if ropv = '1' then 
  
          -- New read operation
 
          v.rnw := '1';
          v.be  := X"FF";

          
          v.axi_s2m.ar.arready := '1';

          v.state := read_t;
   
  
        elsif wopv = '1' then 
          
          -- New write operation
  
          v.wdata := s_axi_m2s.w.wdata;
          v.be    := s_axi_m2s.w.wstrb;
          v.rnw   := '0';

          v.axi_s2m.aw.awready := '1';

          v.state := write_t;
   
        end if; 
  
        if ropv = '1' or wopv = '1' then 

          v.en   := ack;
          v.id   := da.aid;
          v.add  := da.aaddr;
 
          if u_unsigned(v.len) = 0 then  
            v.last  := '1';
            if v.en = '1' then 
              if v.be = mout.gnt then
                v.state := idle_t;
              end if;
            end if;
          end if;

        end if;
  
        when read_t =>
          v.en := ack;
          if v.en = '1' then 
            if v.be = mout.gnt then
              if r.reqcnt = u_unsigned(r.len) then
                v.state := idle_t;
              end if;
            end if;
          end if;
   
        when write_t =>
          v.en    := wdval and ack;
          v.wdata := s_axi_m2s.w.wdata;
          v.be    := s_axi_m2s.w.wstrb;
          if v.en = '1' then 
            if v.be = mout.gnt then
              if r.reqcnt = u_unsigned(r.len) then
                v.state := idle_t;
              end if;
            end if;
          end if;
     
      end case;
      
      if v.en = '1' and ack = '1' then 
        if v.reqcnt = u_unsigned(v.len) then
          v.last := '1'; 
        end if;
        if v.be = mout.gnt then
          v.axi_s2m.w.wready := '1';
          if r.reqcnt = u_unsigned(v.len) then
            v.reqcnt := 0;
          else
            v.reqcnt := r.reqcnt + 1;
          end if;
          v.ack := '1';
        end if;
      end if;

      
      v.axi_s2m.r := drd;
      v.axi_s2m.b := dwr;


      if r.en = '1' and r.ack = '0' then
        min.en    <= r.en; 
        min.add   <= r.add(na + 3 - 1 downto 3); 
        min.wdata <= r.wdata; 
        min.be    <= r.be; 
        min.rnw   <= r.rnw;
      else
        min.en    <= v.en; 
        min.wdata <= v.wdata; 
        min.add   <= v.add(na + 3 - 1 downto 3); 
        min.be    <= v.be; 
        min.rnw   <= v.rnw;
      end if;

      rin <= v;
    
  end process;

  process(all)

    variable rd : axi_s2m_r;
    variable wr : axi_s2m_b;

  begin

    rd.rvalid := '0';
    wr.bvalid := '0';

    if pipe(depth).en = '1' then
      if pipe(depth).ack = '1' then 
      -- New r/w operation finished 
        if pipe(depth).rnw = '1' then 
        -- Read data available
          rd.rid    := pipe(depth).id;
          rd.rdata  := mout.rdata;
          rd.rresp  := r.err & '0';
          rd.rlast  := pipe(depth).last;
          rd.rvalid := '1';
        elsif pipe(depth).last = '1' then
        -- Last data written
          wr.bid    := pipe(depth).id;
          wr.bresp  := r.err & '0';
          wr.bvalid := '1';
        end if;
      end if;
    end if;

      
    fwri.di  <= to_std_ulogic_vector(wr);
    fwri.enr <= wrack and not fwro.empty;
    fwri.enw <= wr.bvalid; 

    frdi.di  <= to_std_ulogic_vector(rd);
    frdi.enr <= rdack and not frdo.empty;
    frdi.enw <= rd.rvalid; 

  end process;

  s_axi_s2m <= rin.axi_s2m;

  process(clk)

  begin
    if (clk'event and clk = '1') then
      if srstn = '0' then 
        r.en  <= '0'; 
        r.err <= '0'; 
        for i in 0 to depth loop
          pipe(i) <= v_none;
        end loop;
      else

        r <= rin;

        pipe(0).en   <= rin.en;
        pipe(0).ack  <= rin.ack;
        pipe(0).last <= rin.last;
        pipe(0).id   <= rin.id;
        pipe(0).rnw  <= rin.rnw;
        pipe(0).err  <= rin.err;
        
        for i in 0 to depth - 1 loop
          pipe(i + 1)  <= pipe(i);
        end loop;

      end if;
     end if;
   end process;
  

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
