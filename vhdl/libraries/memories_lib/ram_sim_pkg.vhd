--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Utility package for simulations

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.sim_utils.all;

package ram_sim_pkg is

  function ram_file_name(prefix: string; idx: natural) return string;

  -- Print debug information on a transaction: name, read or write, address,
  -- read data and - if write - byte enables and written data.
  procedure print(name: in string; rnw: in std_ulogic; be: in std_ulogic_vector;
    add: in natural; wdata: in std_ulogic_vector; rdata: in std_ulogic_vector);

end package ram_sim_pkg;

package body ram_sim_pkg is

  function ram_file_name(prefix: string; idx: natural) return string is
    variable res: string(1 to prefix'length + 13);
    variable l: line;
  begin
    if prefix'length /= 0 then
      write(l, prefix);
      write(l, string'("_"));
      hwrite(l, to_unsigned(idx, 32));
      write(l, string'(".txt"));
      res := l.all;
      deallocate(l);
      return res;
    else
      return "";
    end if;
  end function ram_file_name;

  procedure print(name: in string; rnw: in std_ulogic; be: in std_ulogic_vector;
    add: in natural; wdata: in std_ulogic_vector; rdata: in std_ulogic_vector) is
    variable l: line;
  begin
    write(l, string'("@"));
    write(l, now);
    write(l, string'(", "));
    write(l, name);
    if rnw = '1' then
      write(l, string'(": RD("));
      write(l, add);
      write(l, string'(") should return "));
      hwrite(l, rdata);
    else
      write(l, string'(": WR("));
      write(l, add);
      write(l, string'(", "));
      write(l, be);
      write(l, string'(", "));
      hwrite(l, wdata);
      write(l, string'(") should return "));
      hwrite(l, rdata);
    end if;
    writeline(output, l);
  end procedure print;

end package body ram_sim_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
