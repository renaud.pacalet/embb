--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Single port RAM
--*
--*  a single port synchronous RAM model that should be compatible with the Xilinx RAM Blocks with the following parameters:
--* - The RAM Block is a Single Port one (one read-write port)
--* - The RAM Block mode is read first
--* - The RAM Block is clocked on positive edges of a single clock
--* - Individual byte write enables
--* - Ouput registers are optional (registered generic parameter)

-- pragma translate_off
use std.textio.all;
-- pragma translate_on

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- use work.ram_pkg.all;

library global_lib;
-- pragma translate_off
use global_lib.global.all;
use global_lib.utils.all;
use global_lib.sim_utils.all;
-- pragma translate_on

entity spram is
  generic(
-- pragma translate_off
          -- Name of initialization file if any. The initialization file is
          -- ASCII and contains u_unsigned hexadecimal values, one per
          -- address, in addresses increasing order, separated by spaces
          -- (spaces, tabs or CR).
          init_file: string := "";
          -- Address of the first word in RAM of the first data in
          -- initialization file. If this parameter is not 0, then some low
          -- addresses are uninitialized.
          si: natural := 0;
          instance_name: string := "spram"; -- Name of the RAM instance.
-- pragma translate_on
          registered: boolean := true; -- control output registers
          na: natural := 10; -- bit-width of address busses
          nb: positive := 4); -- byte-width of data busses
  port(clk: in  std_ulogic; -- Clock. The RAM samples its inputs on the rising edge of clk.
       en: in std_ulogic; -- Active high enable
       we: in std_ulogic_vector(nb - 1 downto 0); -- Active high write enables
       add: in std_ulogic_vector(na - 1 downto 0); -- Address bus
       din: in std_ulogic_vector(nb * 8 - 1 downto 0); -- Input data bus
       dout: out std_ulogic_vector(nb * 8 - 1 downto 0)); -- Output data bus
end entity spram;

architecture arc of spram is

  constant ww: positive := nb * 8;
  subtype word is std_ulogic_vector(ww - 1 downto 0);
  type word_array is array(natural range <>) of word;
  subtype tmem is word_array(0 to 2**na - 1);

-- pragma translate_off
  impure function init_mem return tmem is
    variable l: line;
    variable mem: tmem; -- The variable used to build the returned value.
    file f: text; -- File descriptor for initialization file.
    constant nbx4: positive := 4 * ((ww + 3) / 4);
    subtype wordx4 is std_ulogic_vector(nbx4 - 1 downto 0);
    constant zero: std_ulogic_vector(nbx4 - 1 downto ww) := (others => '0');
    variable val: wordx4;
    variable i: natural; -- Address index.
  begin
    if init_file'length = 0 then -- If no initialization file.
      return mem; -- spram array uninitialized: (others => (others => 'U'))
    end if;
    file_open(f, init_file); -- Else, open the file.
    i := si; -- Address of first initialized word in the RAM.
    while not endfile(f) loop -- Parse the file.
      readline(f, l); -- Read a new line.
      while l'length /= 0 loop -- Parse the line.
        hread(l, val); -- Hex-read a value.
        assert i < 2**na
          report "spram: out of range address during ASCII initialization: " &
            integer'image(i) & " (instance " & instance_name & ")"
          severity failure;
        assert or_reduce(val(nbx4 - 1 downto ww)) = '0'
          report "spram: out of range value during ASCII initialization: " &
            integer'image(to_integer(u_unsigned(val))) & " (instance " & instance_name & ")"
          severity failure;
        mem(i) := val(ww - 1 downto 0); -- Assign the value.
        i := i + 1; -- Next address.
      end loop;
    end loop;
    file_close(f); -- Close the file.
    return mem; -- Return initialized spram array.
  end function init_mem;
-- pragma translate_on

  signal dout1: word;
  signal en1: std_ulogic;

begin

  process(clk)
    variable mem: tmem
-- pragma translate_off
                       := init_mem
-- pragma translate_on
                                  ; -- DO NOT DELETE THIS LINE
    variable a: natural range 0 to 2**na - 1;
  begin
    if rising_edge(clk) then
      en1 <= en;
      if registered and en1 = '1' then
        dout <= dout1;
      end if;
      if en = '1' then
        a := to_integer(u_unsigned(add));
        dout1 <= mem(a);
        if not registered then
          dout <= mem(a);
        end if;
        for b in nb - 1 downto 0 loop
          if we(b) = '1' then
            mem(a)(8 * b + 8 - 1 downto 8 * b) := din(8 * b + 8 - 1 downto 8 * b);
          end if;
        end loop;
      end if;
    end if;
  end process;

end architecture arc;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
