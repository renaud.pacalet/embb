--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Utility package for the interleaver
--*
--*  The INTL_PKG package defines constants for the different address mapping. It also defines the custom PSS2MSS_TYPE and MSS2PSS_TYPE record
--* types used to interface the PSS and the memory subsystem.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;

library memories_lib;
use memories_lib.ram_pkg.all;

use work.bitfield.all;

package intl_pkg is

  constant cmdsize:  positive := bitfield_width/8;

  type pss2mss_type is record
    enp:   std_ulogic;  -- enable read channel p
    enx:   std_ulogic;  -- enable read channel x
    eny:   std_ulogic;  -- enable read/write channel y
    rnwy:  std_ulogic;  -- read-not-write for channel y
    addp:  std_ulogic_vector(15 downto 0); -- address for channel p
    addx:  std_ulogic_vector(15 downto 0); -- address for channel x
    addy:  std_ulogic_vector(15 downto 0); -- read/write address for channel y
    y:     std_ulogic_vector(7 downto 0);  -- write data for channel y
  end record;

  type mss2pss_type is record
    x: std_ulogic_vector(7 downto 0);  -- read data from channel x
    y: std_ulogic_vector(7 downto 0);  -- read data from channel y
    p: std_ulogic_vector(15 downto 0); -- read data from channel p
  end record;

  -- Interleaver component definition
  component intl is
    generic(
-- pragma translate_off
      debug:    boolean := true;  --* Print debug information
      verbose:  boolean := false; --* Print more debug information
-- pragma translate_on
      n0:       positive := 1;  --* Number of input pipeline registers in MSS, including input registers of RAMs
      n1:       positive := 1); --* Number of output pipeline registers in MSS, including output registers of RAMs
    port(
      clk:            in  std_ulogic;  -- master clock
      srstn:          in  std_ulogic;  -- master reset
      ce:             in  std_ulogic;  -- chip enable
      hirq:           in  std_ulogic;  -- host input irq,
      pirq:           out std_ulogic;  -- PSS output irq,
      dirq:           out std_ulogic;  -- DMA output irq,
      uirq:           out std_ulogic;  -- UC output irq,
      tvci_in:        in  vci_i2t_type;  -- Target AVCI input
      tvci_out:       out vci_t2i_type; -- Target AVCI output
      ivci_in:        in  vci_t2i_type;  -- Initiator AVCI input
      ivci_out:       out vci_i2t_type); -- Initiator AVCI output
  end component;

  function div3(a: u_unsigned(9 downto 0)) return u_unsigned;
  function div5(a: u_unsigned(10 downto 0)) return u_unsigned;
  function div7(a: u_unsigned(10 downto 0)) return u_unsigned;
  function divide(a: u_unsigned(10 downto 0); d: natural range 0 to 7) return u_unsigned;

  --RAMS interfaces
  type u_in_type is array(0 to 1) of in_port_1kx32;
  type u_out_type is array(0 to 1) of word32;
  type xy_in_type is array(0 to 1, 0 to 7) of in_port_8kx8;
  type xy_out_type is array(0 to 1, 0 to 7) of word8;
  type p_in_type is array(0 to 1, 0 to 3) of in_port_16kx16;
  type p_out_type is array(0 to 1, 0 to 3) of word16;

  type mss2rams_type is record
    u : u_in_type;
    x : xy_in_type;
    y : xy_in_type;
    p : p_in_type;
  end record;
  type mss2rams_vector is array(natural range <>) of mss2rams_type;

  type rams2mss_type is record
    u : u_out_type;
    x : xy_out_type;
    y : xy_out_type;
    p : p_out_type;
  end record;
  type rams2mss_vector is array(natural range <>) of rams2mss_type;

end package intl_pkg;

package body intl_pkg is

  type div_3_table_entry is record
    q0: natural range 0 to 5;
    q1: natural range 0 to 80;
    q2: natural range 0 to 170;
    r0, r1, r2: natural range 0 to 2;
  end record;
  type div_3_table is array(0 to 15) of div_3_table_entry;

  constant d3t: div_3_table := (
    (0, 0, 0, 0, 0, 0),                                                                                                                            
    (0, 5, 85, 1, 1, 1),                                                                                                                           
    (0, 10, 170, 2, 2, 2),                                                                                                                         
    (1, 16, 0, 0, 0, 0),                                                                                                                           
    (1, 21, 0, 1, 1, 0),                                                                                                                           
    (1, 26, 0, 2, 2, 0),                                                                                                                           
    (2, 32, 0, 0, 0, 0),                                                                                                                           
    (2, 37, 0, 1, 1, 0),                                                                                                                           
    (2, 42, 0, 2, 2, 0),                                                                                                                           
    (3, 48, 0, 0, 0, 0),                                                                                                                           
    (3, 53, 0, 1, 1, 0),                                                                                                                           
    (3, 58, 0, 2, 2, 0),                                                                                                                           
    (4, 64, 0, 0, 0, 0),                                                                                                                           
    (4, 69, 0, 1, 1, 0),                                                                                                                           
    (4, 74, 0, 2, 2, 0),                                                                                                                           
    (5, 80, 0, 0, 0, 0));

  type div_5_table_entry is record
    q0: natural range 0 to 3;
    q1: natural range 0 to 48;
    q2: natural range 0 to 204;
    r0, r1, r2: natural range 0 to 4;
  end record;
  type div_5_table is array(0 to 15) of div_5_table_entry;

  constant d5t: div_5_table := (
    (0, 0, 0, 0, 0, 0),                                                                                                                            
    (0, 3, 51, 1, 1, 1),                                                                                                                           
    (0, 6, 102, 2, 2, 2),                                                                                                                          
    (0, 9, 153, 3, 3, 3),                                                                                                                          
    (0, 12, 204, 4, 4, 4),                                                                                                                         
    (1, 16, 0, 0, 0, 0),                                                                                                                           
    (1, 19, 0, 1, 1, 0),                                                                                                                           
    (1, 22, 0, 2, 2, 0),                                                                                                                           
    (1, 25, 0, 3, 3, 0),                                                                                                                           
    (1, 28, 0, 4, 4, 0),                                                                                                                           
    (2, 32, 0, 0, 0, 0),                                                                                                                           
    (2, 35, 0, 1, 1, 0),                                                                                                                           
    (2, 38, 0, 2, 2, 0),                                                                                                                           
    (2, 41, 0, 3, 3, 0),                                                                                                                           
    (2, 44, 0, 4, 4, 0),                                                                                                                           
    (3, 48, 0, 0, 0, 0));

  type div_7_table_entry is record
    q0: natural range 0 to 3;
    q1: natural range 0 to 34;
    q2: natural range 0 to 219;
    r0, r1, r2: natural range 0 to 6;
  end record;
  type div_7_table is array(0 to 15) of div_7_table_entry;

  constant d7t: div_7_table := (
    (0, 0, 0, 0, 0, 0),                                                                                                                            
    (0, 2, 36, 1, 2, 4),                                                                                                                           
    (0, 4, 73, 2, 4, 1),                                                                                                                           
    (0, 6, 109, 3, 6, 5),                                                                                                                          
    (0, 9, 146, 4, 1, 2),                                                                                                                          
    (0, 11, 182, 5, 3, 6),                                                                                                                         
    (0, 13, 219, 6, 5, 3),                                                                                                                         
    (1, 16, 0, 0, 0, 0),                                                                                                                           
    (1, 18, 0, 1, 2, 0),                                                                                                                           
    (1, 20, 0, 2, 4, 0),                                                                                                                           
    (1, 22, 0, 3, 6, 0),                                                                                                                           
    (1, 25, 0, 4, 1, 0),                                                                                                                           
    (1, 27, 0, 5, 3, 0),                                                                                                                           
    (1, 29, 0, 6, 5, 0),                                                                                                                           
    (2, 32, 0, 0, 0, 0),                                                                                                                           
    (2, 34, 0, 1, 2, 0));

  function div3(a: u_unsigned(9 downto 0)) return u_unsigned is
    variable va: u_unsigned(9 downto 0) := a;
    variable ia0, ia1: natural range 0 to 15;
    variable ia2: natural range 0 to 3;
    variable q0: natural range 0 to 5;
    variable q1: natural range 0 to 80;
    variable q2: natural range 0 to 170;
    variable r0: natural range 0 to 2;
    variable r1: natural range 0 to 2;
    variable r2: natural range 0 to 2;
    variable q: natural range 0 to 255;
    variable r: natural range 0 to 6;
  begin
    ia0 := to_integer(va(3 downto 0));
    ia1 := to_integer(va(7 downto 4));
    ia2 := to_integer(va(9 downto 8));
     q0 := d3t(ia0).q0;
     r0 := d3t(ia0).r0;
     q1 := d3t(ia1).q1;
     r1 := d3t(ia1).r1;
     q2 := d3t(ia2).q2;
     r2 := d3t(ia2).r2;
      q := q0 + q1 + q2;
      r := r0 + r1 + r2;
    if r = 6 then
      q := q + 2;
    elsif r >= 3 then
      q := q + 1;
    end if;
    return to_unsigned(q, 8);
  end function div3;

  function div5(a: u_unsigned(10 downto 0)) return u_unsigned is
    variable va: u_unsigned(10 downto 0) := a;
    variable ia0, ia1: natural range 0 to 15;
    variable ia2: natural range 0 to 4;
    variable q0: natural range 0 to 3;
    variable q1: natural range 0 to 48;
    variable q2: natural range 0 to 204;
    variable r0: natural range 0 to 4;
    variable r1: natural range 0 to 4;
    variable r2: natural range 0 to 4;
    variable q: natural range 0 to 255;
    variable r: natural range 0 to 12;
  begin
    ia0 := to_integer(va(3 downto 0));
    ia1 := to_integer(va(7 downto 4));
    ia2 := to_integer(va(10 downto 8));
     q0 := d5t(ia0).q0;
     r0 := d5t(ia0).r0;
     q1 := d5t(ia1).q1;
     r1 := d5t(ia1).r1;
     q2 := d5t(ia2).q2;
     r2 := d5t(ia2).r2;
      q := q0 + q1 + q2;
      r := r0 + r1 + r2;
    if r >= 10 then
      q := q + 1;
    end if;
    if r >= 5 then
      q := q + 1;
    end if;
    return to_unsigned(q, 8);
  end function div5;

  function div7(a: u_unsigned(10 downto 0)) return u_unsigned is
    variable va: u_unsigned(10 downto 0) := a;
    variable ia0, ia1: natural range 0 to 15;
    variable ia2: natural range 0 to 4;
    variable q0: natural range 0 to 2;
    variable q1: natural range 0 to 34;
    variable q2: natural range 0 to 219;
    variable r0: natural range 0 to 6;
    variable r1: natural range 0 to 6;
    variable r2: natural range 0 to 6;
    variable q: natural range 0 to 255;
    variable r: natural range 0 to 18;
  begin
    ia0 := to_integer(va(3 downto 0));
    ia1 := to_integer(va(7 downto 4));
    ia2 := to_integer(va(10 downto 8));
     q0 := d7t(ia0).q0;
     r0 := d7t(ia0).r0;
     q1 := d7t(ia1).q1;
     r1 := d7t(ia1).r1;
     q2 := d7t(ia2).q2;
     r2 := d7t(ia2).r2;
      q := q0 + q1 + q2;
      r := r0 + r1 + r2;
    if r >= 14 then
      q := q + 1;
    end if;
    if r >= 7 then
      q := q + 1;
    end if;
    return to_unsigned(q, 8);
  end function div7;

  -- divide by (d+1), that is, 1, 2, 3, 4, 5, 6, 7 or 8, rounding towards zero
  function divide(a: u_unsigned(10 downto 0); d: natural range 0 to 7) return u_unsigned is
    variable tmp: u_unsigned(7 downto 0);
  begin
    case d is
      when 0 => tmp := a(7 downto 0);
      when 1 => tmp := a(8 downto 1);
      when 2 => tmp := div3(a(9 downto 0));
      when 3 => tmp := a(9 downto 2);
      when 4 => tmp := div5(a);
      when 5 => tmp := div3(a(10 downto 1));
      when 6 => tmp := div7(a);
      when 7 => tmp := a(10 downto 3);
    end case;
    return tmp;  
  end function divide;

end package body intl_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
