--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Interleaver PSS

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.utils.all;
use global_lib.global.all;

use work.bitfield.all;
use work.intl_pkg.all;
use work.errors.all;

entity pss is
  generic(mss_pipeline_depth: positive range 2 to integer'high); --* Number of pipeline stages in MSS, including input and output registers of RAMs
  port(clk    : in  std_ulogic;  -- master clock
       css2pss : in  css2pss_type; -- inputs from vciinterface (but parameter)
       param:  in  std_ulogic_vector(cmdsize * 8 - 1 downto 0); -- parameter input from vciinterface
       pss2css : out pss2css_type;  -- outputs to vciinterface
       pss2mss : out pss2mss_type;  -- outputs to memory subsystem
       mss2pss : in  mss2pss_type); -- inputs from memory subsystem
end entity pss;

architecture rtl of pss is

  --* parameters data structure; type of internal register in which processing parameters are stored when a processing is launched
  signal cmd    : bitfield_type;
  constant param0 : std_ulogic_vector(bitfield_width - 1 downto 0) := (others => '0');

  --* aliases for pss - mss interface signals
  alias enp is pss2mss.enp;
  alias addp is pss2mss.addp;
  alias p is mss2pss.p;
  alias enx is pss2mss.enx;
  alias addx is pss2mss.addx;
  alias x is mss2pss.x;
  alias eny is pss2mss.eny;
  alias rnwy is pss2mss.rnwy;
  alias addy is pss2mss.addy;
  alias yw is pss2mss.y;
  alias yr is mss2pss.y;
  alias ce is css2pss.ce;

  --* alias for return status
  alias status is pss2css.status;
  
  --* synchronous, active low, reset, combination of external and internal resets
  signal srstn: boolean;
  --* internal, synchronous, active low, reset, used to re-initialize the whole machine when a processing ends or when an error is encountered
  signal srstnl: boolean;
  --* start interleaving (one cycle duration)
  signal start: boolean;
  --* or-reduce of error flags
  signal err: std_ulogic;
  --* return status (internal siggnal)
  signal statusl: status_type;
  --* interleaving over (from last pipeline stage)
  signal done: boolean;

  signal mask : u_unsigned(7 downto 0);

  ----------------------------------
  -- Pipeline stages around P mem --
  ----------------------------------
  -- pipeline stage on front of P mem
  type stage1a_type is record
    -- enable access in P mem; travels all along the pipeline
    enp: boolean;
    cnt: u_unsigned(15 downto 0);  -- counter of permutation entries
    addp: u_unsigned(16 downto 0); -- address in P mem
    stop: boolean; -- stop flag
  end record;
  signal stage1a: stage1a_type;
  -- pipeline stages accross P mem
  signal stage1b: boolean_vector(1 to mss_pipeline_depth);
  -- pipeline stage at the output of P mem and on front of combinatorial decoder
  -- of permutation entries
  type stage1c_type is record
    enp: boolean; -- from previous stage
    p: u_unsigned(15 downto 0); -- permutation entry (from P mem)
  end record;
  signal stage1c: stage1c_type;

  ----------------------------------
  -- Pipeline stages around X mem --
  ----------------------------------
  -- operation codes for accumulator:
  -- nop:    idle state
  -- repeat: when repeat enabled, sample of a repeat sequence, but not the last
  -- valid:  when repeat disabled, regular sample; when repeat enabled
  --         non-repeated sample or last sample of a repeat sequence
  -- force_one:  when force enabled, force one special entry
  -- force_zero: when force enabled, force zero special entry
  -- skip:       when skip enabled, skip special entry
  type accu_op_type is (nop, force_one, force_zero, skip, repeat, valid);
  -- pipeline stage on front of X mem
  type stage2a_type is record
    enp: boolean; -- from previous stage
    op: accu_op_type; -- operation code for accumulator
    -- bit offset (used in packed bit input - pbi - mode)
    enx: boolean;
    addx: u_unsigned(19 downto 0); -- 20 bits bit-address in X mem
  end record;
  signal stage2a: stage2a_type;
  -- pipeline stages accross X mem
  type stage2b_type is record
    enp: boolean; -- from previous stage
    op: accu_op_type; -- operation code for accumulator
    -- bit offset (used in packed bit input - pbi - mode)
    bidx: natural range 0 to 7; -- bit index in X mem
  end record;
  type stage2b_vector is array(1 to mss_pipeline_depth) of stage2b_type;
  signal stage2b: stage2b_vector;
  -- pipeline stage at the output of X mem and on front of combinatorial
  -- accumulator of input samples (used in average repeat mode)
  type stage2c_type is record
    enp: boolean; -- from previous stage
    op: accu_op_type; -- from previous stage
    bidx: natural range 0 to 7; -- from previous stage
    x: u_unsigned(7 downto 0); -- read input byte from X mem
  end record;
  signal stage2c: stage2c_type;

  -----------------------------------------------------------------
  -- Pipeline stage at ouput of accumulator and input of divider --
  -----------------------------------------------------------------
  -- op code for divider:
  -- nop:    idle state
  -- skip:   see user documentation
  -- repeat: not last sample of a repeat sequence, when repeat
  --         mode enabled
  -- valid:  not repeated sample or last sample of a repeat sequence,
  --         when repeat mode enabled
  type div_op_type is (nop, skip, repeat, valid);
  type stage3_type is record
    enp: boolean; -- from previous stage
    cnt: natural range 0 to 7; -- counter in repeat averaged mode
    op: div_op_type; -- op code for divider
    accu: u_unsigned(10 downto 0); -- accumulator in repeat averaged mode
  end record;
  signal stage3: stage3_type;

  ---------------------------------------------------------------
  -- Pipeline stage at ouput of divider and input of assembler --
  ---------------------------------------------------------------
  subtype asm_op_type is div_op_type;
  type stage4a_type is record
    enp: boolean; -- from previous stage
    op: asm_op_type; -- op code for assembler stage
    val: u_unsigned(10 downto 0); -- value of incoming sample
    d3: u_unsigned(7 downto 0);   -- value of incoming sample, divided by 3
    d5: u_unsigned(7 downto 0);   -- value of incoming sample, divided by 5
    d6: u_unsigned(7 downto 0);   -- value of incoming sample, divided by 6
    d7: u_unsigned(7 downto 0);   -- value of incoming sample, divided by 7
    cnt: natural range 0 to 7;  -- counter in repeat averaged mode
  end record;
  signal stage4a: stage4a_type;
  type stage4b_type is record
    enp: boolean; -- from previous stage
    op: asm_op_type; -- op code for assembler stage
    val: u_unsigned(7 downto 0); -- value of incoming sample
  end record;
  signal stage4b: stage4b_type;

  ----------------------------------
  -- Pipeline stages around Y mem --
  ----------------------------------
  -- pipeline stage on front of Y mem
  type stage5a_type is record
    eny: boolean; -- enable access to Y mem
    rnwy: boolean; -- read-not-write in Y mem
    sk: boolean; -- skip flag
    -- previous value of a byte from Y mem (needed to handle skipped bits in
    -- packed bit output - pbo - mode)
    newy: u_unsigned(0 to 7); -- use to build the new value of a byte in pbo mode
    msky: u_unsigned(0 to 7); -- use to build the new value of a byte in pbo mode
    -- bit index of current single-bit sample in pbo mode (0 being the leftmost
    -- in the byte)
    bidx: natural range 0 to 7;
    addy: u_unsigned(16 downto 0); -- 17-bits byte address in Y mem
  end record;
  signal stage5a: stage5a_type;
  -- pipeline stages accross Y mem
  signal stage5b: boolean_vector(1 to mss_pipeline_depth);
  -- pipeline stage at the output of Y mem
  signal stage5c: u_unsigned(0 to 7);

begin

  pss2css.data <= (others => '0'); -- INTL does not support reading PSS internal registers from CSS
  pss2css.eirq <= (others => '0'); -- INTL has zero extended interrupts

  -- reset when external or internal reset
  srstn <= srstnl and css2pss.srstn /= '0';

  -- control process; drives internal reset, start, end-of-computation, error,
  -- exit status and internal copies of parameters
  ctrl: process(clk)
    
    variable  cmd_v  : bitfield_type;

    -- translate width-minus-one (widm1) parameter into an 8 bits mask
    function get_mask(val: std_ulogic_vector) return u_unsigned is
      variable tmp: natural range 0 to 7 := to_integer(u_unsigned(val));
      variable res: u_unsigned(7 downto 0) := (others => '0');
    begin
      case tmp is
        when 0 => res := x"01";
        when 1 => res := x"03";
        when 2 => res := x"07";
        when 3 => res := x"0f";
        when 4 => res := x"1f";
        when 5 => res := x"3f";
        when 6 => res := x"7f";
        when 7 => res := x"ff";
      end case;
      return res;
    end function get_mask;

  begin
    if rising_edge(clk) then
      if css2pss.srstn = '0' then -- external reset active
        cmd_v	  := bitfield_slice(param0);
        srstnl	  <= false;
        pss2css.eoc <= '0';
        pss2css.err <= '0';
        pss2css.status <= (others => '0');
        mask <= (others => '0');
        start <= false;
      elsif ce = '1' then
        if css2pss.exec = '1' then -- start processing request
          cmd_v	:= bitfield_slice(param);
          mask	<= get_mask(cmd_v.widm1);
          srstnl	<= true;
          start	<= true;
          pss2css.eoc <= '0';
          pss2css.err <= '0';
          pss2css.status <= (others => '0');
          mask <= (others => '0');
        else -- external reset inactive and no start request
          if start then
            mask <= get_mask(cmd.widm1);
          end if;
          start <= false;
          if err = '1' or done then -- if error encountered or end-of-computation
            pss2css.eoc <= '1';
            pss2css.err <= err;
            pss2css.status <= statusl;
            srstnl <= false;
          else -- still running or idle
            pss2css.eoc <= '0';
            pss2css.err <= '0';
            pss2css.status <= (others => '0');
          end if;
        end if;
        cmd	<= cmd_v;
      end if;
    end if;
  end process ctrl;

  -- Stage #1 around P mem
  r1: process(clk)
  begin
    if rising_edge(clk) then
      if not srstn then -- idle state
        stage1a <= (enp => false, cnt => (others => '0'),
                    addp => (others => '0'), stop => false);
        stage1b <= (others => false);
        stage1c <= (enp => false, p => (others => '0'));
      elsif ce = '1' then -- running
        -- compute address in P mem from base offset and counter
        stage1a.addp <= u_unsigned('0' & cmd.pof) + stage1a.cnt;
        if start then -- start request
          stage1a.enp <= true; -- enable read access in P mem
        elsif stage1a.stop then -- end of permutation
          stage1a.enp <= false;
        end if;
        -- if last counter value, end of permutation on next cycle
        stage1a.stop <= stage1a.cnt = u_unsigned(cmd.lenm1);
        -- if start request or currently running but not end on next cycle
        if (start or stage1a.enp) and (not stage1a.stop) then
          stage1a.cnt <= stage1a.cnt + 1; -- increment counter
        else
          stage1a.cnt <= (others => '0');
        end if;
        stage1b <= stage1a.enp & stage1b(1 to mss_pipeline_depth - 1); -- propagate enp accross P mem
        stage1c.enp <= stage1b(mss_pipeline_depth);
      end if;
      stage1c.p <= u_unsigned(p); -- store value read in P mem, if any
    end if;
  end process r1;
  -- enable read access in P mem
  enp <= '1' when stage1a.enp else '0';
  -- P mem address
  addp <= std_ulogic_vector(stage1a.addp(15 downto 0));

  -- Stage #2 around X mem
  r2: process(clk)
    -- offset of input sample from base bit offset
    variable tmp: u_unsigned(19 downto 0);
  begin
    if rising_edge(clk) then
      if not srstn then -- idle state
        stage2a <= (enp => false, op => nop, enx => false,
                    addx => (others => '0'));
        stage2b <= (others => (enp => false, op => nop, bidx => 0));
        stage2c <= (enp => false, op => nop, bidx => 0, x => (others => '0'));
      elsif ce = '1' then -- running
        stage2a.enx <= false; -- by default no access in X mem
        stage2a.enp <= stage1c.enp; -- propagate P entries indicator
        if cmd.re = '1' then -- repeat enabled
          tmp := "00" & stage1c.p(14 downto 0) & "000"; -- drop repeat flag
        else
          tmp := '0' & stage1c.p & "000";
        end if;
        if cmd.pbi = '1' then -- packed hard bit input samples (8 per byte in X mem)
          tmp := "000" & tmp(19 downto 3); -- bit offset instead of byte offset
        end if;
        if stage1c.enp then -- there is a permutation entry to process
          stage2a.addx <= tmp + (u_unsigned(cmd.iof) & u_unsigned(cmd.biof)); -- compute (bit) read address in X mem
          -- force enabled and force one special entry encountered
          if cmd.fe = '1' and stage1c.p = u_unsigned(cmd.fo) then
            stage2a.op <= force_one;
          -- force enabled and force zero special entry encountered
          elsif cmd.fe = '1' and stage1c.p = u_unsigned(cmd.fz) then
            stage2a.op <= force_zero;
          -- skip enabled and skip special entry encountered
          elsif cmd.se = '1' and stage1c.p = u_unsigned(cmd.sv) then
            stage2a.op <= skip;
          -- repeat enabled and repeat flag set
          elsif cmd.re = '1' and stage1c.p(15) = '1' then
            stage2a.op <= repeat;
            if cmd.arm = '1' then -- average repeat mode => must read input sample
              stage2a.enx <= true;
            end if;
          else -- regular entry => must read input sample
            stage2a.op <= valid;
            stage2a.enx <= true;
          end if;
        else -- no permutation entry to process
          stage2a.op <= nop;
        end if;
        -- propagate accross X mem
        stage2b(1) <= (enp => stage2a.enp, op => stage2a.op, bidx => to_integer(stage2a.addx(2 downto 0)));
        stage2b(2 to mss_pipeline_depth) <= stage2b(1 to mss_pipeline_depth - 1);
        stage2c.enp <= stage2b(mss_pipeline_depth).enp;
        stage2c.op <= stage2b(mss_pipeline_depth).op;
        stage2c.bidx <= stage2b(mss_pipeline_depth).bidx;
      end if;
      stage2c.x <= u_unsigned(x); -- store value read in X mem, if any
    end if;
  end process r2;
  -- enable read access in X mem
  enx <= '1' when stage2a.enx else '0';
  -- X mem (byte) address
  addx <= std_ulogic_vector(stage2a.addx(18 downto 3));

  -- Stage #3 between accumulator and divider
  r3: process(clk)
    -- incomming sample
    variable tmp: u_unsigned(0 to 7);
    -- accumulation result
    variable accu: u_unsigned(10 downto 0);
  begin
    if rising_edge(clk) then
      tmp := stage2c.x; -- byte read from X mem if any
      if not srstn then -- idle state
        stage3 <= (enp => false, cnt => 0, op => nop, accu => (others => '0'));
      elsif ce = '1' then -- running
        if stage2c.op = force_one then -- force one
          tmp := x"ff";
        elsif stage2c.op = force_zero then -- force zero
          tmp := x"00";
        elsif cmd.pbi = '1' then -- packed hard bit input samples (8 per byte in X mem)
          tmp(7) := tmp(stage2c.bidx); -- extract right bit from read byte
        end if;
        tmp := tmp and mask; -- force unused bits to '0'
        accu := stage3.accu + tmp; -- accumulation result
        stage3.enp <= stage2c.enp; -- propagate P entries flag
        case stage2c.op is -- depending on decoding of P entry
          when nop => stage3.op <= nop; -- nothing to do
          when skip => stage3.op <= skip; -- skip entry
          when force_one | force_zero => -- force entry
            stage3.op <= valid;
            stage3.accu <= "000" & tmp; -- store in accu
            stage3.cnt <= 0;            -- reset repeat counter
          when repeat =>
            stage3.op <= repeat; -- repeat enabled and flag set
                                 -- if average mode and not first nor last sample of repeat sequence
            if cmd.arm = '1' and stage3.op = repeat then
              stage3.accu <= accu; -- accumulate
              stage3.cnt <= stage3.cnt + 1;     -- increment repeat counter
                                                -- if average mode and first sample of repeat sequence
            elsif cmd.arm = '1' then
              stage3.accu <= "000" & tmp; -- store in accu
              stage3.cnt <= 0;            -- reset repeat counter
            end if;
          when valid => -- regular sample or last of repeat sequence
            stage3.op <= valid;
            -- if average mode and last sample of repeat sequence
            if cmd.arm = '1' and stage3.op = repeat then
              stage3.accu <= accu; -- accumulate
              stage3.cnt <= stage3.cnt + 1;     -- increment repeat counter
            else -- regular sample or last of repeat sequence in last mode
              stage3.accu <= "000" & tmp; -- store in accu
              stage3.cnt <= 0;            -- reset repeat counter
            end if;
        end case;
      end if;
    end if;
  end process r3;

  -- Stage #4 between divider and assembler
  r4: process(clk)
  begin
    if rising_edge(clk) then
      -- averaged sample; no need to mask because acumulator inputs are masked
      -- already and division is rounded towards zero
      if not srstn then -- idle state
        stage4a <= (enp => false, op => nop, val => (others => '0'), d3 => (others => '0'), d5 => (others => '0'), d6 => (others => '0'), d7 => (others => '0'),
                   cnt => 0);
        stage4b <= (enp => false, op => nop, val => (others => '0'));
      elsif ce = '1' then -- running
        stage4a.enp <= stage3.enp; -- propagate P entries flag
        stage4a.op  <= stage3.op;
        stage4a.val <= stage3.accu;
        stage4a.d3  <= div3(stage3.accu(9 downto 0));
        stage4a.d5  <= div5(stage3.accu);
        stage4a.d6  <= div3(stage3.accu(10 downto 1));
        stage4a.d7  <= div7(stage3.accu);
        stage4a.cnt <= stage3.cnt;
        stage4b.enp <= stage4a.enp;
        stage4b.op  <= stage4a.op;
        case stage4a.cnt is
          when 0 => stage4b.val <= stage4a.val(7 downto 0);
          when 1 => stage4b.val <= stage4a.val(8 downto 1);
          when 2 => stage4b.val <= stage4a.d3;
          when 3 => stage4b.val <= stage4a.val(9 downto 2);
          when 4 => stage4b.val <= stage4a.d5;
          when 5 => stage4b.val <= stage4a.d6;
          when 6 => stage4b.val <= stage4a.d7;
          when 7 => stage4b.val <= stage4a.val(10 downto 3);
        end case;
      end if;
    end if;
  end process r4;

  -- Stage #5 around Y mem
  r5: process(clk)
    -- bit index (used in pbo mode)
    variable tmp_bidx: natural range 0 to 8;
    -- byte address in Y mem
    variable tmp_addy: u_unsigned(16 downto 0);
    -- next output sample and mask of modified bits (used in pbo mode)
    variable tmp_newy, tmp_msky: u_unsigned(0 to 7);
  begin
    if rising_edge(clk) then
      if not srstn then -- idle state
        stage5a <= (eny => false, rnwy => true, sk => false, newy => (others => '0'),
                    msky => (others => '0'), bidx => 0, addy => (others => '0'));
        stage5b <= (others => false);
        stage5c <= (others => '0');
      elsif ce = '1' then -- running
        tmp_bidx := stage5a.bidx + 1; -- value of next bit-offset
        tmp_addy := stage5a.addy + 1; -- value of next byte-address
        stage5a.eny <= false; -- by default no access to Y mem
        stage5a.rnwy <= true; -- if access to Y mem, by default read
        stage5a.sk <= false; -- by default do not skip
        if start then -- if we start a new interleaving
          stage5a.addy <= '0' & u_unsigned(cmd.oof); -- initialize byte-address
        end if;
        if cmd.pbo = '0' then -- Not in pbo mode
          -- if we recieve or skip an output sample
          if stage4b.enp and (stage4b.op = valid or stage4b.op = skip) then
            if stage4b.op = valid then -- if we receive a new output sample
              -- write the output sample at current byte address
              stage5a.eny <= true;
              stage5a.rnwy <= false;
              stage5a.newy <= stage4b.val;
            else
              stage5a.eny <= false;
              stage5a.rnwy <= true;
              stage5a.sk <= true;
            end if;
          end if;
        else -- pbo mode (8 single-bit samples per output byte)
          tmp_newy := stage5a.newy; -- current output byte
          tmp_msky := stage5a.msky; -- current output mask
          if start then -- if we start a new interleaving
            stage5a.bidx <= to_integer(u_unsigned(cmd.boof)); -- initialize bit-offset
            stage5a.eny <= true; -- read first output byte
            stage5a.msky <= x"00"; -- initialize mask of modified bits
          end if;
          -- if we wrote an output byte on previous cycle and the interleaving is
          -- not complete
          if cmd.pbo = '1' and stage5a.eny and (not stage5a.rnwy) and stage4b.enp then
            -- read the next output byte
            stage5a.eny <= true;
          end if;
          -- if we recieve or skip an output sample or if interleaving is over
          -- but we did not write yet the last output sample
          if (stage4b.enp and (stage4b.op = valid or stage4b.op = skip)) or
             (not stage4b.enp and stage5a.bidx /= 0) then
            if stage4b.enp and stage4b.op = valid then -- if we receive a new output sample
              -- update new value of output byte (one bit change)
              tmp_newy(stage5a.bidx) := stage4b.val(0);
              -- update mask of modified bits
              tmp_msky(stage5a.bidx) := '1';
            end if;
            -- if last bit of byte or time has come for last write access; there
            -- is a four cycles latency to read the 'old' value of last byte
            -- from Y mem; so, if the last permutated sample falls in bits 0 to
            -- 3 (numbering from left to right), we must wait before we can
            -- write the last byte
            if tmp_bidx = 8 or (not stage4b.enp and stage5a.bidx >= 4) then
              -- write output byte at current byte address
              stage5a.eny <= true;
              stage5a.rnwy <= false;
              -- written value is a masked combination of old and new values
              tmp_newy := (tmp_newy and tmp_msky) or (stage5c and (not tmp_msky));
              tmp_msky := (others => '0'); -- re-initialize mask of modified bits
              stage5a.bidx <= 0; -- reset bit offset
            else -- not last bit of byte
              stage5a.bidx <= tmp_bidx; -- update bit offset
            end if;
            stage5a.newy <= tmp_newy;
            stage5a.msky <= tmp_msky;
          end if;
        end if;
        stage5b <= (stage5a.eny and stage5a.rnwy) & stage5b(1 to mss_pipeline_depth - 1);
        if stage5b(mss_pipeline_depth) then -- if read access in Y mem
          stage5c <= u_unsigned(yr); -- store read byte
        end if;
        if (stage5a.eny and (not stage5a.rnwy)) or stage5a.sk then -- if write access to Y mem or skip
          stage5a.addy <= tmp_addy; -- increment byte address
        end if;
      end if;
    end if;
  end process r5;
  -- access enable in Y mem
  eny <= '1' when stage5a.eny else '0';
  -- read-not-write in Y mem
  rnwy <= '1' when stage5a.rnwy else '0';
  -- byte address in Y mem
  addy <= std_ulogic_vector(stage5a.addy(15 downto 0));
  -- write data in Y mem
  yw <= std_ulogic_vector(stage5a.newy);
  -- interleaving is done when we write the last sample in Y mem and there is no
  -- next permutation entry to process
  done <= (not stage4b.enp) and stage5a.eny and (not stage5a.rnwy);

  -- error management
  e: process(srstn, start, stage1a, stage1c, stage2a, stage3,
             stage5a, cmd)
  begin
    statusl <= (others => '0'); -- default exit status 
    if (srstn and start) then -- start request, check parameters
      -- input samples are hard bits but they are coded on more than one bit
      if cmd.pbi = '1' and or_reduce(cmd.widm1) /= '0' then
        statusl(intl_nonbinarypackedinputs) <= '1';
-- pragma translate_off
        assert true
          report intl_nonbinarypackedinputs_message
          severity failure;
-- pragma translate_on
      end if;
      -- output samples are hard bits but they are coded on more than one bit
      if cmd.pbo = '1' and or_reduce(cmd.widm1) /= '0' then
        statusl(intl_nonbinarypackedoutputs) <= '1';
-- pragma translate_off
        assert true
          report intl_nonbinarypackedoutputs_message
          severity failure;
-- pragma translate_on
      end if;
      -- force enabled but force zero and force one special values are equal
      if cmd.fe = '1' and cmd.fo = cmd.fz then
        statusl(intl_forceoneequalforcezero) <= '1';
-- pragma translate_off
        assert true
          report intl_forceoneequalforcezero_message
          severity failure;
-- pragma translate_on
      end if;
      -- force and skip enabled but the skip special value is equal to one of
      -- the two force special values
      if cmd.se = '1' and (cmd.sv = cmd.fo or cmd.sv = cmd.fz) then
        statusl(intl_forceequalskip) <= '1';
-- pragma translate_off
        assert true
          report intl_forceequalskip_message
          severity failure;
-- pragma translate_on
      end if;
    elsif (srstn and (not start)) then -- running
      -- out-of-range read access in P mem
      if stage1a.enp and stage1a.addp(16) = '1' then
        statusl(intl_poutofrange) <= '1'; -- raise error
-- pragma translate_off
        assert true
          report intl_poutofrange_message
          severity failure;
-- pragma translate_on
      end if;
      -- out-of-range read access in X mem
      if stage2a.enx and stage2a.addx(19) = '1' then
        statusl(intl_xoutofrange) <= '1'; -- raise error
-- pragma translate_off
        assert true
          report intl_xoutofrange_message
          severity failure;
-- pragma translate_on
      end if;
      -- repat enabled and in a repeat series
      if (cmd.re = '1' and stage1c.p(15) = '1') or stage2a.op = repeat then
        -- force enabled and force special value encountered
        if cmd.fe = '1' and (stage1c.p = u_unsigned(cmd.fo) or stage1c.p = u_unsigned(cmd.fz)) then
          statusl(intl_forcewhilerepeating) <= '1';
-- pragma translate_off
          assert true
            report intl_forcewhilerepeating_message
            severity failure;
-- pragma translate_on
        end if;
        -- skip enabled and skip special value encountered
        if cmd.se = '1' and (stage1c.p = u_unsigned(cmd.sv)) then
          statusl(intl_skipwhilerepeating) <= '1';
-- pragma translate_off
          assert true
            report intl_skipwhilerepeating_message
            severity failure;
-- pragma translate_on
        end if;
      end if;
      -- end of permutation while repeating
      if (not stage1c.enp) and stage2a.op = repeat then
        statusl(intl_endoftablewhilerepeating) <= '1';
-- pragma translate_off
        assert true
          report intl_endoftablewhilerepeating_message
          severity failure;
-- pragma translate_on
      end if;
      -- if more than 8 repeated samples in repeat average mode
      if stage3.cnt = 8 then
        statusl(intl_repeatoverflow) <= '1'; -- raise error
-- pragma translate_off
        assert true
          report intl_repeatoverflow_message
          severity failure;
-- pragma translate_on
      end if;
      -- out-of-range access in Y mem
      if stage5a.eny and stage5a.addy(16) = '1' then
        statusl(intl_youtofrange) <= '1'; -- raise error
-- pragma translate_off
        assert true
          report intl_youtofrange_message
          severity failure;
-- pragma translate_on
      end if;
    end if;
  end process e;
  -- error flag is or of individual error flags
  err <= or_reduce(statusl(intl_num_error_codes - 1 downto 0));

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
