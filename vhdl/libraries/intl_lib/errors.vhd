--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Utility package for the interleaver
--*
--*  Defines error codes and messages

library ieee;
use ieee.std_logic_1164.all;

package errors is

  --------------
  -- error codes
  --------------
  -- total number of error codes
  constant intl_num_error_codes: natural                                                                                                                      := 11;
  -- soft bits represented on more than one bit cannot be packed.
  constant intl_nonbinarypackedinputs: natural                                                                                                                := 0;
  -- soft bits represented on more than one bit cannot be packed.
  constant intl_nonbinarypackedoutputs: natural                                                                                                               := 1;
  -- forcing is enabled and the two forcing values are equal.
  constant intl_forceoneequalforcezero: natural                                                                                                               := 2;
  -- forcing value equal to skipping value.
  constant intl_forceequalskip: natural                                                                                                                       := 3;
  -- repeating is enabled in average mode and a repeated series of more than 8 samples is detected.
  constant intl_repeatoverflow: natural                                                                                                                       := 4;
  -- forcing and repeating are enabled and a forcing value was found in a repeat series.
  constant intl_forcewhilerepeating: natural                                                                                                                  := 5;
  -- skipping and repeating are enabled and the skipping value was found in a repeat series.
  constant intl_skipwhilerepeating: natural                                                                                                                   := 6;
  -- repeating is enables and the permutation table ends in a repeat series.
  constant intl_endoftablewhilerepeating: natural                                                                                                             := 7;
  -- the combination of the start index in the input buffer and an offset in the permutation table points beyond the end of the input buffer.
  constant intl_xoutofrange: natural                                                                                                                          := 8;
  -- the combination of the start index in the permutation buffer and the permutation length points beyond the end of the permutation buffer.
  constant intl_poutofrange: natural                                                                                                                          := 9;
  -- the combination of the start index in the output buffer and the permutation table leads to writing an output sample beyond the end of the output buffer.
  constant intl_youtofrange: natural                                                                                                                          := 10;

-- pragma translate_off
  -----------------
  -- error messages
  -----------------
  constant intl_nonbinarypackedinputs_message:    string := "soft bits represented on more than one bit cannot be packed.";
  constant intl_nonbinarypackedoutputs_message:   string := "soft bits represented on more than one bit cannot be packed.";
  constant intl_forceoneequalforcezero_message:   string := "forcing is enabled and the two forcing values are equal.";
  constant intl_forceequalskip_message:           string := "forcing value equal to skipping value.";
  constant intl_repeatoverflow_message:           string := "repeating is enabled in average mode and a repeated series of more than 8 samples is detected.";
  constant intl_forcewhilerepeating_message:      string := "forcing and repeating are enabled and a forcing value was found in a repeat series.";
  constant intl_skipwhilerepeating_message:       string := "skipping and repeating are enabled and the skipping value was found in a repeat series.";
  constant intl_endoftablewhilerepeating_message: string := "repeating is enables and the permutation table ends in a repeat series.";
  constant intl_xoutofrange_message:              string := "combination of start index in input buffer and offset in permutation table points beyond end of input buffer.";
  constant intl_poutofrange_message:              string := "combination of start index in permutation buffer and permutation length points beyond end of permutation buffer.";
  constant intl_youtofrange_message:              string := "combination of start index in output buffer and permutation table leads to writing output sample beyond end of output buffer.";
  constant intl_invalidwidm1_message:             string := "invalid bit-width for soft bits.";
  constant intl_unknown_message:                  string := "unknown error case.";
-- pragma translate_on

end package errors;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
