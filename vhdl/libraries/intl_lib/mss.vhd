--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Interleaver's memory subsystem.
--*
--*  The Interleaver's memory subsystem is made of 21 physical RAMs:
--* - P0, P1, P2, P3 are four 16kx16 BRAMs
--* - X0, X1, ..., X7, Y0, Y1, ..., Y7 are 16 8kx8 BRAMs
--* - UCR is one 1kx32 BRAM
--* All RAMs are true dual ports RAMs. The mapping of these RAMs in the address space of their 4 clients (VCI, DMA, UC and PSS) is detailed in the documentation
--* of the General Purpose Interleaver

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.utils.all;
use global_lib.global.all;

use work.intl_pkg.all;

library memories_lib;
use memories_lib.ram_pkg.all;

entity mss is
  generic(n0: positive;  --* Number of input pipeline registers in MSS, including input registers of RAMs
          n1: positive); --* Number of output pipeline registers in MSS, including output registers of RAMs
  port(clk: in std_ulogic;
       css2mss: in css2mss_type;
       mss2css: out mss2css_type;
       pss2mss: in pss2mss_type;
       mss2pss: out mss2pss_type;
       mss2rams : out mss2rams_type;
       rams2mss : in rams2mss_type);
end entity mss;

architecture rtl of mss is

  type rams_type is (p, x, y, u);

  --* Possible sources of the X and Y MSS2PSS 8 bits data bus (A port).
  subtype pss_xy_mux_type is natural range 0 to 7;
  type pss_xy_mux_vector is array(1 to n0 + n1) of pss_xy_mux_type;
  signal pss_x_mux_d, pss_x_mux, pss_y_mux_d, pss_y_mux: pss_xy_mux_type;
  signal pss_x_mux_pipe, pss_y_mux_pipe: pss_xy_mux_vector;

  --* Possible sources of the P MSS2PSS 16 bits data bus (A port).
  subtype pss_p_mux_type is natural range 0 to 3;
  type pss_p_mux_vector is array(1 to n0 + n1) of pss_p_mux_type;
  signal pss_p_mux_d, pss_p_mux: pss_p_mux_type;
  signal pss_p_mux_pipe: pss_p_mux_vector;

  -- The source of the 64 bits word read by UC is always ports A and B of the UCR RAM. The byte enable is sufficient.
  subtype uc_mux_type is word64_be_type;
  type uc_mux_vector is array(1 to n0 + n1) of uc_mux_type;
  signal uc_mux_d, uc_mux: uc_mux_type;
  signal uc_mux_pipe: uc_mux_vector;

  --* The source of a 64 bits read by DMA (or VCI) can be:
  --* - the A and B ports of the UCR RAM
  --* - the B port of the 8 X or Y RAMs or the 4 P RAMs.
  type dma_vci_mux_type is record
    mux: rams_type;
    be:  word64_be_type;
  end record;
  type dma_vci_mux_vector is array(1 to n0 + n1) of dma_vci_mux_type;
  signal dma_mux_d, dma_mux, vci_mux_d, vci_mux: dma_vci_mux_type;
  signal dma_mux_pipe, vci_mux_pipe: dma_vci_mux_vector;

  --* Type used to mark the B port of a RAM as already used by DMA when arbitrating between DMA and VCI
  type is_used_type is array(rams_type) of boolean;

  alias dma2mss is css2mss.dma2mss;
  alias vci2mss is css2mss.vci2mss;
  alias uc2mss  is css2mss.uc2mss;
  alias mss2dma is mss2css.mss2dma;
  alias mss2vci is mss2css.mss2vci;
  alias mss2uc  is mss2css.mss2uc;

  signal mss2rams_pipe: mss2rams_vector(1 to n0);
  signal rams2mss_pipe: rams2mss_vector(1 to n1);

begin

  --* The arbiter combinatorial process computes the different requests each client sends to each RAM. It then enables only one client per RAM according the
  --* fixed priority: PSS first, then UC, DMA and VCI. The arbiter also prepares the commands of the multiplexors that select RAMs outputs to send to the clients.
  arbiter: process(css2mss, pss2mss)

    --* Keeps track of what RAM is already in use by DMA
    variable is_used: is_used_type;

    --* Intermediate address, used to compute absolute byte addresses in RAMs
    variable add: std_ulogic_vector(15 downto 0);

    variable x_in_v, y_in_v: xy_in_type;
    variable p_in_v: p_in_type;
    variable u_in_v: u_in_type;

  begin

    -------------------------------------------------------
    ------------------ INITIALIZATION ---------------------
    -------------------------------------------------------

    -- Assign the default request to all RAMs
    for i in 0 to 1 loop
      for j in 0 to 3 loop -- For all P RAMs
        p_in_v(i, j) := in_port_16kx16_default;
      end loop;
      for j in 0 to 7 loop -- For all X and Y RAMs
        x_in_v(i, j) := in_port_8kx8_default;
        y_in_v(i, j) := in_port_8kx8_default;
      end loop;
      u_in_v(i) := in_port_1kx32_default;
    end loop;

    is_used := (others => false); -- mark all RAMs as free

    mss2uc.gnt <= (others => '1'); -- UC requests are always served
    mss2uc.oor <= '0'; -- By default we expect UC not to access out of range memory locations

    mss2dma.gnt <= (others => '0'); -- By default, don't grant DMA's request (lower priority than UC) on UCR
    mss2dma.oor <= '0';             -- By default we expect DMA not to access out of range memory locations

    mss2vci.gnt <= (others => '0'); -- By default, don't grant VCI's request (lower priority than DMA)
    mss2vci.oor <= '0';             -- By default we expect VCI not to access out of range memory locations

    pss_p_mux_d <= 0; -- By default the source of MSS2PSS P channel is output of P0
    pss_x_mux_d <= 0; -- By default the source of MSS2PSS X channel is output of X0
    pss_y_mux_d <= 0; -- By default the source of MSS2PSS Y channel is output of Y0

    uc_mux_d <= (others => '0');
    dma_mux_d <= (mux => p, be => (others => '0')); -- By default the source of MSS2DMA RDATA is P0 to P3
    vci_mux_d <= (mux => p, be => (others => '0')); -- By default the source of MSS2VCI RDATA is P0 to P3

    -------------------------------------------------------
    ------------------ PSS REQUESTS ------------------------
    -------------------------------------------------------
    -- PSS requests (port A) are always served. PSS reads only in X and P. It reads and writes in Y. When reading in X it reads in one of the 8 RAMs
    -- only. The RAM is selected by the 3 LSBs of the PSS2MSS.ADDX address. Same when accessing Y. When reading in P it reads in one of the 4 RAMs only, the one
    -- selected by the 2 LSBs of the PSS2MSS.ADDP address.

    -- Requests on P
    for j in 0 to 3 loop
      if to_integer(u_unsigned(pss2mss.addp(1 downto 0))) = j and pss2mss.enp = '1' then
        p_in_v(0, j).en    := '1';
        p_in_v(0, j).add   := pss2mss.addp(15 downto 2);
        pss_p_mux_d         <= j;
      end if;
    end loop;

    -- Requests on X
    for j in 0 to 7 loop
      if to_integer(u_unsigned(pss2mss.addx(2 downto 0))) = j and pss2mss.enx = '1' then
        x_in_v(0, j).en    := '1';
        x_in_v(0, j).add   := pss2mss.addx(15 downto 3);
        pss_x_mux_d         <= j;
      end if;
    end loop;

    -- Requests on Y
    for j in 0 to 7 loop
      if to_integer(u_unsigned(pss2mss.addy(2 downto 0))) = j and pss2mss.eny = '1' then
        y_in_v(0, j).en    := '1';
        y_in_v(0, j).we    := (others => not pss2mss.rnwy);
        y_in_v(0, j).wdata := pss2mss.y;
        y_in_v(0, j).add   := pss2mss.addy(15 downto 3);
        pss_y_mux_d         <= j;
      end if;
    end loop;

    -------------------------------------------------------
    ------------------ UC REQUESTS ------------------------
    -------------------------------------------------------
    -- UC requests are always served.

    add := uc2mss.add;
    if uc2mss.en = '1' then
      mss2css.mss2uc.gnt <= (others => '1'); -- grant the access
      is_used(u)      := true;
      -- Port A
      u_in_v(0).en    := '1';
      u_in_v(0).we    := band((not uc2mss.rnw), uc2mss.be(7 downto 4));
      u_in_v(0).add   := uc2mss.add(11 downto 3) & '0';
      u_in_v(0).wdata := uc2mss.wdata(63 downto 32);
      -- Port B
      u_in_v(1).en    := '1';
      u_in_v(1).we    := band((not uc2mss.rnw), uc2mss.be(3 downto 0));
      u_in_v(1).add   := uc2mss.add(11 downto 3) & '1';
      u_in_v(1).wdata := uc2mss.wdata(31 downto 0);
      -- Read response
      if uc2mss.rnw = '1' then
        uc_mux_d <= uc2mss.be;
      end if;
    end if;

    -- If UC access is out of range stop the simulation and set the "oor" field of mss2uc.
    if or_reduce(add(15 downto 12)) = '1' then
      mss2uc.oor <= '1';
-- pragma translate_off
      assert uc2mss.en = '0'
        report "UC: out of range memory access (" & integer'image(to_integer(u_unsigned(uc2mss.add))) & " >= 0x1000"
        severity error;
-- pragma translate_on
    end if;

    -------------------------------------------------------
    ------------------ DMA REQUESTS -----------------------
    -------------------------------------------------------
    -- DMA requests to X, Y and P are always served (port B). Out of range accesses raise an error during simulation and set the OOR field of MSS2DMA. DMA
    -- provides individual, active high, byte enables. MSS returns individual, active high, byte grants to indicate which byte has been successfully read or
    -- written. If some requested bytes are not granted DMA can request them again on the next clock cyle. DMA requests to UCR use both ports; if UC accesses
    -- UCR, DMA requests are not granted.

    add := dma2mss.add(15 downto 0);
    if dma2mss.en = '1' then
      -- Requests on P
      if add(15 downto 14) = "00" then
        is_used(p)     := true;
        dma_mux_d.mux  <= p;
        mss2dma.gnt    <= (others => '1');
        if dma2mss.rnw = '1' then
          dma_mux_d.be <= dma2mss.be;
        end if;
        for j in 0 to 3 loop
          p_in_v(1, j).en    := '1';
          p_in_v(1, j).wdata := dma2mss.wdata(63 - 16 * j downto 48 - 16 * j);
          p_in_v(1, j).add   := add(13 downto 0);
          p_in_v(1, j).we    := (others => not dma2mss.rnw);
          p_in_v(1, j).we    := p_in_v(1, j).we and dma2mss.be(7 - 2 * j downto 6 - 2 * j);
        end loop;
      end if;

      -- Requests on X
      if add(15 downto 13) = "010" then
        is_used(x)    := true;
        dma_mux_d.mux <= x;
        mss2dma.gnt   <= (others => '1');
        if dma2mss.rnw = '1' then
          dma_mux_d.be <= dma2mss.be;
        end if;
        for j in 0 to 7 loop
          x_in_v(1, j).en    := '1';
          x_in_v(1, j).wdata := dma2mss.wdata(63 - 8 * j downto 56 - 8 * j);
          x_in_v(1, j).add   := add(12 downto 0);
          x_in_v(1, j).we(0) := not dma2mss.rnw;
          x_in_v(1, j).we(0) := x_in_v(1, j).we(0) and dma2mss.be(7 - j);
        end loop;
      end if;

      -- Requests on Y
      if add(15 downto 13) = "011" then
        is_used(y)    := true;
        dma_mux_d.mux <= y;
        mss2dma.gnt   <= (others => '1');
        if dma2mss.rnw = '1' then
          dma_mux_d.be <= dma2mss.be;
        end if;
        for j in 0 to 7 loop
          y_in_v(1, j).en    := '1';
          y_in_v(1, j).wdata := dma2mss.wdata(63 - 8 * j downto 56 - 8 * j);
          y_in_v(1, j).add   := add(12 downto 0);
          y_in_v(1, j).we(0) := not dma2mss.rnw;
          y_in_v(1, j).we(0) := y_in_v(1, j).we(0) and dma2mss.be(7 - j);
        end loop;
      end if;

      -- Requests on U
      if add(15) = '1' and not is_used(u) then
        is_used(u)     := true;
        dma_mux_d.mux  <= u;
        mss2dma.gnt    <= (others => '1');
        if dma2mss.rnw = '1' then
          dma_mux_d.be <= dma2mss.be;
        end if;
        u_in_v(0).en            := '1';
        u_in_v(0).wdata         := dma2mss.wdata(63 downto 32);
        u_in_v(0).add           := add(8 downto 0) & '0';
        u_in_v(0).we            := (others => not dma2mss.rnw);
        u_in_v(0).we            := u_in_v(0).we and dma2mss.be(7 downto 4);
        u_in_v(1).en            := '1';
        u_in_v(1).wdata         := dma2mss.wdata(31 downto 0);
        u_in_v(1).add           := add(8 downto 0) & '1';
        u_in_v(1).we            := (others => not dma2mss.rnw);
        u_in_v(1).we            := u_in_v(1).we and dma2mss.be(3 downto 0);
      end if;

      -- If DMA access is out of range stop the simulation and set the "oor" field of mss2dma.
      if add(15) = '1' and add(14 downto 9) /= "000000" then
-- pragma translate_off
        assert false
        report "DMA: out of range memory access (" & integer'image(to_integer(u_unsigned(dma2mss.add))) & " >= 0x8200"
        severity error;
-- pragma translate_on
        mss2dma.oor <= '1';
      end if;
    end if;

    -------------------------------------------------------
    ------------------ VCI REQUESTS -----------------------
    -------------------------------------------------------
    -- VCI requests (lower priority than DMA) are not always served. They can be prevented by accesses to RAMs by DMA. Apart from this, VCI and DMA are exactly
    -- the same.

    add := vci2mss.add(15 downto 0);
    if vci2mss.en = '1' then
      -- Requests on P
      if add(15 downto 14) = "00" and not is_used(p) then
        vci_mux_d.mux  <= p;
        mss2vci.gnt    <= (others => '1');
        if vci2mss.rnw = '1' then
          vci_mux_d.be <= vci2mss.be;
        end if;
        for j in 0 to 3 loop
          p_in_v(1, j).en    := '1';
          p_in_v(1, j).wdata := vci2mss.wdata(63 - 16 * j downto 48 - 16 * j);
          p_in_v(1, j).add   := add(13 downto 0);
          p_in_v(1, j).we    := (others => not vci2mss.rnw);
          p_in_v(1, j).we    := p_in_v(1, j).we and vci2mss.be(7 - 2 * j downto 6 - 2 * j);
        end loop;
      end if;

      -- Requests on X
      if add(15 downto 13) = "010" and not is_used(x) then
        vci_mux_d.mux <= x;
        mss2vci.gnt   <= (others => '1');
        if vci2mss.rnw = '1' then
          vci_mux_d.be <= vci2mss.be;
        end if;
        for j in 0 to 7 loop
          x_in_v(1, j).en    := '1';
          x_in_v(1, j).wdata := vci2mss.wdata(63 - 8 * j downto 56 - 8 * j);
          x_in_v(1, j).add   := add(12 downto 0);
          x_in_v(1, j).we(0) := not vci2mss.rnw;
          x_in_v(1, j).we(0) := x_in_v(1, j).we(0) and vci2mss.be(7 - j);
        end loop;
      end if;

      -- Requests on Y
      if add(15 downto 13) = "011" and not is_used(y) then
        vci_mux_d.mux <= y;
        mss2vci.gnt   <= (others => '1');
        if vci2mss.rnw = '1' then
          vci_mux_d.be <= vci2mss.be;
        end if;
        for j in 0 to 7 loop
          y_in_v(1, j).en    := '1';
          y_in_v(1, j).wdata := vci2mss.wdata(63 - 8 * j downto 56 - 8 * j);
          y_in_v(1, j).add   := add(12 downto 0);
          y_in_v(1, j).we(0) := not vci2mss.rnw;
          y_in_v(1, j).we(0) := y_in_v(1, j).we(0) and vci2mss.be(7 - j);
        end loop;
      end if;

      -- Requests on U
      if add(15) = '1' and not is_used(u) then
        vci_mux_d.mux  <= u;
        mss2vci.gnt    <= (others => '1');
        if vci2mss.rnw = '1' then
          vci_mux_d.be <= vci2mss.be;
        end if;
        u_in_v(0).en            := '1';
        u_in_v(0).wdata         := vci2mss.wdata(63 downto 32);
        u_in_v(0).add           := add(8 downto 0) & '0';
        u_in_v(0).we            := (others => not vci2mss.rnw);
        u_in_v(0).we            := u_in_v(0).we and vci2mss.be(7 downto 4);
        u_in_v(1).en            := '1';
        u_in_v(1).wdata         := vci2mss.wdata(31 downto 0);
        u_in_v(1).add           := add(8 downto 0) & '1';
        u_in_v(1).we            := (others => not vci2mss.rnw);
        u_in_v(1).we            := u_in_v(1).we and vci2mss.be(3 downto 0);
      end if;

      -- If VCI access is out of range stop the simulation and set the "oor" field of mss2vci.
      if add(15) = '1' and add(14 downto 9) /= "000000" then
-- pragma translate_off
        assert false
        report "VCI: out of range memory access (" & integer'image(to_integer(u_unsigned(vci2mss.add))) & " >= 0x8200"
        severity error;
-- pragma translate_on
        mss2vci.oor <= '1';
      end if;
    end if;

    mss2rams_pipe(1) <= (x => x_in_v, y => y_in_v, p => p_in_v, u => u_in_v);

  end process arbiter;

  gn0: if n0 > 1 generate
    process(clk)
    begin
      if rising_edge(clk) then
        mss2rams_pipe(2 to n0) <= mss2rams_pipe(1 to n0 - 1);
      end if;
    end process;
  end generate gn0;
  rams2mss_pipe(1) <= rams2mss;
  gn1: if n1 > 1 generate
    process(clk)
    begin
      if rising_edge(clk) then
        rams2mss_pipe(2 to n1) <= rams2mss_pipe(1 to n1 - 1);
      end if;
    end process;
  end generate gn1;

  -- The regs process implements the sequential part: registers to store the commands of the output multiplexers that select the "rdata" fields of UC, DMA and
  -- VCI and the read channels of PSS.
  regs: process(clk) -- 26 dff (or more if one-hot encoded)
  begin
    if rising_edge(clk) then
      pss_x_mux_pipe  <= pss_x_mux_d & pss_x_mux_pipe(1 to n0 + n1 - 1);
      pss_y_mux_pipe  <= pss_y_mux_d & pss_y_mux_pipe(1 to n0 + n1 - 1);
      pss_p_mux_pipe  <= pss_p_mux_d & pss_p_mux_pipe(1 to n0 + n1 - 1);
      uc_mux_pipe  <= uc_mux_d & uc_mux_pipe(1 to n0 + n1 - 1);
      dma_mux_pipe <= dma_mux_d & dma_mux_pipe(1 to n0 + n1 - 1);
      vci_mux_pipe <= vci_mux_d & vci_mux_pipe(1 to n0 + n1 - 1);
    end if;
  end process regs;

  mss2rams <= mss2rams_pipe(n0);

  pss_x_mux  <= pss_x_mux_pipe(n0 + n1);
  pss_y_mux  <= pss_y_mux_pipe(n0 + n1);
  pss_p_mux  <= pss_p_mux_pipe(n0 + n1);
  uc_mux  <= uc_mux_pipe(n0 + n1);
  dma_mux <= dma_mux_pipe(n0 + n1);
  vci_mux <= vci_mux_pipe(n0 + n1);

  -- The muxes process implements the multiplexing of RAM outputs
  muxes: process(pss_x_mux, pss_y_mux, pss_p_mux, uc_mux, dma_mux, vci_mux, rams2mss_pipe(n1))
  begin
    mss2pss.x <= rams2mss_pipe(n1).x(0, pss_x_mux);
    mss2pss.y <= rams2mss_pipe(n1).y(0, pss_y_mux);
    mss2pss.p <= rams2mss_pipe(n1).p(0, pss_p_mux);
    mss2uc.rdata <= rams2mss_pipe(n1).u(0) & rams2mss_pipe(n1).u(1);
    mss2uc.be    <= uc_mux;
    case dma_mux.mux is
      when p =>
        for j in 0 to 3 loop
          mss2dma.rdata(63 - 16 * j downto 48 - 16 * j) <= rams2mss_pipe(n1).p(1, j);
        end loop;
      when x =>
        for j in 0 to 7 loop
          mss2dma.rdata(63 - 8 * j downto 56 - 8 * j) <= rams2mss_pipe(n1).x(1, j);
        end loop;
      when y =>
        for j in 0 to 7 loop
          mss2dma.rdata(63 - 8 * j downto 56 - 8 * j) <= rams2mss_pipe(n1).y(1, j);
        end loop;
      when u =>
        mss2dma.rdata <= rams2mss_pipe(n1).u(0) & rams2mss_pipe(n1).u(1);
    end case;
    mss2dma.be <= dma_mux.be;
    case vci_mux.mux is
      when p =>
        for j in 0 to 3 loop
          mss2vci.rdata(63 - 16 * j downto 48 - 16 * j) <= rams2mss_pipe(n1).p(1, j);
        end loop;
      when x =>
        for j in 0 to 7 loop
          mss2vci.rdata(63 - 8 * j downto 56 - 8 * j) <= rams2mss_pipe(n1).x(1, j);
        end loop;
      when y =>
        for j in 0 to 7 loop
          mss2vci.rdata(63 - 8 * j downto 56 - 8 * j) <= rams2mss_pipe(n1).y(1, j);
        end loop;
      when u =>
        mss2vci.rdata <= rams2mss_pipe(n1).u(0) & rams2mss_pipe(n1).u(1);
    end case;
    mss2vci.be <= vci_mux.be;
  end process muxes;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
