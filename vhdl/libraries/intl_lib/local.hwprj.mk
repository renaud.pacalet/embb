#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

gh-IGNORE	+= intl_lib.rams intl_lib.intl intl_lib.pss_mss intl_lib.pss_sim intl_lib.mss_sim intl_lib.intl_no_rams
gh-IGNORE	+= intl_lib.intl intl_lib.intl_sim intl_lib.top

intl_lib.intl_sim: \
	global_lib.global \
	global_lib.hst_emulator \
	intl_lib.bitfield_pkg \
	intl_lib.intl

intl_lib.intl_no_rams: \
	global_lib.global \
	memories_lib.ram_pkg \
	css_lib.css \
	intl_lib.bitfield_pkg \
	intl_lib.intl_pkg \
	intl_lib.mss \
	intl_lib.pss

intl_lib.intl: \
	global_lib.global \
	intl_lib.rams \
	intl_lib.intl_pkg \
	intl_lib.intl_no_rams

intl_lib.intl_pkg: \
	global_lib.global \
	memories_lib.ram_pkg \
	intl_lib.bitfield_pkg

intl_lib.pss: \
	global_lib.global \
	global_lib.utils \
	intl_lib.bitfield_pkg \
	intl_lib.intl_pkg \
	intl_lib.errors

intl_lib.pss_mss: \
	global_lib.global \
	global_lib.utils \
	intl_lib.bitfield_pkg \
	intl_lib.intl_pkg \
	intl_lib.mss \
	intl_lib.rams \
	intl_lib.pss

intl_lib.pss_sim: \
	global_lib.global \
	global_lib.css_emulator \
	memories_lib.ram_pkg \
	intl_lib.bitfield_pkg \
	intl_lib.intl_pkg \
	intl_lib.pss \
	intl_lib.mss \
	intl_lib.rams

intl_lib.mss: \
	global_lib.global \
	global_lib.utils \
	memories_lib.ram_pkg \
	intl_lib.intl_pkg

intl_lib.intl_sim: \
	global_lib.global \
	global_lib.utils \
	intl_lib.intl_pkg \
	intl_lib.intl

intl_lib.mss_sim: \
	random_lib.rnd \
	global_lib.global \
	memories_lib.ram_pkg \
	memories_lib.ram_sim_pkg \
	intl_lib.intl_pkg \
	intl_lib.mss \
	intl_lib.rams

intl_lib.rams: \
	memories_lib.ram_pkg \
	memories_lib.tdpram \
	intl_lib.intl_pkg

intl_lib.top: \
	global_lib.global \
	global_lib.utils \
	intl_lib.intl

# Default input and output pipeline depths of MSS.
INTL_LIB_N0	?= 1
INTL_LIB_N1	?= 1

# Simulations

intl_lib_csrc		:= $(hwprj_db_path.intl_lib)/C
# Name of command file for PSS simulation
intl_lib_c_cmdfile	:= $(intl_lib_csrc)/c_intl.cmd
# Name of command file for full INTL simulation
intl_lib_cmdfile	:= $(intl_lib_csrc)/intl.cmd
# Number of operations to simulate
INTL_LIB_CMDNUM		:= 100
# Name of command file generator
INTL_LIB_CMDGEN		:= mkcmdfile
intl_lib_cmdgen		:= $(intl_lib_csrc)/$(INTL_LIB_CMDGEN)
# Options for command file generation
INTL_LIB_CMDGENFLAGS	:= --tests=$(INTL_LIB_CMDNUM)
# Target technology
INTL_LIB_TARGET		:= $(VIRTEX5)
# Generic parameters for full INTL simulation
INTL_LIB_DEBUG		:= true
INTL_LIB_VERBOSE	:= false

ms-sim.intl_lib.%: MSSIMFLAGS += -c -do 'run -all; quit' -t ps
ms-sim.intl_lib.intl_sim: $(intl_lib_cmdfile)
ms-sim.intl_lib.intl_sim: MSSIMFLAGS += -Gcmdfile="$(intl_lib_cmdfile)" -Gdebug=$(INTL_LIB_DEBUG) -Gverbose=$(INTL_LIB_VERBOSE)
ms-sim.intl_lib.pss_sim: $(intl_lib_c_cmdfile)
ms-sim.intl_lib.pss_sim: MSSIMFLAGS += -Gcmdfile="$(intl_lib_c_cmdfile)"
ms-sim.intl_lib.mss_sim: MSSIMFLAGS += -Gn=100000 -Gdebug=false
$(addprefix ms-sim.intl_lib.,intl_sim pss_sim mss_sim): MSSIMFLAGS += -Gn0=$(INTL_LIB_N0) -Gn1=$(INTL_LIB_N1)
ms-sim.intl_lib: $(addprefix ms-sim.intl_lib.,intl_sim pss_sim mss_sim)
ms-sim: ms-sim.intl_lib

.PHONY: $(intl_lib_cmdgen)

$(intl_lib_c_cmdfile): $(intl_lib_cmdgen)
	$< --core $(intl_lib_cmdgenflags) $@

$(intl_lib_cmdfile): $(intl_lib_cmdgen)
	$< $(intl_lib_cmdgenflags) $@

$(intl_lib_cmdgen):
	$(MAKE) -C $(dir $@) $(notdir $@)

# Synthesis

$(addprefix pr-syn.intl_lib.,intl pss pss_mss mss top): $(hwprj_db_path.intl_lib)/bitfield_pkg.vhd
pr-syn.intl_lib: $(addprefix pr-syn.intl_lib.,intl pss pss_mss mss top)
pr-syn: pr-syn.intl_lib

$(addprefix rc-syn.intl_lib.,pss intl_no_rams): $(hwprj_db_path.intl_lib)/bitfield_pkg.vhd
rc-syn.intl_lib: $(addprefix rc-syn.intl_lib.,intl pss pss_mss mss top)
rc-syn: rc-syn.intl_lib

hw-clean: intl_lib_clean

intl_lib_clean:
	$(MAKE) -C $(intl_lib_csrc) clean
	rm -rf $(intl_lib_cmdfile) $(intl_lib_c_cmdfile)

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
