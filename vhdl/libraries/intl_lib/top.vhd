--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

entity top is
  port(clk:            in  std_ulogic;  -- master clock
       srstn:          in  std_ulogic;  -- master reset
       ce:             in  std_ulogic;  -- chip enable
       cmdval_in:      in  std_ulogic;
       rspack_in:      in  std_ulogic;
       tin:            in  std_ulogic_vector(7 downto 0);
       cmdack_in:      in  std_ulogic;
       rspval_in:      in  std_ulogic;
       iin:            in  std_ulogic_vector(7 downto 0);
       dso:            in  std_ulogic;  -- data strobe out
       hirq:           in  std_ulogic;  -- host input irq,
       pirq:           out std_ulogic;  -- PSS output irq,
       dirq:           out std_ulogic;  -- DMA output irq,
       uirq:           out std_ulogic;  -- UC output irq,
       cmdack_out:     out std_ulogic;
       rspval_out:     out std_ulogic;
       tout:           out std_ulogic_vector(7 downto 0);
       cmdval_out:     out std_ulogic;
       rspack_out:     out std_ulogic;
       iout:           out std_ulogic_vector(7 downto 0));
end entity top;

architecture rtl of top is

  signal tvci_in:  vci_i2t_type;  -- Target AVCI input
  signal tvci_out: vci_t2i_type; -- Target AVCI output
  signal ivci_in:  vci_t2i_type;  -- Initiator AVCI input
  signal ivci_out: vci_i2t_type; -- Initiator AVCI output

begin

  intl: entity work.intl(rtl)
  port map(clk      => clk,
           srstn    => srstn,
           ce       => ce,
           hirq     => hirq,
           pirq     => pirq,
           dirq     => dirq,
           uirq     => uirq,
           tvci_in  => tvci_in,
           tvci_out => tvci_out,
           ivci_in  => ivci_in,
           ivci_out => ivci_out);

  process(clk)
    variable v_req_in: std_ulogic_vector(vci_request_length - 1 downto 0);
    variable v_req_out: std_ulogic_vector(vci_request_length - 1 downto 0);
    variable v_rsp_in: std_ulogic_vector(vci_response_length - 1 downto 0);
    variable v_rsp_out: std_ulogic_vector(vci_response_length - 1 downto 0);
  begin
    if rising_edge(clk) then

      if cmdval_in = '1' then
        tvci_in.req <= to_vcirequesttype(v_req_in);
      else
        tvci_in.req <= vci_request_none;
      end if;
      tvci_in.rspack <= rspack_in;
      v_req_in := shift_left(v_req_in, 8);
      v_req_in(7 downto 0) := tin;

      cmdack_out <= tvci_out.cmdack;
      tout <= v_rsp_out(7 downto 0);
      if tvci_out.rsp.rspval = '1' then
        v_rsp_out := to_stdulogicvector(tvci_out.rsp);
      else
        v_rsp_out := shift_right(v_rsp_out, 8);
      end if;

      if rspval_in = '1' then
        ivci_in.rsp <= to_vciresponsetype(v_rsp_in);
      else
        ivci_in.rsp <= vci_response_none;
      end if;
      ivci_in.cmdack <= cmdack_in;
      v_rsp_in := shift_left(v_rsp_in, 8);
      v_rsp_in(7 downto 0) := iin;

      rspack_out <= ivci_out.rspack;
      iout <= v_req_out(7 downto 0);
      if ivci_out.req.cmdval = '1' then
        v_req_out := to_stdulogicvector(ivci_out.req);
      else
        v_req_out := shift_right(v_req_out, 8);
      end if;

    end if;
  end process;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
