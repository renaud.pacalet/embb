--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Interleaver's memory subsystem.
--*
--*  The interleaver's memory subsystem simulation environment. Each client (PSS, UC, DMA and VCI) randomly sends requests to the memory sub-system
--* (but avoiding write conflicts). Write requests are recorded and read requests are checked against this record.
--* The generic parameters are the following:
--* - debug: when true, prints requests as soon as they are taken into account by the memory subsystem. Defaults to false.
--* - xilinx: when true, selects the Xilinx simulation model for the block RAMs, else the custom (and faster) simulation model. Defaults to true.
--* - n: number of simulated requests. Defaults to 100000.

entity mss_sim is
  generic(
    debug: boolean := false;
    n0:    positive range 1 to 1 := 1; --* Number of input pipeline registers in MSS, including input registers of RAMs
    n1:    positive range 1 to 1 := 1; --* Number of output pipeline registers in MSS, including output registers of RAMs
    n:     positive := 100000);
end entity mss_sim;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;

library global_lib;
use global_lib.global.all;

library random_lib;
use random_lib.rnd.all;

library memories_lib;
use memories_lib.ram_pkg.all;
use memories_lib.ram_sim_pkg.all;

use work.intl_pkg.all;

use std.textio.all;

architecture sim of mss_sim is

  -- IOs of the memory subsystem
  signal clk: std_ulogic;
  signal css2mss: css2mss_type;
  signal mss2css: mss2css_type;
  alias vci2mss is css2mss.vci2mss;
  alias mss2vci is mss2css.mss2vci;
  alias dma2mss is css2mss.dma2mss;
  alias mss2dma is mss2css.mss2dma;
  alias uc2mss  is css2mss.uc2mss;
  alias mss2uc  is mss2css.mss2uc;
  signal pss2mss: pss2mss_type;
  signal mss2pss: mss2pss_type;
  signal mss2rams: mss2rams_type;
  signal rams2mss: rams2mss_type;

-- Local copies of DMA and VCI request signals. Used for write conflict avoidance. These 2 clients issue totally random requests. They are then filtered by the
-- "conflict" process that copies them on the real request interfaces (dma2mss, vci2mss). In case a write conflict is detected (two clients request a write
-- operation on overlapping memory addresses), the client with the lowest priority (PSS > DMA > VCI) has its write request changed into a read request.
  signal dma2mssl: dma2mss_type;
  signal vci2mssl: dma2mss_type;

  signal end_of_simulation: boolean := false;

begin

  -- Instanciation of the memory subsystem.
  mss: entity work.mss(rtl)
  generic map(
    n0 => n0,
    n1 => n1)
  port map(
    clk => clk,
    css2mss => css2mss,
    mss2css => mss2css,
    pss2mss => pss2mss,
    mss2pss => mss2pss,
    mss2rams => mss2rams,
    rams2mss => rams2mss);
  
  i_rams: entity work.rams(rtl)
    port map(clk => clk,
             mss2rams => mss2rams,
             rams2mss => rams2mss);

  -- Clock generator.
  process
  begin
    clk <= '0';
    wait for 1 ns;
    clk <= '1';
    wait for 1 ns;
    if end_of_simulation then
      wait;
    end if;
  end process;

  -- This process generates n requests from PSS to MSS through the P channel
  -- (read requests only). It also prints the percentage of simulation
  -- completion. After the last request it stops requesting, waits until all the
  -- other clients (including PSS through the other channels) also stop
  -- requesting and then, it ends the simulation.
  pss_perm_pr: process
    variable l: line;
  begin
    pss2mss.enp <= '0';
    pss2mss.addp <= (others => '0');
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    for i in 1 to 100 loop
      for j in 1 to n / 100 loop
        if boolean_rnd then
          pss2mss.enp <= '1';
          pss2mss.addp <= std_ulogic_vector_rnd(16);
        else
          pss2mss.enp <= '0';
        end if;
        wait until rising_edge(clk);
      end loop;
      write(l, integer'image(i) & "%");
      writeline(output, l);
    end loop;
    pss2mss.enp <= '0';
    pss2mss.addp <= (others => '0');
    wait until rising_edge(clk) and pss2mss.enp = '0' and pss2mss.enx = '0' and
      uc2mss.en = '0' and dma2mss.en = '0' and vci2mss.en = '0';
    end_of_simulation <= true;
    report "Regression test passed";
    wait;
  end process pss_perm_pr;

  -- This process generates n requests from PSS to MSS through the X channel
  -- (read requests only). After the last request it stops requesting and it
  -- waits forever.
  pss_di_pr: process
  begin
    pss2mss.enx <= '0';
    pss2mss.addx <= (others => '0');
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    for i in 1 to n loop
      if boolean_rnd then
        pss2mss.enx <= '1';
        pss2mss.addx <= std_ulogic_vector_rnd(16);
      else
        pss2mss.enx <= '0';
      end if;
      wait until rising_edge(clk);
    end loop;
    pss2mss.enx <= '0';
    pss2mss.addx <= (others => '0');
    wait;
  end process pss_di_pr;

  -- This process generates n requests from PSS to MSS through the Y channel
  -- (read or write requests). After the last request it stops requesting and it
  -- waits forever.
  pss_do_pr: process
  begin
    pss2mss.eny <= '0';
    pss2mss.rnwy <= '1';
    pss2mss.addy <= (others => '0');
    pss2mss.y <= (others => '0');
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    for i in 1 to n loop
      if boolean_rnd then
        pss2mss.eny <= '1';
        pss2mss.rnwy <= std_ulogic_rnd;
        pss2mss.addy <= std_ulogic_vector_rnd(16);
        pss2mss.y <= std_ulogic_vector_rnd(8);
      else
        pss2mss.eny <= '0';
      end if;
      wait until rising_edge(clk);
    end loop;
    pss2mss.eny <= '0';
    pss2mss.rnwy <= '1';
    pss2mss.addy <= (others => '0');
    pss2mss.y <= (others => '0');
    wait;
  end process pss_do_pr;

  -- This process generates n requests from UC to MSS. After the last request it stops requesting and waits forever.
  uc_pr: process
  begin
    uc2mss <= (en => '0', rnw => '1', be => (others => '0'), add => (others => '0'), wdata => (others => '0'));
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    for i in 1 to n loop
      if boolean_rnd then
        uc2mss <= (en => '1', rnw => std_ulogic_rnd, be => std_ulogic_vector_rnd(8), add => std_ulogic_vector(to_unsigned(int_rnd(0, 16#03ff#), 16)),
                   wdata => std_ulogic_vector_rnd(64));
      else
        uc2mss.en <= '0';
      end if;
      wait until rising_edge(clk);
    end loop;
    uc2mss <= (en => '0', rnw => '1', be => (others => '0'), add => (others => '0'), wdata => (others => '0'));
    wait;
  end process uc_pr;

  -- This process generates n requests from DMA to MSS (8 bytes, read and write requests, with individual byte enables). Before issuing the next request it
  -- waits for the previous one, if any, to be granted. The grant of the last request is waited for and then, it stops requesting and waits forever.
  dma_pr: process
    variable cnt: natural;
    variable add: word64_address_type := (others => '0');
  begin
    dma2mssl <= (en => '0', rnw => '1', be => (others => '0'), add => (others => '0'), wdata => (others => '0'));
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    cnt := 0;
    l1: loop
      l2: loop
        if dma2mss.en = '1' and ((mss2dma.gnt and dma2mss.be) = dma2mss.be) then -- end of current access
          cnt := cnt + 1;
          add(14 downto 0) := std_ulogic_vector_rnd(15);
          if boolean_rnd and cnt /= n then -- new access
            dma2mssl <= (en => '1', rnw => std_ulogic_rnd, be => std_ulogic_vector_rnd(8), add => add, wdata => std_ulogic_vector_rnd(64));
          else -- no access
            dma2mssl.en <= '0';
          end if;
          wait until rising_edge(clk);
          exit l1 when cnt = n;
          next l1;
        elsif dma2mss.en = '1' then -- ongoing access
          dma2mssl.be <= (not mss2dma.gnt) and dma2mss.be;
        elsif boolean_rnd then -- no ongoing access, launch a new one
          add(14 downto 0) := std_ulogic_vector_rnd(15);
          dma2mssl <= (en => '1', rnw => std_ulogic_rnd, be => std_ulogic_vector_rnd(8), add => add, wdata => std_ulogic_vector_rnd(64));
        else
          dma2mssl.en <= '0';
        end if;
        wait until rising_edge(clk);
      end loop l2;
    end loop l1;
    wait;
  end process dma_pr;

  -- This process generates n requests from VCI to MSS (8 bytes, read and write requests, with individual byte enables). Before issuing the next request it
  -- waits for the previous one, if any, to be granted. The grant of the last request is waited for and then, it stops requesting and waits forever.
  vci_pr: process
    variable cnt: natural;
    variable add: word64_address_type := (others => '0');
  begin
    vci2mssl <= (en => '0', rnw => '1', be => (others => '0'), add => (others => '0'), wdata => (others => '0'));
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    cnt := 0;
    l1: loop
      l2: loop
        if vci2mss.en = '1' and ((mss2vci.gnt and vci2mss.be) = vci2mss.be) then
          cnt := cnt + 1;
          add(14 downto 0) := std_ulogic_vector_rnd(15);
          if boolean_rnd and cnt /= n then
            vci2mssl <= (en => '1', rnw => std_ulogic_rnd, be => std_ulogic_vector_rnd(8), add => add, wdata => std_ulogic_vector_rnd(64));
          else
            vci2mssl.en <= '0';
          end if;
          wait until rising_edge(clk);
          exit l1 when cnt = n;
          next l1;
        elsif vci2mss.en = '1' then
          vci2mssl.be <= (not mss2vci.gnt) and vci2mss.be;
        elsif boolean_rnd then
          add(14 downto 0) := std_ulogic_vector_rnd(15);
          vci2mssl <= (en => '1', rnw => std_ulogic_rnd, be => std_ulogic_vector_rnd(8), add => add, wdata => std_ulogic_vector_rnd(64));
        else
          vci2mssl.en <= '0';
        end if;
        wait until rising_edge(clk);
      end loop l2;
    end loop l1;
    wait;
  end process vci_pr;

  -- This process filters the requests from DMA and VCI. In case there is a write conflict between PSS (Y channel only) and DMA or VCI, the request of the client
  -- with the lowest priority is changed into a read request.
  conflict: process(pss2mss, dma2mssl, vci2mssl)
    variable addpss, adduc, adddma, addvci: integer;
  begin
    if pss2mss.eny = '1' and pss2mss.rnwy = '0' then
      addpss := to_integer(u_unsigned(pss2mss.addy)) + 16#30000#;
    else
      addpss := -8;
    end if;
    if dma2mssl.en = '1' and dma2mssl.rnw = '0' then
      adddma := to_integer(u_unsigned(dma2mssl.add)) * 8;
    else
      adddma := -16;
    end if;
    if vci2mssl.en = '1' and vci2mssl.rnw = '0' then
      addvci := to_integer(u_unsigned(vci2mssl.add)) * 8;
    else
      addvci := -24;
    end if;
    dma2mss <= dma2mssl;
    if adddma <= addpss and addpss <= adddma + 7 then
      dma2mss.rnw <= '1';
    end if;
    vci2mss <= vci2mssl;
    if addvci <= addpss and addpss <= addvci + 7 then
      vci2mss.rnw <= '1';
    end if;
  end process conflict;

  -- This process monitors the (granted) write requests and records them all. It thus always has an up-to-date model of the memory subsystem content. This model
  -- is used to check the result of the requests. An error is issued when a mismatch is detected between the beheavior of the memory subsystem and what the
  -- model predicts.
  process(clk)

		type tmem is array(natural range <>) of word8;
    variable ram: tmem(0 to 16#40fff#) := (others => (others => 'U'));
    variable add: natural range 0 to 16#40fff#;
    variable val: std_ulogic_vector(63 downto 0);

    type ref_64_type is record
      valid: boolean;
      val: std_ulogic_vector(63 downto 0);
    end record;
	  type ref_64_type_array is array(1 to n0 + n1) of ref_64_type;
    type ref_16_type is record
      valid: boolean;
      val: std_ulogic_vector(15 downto 0);
    end record;
	  type ref_16_type_array is array(1 to n0 + n1) of ref_16_type;
    type ref_8_type is record
      valid: boolean;
      val: std_ulogic_vector(7 downto 0);
    end record;
	  type ref_8_type_array is array(1 to n0 + n1) of ref_8_type;

    variable uc_ref, vci_ref, dma_ref: ref_64_type_array;
    variable pss_ref_perm: ref_16_type_array;
    variable pss_ref_di: ref_8_type_array;
    variable pss_ref_do: ref_8_type_array;

    function compare(e, a, b: std_ulogic_vector) return boolean is
      constant n: natural := e'length;
      variable ve: std_ulogic_vector(n - 1 downto 0) := e;
      variable va: std_ulogic_vector(n * 8 - 1 downto 0) := a;
      variable vb: std_ulogic_vector(n * 8 - 1 downto 0) := b;
    begin
      va := to_x01(va xor vb);
      for i in n - 1 downto 0 loop
        if ve(i) = '1' and va(8 * i + 7 downto 8 * i) /= "00000000" and va(8 * i + 7 downto 8 * i) /= "XXXXXXXX" then
          return false;
        end if;
      end loop;
      return true;
    end function compare;

    function compare(a, b: std_ulogic_vector) return boolean is
      constant n: natural := a'length / 8;
      constant e: std_ulogic_vector(n - 1 downto 0) := (others => '1');
    begin
      return compare(e, a, b);
    end function compare;

    variable l: line;

  begin

    if rising_edge(clk) then
      -- Check correctness of last read operations
      if vci_ref(n0 + n1).valid and (not compare(mss2vci.be, mss2vci.rdata, vci_ref(n0 + n1).val)) then
        write(l, string'("VCI error: read "));
        hwrite(l, mss2vci.rdata);
        write(l, string'(", should be "));
        hwrite(l, vci_ref(n0 + n1).val);
        writeline(output, l);
        assert false severity failure;
      end if;
      if dma_ref(n0 + n1).valid and (not compare(mss2dma.be, mss2dma.rdata, dma_ref(n0 + n1).val)) then
        write(l, string'("DMA error: read "));
        hwrite(l, mss2dma.rdata);
        write(l, string'(", should be "));
        hwrite(l, dma_ref(n0 + n1).val);
        writeline(output, l);
        assert false severity failure;
      end if;
      if uc_ref(n0 + n1).valid and (not compare(mss2uc.be, mss2uc.rdata, uc_ref(n0 + n1).val)) then
        write(l, string'("UC error: read "));
        hwrite(l, mss2uc.rdata);
        write(l, string'(", should be "));
        hwrite(l, uc_ref(n0 + n1).val);
        writeline(output, l);
        assert false severity failure;
      end if;
      if pss_ref_perm(n0 + n1).valid and (not compare(mss2pss.p, pss_ref_perm(n0 + n1).val)) then
        write(l, string'("PSS.P error: read "));
        hwrite(l, mss2pss.p);
        write(l, string'(", should be "));
        hwrite(l, pss_ref_perm(n0 + n1).val);
        writeline(output, l);
        assert false severity failure;
      end if;
      if pss_ref_di(n0 + n1).valid and (not compare(mss2pss.x, pss_ref_di(n0 + n1).val)) then
        write(l, string'("PSS.X error: read "));
        hwrite(l, mss2pss.x);
        write(l, string'(", should be "));
        hwrite(l, pss_ref_di(n0 + n1).val);
        writeline(output, l);
        assert false severity failure;
      end if;
      if pss_ref_do(n0 + n1).valid and (not compare(mss2pss.y, pss_ref_do(n0 + n1).val)) then
        write(l, string'("PSS.Y error: read "));
        hwrite(l, mss2pss.y);
        write(l, string'(", should be "));
        hwrite(l, pss_ref_do(n0 + n1).val);
        writeline(output, l);
        assert false severity failure;
      end if;
			-- Move pipeline one stage ahead
      vci_ref(2 to n0 + n1) := vci_ref(1 to n0 + n1 - 1);
	 	  dma_ref(2 to n0 + n1) := dma_ref(1 to n0 + n1 - 1);
      uc_ref(2 to n0 + n1) := uc_ref(1 to n0 + n1 - 1);
      pss_ref_perm(2 to n0 + n1) := pss_ref_perm(1 to n0 + n1 - 1);
      pss_ref_di(2 to n0 + n1) := pss_ref_di(1 to n0 + n1 - 1);
      pss_ref_do(2 to n0 + n1) := pss_ref_do(1 to n0 + n1 - 1);
      -- Read operations
      if vci2mss.en = '1' and vci2mss.rnw = '1' and mss2vci.gnt /= X"00" then
        add := to_integer(u_unsigned(vci2mss.add));
        val := (others => 'X');
        for i in 7 downto 0 loop
          if mss2vci.gnt(i) = '1' then
            val(8 * i + 7 downto 8 * i) := ram(8 * add + 7 - i);
          end if;
        end loop;
        vci_ref(1) := (valid => true, val => val);
        if debug then
          print("VCI", vci2mss.rnw, vci2mss.be, 8 * add, vci2mss.wdata, vci_ref(1).val);
        end if;
      else
        vci_ref(1).valid := false;
      end if;
      if dma2mss.en = '1' and dma2mss.rnw = '1' and mss2dma.gnt /= X"00" then
        add := to_integer(u_unsigned(dma2mss.add));
        val := (others => 'X');
        for i in 7 downto 0 loop
          if mss2dma.gnt(i) = '1' then
            val(8 * i + 7 downto 8 * i) := ram(8 * add + 7 - i);
          end if;
        end loop;
        dma_ref(1) := (valid => true, val => val);
        if debug then
          print("DMA", dma2mss.rnw, dma2mss.be, 8 * add, dma2mss.wdata, dma_ref(1).val);
        end if;
      else
        dma_ref(1).valid := false;
      end if;
      if uc2mss.en = '1' and uc2mss.rnw = '1' and mss2uc.gnt /= X"00" then
        add := to_integer(u_unsigned(uc2mss.add(15 downto 3)));
        val := (others => 'X');
        for i in 7 downto 0 loop
          if mss2uc.gnt(i) = '1' then
            val(8 * i + 7 downto 8 * i) := ram(16#40000# + 8 * add + 7 - i);
          end if;
        end loop;
        uc_ref(1) := (valid => true, val => val);
        if debug then
          print("UC", uc2mss.rnw, uc2mss.be, 8 * add, uc2mss.wdata, uc_ref(1).val);
        end if;
      else
        uc_ref(1).valid := false;
      end if;
      if pss2mss.enp = '1' then
        add := to_integer(u_unsigned(pss2mss.addp));
        pss_ref_perm(1) := (valid => true, val => ram(2 * add) & ram(2 * add + 1));
        if debug then
          print("PSS.P", '1', "11", 2 * add, "", pss_ref_perm(1).val);
        end if;
      else
        pss_ref_perm(1).valid := false;
      end if;
      if pss2mss.enx = '1' then
        add := to_integer(u_unsigned(pss2mss.addx));
        pss_ref_di(1) := (valid => true, val => ram(16#20000# + add));
        if debug then
          print("PSS.X", '1', "1", 16#20000# + add, "", pss_ref_di(1).val);
        end if;
      else
        pss_ref_di(1).valid := false;
      end if;
      if pss2mss.eny = '1' and pss2mss.rnwy = '1' then
        add := to_integer(u_unsigned(pss2mss.addy));
        pss_ref_do(1) := (valid => true, val => ram(16#30000# + add));
        if debug then
          print("PSS.Y", pss2mss.rnwy, "1", 16#30000# + add, pss2mss.y,
          pss_ref_do(1).val);
        end if;
      else
        pss_ref_do(1).valid := false;
      end if;
      -- Write operations
      if vci2mss.en = '1' and vci2mss.rnw = '0' and mss2vci.gnt /= X"00" then
        add := to_integer(u_unsigned(vci2mss.add));
        for i in 7 downto 0 loop
          if vci2mss.be(i) = '1' and mss2vci.gnt(i) = '1' then
            ram(8 * add + 7 - i) := vci2mss.wdata(8 * i + 7 downto 8 * i);
          end if;
        end loop;
      end if;
      if dma2mss.en = '1' and dma2mss.rnw = '0' and mss2dma.gnt /= X"00" then
        add := to_integer(u_unsigned(dma2mss.add));
        for i in 7 downto 0 loop
          if dma2mss.be(i) = '1' and mss2dma.gnt(i) = '1' then
            ram(8 * add + 7 - i) := dma2mss.wdata(8 * i + 7 downto 8 * i);
          end if;
        end loop;
      end if;
      if uc2mss.en = '1' and uc2mss.rnw = '0' and mss2uc.gnt /= X"00" then
        add := to_integer(u_unsigned(uc2mss.add(15 downto 3)));
        for i in 7 downto 0 loop
          if uc2mss.be(i) = '1' and mss2uc.gnt(i) = '1' then
            ram(16#40000# + 8 * add + 7 - i) := uc2mss.wdata(8 * i + 7 downto 8 * i);
          end if;
        end loop;
      end if;
      if pss2mss.eny = '1' and pss2mss.rnwy = '0' then
        add := to_integer(u_unsigned(pss2mss.addy));
        ram(16#30000# + add) := pss2mss.y;
      end if;
    end if;
  end process;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
