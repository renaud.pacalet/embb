--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief FEP's RAMs

library ieee;
use ieee.std_logic_1164.all;

library memories_lib;
use memories_lib.ram_pkg.all;

use work.intl_pkg.all;

entity rams is
  port(clk: in std_ulogic;
       mss2rams : in mss2rams_type;
       rams2mss : out rams2mss_type);
end entity rams;

architecture rtl of rams is

  begin
  
  -------------------------------------------------------
  ------------------ BLOCK RAM INSTANCES ----------------
  -------------------------------------------------------
  g_u: entity memories_lib.tdpram(arc) -- tdpram 1kx32
  generic map(
    registered => true,
    na => 10,
    nb => 4)
  port map(
    clk => clk,
    ena => mss2rams.u(0).en,
    enb => mss2rams.u(1).en,
    wea => mss2rams.u(0).we,
    web => mss2rams.u(1).we,
    adda => mss2rams.u(0).add,
    addb => mss2rams.u(1).add,
    dina => mss2rams.u(0).wdata,
    dinb => mss2rams.u(1).wdata,
    douta => rams2mss.u(0),
    doutb => rams2mss.u(1));

  g_p: for j in 0 to 3 generate
    ram_p: entity memories_lib.tdpram(arc) -- tdpram 16kx16
    generic map(
      registered => true,
      na => 14,
      nb => 2)
    port map(
      clk => clk,
      ena => mss2rams.p(0, j).en,
      enb => mss2rams.p(1, j).en,
      wea => mss2rams.p(0, j).we,
      web => mss2rams.p(1, j).we,
      adda => mss2rams.p(0, j).add,
      addb => mss2rams.p(1, j).add,
      dina => mss2rams.p(0, j).wdata,
      dinb => mss2rams.p(1, j).wdata,
      douta => rams2mss.p(0, j),
      doutb => rams2mss.p(1, j));
  end generate g_p;

  g_x: for j in 0 to 7 generate
    ram_x: entity memories_lib.tdpram(arc) -- tdpram 8kx8
    generic map(
      registered => true,
      na => 13,
      nb => 1)
    port map(
      clk => clk,
      ena => mss2rams.x(0, j).en,
      enb => mss2rams.x(1, j).en,
      wea => mss2rams.x(0, j).we,
      web => mss2rams.x(1, j).we,
      adda => mss2rams.x(0, j).add,
      addb => mss2rams.x(1, j).add,
      dina => mss2rams.x(0, j).wdata,
      dinb => mss2rams.x(1, j).wdata,
      douta => rams2mss.x(0, j),
      doutb => rams2mss.x(1, j));
  end generate g_x;

  g_y: for j in 0 to 7 generate
    ram_y: entity memories_lib.tdpram(arc) -- tdpram 8kx8
    generic map(
      registered => true,
      na => 13,
      nb => 1)
    port map(
      clk => clk,
      ena => mss2rams.y(0, j).en,
      enb => mss2rams.y(1, j).en,
      wea => mss2rams.y(0, j).we,
      web => mss2rams.y(1, j).we,
      adda => mss2rams.y(0, j).add,
      addb => mss2rams.y(1, j).add,
      dina => mss2rams.y(0, j).wdata,
      dinb => mss2rams.y(1, j).wdata,
      douta => rams2mss.y(0, j),
      doutb => rams2mss.y(1, j));
  end generate g_y;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
