/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <embb/intl.h>
#include <embb/memory.h>
#include <assert.h>
#include <stdint.h>
#include <utils.h>
#include <tests.h>

// #define _GNU_SOURCE
#include <getopt.h>

/* Parse command line options, initialize file names. */
void parse_options (int argc, char **argv);

FILE *f;
int n;
int core;

int main(int argc, char **argv)
{
  int i;
  INTL_CONTEXT ctx; // Context
  intl_params cst, val; // Parameters constraints

  parse_options (argc, argv);

  intl_ctx_init(&ctx, 0);
  do_srand(1);

  // Initialize cst (randomly pick fields)
  memset(&cst, 0, sizeof(cst));

  // Initialize val
  val.sv    = 1 << 16;
  val.arm   = 1;
  val.re    = 1;
  val.se    = 1;
  val.fe    = 1;
  val.pbo   = 1;
  val.pbi   = 1;
  val.widm1 = 1 << 8;
  val.biof  = 1 << 8;
  val.boof  = 1 << 8;
  val.fz    = 1 << 16;
  val.fo    = 1 << 16;
  val.iof   = 1 << 16;
  val.oof   = 1 << 16;
  val.pof   = 1 << 16;
  val.lenm1 = 100;

  fprintf(f, "# Reset\n");
  fprintf(f, "RS 10\n");
  if(core == 0) {
    fprintf(f, "# Clear interrupt flags\n");
    fprintf(f, "VR fffb0 1 0 ff 0 0\n");
    fprintf(f, "# Set resets, chip enables and interrupt enables\n");
    fprintf(f, "VW fffa8 1 0 ff\n");
    fprintf(f, "0000000007001636\n"); // No UC, no extended interrupts
  }
  for(i = 0; i < n; i++) {
    fprintf(f, "#################\n");
    fprintf(f, "# Test #%d\n", i + 1);
    fprintf(f, "#################\n");
    if(core == 1) {
      fprintf(f, "RS 10\n");
    }
    mktests(f, &ctx, cst, val);
    fprintf(f, "# End of test #%d\n\n", i + 1);
  }
  intl_ctx_cleanup(&ctx);
  fclose(f);
  return 0;
}

const char *version_string = "\
mkcmdfile 0.1\n\
\n\
Copyright (C) 2010 Institut Telecom\n\
\n\
This software is governed by the CeCILL license under French law and\n\
abiding by the rules of distribution of free software.  You can  use,\n\
modify and/ or redistribute the software under the terms of the CeCILL\n\
license as circulated by CEA, CNRS and INRIA at the following URL\n\
http://www.cecill.info\n\
\n\
Written by Renaud Pacalet <renaud.pacalet@telecom-paristech.fr>.\n\
";

const char *usage_string = "\
mkcmdfile generate commands for VHDL functional simulation\n\
of a DSP unit or an IP core.\n\
\n\
Usage: mkcmdfile [OPTION]... [OUTPUTFILE]\n\
\n\
Mandatory arguments to long options are mandatory for short options too.\n\
If OUTPUTFILE is not specified stdout is used. On success the exit status is 0.\n\
\n\
Options:\n\
  -c, --core            generate a command file for IP core simulation instead of whole DSP unit\n\
  -t, --tests=NUM       number of tests to generate (default=1)\n\
  -v, --version\n\
        Output version information and exit\n\
  -h, --help\n\
        Display this help and exit\n\
\n\
Please report bugs to <renaud.pacalet@telecom-paristech.fr>.\n\
";

void
parse_options (int argc, char **argv) {
  int o, option_index;
  struct option long_options[] = {
    {"core", no_argument, NULL, 'c'},
    {"tests", required_argument, NULL, 't'},
    {"version", no_argument, NULL, 'v'},
    {"help", no_argument, NULL, 'h'},
    {0, 0, 0, 0}
  };

  option_index = 0;
  core = 0;
  n = 1;
  while (1) {
    o =
      getopt_long (argc, argv, "ct:vh",
      	     long_options, &option_index);
    if (o == -1) {
      break;
    }
    switch (o) {
      case 'c':
        core = 1;
        break;
      case 't':
        n = atoi(optarg);
        if(n < 1) {
          fprintf (stderr, "*** Invalid number of tests: %d\n", n);
          ERROR(-1, "%s", usage_string);
        }
        break;
      case 'v':
        fprintf (stderr, "%s", version_string);
        exit (0);
        break;
      default:
        fprintf (stderr, "%s", usage_string);
        exit (0);
        break;
    }
  }
  f = stdout;
  if (optind < argc) {
    f = XFOPEN(argv[optind], "w");
    optind += 1;
  }
  if (optind < argc) {
    ERROR(-1, "%s", usage_string);
  }
}
