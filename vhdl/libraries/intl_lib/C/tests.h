/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <embb/intl.h>
#include <embb/memory.h>
#include <stdint.h>

typedef struct {
  uint32_t sv;
  uint32_t arm;
  uint32_t re;
  uint32_t se;
  uint32_t fe;
  uint32_t pbo;
  uint32_t pbi;
  uint32_t widm1;
  uint32_t biof;
  uint32_t boof;
  uint32_t fz;
  uint32_t fo;
  uint32_t iof;
  uint32_t oof;
  uint32_t pof;
  uint32_t lenm1;
} intl_params;

// Run one random test
void mktests(FILE *f, INTL_CONTEXT *ctx, intl_params cst, intl_params val);
