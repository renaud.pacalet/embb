/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <embb/intl.h>
#include <embb/memory.h>
#include <assert.h>
#include <stdint.h>
#include <utils.h>
#include <tests.h>

#define INDATA_SIZE 32

#define DEBUG(...) fprintf(stderr, __VA_ARGS__);

#define ASSERT_EQ(a, b)							\
  do {									\
    typeof(a) a_ = a;							\
    typeof(a) b_ = b;							\
    if (a_ != b_) {							\
      DEBUG("error:%u:%s:%s %s equals to 0x%x, expected 0x%x\n", __LINE__, __FILE__, __func__, #a, a_, b_); \
      abort();								\
    }									\
  } while (0)

// Human-readable print of parameters
void print_params(FILE *f, INTL_CONTEXT *ctx) {
  fprintf(f, "# SV=0x%04llx, ARM=%lld, RE=%lld, SE=%lld, FE=%lld, PBO=%lld, PBI=%lld, WIDM1=%lld, BIOF=%lld, BOOF=%lld\n", intl_get_sv(ctx), intl_get_arm(ctx), intl_get_re(ctx),
      intl_get_se(ctx), intl_get_fe(ctx), intl_get_pbo(ctx), intl_get_pbi(ctx), intl_get_widm1(ctx), intl_get_biof(ctx), intl_get_boof(ctx));
  fprintf(f, "# FZ=0x%04llx, FO=0x%04llx, IOF=0x%04llx, OOF=0x%04llx, POF=0x%04llx, LENM1=0x%04llx\n", intl_get_fz(ctx), intl_get_fo(ctx), intl_get_iof(ctx),
      intl_get_oof(ctx), intl_get_pof(ctx), intl_get_lenm1(ctx));
}

// Print execute command
void print_ex(FILE *f, INTL_CONTEXT *ctx) {
  fprintf(f, "EX 0 0 0 0f\n%016llx\n%016llx\n%016llx\n", ctx->devmem->cmd._r2, ctx->devmem->cmd._r1, ctx->devmem->cmd._r0);
}

// randomly generates context ctx. cst and val are two intl_params structures expressing constraints on the random generation and must be initialized prior
// calling randctx. In cst field values are either 0 or 1; a 1 indicates that the corresponding field value must be taken as is from val; a 0 indicates that the
// field must be randomly picked between 0 (included) and the same filed's value in val (exluded). When randomly picked, each field is guaranteed to have a
// valid value with respect to the other ctx fields.
void randctx(INTL_CONTEXT *ctx, intl_params cst, intl_params val)
{
  uint64_t tmp, tmp1;

  if(cst.widm1 == 1)
    intl_set_widm1(ctx, val.widm1);
  else
    intl_set_widm1(ctx, do_rand() % val.widm1);

  if(cst.pbi == 1)
    intl_set_pbi(ctx, val.pbi);
  else if(intl_get_widm1(ctx) == 0)
    intl_set_pbi(ctx, do_rand() % val.pbi);
  else
    intl_set_pbi(ctx, 0); // PBI cannot be set if WIDM1 != 0

  if(cst.pbo == 1)
    intl_set_pbo(ctx, val.pbo);
  else if(intl_get_widm1(ctx) == 0)
    intl_set_pbo(ctx, do_rand() % val.pbo);
  else
    intl_set_pbo(ctx, 0); // PBI cannot be set if WIDM1 != 0

  if(cst.re == 1)
    intl_set_re(ctx, val.re);
  else
    intl_set_re(ctx, do_rand() % val.re);

  if(cst.fe == 1)
    intl_set_fe(ctx, val.fe);
  else
    intl_set_fe(ctx, do_rand() % val.fe);

  if(cst.se == 1)
    intl_set_se(ctx, val.se);
  else
    intl_set_se(ctx, do_rand() % val.se);

  if(cst.arm == 1)
    intl_set_arm(ctx, val.arm);
  else
    intl_set_arm(ctx, do_rand() % val.arm);

  if(cst.sv == 1)
    intl_set_sv(ctx, val.sv);
  else {
    if(intl_get_re(ctx) == 1 && intl_get_se(ctx) == 1)
      tmp = val.sv < 0x8000 ? val.sv : 0x8000; // If RE and SE, SV MSB cannot be set
    else
      tmp = val.sv;
    intl_set_sv(ctx, do_rand() % tmp);
  }

  if(cst.fz == 1)
    intl_set_fz(ctx, val.fz);
  else {
    if(intl_get_re(ctx) == 1 && intl_get_fe(ctx) == 1)
      tmp = val.fz < 0x8000 ? val.fz : 0x8000; // If RE and FE, FZ MSB cannot be set
    else
      tmp = val.fz;
    if(intl_get_se(ctx) == 1)
      do {
        tmp1 = do_rand() % tmp;
      } while(tmp1 == intl_get_sv(ctx)); // If RE and SE, FZ != SV
    else
      tmp1 = do_rand() % tmp;
    intl_set_fz(ctx, tmp1);
  }

  if(cst.fo == 1)
    intl_set_fo(ctx, val.fo);
  else {
    if(intl_get_re(ctx) == 1 && intl_get_fe(ctx) == 1)
      tmp = val.fo < 0x8000 ? val.fo : 0x8000; // If RE and FE, FO MSB cannot be set
    else
      tmp = val.fo;
    if(intl_get_se(ctx) == 1)
      do {
        tmp1 = do_rand() % tmp;
      } while(tmp1 == intl_get_sv(ctx) || tmp1 == intl_get_fz(ctx)); // If RE and SE, FO != SV && FO != FZ
    else
      do {
        tmp1 = do_rand() % tmp;
      } while(tmp1 == intl_get_fz(ctx)); // FO != FZ
    intl_set_fo(ctx, tmp1);
  }

  if(cst.biof == 1)
    intl_set_biof(ctx, val.biof);
  else
    intl_set_biof(ctx, do_rand() % val.biof);

  if(cst.boof == 1)
    intl_set_boof(ctx, val.boof);
  else
    intl_set_boof(ctx, do_rand() % val.boof);

  if(cst.iof == 1)
    intl_set_iof(ctx, val.iof);
  else
    intl_set_iof(ctx, do_rand() % val.iof);

  if(cst.oof == 1)
    intl_set_oof(ctx, val.oof);
  else
    intl_set_oof(ctx, do_rand() % val.oof);

  if(cst.pof == 1)
    intl_set_pof(ctx, val.pof);
  else
    intl_set_pof(ctx, do_rand() % val.pof);

  // LENM1 is not as free as one could think: it must not overflow the permutation table nor the output memory. The former constraint is simple:
  //   POF + LENM1 < 0x10000 => MAXLEN <= 0x01000 - POF
  // The latter is a bit more complex as it depends on the other parameters. The counter CNT of output samples must not overflow, that is:
  // - OOF + CNT < 0x10000            => MAXCNT = 0x10000 - OOF - 1            (if PBO = 0)
  // - 8 * OOF + BOOF + CNT < 0x80000 => MAXCNT = 0x80000 - 8 * OOF - BOOF - 1 (if PBO = 1)
  // On the other hand, CNT also has a lower bound, that is, a minimum value it will take at the end of the processing. At the end of the processing CNT value
  // is the number of written output samples, plus the number of skip entries, minus one (it is not incremented the last time):
  // - If RE = 1 and ARM = 0 and if the permutation table is made of a single, LENM1+1-entries, repeat sequence, there will be only one output sample and no
  //   skips =>
  //     MINCNT = 0
  // - If RE = 1 and ARM = 1, the minimum final value of CNT is obtained when the permutation table is made of no skips and as few repeat sequences as possible.
  //   As the repeat sequences are 8-entries long at maximum, there are LENM1/8 + 1 repeat sequences, minimum, that is, as many output samples, so =>
  //     MINCNT = LENM1/8
  // - If RE = 0 (no repeat sequences), each of the LENM1 + 1 entries (but the last) in the permutation table will increment CNT, either by outputting a sample
  //   or by skipping =>
  //     MINCNT = LENM1
  // All in all, as MINCNT <= MAXCNT must hold, we have:
  // - If RE = 0 and             PBO = 0,                                            LENM1 <= 0x10000 - OOF - 1                  => MAXLEN <= 0x10000 - OOF
  // - If RE = 0 and             PBO = 1,                                            LENM1 <= 0x80000 - 8 * OOF - BOOF - 1       => MAXLEN <= 0x80000 - 8 * OOF - BOOF
  // - If RE = 1 and ARM = 0 and PBO = 0,       0 <= 0x10000 - OOF - 1                                                           => true
  // - If RE = 1 and ARM = 0 and PBO = 1,       0 <= 0x80000 - 8 * OOF - BOOF - 1                                                => true
  // - If RE = 1 and ARM = 1 and PBO = 0, LENM1/8 <= 0x10000 - OOF - 1            => LENM1 <= 0x80000 - 8 * OOF - 1              => MAXLEN <= 0x80000 - 8 * OOF
  // - If RE = 1 and ARM = 1 and PBO = 1, LENM1/8 <= 0x80000 - 8 * OOF - BOOF - 1 => LENM1 <= 0x400000 - 64 * OOF - 8 * BOOF - 1 => MAXLEN <= 0x400000 - 64 * OOF - 8 * BOOF
  if(cst.lenm1 == 1)
    intl_set_lenm1(ctx, val.lenm1);
  else { // Compute MAXLEN = TMP
    tmp = 0x10000 - intl_get_pof(ctx);       // TMP = 0x10000 - POF
    tmp = val.lenm1 < tmp ? val.lenm1 : tmp; // From VAL constraint
    tmp1 = 0x10000 - intl_get_oof(ctx);       // TMP1 = 0x10000 - OOF
    if(intl_get_pbo(ctx))                    // If PBO = 1
      tmp1 = 8 * tmp1 - intl_get_boof(ctx);  //   TMP1 = 0x80000 - OOF - BOOF
    if(!intl_get_re(ctx))                    // If RE = 0
      tmp = tmp < tmp1 ? tmp : tmp1;         //   TMP = MIN(TMP, TMP1)
    else if(intl_get_arm(ctx))               // Elsif RE = 1 and ARM = 1
      tmp = tmp < 8 * tmp1 ? tmp : 8 * tmp1; //   TMP = MIN(TMP, 8 * TMP1)
    tmp = tmp < 100 ? tmp : 100;             // TMP = MIN(TMP, 100) (avoid too long permutations)
    intl_set_lenm1(ctx, do_rand() % tmp);    // LENM1 = RANDOM % MAXLEN => LENM1 < MAXLEN
  }
}

// Generate a random but valid permutation table leading with IMIN <= offset <= IMAX in input sequence and a OMAX maximum offset in output sequence.
void generate_permutation(INTL_CONTEXT *ctx, uint8_t *datap, uint32_t imin, uint32_t imax, uint32_t omax);

// Run one random test
void mktests(FILE *f, INTL_CONTEXT *ctx, intl_params cst, intl_params val)
{
  uint32_t i, lenm1, tmp, imin, imax, omax, omax_min, omax_max, bp0, bp1, sp, bx0, bx1, sx, by0, by1, sy, *ptr;

  randctx(ctx, cst, val);

  print_params(f, ctx);

  lenm1 = intl_get_lenm1(ctx);

  // Allocate a double-word aligned permutation buffer large enough to store the permutation table. Randomly initialize the 8 first and last bytes of the
  // buffer. Store the generated permutation table at right position in buffer.
  bp0 = 2 * intl_get_pof(ctx); // Offset of first byte of permutation table in P buffer
  bp1 = bp0 + 2 * lenm1 + 1;   // Offset of last byte of permutation table in P buffer
  bp0 &= 0xffff8;              // Offset of first byte with double-word alignment
  bp1 |= 0x7;                  // Offset of last byte with double-word alignment
  sp = bp1 + 1 - bp0;          // Byte size with double-word alignment
  uint8_t datap[sp];           // Double-word aligned permutation table buffer
  ptr = (uint32_t *)(datap);
  for(i = 0; i < 2; i++, ptr++) // Randomly initialize 8 first bytes
    *ptr = do_rand();
  ptr = (uint32_t *)(datap + sp - 8);
  for(i = 0; i < 2; i++, ptr++) // Randomly initialize 8 last bytes
    *ptr = do_rand();
  if(!intl_get_pbi(ctx))            // If PBI=1
    imax = 0x10000 - 1 - intl_get_iof(ctx); // Maximum offset in input buffer
  else
    imax = 0x80000 - 1 - 8 * intl_get_iof(ctx) - intl_get_biof(ctx); // Maximum offset in input buffer
  if(intl_get_re(ctx))
    imax &= 0x7fff;
  if(imax < 100)
    imin = 0;
  else {
    imin = do_rand() % (imax - 99);
    imax = imin + 100;
  }
  if(!intl_get_pbo(ctx))
    omax_min = intl_get_oof(ctx);
  else
    omax_min = 8 * intl_get_oof(ctx) + intl_get_boof(ctx);
  omax_max = omax_min + lenm1;
  if(!intl_get_pbo(ctx))
    omax_max = omax_max > 0x10000 - 1 ? 0x10000 - 1 : omax_max;
  else
    omax_max = omax_max > 0x80000 - 1 ? 0x80000 - 1 : omax_max;
  if(!intl_get_re(ctx))
    omax = omax_max - omax_min;
  else if(!intl_get_arm(ctx))
    omax = do_rand() % (omax_max + 1 - omax_min);
  else
    omax = do_rand() % (omax_max + 1 - omax_min - lenm1 / 8) + lenm1 / 8;
  generate_permutation(ctx, datap + (2 * intl_get_pof(ctx)) % 8, imin, imax, omax);   // Create permutation table and store it in datap
  embb_mem2ip((EMBB_CONTEXT*)ctx, INTL_PERMBUF_OFFSET + bp0, datap, sp); // Install permutation buffer in IP MSS
  fprintf(f, "DW %lx %d 0 ff\n", INTL_PERMBUF_OFFSET + bp0, sp / 8); // DMA command
  dump_buffer(f, datap, sp / 2, 2, 0, 1, 8); // DMA data
  fprintf(f, "WD 10000\n"); // Wait end of DMA transfer

  // Allocate a double-word aligned input buffer large enough to store the input sequence. Randomly initialize the buffer.
  bx0 = intl_get_iof(ctx);
  if(intl_get_pbi(ctx) == 1)
    bx0 = 8 * bx0 + intl_get_biof(ctx);
  bx1 = bx0 + imax;
  bx0 += imin;
  if(intl_get_pbi(ctx) == 1) {
    bx0 /= 8;
    bx1 /= 8;
  }
  bx0 &= 0xfff8;
  bx1 |= 0x7;
  sx = bx1 + 1 - bx0;
  uint8_t datax[sx];
  ptr = (uint32_t *)(datax);
  for (i = 0; i < sx / 4; i++, ptr++)
    *ptr = do_rand();
//  DEBUG("INTL_INBUF_OFFSET + bx0 = %lx, sx = %d\n", INTL_INBUF_OFFSET + bx0, sx);
  embb_mem2ip((EMBB_CONTEXT*)ctx, INTL_INBUF_OFFSET + bx0, datax, sx);
  fprintf(f, "DW %lx %d 0 ff\n", INTL_INBUF_OFFSET + bx0, sx / 8);
  dump_buffer(f, datax, sx, 1, 0, 1, 8);
  fprintf(f, "WD 10000\n");

  // Allocate a double-word aligned input buffer large enough to store the input sequence. Randomly initialize the buffer.
  by0 = intl_get_oof(ctx);
  by1 = by0 + omax;
  by0 &= 0xfff8;
  by1 |= 0x7;
  sy = by1 + 1 - by0;
//  DEBUG("by0=%04x, by1=%04x, sy=%04x\n", by0, by1, sy);
  uint8_t datay[sy];
  ptr = (uint32_t *)(datay);
  for (i = 0; i < sy / 4; i++, ptr++)
    *ptr = do_rand();
  embb_mem2ip((EMBB_CONTEXT*)ctx, INTL_OUTBUF_OFFSET + by0, datay, sy);
  fprintf(f, "DW %lx %d 0 ff\n", INTL_OUTBUF_OFFSET + by0, sy / 8);
  dump_buffer(f, datay, sy, 1, 0, 1, 8);
  fprintf(f, "WD 10000\n");

  intl_start(ctx);

  print_ex(f, ctx);
  fprintf(f, "WE 10000\n");

  embb_ip2mem(datay, (EMBB_CONTEXT*)ctx, INTL_OUTBUF_OFFSET + by0, sy);
  fprintf(f, "DR %lx %d 0 ff 1 0 ff\n", INTL_OUTBUF_OFFSET + by0, sy / 8);
  dump_buffer(f, datay, sy, 1, 0, 1, 8);
  fprintf(f, "WD 10000\n");
}

void generate_permutation(INTL_CONTEXT *ctx, uint8_t *datap, uint32_t imin, uint32_t imax, uint32_t omax)
{
  intl_params val;

  val.sv    = intl_get_sv(ctx);
  val.arm   = intl_get_arm(ctx);
  val.re    = intl_get_re(ctx);
  val.se    = intl_get_se(ctx);
  val.fe    = intl_get_fe(ctx);
  val.pbo   = intl_get_pbo(ctx);
  val.pbi   = intl_get_pbi(ctx);
  val.widm1 = intl_get_widm1(ctx);
  val.biof  = intl_get_biof(ctx);
  val.boof  = intl_get_boof(ctx);
  val.fz    = intl_get_fz(ctx);
  val.fo    = intl_get_fo(ctx);
  val.iof   = intl_get_iof(ctx);
  val.oof   = intl_get_oof(ctx);
  val.pof   = intl_get_pof(ctx);
  val.lenm1 = intl_get_lenm1(ctx);

  uint32_t num_rss, num_reg, num_rsq;

  if(!val.re) { // Repeat disabled
    if(omax != val.lenm1) {
      DEBUG("error:%u:%s:%s re=0, omax=%d, lenm1=%d\n", __LINE__, __FILE__, __func__, omax, val.lenm1);
      abort();
    }
    num_rss = 0; // No entries with repeat flag set
    num_reg = val.lenm1 + 1; // All entries are regular
    num_rsq = 0; // No repeat sequences
  }
  else {
    if(val.arm && omax < val.lenm1 / 8) {
      DEBUG("error:%u:%s:%s re=0, arm=1, omax=%d, lenm1=%d\n", __LINE__, __FILE__, __func__, omax, val.lenm1);
      abort();
    }
    num_rss = val.lenm1 - omax; // Number of entries with repeat flag set
    num_reg = val.lenm1 + 1 - num_rss; // Number of regular entries

    uint32_t num_rsq_min, num_rsq_max;

    if(val.arm) { // Average mode, repeat sequences are 1 to 8 entries long
      num_rsq_min = (num_rss + 6) / 7; // Minimum number of repeat sequences (8 entries each, but eventually the one or two last)
      num_rsq_max = (val.lenm1 + 1) / 2; // Maximum number of repeat sequences (2 entries each, but eventually the last one)
      num_rsq_max = num_rsq_max < num_rss ? num_rsq_max : num_rss; // Cannot have more repeat sequences than entries with repeat flag set
    }
    else { // Last mode
      num_rsq_min = num_rss ? 1 : 0; // Minimum number of repeat sequences
      num_rsq_max = (val.lenm1 + 1) / 2; // Maximum number of repeat sequences (2 entries each, but eventually the last one)
      num_rsq_max = num_rsq_max < num_rss ? num_rsq_max : num_rss; // Cannot have more repeat sequences than entries with repeat flag set
    }
    num_rsq = do_rand() % (num_rsq_max - num_rsq_min + 1) + num_rsq_min; // Randomly select number of repeat sequences
  }

  uint32_t sequences[2 * num_rsq + 1]; // sequences[2*i]: number - 1 of regular entries, sequences[2*i+1]: number - 1 of entries with repeat flag set
  int i, n;

  if(!val.re) // Repeat disabled
    sequences[0] = val.lenm1; // One single sequence of LENM1+1 entries
  else { // Repeat enabled
    memset(sequences, 0, (2 * num_rsq + 1) * sizeof(uint32_t));
    sequences[0] = -1; // First series of regular entries can be empty, all other series are initialized to 0, that is, one entry
    for(i = num_rsq; i < num_rss; i++) { // For all remaining entries with repeat flag set
      n = do_rand() % num_rsq; // Randomly select a sequence
      if(val.arm) // If average mode
        while(sequences[2 * n + 1] == 6) // While sequence is already full
          n = (n + 1) % num_rsq; // Go to next
      sequences[2 * n + 1] += 1; // Add entry to found sequence
    }
    for(i = num_rsq; i < num_reg; i++) { // For all remaining regular entries
      n = do_rand() % (num_rsq + 1); // Randomly select a sequence
      sequences[2 * n] += 1; // Add entry to found sequence
    }
  }
  if(! val.fe) { // Force disabled
    val.fz = 0x10000;
    val.fo = 0x10000;
  }
  if(! val.se) // Skip disabled
    val.sv = 0x10000;

  uint16_t *p;
  p = (uint16_t *)(datap);
  uint16_t tmp;
  int j;

  for(j = 0, n = 0; n <= 2 * num_rsq; n++) { // For all sequences
//    DEBUG("SEQUENCE #%d: %d\n", j, sequences[j]);
    if(n % 2) // If repeat sequence
      for(i = 0; i < sequences[n] + 1; i++, p++) { // For all entries in sequence
        do {
          tmp = 0x8000 | (do_rand() % (imax + 1 - imin) + imin);
        } while(tmp == val.sv || tmp == val.fz || tmp == val.fo);
        *p = tmp;
      }
    else
      for(i = 0; i < sequences[n] + 1; i++, p++) { // For all entries in sequence
        tmp = do_rand() % (imax + 1 - imin) + imin;
        switch(do_rand() % 20) {
          case 0: if(val.fe)
                    tmp = val.fz;
                  break;
          case 1: if(val.fe)
                    tmp = val.fo;
                  break;
          case 2: if(val.se)
                    tmp = val.sv;
                  break;
          default: break;
        }
        *p = tmp;
//        DEBUG("P[%d]: %d\n", i, *p);
      }
  }
}
