--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Memory Sub system

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library memories_lib;
use memories_lib.ram_pkg.all;

library global_lib;
use global_lib.utils.all;
use global_lib.global.all;
 
use work.adaif_pkg.all;

entity mss is
  generic(n0: positive := 1;  --* Number of input pipeline registers in MSS, including input registers of RAMs
          n1: positive := 1;  --* Number of output pipeline registers in MSS, including output registers of RAMs
          with_uc: boolean := false;        -- With or without UC
          nrx: natural range 0 to 4 := 1;  -- Number of RX chains
          ntx: natural range 0 to 4 := 0); -- Number of TX chains
  port(clk:      in  std_ulogic;
       -- Control subsystem interface (VCIInterface, DMA ans UC)
       css2mss:  in  css2mss_type;
       mss2css:  out mss2css_type;
       -- PSS interface
       fifo2mss: in fifo2mss_type;
       mss2fifo: out mss2fifo_type;
       pss2mss:   in  pss2mss_type;
       mss2pss:   out mss2pss_type;
       rams2mss: in  rams2mss_type;
       mss2rams: out mss2rams_type);
end entity mss;

architecture rtl of mss is

  alias vci2mss is css2mss.vci2mss;
  alias mss2vci is mss2css.mss2vci;
  alias dma2mss is css2mss.dma2mss;
  alias mss2dma is mss2css.mss2dma;
  alias uc2mss  is css2mss.uc2mss;
  alias mss2uc  is mss2css.mss2uc;
  alias pss2tx   is pss2mss.pss2tx;
  alias pss2rx   is pss2mss.pss2rx;
  alias tx2pss   is mss2pss.tx2pss;
  alias rx2pss   is mss2pss.rx2pss;

  signal ucr_in : ucr_in_type;  -- Input ports of the UCR RAM
  signal tx_in  : tx_in_type;   -- Input ports of the TX RAM
  signal rx_in  : rx_in_type;   -- Input ports of the RX RAM

  signal ucr_out: ucr_out_type; -- Output ports of the UCR RAM
  signal tx_out: tx_out_type;   -- Output ports of the TX RAM
  signal rx_out: rx_out_type;   -- Output ports of the RX RAM

  signal mss2rams_pipe: mss2rams_vector(1 to n0);
  signal rams2mss_pipe: rams2mss_vector(1 to n1);

  type area_type is (TX, RX, UCR);

  type index_type is array (natural range <>) of natural range 0 to 1;

  type avci_mux_type is record
    en   : std_ulogic;
    area : area_type;
    idx  : natural range 0 to 3;
    p    : index_type(0 to 1);
    be   : word64_be_type;
    col  : natural range 0 to 1;
  end record;

  type pss_mux_type is record
    ten   : std_ulogic_vector(0 to 3);
    ren   : std_ulogic;
    rcol  : natural range 0 to 1;
    tcol  : index_type(0 to 3);
    idx   : natural range 0 to 3;
  end record;

  type uc_mux_type is record
    en   : std_ulogic;
    be   : word64_be_type;
  end record;

  type used_type is array (0 to 3, 0 to 1, ports) of boolean;

  type mux_type is record
    dma: avci_mux_type;
    vci: avci_mux_type;
    pss: pss_mux_type;
    uc: uc_mux_type;
  end record;

  type mux_pipe_type is array(1 to n0 + n1) of mux_type;

  signal mux, mux_in : mux_type;
  signal mux_pipe: mux_pipe_type;

begin

  -- The arbiter combinatorial process computes the different requests each
  -- client sends to each block RAM. It then enables only one client per port
  -- according to the fixed priority: PSS first, then UC, DMA and VCI. The
  -- arbiter also prepares the commands of the multiplexors that select block
  -- RAMs outputs to send to the clients.

  arbiter: process(css2mss, pss2mss, fifo2mss)

    variable ucr_in_v: ucr_in_type; -- Input ports of the UCR RAM
    variable tx_in_v: tx_in_type;   -- Input ports of the TX RAM
    variable rx_in_v: rx_in_type;   -- Input ports of the RX RAM
    variable pair: natural range 0 to 3;
    variable po: natural range 0 to 1;
    variable en: std_ulogic;
    variable mux_in_v: mux_type;
    variable uc_oor: std_ulogic;
    variable uc_ucr_access, dma_ucr_access, vci_ucr_access : boolean;
    variable rx_used, tx_used : used_type;
    variable wopr, ropr, wopt, ropt : op_vector(0 to 3);
    variable col : natural range 0 to 1;
    variable tx_fifo_written, rx_fifo_read : boolean_vector(0 to 3);
    variable tmp_gnt: word64_be_type;
    variable tmp_oor: std_ulogic;

    -- dmavci is called twice: once for DMA and once for VCI. It handles the arbitration for all DMA (or VCI) accesses. It is there only to factorize the code
    -- between DMA and VCI.
    procedure dmavci(xxx2mss: in dma2mss_type; mss2xxx_oor: out std_ulogic; mss2xxx_gnt: out word64_be_type; m: inout avci_mux_type) is
      variable oor: boolean; -- Out of range flag
      variable pair: natural range 0 to 3; -- Index of a TX/RX pair
    begin
      oor := false; -- By default no out of range access
      oor := oor or (or_reduce(xxx2mss.add(28 downto 15)) = '1'); -- Out of range (trivial case)
      if xxx2mss.add(14) = '0' then -- Address falls in TX or RX FIFOs
        oor := oor or (xxx2mss.add(10) = '0' and to_integer(u_unsigned(xxx2mss.add(12 downto 11))) >= ntx); -- Not implemented TX FIFO
        oor := oor or (xxx2mss.add(10) = '1' and to_integer(u_unsigned(xxx2mss.add(12 downto 11))) >= nrx); -- Not implemented RX FIFO
      end if;
      if xxx2mss.add(14 downto 13) = "01" then -- Address falls in FIFO-like accesses in TX/RX FIFOs
        oor := oor or (xxx2mss.add(10) = '0' and xxx2mss.rnw = '1'); -- Read in TX FIFO
        oor := oor or (xxx2mss.add(10) = '1' and xxx2mss.rnw = '0'); -- Write in RX FIFO
        oor := oor or (xxx2mss.be /= X"FF" and xxx2mss.be /= X"0F" and xxx2mss.be /= X"F0"); -- Invalid byte enables in FIFO-like mode
      elsif xxx2mss.add(14 downto 13) = "10" then -- Address falls in UC RAM
        oor := oor or (or_reduce(xxx2mss.add(12 downto 9)) = '1'); -- Out of range (trivial case)
      end if;
      if oor then -- Invalid access
        mss2xxx_oor := '1';
-- pragma translate_off
        assert xxx2mss.en = '0' report "Out of range memory access (" & integer'image(8 * to_integer(u_unsigned(xxx2mss.add))) & ")" severity error;
-- pragma translate_on
      else
        mss2xxx_oor := '0';
      end if;
  
      mss2xxx_gnt := (others => '0'); -- By default grant none of the bytes
      pair := to_integer(u_unsigned(xxx2mss.add(12 downto 11))); -- Index of the accessed TX/RX pair
      m.idx  := pair;
      if xxx2mss.en = '1' then -- Access
        if xxx2mss.add(14 downto 13) = "01" then -- Address falls in FIFO-like accesses in TX/RX FIFOs
          for p in 0 to 3 loop -- For all pairs (synthesis trick)
            if p = pair then   -- p is the index of the accessed RX/TX (synthesis trick)
              if xxx2mss.add(10) = '0' and xxx2mss.rnw = '0' and p < ntx then -- FIFO-like write in a implemented TX FIFO
              -- This case is complex. There may be:
              -- - one or two samples to write,
              -- - zero, one or more free places in the FIFO,
              -- - the target column(s) may have two busy ports or not,
              -- - a higher priority FIFO-like access may prevent this one.
              -- If only one sample can be written while two should be, the first sample is written and the second is delayed until it can be written.
                col := to_i01(fifo2mss.wptrt(p)(0)); -- Column of first sample
                m.area := TX; -- Area indicator for output multiplexor 
                m.idx := p;   -- Indicator of TX/RX pair for output multiplexor
                m.col := col; -- Column indicator for output multiplexor
                if tx_fifo_written(p) or (tx_used(p, col, 0) and tx_used(p, col, 1)) then -- FIFO already written in FIFO-like mode or the two ports of first target column are already busy
                  null; -- Do nothing
                elsif fifo2mss.wokt(p) = '0' then -- Write forbidden (FIFO full)
                  tx2pss.ovf(p) <= '1'; -- Signal overflow
                else -- Write allowed (FIFO not full, not already written in FIFO-like mode) and at least one free port
                  m.en := '1'; -- Valid response
                  if not tx_used(p, col, 0) then -- Port 0 not used
                    po := 0; -- Use port 0
                  else -- Port 1 not used
                    po := 1; -- Use port 1
                  end if;
                  m.p(col) := po; -- Output port for output multiplexor
                  tx_in_v(p, col, po).we    := X"F"; -- Write enables
                  if xxx2mss.be(7 downto 4) = X"F" then -- Leftmost sample
                    mss2xxx_gnt(7 downto 4) := X"F"; -- Grant access
                    tx_in_v(p, col, po).wdata := xxx2mss.wdata(63 downto 32); -- Written data
                    m.be(7 downto 4) := X"F"; -- Byte enables for output multiplexor
                  else -- Rightmost sample
                    mss2xxx_gnt(3 downto 0) := X"F"; -- Grant access
                    tx_in_v(p, col, po).wdata := xxx2mss.wdata(31 downto 0); -- Written data
                    m.be(3 downto 0) := X"F"; -- Byte enables for output multiplexor
                  end if;
                  tx_in_v(p, col, po).add   := drop_lsbs(fifo2mss.wptrt(p), 1); -- Address is MSBs of write pointer (the LSB selects the column)
                  tx_in_v(p, col, po).en    := xxx2mss.en; -- Enable RAM access
                  tx_used(p, col, po)       := true; -- Mark port as used
                  wopt(p) := 1; -- Wrote one sample
                  if xxx2mss.be = X"FF" then -- There is a second sample to write
                    col := to_i01(not fifo2mss.wptrt(p)(0)); -- Column of second sample
                    if tx_used(p, col, 0) and tx_used(p, col, 1) then -- The two ports of second target column are already busy
                      null; -- Do nothing
                    elsif fifo2mss.dwokt(p) = '0' then -- Double write forbidden (FIFO full)
                      tx2pss.ovf(p) <= '1';
                    else -- Write allowed (FIFO not full) and at least one free port
                      if not tx_used(p, col, 0) then -- Port 0 not used
                        po := 0; -- Use port 0
                      else -- Port 1 not used
                        po := 1; -- Use port 1
                      end if;
                      m.p(col) := po; -- Output port for output multiplexor
                      mss2xxx_gnt(3 downto 0) := X"F"; -- The second sample, if any, is always on the 32 LSBs
                      m.be(3 downto 0) := X"F"; -- Byte enables for output multiplexor
                      tx_in_v(p, col, po).we    := X"F"; -- Write enables
                      tx_in_v(p, col, po).wdata := xxx2mss.wdata(31 downto 0); -- Written data
                      if col = 1 then -- Same address as first sample
                        tx_in_v(p, col, po).add   := drop_lsbs(fifo2mss.wptrt(p), 1);
                      else -- Address of second sample is address of first sample + 1
                        tx_in_v(p, col, po).add   := drop_lsbs(fifo2mss.wptrt1(p), 1);
                      end if;
                      tx_in_v(p, col, po).en    := xxx2mss.en; -- Enable RAM access
                      tx_used(p, col, po)       := true; -- Mark port as used
                      wopt(p) := 2; -- Wrote two samples
                    end if;
                  end if;
                end if;
                tx_fifo_written(p) := true; -- Record that TX FIFO has already been written in FIFO-like mode
              elsif xxx2mss.rnw = '1' and p < nrx then -- FIFO-like read in a implemented RX FIFO
              -- This case is complex too. There may be:
              -- - one or two samples to read,
              -- - zero, one or more samples in the FIFO,
              -- - the target column(s) may have two busy ports or not,
              -- - a higher priority FIFO-like access may prevent this one.
              -- If only one sample can be read while two should be, the first sample is read and the second is delayed until it can be read.
                col := to_i01(fifo2mss.rptrr(p)(0)); -- Column of first sample
                m.area := RX; -- Area indicator for output multiplexor 
                m.idx := p;   -- Indicator of TX/RX pair for output multiplexor
                m.col := col; -- Column indicator for output multiplexor
                if rx_fifo_read(p) or (rx_used(p, col, 0) and rx_used(p, col, 1)) then -- FIFO already read in FIFO-like mode or the two ports of first target column are already busy
                  null; -- Do nothing
                elsif fifo2mss.rokr(p) = '0' then -- Read forbidden (FIFO empty)
                  rx2pss.udf(p) <= '1'; -- Signal underflow
                else -- Read allowed (FIFO not empty, not already read in FIFO-like mode) and at least one free port
                  m.en := '1'; -- Valid response
                  if not rx_used(p, col, 0) then -- Port 0 not used
                    po := 0; -- Use port 0
                  else -- Port 1 not used
                    po := 1; -- Use port 1
                  end if;
                  m.p(col) := po; -- Output port for output multiplexor
                  rx_in_v(p, col, po).we    := X"0";  -- Write enables
                  if xxx2mss.be(7 downto 4) = X"F" then -- Leftmost sample
                    mss2xxx_gnt(7 downto 4) := X"F"; -- Grant access
                    rx_in_v(p, col, po).wdata := (others => '0'); -- Written data
                    m.be(7 downto 4) := X"F"; -- Byte enables for output multiplexor
                  else -- Rightmost sample
                    mss2xxx_gnt(3 downto 0) := X"F"; -- Grant access
                    rx_in_v(p, col, po).wdata := (others => '0'); -- Written data
                    m.be(3 downto 0) := X"F"; -- Byte enables for output multiplexor
                  end if;
                  rx_in_v(p, col, po).add   := drop_lsbs(fifo2mss.rptrr(p), 1); -- -- Address is MSBs of write pointer (the LSB selects the column)
                  rx_in_v(p, col, po).en    := xxx2mss.en; -- Enable RAM access
                  rx_used(p, col, po)       := true; -- Mark port as used
                  ropr(p) := 1; -- Read one sample
                  if xxx2mss.be = X"FF" then -- There is a second sample to read
                    col := to_i01(not fifo2mss.rptrr(p)(0)); -- Column of second sample
                    if rx_used(p, col, 0) and rx_used(p, col, 1) then -- The two ports of second target column are already busy
                      null; -- Do nothing
                    elsif fifo2mss.drokr(p) = '0' then -- Double read forbidden (FIFO empty)
                      rx2pss.udf(p) <= '1'; -- Signal underflow
                    else -- Read allowed (FIFO not empty) and at least one free port
                      if not rx_used(p, col, 0) then -- Port 0 not used
                        po := 0; -- Use port 0
                      else -- Port 1 not used
                        po := 1; -- Use port 1
                      end if;
                      m.p(col) := po; -- Output port for output multiplexor
                      mss2xxx_gnt(3 downto 0) := X"F"; -- The second sample, if any, is always on the 32 LSBs
                      m.be(3 downto 0) := X"F"; -- Byte enables for output multiplexor
                      rx_in_v(p, col, po).we    := X"0"; -- Write enables
                      rx_in_v(p, col, po).wdata := (others => '0'); -- Written data
                      if col = 1 then -- Same address as first sample
                        rx_in_v(p, col, po).add   := drop_lsbs(fifo2mss.rptrr(p), 1);
                      else -- Address of second sample is address of first sample + 1
                        rx_in_v(p, col, po).add   := drop_lsbs(fifo2mss.rptrr1(p), 1);
                      end if;
                      rx_in_v(p, col, po).en    := xxx2mss.en; -- Enable RAM access
                      rx_used(p, col, po)       := true; -- Mark port as used
                      ropr(p) := 2; -- Wrote two samples
                    end if;
                  end if;
                end if;
                rx_fifo_read(p) := true; -- Record that RX FIFO has already been read in FIFO-like mode
  						end if;
  					end if;
  				end loop;
        elsif xxx2mss.add(14 downto 13) = "00" then -- Ramdom accesses in TX/RX FIFOs
          for p in 0 to 3 loop -- For all pairs (synthesis trick)
            if p = pair then -- p is the index of the accessed RX/TX (synthesis trick)
              m.idx      := p; -- Indicator of TX/RX pair for output multiplexor
              m.col := 0; -- Random accesses are always double-word aligned
              if xxx2mss.add(10) = '0' and p < ntx then -- Random read or write in a implemented TX FIFO
                m.area := TX; -- Area indicator for output multiplexor
                for c in 0 to 1 loop -- Two columns
                  if xxx2mss.be(7 - 4 * c downto 4 - 4 * c) /= X"0" and ((not tx_used(p, c, 0)) or (not tx_used(p, c, 1))) then -- Column is accessed and at least one free port
                    if not tx_used(p, c, 0) then -- Port 0 not used
                      po := 0; -- Use port 0
                    else -- Port 1 not used
                      po := 1; -- Use port 1
                    end if;
                    m.p(c) := po; -- Output port for output multiplexor
                    m.en := xxx2mss.rnw; -- Valid response
                    m.be(7 - 4 * c downto 4 - 4 * c) := xxx2mss.be(7 - 4 * c downto 4 - 4 * c); -- Byte enables for output multiplexor
                    tx_used(p, c, po) := true; -- Mark port as used
                    mss2xxx_gnt(7 - 4 * c downto 4 - 4 * c) := xxx2mss.be(7 - 4 * c downto 4 - 4 * c); -- Grant access
                    tx_in_v(p, c, po).en    := xxx2mss.en; -- Enable RAM access
                    tx_in_v(p, c, po).we    := band(xxx2mss.be(7 - 4 * c downto 4 - 4 * c), not xxx2mss.rnw); -- Write enable
                    tx_in_v(p, c, po).wdata := xxx2mss.wdata(63 - 32 * c downto 32 - 32 * c); -- Written data
                    tx_in_v(p, c, po).add  := get_lsbs(xxx2mss.add, 10); -- Address is LSBs of address issued by DMA
                  end if;
                end loop;
              elsif p < nrx then -- Random read or write in a implemented RX FIFO
                m.area := RX; -- Area indicator for output multiplexor
                for c in 0 to 1 loop -- Two columns
                  if xxx2mss.be(7 - 4 * c downto 4 - 4 * c) /= X"0" and ((not rx_used(p, c, 0)) or (not rx_used(p, c, 1))) then -- Column is accessed and at least one free port
                    if not rx_used(p, c, 0) then -- Port 0 not used
                      po := 0; -- Use port 0
                    else -- Port 1 not used
                      po := 1; -- Use port 1
                    end if;
                    m.p(c) := po; -- Output port for output multiplexor
                    m.en := xxx2mss.rnw; -- Valid response
                    m.be(7 - 4 * c downto 4 - 4 * c) := xxx2mss.be(7 - 4 * c downto 4 - 4 * c); -- Byte enables for output multiplexor
                    rx_used(p, c, po) := true; -- Mark port as used
                    mss2xxx_gnt(7 - 4 * c downto 4 - 4 * c) := xxx2mss.be(7 - 4 * c downto 4 - 4 * c); -- Grant access
                    rx_in_v(p, c, po).en    := xxx2mss.en; -- Enable RAM access
                    rx_in_v(p, c, po).we    := band(xxx2mss.be(7 - 4 * c downto 4 - 4 * c), not xxx2mss.rnw); -- Write enable
                    rx_in_v(p, c, po).wdata := xxx2mss.wdata(63 - 32 * c downto 32 - 32 * c); -- Written data
                    rx_in_v(p, c, po).add  := get_lsbs(xxx2mss.add, 10); -- Address is LSBs of address issued by DMA
                  end if;
                end loop;
              end if;
            end if;
          end loop;
        end if;
      end if;
    end procedure dmavci;

  begin

    -------------------------------------------------------
    ------------------ INITIALIZATION ---------------------
    -------------------------------------------------------

    for p in 0 to 1 loop -- Two ports
      ucr_in_v(p) := (en => '0', we => (others => '0'), add => (others => '0'), wdata => (others => '0')); -- UCR RAM
      for i in 0 to 3 loop -- Four TX/RX pairs
        wopt(i) := 0; -- Zero TX writes
        wopr(i) := 0; -- Zero RX writes
        ropt(i) := 0; -- Zero TX reads
        ropr(i) := 0; -- Zero RX reads
        tx_fifo_written(i) := false; -- Indicators of FIFO-like write accesses in TX FIFOs
        rx_fifo_read(i) := false; -- Indicators of FIFO-like read accesses in RX FIFOs
        for c in 0 to 1 loop -- Two columns
          tx_in_v(i, c, p) := (en => '0', we => (others => '0'), add => (others => '0'), wdata => (others => '0')); -- TX input port
          rx_in_v(i, c, p) := (en => '0', we => (others => '0'), add => (others => '0'), wdata => (others => '0')); -- RX input port
          tx_used(i, c, p) := false; -- Mark TX port unused
          rx_used(i, c, p) := false; -- Mark RX port unused
        end loop;
      end loop;
    end loop;

    mss2uc.gnt <= (others => '1');  -- by default, grant UC's request
    mss2uc.oor <= '0';  -- by default we expect UC not to access out of range memory locations

    mux_in_v.pss := (ten => (others => '0'), ren => '0', rcol => 0, tcol => (others => 0), idx => 0);
    mux_in_v.uc := (en => '0', be => (others => '0'));
    mux_in_v.dma := (en => '0', area => TX, idx => 0, p => (others => 0), be => (others => '0'), col => 0);
    mux_in_v.vci := (en => '0', area => TX, idx => 0, p => (others => 0), be => (others => '0'), col => 0);

    mss2fifo <= mss2fifo_none;

    tx2pss.rgnt <= (others => '0');
    tx2pss.wgnt <= '0';
    tx2pss.ovf <= (others => '0');
    tx2pss.udf <= (others => '0');
    rx2pss.ovf <= (others => '0');
    rx2pss.udf <= (others => '0');
    rx2pss.rgnt <= '0';

    po := 0;

    -----------------------------
    -- Accesses to TX/RX FIFOs --
    -----------------------------
    
    -- Accesses by PSS (ADC/DAC and PP interface)
    for i in 0 to 3 loop -- Four TX/RX pairs
      if i < ntx and pss2tx.ren(i) = '1' then -- TXi implemented and DAC read
        col := to_i01(fifo2mss.rptrt(i)(0)); -- Column of next sample to read
        if fifo2mss.rokt(i) = '1' then -- Read allowed (FIFO not empty)
          tx_used(i, col, 0)       := true; -- Use port 0 and mark it as used
          tx2pss.rgnt(i)            <= '1'; -- Grant access
          -- Configure TX RAM input port 0
          tx_in_v(i, col, 0).en    := '1'; -- Enable
          tx_in_v(i, col, 0).we    := (others => '0'); -- Read
          tx_in_v(i, col, 0).add   := drop_lsbs(fifo2mss.rptrt(i), 1); -- Address
          -- Configure multiplexor on RAMs outputs for PSS
          mux_in_v.pss.ten(i)       := '1'; -- Enable
          mux_in_v.pss.tcol(i)      := col; -- Active column
          ropt(i)                  := 1;   -- Read one sample in FIFO-like mode
        else -- FIFO empty
          tx2pss.udf(i) <= '1'; -- Signal underflow
        end if;  
      end if;
      if i < ntx and pss2tx.wen(i) = '1' then -- TXi implemented and PP write
        col := to_i01(fifo2mss.wptrt(i)(0)); -- Column of next sample to write
        if fifo2mss.wokt(i) = '1' then -- Write is allowed (FIFO not full)
          tx_used(i, col, 1)       := true; -- Use port 1 and mark it as used
          tx2pss.wgnt                <= '1'; -- Grant access
          -- Configure TX RAM input port 1
          tx_in_v(i, col, 1).en    := '1'; -- Enable
          tx_in_v(i, col, 1).we    := (others => '1'); -- Write
          tx_in_v(i, col, 1).wdata := pss2tx.wdata; -- Written data
          tx_in_v(i, col, 1).add   := drop_lsbs(fifo2mss.wptrt(i), 1); -- Address
          tx_fifo_written(i)       := true; -- Mark TX FIFO as already written in FIFO-like mode
          wopt(i)                  := 1; -- Wrote one sample in FIFO-like mode
        else -- FIFO full
          tx2pss.ovf(i) <= '1'; -- Signal overflow
        end if;  
      end if;
      if i < nrx and pss2rx.wen(i) = '1' then  -- RXi implemented and ADC write
        col := to_i01(fifo2mss.wptrr(i)(0)); -- Column of next sample to write
        if fifo2mss.wokr(i) = '1' then -- Write allowed (FIFO not full)
          rx_used(i, col, 0)       := true;   -- Use port 0 and mark it as used
          -- Configure RX RAM input port 0
          rx_in_v(i, col, 0).en    := '1'; -- Enable
          rx_in_v(i, col, 0).we    := (others => '1'); -- Write
          rx_in_v(i, col, 0).wdata := pss2rx.wdata(i); -- Written data
          rx_in_v(i, col, 0).add   := drop_lsbs(fifo2mss.wptrr(i), 1); -- Address
          wopr(i)                   := 1; -- Wrote one sample in FIFO-like mode
        else -- FIFO full
          rx2pss.ovf(i) <= '1'; -- Signal overflow
        end if;  
      end if;
      if i < nrx and pss2rx.ren(i) = '1' then -- RXi implemented PP read
        col := to_i01(fifo2mss.rptrr(i)(0)); -- Column of next sample to read
        if fifo2mss.rokr(i) = '1' then -- Read allowed (FIFO not empty)
          rx_used(i, col, 1)       := true; -- Use port 1 and mark it as used
          rx2pss.rgnt                <= '1'; -- Grant access
          rx_in_v(i, col, 1).en    := '1'; -- Enable
          rx_in_v(i, col, 1).we    := (others => '0'); -- Read
          rx_in_v(i, col, 1).wdata := (others => '0'); -- Written data
          rx_in_v(i, col, 1).add   := drop_lsbs(fifo2mss.rptrr(i), 1); -- Address
          rx_fifo_read(i)       := true; -- Mark RX FIFO as already read in FIFO-like mode
          ropr(i)                   := 1; -- Read one sample in FIFO-like mode
          mux_in_v.pss.idx           := i; -- Index of read RX FIFO
          mux_in_v.pss.rcol         := col; -- Read column
          mux_in_v.pss.ren           := '1'; -- Enable
        else -- FIFO empty
          rx2pss.udf(i) <= '1'; -- Signal underflow
        end if;  
      end if;
    end loop;

    dmavci(xxx2mss => dma2mss, mss2xxx_oor => tmp_oor, mss2xxx_gnt => tmp_gnt, m => mux_in_v.dma); -- DMA accesses to TX/RX FIFOs
    mss2dma.oor <= tmp_oor;
    mss2dma.gnt <= tmp_gnt;
    dmavci(xxx2mss => vci2mss, mss2xxx_oor => tmp_oor, mss2xxx_gnt => tmp_gnt, m => mux_in_v.vci); -- VCI accesses to TX/RX FIFOs
    mss2vci.oor <= tmp_oor;
    mss2vci.gnt <= tmp_gnt;

    mss2fifo.wopr <= wopr;
    mss2fifo.ropr <= ropr;
    mss2fifo.wopt <= wopt;
    mss2fifo.ropt <= ropt;

    -- UCR accesses

    uc_ucr_access  := uc2mss.en = '1' and uc2mss.add(15 downto 12) = "0000";
    dma_ucr_access := dma2mss.en = '1' and dma2mss.add(14 downto 13) = "10";
    vci_ucr_access := vci2mss.en = '1' and vci2mss.add(14 downto 13) = "10";

    -- Uc oor
    uc_oor := or_reduce(uc2mss.add(15 downto 12));
    if uc_oor = '1' then
      mss2uc.oor <= '1';
      -- pragma translate_off
      assert uc2mss.en = '0'
        report "UC: out of range memory access (" & integer'image(to_integer(u_unsigned(uc2mss.add))) & ")"
        severity error;
      -- pragma translate_on
    end if;

    if uc_ucr_access then -- if UC read/write channel enabled and access to the UCR area
      mss2uc.gnt        <= (others => '1'); -- grant the access
      -- Port A
      ucr_in_v(0).en    := '1';
      ucr_in_v(0).we    := band((not uc2mss.rnw), uc2mss.be(7 downto 4));
      ucr_in_v(0).add   := uc2mss.add(11 downto 3) & '0';
      ucr_in_v(0).wdata := uc2mss.wdata(63 downto 32);
      -- Port B
      ucr_in_v(1).en    := '1';
      ucr_in_v(1).we    := band((not uc2mss.rnw), uc2mss.be(3 downto 0));
      ucr_in_v(1).add   := uc2mss.add(11 downto 3) & '1';
      ucr_in_v(1).wdata := uc2mss.wdata(31 downto 0);
      mux_in_v.uc.be    := uc2mss.be;
    elsif dma_ucr_access then --UCR access
      mux_in_v.dma.area := UCR;
      mux_in_v.dma.be   := dma2mss.be;
      mux_in_v.dma.en   := '1';
      mss2dma.gnt       <= (others => '1');
      -- port a
      ucr_in_v(0).en    := '1';
      ucr_in_v(0).we    := band((not dma2mss.rnw), dma2mss.be(7 downto 4));
      ucr_in_v(0).wdata := dma2mss.wdata(63 downto 32);
      ucr_in_v(0).add   := dma2mss.add(8 downto 0) & '0';
      -- port b
      ucr_in_v(1).en    := '1';
      ucr_in_v(1).we    := band((not dma2mss.rnw), dma2mss.be(3 downto 0));
      ucr_in_v(1).wdata := dma2mss.wdata(31 downto 0);
      ucr_in_v(1).add   := dma2mss.add(8 downto 0) & '1';
    elsif vci_ucr_access then --UCR access
      mux_in_v.vci.area := UCR;
      mss2vci.gnt    <= (others => '1');
      mux_in_v.vci.be   := vci2mss.be;
      mux_in_v.vci.en   := '1';
      -- Port A
      ucr_in_v(0).en    := '1';
      ucr_in_v(0).we    := band((not vci2mss.rnw), vci2mss.be(7 downto 4));
      ucr_in_v(0).wdata := vci2mss.wdata(63 downto 32);
      ucr_in_v(0).add   := vci2mss.add(8 downto 0) & '0';
      -- Port B
      ucr_in_v(1).en    := '1';
      ucr_in_v(1).we    := band((not vci2mss.rnw), vci2mss.be(3 downto 0));
      ucr_in_v(1).wdata := vci2mss.wdata(31 downto 0);
      ucr_in_v(1).add   := vci2mss.add(8 downto 0) & '1';
    end if;

    tx_in  <= tx_in_v;
    rx_in  <= rx_in_v;
    ucr_in <= ucr_in_v;
    
    mux_in <= mux_in_v;

  end process arbiter;

  mss2rams_pipe(1) <= (tx => tx_in, rx => rx_in, ucr => ucr_in);
  gn0: if n0 > 1 generate
    process(clk)
    begin
      if rising_edge(clk) then
        mss2rams_pipe(2 to n0) <= mss2rams_pipe(1 to n0 - 1);
      end if;
    end process;
  end generate gn0;
  rams2mss_pipe(1) <= rams2mss;
  gn1: if n1 > 1 generate
    process(clk)
    begin
      if rising_edge(clk) then
        rams2mss_pipe(2 to n1) <= rams2mss_pipe(1 to n1 - 1);
      end if;
    end process;
  end generate gn1;

  -- The regs process implements the sequential part:
  -- + registers to store the commands of the output multiplexers that select
  --   the "rdata" fields of UC, DMA and VCI and the read channels of PSS
  -- + input and output pipeline stages around RAMs (but last input and first output which are part of RAMs)
  regs: process(clk)
  begin

    if rising_edge(clk) then
      mux_pipe <= mux_in & mux_pipe(1 to n0 + n1 - 1);
    end if;
  end process regs;

  mss2rams <= mss2rams_pipe(n0);

  mux <= mux_pipe(n0 + n1);

  ucr_out <= rams2mss_pipe(n1).ucr;
  tx_out <= rams2mss_pipe(n1).tx;
  rx_out <= rams2mss_pipe(n1).rx;

  -- The muxes process implements the multiplexing of block RAM outputs
  muxes: process(ucr_out, tx_out, rx_out, mux)
  begin

    -- PSS read data mux
    rx2pss.en    <= mux.pss.ren;
    rx2pss.rdata <= rx_out(mux.pss.idx, mux.pss.rcol, 1);
    tx2pss.en    <= mux.pss.ten;
    for i in 0 to 3 loop
      tx2pss.rdata(i) <= tx_out(i, mux.pss.tcol(i), 0);
    end loop;

     -- UC read data mux

    mss2uc.rdata <= ucr_out(0) & ucr_out(1);
    mss2uc.be    <= mux.uc.be;

    -- DMA read data mux
    
    if mux.dma.area = RX then 
      if mux.dma.col = 0 then  
        mss2dma.rdata <= rx_out(mux.dma.idx, 0, mux.dma.p(0)) & rx_out(mux.dma.idx, 1, mux.dma.p(1));
      else
        mss2dma.rdata <= rx_out(mux.dma.idx, 1, mux.dma.p(1)) & rx_out(mux.dma.idx, 0, mux.dma.p(0));
      end if;
    elsif mux.dma.area = TX then 
      if mux.dma.col = 0 then 
        mss2dma.rdata <= tx_out(mux.dma.idx, 0, mux.dma.p(0)) & tx_out(mux.dma.idx, 1, mux.dma.p(1));
      else
        mss2dma.rdata <= tx_out(mux.dma.idx, 1, mux.dma.p(1)) & tx_out(mux.dma.idx, 0, mux.dma.p(0));
      end if;
    else
      mss2dma.rdata  <= ucr_out(0) & ucr_out(1);
    end if;
    mss2dma.be <= mux.dma.be;
    mss2dma.en <= mux.dma.en;

    -- VCI read data mux
    
    if mux.vci.area = RX then  
      if mux.vci.col = 0 then  
        mss2vci.rdata <= rx_out(mux.vci.idx, 0, mux.vci.p(0)) & rx_out(mux.vci.idx, 1, mux.vci.p(1));
      else
        mss2vci.rdata <= rx_out(mux.vci.idx, 1, mux.vci.p(1)) & rx_out(mux.vci.idx, 0, mux.vci.p(0));
      end if;
    elsif mux.vci.area = TX then 
      if mux.vci.col = 0 then 
        mss2vci.rdata <= tx_out(mux.vci.idx, 0, mux.vci.p(0)) & tx_out(mux.vci.idx, 1, mux.vci.p(1));
      else
        mss2vci.rdata <= tx_out(mux.vci.idx, 1, mux.vci.p(1)) & tx_out(mux.vci.idx, 0, mux.vci.p(0));
      end if;
    else
      mss2vci.rdata  <= ucr_out(0) & ucr_out(1);
    end if;
    mss2vci.be <= mux.vci.be;
    mss2vci.en <= mux.vci.en;

  end process;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
