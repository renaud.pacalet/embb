--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief ADAIF_NO_RAMS top level without RAMs

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;

library css_lib;
use css_lib.css_pkg.all;

use work.bitfield.all; -- defines bitfield_width and bitfield_padmask constants
use work.adaif_pkg.all;  -- defines the custom pss2mss_type and mss2pss_type types

entity adaif_no_rams is
  generic(
-- pragma translate_off
    debug:    boolean  := true;   --* Print debug information
    verbose:  boolean  := false;  --* Print more debug information
-- pragma translate_on
    with_uc:  boolean := true;        -- With or without UC
    nrx:      natural range 0 to 4 := 4;  -- Number of RX chains
    ntx:      natural range 0 to 4 := 4; -- Number of TX chains
    txdepth:  positive := 2;          -- Depth of TX synchronizers
    rxdepth:  positive := 2;          -- Depth of RX synchronizers
    n0:       positive := 1;
    n1:       positive := 1);
port(
-- pragma translate_off
    dbg:      out debug_type;    -- debug out
-- pragma translate_on
    nsamples:    out std_ulogic;  -- RP DEBUG RP
    clk:      in  std_ulogic;  -- master clock
    srstn:    in  std_ulogic;  -- master reset
    ce:       in  std_ulogic;  -- chip enable
    hirq:     in  std_ulogic;  -- host input irq,
    pirq:     out std_ulogic;  -- PSS output irq,
    dirq:     out std_ulogic;  -- DMA output irq,
    uirq:     out std_ulogic;  -- UC output irq,
    eirq:     out std_ulogic_vector(7 downto 0); -- Extended output irq,
    pp2adaif: in  pp2adaif_type;  -- PreProcessor input
    adaif2pp: out adaif2pp_type;  -- PreProcessor output
    adac2adaif: in  adac2adaif_type; 
    adaif2adac: out adaif2adac_type;
    tvci_in:  in  vci_i2t_type;  -- Target AVCI input
    tvci_out: out vci_t2i_type; -- Target AVCI output
    ivci_in:  in  vci_t2i_type;  -- Initiator AVCI input
    ivci_out: out vci_i2t_type;  -- Initiator AVCI output
    rams2mss: in  rams2mss_type;  -- Inputs from external RAMs
    mss2rams: out mss2rams_type); -- Outputs to external RAMs
end entity adaif_no_rams;

architecture rtl of adaif_no_rams is

  signal css2pss:   css2pss_type;
  signal pss2css:   pss2css_type;
  signal param:    std_ulogic_vector(bitfield_width - 1 downto 0);
  signal pss2mss:   pss2mss_type;
  signal mss2pss:   mss2pss_type;
  signal css2mss:  css2mss_type;
  signal mss2css:  mss2css_type;
  signal fifo2mss: fifo2mss_type;
  signal mss2fifo: mss2fifo_type;

begin

-- pragma translate_off
    dbg.param  <= param;
    dbg.eoc    <= pss2css.eoc;
    dbg.data   <= pss2css.data;
    dbg.err    <= pss2css.err;
    dbg.status <= pss2css.status;
    dbg.eirq   <= pss2css.eirq(7 downto 0);
-- pragma translate_on
  
  i_pss: entity work.pss(rtl)
  generic map(
    with_uc => with_uc,
    nrx     => nrx,
    ntx     => ntx,
    txdepth => txdepth,
    rxdepth => rxdepth)
  port map(
-- pragma translate_off
    tts     => dbg.tts,
    trs     => dbg.trs,
    iut     => dbg.iut,
    iot     => dbg.iot,
    iur     => dbg.iur,
    ior     => dbg.ior,
    lvlr    => dbg.lvlr,
    lvlt    => dbg.lvlt,
-- pragma translate_on
    nsamples    => nsamples,  -- RP DEBUG RP
    clk      => clk,
    css2pss  => css2pss,
    param    => param,
    pss2css  => pss2css,
    fifo2mss => fifo2mss,
    mss2fifo => mss2fifo,
    pp2adaif => pp2adaif,
    adaif2pp => adaif2pp,
    adac2adaif => adac2adaif,
    adaif2adac => adaif2adac,
    pss2mss   => pss2mss,
    mss2pss   => mss2pss);


  i_css: entity css_lib.css(rtl)
  generic map(
-- pragma translate_off
    debug     => debug,
    verbose   => verbose,
    name      => "ADAIF",
-- pragma translate_on
    n_pa_regs => bitfield_width / 64,
    pa_rmask => bitfield_rpadmask,
    pa_wmask => bitfield_wpadmask,
    with_uc  => with_uc,
    n0       => n0,
    n1       => n1,
    neirq    => 8)
  port map(
    clk            => clk,
    srstn          => srstn,
    ce             => ce,
    hirq           => hirq,
    uirq           => uirq,
    pirq           => pirq,
    dirq           => dirq,
    eirq           => eirq,
    pss2css         => pss2css,
    css2pss         => css2pss,
    param          => param,
    mss2css        => mss2css,
    css2mss        => css2mss,
    tvci_in        => tvci_in,
    tvci_out       => tvci_out,
    ivci_in        => ivci_in,
    ivci_out       => ivci_out);

  i_mss: entity work.mss(rtl)
  generic map(
    with_uc  => with_uc,
    nrx      => nrx,
    ntx      => nrx,
    n0       => n0,
    n1       => n1)
  port map(
    clk       => clk,
    css2mss   => css2mss,
    mss2css   => mss2css,
    fifo2mss  => fifo2mss,
    mss2fifo  => mss2fifo,
    pss2mss    => pss2mss,
    mss2pss    => mss2pss,
    rams2mss  => rams2mss,
    mss2rams  => mss2rams);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
