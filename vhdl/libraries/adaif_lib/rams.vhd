--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief RAMS

library ieee;
use ieee.std_logic_1164.all;

library memories_lib;
use memories_lib.ram_pkg.all;

use work.adaif_pkg.all;

entity rams is
  generic(with_uc: boolean := true;        -- With or without UC
          nrx: natural range 0 to 4 := 4;  -- Number of RX chains
          ntx: natural range 0 to 4 := 4); -- Number of TX chains
  port(clk:      in  std_ulogic;
       mss2rams: in  mss2rams_type;
       rams2mss: out rams2mss_type);
end entity rams;

architecture rtl of rams is

  signal ucr_out: ucr_out_type;
  signal rx_out: rx_out_type;
  signal tx_out: tx_out_type;

begin

  ucr_g: if with_uc generate
    ram_ucr: entity memories_lib.tdpram(arc)
      generic map(
  -- pragma translate_off
                  init_file     => "", -- initialization file
                  si            => 0,  -- first address of initialization file
                  instance_name => "ucr", -- Name of the RAM instance.
  -- pragma translate_on
                  registered => true,  -- control output registers
                  na         => 10,    -- bit-width of address busses
                  nb         => 4)     -- byte-width of data busses
      port map(clk   => clk,
               ena   => mss2rams.ucr(0).en,
               enb   => mss2rams.ucr(1).en,
               wea   => mss2rams.ucr(0).we,
               web   => mss2rams.ucr(1).we,
               adda  => mss2rams.ucr(0).add,
               addb  => mss2rams.ucr(1).add,
               dina  => mss2rams.ucr(0).wdata,
               dinb  => mss2rams.ucr(1).wdata,
               douta => ucr_out(0),
               doutb => ucr_out(1));
  end generate ucr_g;

  rams2mss.ucr <= ucr_out when with_uc else
                  (others => (others => '0'));

  irx_fifo: for i in 0 to nrx - 1 generate
    jrx_fifo: for j in 0 to 1 generate
      ram_rx: entity memories_lib.tdpram(arc)
        generic map(
-- pragma translate_off
                    init_file     => "", -- initialization file
                    si            => 0,  -- first address of initialization file
                    instance_name => "RX fifo" & integer'image(i), -- Name of the RAM instance.
-- pragma translate_on
                    registered => true,  -- control output registers
                    na         => fifo_ptr_width - 1, -- bit-width of address busses
                    nb         => 4)  -- byte-width of data busses
          port map(clk => clk,
                   ena   => mss2rams.rx(i, j, 0).en,
                   enb   => mss2rams.rx(i, j, 1).en,
                   wea   => mss2rams.rx(i, j, 0).we,
                   web   => mss2rams.rx(i, j, 1).we,
                   adda  => mss2rams.rx(i, j, 0).add,
                   addb  => mss2rams.rx(i, j, 1).add,
                   dina  => mss2rams.rx(i, j, 0).wdata,
                   dinb  => mss2rams.rx(i, j, 1).wdata,
                   douta => rx_out(i, j, 0),
                   doutb => rx_out(i, j, 1));
    end generate jrx_fifo;
  end generate irx_fifo;

  rams2mss_rx_g: for i in 0 to 3 generate
    gil: if i < nrx generate
      gj: for j in 0 to 1 generate
        rams2mss.rx(i, j, 0) <= rx_out(i, j, 0);
        rams2mss.rx(i, j, 1) <= rx_out(i, j, 1);
      end generate gj;
    end generate gil;
    gih: if i >= nrx generate
      gj: for j in 0 to 1 generate
        rams2mss.rx(i, j, 0) <= (others => '0');
        rams2mss.rx(i, j, 1) <= (others => '0');
      end generate gj;
    end generate gih;
  end generate rams2mss_rx_g;

  itx_fifo: for i in 0 to ntx - 1 generate
    jtx_fifo: for j in 0 to 1 generate
      ram_tx: entity memories_lib.tdpram(arc)
        generic map(
-- pragma translate_off
                    init_file     => "", -- initialization file
                    si            => 0,  -- first address of initialization file
                    instance_name => "TX fifo" & integer'image(i), -- Name of the RAM instance.
-- pragma translate_on
                    registered => true,  -- control output registers
                    na         => fifo_ptr_width - 1, -- bit-width of address busses
                    nb         => 4)  -- byte-width of data busses
          port map(clk => clk,
                   ena   => mss2rams.tx(i, j, 0).en,
                   enb   => mss2rams.tx(i, j, 1).en,
                   wea   => mss2rams.tx(i, j, 0).we,
                   web   => mss2rams.tx(i, j, 1).we,
                   adda  => mss2rams.tx(i, j, 0).add,
                   addb  => mss2rams.tx(i, j, 1).add,
                   dina  => mss2rams.tx(i, j, 0).wdata,
                   dinb  => mss2rams.tx(i, j, 1).wdata,
                   douta => tx_out(i, j, 0),
                   doutb => tx_out(i, j, 1));
    end generate jtx_fifo;
  end generate itx_fifo;

  rams2mss_tx_g: for i in 0 to 3 generate
    gil: if i < ntx generate
      gj: for j in 0 to 1 generate
        rams2mss.tx(i, j, 0) <= tx_out(i, j, 0);
        rams2mss.tx(i, j, 1) <= tx_out(i, j, 1);
      end generate gj;
    end generate gil;
    gih: if i >= ntx generate
      gj: for j in 0 to 1 generate
        rams2mss.tx(i, j, 0) <= (others => '0');
        rams2mss.tx(i, j, 1) <= (others => '0');
      end generate gj;
    end generate gih;
  end generate rams2mss_tx_g;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
