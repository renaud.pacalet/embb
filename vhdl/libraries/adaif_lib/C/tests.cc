/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <fcntl.h>
#include <unistd.h>

#include <embb/adaif_regs.h>
#include <embb/memory.h>
#include <assert.h>
#include <stdint.h>
#include <utils.h>
#include <tests.h>

#define INDATA_SIZE 32

#define DEBUG(...) fprintf(stderr, __VA_ARGS__);

#define ASSERT_EQ(a, b)							\
  do {									\
    typeof(a) a_ = a;							\
    typeof(a) b_ = b;							\
    if (a_ != b_) {							\
      DEBUG("error:%u:%s:%s %s equals to 0x%x, expected 0x%x\n", __LINE__, __FILE__, __func__, #a, a_, b_); \
      abort();								\
    }									\
  } while (0)

// Human-readable print of parameters
void print_params(FILE *f, ADAIF_CONTEXT *ctx) {
  fprintf(f, "# tdd=%01llx\n", adaif_get_tdd(ctx));
  fprintf(f, "# st=%04llx\n", adaif_get_st(ctx));
  fprintf(f, "# ldt0=   0x%08llx, ldt1=   0x%08llx, ldt2=   0x%08llx, ldt3=   0x%08llx\n", adaif_get_ldt0(ctx), adaif_get_ldt1(ctx), adaif_get_ldt2(ctx), adaif_get_ldt3(ctx));
  fprintf(f, "# startt0=0x%08llx, startt1=0x%08llx, startt2=0x%08llx, startt3=0x%08llx\n", adaif_get_startt0(ctx), adaif_get_startt1(ctx), adaif_get_startt2(ctx), adaif_get_startt3(ctx));
  fprintf(f, "# stopt0= 0x%08llx, stopt1= 0x%08llx, stopt2= 0x%08llx, stopt3= 0x%08llx\n", adaif_get_stopt0(ctx), adaif_get_stopt1(ctx), adaif_get_stopt2(ctx), adaif_get_stopt3(ctx));
  fprintf(f, "# ldr0=   0x%08llx, ldr1=   0x%08llx, ldr2=   0x%08llx, ldr3=   0x%08llx\n", adaif_get_ldr0(ctx), adaif_get_ldr1(ctx), adaif_get_ldr2(ctx), adaif_get_ldr3(ctx));
  fprintf(f, "# startr0=0x%08llx, startr1=0x%08llx, startr2=0x%08llx, startr3=0x%08llx\n", adaif_get_startr0(ctx), adaif_get_startr1(ctx), adaif_get_startr2(ctx), adaif_get_startr3(ctx));
  fprintf(f, "# stopr0= 0x%08llx, stopr1= 0x%08llx, stopr2= 0x%08llx, stopr3= 0x%08llx\n", adaif_get_stopr0(ctx), adaif_get_stopr1(ctx), adaif_get_stopr2(ctx), adaif_get_stopr3(ctx));
  fprintf(f, "# dst=%02llx, src=%02llx, op=%01llx, data=0x%08llx\n", adaif_get_dst(ctx), adaif_get_src(ctx), adaif_get_op(ctx), adaif_get_data(ctx));
}

// Print execute command
void print_ex(FILE *f, ADAIF_CONTEXT *ctx, int rdata) {
  fprintf(f, "EX 0 0 %08llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n", 
     rdata, ctx->params.hrop, ctx->params.bndr3, ctx->params.bndt3, ctx->params.bndr2, ctx->params.bndt2, ctx->params.bndr1, ctx->params.bndt1,
      ctx->params.bndr0, ctx->params.bndt0, ctx->params.ld3, ctx->params.ld2, ctx->params.ld1, ctx->params.ld0, ctx->params.cfg);
}

// Change execute param
 void change_param(FILE *f, ADAIF_CONTEXT *ctx) {
  fprintf(f, "VW fff08 13 0 ff \n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n%016llx\n", 
      ctx->params.ld0, ctx->params.ld1, ctx->params.ld2, ctx->params.ld3, ctx->params.bndt0, ctx->params.bndr0,
      ctx->params.bndt1, ctx->params.bndr1, ctx->params.bndt2, ctx->params.bndr2, ctx->params.bndt3, ctx->params.bndr3, ctx->params.hrop);
  fprintf(f, "VW fff00 1 0 ff \n%016llx\n", ctx->params.cfg);
}

// randomly generates context ctx. min and max are two adaif_params structures expressing constraints on the random generation and must be initialized prior
// calling randctx. Each field is randomly thrown between its corresponding fields in min and max, included. Packed fields (tdd, st) are built piece by piece. When
// randomly picked, each field is guaranteed to have a valid value with respect to the other ctx fields.
void randctx(ADAIF_CONTEXT *ctx, adaif_params min, adaif_params max)
{
  uint32_t i, tmp, v1, v2;

  for(i = 0, tmp = 0, v1 = min.tdd, v2 = max.tdd; i < 4; i++, v1 >>= 1, v2 >>= 1) {
    tmp |= (do_rand_min_max(v1 & 0x1, v2 & 0x1)) << i;
  }
  adaif_set_tdd(ctx, tmp);

  for(i = 0, tmp = 0, v1 = min.st, v2 = max.st; i < 8; i++, v1 >>= 2, v2 >>= 2) {
    tmp |= (do_rand_min_max(v1 & 0x3, v2 & 0x3)) << 2 * i;
  }
  adaif_set_st(ctx, tmp);

  tmp = (do_rand_min_max(min.ldt0, max.ldt0));
  adaif_set_ldt0(ctx, tmp);
  tmp = (do_rand_min_max(min.ldr0, max.ldr0));
  adaif_set_ldr0(ctx, tmp);
  tmp = (do_rand_min_max(min.ldt1, max.ldt1));
  adaif_set_ldt1(ctx, tmp);
  tmp = (do_rand_min_max(min.ldr1, max.ldr1));
  adaif_set_ldr1(ctx, tmp);
  tmp = (do_rand_min_max(min.ldt2, max.ldt2));
  adaif_set_ldt2(ctx, tmp);
  tmp = (do_rand_min_max(min.ldr2, max.ldr2));
  adaif_set_ldr2(ctx, tmp);
  tmp = (do_rand_min_max(min.ldt3, max.ldt3));
  adaif_set_ldt3(ctx, tmp);
  tmp = (do_rand_min_max(min.ldr3, max.ldr3));
  adaif_set_ldr3(ctx, tmp);

  tmp = (do_rand_min_max(min.startt0, max.startt0));
  adaif_set_startt0(ctx, tmp);
  tmp = (do_rand_min_max(min.stopt0, max.stopt0));
  adaif_set_stopt0(ctx, tmp);
  tmp = (do_rand_min_max(min.startr0, max.startr0));
  adaif_set_startr0(ctx, tmp);
  tmp = (do_rand_min_max(min.stopr0, max.stopr0));
  adaif_set_stopr0(ctx, tmp);
  tmp = (do_rand_min_max(min.startt1, max.startt1));
  adaif_set_startt1(ctx, tmp);
  tmp = (do_rand_min_max(min.stopt1, max.stopt1));
  adaif_set_stopt1(ctx, tmp);
  tmp = (do_rand_min_max(min.startr1, max.startr1));
  adaif_set_startr1(ctx, tmp);
  tmp = (do_rand_min_max(min.stopr1, max.stopr1));
  adaif_set_stopr1(ctx, tmp);
  tmp = (do_rand_min_max(min.startt2, max.startt2));
  adaif_set_startt2(ctx, tmp);
  tmp = (do_rand_min_max(min.stopt2, max.stopt2));
  adaif_set_stopt2(ctx, tmp);
  tmp = (do_rand_min_max(min.startr2, max.startr2));
  adaif_set_startr2(ctx, tmp);
  tmp = (do_rand_min_max(min.stopr2, max.stopr2));
  adaif_set_stopr2(ctx, tmp);
  tmp = (do_rand_min_max(min.startt3, max.startt3));
  adaif_set_startt3(ctx, tmp);
  tmp = (do_rand_min_max(min.stopt3, max.stopt3));
  adaif_set_stopt3(ctx, tmp);
  tmp = (do_rand_min_max(min.startr3, max.startr3));
  adaif_set_startr3(ctx, tmp);
  tmp = (do_rand_min_max(min.stopr3, max.stopr3));
  adaif_set_stopr3(ctx, tmp);

  tmp = (do_rand_min_max(min.src, max.src));
  adaif_set_src(ctx, tmp);
  tmp = (do_rand_min_max(min.dst, max.dst));
  adaif_set_dst(ctx, tmp);
  tmp = (do_rand_min_max(min.op, max.op));
  adaif_set_op(ctx, tmp);
  tmp = (do_rand_min_max(min.data, max.data));
  adaif_set_data(ctx, tmp);
}

// Run one random test
void mktests(FILE *f, ADAIF_CONTEXT *ctx, adaif_params min, adaif_params max)
{
  uint32_t tmp, i , src, dst, da, db;
  uint32_t ld, a, b;

  uint32_t rd0[2048];
  uint32_t rdr = 0xFFFF;
  uint32_t rdi = 0xFFFF;

  //Generate rd0 
  for (i = 0; i<2048; i++)
  {
    rd0[i] = (rdr << 16) | rdi;
    if (rdr == 0xFFFF)
      rdr = 0;
    else
      rdr += 1;
    if (rdi == 1)
      rdi = 0xFFFF;
    else
      rdi -= 1;
  }

  randctx(ctx, min, max);
  print_params(f, ctx);

  //Basic configuration 
  adaif_set_st(ctx, 0);
  adaif_set_tdd(ctx, 0);
  adaif_set_ldt0(ctx, 10);
  adaif_set_startt0(ctx, 5);
  adaif_set_stopt0(ctx, 5);
  adaif_set_ldt1(ctx, 10);
  adaif_set_startt1(ctx, 5);
  adaif_set_stopt1(ctx, 5);
  adaif_set_ldt2(ctx, 10);
  adaif_set_startt2(ctx, 5);
  adaif_set_stopt2(ctx, 5);
  adaif_set_ldt3(ctx, 10);
  adaif_set_startt3(ctx, 5);
  adaif_set_stopt3(ctx, 5);
  adaif_set_startr0(ctx, 5);
  adaif_set_stopr0(ctx, 5);
  adaif_set_ldr1(ctx, 10);
  adaif_set_startr1(ctx, 5);
  adaif_set_stopr1(ctx, 5);
  adaif_set_ldr2(ctx, 10);
  adaif_set_startr2(ctx, 5);
  adaif_set_stopr2(ctx, 5);
  adaif_set_ldr3(ctx, 10);
  adaif_set_startr3(ctx, 5);
  adaif_set_stopr3(ctx, 5);

  change_param(f, ctx);
  fprintf(f, "WV 1000\n");

  for (i=0;i<100;i++)
  {
    a = do_rand() % 0x8000;
    dst = do_rand() % 24;
    //set a @ dst
    adaif_set_op(ctx, 1);
    adaif_set_data(ctx, a);
    adaif_set_dst(ctx, dst);
    fprintf(f, "# SET %d in HREG[%d]\n", a, dst);
    adaif_start(ctx);
    print_ex(f, ctx, a);
    fprintf(f, "WE 10000\n");
    //get data @ dst
    src = dst;
    adaif_set_src(ctx, src);
    adaif_set_op(ctx, 0);
    fprintf(f, "# GET data in HREG[%d]\n", src);
    adaif_start(ctx);
    print_ex(f, ctx, a);
    fprintf(f, "WE 10000\n");
    //copy data from src to dst
    src = dst;
    dst = do_rand() % 24;
    adaif_set_src(ctx, src);
    adaif_set_dst(ctx, dst);
    adaif_set_op(ctx, 3);
    fprintf(f, "# COPY data from HREG[%d] to HREG[%d]\n", src, dst);
    adaif_start(ctx);
    print_ex(f, ctx, a);
    fprintf(f, "WE 10000\n");
    //get data @ dst
    src = dst;
    adaif_set_src(ctx, src);
    adaif_set_op(ctx, 0);
    fprintf(f, "# GET data in HREG[%d]\n", src);
    adaif_start(ctx);
    print_ex(f, ctx, a);
    //Add
    a = do_rand()% 0x4000;
    b = do_rand()% 0x4000;
    dst = do_rand() % 24;
    //set a @ dst
    adaif_set_op(ctx, 1);
    adaif_set_data(ctx, a);
    adaif_set_dst(ctx, dst);
    fprintf(f, "# SET %d in HREG[%d]\n", a, dst);
    adaif_start(ctx);
    print_ex(f, ctx, a);
    fprintf(f, "WE 10000\n");
    //add a and b
    src = dst;
    dst = do_rand() % 24;
    adaif_set_src(ctx, src);
    adaif_set_dst(ctx, dst);
    adaif_set_op(ctx, 2);
    adaif_set_data(ctx, b);
    fprintf(f, "# Add %d in HREG[%d] and %d store in HREG[%d]\n", a, src, b, dst);
    adaif_start(ctx);
    print_ex(f, ctx, a + b);
    fprintf(f, "WE 10000\n");
    //check data @ dst
    src = dst;
    adaif_set_src(ctx, src);
    adaif_set_op(ctx, 0);
    fprintf(f, "# GET data in HREG[%d]\n", src);
    adaif_start(ctx);
    print_ex(f, ctx, a + b);
  }

  fprintf(f, "# Reset\n");
  fprintf(f, "RS 10\n");
  fprintf(f, "VW FFFA8 1 0 ff\n");
  fprintf(f, "000000000003ff3f\n");

  //FDD

  //Basic configuration 
  adaif_set_st(ctx, 0);
  adaif_set_tdd(ctx, 0);
  adaif_set_ldt0(ctx, 10);
  adaif_set_startt0(ctx, 5);
  adaif_set_stopt0(ctx, 5);
  adaif_set_ldt1(ctx, 10);
  adaif_set_startt1(ctx, 5);
  adaif_set_stopt1(ctx, 5);
  adaif_set_ldt2(ctx, 10);
  adaif_set_startt2(ctx, 5);
  adaif_set_stopt2(ctx, 5);
  adaif_set_ldt3(ctx, 10);
  adaif_set_startt3(ctx, 5);
  adaif_set_stopt3(ctx, 5);
  adaif_set_startr0(ctx, 5);
  adaif_set_stopr0(ctx, 5);
  adaif_set_ldr0(ctx, 10);
  adaif_set_startr0(ctx, 5);
  adaif_set_ldr1(ctx, 10);
  adaif_set_startr1(ctx, 5);
  adaif_set_stopr1(ctx, 5);
  adaif_set_ldr2(ctx, 10);
  adaif_set_startr2(ctx, 5);
  adaif_set_stopr2(ctx, 5);
  adaif_set_ldr3(ctx, 10);
  adaif_set_startr3(ctx, 5);
  adaif_set_stopr3(ctx, 5);

  change_param(f, ctx);
  fprintf(f, "WV 1000\n");
  fprintf(f, "WC 100\n");

  //RX0 is filled by DMA
  
  //Write data in RXO with DMA
  fprintf(f, "DW %llx %d 1 ff\n", 0x10008, 1024);
  dump_buffer(f, (uint8_t*)rd0, 2048, 4, 0, 1, 8);
  fprintf(f, "WD 50000\n");
  
  //Read data in RX0 with DMA
  fprintf(f, "CR %llx %d ff\n", 0x10008/8, 1024);
  dump_buffer(f, (uint8_t*)rd0, 2048, 4, 0, 1, 8);
  fprintf(f, "WD 50000\n");
  
  //FDD

  fprintf(f, "# Reset\n");
  fprintf(f, "RS 10\n");
  fprintf(f, "VW 1fff5 1 ff\n");
  fprintf(f, "000000000003ff3f\n");

  //Basic configuration 
  adaif_set_st(ctx, 0);
  adaif_set_tdd(ctx, 0);

  change_param(f, ctx);
  fprintf(f, "WV 1000\n");
  fprintf(f, "WC 1000\n");
  //ENABLE RX 0
  adaif_set_st(ctx, 1 << 2);

  change_param(f, ctx);
  fprintf(f, "WV 1000\n");
  fprintf(f, "WC 10000\n");
  
  //IDLE  RX 0
  adaif_set_st(ctx, 2 << 2);

  change_param(f, ctx);

  fprintf(f, "WV 1000\n");
  fprintf(f, "WC 5000\n");
  
  //TDD  
  
  fprintf(f, "# Reset\n");
  fprintf(f, "RS 10\n");
  fprintf(f, "VW 1fff5 1 ff\n");
  fprintf(f, "000000000003ff3f\n");

  //Basic configuration 
  adaif_set_st(ctx, 0);
  adaif_set_tdd(ctx, 0);

  change_param(f, ctx);
  fprintf(f, "WV 1000\n");
  fprintf(f, "WC 1000\n");

  //Enable RX 
  adaif_set_st(ctx, 1 << 2);
  adaif_set_tdd(ctx, 1);
  adaif_set_ldr0(ctx, 10);
  tmp = 8;
  adaif_set_startr0(ctx, tmp);
  adaif_set_stopr0(ctx, 2);

  change_param(f, ctx);
  fprintf(f, "WV 1000\n");
  fprintf(f, "WC 50000\n");
 
  fprintf(f, "# Reset\n");
  fprintf(f, "RS 10\n");
  fprintf(f, "VW 1fff5 1 ff\n");
  fprintf(f, "000000000003ff3f\n");
}
