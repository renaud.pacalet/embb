/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <embb/memory.h>
#include <embb/adaif.h>
#include <stdint.h>

typedef struct {
  uint8_t  tdd;
  uint8_t  st;
  uint32_t ldt0;
  uint32_t ldr0;
  uint32_t ldt1;
  uint32_t ldr1;
  uint32_t ldt2;
  uint32_t ldr2;
  uint32_t ldt3;
  uint32_t ldr3;
  uint32_t startt0;
  uint32_t stopt0;
  uint32_t startr0;
  uint32_t stopr0;
  uint32_t startt1;
  uint32_t stopt1;
  uint32_t startr1;
  uint32_t stopr1;
  uint32_t startt2;
  uint32_t stopt2;
  uint32_t startr2;
  uint32_t stopr2;
  uint32_t startt3;
  uint32_t stopt3;
  uint32_t startr3;
  uint32_t stopr3;
  uint8_t  dst;
  uint8_t  src;
  uint8_t  op;
  uint32_t data;
} adaif_params;

// Set individual TDD modes
static inline void adaif_set_tddn(struct adaif_context* __base, uint64_t __channel, uint64_t __value)
{
    uint64_t __x = adaif_get_tdd(__base);
    __x |= (__value & 0x1ULL) << __channel;
    adaif_set_tdd(__base, __x);
}

#define ADAIF_OFF_STATE 0
#define ADAIF_IDLE_STATE 2
#define ADAIF_ON_STATE 1

#define ADAIF_TX0_FIFO_ADD_RANDOM_MODE 0x00000
#define ADAIF_RX0_FIFO_ADD_RANDOM_MODE 0x02000
#define ADAIF_TX1_FIFO_ADD_RANDOM_MODE 0x04000
#define ADAIF_RX1_FIFO_ADD_RANDOM_MODE 0x06000
#define ADAIF_TX2_FIFO_ADD_RANDOM_MODE 0x08000
#define ADAIF_RX2_FIFO_ADD_RANDOM_MODE 0x0a000
#define ADAIF_TX3_FIFO_ADD_RANDOM_MODE 0x0c000
#define ADAIF_RX3_FIFO_ADD_RANDOM_MODE 0x0e000
#define ADAIF_TX0_FIFO_ADD_FIFO_MODE 0x10000
#define ADAIF_RX0_FIFO_ADD_FIFO_MODE 0x12000
#define ADAIF_TX1_FIFO_ADD_FIFO_MODE 0x14000
#define ADAIF_RX1_FIFO_ADD_FIFO_MODE 0x16000
#define ADAIF_TX2_FIFO_ADD_FIFO_MODE 0x18000
#define ADAIF_RX2_FIFO_ADD_FIFO_MODE 0x1a000
#define ADAIF_TX3_FIFO_ADD_FIFO_MODE 0x1c000
#define ADAIF_RX3_FIFO_ADD_FIFO_MODE 0x1e000

// Set individual TX channel STATES
static inline void adaif_set_stt(struct adaif_context* __base, uint64_t __channel, uint64_t __value)
{
    uint64_t __x = adaif_get_st(__base);
    __x |= (__value & 0x3ULL) << (4 * __channel);
    adaif_set_st(__base, __x);
}

// Set individual RX channel STATES
static inline void adaif_set_str(struct adaif_context* __base, uint64_t __channel, uint64_t __value)
{
    uint64_t __x = adaif_get_st(__base);
    __x |= (__value & 0x3ULL) << (2 + 4 * __channel);
    adaif_set_st(__base, __x);
}

// Human-readable print of parameters
void print_params(FILE *f, ADAIF_CONTEXT *ctx);

// Print execute command
void print_ex(FILE *f, ADAIF_CONTEXT *ctx, int rdata);

// Change execute param
 void change_param(FILE *f, ADAIF_CONTEXT *ctx);

// randomly generates context ctx. min and max are two adaif_params structures expressing constraints on the random generation and must be initialized prior
// calling randctx. Each field is randomly thrown between its corresponding fields in min and max, included. Packed fields (tdd, st) are built piece by piece. When
// randomly picked, each field is guaranteed to have a valid value with respect to the other ctx fields.
void randctx(ADAIF_CONTEXT *ctx, adaif_params min, adaif_params max);

// Run one random test
void mktests(FILE *f, ADAIF_CONTEXT *ctx, adaif_params min, adaif_params max);
