/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <embb/memory.h>
#include <embb/css.h>
#include <embb/adaif.h>
#include <assert.h>
#include <cstdint>
#include <cinttypes>
#include "utils.h"
#include "tests.h"
#include "css_addresses.h"
#include "adaif_addresses.h"

// #define _GNU_SOURCE
#include <getopt.h>

/* Parse command line options, initialize file names. */
void parse_options (int argc, char **argv);

FILE *f;
int n;
uint32_t seed;

CSS_FUNC_DECL

#define ADAIF_RX0_EIE_SMASK 0x2

int main(int argc, char **argv)
{
  int i;
  ADAIF_CONTEXT ctx;  // ADAIF context
  struct css_regs cr; // CSS registers

  parse_options (argc, argv);

  adaif_ctx_init(&ctx, 0); // Initialize ADAIF context
  memset(&cr, 0, sizeof(struct css_regs));

  fprintf(f, "# Reset\n"); // 10 cycles reset
  fprintf(f, "RS 10\n");
  fprintf(f, "# Clear interrupt flags\n");
  fprintf(f, "VR %08" PRIx32 " 1 0 FF 0 0\n", CSS_IRQ_ADDR);
  fprintf(f, "# Set resets, chip enables and interrupt enables\n");
  css_set_prst(&cr, 1);  // De-assert PSS reset
  css_set_drst(&cr, 1);  // De-assert DMA reset
  css_set_pce(&cr, 1);   // Chip-enable PSS
  css_set_dce(&cr, 1);   // Chip-enable DMA
  css_set_p2hie(&cr, 1); // Enable PSS interrupts to host
  css_set_d2hie(&cr, 1); // Enable DMA interrupts to host
  css_set_e2hie(&cr, 1); // Enable extended interrupts to host
  css_set_hie(&cr, 1);   // Enable interrupts to host
  css_set_eie(&cr, ADAIF_RX0_EIE_SMASK); // Enable RX0 extended interrupt
  css_set_lhirq(&cr, 1); // Level-triggered PSS and DMA interrupts to host
  css_set_leirq(&cr, 1); // Level-triggered extended interrupts
  fprintf(f, "VW %08" PRIx32 " 1 0 ff %016" PRIx64 "\n", CSS_CTRL_ADDR, cr.ctrl);
  // Configure RX0:
  //  PERIOD = 11 samples
  adaif_set_ldr0(&ctx, 10);
  fprintf(f, "VW %08" PRIx32 " 1 0 ff %016" PRIx64 "\n", ADAIF_LD0_ADDR, ctx.params.ld0);
  //  START = 3, STOP = 8
  adaif_set_startr0(&ctx, 3);
  adaif_set_stopr0(&ctx, 8);
  fprintf(f, "VW %08" PRIx32 " 1 0 ff %016" PRIx64 "\n", ADAIF_BNDR0_ADDR, ctx.params.bndr0);
  //  TDD = NO, STATE = ON
  adaif_set_tddn(&ctx, 0, 0);
  adaif_set_str(&ctx, 0, ADAIF_ON_STATE);
  fprintf(f, "VW %08" PRIx32 " 1 0 ff %016" PRIx64 "\n", ADAIF_CFG_ADDR, ctx.params.cfg);

  // ////////////////////////////////////////////////////////
  // Acquire 1024 first samples, DMA and check them with ff mask.
  // ////////////////////////////////////////////////////////
  // Wait until RX0 extended interrupt
  fprintf(f, "WX %08" PRIx32 " %u\n", ADAIF_RX0_EIE_SMASK, 100000);
  // Inform verifier that 512x2 new samples will be sent to GPOD MSBs and LSBs
  fprintf(f, "GD %08" PRIx32 "%08" PRIx32 "\n", 1024, 0x1ff);
  // Launch DMA transfer from RX0 to GPOD (512x2 samples)
  fprintf(f, "DR %08" PRIx32 " 512 1 ff 0 1\n", ADAIF_RX0_FIFO_ADD_FIFO_MODE);
  // Clear RX0 extended interrupt:
  //  First read lvlr0 hidden register (and thus clear lvlr0.mirq flag)
  adaif_set_src(&ctx, 25); // lvlr0 index
  adaif_set_op(&ctx, ADAIF_OP_GET);
  fprintf(f, "VW %08" PRIx32 " 1 0 ff %016" PRIx64 "\n", ADAIF_HROP_ADDR, ctx.params.hrop); // Write hrop register
  fprintf(f, "EN 0 0 0 0f\n"); // Run command. Do not check returned data (we do not know what the level will be)
  fprintf(f, "WE 10000\n"); // Wait end of execution
  // Wait end of DMA transfer
  fprintf(f, "WD 10000\n");

  // ////////////////////////////////////////////////////////
  // Acquire 1024 next samples, DMA and check them with f0 mask.
  // ////////////////////////////////////////////////////////
  // Wait until RX0 extended interrupt
  fprintf(f, "WX %08" PRIx32 " %u\n", ADAIF_RX0_EIE_SMASK, 100000);
  // Inform verifier that 1024x1 next samples will be sent to GPOD MSBs
  fprintf(f, "GD %08" PRIx32 "%08" PRIx32 "\n", 1024, 0x0f0);
  // Launch DMA transfer from RX0 to GPOD (1024x1 samples)
  fprintf(f, "DR %08" PRIx32 " 1024 1 f0 0 1\n", ADAIF_RX0_FIFO_ADD_FIFO_MODE);
  // Clear RX0 extended interrupt:
  //  First read lvlr0 hidden register (and thus clear lvlr0.mirq flag)
  adaif_set_src(&ctx, 25); // lvlr0 index
  adaif_set_op(&ctx, ADAIF_OP_GET);
  fprintf(f, "VW %08" PRIx32 " 1 0 ff %016" PRIx64 "\n", ADAIF_HROP_ADDR, ctx.params.hrop); // Write hrop register
  fprintf(f, "EN 0 0 0 0f\n"); // Run command. Do not check returned data (we do not know what the level will be)
  fprintf(f, "WE 10000\n"); // Wait end of execution
  // Wait end of DMA transfer
  fprintf(f, "WD 10000\n");

  // ////////////////////////////////////////////////////////
  // Acquire 1024 next samples, DMA and check them with 0f mask.
  // ////////////////////////////////////////////////////////
  // Wait until RX0 extended interrupt
  fprintf(f, "WX %08" PRIx32 " %u\n", ADAIF_RX0_EIE_SMASK, 100000);
  // Inform verifier that 1024x1 next samples will be sent to GPOD LSBs
  fprintf(f, "GD %08" PRIx32 "%08" PRIx32 "\n", 1024, 0x00f);
  // Launch DMA transfer from RX0 to GPOD (1024x1 samples)
  fprintf(f, "DR %08" PRIx32 " 1024 1 0f 0 1\n", ADAIF_RX0_FIFO_ADD_FIFO_MODE);
  // Clear RX0 extended interrupt:
  //  First read lvlr0 hidden register (and thus clear lvlr0.mirq flag)
  adaif_set_src(&ctx, 25); // lvlr0 index
  adaif_set_op(&ctx, ADAIF_OP_GET);
  fprintf(f, "VW %08" PRIx32 " 1 0 ff %016" PRIx64 "\n", ADAIF_HROP_ADDR, ctx.params.hrop); // Write hrop register
  fprintf(f, "EN 0 0 0 0f\n"); // Run command. Do not check returned data (we do not know what the level will be)
  fprintf(f, "WE 10000\n"); // Wait end of execution
  // Wait end of DMA transfer
  fprintf(f, "WD 10000\n");

  fprintf(f, "# End simulation\n");
  fprintf(f, "QT\n");

  adaif_ctx_cleanup(&ctx);

  fclose(f);
  return 0;
}

const char *version_string = "\
mkcmdfile 0.1\n\
\n\
Copyright (C) 2010 Institut Telecom\n\
\n\
This software is governed by the CeCILL license under French law and\n\
abiding by the rules of distribution of free software.  You can  use,\n\
modify and/ or redistribute the software under the terms of the CeCILL\n\
license as circulated by CEA, CNRS and INRIA at the following URL\n\
http://www.cecill.info\n\
\n\
Written by Renaud Pacalet <renaud.pacalet@telecom-paristech.fr>.\n\
";

const char *usage_string = "\
mkcmdfile generate commands for VHDL functional simulation\n\
of the ADAIF DSP unit.\n\
\n\
USAGE: mkcmdfile [OPTION]... [OUTPUTFILE]\n\
\n\
Mandatory arguments to long options are mandatory for short options too.\n\
If OUTPUTFILE is not specified stdout is used. On success the exit status is 0.\n\
\n\
OPTIONS:\n\
  -t, --tests=NUM       number of tests to generate (default=1)\n\
  -s, --seed=NUM        seed of random generator (default=1)\n\
  -v, --version\n\
        Output version information and exit\n\
  -h, --help\n\
        Display this help and exit\n\
\n\
EXAMPLES:\n\
\n\
  To generate a command file named adaif.cmd driving the simulation for 10 tests:\n\
\n\
    mkcmdfile --tests=10 adaif.cmd\n\
\n\
Please report bugs to <renaud.pacalet@telecom-paristech.fr>.\n\
";

void
parse_options (int argc, char **argv) {
  int o, option_index, tmp;
  struct option long_options[] = {
    {"tests", required_argument, NULL, 't'},
    {"seed", required_argument, NULL, 's'},
    {"version", no_argument, NULL, 'v'},
    {"help", no_argument, NULL, 'h'},
    {0, 0, 0, 0}
  };

  option_index = 0;
  n = 1;
  seed = 1;
  while (1) {
    o =
      getopt_long (argc, argv, "t:s:vh",
      	     long_options, &option_index);
    if (o == -1) {
      break;
    }
    switch (o) {
      case 't':
        n = atoi(optarg);
        if(n < 1) {
          fprintf (stderr, "*** Invalid number of tests: %d\n", n);
          ERROR(-1, "%s", usage_string);
        }
        break;
      case 's':
        tmp = atoi(optarg);
        if(tmp < 0) {
          fprintf (stderr, "*** Invalid random seed: %d\n", tmp);
          ERROR(-1, "%s", usage_string);
        }
        seed = tmp;
        break;
      case 'v':
        fprintf (stderr, "%s", version_string);
        exit (0);
        break;
      default:
        fprintf (stderr, "%s", usage_string);
        exit (0);
        break;
    }
  }
  f = stdout;
  if (optind < argc) {
    f = XFOPEN(argv[optind], "w");
    optind += 1;
  }
  if (optind < argc) {
    ERROR(-1, "%s", usage_string);
  }
}
