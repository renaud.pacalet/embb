--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief ADAIF top level

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;

use work.adaif_pkg.all;

entity adaif is
  generic(
-- pragma translate_off
    debug:    boolean := true;   --* Print debug information
    verbose:  boolean := false;  --* Print more debug information
-- pragma translate_on
    with_uc:  boolean := true;        -- With or without UC
    nrx:      natural range 0 to 4 := 4;  -- Number of RX chains
    ntx:      natural range 0 to 4 := 4; -- Number of TX chains
    txdepth:  positive := 2;          -- Depth of TX synchronizers
    rxdepth:  positive := 2;          -- Depth of RX synchronizers
    n0:       positive := 1;  --* Number of input pipeline registers in MSS, including input registers of RAMs
    n1:       positive := 1); --* Number of output pipeline registers in MSS, including output registers of RAMs
  port(
-- pragma translate_off
    dbg:      out debug_type;    -- debug out
-- pragma translate_on
    nsamples:    out std_ulogic;    -- RP DEBUG RP
    clk:      in  std_ulogic;    -- master clock
    srstn:    in  std_ulogic;    -- master reset
    ce:       in  std_ulogic;    -- chip enable
    hirq:     in  std_ulogic;    -- host input irq,
    pirq:     out std_ulogic;    -- PSS output irq,
    dirq:     out std_ulogic;    -- DMA output irq,
    uirq:     out std_ulogic;    -- UC output irq,
    eirq:     out std_ulogic_vector(7 downto 0); -- Extended output irq,
    pp2adaif: in  pp2adaif_type;  -- PreProcessor input
    adaif2pp: out adaif2pp_type;  -- PreProcessor output
    adac2adaif: in  adac2adaif_type; 
    adaif2adac: out adaif2adac_type;
    tvci_in:  in  vci_i2t_type;  -- Target AVCI input
    tvci_out: out vci_t2i_type;  -- Target AVCI output
    ivci_in:  in  vci_t2i_type;  -- Initiator AVCI input
    ivci_out: out vci_i2t_type); -- Initiator AVCI output
end entity adaif;

architecture rtl of adaif is

  signal rams2mss: rams2mss_type;
  signal mss2rams: mss2rams_type;

begin

  i_adaifnr: entity work.adaif_no_rams(rtl)
  generic map(
-- pragma translate_off
    debug     => debug,
    verbose   => verbose,
-- pragma translate_on
    with_uc   => with_uc,
    nrx       => nrx,
    ntx       => ntx,
    txdepth   => txdepth,
    rxdepth   => rxdepth,
    n0        => 1,
    n1        => 1)
  port map(
-- pragma translate_off
    dbg      => dbg,
-- pragma translate_on
    nsamples    => nsamples,
    clk      => clk,
    srstn    => srstn,
    ce       => ce,
    hirq     => hirq,
    pirq     => pirq,
    dirq     => dirq,
    uirq     => uirq,
    eirq     => eirq,
    pp2adaif   => pp2adaif,
    adaif2pp   => adaif2pp,
    adac2adaif => adac2adaif,
    adaif2adac => adaif2adac,
    tvci_in  => tvci_in,
    tvci_out => tvci_out,
    ivci_in  => ivci_in,
    ivci_out => ivci_out,
    rams2mss => rams2mss,
    mss2rams => mss2rams);

  i_rams: entity work.rams(rtl)
  generic map(
    with_uc   => with_uc,
    nrx       => nrx,
    ntx       => ntx)
  port map(
    clk      => clk,
    rams2mss => rams2mss,
    mss2rams => mss2rams);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
