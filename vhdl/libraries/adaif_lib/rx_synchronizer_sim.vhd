--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for RX synchronizer

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.sim_utils.all;

use work.adaif_pkg.all;

entity rx_synchronizer_sim is
  generic(
    debug:     boolean  := false;  -- print low severity level asserts
    n0:        positive := 1000;   -- time spent in each state (clk cycles)
    n1:        positive := 1000;   -- number of different random rxclk periods to check
    clkp:      time     := 10 ns;  -- clk period
    rxclkpmin: time     := 13 ns;  -- minimum rxclk period
    smplpmin:  time     := 25 ns;  -- minimum sample period
    p_success: real     := 0.0001; -- probability of success of geometric random generator
    depth:     positive := 2       -- depth of re-synchronizers
  );
  port(
    valid: out std_ulogic;
    data:  out std_ulogic_vector(31 downto 0)
  );
end entity rx_synchronizer_sim;

architecture sim of rx_synchronizer_sim is

  subtype word is std_ulogic_vector(31 downto 0);
  subtype half_word is std_ulogic_vector(31 downto 0);

  signal clk:         std_ulogic;
  signal srstn:       std_ulogic := '0';
  signal st:          state_type := off_state;
  signal valid_local: std_ulogic := '0';
  signal data_local:  word;
  signal rxclk:       std_ulogic;
  signal rxdata:      word := (others => '0');
  signal rxvalid:     std_ulogic := '0';
  signal eos:         boolean := false; -- end of simulation

  shared variable valid_off, zero_on, non_zero_no, shift, match: shared_natural;

begin

  -- clk generator
  process
  begin
    clk <= '0';
    wait for clkp / 2;
    clk <= '1';
    wait for clkp / 2;
    if eos then
      wait;
    end if;
  end process;

  -- srstn and states generator. forces a 100 clk cycles reset at the beginning of simulation. loops accross the 6 states changes until end of simulation.
  -- spends n0 clk periods in each state.
  process
  begin
    seed_rnd(1);
    g_seed_rnd(1);
    srstn <= '0';
    for i in 1 to 100 loop
      wait until rising_edge(clk);
    end loop;
    srstn <= '1';
    loop
      st <= off_state;
      assert not debug report "state=off";
      for i in 1 to n0 loop
        wait until rising_edge(clk);
      end loop;
      st <= idle_state;
      assert not debug report "state=idle";
      for i in 1 to n0 loop
        wait until rising_edge(clk);
      end loop;
      st <= on_state;
      assert not debug report "state=on";
      for i in 1 to n0 loop
        wait until rising_edge(clk);
      end loop;
      st <= off_state;
      assert not debug report "state=off";
      for i in 1 to n0 loop
        wait until rising_edge(clk);
      end loop;
      st <= on_state;
      assert not debug report "state=on";
      for i in 1 to n0 loop
        wait until rising_edge(clk);
      end loop;
      st <= idle_state;
      assert not debug report "state=idle";
      for i in 1 to n0 loop
        wait until rising_edge(clk);
      end loop;
      exit when eos;
    end loop;
    wait;
  end process;

  -- rxclk generator. first runs rxclk at rxclkpmin period until end of reset. runs rxclk at rxclkpmin, then n1 random periods at rxclkpmin + (t ps) where t is
  -- a geometric random number of pico-seconds. stays at the same frequency until all 6 state changes have occured. at end of simulation prints statistics.
  process
    variable rxclkp: time;
    variable tmp: boolean;
    variable l: line;
  begin
    rxclkp := rxclkpmin;
    loop
      rxclk <= '0';
      wait for rxclkp / 2;
      rxclk <= '1';
      wait for rxclkp / 2;
      exit when srstn = '1';
    end loop;
    write(l, string'("RXCLK: "));
    write(l, (1 sec) / rxclkp);
    write(l, string'(" Hz"));
    writeline(output, l);
    tmp := false;
    loop
      rxclk <= '0';
      wait for rxclkp / 2;
      rxclk <= '1';
      wait for rxclkp / 2;
      exit when st = off_state and tmp;
      tmp := st = idle_state;
    end loop;
    for i in 1 to n1 loop
      rxclkp := rxclkpmin + g_int_rnd(p_success) * (1 ps);
      write(l, string'("RXCLK: "));
      write(l, (1 sec) / rxclkp);
      write(l, string'(" Hz"));
      writeline(output, l);
      tmp := false;
      loop
        rxclk <= '0';
        wait for rxclkp / 2;
        rxclk <= '1';
        wait for rxclkp / 2;
        exit when st = off_state and tmp;
        tmp := st = idle_state;
      end loop;
    end loop;
    eos <= true;
    write(l, string'("STATE changes:         6 * "));
    write(l, n1);
    writeline(output, l);
    write(l, string'("VALID while OFF:       "));
    write(l, valid_off.get);
    writeline(output, l);
    write(l, string'("ZERO while ON:         "));
    write(l, zero_on.get);
    writeline(output, l);
    write(l, string'("Non ZERO while not ON: "));
    write(l, non_zero_no.get);
    writeline(output, l);
    write(l, string'("SHIFTS:                "));
    write(l, shift.get);
    writeline(output, l);
    write(l, string'("MATCHES:               "));
    write(l, match.get);
    writeline(output, l);
    write(l, string'("Regression test passed."));
    writeline(output, l);
    wait;
  end process;

  -- rxdata and rxvalid generator. generates non zero data in on state only.
  process(rxclk)
    variable delta: time := 0 ns;
  begin
    if rising_edge(rxclk) then
      if srstn = '0' then
        rxvalid <= '0';
        rxdata <= (others => '0');
      else
        if st /= off_state and now - delta > smplpmin then
          rxvalid <= '1';
          delta := now;
        else
          rxvalid <= '0';
        end if;
        if st = on_state then
          rxdata <= std_ulogic_vector_rnd(32);
          rxdata(0) <= '1';
        else
          rxdata <= (others => '0');
        end if;
      end if;
    end if;
  end process;

  -- design under test.
  rxs: entity work.rx_synchronizer(rtl)
  generic map(
    depth      => depth
  )
  port map(
    rxclk    => rxclk,
    rxvalid  => rxvalid,
    rxdata   => rxdata,
    clk      => clk,
    srstn    => srstn,
    st       => st,
    valid    => valid_local,
    data     => data_local
  );

  valid <= valid_local;
  data  <= data_local;

  -- checker. stores non-zero incomming samples in a fifo, checks that non-zero input samples are finally received at the other end. for each non-zero output
  -- checks that the current top of the fifo matches. tolerates some deviation as, for instance, the loss of some non-zero samples (but no more than the fifo
  -- depth), the appearance of unexpected zero samples or active valid signal while in off state. this tolerance is due to state changes that are taken into
  -- account at different times on both ends of the re-synchronizer. counts the deviations of each kind and the regular matches in order to print a summary at
  -- the end of simulation.
  process(clk, rxclk)
    constant stages: natural := 10;
    type fifo_type is array(0 to stages - 1) of word;
    variable fifo: fifo_type := (others => (others => '0'));
    variable rd: natural range 0 to stages - 1 := 0;
    variable wr: natural range 0 to stages - 1 := 0;
    constant w0: word := (others => '0');
    variable w1, w2: word;
  begin
    if rising_edge(rxclk) then
      if srstn = '0' then
        fifo := (others => (others => '0'));
        rd := 0;
        wr := 0;
      elsif rxdata /= w0 then
        if rxvalid = '1' then
          fifo(wr) := rxdata;
          if wr = stages - 1 then
            wr := 0;
          else
            wr := wr + 1;
          end if;
        end if;
      end if;
    end if;
    if rising_edge(clk) then
      if srstn = '1' and valid_local = '1' then
        if st = off_state then
          valid_off.inc;
          assert not debug report "VALID raised while in off_state";
        end if;
        w1 := data_local;
        if w1 /= w0 then
          for r in 0 to stages - 1 loop
            w2 := fifo(rd);
            if rd = stages - 1 then
              rd := 0;
            else
              rd := rd + 1;
            end if;
            if w1 = w0 then
              if st = on_state then
                zero_on.inc;
                assert not debug report "Zero DATA while in on_state";
              end if;
              w2 := w0;
            else
              if st /= on_state then
                non_zero_no.inc;
                assert not debug report "Non zero DATA while not in on_state";
              end if;
            end if;
            if w1 /= w2 then
              shift.inc;
              assert not debug report "shift" severity note;
              assert r < stages - 1 report "Output mismatch" severity failure;
            else
              match.inc;
              exit;
            end if;
          end loop;
        end if;
      end if;
    end if;
  end process;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
