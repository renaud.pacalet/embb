#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

gh-IGNORE	+= adaif_lib.rams adaif_lib.adaif_no_rams adaif_lib.adaif_sim adaif_lib.adaif

adaif_lib.adaif: \
	global_lib.global \
	adaif_lib.adaif_pkg \
	adaif_lib.adaif_no_rams \
	adaif_lib.rams

adaif_lib.adaif_no_rams: \
	global_lib.global \
	css_lib.css_pkg \
	css_lib.css \
	adaif_lib.bitfield_pkg \
	adaif_lib.adaif_pkg \
	adaif_lib.mss \
	adaif_lib.pss

adaif_lib.adaif_pkg: \
	global_lib.global \
	global_lib.utils \
	memories_lib.ram_pkg \
	adaif_lib.bitfield_pkg

adaif_lib.adaif_sim: \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils \
	global_lib.hst_emulator \
	adaif_lib.adaif_pkg \
	adaif_lib.bitfield_pkg \
	adaif_lib.adc_emulator \
	adaif_lib.adaif

adaif_lib.adc_emulator: \
	random_lib.rnd \
	global_lib.global

adaif_lib.dac_emulator: \
	random_lib.rnd \
	global_lib.global \
	global_lib.sim_utils

adaif_lib.pss: \
	global_lib.global \
	global_lib.utils \
	adaif_lib.bitfield_pkg \
	adaif_lib.adaif_pkg \
	adaif_lib.tx_synchronizer \
	adaif_lib.rx_synchronizer

adaif_lib.mss: \
	global_lib.global \
	global_lib.utils \
	memories_lib.ram_pkg \
	adaif_lib.adaif_pkg

adaif_lib.pp_emulator: \
	global_lib.global \
	global_lib.sim_utils \
	random_lib.rnd \
	adaif_lib.adaif_pkg \
	adaif_lib.bitfield_pkg

adaif_lib.rams: \
	memories_lib.ram_pkg \
	memories_lib.tdpram \
	adaif_lib.adaif_pkg

adaif_lib.rx_synchronizer: \
	adaif_lib.adaif_pkg

adaif_lib.rx_synchronizer_sim: \
	global_lib.sim_utils \
	random_lib.rnd \
	adaif_lib.adaif_pkg  \
	adaif_lib.rx_synchronizer

adaif_lib.tx_synchronizer: \
	global_lib.utils \
	adaif_lib.adaif_pkg

adaif_lib.tx_synchronizer_sim: \
	global_lib.sim_utils \
	random_lib.rnd \
	adaif_lib.adaif_pkg  \
	adaif_lib.tx_synchronizer

# Default input and output pipeline depths of MSS.
ADAIF_LIB_N0	?= 1
ADAIF_LIB_N1	?= 1

# Simulations

adaif_lib_csrc		:= $(hwprj_db_path.adaif_lib)/C
adaif_lib_c_cmdfile		:= $(adaif_lib_csrc)/c_adaif.cmd
adaif_lib_cmdfile		:= $(adaif_lib_csrc)/adaif.cmd
ADAIF_LIB_CMDNUM		:= 1
ADAIF_LIB_CMDGEN		:= $(ADAIF_LIB_CMDGEN)
adaif_lib_cmdgen		:= $(adaif_lib_csrc)/$(ADAIF_LIB_CMDGEN)
ADAIF_LIB_CMDGENFLAGS	:= --tests=$(ADAIF_LIB_CMDNUM)

ms-sim.adaif_lib.rx_synchronizer_sim: MSSIMFLAGS += -Gn0=1000 -Gn1=100
ms-sim.adaif_lib.tx_synchronizer_sim: MSSIMFLAGS += -Gn0=1000 -Gn1=100
ms-sim.adaif_lib.adaif_sim: $(adaif_lib_cmdfile)
ms-sim.adaif_lib.adaif_sim: MSSIMFLAGS += -Gcmdfile="$(adaif_lib_cmdfile)" -Gdebug=true -Gverbose=false -Gn0=$(ADAIF_LIB_N0) -Gn1=$(ADAIF_LIB_N1)
ms-sim.adaif_lib.pss_sim: $(adaif_lib_c_cmdfile)
ms-sim.adaif_lib.pss_sim: MSSIMFLAGS += -Gcmdfile="$(adaif_lib_c_cmdfile)" -Gn0=$(ADAIF_LIB_N0) -Gn1=$(ADAIF_LIB_N1)
ms-sim.adaif_lib.mss_sim: MSSIMFLAGS += -Gn=100000 -Gdebug=false -Gn0=$(ADAIF_LIB_N0) -Gn1=$(ADAIF_LIB_N1)
ms-sim.adaif_lib: $(addprefix ms-sim.adaif_lib.,rx_synchronizer_sim tx_synchronizer_sim adaif_sim pss_sim mss_sim)
ms-sim: ms-sim.adaif_lib

.PHONY: $(adaif_lib_cmdgen)

$(adaif_lib_cmdfile): $(adaif_lib_cmdgen)
	$(adaif_lib_cmdgen) $(ADAIF_LIB_CMDGENFLAGS) $@

$(adaif_lib_cmdgen):
	$(MAKE) -C $(dir $@) $(notdir $@)

# Synthesis

## Precision RTL
$(addprefix pr-syn.adaif_lib.,pss adaif adaif_no_rams top): $(hwprj_db_path.adaif_lib)/bitfield_pkg.vhd
$(addprefix pr-syn.adaif_lib.,adaif adaif_no_rams adaif_top): $(hwprj_db_path.css_lib)/bitfield_pkg.vhd
pr-syn.adaif_lib: $(addprefix pr-syn.adaif_lib.,adaif adaif_no_rams pss mss rams rx_synchronizer tx_synchronizer)
pr-syn: pr-syn.adaif_lib

## RTL compiler
$(addprefix rc-syn.adaif_lib.,adaif_no_rams): $(hwprj_db_path.adaif_lib)/bitfield_pkg.vhd $(hwprj_db_path.css_lib)/bitfield_pkg.vhd
rc-syn.adaif_lib: $(addprefix rc-syn.adaif_lib.,adaif adaif_no_rams pss mss rams rx_synchronizer tx_synchronizer)
rc-syn: rc-syn.adaif_lib

hw-clean: adaif_lib_clean

adaif_lib_clean:
	$(MAKE) -C $(adaif_lib_csrc) clean
	rm -f $(adaif_lib_cmdfile) $(adaif_lib_c_cmdfile)

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
