--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief RX synchronizer

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.adaif_pkg.all;

entity rx_synchronizer is
  generic(
    depth:      positive := 2      -- depth of re-synchronizers
  );
  port(
    rxclk:    in  std_ulogic; -- RX samples are first sampled on RXCLK rising edges before being transferred in CLK clock domain
    rxvalid:  in  std_ulogic; -- when set on a RXCLK rising edge, indicates that RXDATA input carries a sample
    rxdata:   in  std_ulogic_vector(31 downto 0); -- real part on left half (31 downto 16), imaginary part on right half (15 downto 0)
    clk:      in  std_ulogic; -- clock of receiving clock domain
    srstn:    in  std_ulogic; -- synchronous, active low, reset; used in CLK clock domain only
    st:       in  state_type; -- current state on the interface (OFF, IDLE, ON)
    valid:    out std_ulogic; -- when set on a CLK rising edge, indicates that DATA output carries a sample
    data:     out std_ulogic_vector(31 downto 0)  -- real part on left half (31 downto 16), imaginary part on right half (15 downto 0)
  );
end entity rx_synchronizer;

-- please see ADAIF datasheet for detailed description of architecture
architecture rtl of rx_synchronizer is

  signal d0: std_ulogic_vector(31 downto 0); -- sample register in rxclk domain
  signal v: gray2_vector(depth + 1 downto 0); -- rxvalid synchronizer (rxclk to clk); index depth+1 is the input stage
  signal valid_d: std_ulogic; -- signals a valid new sample in d0
  signal r: std_ulogic_vector(depth downto 0); -- srstn resynchroniser
  alias rx_srstn: std_ulogic is r(0);

begin

  rxclk_pr: process(rxclk)
  begin
    if rising_edge(rxclk) then
      r(depth - 1 downto 0) <= r(depth downto 1);
      if rx_srstn = '0' then
        d0 <= (others => '0');
        v(depth + 1) <= gray2_idle1;
      else
        if rxvalid = '1' then
          d0 <= rxdata; -- Sample incomming sample in d0 register (rxcllk domain)
        end if;
        v(depth + 1) <= gray_encode(rxvalid, v(depth + 1)); -- Sample Gray-encoded rxvalid in first stage of resynchronizer (rxclk domain)
      end if;
    end if;
  end process rxclk_pr;

  clkl_pr: process(clk) -- Part of resynchronizer in system clock domain (falling edge to reduce the latency by one half clock period)
  begin
    if falling_edge(clk) then
      if srstn = '0' then
        v(depth downto 0) <= (others => gray2_idle1); -- Reset
      else
        v(depth downto 0) <= v(depth + 1 downto 1);
      end if;
    end if;
  end process clkl_pr;

  valid_d <= gray_decode(v(1), v(0));

  clkh_pr: process(clk) -- Data and valid output registers (system clock domain, rising edge)
    variable cnt: natural range 0 to 9;
    variable cntri: u_unsigned(15 downto 2);
  begin
    if rising_edge(clk) then
      r(depth) <= srstn;
      if srstn = '0' then -- Reset
        valid <= '0';
        data <= (others => '0');
        cnt := 0;
        cntri := (others => '0');
      else
        if st = off_state then -- Off state
          valid <= '0';
          data <= (others => '0');
        else
          valid <= valid_d;
          if st = on_state and valid_d = '1' then  -- On state and new sample in d0
            data <= d0; -- Sample
          elsif st = idle_state then -- Idle state
            data <= (others => '0'); -- Sample zero
          end if;
        end if;
      end if;
    end if;
  end process clkh_pr;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
