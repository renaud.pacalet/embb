--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief TX synchronizer

library ieee;
use ieee.std_logic_1164.all;

use work.adaif_pkg.all;

entity tx_synchronizer is
  generic(
    depth: positive := 2   -- depth of re-synchronizers
  );
  port(
    txclk:   in  std_ulogic; -- the receiving environment samples TXDATA on TXCLK rising edges for which TXACK is set
    txack:   in  std_ulogic; -- when set on a TXCLK rising edge, indicates that the TXDATA output has been sampled
    txdata:  out std_ulogic_vector(31 downto 0); -- real part on left half (31 downto 16), imaginary part on right half (15 downto 0)
    clk:     in  std_ulogic; -- clock of sending clock domain
    srstn:   in  std_ulogic; -- synchronous, active low, reset; used in CLK clock domain only
    st:      in  state_type; -- current state on the interface (OFF, IDLE, ON)
    ack:     out std_ulogic; -- re-synchronized TXACK in the CLK clock domain
    data:    in  std_ulogic_vector(31 downto 0)  -- real part on left half (31 downto 16), imaginary part on right half (15 downto 0)
  );
end entity tx_synchronizer;

-- please see ADAIF datasheet for detailed description of architecture
architecture rtl of tx_synchronizer is

  signal v: gray2_vector(depth + 1 downto 0); -- txvalid synchronizer (txclk to clk); index depth+1 is the input stage
  signal ack_local: std_ulogic; -- local copy of ACK output
  signal data_local: std_ulogic_vector(31 downto 0);

begin

  ack <= ack_local when st /= off_state else
         '0';
  data_local <= data when st = on_state else
                (others => '0');

  txclk_pr: process(txclk)
  begin
    if rising_edge(txclk) then
      if srstn = '0' then
        v(depth + 1) <= gray2_idle1; -- Reset. Do we need a resynchronizer on srstn?
      else
        v(depth + 1) <= gray_encode(txack, v(depth + 1)); -- Sample Gray-encoded txvalid in first stage of resynchronizer (txclk domain)
      end if;
    end if;
  end process txclk_pr;

  clkl_pr: process(clk) -- Part of resynchronizer in system clock domain (falling edge to reduce the latency by one half clock period)
  begin
    if falling_edge(clk) then
      if srstn = '0' then
        v(depth downto 0) <= (others => gray2_idle1); -- Reset
      else
        v(depth downto 0) <= v(depth + 1 downto 1);
      end if;
    end if;
  end process clkl_pr;

  ack_local <= gray_decode(v(1), v(0));

  clkh_pr: process(clk) -- Data input register (system clock domain, rising edge)
  begin
    if rising_edge(clk) then
      if srstn = '0' then -- Reset
        txdata <= (others => '0');
      elsif ack_local = '1' then -- Previous TXDATA output has been sampled, send the next one
        txdata <= data_local;
      end if;
    end if;
  end process clkh_pr;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
