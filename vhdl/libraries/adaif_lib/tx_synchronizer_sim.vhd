--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for TX synchronizer

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.sim_utils.all;

use work.adaif_pkg.all;

entity tx_synchronizer_sim is
  generic(
    debug:     boolean  := false;  -- print low severity level asserts
    n0:        positive := 1000;   -- time spent in each state (clk cycles)
    n1:        positive := 1000;   -- number of different random txclk periods to check
    clkp:      time     := 10 ns;  -- clk period
    txclkpmin: time     := 13 ns;  -- minimum txclk period
    smplpmin:  time     := 25 ns;  -- minimum sample period
    p_success: real     := 0.0001; -- probability of success of geometric random generator
    depth:     positive := 2       -- depth of re-synchronizers
  );
  port(
    ack:    out std_ulogic;
    txdata: out std_ulogic_vector(31 downto 0)
  );
end entity tx_synchronizer_sim;

architecture sim of tx_synchronizer_sim is

  subtype word is std_ulogic_vector(31 downto 0);

  signal clk:          std_ulogic;
  signal srstn:        std_ulogic := '0';
  signal st:           state_type := off_state;
  signal ack_local:    std_ulogic := '0';
  signal data:         word;
  signal txclk:        std_ulogic;
  signal txdata_local: word := (others => '0');
  signal txack:        std_ulogic := '0';
  signal eos:          boolean := false; -- end of simulation

  shared variable ack_off, zero_on, non_zero_no, shift, match: shared_natural;

begin

  -- clk generator
  process
  begin
    clk <= '0';
    wait for clkp / 2;
    clk <= '1';
    wait for clkp / 2;
    if eos then
      wait;
    end if;
  end process;

  -- srstn and states generator. forces a 100 clk cycles reset at the beginning of simulation. loops accross the 6 states changes until end of simulation.
  -- spends n0 clk periods in each state.
  process
  begin
    seed_rnd(1);
    g_seed_rnd(1);
    srstn <= '0';
    for i in 1 to 100 loop
      wait until rising_edge(clk);
    end loop;
    srstn <= '1';
    loop
      st <= off_state;
      assert not debug report "state=off" severity note;
      for i in 1 to n0 loop
        wait until rising_edge(clk);
      end loop;
      st <= idle_state;
      assert not debug report "state=idle" severity note;
      for i in 1 to n0 loop
        wait until rising_edge(clk);
      end loop;
      st <= on_state;
      assert not debug report "state=on" severity note;
      for i in 1 to n0 loop
        wait until rising_edge(clk);
      end loop;
      st <= off_state;
      assert not debug report "state=off" severity note;
      for i in 1 to n0 loop
        wait until rising_edge(clk);
      end loop;
      st <= on_state;
      assert not debug report "state=on" severity note;
      for i in 1 to n0 loop
        wait until rising_edge(clk);
      end loop;
      st <= idle_state;
      assert not debug report "state=idle" severity note;
      for i in 1 to n0 loop
        wait until rising_edge(clk);
      end loop;
      exit when eos;
    end loop;
    wait;
  end process;

  -- txclk generator. first runs txclk at txclkpmin period until end of reset. runs txclk at txclkpmin, then n1 random periods at txclkpmin + (t ps) where t is
  -- a geometric random number of pico-seconds. stays at the same frequency until all 6 state changes have occured. at end of simulation prints statistics.
  process
    variable txclkp: time;
    variable tmp: boolean;
    variable l: line;
  begin
    txclkp := txclkpmin;
    loop
      txclk <= '0';
      wait for txclkp / 2;
      txclk <= '1';
      wait for txclkp / 2;
      exit when srstn = '1';
    end loop;
    write(l, string'("TXCLK: "));
    write(l, (1 sec) / txclkp);
    write(l, string'(" Hz"));
    writeline(output, l);
    tmp := false;
    loop
      txclk <= '0';
      wait for txclkp / 2;
      txclk <= '1';
      wait for txclkp / 2;
      exit when st = off_state and tmp;
      tmp := st = idle_state;
    end loop;
    for i in 1 to n1 loop
      txclkp := txclkpmin + g_int_rnd(p_success) * (1 ps);
      write(l, string'("TXCLK: "));
      write(l, (1 sec) / txclkp);
      write(l, string'(" Hz"));
      writeline(output, l);
      tmp := false;
      loop
        txclk <= '0';
        wait for txclkp / 2;
        txclk <= '1';
        wait for txclkp / 2;
        exit when st = off_state and tmp;
        tmp := st = idle_state;
      end loop;
    end loop;
    eos <= true;
    write(l, string'("STATE changes:         6 * "));
    write(l, n1);
    writeline(output, l);
    write(l, string'("ACK while OFF:         "));
    write(l, ack_off.get);
    writeline(output, l);
    write(l, string'("ZERO while ON:         "));
    write(l, zero_on.get);
    writeline(output, l);
    write(l, string'("Non ZERO while not ON: "));
    write(l, non_zero_no.get);
    writeline(output, l);
    write(l, string'("SHIFTS:                "));
    write(l, shift.get);
    writeline(output, l);
    write(l, string'("MATCHES:               "));
    write(l, match.get);
    writeline(output, l);
    write(l, string'("Regression test passed."));
    writeline(output, l);
    wait;
  end process;

  -- txack generator
  process(txclk)
    variable delta: time := 0 ns;
  begin
    if rising_edge(txclk) then
      if srstn = '0' then
        txack <= '0';
      elsif now - delta > smplpmin then
        txack <= '1';
        delta := now;
      else
        txack <= '0';
      end if;
    end if;
  end process;

  -- design under test.
  txs: entity work.tx_synchronizer(rtl)
  generic map(
    depth => depth
  )
  port map(
    txclk   => txclk,
    txack   => txack,
    txdata  => txdata_local,
    clk     => clk,
    srstn   => srstn,
    st      => st,
    ack     => ack_local,
    data    => data
  );

  ack    <= ack_local;
  txdata <= txdata_local;

  -- data generator
  process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        data <= (others => '0');
      elsif ack_local = '1' then
        data <= std_ulogic_vector_rnd(31) & '1';
      end if;
    end if;
  end process;

  -- checker. stores non-zero incomming samples in a fifo, checks that non-zero input samples are finally received at the other end. for each non-zero output
  -- checks that the current top of the fifo matches. tolerates some deviation as, for instance, the loss of some non-zero samples (but no more than the fifo
  -- depth), the appearance of unexpected zero samples or active ack signal while in off state. this tolerance is due to state changes that are taken into
  -- account at different times on both ends of the re-synchronizer. counts the deviations of each kind and the regular matches in order to print a summary at
  -- the end of simulation.
  process(clk, txclk)
    constant stages: natural := 10;
    type fifo_type is array(0 to stages - 1) of word;
    variable fifo: fifo_type := (others => (others => '0'));
    variable rd: natural range 0 to stages - 1 := 0;
    variable wr: natural range 0 to stages - 1 := 0;
    constant w0: word := (others => '0');
    variable w1, w2: word;
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        fifo := (others => (others => '0'));
        rd := 0;
        wr := 0;
      elsif ack_local = '1' then
        if st = off_state then
          ack_off.inc;
          assert not debug report "ACK raised while in off_state" severity warning;
        elsif st = on_state then
          fifo(wr) := data;
          if wr = stages - 1 then
            wr := 0;
          else
            wr := wr + 1;
          end if;
        end if;
      end if;
    end if;
    if rising_edge(txclk) and txack = '1' then
      if st = off_state and txdata_local /= w0 then
        non_zero_no.inc;
        assert not debug report "Non zero DATA while in off_state" severity warning;
      elsif st /= off_state then
        w1 := txdata_local;
        if w1 = w0 and st = on_state then
          zero_on.inc;
          assert not debug report "Zero DATA while in on_state" severity warning;
        elsif w1 /= w0 and st = idle_state then
          non_zero_no.inc;
          assert not debug report "Non zero DATA while not in on_state" severity warning;
        elsif st = on_state then
          for r in 0 to stages - 1 loop
            w2 := fifo(rd);
            if rd = stages - 1 then
              rd := 0;
            else
              rd := rd + 1;
            end if;
            if w1 /= w2 then
              shift.inc;
              assert not debug report "shift" severity note;
              assert r < stages - 1 report "Output mismatch" severity failure;
            else
              match.inc;
              exit;
            end if;
          end loop;
        end if;
      end if;
    end if;
  end process;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
