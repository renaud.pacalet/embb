--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief ADAIF PSS

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

use work.bitfield.all;
use work.adaif_pkg.all;

entity pss is
  generic(with_uc: boolean := true;        -- With or without UC
          txdepth: positive := 2;          -- Depth of TX synchronizers
          rxdepth: positive := 2;          -- Depth of RX synchronizers
          nrx: natural range 0 to 4 := 4;  -- Number of RX chains
          ntx: natural range 0 to 4 := 4); -- Number of TX chains
  port(
-- pragma translate_off
       tts      : out state_vector(0 to 3);
       trs      : out state_vector(0 to 3);
       iut      : out std_ulogic_vector(0 to 3);
       iot      : out std_ulogic_vector(0 to 3);
       iur      : out std_ulogic_vector(0 to 3);
       ior      : out std_ulogic_vector(0 to 3);
       lvlr     : out word32_vector(0 to 3); 
       lvlt     : out word32_vector(0 to 3); 
-- pragma translate_on
       clk      : in  std_ulogic;   -- Master clock
       nsamples    : out std_ulogic;   -- RP DEBUG RP
       css2pss  : in  css2pss_type; -- Commands from CSS
       param    : in  std_ulogic_vector(cmdsize * 8 - 1 downto 0); -- Command parameters from CSS
       pss2css  : out pss2css_type;  -- Command results from PSS to CSS
       fifo2mss : out fifo2mss_type; -- FIFO read/write pointers, incremented FIFO read/write pointers, single and double read/write enablers
       mss2fifo : in  mss2fifo_type; -- Values to increment FIFO read/write pointers by (0, 1 or 2)
       pp2adaif : in  pp2adaif_type; -- Requests from PP interface
       adaif2pp : out adaif2pp_type; -- Responses to PP requests
       adac2adaif: in  adac2adaif_type; -- Inputs from AD/DA converters
       adaif2adac: out adaif2adac_type; -- Outputs to AD/DA converters
       pss2mss   : out pss2mss_type;  -- Read/write requests to FIFOs
       mss2pss   : in  mss2pss_type); -- Responses from FIFOs to read/write requests
end entity pss;

architecture rtl of pss is

  alias pss2tx   is pss2mss.pss2tx;  -- Requests to TX FIFOs
  alias pss2rx   is pss2mss.pss2rx;  -- Requests to RX FIFOs
  alias tx2pss   is mss2pss.tx2pss;  -- Responses from TX FIFOs
  alias rx2pss   is mss2pss.rx2pss;  -- Responses from RX FIFOs
  alias wopt    is mss2fifo.wopt; -- TX FIFO write pointer increment (0 to 2)
  alias ropt    is mss2fifo.ropt; -- TX FIFO read pointer increment (0 to 2)
  alias wopr    is mss2fifo.wopr; -- RX FIFO write pointer increment (0 to 2)
  alias ropr    is mss2fifo.ropr; -- RX FIFO read pointer increment (0 to 2)
  
  signal hreg: word32_vector(0 to 31); --Hidden registers 

  signal cmd       : bitfield_type; -- Parameters from CSS as a record
  signal rxdata    : word32_vector(0 to 3); -- Output data samples from RX synchronizers
  signal txdata    : word32_vector(0 to 3); -- Input data samples to TX synchronizers
  signal ack       : std_ulogic_vector(0 to 3); -- Acknowledges from TX synchronizers
  signal val       : std_ulogic_vector(0 to 3); -- Valid sample indicators from RX synchronizers
  signal tdd       : std_ulogic_vector(3 downto 0); -- TDD/FDD mode indicators
  signal stt       : state_vector(0 to 3); -- State requests from CSS for TX interfaces (OFF, IDLE, ON)
  signal sttd      : state_vector(0 to 3); -- Current states of TX interfaces (OFF, IDLE, ON) 
  signal str       : state_vector(0 to 3); -- State requests from CSS for RX interfaces (OFF, IDLE, ON)
  signal strd      : state_vector(0 to 3); -- Current states of RX interfaces (OFF, IDLE, ON) 
  
  -- Reload, start and stop values of free running counters
  signal ldr, ldt,  startr, startt, stopr, stopt : word32_vector(0 to 3);
  -- TDD-related indicators
  signal rxstarted, txstarted : boolean_vector(0 to 3);

  signal pss2txren, pss2rxwen : std_ulogic_vector(0 to 3);
  signal f2m : fifo2mss_type;

  signal nsamplescnt: u_unsigned(19 downto 0); -- RP DEBUG RP

begin

  nsamples <= nsamplescnt(19); -- RP DEBUG RP

  txg : for i in 0 to 3 generate 
    txg1: if i < ntx generate
      txi : entity work.tx_synchronizer(rtl)
      generic map(
        depth => txdepth)      
      port map(
        txclk    => adac2adaif.txclk(i),
        txack    => adac2adaif.txack(i),
        txdata   => adaif2adac.txdata(i),
        clk      => clk,
        srstn    => css2pss.srstn,
        st       => sttd(i),
        ack      => ack(i),
        data     => txdata(i)
      );
      txdata(i)    <= tx2pss.rdata(i) when tx2pss.en(i) = '1' else (others => '0');
      pss2tx.ren(i) <= pss2txren(i);
    end generate txg1;
    txg2: if i >= ntx generate
      adaif2adac.txdata(i)  <= (others => '0');
      txdata(i)             <= (others => '0');
      pss2tx.ren(i)          <= '0';
    end generate txg2;
  end generate txg;

  rxg : for i in 0 to 3 generate 
    rxg1: if i < nrx generate
      rxi : entity work.rx_synchronizer(rtl)
      generic map(
        depth => rxdepth)      
      port map(
        rxdata   => adac2adaif.rxdata(i),
        rxclk    => adac2adaif.rxclk(i),
        rxvalid  => adac2adaif.rxvalid(i),
        data     => rxdata(i),
        clk      => clk,
        st       => strd(i),
        valid    => val(i),
        srstn    => css2pss.srstn
      );
      pss2rx.wen(i) <= pss2rxwen(i);
    end generate rxg1;
    rxg2: if i >= nrx generate
      val(i)  <= '0';
      rxdata(i) <= (others => '0');
      pss2rx.wen(i) <= '0';
    end generate rxg2;
  end generate rxg;

  cmd     <= bitfield_slice(param);

  adaif2pp.en   <= rx2pss.en;
  adaif2pp.gnt  <= rx2pss.rgnt when pp2adaif.rnw = '1' else tx2pss.wgnt;
  adaif2pp.d    <= rx2pss.rdata;
  pss2tx.wen   <= pp2adaif.en when pp2adaif.rnw = '0' else (others => '0');
  pss2rx.ren   <= pp2adaif.en when pp2adaif.rnw = '1' else (others => '0');
  pss2tx.wdata <= pp2adaif.d;
       
-- pragma translate_off
  tts <= sttd;
  trs <= strd;
-- pragma translate_on
  
  process(cmd)
  begin
    tdd <= cmd.tdd;

    ldr(0) <= cmd.ldr0;
    ldr(1) <= cmd.ldr1;
    ldr(2) <= cmd.ldr2;
    ldr(3) <= cmd.ldr3;
  
    ldt(0) <= cmd.ldt0;
    ldt(1) <= cmd.ldt1;
    ldt(2) <= cmd.ldt2;
    ldt(3) <= cmd.ldt3;

    startr(0) <= cmd.startr0;
    startr(1) <= cmd.startr1;
    startr(2) <= cmd.startr2;
    startr(3) <= cmd.startr3;

    startt(0) <= cmd.startt0;
    startt(1) <= cmd.startt1;
    startt(2) <= cmd.startt2;
    startt(3) <= cmd.startt3;

    stopt(0) <= cmd.stopt0;
    stopt(1) <= cmd.stopt1;
    stopt(2) <= cmd.stopt2;
    stopt(3) <= cmd.stopt3;

    stopr(0) <= cmd.stopr0;
    stopr(1) <= cmd.stopr1;
    stopr(2) <= cmd.stopr2;
    stopr(3) <= cmd.stopr3;

    for i in 0 to 3 loop
      stt(i)  <= cmd.st(4 * i + 1 downto 4 * i);
      str(i)  <= cmd.st(4 * i + 3 downto 4 * i + 2);

      if i >= ntx then
        ldt(i)    <= (others => '0');
        startt(i) <= (others => '0');
        stopt(i)  <= (others => '0');
        stt(i)    <= off_state;
      end if;

      if i >= nrx then
        ldr(i)    <= (others => '0');
        startr(i) <= (others => '0');
        stopr(i)  <= (others => '0');
        str(i)    <= off_state;
      end if;
    end loop;
  end process;

  process(hreg, f2m)
  begin
    for i in 0 to 3 loop
      fifo2mss.wptrt(i)  <= hreg(wptrt_idx + i * 2)(fifo_ptr_width - 1 downto 0); 
      fifo2mss.rptrt(i)  <= hreg(rptrt_idx + i * 2)(fifo_ptr_width - 1 downto 0); 
      fifo2mss.wptrt1(i) <= f2m.wptrt1(i); 
      fifo2mss.rptrt1(i) <= f2m.rptrt1(i); 
      fifo2mss.rokt(i)   <= f2m.rokt(i) ;
      fifo2mss.drokt(i)  <= f2m.drokt(i);
      fifo2mss.wokt(i)   <= f2m.wokt(i) ;
      fifo2mss.dwokt(i)  <= f2m.dwokt(i);
      fifo2mss.wptrr(i)  <= hreg(wptrr_idx + i * 2)(fifo_ptr_width - 1 downto 0);
      fifo2mss.rptrr(i)  <= hreg(rptrr_idx + i * 2)(fifo_ptr_width - 1 downto 0); 
      fifo2mss.wptrr1(i) <= f2m.wptrr1(i);
      fifo2mss.rptrr1(i) <= f2m.rptrr1(i); 
      fifo2mss.rokr(i)   <= f2m.rokr(i) ;
      fifo2mss.drokr(i)  <= f2m.drokr(i);
      fifo2mss.wokr(i)   <= f2m.wokr(i) ;
      fifo2mss.dwokr(i)  <= f2m.dwokr(i);
      if i >= ntx then
        fifo2mss.wptrt(i) <= (others => '0');
        fifo2mss.rptrt(i) <= (others => '0');
        fifo2mss.wptrt1(i) <= (others => '0');
        fifo2mss.rptrt1(i) <= (others => '0');
        fifo2mss.rokt(i)   <= '0';
        fifo2mss.drokt(i)  <= '0';
        fifo2mss.wokt(i)   <= '0';
        fifo2mss.dwokt(i)  <= '0';
      end if;
      if i >= nrx then
        fifo2mss.wptrr(i) <= (others => '0');
        fifo2mss.rptrr(i) <= (others => '0');
        fifo2mss.wptrr1(i) <= (others => '0');
        fifo2mss.rptrr1(i) <= (others => '0');
        fifo2mss.rokr(i)   <= '0';
        fifo2mss.drokr(i)  <= '0';
        fifo2mss.wokr(i)   <= '0';
        fifo2mss.dwokr(i)  <= '0';
      end if;
    end loop;
  end process;

  pss2x_p: process(clk)
    variable inc: natural range 0 to 1; -- RP DEBUG RP
  begin
    if rising_edge(clk) then
      if css2pss.srstn = '0' then
        pss2txren <= (others => '0');
        pss2rxwen <= (others => '0');
        nsamplescnt <= (others => '0'); -- RP DEBUG RP
        inc := 0; -- RP DEBUG RP
      elsif css2pss.ce = '1' then
        pss2txren  <= (others => '0');
        pss2rxwen  <= (others => '0');
        nsamplescnt <= nsamplescnt + inc; -- RP DEBUG RP
        inc := 0; -- RP DEBUG RP
        for i in 0 to 3 loop
          -- If RX channel implemented and RX period of TDD (or FDD) and sample available and interface ON
          if i < nrx and (rxstarted(i) or tdd(i) = '0') and val(i) = '1' and strd(i) = on_state then
            pss2rxwen(i)    <= '1';       -- Write incoming sample in RX FIFO
            pss2rx.wdata(i) <= rxdata(i);
            if i = 0 then -- RP DEBUG RP
              inc := 1; -- RP DEBUG RP
            end if; -- RP DEBUG RP
          else
            pss2rxwen(i)    <= '0';
            pss2rx.wdata(i) <= (others => '0');
          end if;
          -- If TX channel implemented and TX period of TDD (or FDD) nd previous sample has been sampled and interface ON
          if i < ntx and (txstarted(i) or tdd(i) = '0') and ack(i) = '1' and sttd(i) = on_state then
            pss2txren(i) <= '1'; -- Request next sample from TX FIFO
          else
            pss2txren(i) <= '0';
          end if;
        end loop;
      end if;
    end if;
  end process pss2x_p;

  hreg_p: process(clk)

    variable next_hreg : word32_vector(0 to 31);
    variable status : std_ulogic_vector(23 downto 0);
    variable cnt : word32;
    variable add : word32;
    variable lvl: integer range 0 to fifo_depth;
    variable isrc, idst: natural range 0 to 31;
    variable ptr: fifo_ptr;
    variable srcexists, dstexists: boolean;

  begin

    if rising_edge(clk) then
      if css2pss.srstn = '0' then
        hreg <= (others => (others => '0')); -- Reset all hidden registers
        for i in 0 to 3 loop 
          -- Initialize TX FIFO state indicators:          FULL=false, EMPTY=true, HALF_EMPTY=true
          --                    event indicators:          HALF_EMPTIES=false, OVERFLOW=false, UNDERFLOW=false
          --                    and number of free places: LEVEL=FIFO_DEPTH
          hreg(txlvl_idx + i * 2)(eidx)   <= '1';
          hreg(txlvl_idx + i * 2)(hefidx) <= '1';
          hreg(txlvl_idx + i * 2)(fifo_ptr_width) <= '1';
          -- Initialize RX FIFO state indicators:              FULL=false, EMPTY=true, HALF_FULL=false
          --                    event indicators:              HALF_FILLS=false, OVERFLOW=false, UNDERFLOW=false
          --                    and number of occupied places: LEVEL=0
          hreg(rxlvl_idx + i * 2)(eidx)   <= '1';
        end loop;
        rxstarted  <= (others => false);
        txstarted  <= (others => false);
        strd       <= (others => off_state);
        sttd       <= (others => off_state);
        f2m        <= fifo2mss_none;
        pss2css    <= pss2css_none;
      elsif css2pss.ce = '1' then
        next_hreg := hreg;
        status := (others => '0');
        pss2css.eirq(31 downto 8) <= (others => '0');
        for i in 0 to 3 loop
          -- TX FIFOs
          if i < ntx then -- If TX channel implemented
            -- Extended interrupts
            pss2css.eirq(i * 2) <= hreg(txlvl_idx + i * 2)(uirqidx) or hreg(txlvl_idx + i * 2)(oirqidx) or hreg(txlvl_idx + i * 2)(mirqidx); 
            if sttd(i) = invalid_state then 
              status := status or adaif_invalid_state;
-- pragma translate_off
              assert false report "ADAIF: Invalid TX state" severity warning;
-- pragma translate_on
            end if;

            -- FIFO pointers and FIFO pointers + 1 updates
            ptr := std_ulogic_vector(u_unsigned(hreg(wptrt_idx + i * 2)(fifo_ptr_width - 1 downto 0)) + wopt(i));
            next_hreg(wptrt_idx + i * 2)(fifo_ptr_width - 1 downto 0) := ptr;
            ptr := std_ulogic_vector(u_unsigned(ptr) + 1);
            f2m.wptrt1(i) <= ptr;
            ptr := std_ulogic_vector(u_unsigned(hreg(rptrt_idx + i * 2)(fifo_ptr_width - 1 downto 0)) + ropt(i));
            next_hreg(rptrt_idx + i * 2)(fifo_ptr_width - 1 downto 0) := ptr;
            ptr := std_ulogic_vector(u_unsigned(ptr) + 1);
            f2m.rptrt1(i) <= ptr;

            -- TX counter values update 
            cnt := std_ulogic_vector(u_unsigned(hreg(txcnt_idx + i * 2)) - 1); -- Free running counter - 1
            if cnt(31) = '1' and hreg(txcnt_idx + i * 2)(31) = '0' then      -- If wrap around zero
              cnt := ldt(i);                                                 -- Reload
            end if;
            if ack(i) = '1' then -- TX sample rate tick
              next_hreg(txcnt_idx + i * 2) := cnt; -- Update free running counter
            end if; 
            if hreg(txcnt_idx + i * 2) = startt(i) then -- Start transmission period of TDD
              txstarted(i) <= true;
            end if;
            if hreg(txcnt_idx + i * 2) = stopt(i) then  -- Stop transmission period of TDD
              txstarted(i) <= false;
            end if;

            -- State changes
            if stt(i) = on_state and sttd(i) = idle_state then -- IDLE to ON state change
              if hreg(txcnt_idx + i * 2) = startt(i) then      -- If free running pointer reaches the start point
                sttd(i) <= on_state;                           -- Change state
              end if;
            else                 -- Other state changes
              sttd(i) <= stt(i); -- Change state immediately
            end if;
          end if;

          -- RX FIFOs
          if i < nrx then -- If RX channel implemented
            pss2css.eirq(i * 2 + 1) <= hreg(rxlvl_idx + i * 2)(uirqidx) or hreg(rxlvl_idx + i * 2)(oirqidx) or hreg(rxlvl_idx + i * 2)(mirqidx);
            if strd(i) = invalid_state then 
              status := status or adaif_invalid_state;
-- pragma translate_off
              assert false report "ADAIF: Invalid RX state" severity warning;
-- pragma translate_on
            end if;

            -- FIFO pointers and FIFO pointers + 1 updates
            ptr := std_ulogic_vector(u_unsigned(hreg(wptrr_idx + i * 2)(fifo_ptr_width - 1 downto 0)) + wopr(i));
            next_hreg(wptrr_idx + i * 2)(fifo_ptr_width - 1 downto 0) := ptr;
            ptr := std_ulogic_vector(u_unsigned(ptr) + 1);
            f2m.wptrr1(i) <= ptr;
            ptr := std_ulogic_vector(u_unsigned(hreg(rptrr_idx + i * 2)(fifo_ptr_width - 1 downto 0)) + ropr(i));
            next_hreg(rptrr_idx + i * 2)(fifo_ptr_width - 1 downto 0) := ptr;
            ptr := std_ulogic_vector(u_unsigned(ptr) + 1);
            f2m.rptrr1(i) <= ptr;

            -- RX counter values update 
            cnt := std_ulogic_vector(u_unsigned(hreg(rxcnt_idx + i * 2)) - 1); -- Free running counter - 1
            if cnt(31) = '1' and hreg(rxcnt_idx + i * 2)(31) = '0' then      -- If wrap around zero
              cnt := ldr(i);                                                 -- Reload
            end if;
            if val(i) = '1' then -- RX sample tick
              next_hreg(rxcnt_idx + i * 2) := cnt; -- Update free running counter
            end if; 
            if hreg(rxcnt_idx + i * 2) = startr(i) then -- Start receiving period of TDD
              rxstarted(i) <= true;
            end if;
            if hreg(rxcnt_idx + i * 2) = stopr(i) then  -- Stop receiving period of TDD
              rxstarted(i) <= false;
            end if;

            -- State changes
            if str(i) = on_state and strd(i) = idle_state then -- IDLE to ON state change
              if hreg(rxcnt_idx + i * 2) = startr(i) then      -- If free running pointer reaches the start point
                strd(i) <= on_state;                           -- Change state
              end if;
            else                 -- Other state changes
              strd(i) <= str(i); -- Change state immediately
            end if;
          end if;
        end loop;

        -- Actions during operation management
        isrc := to_integer(u_unsigned(cmd.src)); -- Index of source hidden register
        srcexists := is_hidden_register_implemented(isrc, ntx, nrx); -- Is source hidden register implemented?
        idst := to_integer(u_unsigned(cmd.dst)); -- Index of destination hidden register
        dstexists := is_hidden_register_implemented(idst, ntx, nrx); -- Is destination hidden register implemented?
        pss2css.err <= '0'; -- Assume no error
        pss2css.eoc <= '0'; -- No End Of Computation
        add := std_ulogic_vector(u_unsigned(next_hreg(isrc)) + u_unsigned(cmd.data)); -- Result of an ADD operation
        if css2pss.exec = '1' then -- If EXEC request from CSS, action
          pss2css.eoc <= '1'; -- End Of Computation (operations on hidden registers are one-cycle only)
          if cmd.op /= set_op and ((cmd.op /= get_op and isrc >= txlvl_idx) or (not srcexists)) then -- Invalid source index
            pss2css.err <= '1'; -- Error
            status      := status or adaif_invalid_src;
-- pragma translate_off
            assert false report "ADAIF: Invalid source index" severity warning;
-- pragma translate_on
          elsif cmd.op /= get_op and ((idst >= txlvl_idx) or (not dstexists)) then -- Invalid destination index
            pss2css.err <= '1'; -- Error
            status      := status or adaif_invalid_dst;
-- pragma translate_off
            assert false report "ADAIF: Invalid destination index" severity warning;
-- pragma translate_on
          else
            case cmd.op is 
              when get_op => -- GET operation
                pss2css.data <= hreg(isrc); -- Get source register value
                if isrc >= txlvl_idx then   -- Get level register => clear interrupt pending flags
                  next_hreg(isrc)(mirqidx) := '0';
                  next_hreg(isrc)(oirqidx) := '0';
                  next_hreg(isrc)(uirqidx) := '0';
                end if;
              when set_op => -- SET operation
                next_hreg(idst) := cmd.data; -- Set destination register to provided value
                pss2css.data <= cmd.data;    -- Return set data as command result
              when add_op => -- ADD operation
                next_hreg(idst) := add; -- Set destination register to result of ADD operation
                pss2css.data <= add;    -- Return set data as command result
              when cpy_op => -- COPY operation
                next_hreg(idst) := next_hreg(isrc); -- Set destination register to next source register value
                pss2css.data <= next_hreg(isrc);    -- Return set data as command result
              when others => NULL;
            end case;
          end if;
        end if;
        -- Levels, underflow-overflow and threshold crossing of FIFOs
        for i in 0 to 3 loop
          -- TX FIFO
          if i < ntx then
            lvl  := to_integer(u_unsigned(hreg(txlvl_idx + i * 2)(fifo_ptr_width downto 0))) - get_inc(wopt(i), ropt(i)); -- New number of free places in FIFO

            f2m.rokt(i)  <= to_stdulogic(lvl < fifo_depth);     -- FIFO not empty => one read is OK
            f2m.drokt(i) <= to_stdulogic(lvl < fifo_depth - 1); -- FIFO contains at least two samples => double read is OK
            f2m.wokt(i)  <= to_stdulogic(lvl > 0);              -- At least 1 free place in FIFO => one write is OK
            f2m.dwokt(i) <= to_stdulogic(lvl > 1);              -- At least 2 free places in FIFO => double write is OK

            next_hreg(txlvl_idx + i * 2)(fifo_ptr_width downto 0) := std_ulogic_vector(to_unsigned(lvl, fifo_ptr_width + 1)); -- New number of free places
            if lvl = fifo_depth then -- If FIFO empty
              next_hreg(txlvl_idx + i * 2)(eidx) := '1';
            else
              next_hreg(txlvl_idx + i * 2)(eidx) := '0';
            end if;
            if lvl = 0 then -- If FIFO full
              next_hreg(txlvl_idx + i * 2)(fidx) := '1';
            else
              next_hreg(txlvl_idx + i * 2)(fidx) := '0';
            end if;
            if lvl >= fifo_threshold then -- If FIFO half-empty
              next_hreg(txlvl_idx + i * 2)(hefidx) := '1';
            else
              next_hreg(txlvl_idx + i * 2)(hefidx) := '0';
            end if;
            if tx2pss.udf(i) = '1' then -- If FIFO underflows
              next_hreg(txlvl_idx + i * 2)(uirqidx) := '1';
            end if;
            if tx2pss.ovf(i) = '1' then -- If FIFO overflows
              next_hreg(txlvl_idx + i * 2)(oirqidx) := '1';
            end if;
            if next_hreg(txlvl_idx + i * 2)(hefidx) = '1' and hreg(txlvl_idx + i * 2)(hefidx) = '0' then -- If FIFO half-empties
              next_hreg(txlvl_idx + i * 2)(mirqidx) := '1';
            end if;
          end if;
          -- RX FIFO
          if i < nrx then
            lvl  := to_integer(u_unsigned(hreg(rxlvl_idx + i * 2)(fifo_ptr_width downto 0))) + get_inc(wopr(i), ropr(i)); -- New number of occupied places in FIFO

            f2m.rokr(i)  <= to_stdulogic(lvl > 0);              -- FIFO not empty => one read is OK
            f2m.drokr(i) <= to_stdulogic(lvl > 1);              -- FIFO contains at least two samples => double read is OK
            f2m.wokr(i)  <= to_stdulogic(lvl < fifo_depth);     -- At least 1 free place in FIFO => one write is OK
            f2m.dwokr(i) <= to_stdulogic(lvl < fifo_depth - 1); -- At least 2 free places in FIFO => double write is OK

            next_hreg(rxlvl_idx + i * 2)(fifo_ptr_width downto 0) := std_ulogic_vector(to_unsigned(lvl, fifo_ptr_width + 1)); -- New number of occupied places
            if lvl = 0 then -- If FIFO empty
              next_hreg(rxlvl_idx + i * 2)(eidx) := '1';
            else
              next_hreg(rxlvl_idx + i * 2)(eidx) := '0';
            end if;
            if lvl = fifo_depth then -- If FIFO full
              next_hreg(rxlvl_idx + i * 2)(fidx) := '1';
            else
              next_hreg(rxlvl_idx + i * 2)(fidx) := '0';
            end if;
            if lvl >= fifo_threshold then -- If FIFO half-full
              next_hreg(rxlvl_idx + i * 2)(hefidx) := '1';
            else
              next_hreg(rxlvl_idx + i * 2)(hefidx) := '0';
            end if;
            if rx2pss.udf(i) = '1' then -- If FIFO underflows
              next_hreg(rxlvl_idx + i * 2)(uirqidx) := '1';
            end if;
            if rx2pss.ovf(i) = '1' then -- If FIFO overflows
              next_hreg(rxlvl_idx + i * 2)(oirqidx) := '1';
            end if;
            if next_hreg(rxlvl_idx + i * 2)(hefidx) = '1' and hreg(rxlvl_idx + i * 2)(hefidx) = '0' then -- If FIFO half-fills
              next_hreg(rxlvl_idx + i * 2)(mirqidx) := '1';
            end if;
          end if;
-- pragma translate_off
          iut(i) <= next_hreg(txlvl_idx + i * 2)(uirqidx);
          iot(i) <= next_hreg(txlvl_idx + i * 2)(oirqidx);
          iur(i) <= next_hreg(rxlvl_idx + i * 2)(uirqidx);
          ior(i) <= next_hreg(rxlvl_idx + i * 2)(oirqidx);
          lvlr(i) <= next_hreg(rxlvl_idx + i * 2);
          lvlt(i) <= next_hreg(rxlvl_idx + i * 2);
-- pragma translate_on
        end loop;
        pss2css.status <= status;
        hreg <= (others => (others => '0'));
        for i in 0 to 3 loop
          if i < ntx then
            hreg(wptrt_idx + i * 2) <= next_hreg(wptrt_idx + i * 2);
            hreg(rptrt_idx + i * 2) <= next_hreg(rptrt_idx + i * 2);
            hreg(txcnt_idx + i * 2) <= next_hreg(txcnt_idx + i * 2);
            hreg(txlvl_idx + i * 2) <= next_hreg(txlvl_idx + i * 2);
          end if;
          if i < nrx then
            hreg(wptrr_idx + i * 2) <= next_hreg(wptrr_idx + i * 2);
            hreg(rptrr_idx + i * 2) <= next_hreg(rptrr_idx + i * 2);
            hreg(rxcnt_idx + i * 2) <= next_hreg(rxcnt_idx + i * 2);
            hreg(rxlvl_idx + i * 2) <= next_hreg(rxlvl_idx + i * 2);
          end if;
        end loop;
      end if;
    end if;
  end process hreg_p;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
