--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Emulator of AD converter for simulation
--*
--* @description
--* adc_emulator emulates a AD converter. It generates a sampling clock rxclk, a valid signal rxvalid and 32 bits data samples rxdata. The data samples are
--* supposed to be sampled on rising edges of rxclk for which rxvalid is set. rxvalid is generated randomly but in such a way that two consecutive sampling
--* points are separated by at least smplpmin. To allow the checking of the received samples they are generated from a 14 bits counter CNT that starts at 0
--* and wraps back to 0 around 2^14-1. The samples are always {4*CNT+1,4*CNT+3} when rxvalid is set and {2*R1,2*R2} when rxvalid is unset, where R1 and R2 are
--* two random 15-bits values. The receiver shall thus sample consecutive odd natural numbers and shall never sample an even sample. If it does or if the
--* sequence of samples is not a series of consecutive odd natural numbers, it may be the sign that things go wrong.

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.global.all;

entity adc_emulator is
  generic(rxclkperiod: time     := 13 ns;  -- rxclk clock period
          smplpmin:    time     := 25 ns); -- Minimum sample period
  port(
    rxclk:   out std_ulogic; -- The receiver must samples RXDATA on RXCLK rising edges for which RXVALID is set
    rxvalid: out std_ulogic; -- When set on a RXCLK rising edge, indicates that the RXDATA output must be sampled by the receiver
    rxdata:  out std_ulogic_vector(31 downto 0); -- real part on left half (31 downto 16), imaginary part on right half (15 downto 0)
    eos:     in  boolean -- Stopper
  );
end entity adc_emulator;

architecture sim of adc_emulator is

  signal rxclk_local, rxclk_internal: std_ulogic;
  signal rxvalid_local: std_ulogic;

begin

  rxclk       <= rxclk_internal;
  rxclk_local <= rxclk_internal;
  rxvalid     <= rxvalid_local;

  process
  begin
    rxclk_internal <= '0';
    wait for rxclkperiod / 2;
    rxclk_internal <= '1';
    wait for rxclkperiod / 2;
    if eos then
      wait;
    end if;
  end process;

  process
    variable t: time   := 0 ns;
    variable cnt: u_unsigned(15 downto 2) := (others => '0');
  begin
    rxvalid_local <= '0';
    rxdata        <= std_ulogic_vector_rnd(15) & '0' & std_ulogic_vector_rnd(15) & '0';
    loop
      wait until rising_edge(rxclk_local);
      if rxvalid_local = '1' then -- If now is a sampling point
        t := now;                 -- Record time of sampling
      end if;
      -- If time of next rising edge of rxclk is separated from last sampling point by more than minimum, randomly decide to send the next sample
      if now + rxclkperiod - t > smplpmin and boolean_rnd then
        rxvalid_local <= '1';
        rxdata        <= std_ulogic_vector(cnt) & "01" & std_ulogic_vector(cnt) & "11"; -- {4*CNT+1,4*CNT+3}
        cnt           := cnt + 1; -- Increment counter
      else -- Last sampling too close or random generator thrown "no sample"
        rxvalid_local <= '0';
        rxdata        <= std_ulogic_vector_rnd(15) & '0' & std_ulogic_vector_rnd(15) & '0';
      end if;
    end loop;
  end process;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
