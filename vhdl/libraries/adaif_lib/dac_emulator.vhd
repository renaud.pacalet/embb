--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Emulator of DA converter for simulation
--*
--* @description
--* dac_emulator emulates a DA converter. It generates a sampling clock txclk and an acknowledge signal txack and samples its input data txdata on rising edges
--* of txclk for which txack is set. txack is generated randomly but in such a way that two consecutive sampling points are separated by at least smplpmin. The
--* sampled data are checked against expected values:
--* - An error is raised if the first sampled data is non 0.
--* - An error is raised if a series of non 0 samples comprises less than 3 values.
--* - In any series of samples of the form ...,0,S1,...,Sn,Sn+1,Sn+2,0,... where n>=1 and S1,...,Sn+2 are non 0, let C=CRC32(S1,...,Sn).
--* An error is raised if Sn+1 != n or Sn+2 != (C == 0 ?= 1 : C). CRC32 is based on the 0x04C11DB7 polynomial and is implemented in global_lib.sim_utils as
--* function crc32_0x104c11db7.

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.global.all;
use global_lib.sim_utils.all;

entity dac_emulator is
  generic(txclkperiod: time     := 13 ns;  -- txclk clock period
          smplpmin:    time     := 25 ns); -- Minimum sample period
  port(
    txclk:   out std_ulogic; -- The DAC samples TXDATA on TXCLK rising edges for which TXACK is set
    txack:   out std_ulogic; -- When set on a TXCLK rising edge, indicates that the TXDATA output has been sampled by the DAC
    txdata:  in  std_ulogic_vector(31 downto 0); -- Data sample
    eos:     in  boolean -- Stopper
  );
end entity dac_emulator;

architecture sim of dac_emulator is

  signal txclk_local: std_ulogic;
  signal txack_local: std_ulogic;

begin

  txclk <= txclk_local;
  txack <= txack_local;

  process
  begin
    txclk_local <= '0';
    wait for txclkperiod / 2;
    txclk_local <= '1';
    wait for txclkperiod / 2;
    if eos then
      wait;
    end if;
  end process;

  process(txclk_local)
    variable t: time := 0 ns;
  begin
    if rising_edge(txclk_local) then
      if txack_local = '1' then -- If now is a sampling point
        t := now;               -- Record time of sampling
      end if;
      if now + txclkperiod - t > smplpmin then -- If next rising edge of txclk is separated from last sampling by more than the minimum
        txack_local <= std_ulogic_rnd;         -- Randomly decide to sample
      end if;
    end if;
  end process;

  process(txclk_local)
    constant s0: word32 := (others => '0');
    variable val: word32_vector(0 to 2) := (others => s0); -- Sliding window on sampled data
    variable crc: word32;
    variable n: natural; -- Counter of samples
    type state is (reset, idle, run, runok); -- Current state of DAC emulator: reset = "not yet started", idel = "started but waiting for the first non 0
                                             -- sample", run = "received one or two non 0 samples", runok: "received at least 3 non zero samples"
    variable st: state := reset;
    variable l: line;
  begin
    if rising_edge(txclk_local) and txack_local = '1' then -- If sampling time
      val := val(1 to 2) & txdata; -- Move sliding window
      case st is
        when reset =>
          assert txdata = s0 report "First sample non 0" severity failure;
          st := idle;
        when idle =>
          if txdata /= s0 then
            st := run;
          end if;
        when run =>
          assert txdata /= s0 report "Empty non 0 sequence" severity failure;
          if val(0) /= s0 then -- val(0) is the first non 0 sample of a sequence
            st := runok;
            crc := X"FFFFFFFF";
            crc32_0x104c11db7(crc, val(0));
            n := 1;
          end if;
        when runok =>
          if txdata /= s0 then -- Sequence continues
            crc32_0x104c11db7(crc, val(0));
            n := n + 1;
          else -- Sequence ends, val(0) shall be its length and val(1) its CRC32
            assert to_integer(u_unsigned(val(0))) = n report "Wrong sequence length" severity failure;
            assert val(1) = crc report "Wrong CRC32" severity failure;
            write(l, string'("Sequence length: "));
            write(l, n);
            writeline(output, l);
            write(l, string'("Sequence CRC32:  0x"));
            hwrite(l, crc);
            writeline(output, l);
            val := (others => s0);
            st := idle;
          end if;
      end case;
    end if;
  end process;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
