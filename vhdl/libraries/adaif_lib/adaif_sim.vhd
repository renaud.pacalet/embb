--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for ADAIF

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.sim_utils.all;

use work.adaif_pkg.all;
use work.bitfield.all;

entity adaif_sim is
  generic(cmdfile:  string := "adaif.cmd";
          debug:    boolean := false;    --* Print debug information
          verbose:  boolean := false;    --* Print more debug information
          txdepth:  positive := 2;          -- Depth of TX synchronizers
          rxdepth:  positive := 2;          -- Depth of RX synchronizers
          n0:       positive := 1;  --* Number of input pipeline registers in MSS, including input registers of RAMs
          n1:       positive := 1); --* Number of output pipeline registers in MSS, including output registers of RAMs
end entity adaif_sim;

architecture sim of adaif_sim is

  signal dbg:        debug_type;    -- debug out
  signal clk:        std_ulogic;    -- master clock
  signal srstn:      std_ulogic;    -- master reset
  signal ce:         std_ulogic;    -- chip enable
  signal hirq:       std_ulogic;    -- host input irq,
  signal pirq:       std_ulogic;    -- PSS output irq,
  signal dirq:       std_ulogic;    -- DMA output irq,
  signal uirq:       std_ulogic;    -- UC output irq,
  signal eirq:       std_ulogic_vector(7 downto 0); -- Extended output irq,
  signal pp2adaif:   pp2adaif_type; -- PreProcessor input
  signal adaif2pp:   adaif2pp_type; -- PreProcessor output
  signal adac2adaif: adac2adaif_type; 
  signal adaif2adac: adaif2adac_type;
  signal hst2tvci:   vci_i2t_type;  -- Target AVCI input
  signal tvci2hst:   vci_t2i_type;  -- Target AVCI output
  signal hst2ivci:   vci_t2i_type;  -- Initiator AVCI input
  signal ivci2hst:   vci_i2t_type;  -- Initiator AVCI output
  signal gpodvalid:  boolean;
  signal gpod:       word64;
  signal gpovvalid:  boolean;
  signal gpov:       word64;
  signal eos:        boolean;       -- End Of Simulation

begin

  i_adaif: entity work.adaif(rtl)
  generic map(
    debug     => debug,
    verbose   => verbose,
    with_uc   => false,
    nrx       => 1,
    ntx       => 0,
    txdepth   => txdepth,
    rxdepth   => rxdepth,
    n0        => n0,
    n1        => n1)
  port map(dbg      => dbg,
           nsamples => open,
           clk      => clk,
           srstn    => srstn,
           ce       => ce,
           hirq     => hirq,
           pirq     => pirq,
           dirq     => dirq,
           uirq     => uirq,
           eirq     => eirq,
           pp2adaif => pp2adaif,
           adaif2pp => adaif2pp,
           adac2adaif => adac2adaif,
           adaif2adac => adaif2adac,
           tvci_in  => hst2tvci,
           tvci_out => tvci2hst,
           ivci_in  => hst2ivci,
           ivci_out => ivci2hst);

  -- Emulates an ADC. Randomly sends valid samples (but never faster than one every smplpmin). Sent valid samples are consecutive odd values. Non-valid output
  -- samples are random even values.
  i_adc: entity work.adc_emulator(sim)
  generic map(rxclkperiod => 13 ns,
              smplpmin    => 25 ns)
  port map(rxclk   => adac2adaif.rxclk(0),
           rxvalid => adac2adaif.rxvalid(0),
           rxdata  => adac2adaif.rxdata(0),
           eos     => eos);

  -- Host emulator: emulates the whole surrounding Embb (crossbar, main CPU, etc.)
  i_hst: entity global_lib.hst_emulator(arc)
	generic map(cmdfile   => cmdfile,
              n_pa_regs => bitfield_width / 64,
              neirq     => 8)
  port map(clk       => clk,
           srstn     => srstn,
           ce        => ce,
           hirq      => hirq,
           pirq      => pirq,
           dirq      => dirq,
           uirq      => uirq,
           eirq      => eirq,
           hst2tvci  => hst2tvci,
           tvci2hst  => tvci2hst,
           hst2ivci  => hst2ivci,
           ivci2hst  => ivci2hst,
           gpodvalid => gpodvalid,
           gpod      => gpod,
           gpovvalid => gpovvalid,
           gpov      => gpov,
           eos       => eos);

  -- Check that samples sent to the GPOD port are of the form {4X+1,4X+3} where X is a counter that wraps around 2**14-1.
  -- At the send of the simulation prints the number of times the check failed.
  process(clk, eos)
    variable nxt, tmp: u_unsigned(31 downto 0);
    variable bad, nocontig: natural;
    variable l: line;
    type states is (idle, first, running);
    variable state: states;
    variable notnew: boolean;
    variable cnt: natural;
    variable msk: std_ulogic_vector(7 downto 0);
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        nxt := (others => '0');
        bad := 0;
        nocontig := 0;
        state := idle;
      elsif gpodvalid then
        if state = idle then -- Control information
          cnt := to_integer(u_unsigned(gpod(63 downto 32)));
          notnew := gpod(8) = '0';
          msk := gpod(7 downto 0);
          if notnew then
            state := running;
          else
            state := first;
          end if;
          write(l, string'("Expecting "));
          write(l, cnt);
          if notnew then
            write(l, string'(" next"));
          else
            write(l, string'(" new"));
          end if;
          write(l, string'(" samples (mask=0x"));
          hwrite(l, msk);
          write(l, string'(")"));
          writeline(output, l);
        else
          assert cnt /= 0 report "Unexpected extra data on GPOD" severity failure;
          for i in 1 downto 0 loop
            if msk(3 + 4 * i downto 4 * i) = X"F" then
              tmp := u_unsigned(gpod(31 + 32 * i downto 32 * i));
              if tmp(31 downto 18) /= tmp(15 downto 2) or tmp(17 downto 16) /= "01" or tmp(1 downto 0) /= "11" then
                write(l, now);
                write(l, string'(": **** BAD SAMPLE: "));
                hwrite(l, tmp);
                writeline(output, l);
                bad := bad + 1;
                nxt := tmp;
              end if;
              if state = first then
                state := running;
                nxt := tmp;
              end if;
              if tmp /= nxt then
                write(l, now);
                write(l, string'(": **** NON CONTIGUOUS: expected "));
                hwrite(l, nxt);
                write(l, string'(" but got "));
                hwrite(l, tmp);
                writeline(output, l);
                nocontig := nocontig + 1;
              end if;
              nxt := (tmp(31 downto 18) + 1) & "01" & (tmp(15 downto 2) + 1) & "11";
              cnt := cnt - 1;
              if cnt = 0 then
                state := idle;
                exit;
              end if;
            end if;
          end loop;
        end if;
      end if;
    end if;
    if eos then
      write(l, string'("BAD SAMPLES:            "));
      write(l, bad);
      writeline(output, l);
      write(l, string'("NON CONTIGUOUS SAMPLES: "));
      write(l, nocontig);
      writeline(output, l);
    end if;
  end process;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
