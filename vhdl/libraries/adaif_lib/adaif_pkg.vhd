--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief ADAIF utility package

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library memories_lib;
use memories_lib.ram_pkg.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

use work.bitfield.all;

package adaif_pkg is

  constant fifo_ptr_width: natural := 11; -- Bit width of FIFOs read and write pointers
  constant fifo_depth: natural := 2**fifo_ptr_width; -- Depth of FIFOs
  constant fifo_threshold: natural := fifo_depth / 2; -- Threshold for "half full" and "half empty" signalling
  subtype fifo_ptr is std_ulogic_vector(fifo_ptr_width - 1 downto 0); -- Type of FIFOs read and write pointers
  type fifo_ptr_vector is array(natural range <>) of fifo_ptr; -- Type of vectors of FIFOs read and write pointers

  constant lvl_width: natural := fifo_ptr_width + 1; -- Bit width of FIFOs levels
  constant lvl_depth: natural := 2**lvl_width;       -- Range of FIFOs levels

  -- Indexes of hidden registers
  constant wptrt_idx : natural := 0;  -- TX0 write pointer
  constant wptrr_idx : natural := 1;  -- RX0 write pointer
  constant rptrt_idx : natural := 8;  -- TX0 read pointer
  constant rptrr_idx : natural := 9;  -- RX0 read pointer
  constant txcnt_idx : natural := 16; -- TX0 free running counter
  constant rxcnt_idx : natural := 17; -- RX0 free running counter
  constant txlvl_idx : natural := 24; -- TX0 level
  constant rxlvl_idx : natural := 25; -- RX0 level
  -- indexes of [mou]irq interrupt pending flags in level hidden registers
  constant uirqidx: natural := 16; -- underflow
  constant oirqidx: natural := 17; -- overflow
  constant mirqidx: natural := 18; -- half-empties or half-fills
  constant hefidx:  natural := 19; -- half-empty or half-full
  constant eidx:    natural := 20; -- empty
  constant fidx:    natural := 21; -- full

  -- Considering number of implemented TX channels (ntx) and RX channels (nrx) returns true is hidden register idx is implemented
  function is_hidden_register_implemented(idx: natural range 0 to 31; ntx, nrx: natural range 0 to 3) return boolean;

  -- error statuses
  constant adaif_invalid_state: status_type := X"000001";
  constant adaif_invalid_src:   status_type := X"000002";
  constant adaif_invalid_dst:   status_type := X"000004";

  subtype state_type is std_ulogic_vector(1 downto 0);
  type state_vector is array(natural range <>) of state_type;

  constant off_state: state_type := "00";
  constant on_state:  state_type := "01";
  constant idle_state:  state_type := "10";
  constant invalid_state:  state_type := "11";

  constant cmdsize:  positive := bitfield_width/8;

  subtype v2 is std_ulogic_vector(1 downto 0);
  constant get_op: v2 := "00";
  constant set_op: v2 := "01";
  constant add_op: v2 := "10";
  constant cpy_op: v2 := "11";

  subtype lvl_type is integer range 0 to fifo_depth; 
  type lvl_vector is array (natural range <>) of lvl_type;

  type debug_type is record 
    param   : std_ulogic_vector(cmdsize * 8 - 1 downto 0);
    eoc     : std_ulogic;  
    err     : std_ulogic;  
    status  : status_type;
    eirq    : std_ulogic_vector(7 downto 0); 
    data    : data_type;   
    tts     : state_vector(0 to 3);
    trs     : state_vector(0 to 3);
    iut     : std_ulogic_vector(0 to 3);
    iur     : std_ulogic_vector(0 to 3);
    iot     : std_ulogic_vector(0 to 3);
    ior     : std_ulogic_vector(0 to 3);
    lvlr    : word32_vector(0 to 3); 
    lvlt    : word32_vector(0 to 3); 
  end record;

  type adac2adaif_type is record
    rxclk   : std_ulogic_vector(0 to 3);
    rxvalid : std_ulogic_vector(0 to 3);
    rxdata  : word32_vector(0 to 3);
    txclk   : std_ulogic_vector(0 to 3);
    txack   : std_ulogic_vector(0 to 3);
  end record;
  
  type adaif2adac_type is record
    txdata  : word32_vector(0 to 3);
  end record;

  subtype op is natural range 0 to 2;

  type op_vector is array(natural range <>) of op;

  type ucr_in_type is array(ports) of in_port_1kx32;
  type tx_in_type is array(0 to 3, 0 to 1, ports) of in_port_1kx32;
  type rx_in_type is array(0 to 3, 0 to 1, ports) of in_port_1kx32;

  type mss2rams_type is record
    ucr: ucr_in_type;
    tx:  tx_in_type;
    rx:  rx_in_type;
  end record;
  type mss2rams_vector is array(natural range <>) of mss2rams_type;

  type ucr_out_type is array(ports) of word32;
  type tx_out_type is array(0 to 3, 0 to 1, ports) of word32;
  type rx_out_type is array(0 to 3, 0 to 1, ports) of word32;

  type rams2mss_type is record
    ucr: ucr_out_type;
    tx:  tx_out_type;
    rx:  rx_out_type;
  end record;
  type rams2mss_vector is array(natural range <>) of rams2mss_type;
   
  type mss2fifo_type is record
    wopt : op_vector(0 to 3); --Feedback operand code for TX fifo wptr 
    ropt : op_vector(0 to 3); --Feedback operand code for TX fifo rptr
    wopr : op_vector(0 to 3); --Feedback operand code for RX fifo wptr
    ropr : op_vector(0 to 3); --Feedback operand code for TX fifo rptr
  end record;

  constant mss2fifo_none : mss2fifo_type := (wopt => (others => 0), ropt => (others => 0), wopr => (others => 0), ropr => (others => 0));

  type fifo2mss_type is record
    wptrt  : fifo_ptr_vector(0 to 3);           --Write pointers of TX fifo 
    rptrt  : fifo_ptr_vector(0 to 3);           --Read  pointers of TX fifo 
    wptrr  : fifo_ptr_vector(0 to 3);           --Write pointers of RX fifo 
    rptrr  : fifo_ptr_vector(0 to 3);           --Read pointers of RX fifo 
    wptrt1 : fifo_ptr_vector(0 to 3);           --Write pointers of TX fifo + 1
    rptrt1 : fifo_ptr_vector(0 to 3);           --Read  pointers of TX fifo + 1 
    wptrr1 : fifo_ptr_vector(0 to 3);           --Write pointers of RX fifo + 1 
    rptrr1 : fifo_ptr_vector(0 to 3);           --Read pointers of RX fifo + 1 
    rokt   : std_ulogic_vector(0 to 3);       --Read allowed in TX fifo 
    wokt   : std_ulogic_vector(0 to 3);       --Write allowed in TX fifo 
    rokr   : std_ulogic_vector(0 to 3);       --Read allowed in RX fifo 
    wokr   : std_ulogic_vector(0 to 3);       --Write allowed in TX fifo 
    drokt  : std_ulogic_vector(0 to 3);       --Double read allowed in TX fifo 
    dwokt  : std_ulogic_vector(0 to 3);       --Double write allowed in TX fifo 
    drokr  : std_ulogic_vector(0 to 3);       --Double read allowed in RX fifo 
    dwokr  : std_ulogic_vector(0 to 3);       --Double write allowed in TX fifo 
  end record;

  constant fifo2mss_none : fifo2mss_type := (wptrt => (others => (others => '0')),
                                             rptrt => (others => (others => '0')),
                                             wptrr => (others => (others => '0')),
                                             rptrr => (others => (others => '0')),
                                             wptrt1 => (others => (others => '0')),
                                             rptrt1 => (others => (others => '0')),
                                             wptrr1 => (others => (others => '0')),
                                             rptrr1 => (others => (others => '0')),
                                             rokt => (others => '0'),
                                             wokt => (others => '0'),
                                             rokr => (others => '0'),
                                             wokr => (others => '0'),
                                             drokt => (others => '0'), 
                                             dwokt => (others => '0'),
                                             drokr => (others => '0'),
                                             dwokr => (others => '0'));

  type pss2tx_type is record
    ren   : std_ulogic_vector(0 to 3);
    wen   : std_ulogic_vector(0 to 3);
    wdata : word32;
  end record;
    
  type pss2rx_type is record
    ren   : std_ulogic_vector(0 to 3);
    wen   : std_ulogic_vector(0 to 3);
    wdata : word32_vector(0 to 3);
  end record;

  type tx2pss_type is record
    en : std_ulogic_vector(0 to 3);
    rdata : word32_vector(0 to 3);
    rgnt : std_ulogic_vector(0 to 3);
    wgnt : std_ulogic;
    ovf : std_ulogic_vector(0 to 3); -- overflow flags (raised by MSS when writting a full FIFO)
    udf : std_ulogic_vector(0 to 3); -- underflow flags (raised by MSS when reading an empty FIFO)
  end record;
  
  type rx2pss_type is record
    en : std_ulogic;
    rdata : word32;
    rgnt : std_ulogic;
    ovf : std_ulogic_vector(0 to 3); -- overflow flags (raised by MSS when writting a full FIFO)
    udf : std_ulogic_vector(0 to 3); -- underflow flags (raised by MSS when reading an empty FIFO)
  end record;

  type pss2mss_type is record
    pss2rx: pss2rx_type;
    pss2tx: pss2tx_type;
  end record;

  type mss2pss_type is record
    rx2pss: rx2pss_type;
    tx2pss: tx2pss_type;
  end record;

  type pp2adaif_type is record
    d : word32;
    en : std_ulogic_vector(0 to 3);
    rnw : std_ulogic;
  end record;
  
  type adaif2pp_type is record
    d : word32;
    en : std_ulogic;
    gnt : std_ulogic;
  end record;

  constant adaif2pp_none : adaif2pp_type := (d => (others => '0'), en => '0', gnt => '0');
  constant pp2adaif_none : pp2adaif_type := (d => (others => '0'), en => (others => '0'), rnw => '0');

  function compare(opa, opb : natural range 0 to 2) return std_ulogic;

  function next_i(v: std_ulogic_vector; idx :  natural range 0 to 3; it : natural) return std_ulogic_vector;
  function next_r(v: std_ulogic_vector; idx :  natural range 0 to 3; it : natural) return std_ulogic_vector;
  function next_r(v: std_ulogic_vector; idx :  natural range 0 to 3) return std_ulogic_vector;
  function next_i(v: std_ulogic_vector; idx : natural range 0 to 3) return std_ulogic_vector;
 
  function get_inc(winc, rinc : natural range 0 to 2) return integer;

  subtype gray2_type is std_ulogic_vector(1 downto 0);

  constant gray2_idle1: gray2_type := "00";
  constant gray2_idle2: gray2_type := "10";
  constant gray2_val1: gray2_type := "01";
  constant gray2_val2: gray2_type := "11";

  type gray2_vector is array(natural range <>) of gray2_type;

  function gray_encode(nxt: std_ulogic; cur: gray2_type) return gray2_type;
  function gray_decode(cur: gray2_type; prv: gray2_type) return std_ulogic;

end package adaif_pkg;
  
package body adaif_pkg is

  function is_hidden_register_implemented(idx: natural range 0 to 31; ntx, nrx: natural range 0 to 3) return boolean is
  begin
    return (idx mod 2 = 0 and (idx mod 8) / 2 < ntx) or (idx mod 2 = 1 and (idx mod 8) / 2 < nrx);
  end function is_hidden_register_implemented;

  function compare(opa, opb : natural range 0 to 2) return std_ulogic is
    variable ret : std_ulogic;
  begin
     ret := '0';
     if opa > opb then 
       ret := '1';
     end if;
     return ret;
  end function compare;

  function next_r(v: std_ulogic_vector; idx :  natural range 0 to 3; it : natural) return std_ulogic_vector is
    variable r : word12 := v;
  begin
    for j in 0 to it loop
      r := next_r(r, idx);
    end loop;
    return r;
  end function next_r;
  
  function next_i(v: std_ulogic_vector; idx :  natural range 0 to 3; it : natural) return std_ulogic_vector is
    variable i : word12 := v;
  begin
    for j in 0 to it loop
      i := next_i(i, idx);
    end loop;
    return i;
  end function next_i;
      
  function next_r(v: std_ulogic_vector; idx : natural range 0 to 3) return std_ulogic_vector is
  begin
    if (u_unsigned(v) + idx + 1) = 0 then 
      return std_ulogic_vector(u_unsigned(v) + idx + 2);
    else
      return std_ulogic_vector(u_unsigned(v) + idx + 1);
    end if;
 end function next_r;

  function next_i(v: std_ulogic_vector; idx : natural range 0 to 3) return std_ulogic_vector is
  begin
    if (u_unsigned(v) - idx - 1) = 0 then 
      return std_ulogic_vector(u_unsigned(v) - idx - 2);
    else
      return std_ulogic_vector(u_unsigned(v) - idx - 1);
    end if;
  end function next_i;

  function get_inc(winc, rinc : natural range 0 to 2) return integer is
  begin
		case winc is 
		when 2 => 
			case rinc is
				when 2 => return 0;
				when 1 => return 1;
				when 0 => return 2;
			end case;
		when 1 => 
			case rinc is
				when 2 => return -1;
				when 1 => return 0;
				when 0 => return 1;
			end case;
		when 0 => 
			case rinc is
				when 2 => return -2;
				when 1 => return -1;
				when 0 => return 0;
			end case;
		end case;
  end function get_inc;

  function gray_encode(nxt: std_ulogic; cur: gray2_type) return gray2_type is
    variable res: gray2_type := cur;
  begin
    case cur is
      when gray2_idle1 =>
        if nxt = '1' then
          res := gray2_val1;
        end if;
      when gray2_idle2 =>
        if nxt = '1' then
          res := gray2_val2;
        end if;
      when gray2_val1 =>
        if nxt = '1' then
          res := gray2_val2;
        else
          res := gray2_idle1;
        end if;
      when gray2_val2 =>
        if nxt = '1' then
          res := gray2_val1;
        else
          res := gray2_idle2;
        end if;
      when others =>
        null;
    end case;
    return res;
  end function gray_encode;

  function gray_decode(cur: gray2_type; prv: gray2_type) return std_ulogic is
  begin
    if prv = cur then
      return '0';
    elsif cur = gray2_val1 or cur = gray2_val2 then
      return '1';
    else
      return '0';
    end if;
  end function gray_decode;

end package body adaif_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
