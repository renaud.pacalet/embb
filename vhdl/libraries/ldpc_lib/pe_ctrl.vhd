--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
-- use ieee.numeric_std.all;

-- library global_lib;
-- use global_lib.global.all;

use work.ldpc_pkg_local.all;
use work.ldpc_sim_pkg.all;
-- use work.bitfield.all;

entity pe_ctrl is
    generic(
        compact_matrix_name: string;
        max_iterations:      positive := 16
    );
    port(
        clk:             in  std_ulogic;
        srstn:           in  std_ulogic;
        start:           in  std_ulogic;
        fgir:            out std_ulogic;                                 -- first gamma in row flag
        fi:              out std_ulogic;                                 -- first iteration flag
        alpha_ram_we:    out std_ulogic;                                 -- write enable
        alpha_ram_wa:    out natural range 0 to max_degree - 1;          -- write address
        alpha_ram_ra:    out natural range 0 to max_degree - 1;          -- read address
        aset_ram_we:     out std_ulogic;                                 -- write enable
        aset_ram_wa:     out natural range 0 to max_num_rows_per_pe - 1; -- write address
        aset_ram_ra:     out natural range 0 to max_num_rows_per_pe - 1; -- read address
        n:               out natural range 0 to max_degree - 1;          -- current node index
        phase:           out natural range 1 to 2                        -- current phase
    );
end entity pe_ctrl;

architecture rtl of pe_ctrl is

    constant cm: compact_matrix := string_to_compact_matrix(compact_matrix_name);
    constant num_rs: natural := (cm.z * cm.m) / npe; -- number of rows per pe

    type state_type is (idle, running);

    signal state: state_type;
    signal it: natural range 0 to max_iterations - 1;
    signal rs: natural range 0 to max_num_rows_per_pe - 1;

begin

    fgir         <= '1' when state = running and phase = 1 and n = 0 else '0';
    fi           <= '1' when it = 0 else '0';
    alpha_ram_we <= '1' when state = running and phase = 1 else '0';
    alpha_ram_wa <= n when state = running and phase = 1 else 0;
    aset_ram_we  <= '1' when state = running and phase = 1 and n = cm.d(rs / npe) - 1 else '0';
    aset_ram_wa  <= rs when state = running;

    ctrl_p: process(clk)
    begin
        if rising_edge(clk) then
            if srstn = '0' then
                alpha_ram_ra    <= 0;
                aset_ram_ra <= 0;
                n               <= 0;
                phase           <= 1;
                state           <= idle;
                it              <= 0;
                rs              <= 0;
            else
                if start = '1' and state = idle then
                    state <= running;
                    phase <= 1;
                    n     <= 0;
                elsif state = running then
                    if phase = 1 then
                        if n = cm.d(rs / npe) - 2 then
                            alpha_ram_ra <= 0;
                        end if;
                        if n = cm.d(rs / npe) - 1 then
                            phase <= 2;
                            n <= 0;
                            alpha_ram_ra <= 1;
                            if rs = num_rs - 1 then
                                aset_ram_ra <= 0;
                            else
                                aset_ram_ra <= aset_ram_ra + 1;
                            end if;
                        else
                            n <= n + 1;
                        end if;
                    else
                        if n < cm.d(rs / npe) - 2 then
                            alpha_ram_ra <= alpha_ram_ra + 1;
                        end if;
                        if n = cm.d(rs / npe) - 1 then
                            phase <= 1;
                            n <= 0;
                            if rs = num_rs - 1 then
                                rs <= 0;
                                if it = max_iterations - 1 then
                                    state <= idle;
                                    it <= 0;
                                else
                                    it <= it + 1;
                                end if;
                            else
                                rs <= rs + 1;
                            end if;
                        else
                            n <= n + 1;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process ctrl_p;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
