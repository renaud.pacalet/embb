--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

package errors is

    constant ldpc_num_error_codes:   natural := 1;
    constant ldpc_unknown_code:      natural := 0;

-- pragma translate_off
    constant ldpc_unknown_code_message: string := "Unknown code";
-- pragma translate_on

end package errors;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
