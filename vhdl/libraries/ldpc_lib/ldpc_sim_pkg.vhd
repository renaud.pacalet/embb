--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- Notations:
-- VN: variable node
-- CN: check node
-- gamma: posterior information
-- alpha: VN-to-CN message
-- lambda: erased VN-to-CN message
-- aset: compact representation of alphas of a row
-- beta:  CN-to-VN message

use std.env.all;
use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library global_lib;
-- use global_lib.global.all;
use global_lib.utils.all;
use global_lib.sim_utils.all;

use work.ldpc_pkg_local.all;
-- use work.bitfield.all;
-- use work.errors.all;

package ldpc_sim_pkg is

    constant mss2pss_channel_void: mss2pss_channel_type := (others => '-');

    procedure write(l: inout line; p: pss2mss_channel_type);
    procedure write(l: inout line; p: pss2mss_channel_vector);
    procedure write(l: inout line; p: mss2pss_type);
    procedure write(l: inout line; p: pss2mss_type);

    function check(l: mss2pss_type; r: mss2pss_type) return boolean;

    type string_vector is array(natural range <>) of string;

    constant compact_matrix_name: string_vector(0 to num_compact_matrix - 1);
    function string_to_compact_matrix(n: string) return compact_matrix;

    -- compute z (block matrix size) from code rate and frame length
    function get_z(r: code_rate; k: positive) return positive;

    -- generate n x n identity matrix
    function generate_identity_matrix(n: positive) return integer_matrix;

    -- right rotate each row of matrix m by n positions
    function rotate_right(m: integer_matrix; n: natural) return integer_matrix;

    -- convert '0'/'1' sign to -1/+1
    function scms_zo2mp(zo: std_ulogic) return integer;

    -- rotate a message vector by shift positions to the right
    function rotate_right(v: message_vector; shift: unsigned) return message_vector;

    -- rotate a message vector by shift positions to the left
    function rotate_left(v: message_vector; shift: unsigned) return message_vector;

    -- miscellaneous tests
    impure function ldpc_pkg_test(sigma: real; max_iterations: integer; rootdir: string) return boolean;

    -- data structure for ldpc decoding
    type scmsla_data is protected
        procedure init(frame: in message_vector); -- initialize scmsla_data structure
        procedure iter(cm: compact_matrix; row: in natural); -- one iteration on one row of layered SCMS
        procedure iter(cm: compact_matrix); -- one iteration of layered SCMS
        -- decode frame (LLR of channel observations) using SCMS algorithm and layered schedule; stop when decoding is successful or after max_iterations; issue a warning when decoded frame is all zeroes
        procedure decode(
            cm:             in  compact_matrix;    -- compact form of parity check matrix
            max_iterations: in  positive;          -- maximum number of iterations before giving up
            frame:          in  message_vector;    -- input channel observations (LLRs)
            iterations:     out positive;          -- number of iterations
            debug:          in  boolean := false;  -- print debug information
            asap:           in  boolean := true    -- stop decoding as soon as possible
        );
        impure function get_gamma(col: natural range 0 to max_num_cols - 1) return message; -- gammas
        impure function get_codeword return std_ulogic_vector; -- decoded word
        impure function get_syndrome return std_ulogic;
        procedure set_syndrome(v: std_ulogic);
        impure function get_changed return boolean;
        procedure set_changed(v: boolean);
    end protected scmsla_data;

end package ldpc_sim_pkg;

package body ldpc_sim_pkg is

    constant compact_matrix_name: string_vector(0 to num_compact_matrix - 1) := (
        "qc_802_11n_R12_Z27",
        "qc_802_11n_R12_Z54",
        "qc_802_11n_R12_Z81",
        "qc_802_11n_R23_Z27",
        "qc_802_11n_R23_Z54",
        "qc_802_11n_R23_Z81",
        "qc_802_11n_R34_Z27",
        "qc_802_11n_R34_Z54",
        "qc_802_11n_R34_Z81",
        "qc_802_11n_R56_Z27",
        "qc_802_11n_R56_Z54",
        "qc_802_11n_R56_Z81"
    );

    function get_z(r: code_rate; k: positive) return positive is
    begin
        return (k + kmin_table(r) - 1) / kmin_table(r);
    end function get_z;

    -- data structure for ldpc decoding
    type scmsla_data is protected body

        variable asets:     aset_vector(0 to max_num_rows - 1);   -- alpha sets (one set per row)
        variable gammas:        message_vector(0 to max_num_cols - 1);    -- gammas
        variable alphas:        message_vector(0 to max_degree - 1);      -- alphas
        variable codeword:      std_ulogic_vector(0 to max_num_cols - 1); -- decoded word
        variable syndrome:      std_ulogic;                               -- syndrome check (0: ok); set by scmsla.iter for input codeword
        variable changed:       boolean;                                  -- set to 1 by scmsla.iter if input codeword and output codeword are different

        procedure init(frame: in message_vector) is
        begin
            gammas(0 to frame'length - 1) := frame; -- initialize gammas with prior channel observation (LLR)
            asets     := (others => (m1 => 0, m2 => 0, i => 0, as => (others => '0'), sp => '0', ae => (others => '1')));
            codeword      := (others => '1');
            syndrome      := '0';
            changed       := true;
        end procedure init;

        procedure iter(cm: compact_matrix; row: in natural) is
            constant num_rows:  positive := cm.m * cm.z;
            constant num_cols:  positive := cm.n * cm.z;
            variable old_aset: aset;     -- old alphaset
            variable new_aset: aset;     -- new alphaset
            variable tmp: message;                         -- temporary message
            variable col: natural range 0 to num_cols - 1; -- column index
            variable i: natural range 0 to max_degree;     -- active column index
            variable rs: std_ulogic;                       -- per-row syndrome check
            variable stmp: std_ulogic;                     -- temporary sign

            constant block_row: natural := row / cm.z;
            constant k: natural := row mod cm.z;
        begin

            rs := '0';
            old_aset := asets(row);
            new_aset := (m1 => magnitude'high, m2 => magnitude'high, i => 0, as => (others => '0'), sp => '0', ae => (others => '0'));
            i := 0;
            for block_col in 0 to cm.n - 1 loop
                next when cm.q(block_row, block_col) < 0;
                col := cm.z * block_col + (cm.q(block_row, block_col) + k) mod cm.z; -- column index

            -- update row syndrome
                rs := rs xor codeword(col);

            -- re-compute old beta (tmp) from alphaset
                if i = old_aset.i then
                    tmp := scms_zo2mp(old_aset.as(i) xor old_aset.sp) * old_aset.m2;
                else
                    tmp := scms_zo2mp(old_aset.as(i) xor old_aset.sp) * old_aset.m1;
                end if;

            -- compute new alpha (tmp) from old gamma and old beta
                tmp := scms_saturate(gammas(col) - tmp);
                alphas(i) := tmp;          -- store new alpha

            -- SCMS erasure
                if scms_sign_zo(tmp) /= old_aset.as(i) and old_aset.ae(i) /= '1' then
                    tmp := 0;
                    new_aset.ae(i) := '1';
                end if;
                new_aset.as(i) := scms_sign_zo(tmp);
                new_aset.sp := new_aset.sp xor new_aset.as(i);   -- compute product of signs of (erased) new alpha

            -- SCMS kernel
                tmp := abs(tmp); -- magnitude of new (erased) alpha
                if tmp < new_aset.m1 then
                    new_aset.m2 := new_aset.m1;
                    new_aset.m1 := tmp;
                    new_aset.i := i;
                elsif tmp < new_aset.m2 then
                    new_aset.m2 := tmp;
                end if;

                i := i + 1;
            end loop;

        -- write back new gamma
            i := 0;
            for block_col in 0 to cm.n - 1 loop
                next when cm.q(block_row, block_col) < 0;
                col := cm.z * block_col + (cm.q(block_row, block_col) + k) mod cm.z; -- column index

            -- re-compute new beta from alphaset
                if i = new_aset.i then
                    tmp := scms_zo2mp(new_aset.sp xor new_aset.as(i)) * new_aset.m2;
                else
                    tmp := scms_zo2mp(new_aset.sp xor new_aset.as(i)) * new_aset.m1;
                end if;
                gammas(col) := scms_saturate(alphas(i) + tmp);               -- compute new gamma
                stmp := '1' when gammas(col) < 0 else '0';    -- hard decision
                if stmp /= codeword(col) then -- if decoded word changed
                    changed := true;
                    codeword(col)  := stmp;
                end if;
                i := i + 1;
            end loop;
        -- write back new alphaset
            asets(row) := new_aset;
        -- update matrix syndrome
            syndrome := syndrome or rs;
        end procedure iter;

        procedure iter(cm: compact_matrix) is
            constant num_rows:  positive := cm.m * cm.z;
            variable row: natural range 0 to num_rows - 1; -- row index
        begin
            syndrome := '0';
            changed := false;
            for block_row in 0 to cm.m - 1 loop
                for k in 0 to cm.z - 1 loop
                    row := cm.z * block_row + k;
                    iter(cm, row);
                end loop;
            end loop;
        end procedure iter;

        procedure decode(cm: in compact_matrix; max_iterations: in positive; frame: in message_vector; iterations: out positive; debug: in boolean := false; asap: in boolean := true) is
            variable l1, l2: line;
            variable ok: boolean;
            variable old_gammas: message_vector(0 to max_num_cols - 1);
        begin
            init(frame);
            ok := false;
            for i in 1 to max_iterations loop
                old_gammas := gammas;
                iter(cm);
                iterations := i;
                if debug then
                    write(l1, string'("Iteration #"));
                    write(l1, i);
                    write(l1, string'(": syndrome "));
                    write(l1, syndrome);
                    write(l1, string'(", changed "));
                    write(l1, changed);
                    writeline(output, l1);
                    for i in 0 to cm.n * cm.z - 1 loop
                        write(l1, old_gammas(i), right, 5);
                        write(l2, gammas(i), right, 5);
                    end loop;
                    writeline(output, l1);
                    writeline(output, l2);
                    if ok and not (syndrome = '0' and (not changed)) then
                        write(l1, string'("KO-after-bad"));
                        writeline(output, l1);
                        stop;
                    end if;
                end if;
                ok := syndrome = '0' and (not changed);
                exit when syndrome = '0' and (not changed) and asap;
            end loop;
        end procedure decode;

        impure function get_gamma(col: natural range 0 to max_num_cols - 1) return message is
        begin
            return gammas(col);
        end function get_gamma;

        impure function get_codeword return std_ulogic_vector is
        begin
            return codeword;
        end function get_codeword;

        procedure set_syndrome(v: std_ulogic) is
        begin
            syndrome := v;
        end procedure set_syndrome;

        impure function get_syndrome return std_ulogic is
        begin
            return syndrome;
        end function get_syndrome;

        procedure set_changed(v: boolean) is
        begin
            changed := v;
        end procedure set_changed;

        impure function get_changed return boolean is
        begin
            return changed;
        end function get_changed;

    end protected body scmsla_data;

    function scms_zo2mp(zo: std_ulogic) return integer is
    begin
        if zo = '1' then
            return -1;
        else
            return 1;
        end if;
    end function scms_zo2mp;

    impure function scmsla_check(cm: in compact_matrix; max_iterations: in positive; encoded: in string; received: in string; sigma: in real) return boolean is
        constant z: positive := cm.z;
        constant n: positive := cm.n * z;
        variable frame: message_vector(0 to n - 1);
        variable e: std_ulogic_vector(0 to n - 1);
        variable sd: scmsla_data;
        file fe: text;
        file fr: text;
        variable l: line;
        variable r: real;
        variable d: natural range 0 to 1;
        variable status: file_open_status;
        variable iterations: positive;
        variable tot_iter: natural;
        variable min_iter: natural;
        variable max_iter: natural;
        variable avg_iter: real;
        variable nframes: natural;
    begin
        min_iter := max_iterations;
        max_iter := 0;
        tot_iter := 0;
        nframes := 0;
        file_open(status, fe, encoded, read_mode);
        assert status = open_ok report "could not open file " & encoded severity failure;
        file_open(status, fr, received, read_mode);
        assert status = open_ok report "could not open file " & received severity failure;
        while not endfile(fr) loop
            readline(fr, l);
            for i in 0 to n - 1 loop
                read(l, r);
                r := -2.0 * r / (sigma * sigma);
                frame(i) := scms_saturate(integer(round(r)));
            end loop;
            sd.decode(cm, max_iterations, frame, iterations);
            readline(fe, l);
            read(l, e);
            if e /= sd.get_codeword(0 to n - 1) then
                report "Error during decoding of " & received;
                return false;
            end if;
            tot_iter := tot_iter + iterations;
            if iterations < min_iter then
                min_iter := iterations;
            end if;
            if iterations > max_iter then
                max_iter := iterations;
            end if;
            nframes := nframes + 1;
        end loop;
        report "successfully decoded " & integer'image(nframes) & " frames in avg/min/max " & real'image(real(tot_iter)/real(nframes)) & "/" & integer'image(min_iter) & "/" & integer'image(max_iter);
        return true;
    end function scmsla_check;

    function generate_identity_matrix(n: positive) return integer_matrix is
        variable res: integer_matrix(0 to n - 1, 0 to n - 1) := (others => (others => 0));
    begin
        for i in 0 to n - 1 loop
            res(i, i) := 1;
        end loop;
        return res;
    end function generate_identity_matrix;

    function rotate_right(m: integer_matrix; n: natural) return integer_matrix is
        constant r: positive := m'length(1);
        constant c: positive := m'length(2);
        constant mc: integer_matrix(0 to r - 1, 0 to c - 1) := m;
        variable res: integer_matrix(0 to r - 1, 0 to c - 1);
    begin
        for i in 0 to r - 1 loop
            for j in 0 to c - 1 loop
                res(i, j) := mc(i, (j + n) mod c);
            end loop;
        end loop;
        return res;
    end function rotate_right;

    function string_to_compact_matrix(n: string) return compact_matrix is
    begin
        for i in 0 to num_compact_matrix - 1 loop
            if n = compact_matrix_name(i) then
                return compact_matrix_list(i);
            end if;
        end loop;
        assert false report "Unknown matrix name: " & n severity failure;
        return compact_matrix_list(0);
    end function string_to_compact_matrix;

    function rotate_right(v: message_vector; shift: unsigned) return message_vector is

        constant vsize: positive                      := v'length;
        constant ssize: positive                      := shift'length;
        constant vshift: unsigned(ssize - 1 downto 0) := shift;
        variable res:  message_vector(0 to vsize - 1) := v;
        variable shamt: positive range 1 to 2**(ssize - 1);

    begin

        for i in ssize - 1 downto 0 loop
            shamt := 2**i;
            if vshift(i) = '1' then
                res := res(vsize - shamt to vsize - 1) & res(0 to vsize - shamt - 1);
            end if;
        end loop;
        return res(0 to vsize - 1);

    end function rotate_right;

    function rotate_right_check return boolean is
        constant di: message_vector(0 to 15) := (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        variable do: message_vector(0 to 15);
    begin
        for i in 2 to 16 loop        -- vector length
            for j in 0 to i - 1 loop -- rotate
                do(0 to i - 1) := rotate_right(di(0 to i - 1), to_unsigned(j, log2_up(i)));
                if di(0 to i - 1) /= do(j to i - 1) & do(0 to j - 1) then
                    report "Error on right rotate by " & to_string(j) & " positions of a message vector of length " & to_string(i);
                    return false;
                end if;
            end loop;
        end loop;
        report "rotate_right: OK";
        return true;
    end function rotate_right_check;

    function rotate_left(v: message_vector; shift: unsigned) return message_vector is

        constant vsize: positive                      := v'length;
        constant ssize: positive                      := shift'length;
        constant vshift: unsigned(ssize - 1 downto 0) := shift;
        variable res:  message_vector(0 to vsize - 1) := v;
        variable shamt: positive range 1 to 2**(ssize - 1);

    begin

        for i in ssize - 1 downto 0 loop
            shamt := 2**i;
            if vshift(i) = '1' then
                res := res(shamt to vsize - 1) & res(0 to shamt - 1);
            end if;
        end loop;
        return res(0 to vsize - 1);

    end function rotate_left;

    function rotate_left_check return boolean is
        constant di: message_vector(0 to 15) := (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        variable do: message_vector(0 to 15);
    begin
        for i in 2 to 16 loop        -- vector length
            for j in 0 to i - 1 loop -- rotate
                do(0 to i - 1) := rotate_left(di(0 to i - 1), to_unsigned(j, log2_up(i)));
                if do(0 to i - 1) /= di(j to i - 1) & di(0 to j - 1) then
                    report "Error on left rotate by " & to_string(j) & " positions of a message vector of length " & to_string(i);
                    return false;
                end if;
            end loop;
        end loop;
        report "rotate_left: OK";
        return true;
    end function rotate_left_check;

    impure function ldpc_pkg_test(sigma: real; max_iterations: integer; rootdir: string) return boolean is
        variable l: line;
        variable dmin, dmax: natural;
    begin
        if not rotate_right_check then
            return false;
        end if;
        if not rotate_left_check then
            return false;
        end if;
        for i in 0 to num_compact_matrix - 1 loop
            write(l, compact_matrix_name(i));
            write(l, string'(": z="));
            write(l, compact_matrix_list(i).z);
            write(l, string'(", m="));
            write(l, compact_matrix_list(i).m);
            write(l, string'(", n="));
            write(l, compact_matrix_list(i).n);
            for j in 0 to compact_matrix_list(i).m - 1 loop
                if j = 0 or dmin > compact_matrix_list(i).d(j) then
                    dmin := compact_matrix_list(i).d(j);
                end if;
                if j = 0 or dmax < compact_matrix_list(i).d(j) then
                    dmax := compact_matrix_list(i).d(j);
                end if;
            end loop;
            write(l, string'(", "));
            write(l, dmin);
            write(l, string'(" <= d <= "));
            write(l, dmax);
            writeline(output, l);
            if not scmsla_check(compact_matrix_list(i), max_iterations, rootdir & "/" & compact_matrix_name(i) & ".enc.txt", rootdir & "/" & compact_matrix_name(i) & ".rec.txt", sigma) then
                return false;
            end if;
        end loop;
        return true;
    end function ldpc_pkg_test;

    function check(l: mss2pss_type; r: mss2pss_type) return boolean is
        constant cl: mss2pss_type := l;
        constant cr: mss2pss_type := r;
    begin
        for i in l'range loop
            if not check(cl(i), cr(i)) then
                return false;
            end if;
        end loop;
        return true;
    end function check;

    procedure write(l: inout line; p: pss2mss_type) is
    begin
        write(l, string'("pss2mss "));
        write(l, string'("en:"));
        write(l, p.en);
        write(l, ' ');
        write(l, string'("wnr:"));
        write(l, p.wnr);
        for i in p.channels'range loop
            write(l, ' ');
            write(l, i);
            write(l, string'(":add="));
            hwrite(l, p.channels(i).add);
            write(l, ',');
            write(l, string'("data="));
            hwrite(l, p.channels(i).data);
        end loop;
        write(l, ' ');
    end procedure write;

    procedure write(l: inout line; p: pss2mss_channel_type) is
    begin
        write(l, string'("add:"));
        hwrite(l, p.add);
        write(l, ' ');
        write(l, string'("data:"));
        hwrite(l, p.data);
        write(l, ' ');
    end procedure write;

    procedure write(l: inout line; p: pss2mss_channel_vector) is
        variable spacer: boolean;
    begin
        spacer := false;
        for i in p'range loop
            if spacer then
                write(l, cr);
                write(l, string'("            "));
            end if;
            spacer := true;
            write(l, i);
            write(l, ' ');
            write(l, p(i));
        end loop;
    end procedure write;

    procedure write(l: inout line; p: mss2pss_type) is
    begin
        write(l, string'("mss2pss "));
        for i in p'range loop
            write(l, i);
            write(l, ':');
            hwrite(l, p(i));
            write(l, ' ');
        end loop;
    end procedure write;

end package body ldpc_sim_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
