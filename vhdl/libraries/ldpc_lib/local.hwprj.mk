#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

IGNORE		+= ldpc_lib.mss_if
gh-IGNORE	+= ldpc_lib

ldpc_lib.ldpc: \
	global_lib.global \
	css_lib.css_pkg \
	css_lib.css \
	ldpc_lib.bitfield_pkg \
	ldpc_lib.ldpc_pkg_local \
	ldpc_lib.mss \
	ldpc_lib.pss

ldpc_lib.pss: \
	global_lib.global \
	global_lib.utils \
	ldpc_lib.ldpc_pkg_local \
	ldpc_lib.pe

ldpc_lib.pss_sim: \
	global_lib.global \
	ldpc_lib.ldpc_pkg_local \
	ldpc_lib.pss \
	ldpc_lib.mss

ldpc_lib.mss: \
	global_lib.global \
	memories_lib.ram_pkg \
	memories_lib.tdpram \
	ldpc_lib.ldpc_pkg_local

ldpc_lib.mss_if: \
	ldpc_lib.ldpc_pkg_local

ldpc_lib.mss_sim: \
	random_lib.rnd \
	global_lib.global \
	global_lib.sim_utils \
	global_lib.fifo_pkg \
	ldpc_lib.ldpc_pkg_local \
	ldpc_lib.ldpc_sim_pkg \
	ldpc_lib.mss

ldpc_lib.ldpc_pkg: \
	global_lib.global \
	ldpc_lib.bitfield_pkg \
	ldpc_lib.errors

ldpc_lib.ldpc_pkg_check: \
	ldpc_lib.ldpc_pkg_local \
	ldpc_lib.ldpc_sim_pkg

ldpc_lib.ldpc_pkg_local: ldpc_lib.ldpc_pkg

ldpc_lib.ldpc_sim_pkg: \
	global_lib.utils \
	global_lib.sim_utils \
	ldpc_lib.ldpc_pkg_local

ldpc_lib.pe: ldpc_lib.ldpc_pkg_local

ldpc_lib.pe_ctrl: \
	global_lib.global \
	ldpc_lib.ldpc_pkg_local \
	ldpc_lib.ldpc_sim_pkg

ldpc_lib.pe_sim: \
	random_lib.rnd \
	ldpc_lib.ldpc_pkg_local \
	ldpc_lib.ldpc_sim_pkg \
	ldpc_lib.pe_ctrl \
	ldpc_lib.pe

QCMATRIX	:= qc_802_11n_R56_Z81
QCMATRICES	:= $(foreach r,12 23 34 56,$(foreach z,27 54 81,qc_802_11n_R$(r)_Z$(z)))
RUNALLQUIT	:= 'run -all; quit'

ms-sim.ldpc_lib.pe_sim: MSSIMFLAGS += -Gmax_iterations=6 -Gcompact_matrix_name=$(QCMATRIX) -Greceived_file_name=$(hwprj_db_path.ldpc_lib)/$(QCMATRIX).rec.txt -logfile ms-sim.ldpc_lib.pe_sim.log
ms-sim-gui.ldpc_lib.pe_sim: MSSIMFLAGS += -Gmax_iterations=6 -Gcompact_matrix_name=$(QCMATRIX) -Greceived_file_name=$(hwprj_db_path.ldpc_lib)/$(QCMATRIX).rec.txt

ms-sim.ldpc_lib.ldpc_pkg_check ms-sim-gui.ldpc_lib.ldpc_pkg_check: MSSIMFLAGS += -Grootdir=$(hwprj_db_path.ldpc_lib)
ms-sim.ldpc_lib.ldpc_pkg_check: MSSIMFLAGS += -do $(RUNALLQUIT)
ms-sim.ldpc_lib.pe_sim: MSSIMFLAGS += -do $(RUNALLQUIT)

ms-sim.ldpc_lib.mss_sim: MSSIMFLAGS += -quiet -Gn=100000 -do $(RUNALLQUIT)

ms-sim.ldpc_lib.pss_sim: I=1
ms-sim.ldpc_lib.pss_sim: MSSIMFLAGS += -quiet -do wave.do -do 'run 10000 ns'

.PHONY: ldpc-pe-test

ldpc-pe-test: MSSIMFLAGS += '-quiet'

# $(1): QC matrix
define LDPCTEST_rule
.PHONY: ldpc-pe-test-$(1)

ldpc-pe-test: ldpc-pe-test-$(1)

ldpc-pe-test-$(1):
	$$(Q)echo $$@ && \
	log=$$$$(tempfile) && \
	$$(MAKE) MSSIMFLAGS="-quiet -Gcompact_matrix_name=$(1) -Greceived_file_name=$$(hwprj_db_path.ldpc_lib)/$(1).rec.txt -do 'run -all; quit'" ms-sim.ldpc_lib.pe_sim $$(MQ) > $$$$log && \
	if ! grep 'decoded 100 frames' $$$$log; then \
		cat $$$$log && \
		exit 1; \
	fi && \
	rm -f $$$$log

ldpc-pe-test-gui-$(1):
	$$(Q)$$(MAKE) MSSIMFLAGS="-Gcompact_matrix_name=$(1) -Greceived_file_name=$$(hwprj_db_path.ldpc_lib)/$(1).rec.txt" ms-sim-gui.ldpc_lib.pe_sim $$(MQ)
endef
$(foreach q,$(QCMATRICES),$(eval $(call LDPCTEST_rule,$(q))))

define LDPCHELP_message
Specific goals:
  make ms-help                                this help
  make ms-sim.ldpc_lib.ldpc_pkg_check         test ldpc_pkg
  make ms-sim-gui.ldpc_lib.ldpc_pkg_check     test ldpc_pkg, GUI
  make ldpc-pe-test-qc_802_11n_Rrr_Zzz        run ldpc pe test with matrix qc_802_11n_Rrr_Zzz
                                                rr in {12,23,34,56}, zz {27,54,81}
  make ldpc-pe-test-gui-qc_802_11n_Rrr_Zzz    same as ldpc-pe-test-qc_802_11n_Rrr_Zzz, GUI
  make ldpc-pe-test                           run all ldpc-pe-test-qc_802_11n_Rrr_Zzz
endef
export LDPCHELP_message

ldpc-help:
	@echo "$$LDPCHELP_message"

help: EXTRAHELP	+= ldpc-help

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
