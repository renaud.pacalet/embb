--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- mss is a 16 k-bytes memory, composed of 8 (nrams) 2kx8 true dual port rams. rams are numbered from 0 to 7 (nrams-1). let 0<=i<=7 be a ram index and
-- 0<=a<=2047 be an address in a 2kx8 ram. from css and pss perspective the byte address (relative to mss base address) of ram[i][a] is a*8+i.
--
-- the npe requests from pss are guaranteed to use no more than 2 ports per ram. they are routed to the target ram, based of the address (target ram = add mod
-- 8). port 0 is used first, port 1 is used only if needed. pss priority > css priority. css requests are always routed to port 1, without loss of performance
-- (if a css request is not granted because port 1 is used by pss, the corresponding port 0 is used too).

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;

library memories_lib;
use memories_lib.ram_pkg.all;

use work.ldpc_pkg_local.all;

entity mss is
    generic(
        debug: boolean := false
    );
    port(
        clk:     in  std_ulogic;
        srstn:   in  std_ulogic;
        css2mss: in  css2mss_type; -- requests from css
        mss2css: out mss2css_type; -- responses to css
        pss2mss: in  pss2mss_type; -- requests from pss
        mss2pss: out mss2pss_type  -- responses to pss
    );
end entity mss;

architecture rtl of mss is

    alias vci2mss is css2mss.vci2mss;
    alias mss2vci is mss2css.mss2vci;
    alias dma2mss is css2mss.dma2mss;
    alias mss2dma is mss2css.mss2dma;
    alias uc2mss  is css2mss.uc2mss;
    alias mss2uc  is mss2css.mss2uc;

    -- requests and responses to/from rams
    type in_ram_requests_type is array(0 to nrams - 1, ports) of in_port_2kx8;
    signal in_rams: in_ram_requests_type;
    type out_ram_responses_type is array(0 to nrams - 1, ports) of word8;
    signal out_rams: out_ram_responses_type;

    -- responses pipeline
    type out_ram_response_mux_type is record
        en: std_ulogic;
        dest: clients;
        pe: natural range 0 to npe - 1;
    end record;
    constant out_ram_response_mux_none: out_ram_response_mux_type := (en => '0', dest => pss, pe => 0);
    type out_ram_responses_mux_type is array(0 to nrams - 1, 0 to 1) of out_ram_response_mux_type;
    constant out_ram_responses_mux_none: out_ram_responses_mux_type := (others => (others => out_ram_response_mux_none));
    type out_ram_responses_mux_pipeline_type is array(0 to 1) of out_ram_responses_mux_type;
    constant out_ram_responses_mux_pipeline_none: out_ram_responses_mux_pipeline_type := (others => out_ram_responses_mux_none);
    signal mux_pipe: out_ram_responses_mux_pipeline_type;
    signal mux_pipe_in: out_ram_responses_mux_type;
    alias mux_pipe_out is mux_pipe(1);

begin

    -- responses pipeline
    pipeline_p: process(clk)
    begin
        if rising_edge(clk) then
            if srstn = '0' then
                mux_pipe <= out_ram_responses_mux_pipeline_none;
            else
                mux_pipe <= mux_pipe_in & mux_pipe(0);
            end if;
        end if;
    end process pipeline_p;

    -- responses routing
    router_p: process(mux_pipe_out, out_rams)
    begin
        mss2vci.be    <= (others => '0');
        mss2vci.rdata <= (others => '0');
        mss2vci.en    <= '0';
        mss2dma.be    <= (others => '0');
        mss2dma.rdata <= (others => '0');
        mss2dma.en    <= '0';
        mss2uc.be     <= (others => '0');
        mss2uc.rdata  <= (others => '0');
        mss2uc.en     <= '0';
        for r in 0 to nrams - 1 loop
            for p in ports loop
                if mux_pipe_out(r, p).en = '1' then
                    case mux_pipe_out(r, p).dest is
                        when pss =>
                            mss2pss(mux_pipe_out(r, p).pe) <= out_rams(r, p);
                        when uc =>
                            mss2uc.be(r) <= '1';
                            mss2uc.rdata(8 * r + 7 downto 8 * r) <= out_rams(r, p);
                            mss2uc.en <= '1';
                        when dma =>
                            mss2dma.be(r) <= '1';
                            mss2dma.rdata(8 * r + 7 downto 8 * r) <= out_rams(r, p);
                            mss2dma.en <= '1';
                        when vci =>
                            mss2vci.be(r) <= '1';
                            mss2vci.rdata(8 * r + 7 downto 8 * r) <= out_rams(r, p);
                            mss2vci.en <= '1';
                    end case;
                end if;
            end loop;
        end loop;
    end process router_p;

    -- arbiter
    arbiter_p: process(pss2mss, vci2mss, dma2mss, uc2mss)
        type port_used_type is array(0 to nrams - 1, ports) of boolean;
        variable port_used: port_used_type;
        variable p: ports;
    begin
        port_used   := (others => (others => false));
        in_rams     <= (others => (others => in_port_2kx8_default));
        mux_pipe_in <= out_ram_responses_mux_none;
        mss2uc.gnt  <= (others => '0');
        mss2uc.oor  <= '0';
        mss2dma.gnt <= (others => '0');
        mss2dma.oor <= '0';
        mss2vci.gnt <= (others => '0');
        mss2vci.oor <= '0';
        if pss2mss.en = '1' then
            for pe in 0 to npe - 1 loop
                for r in 0 to nrams - 1 loop
                    if r = to_integer(u_unsigned(pss2mss.channels(pe).add(2 downto 0))) then
                        if not port_used(r, 0) then
                            p := 0;
                        else
                            p := 1;
                            assert not port_used(r, 1) report "Port b already used" severity failure;
                        end if;
                        in_rams(r, p).en    <= '1';
                        in_rams(r, p).we(0) <= pss2mss.wnr;
                        in_rams(r, p).add   <= pss2mss.channels(pe).add(13 downto 3);
                        in_rams(r, p).wdata <= pss2mss.channels(pe).data;
                        port_used(r, p)     := true;
                        if pss2mss.wnr = '0' then
                            mux_pipe_in(r, p)   <= (en => '1', dest => pss, pe => pe);
                        end if;
                    end if;
                end loop;
            end loop;
        end if;
        if uc2mss.en = '1' then
            if ((or uc2mss.add(2 downto 0)) or (or uc2mss.add(15 downto 14))) /= '0' then
                mss2uc.gnt <= (others => '1');
                mss2uc.oor <= '1';
            else
                for r in 0 to nrams - 1 loop
                    if uc2mss.be(r) = '1' and (not port_used(r, 1)) then
                        mss2uc.gnt(r)       <= '1';
                        in_rams(r, 1).en    <= '1';
                        in_rams(r, 1).add   <= uc2mss.add(13 downto 3);
                        in_rams(r, 1).we(0) <= not uc2mss.rnw;
                        if uc2mss.rnw = '0' then
                            in_rams(r, 1).wdata <= uc2mss.wdata(8 * r + 7 downto 8 * r);
                        else
                            mux_pipe_in(r, 1)   <= (en => '1', dest => uc, pe => 0);
                        end if;
                        port_used(r, 1)     := true;
                    end if;
                end loop;
            end if;
        end if;
        if dma2mss.en = '1' then
            if (or dma2mss.add(28 downto 11)) /= '0' then
                mss2dma.gnt <= (others => '1');
                mss2dma.oor <= '1';
            else
                for r in 0 to nrams - 1 loop
                    if dma2mss.be(r) = '1' and (not port_used(r, 1)) then
                        mss2dma.gnt(r)      <= '1';
                        in_rams(r, 1).en    <= '1';
                        in_rams(r, 1).add   <= dma2mss.add(10 downto 0);
                        in_rams(r, 1).we(0) <= not dma2mss.rnw;
                        if dma2mss.rnw = '0' then
                            in_rams(r, 1).wdata <= dma2mss.wdata(8 * r + 7 downto 8 * r);
                        else
                            mux_pipe_in(r, 1)   <= (en => '1', dest => dma, pe => 0);
                        end if;
                        port_used(r, 1)     := true;
                    end if;
                end loop;
            end if;
        end if;
        if vci2mss.en = '1' then
            if (or vci2mss.add(28 downto 11)) /= '0' then
                mss2vci.gnt <= (others => '1');
                mss2vci.oor <= '1';
            else
                for r in 0 to nrams - 1 loop
                    if vci2mss.be(r) = '1' and (not port_used(r, 1)) then
                        mss2vci.gnt(r)      <= '1';
                        in_rams(r, 1).en    <= '1';
                        in_rams(r, 1).add   <= vci2mss.add(10 downto 0);
                        in_rams(r, 1).we(0) <= not vci2mss.rnw;
                        if vci2mss.rnw = '0' then
                            in_rams(r, 1).wdata <= vci2mss.wdata(8 * r + 7 downto 8 * r);
                        else
                            mux_pipe_in(r, 1)   <= (en => '1', dest => vci, pe => 0);
                        end if;
                        port_used(r, 1)     := true;
                    end if;
                end loop;
            end if;
        end if;
    end process arbiter_p;

    -- for each 2kx8 ram
    data_rams_g: for r in 0 to nrams - 1 generate

        data_ram_u: entity memories_lib.tdpram(arc)
            generic map(
                debug      => debug,
                registered => true,
                na         => 11,
                nb         => 1
            )
            port map(
                clk   => clk,
                ena   => in_rams(r, 0).en,
                wea   => in_rams(r, 0).we,
                adda  => in_rams(r, 0).add,
                dina  => in_rams(r, 0).wdata,
                douta => out_rams(r, 0),
                enb   => in_rams(r, 1).en,
                web   => in_rams(r, 1).we,
                addb  => in_rams(r, 1).add,
                dinb  => in_rams(r, 1).wdata,
                doutb => out_rams(r, 1)
            );

    end generate data_rams_g;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
