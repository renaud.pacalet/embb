--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- mss_if implements a 2-stages fifo between css requests and the rams in mss. css requests are:
-- o 64 bits wide,
-- o 64-bits-aligned addresses between 0 and 128k (1MB),
-- o individual read and write byte enables,
-- o read xor write accesses.
-- the format of the two stages differ: the input stage is the same as the requests from css while the output stage is split in 8 per-byte requests. this
-- allows to serve a 64-bits request in several consecutive clock cycles when the 8 channels are not all simultaneously available. write requests from css are
-- not responded. invalid requests from css are immediately acknowledged but not responded.

library ieee;
use ieee.std_logic_1164.all;

use work.ldpc_pkg_local.all;

entity mss_if is
    generic(
        latency: positive := 2;  -- rams latency
        l2depth: positive := 1   -- log2 of depth of css requests fifo
    );
    port(
        clk:     in  std_ulogic; -- system clock
        srstn:   in  std_ulogic; -- synchronous active low reset
        css2mss: in  css2mss_t;  -- requests from css
        mss2css: out mss2css_t;  -- responses to css
        css2ram: out css2ram_t;  -- requests to rams
        ram2css: in  ram2css_t   -- responses from ram
    );
end entity mss_if;

architecture rtl of mss_if is

    -- depth of css requests fifo
    constant depth: positive := 2**l2depth;

    -- type of css requests fifo
    type css2mss_fifo_t is array(natural range 0 to depth - 1) of css2mss_t;
    -- reset value of css2mss_fifo_t
    constant css2mss_fifo_none: css2mss_fifo_t := (others => css2mss_none);

    -- pipeline of ram read responses (except data)
    -- type of pipeline stage
    type ram_pipe_line_stage_t is record
        last: std_ulogic;                    -- last responses of an 8 bytes group
        be:   std_ulogic_vector(7 downto 0); -- individual read byte valid flags from rams
    end record;
    -- reset value of ram_pipe_line_stage_t
    constant ram_pipe_line_stage_none: ram_pipe_line_stage_t := (last => '0', be => (others => '0'));
    -- type of pipeline
    type ram_pipe_line_t is array(natural range 0 to latency - 1) of ram_pipe_line_stage_t;
    -- reset value of ram_pipe_line_t
    constant ram_pipe_line_none: ram_pipe_line_t := (others => ram_pipe_line_stage_none);

    -- css requests fifo
    signal css2mss_local:         css2mss_t;      -- input request from css after filtering of invalid requests and void write requests
    signal css2mss_fifo:          css2mss_fifo_t; -- css requests fifo
    signal css2mss_fifo_rp:       natural range 0 to depth - 1;   -- read pointer of css requests fifo
    signal css2mss_fifo_wp:       natural range 0 to depth - 1;   -- write pointer of css requests fifo

    -- pipeline of ram read responses (except data)
    signal ram_pipe_line: ram_pipe_line_t;
    signal ram2css_be:    std_ulogic_vector(7 downto 0);  -- temporary register for individual read byte valid flags from rams
    signal ram2css_data:  std_ulogic_vector(63 downto 0); -- temporary register for individual read bytes from rams
    signal ram2css_once:  std_ulogic;                     -- selector of multiplexor of css read responses

    -- control signals
    signal css2ramuest_completes:  boolean; -- rams request pending and all requested bytes acknowledged
    signal can_accept_css_request: boolean; -- write pointer of css requests fifo points to free slot
    signal out_of_range:           boolean; -- address of css request is out of range
    signal read_only_error:        boolean; -- css write request and address is read-only

begin

    -- read pointer of css requests fifo points to css request that drives ram requests
    css2ram <= css2mss_fifo(css2mss_fifo_rp);

    css2ramuest_completes  <= css2mss_fifo(css2mss_fifo_rp).en = '1' and (or (css2mss_fifo(css2mss_fifo_rp).be and (not ram2css.ack))) = '0';
    can_accept_css_request <= css2mss_fifo(css2mss_fifo_wp).en = '0';
    out_of_range           <= not is_mapped(css2mss.add);
    read_only_error        <= css2mss.wnr = '1' and is_read_only(css2mss.add);

    -- css requests acknowledge and error flags
    mss2css.ack <= '1' when can_accept_css_request else                   -- acknowledge if fifo not full
                   '1' when out_of_range or read_only_error else          -- acknowledge invalid requests, even when fifo full
                   '1' when css2mss.wnr = '1' and css2mss.be = x"00" else -- acknowledge write requests with no byte enabled, even when fifo full
                   '0';

    mss2css.oor <= '1' when out_of_range else '0';    -- out of range error
    mss2css.wro <= '1' when read_only_error else '0'; -- write at read-only error

    process(out_of_range, read_only_error, css2mss)
    begin
        css2mss_local <= css2mss;
        if out_of_range or read_only_error then
            css2mss_local.en <= '0'; -- cancel invalid requests
        end if;
        if css2mss.wnr = '1' and css2mss.be = x"00" then
            css2mss_local.en <= '0'; -- cancel write requests with zero bytes enabled
        end if;
    end process;

    -- css requests fifo and ram responses pipeline
    process(clk)
    begin
        if rising_edge(clk) then
            if srstn = '0' then
                css2mss_fifo    <= css2mss_fifo_none;
                css2mss_fifo_rp <= 0;
                css2mss_fifo_wp <= 0;
                ram_pipe_line   <= ram_pipe_line_none;
            else
                -- advance pipeline of ram read responses
                for i in latency - 1 downto 1 loop
                    ram_pipe_line(i) <= ram_pipe_line(i - 1);
                end loop;
                ram_pipe_line(0) <= ram_pipe_line_stage_none;
                -- update pending css request to rams and pipeline of ram read responses
                if css2mss_fifo(css2mss_fifo_rp).en = '1' then     -- if pending ram request
                    if css2mss_fifo(css2mss_fifo_rp).wnr = '0' then -- if read request
                        ram_pipe_line(0).be <= css2mss_fifo(css2mss_fifo_rp).be and ram2css.ack; -- inject read byte valid flags in pipeline
                    end if;
                    css2mss_fifo(css2mss_fifo_rp).be <= css2mss_fifo(css2mss_fifo_rp).be and (not ram2css.ack); -- deassert acknowledged byte requests
                end if;
                if css2ramuest_completes then                       -- if request done
                    css2mss_fifo(css2mss_fifo_rp).en <= '0';       -- de-assert request
                    if css2mss_fifo(css2mss_fifo_rp).wnr = '0' then -- if read request
                        ram_pipe_line(0).last <= '1';               -- inject last flag in pipeline
                    end if;
                    css2mss_fifo_rp <= (css2mss_fifo_rp + 1) mod depth;  -- move read pointer
                end if;
                if css2mss_local.en = '1' and can_accept_css_request then -- if valid request from css and can accept it
                    css2mss_fifo(css2mss_fifo_wp) <= css2mss_local;        -- push request
                    css2mss_fifo_wp <= (css2mss_fifo_wp + 1) mod depth;    -- move write pointer
                end if;
            end if;
        end if;
    end process;

    -- read responses to css
    -- if all byte read responses are received in the same clock cycle
    -- (ram2css_once = '1'), the read response to css depends only on the
    -- (last) ram read responses. else, partial responses are stored in be and
    -- data temporary registers, ram2css_once is cleared, and the read response
    -- to css is a combination of the last ram read responses and the temporary
    -- registers.
    process(clk)
    begin
        if rising_edge(clk) then
            if srstn = '0' then
                ram2css_be   <= (others => '0');
                ram2css_data <= (others => '0');
                ram2css_once <= '1';
            else
                if ram_pipe_line(latency - 1).last = '1' then -- if last responses of an 8 bytes group
                    ram2css_be   <= (others => '0');          -- reset temporary register for individual read byte valid flags from rams
                    ram2css_data <= (others => '0');          -- reset temporary register for individual read bytes from rams
                    ram2css_once <= '1';                      -- reset byte-read-responses-at-once flag
                elsif ram_pipe_line(latency - 1).be /= x"00" then              -- if partial responses of an 8 bytes group
                    ram2css_be <= ram2css_be or ram_pipe_line(latency - 1).be; -- update temporary register for individual read byte valid flags from rams
                    for i in 0 to 7 loop                                       -- update temporary register for individual read bytes from rams
                        if ram_pipe_line(latency - 1).be(i) = '1' then
                            ram2css_data(8 * i + 7 downto 8 * i) <= ram2css.data(8 * i + 7 downto 8 * i);
                        end if;
                    end loop;
                    ram2css_once <= '0'; -- remember that byte read responses were received in several clock cycles
                end if;
            end if;
        end if;
    end process;

    -- send read responses to css when receiving last responses of an 8 bytes group from rams
    mss2css.en <= ram_pipe_line(latency - 1).last;

    process(ram_pipe_line(latency - 1), ram2css_once, ram2css_be, ram2css_data, ram2css.data)
    begin
        -- if all read bytes received from ram at once
        mss2css.be   <= ram_pipe_line(latency - 1).be; -- css response = ram responses
        mss2css.data <= ram2css.data;                  -- css response = ram responses
        if ram2css_once = '0' then -- else, combine with temporary registers
            for i in 0 to 7 loop -- for all bytes
                if ram_pipe_line(latency - 1).be(i) = '1' then -- if present in ram responses
                    mss2css.be(i)                        <= '1';                                  -- assert byte enable
                    mss2css.data(8 * i + 7 downto 8 * i) <= ram2css.data(8 * i + 7 downto 8 * i); -- pick byte from ram responses
                else
                    mss2css.be(i)                        <= ram2css_be(i);                        -- pick byte enable from temporary register
                    mss2css.data(8 * i + 7 downto 8 * i) <= ram2css_data(8 * i + 7 downto 8 * i); -- pick byte from temporary register
                end if;
            end loop;
        end if;
    end process;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
