--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- Processing Element (PE). The PE computes betas, alphas and gammas.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.ldpc_pkg_local.all;

entity pe is
    port(
        clk:          in  std_ulogic;
        srstn:        in  std_ulogic;
        fi:           in  std_ulogic;                        -- first iteration flag
        fgir:         in  std_ulogic;                        -- first gamma in row flag
        phase:        in  natural range 0 to 2;              -- current phase
        n:            in  natural range 0 to max_degree - 1; -- current node index
        aset_ram_we:  in  std_ulogic;                        -- write enable
        aset_ram_wa:  in  natural range 0 to max_num_rows_per_pe - 1;   -- write address
        aset_ram_ra:  in  natural range 0 to max_num_rows_per_pe - 1;   -- read address
        alpha_ram_we: in  std_ulogic;                        -- write enable
        alpha_ram_wa: in  natural range 0 to max_degree - 1; -- write address
        alpha_ram_ra: in  natural range 0 to max_degree - 1; -- read address
        gi:           in  message;                           -- input gamma
        go:           out message                            -- output gammma
    );
end entity pe;

architecture rtl of pe is

    -- aset registers
    signal aset_new: aset;   -- aset built during current iteration
    signal aset_new_d: aset; -- next value of aset_new
    signal aset_new_q: aset; -- registered value of aset_new

    -- aset ram
    signal aset_ram_di: aset;                        -- output data
    signal aset_ram_do: aset;                        -- output data

    -- alpha ram
    signal alpha_ram_di: message;                           -- input data
    signal alpha_ram_do: message;                           -- output data

begin

    -- aset_ram
    aset_ram_p: process(clk)
        variable aset_ram: aset_vector(0 to max_num_rows_per_pe - 1);
    begin
        if rising_edge(clk) then
            aset_ram_do <= aset_ram(aset_ram_ra);
            if aset_ram_we = '1' then
                aset_ram(aset_ram_wa) := aset_ram_di;
            end if;
        end if;
    end process aset_ram_p;

    -- alpha_ram
    alpha_ram_p: process(clk)
        variable alpha_ram: message_vector(0 to max_degree - 1);
    begin
        if rising_edge(clk) then
            alpha_ram_do <= alpha_ram(alpha_ram_ra);
            if alpha_ram_we = '1' then
                alpha_ram(alpha_ram_wa) := alpha_ram_di;
            end if;
        end if;
    end process alpha_ram_p;

    -- combinatorial
    aset_ram_di <= aset_new_d;

    -- computation of new alpha if phase 1, else new gamma
    comb_p: process(aset_ram_do, aset_new, gi, alpha_ram_do, n, phase, fi)

        variable alpha_gamma: message;   -- alpha or gamma
        variable lambda: message;        -- erased alpha
        variable beta: magnitude;        -- absolute value of beta
        variable aset_old: aset; -- previous compact set of alpha messages of row
        variable aset_tmp: aset; -- compact set of alpha messages of row
        variable o: std_ulogic;          -- add ('0') or sub ('1') operation selector

        -- add/sub and saturate
        function addsub(a: message; b: magnitude; o: std_ulogic) return message is -- '0': a+b, '1': a-b
            variable va: signed(bit_width downto 0) := to_signed(a, bit_width + 1);
            variable vb: signed(bit_width downto 0) := to_signed(b, bit_width + 1);
            variable vr: signed(bit_width downto 0);
        begin
            vb := vb xor o;    -- one's complement
            vr := va + vb + o; -- add/sub
            return scms_saturate(to_integer(vr));
        end function addsub;

    begin
        -- default values
        aset_new_d <= aset_new;
        alpha_ram_di   <= 0;
        go             <= 0;
        alpha_gamma    := 0;
        lambda         := 0;
        beta           := 0;
        aset_old   := aset_ram_do;
        aset_tmp   := aset_ram_do;
        o              := '0';

        if phase = 1 then -- must compute alpha_new = gamma_old - beta_old
            if fi = '1' then -- if first iteration
                aset_old := (0, 0, 0, (others => '0'), '0', (others => '1')); -- reset value
            end if;
            alpha_gamma  := gi;          -- gamma_old
            o            := '1';          -- subtract
            aset_tmp := aset_old; -- to compute beta_old
        else              -- must compute gamma_new = alpha_new + beta_new
            alpha_gamma  := alpha_ram_do; -- alpha_new
            o            := '0';          -- add
            aset_tmp := aset_new; -- to compute beta_new
        end if;
        -- compute beta absolute value from n and aset_tmp
        if n = aset_tmp.i then
            beta := aset_tmp.m2;
        else
            beta := aset_tmp.m1;
        end if;
        -- update operation depending on beta sign (aset_tmp.as(n) xor aset_tmp.sp)
        o := o xor aset_tmp.as(n) xor aset_tmp.sp;
        -- compute new alpha (phase 1) or new gamma (phase 2)
        alpha_gamma  := addsub(alpha_gamma, beta, o);
        if phase = 1 then
            -- if alpha sign changed and alpha not erased during previous iteration, erase
            if aset_old.ae(n) = '0' and scms_sign_zo(alpha_gamma) /= aset_old.as(n) then
                aset_new_d.ae(n) <= '1';
                lambda               := 0;
            else
                aset_new_d.ae(n) <= '0';
                lambda               := alpha_gamma;
            end if;
            aset_new_d.as(n) <= scms_sign_zo(lambda);                     -- update sign of erased alpha
            aset_new_d.sp    <= aset_new.sp xor scms_sign_zo(lambda); -- update product of signs of erased alpha
                                                                              -- minimums and argmin finder of erased alpha
            lambda := abs(lambda);
            if lambda < aset_new.m1 then
                aset_new_d.m1 <= lambda;
                aset_new_d.m2 <= aset_new.m1;
                aset_new_d.i  <= n;
            elsif lambda < aset_new.m2 then
                aset_new_d.m2 <= lambda;
            end if;
            alpha_ram_di <= alpha_gamma; -- send alpha to alpha ram
        else
            go           <= alpha_gamma; -- send gamma to go register
        end if;
    end process comb_p;

    -- registers
    -- note: possible performance improvement with as/ae as shift registers in asets
    reg_p: process(clk)
    begin
        if rising_edge(clk) then
            if srstn = '0' then
                aset_new_q <= (magnitude'high, magnitude'high, 0, (others => '0'), '0', (others => '0')); -- all +inf. and not erased
            elsif alpha_ram_we = '1' then       -- one clock cycle after new gamma
                aset_new_q <= aset_new_d; -- update new aset
            end if;
        end if;
    end process reg_p;

    aset_new <= (magnitude'high, magnitude'high, 0, (others => '0'), '0', (others => '0')) when fgir = '1' else -- reset value
                    aset_new_q;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
