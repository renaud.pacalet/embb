--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

use work.ldpc_pkg_local.all;
use work.bitfield.all;

entity pss is
    generic(
        max_iterations: positive := 6
    );
    port(
        clk:     in  std_ulogic;
        param:   in  std_ulogic_vector(bitfield_width - 1 downto 0);
        css2pss: in  css2pss_type;
        pss2css: out pss2css_type;
        pss2mss: out pss2mss_type;
        mss2pss: in  mss2pss_type
    );
end entity pss;

architecture rtl of pss is

    -- the rom contains all definitions about the supported parity matrices. its width is 18 bits. the definition of matrix #i starts at word 128*i. matrices
    -- are represented in compact form: only the non-zero submatrices are defined. the submatrix in block-row R, block-column C is defined by its base index
    -- (B=Z*C) and its rotation (0<=Q<=80). the rom defines:
    --   ddddd: D, degree of current block-row minus 1, 5 bits, constant for block-row (D in {6, 7, 10, 14, 18, 19, 20, 21})
    --   bb..b: B, base index of current block-column, 11 bits (0<=B<=1863)
    --   qq..q: Q, rotation of current submatrix, 7 bits (0<=Q<=80)
    -- the rom format is:
    -- ADDRESS DATA
    -- 128*i     0000000000000ddddd # degree minus 1 of block-row #0 of matrix #i
    -- 128*i+1   bbbbbbbbbbbqqqqqqq # non-zero block-column #0
    -- 128*i+2   bbbbbbbbbbbqqqqqqq # non-zero block-column #1
    -- ...
    -- 128*i+D+1 bbbbbbbbbbbqqqqqqq # non-zero block-column #D
    -- ...       0000000000000ddddd # degree minus 1 of block-row #1
    -- ...       bbbbbbbbbbbqqqqqqq # non-zero block-column #0
    -- ...
    -- ...       bbbbbbbbbbbqqqqqqq # non-zero block-column #D
    -- ...       0000000000000ddddd # degree minus 1 of block-row #M-1
    -- ...       bbbbbbbbbbbqqqqqqq # non-zero block-column #0
    -- ...
    -- ...       bbbbbbbbbbbqqqqqqq # non-zero block-column #D
    type rom_type is array(natural range 0 to num_compact_matrix * 128 - 1) of std_ulogic_vector(17 downto 0);

    function rom_init return rom_type is
        variable res: rom_type := (others => (others => '0'));
        variable add: natural;
        variable cm: compact_matrix;
    begin
        for i in 0 to num_compact_matrix - 1 loop
            cm := compact_matrix_list(i);
            add := i * 128;
            for br in 0 to cm.m - 1 loop
                res(add)(4 downto 0) := std_logic_vector(to_unsigned(cm.d(br) - 1, 5));
                add := add + 1;
                for bc in 0 to max_num_block_cols - 1 loop
                    next when cm.q(br, bc) < 0;
                    res(add)(17 downto 7) := std_logic_vector(to_unsigned(cm.z * bc, 11));
                    res(add)(6 downto 0) := std_logic_vector(to_unsigned(cm.q(br, bc), 7));
                    add := add + 1;
                end loop;
            end loop;
        end loop;
        return res;
    end function rom_init;

    signal cmd:   bitfield_type;
    signal cmd_d: bitfield_type;

    signal srstn: std_ulogic;
    signal ce:    std_ulogic;
    signal err:   std_ulogic;
    alias status is pss2css.status;
    alias exec is css2pss.exec;

    signal load:  std_ulogic;

    constant rom_width:  positive := 11 + 7;
    constant rom_depth:  positive := num_compact_matrix * 128;
    constant rom_awidth: positive := log2_up(num_compact_matrix) + 7;

    -- pipeline stage #1
    type stage1_type is record
        -- registered
        busy:                  std_ulogic;
        iter:                  natural range 0 to 2**cmd.mnim1'length - 1; -- iteration counter
        br:                    natural range 0 to max_num_block_rows - 1;  -- block row counter
        g:                     natural range 0 to 8;                       -- group row counter
        n:                     natural range 0 to max_degree - 1;          -- node counter
        phase:                 natural range 0 to 2;                       -- phase counter
        rom_add_fbc:           u_unsigned(6 downto 0);                     -- relative rom address of first block column definition of row
        rom_add_a:             u_unsigned(rom_awidth - 1 downto 0);        -- rom address port a (block column definitions)
        rom_add_b:             u_unsigned(rom_awidth - 1 downto 0);        -- rom address port b (degree of block row)
        -- combinatorial
        rom_add_a_p1:          u_unsigned(6 downto 0); -- rom_add_a(6 downto 0) + 1
        rom_add_a_p2:          u_unsigned(6 downto 0); -- rom_add_a(6 downto 0) + 2
    end record;
    signal stage1: stage1_type;

    -- pipeline stage #2
    type stage2_type is record
        -- registered
        busy:            std_ulogic;
        fi:              std_ulogic;
        fr:              std_ulogic; -- first row
        fgir:            std_ulogic;
        lgir:            std_ulogic;
        phase:           natural range 0 to 2;                       -- phase counter
        g:               natural range 0 to 8;
        rom_dout_a:      std_ulogic_vector(rom_width - 1 downto 0);
        rom_dout_b:      std_ulogic_vector(rom_width - 1 downto 0);
        -- combinatorial
        dm1:             natural range 0 to 31;
        dm2:             natural range 0 to 31;
        b:               integer range 0 to 2047;
        q:               integer range 0 to 80;
    end record;
    signal stage2: stage2_type;

    -- pipeline stage #3, combinatorial, pss requests to mss
    type stage3_type is record
        --registered
        busy:   std_ulogic;
        fi:     std_ulogic;
        r:      natural range 0 to max_num_rows_per_pe - 1; -- row counter
        phase:  natural range 0 to 2;             -- current phase
        n:      natural range 0 to max_degree - 1;          -- node counter
        fgir:   std_ulogic;
        lgir:   std_ulogic;
        -- combinatorial
        pss2mss: pss2mss_type;
    end record;
    signal stage3: stage3_type;

    -- pipeline stage #4, inputs of pes
    type stage4_type is record
        --registered
        busy:            std_ulogic;
        eoc:             std_ulogic;
        fi:              std_ulogic;
        fgir:            std_ulogic;
        lgir:            std_ulogic;
        phase:           natural range 0 to 2;              -- current phase
        n:               natural range 0 to max_degree - 1; -- current node index
        -- combinatorial
        gi:              message_vector(0 to npe - 1);
    end record;
    signal stage4: stage4_type;

    -- pipeline stage #5, inputs of pes
    type stage5_type is record
        -- combinatorial
        fi:              std_ulogic;
        fgir:            std_ulogic;
        phase:           natural range 0 to 2;             -- current phase
        n:               natural range 0 to max_degree - 1;   -- current node index
        aset_ram_we:     std_ulogic;                        -- write enable
        aset_ram_wa:     natural range 0 to max_num_rows_per_pe - 1;   -- write address
        aset_ram_ra:     natural range 0 to max_num_rows_per_pe - 1;   -- read address
        alpha_ram_we:    std_ulogic;                        -- write enable
        alpha_ram_wa:    natural range 0 to max_degree - 1; -- write address
        alpha_ram_ra:    natural range 0 to max_degree - 1; -- read address
        gi:              message_vector(0 to npe - 1);
        go:              message_vector(0 to npe - 1);
    end record;
    signal stage5: stage5_type;

    signal code:  natural range 0 to 2**cmd.code'length - 1;
    signal iba:   natural range 0 to 2**cmd.iba'length - 1;
    signal oba:   natural range 0 to 2**cmd.oba'length - 1;
    signal mnim1: natural range 0 to 2**cmd.mnim1'length - 1;
    signal estop: boolean;

    signal z012:  natural range 0 to 2;
    signal z:     natural range 0 to 81;
    signal m:     natural range 0 to max_num_block_rows;
    signal mm1:   natural range 0 to max_num_block_rows - 1;
    signal g:     natural range 0 to 9;
    signal gm1:   natural range 0 to 8;

begin

    -- css-pss protocol
    pss2css.eoc  <= stage4.eoc;
    pss2css.err  <= err;
    pss2css.data <= (others => '0'); -- LDPC does not support reading PSS internal registers from CSS
    pss2css.eirq <= (others => '0'); -- LDPC has zero extended interrupts

    ce     <= css2pss.ce;
    srstn  <= '0' when css2pss.srstn = '0' or stage4.eoc = '1' or err = '1' else '1';

    -- command parsing
    cmd_d  <= bitfield_slice(param);
    code   <= to_integer(u_unsigned(cmd.code));
    iba    <= to_integer(u_unsigned(cmd.iba));
    oba    <= to_integer(u_unsigned(cmd.oba));
    mnim1  <= to_integer(u_unsigned(cmd.mnim1));
    estop  <= cmd.estop = '1';

    -- code parsing
    z012   <= to_z012(code); -- size of submatrices (0,  1,  2)
    z      <= to_z(code);    -- size of submatrices (27, 54, 81)
    m      <= to_m(code);    -- number of block-rows
    mm1    <= to_mm1(code);  -- number of block-rows - 1
    g      <= to_g(code);    -- number of group-rows per block-row
    gm1    <= to_gm1(code);  -- number of group-rows per block-row - 1

    -- stage 1
    -- monitor exec commands from css
    -- generate rom addresses
    -- count iterations, block rows, group rows, block columns, phases
    stage1.rom_add_a_p1 <= stage1.rom_add_a(6 downto 0) + 1;
    stage1.rom_add_a_p2 <= stage1.rom_add_a(6 downto 0) + 2;

    stage1_p: process(clk)
        variable verr: std_ulogic;
        variable vstatus: status_type;
    begin
        if rising_edge(clk) then
            if srstn = '0' then
                err                          <= '0';
                status                       <= (others => '0');
                stage1.busy                  <= '0';
                stage1.iter                  <= 0;
                stage1.br                    <= 0;
                stage1.g                     <= 0;
                stage1.n                     <= 0;
                stage1.phase                 <= 0;
                stage1.rom_add_fbc           <= (others => '0');
                stage1.rom_add_a             <= (others => '0');
                stage1.rom_add_b             <= (others => '0');
                load                         <= '0';
                cmd                          <= cmd_none;
                verr                         := '0';
                vstatus                      := (others => '0');
            elsif ce = '1' then
                err                          <= '0';
                status                       <= (others => '0');
                load                         <= '0';
                if exec = '1' then
                    cmd   <= cmd_d;
                    load  <= '1';
                end if;
                if load = '1' then
                    check_param(cmd, verr, vstatus);
                    err                                       <= verr;
                    status                                    <= vstatus;
                    stage1.busy                               <= '1';
                    stage1.iter                               <= 0;
                    stage1.br                                 <= 0;
                    stage1.g                                  <= 0;
                    stage1.n                                  <= 0;
                    stage1.phase                              <= 1;
                    stage1.rom_add_fbc                        <= to_unsigned(1, 7);
                    stage1.rom_add_a(rom_awidth - 1 downto 7) <= u_unsigned(cmd.code);
                    stage1.rom_add_b(rom_awidth - 1 downto 7) <= u_unsigned(cmd.code);
                    stage1.rom_add_a(6 downto 0)              <= to_unsigned(1, 7);
                    stage1.rom_add_b(6 downto 0)              <= to_unsigned(0, 7);
                elsif stage1.busy = '1' then
                    if stage1.phase = 0 then
                        if stage1.n = 1 then
                            stage1.phase <= 2;
                            stage1.n     <= 0;
                        else
                            stage1.n <= stage1.n + 1;
                        end if;
                    elsif stage1.phase = 1 then
                        if stage1.n >= 1 and stage1.n = stage2.dm1 then
                            stage1.rom_add_a(6 downto 0) <= stage1.rom_add_fbc;
                            stage1.phase                 <= 0;
                            stage1.n                     <= 0;
                        else
                            stage1.rom_add_a(6 downto 0) <= stage1.rom_add_a_p1;
                            stage1.n                     <= stage1.n + 1;
                        end if;
                    else -- stage1.phase = 2
                        if stage1.n = stage2.dm1 then
                            stage1.rom_add_a(6 downto 0) <= stage1.rom_add_fbc;
                            stage1.phase                 <= 1;
                            stage1.n                     <= 0;
                            if stage1.g = gm1 then
                                stage1.g                     <= 0;
                                stage1.rom_add_fbc           <= stage1.rom_add_a_p2;
                                stage1.rom_add_a(6 downto 0) <= stage1.rom_add_a_p2;
                                stage1.rom_add_b(6 downto 0) <= stage1.rom_add_a_p1;
                                if stage1.br = mm1 then
                                    stage1.br                    <= 0;
                                    stage1.rom_add_fbc           <= to_unsigned(1, 7);
                                    stage1.rom_add_a(6 downto 0) <= to_unsigned(1, 7);
                                    stage1.rom_add_b(6 downto 0) <= to_unsigned(0, 7);
                                    if stage1.iter = mnim1 then
                                        stage1.busy        <= '0';
                                        stage1.iter        <= 0;
                                        stage1.br          <= 0;
                                        stage1.g           <= 0;
                                        stage1.n           <= 0;
                                        stage1.phase       <= 0;
                                        stage1.rom_add_fbc <= (others => '0');
                                        stage1.rom_add_a   <= (others => '0');
                                        stage1.rom_add_b   <= (others => '0');
                                    else
                                        stage1.iter <= stage1.iter + 1;
                                    end if;
                                else
                                    stage1.br <= stage1.br + 1;
                                end if;
                            else
                                stage1.g <= stage1.g + 1;
                            end if;
                        else
                            stage1.rom_add_a(6 downto 0) <= stage1.rom_add_a_p1;
                            stage1.n                     <= stage1.n + 1;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process stage1_p;

    -- stage 2
    -- rom outputs (block row degree, block column base index, submatrix rotation)
    -- copy stage1 control fields
    stage2_ps: process(clk)
        constant rom: rom_type := rom_init;
    begin
        if rising_edge(clk) then
            if srstn = '0' then
                stage2.busy       <= '0';
                stage2.fi         <= '0';
                stage2.fr         <= '0';
                stage2.fgir       <= '0';
                stage2.phase      <= 0;
                stage2.rom_dout_a <= (others => '0');
                stage2.rom_dout_b <= (others => '0');
                stage2.g          <= 0;
            else
                stage2.busy       <= stage1.busy;
                stage2.fi         <= '1' when stage1.busy = '1' and stage1.iter = 0 else
                                     '0';
                stage2.fr         <= '1' when stage1.busy = '1' and stage1.br = 0 and stage1.g = 0 else
                                     '0';
                stage2.fgir       <= '1' when stage1.busy = '1' and stage1.phase /= 2 and stage1.n = 0 else
                                     '0';
                stage2.phase      <= 1 when stage1.busy = '1' and stage1.phase = 1 else
                                     2 when stage2.busy = '1' and stage2.lgir = '1' and stage2.phase = 1 else
                                     0 when stage2.busy = '1' and stage2.lgir = '1' and stage2.phase = 2;
                stage2.rom_dout_a <= rom(to_integer(stage1.rom_add_a)) when stage1.busy = '1' else
                                     (others => '0');
                stage2.rom_dout_b <= rom(to_integer(stage1.rom_add_b)) when stage1.busy = '1' else
                                     (others => '0');
                stage2.g          <= stage1.g when stage1.busy = '1' else
                                     0;
            end if;
        end if;
    end process stage2_ps;

    stage2.lgir <= '1' when stage1.busy = '1' and ((stage1.phase = 0 and stage1.n = 0) or (stage1.phase = 2 and stage1.n = stage2.dm2)) else '0';
    stage2.dm1  <= to_integer(u_unsigned(stage2.rom_dout_b(4 downto 0)));     -- block row degree - 1
    stage2.dm2  <= to_integer(u_unsigned(stage2.rom_dout_b(4 downto 0)) - 1); -- block row degree - 2
    stage2.b    <= to_integer(u_unsigned(stage2.rom_dout_a(17 downto 7)));    -- base index of block column
    stage2.q    <= to_integer(u_unsigned(stage2.rom_dout_a(6 downto 0)));     -- submatrix rotation

    -- stage 3
    -- read-write requests to posterior memory
    pss2mss <= stage3.pss2mss;

    stage3_pc: process(stage2, stage4, stage5, iba, z)
        variable idxb: natural range 0 to nrams * 2048 - 1;
        variable idx: natural range 0 to nrams * 2048 - 1;
    begin
        stage3.pss2mss     <= pss2mss_none;
        stage3.pss2mss.en  <= '1' when (stage2.busy = '1' and stage2.phase = 1) or (stage4.busy = '1' and stage4.phase = 2) else
                              '0';
        stage3.pss2mss.wnr <= '1' when stage4.busy = '1' and stage4.phase = 2 else
                              '0';
        idxb := iba * 2048 + stage2.b;
        for pe in 0 to npe - 1 loop
            idx := stage2.q + stage2.g * 9 + pe;
            if idx >= z then
                idx := idx - z;
            end if;
            idx := idxb + idx;
            stage3.pss2mss.channels(pe).add  <= std_ulogic_vector(to_unsigned(idx, 14)) when (stage2.busy = '1' and stage2.phase = 1) or (stage4.busy = '1' and stage4.phase = 2) else
                                                (others => '0');
            stage3.pss2mss.channels(pe).data <= std_ulogic_vector(to_signed(stage5.go(pe), bit_width)) when stage4.busy = '1' and stage4.phase = 2 else
                                                (others => '0');
        end loop; 
    end process stage3_pc;

    stage3_ps: process(clk)
    begin
        if rising_edge(clk) then
            if srstn = '0' then
                stage3.busy  <= '0';
                stage3.fi    <= '0';
                stage3.r     <= 0;
                stage3.phase <= 0;
                stage3.fgir  <= '0';
                stage3.lgir  <= '0';
                stage3.n     <= 0;
            else
                stage3.busy         <= stage2.busy;
                stage3.fi           <= stage2.fi when stage2.busy = '1' else '0';
                stage3.r            <= 0 when stage2.busy = '1' and stage2.fr = '1' and stage2.fgir = '1' and stage2.phase = 1 else
                                       stage3.r + 1 when stage2.busy = '1' and stage2.fgir = '1' and stage2.phase = 1;
                stage3.phase        <= stage2.phase when stage2.busy = '1' else 0;
                stage3.fgir         <= stage2.fgir when stage2.busy = '1' else '0';
                stage3.lgir         <= stage2.lgir when stage2.busy = '1' else '0';
                stage3.n            <= 0 when stage2.busy = '1' and stage2.fgir = '1' else
                                       stage3.n + 1 when stage2.busy = '1' and stage2.phase /= 0 else
                                       0;
            end if;
        end if;
    end process stage3_ps;

    -- stage 4, pes inputs and control
    stage4_p: process(clk)
    begin
        if rising_edge(clk) then
            if srstn = '0' then
                stage4.busy            <= '0';
                stage4.eoc             <= '0';
                stage4.fi              <= '0';
                stage4.fgir            <= '0';
                stage4.lgir            <= '0';
                stage4.phase           <= 0;
                stage4.n               <= 0;
            else
                stage4.busy            <= stage3.busy;
                stage4.eoc             <= '1' when stage4.busy = '1' and stage3.busy = '0' else
                                          '0';
                stage4.fi              <= stage3.fi when stage3.busy = '1' else '0';
                stage4.fgir            <= stage3.fgir when stage3.busy = '1' else '0';
                stage4.lgir            <= stage3.lgir when stage3.busy = '1' else '0';
                stage4.phase           <= stage3.phase when stage3.busy = '1' else 0;
                stage4.n               <= stage3.n when stage3.busy = '1' and stage3.phase /= 0 else
                                          0;
            end if;
        end if;
    end process stage4_p;

    stage4_g: for pe in 0 to npe - 1 generate
        stage4.gi(pe) <= to_integer(u_signed(mss2pss(pe)));
    end generate stage4_g;

    stage5.fi              <= stage4.fi when stage4.busy = '1' else '0';
    stage5.fgir            <= stage4.fgir when stage4.busy = '1' else '0';
    stage5.phase           <= stage4.phase when stage4.busy = '1' else 0;
    stage5.n               <= stage4.n when stage4.busy = '1' and stage4.phase /= 0 else 0;
    stage5.aset_ram_we     <= '1' when stage4.busy = '1' and stage4.phase = 1 and stage4.lgir = '1' else '0';
    stage5.aset_ram_wa     <= stage3.r when stage4.busy = '1' and stage4.phase = 1 and stage4.lgir = '1' else 0;
    stage5.aset_ram_ra     <= stage3.r when stage4.busy = '1' and stage3.phase = 1 else 0;
    stage5.alpha_ram_we    <= '1' when stage4.busy = '1' and stage4.phase = 1 else '0';
    stage5.alpha_ram_wa    <= stage4.n when stage4.busy = '1' and stage4.phase = 1 else 0;
    stage5.alpha_ram_ra    <= stage3.n when stage4.busy = '1' and stage3.phase = 2 else 0;
    stage5.gi              <= stage4.gi;

    -- pes
    stage5_g: for pe in 0 to npe - 1 generate
        u: entity work.pe
            port map(
                clk             => clk,
                srstn           => srstn,
                fi              => stage5.fi,
                fgir            => stage5.fgir,
                phase           => stage5.phase,
                n               => stage5.n,
                aset_ram_we     => stage5.aset_ram_we,
                aset_ram_wa     => stage5.aset_ram_wa,
                aset_ram_ra     => stage5.aset_ram_ra,
                alpha_ram_we    => stage5.alpha_ram_we,
                alpha_ram_wa    => stage5.alpha_ram_wa,
                alpha_ram_ra    => stage5.alpha_ram_ra,
                gi              => stage5.gi(pe),
                go              => stage5.go(pe)
            );
    end generate stage5_g;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
