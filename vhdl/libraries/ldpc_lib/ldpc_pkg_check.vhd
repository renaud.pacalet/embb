--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

use work.ldpc_pkg_local.all;
use work.ldpc_sim_pkg.all;

entity ldpc_pkg_check is
    generic(
        sigma:          real    := 0.45;
        max_iterations: integer := 6;
        rootdir:        string
    );
end entity ldpc_pkg_check;

architecture sim of ldpc_pkg_check is
begin

    process
    begin
        if ldpc_pkg_test(sigma, max_iterations, rootdir) then
            report "Non-regression test passed" severity note;
        else
            report "Non-regression test failed" severity failure;
        end if;
        wait;
    end process;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
