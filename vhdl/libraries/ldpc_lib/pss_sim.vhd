--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

use std.textio.all;
use std.env.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;

use work.ldpc_pkg_local.all;
use work.bitfield.all;

entity pss_sim is
end entity pss_sim;

architecture sim of pss_sim is

    signal clk: std_ulogic;
    signal srstn: std_ulogic;

    signal css2pss: css2pss_type;
    signal pss2css: pss2css_type;
    signal pss2mss: pss2mss_type;
    signal mss2pss: mss2pss_type;
    signal css2mss: css2mss_type;
    signal mss2css: mss2css_type;

    signal param : std_ulogic_vector (cmdsize*8 -1 downto 0); 

begin

    i_pss: entity work.pss(rtl)
    port map(
        clk     => clk,
        param   => param,
        css2pss => css2pss,
        pss2css => pss2css,
        pss2mss => pss2mss,
        mss2pss => mss2pss);

    i_mss: entity work.mss(rtl)
    port map(
        clk        => clk,
        srstn      => srstn,
        css2mss    => css2mss,
        mss2css    => mss2css,
        pss2mss    => pss2mss,
        mss2pss    => mss2pss
    );

    process
    begin
        clk <= '0';
        wait for 1 ns;
        clk <= '1';
        wait for 1 ns;
    end process;

    process
        variable vparam: bitfield_type;
    begin
        srstn <= '0';
        param <= (others => '0');
        css2pss <= css2pss_none;
        for i in 1 to 10 loop
            wait until rising_edge(clk);
        end loop;
        srstn <= '1';
        css2pss <= (srstn => '1', ce => '1', exec => '0');
        for i in 1 to 10 loop
            wait until rising_edge(clk);
        end loop;
        for j in 0 to 15 loop
            vparam.code  := std_ulogic_vector(to_unsigned(j, vparam.code'length));
            vparam.iba   := std_ulogic_vector(to_unsigned(0, vparam.iba'length));
            vparam.oba   := std_ulogic_vector(to_unsigned(8, vparam.oba'length));
            vparam.mnim1 := std_ulogic_vector(to_unsigned(5, vparam.mnim1'length));
            vparam.estop := '0';
            param <= bitfield_concat(vparam);
            css2pss.exec <= '1';
            wait until rising_edge(clk);
            css2pss.exec <= '0';
            wait until rising_edge(clk) and pss2css.eoc = '1';
        end loop;
        finish;
    end process;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
