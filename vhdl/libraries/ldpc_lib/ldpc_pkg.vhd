--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- Notations:
-- VN: variable node
-- CN: check node
-- gamma: posterior information
-- alpha: VN-to-CN message
-- lambda: erased VN-to-CN message
-- aset: compact representation of alphas of a row
-- beta:  CN-to-VN message

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;

use work.bitfield.all;
use work.errors.all;

package ldpc_pkg is

    generic(
        bit_width:    positive; -- bit-width of messages
        max_degree:   positive; -- maximum number of ones per row of parity check matrix
        nrams:        positive; -- number of 2kx8 rams in mss
        npe:          positive  -- number of processing elements
    );

    -- various interfaces
    type pss2mss_channel_type is record
        add:    std_ulogic_vector(13 downto 0); -- address
        data:   std_ulogic_vector(7 downto 0);  -- write data
    end record;
    constant pss2mss_channel_none: pss2mss_channel_type := (add => (others => '0'), data => (others => '0'));
    type pss2mss_channel_vector is array(natural range <>) of pss2mss_channel_type;
    type pss2mss_type is record
        en:     std_ulogic;                     -- request
        wnr:    std_ulogic;                     -- write-not-read
        channels: pss2mss_channel_vector(0 to npe - 1);
    end record;
    constant pss2mss_none: pss2mss_type := (en => '0', wnr => '0', channels => (others => pss2mss_channel_none));

    alias mss2pss_channel_type is word8;
    constant mss2pss_channel_none: mss2pss_channel_type := (others => '0');
    subtype mss2pss_type is word8_vector(0 to npe - 1);

    constant cmdsize:  positive := bitfield_width/8;

    function is_mapped(add: std_ulogic_vector) return boolean;
    function is_read_only(add: std_ulogic_vector) return boolean;

    procedure check_param(cmd: in bitfield_type; err: out std_ulogic; status: out status_type);

    -- ldpc codes
    type code_rate is (cr56, cr34, cr23, cr12);
    type code_rate_to_integer is array(code_rate) of integer;

    constant kmin_table:       code_rate_to_integer := (cr56 => 540, cr34 => 486, cr23 => 432, cr12 => 324);
    constant kmax_table:       code_rate_to_integer := (cr56 => 1620, cr34 => 1458, cr23 => 1296, cr12 => 972);
    constant n_ldpc_b_table:   code_rate_to_integer := (others => 24);
    constant k_ldpc_b_table:   code_rate_to_integer := (cr56 => 20, cr34 => 18, cr23 => 16, cr12 => 12);
    constant n_parity_b_table: code_rate_to_integer := (cr56 => 4, cr34 => 6, cr23 => 8, cr12 => 12);

    constant min_num_block_rows: positive := 4;
    constant max_num_block_rows: positive := 12;
    constant min_num_block_cols: positive := 24;
    constant max_num_block_cols: positive := 24;
    constant min_z: positive := 27;
    constant max_z: positive := 81;
    constant min_num_rows: positive := min_z * min_num_block_rows;
    constant max_num_rows: positive := max_z * max_num_block_rows;
    constant min_num_cols: positive := min_z * min_num_block_cols;
    constant max_num_cols: positive := max_z * max_num_block_cols;
    constant max_num_rows_per_pe: positive := max_num_rows / npe;

    subtype magnitude is natural range 0 to 2**(bit_width - 1) - 1;                     -- absolute value of messages
    subtype message is integer range -2**(bit_width - 1) + 1 to 2**(bit_width - 1) - 1; -- alpha, beta, lambda, gamma messages
    type message_vector is array(natural range <>) of message;
    type message_matrix is array(natural range <>) of message_vector;

    -- compact representation of set of alpha of a row
    type aset is record
        m1:  magnitude;                              -- first minimum of absolute values of alphas
        m2:  magnitude;                              -- second minimum of absolute values of alphas
        i: natural range 0 to max_degree - 1;        -- argmin of absolute values of alphas
        as: std_ulogic_vector(0 to max_degree - 1);  -- signs of alphas ('0': +1)
        sp: std_ulogic;                              -- exclusive or of signs of alphas ('0': +1)
        ae: std_ulogic_vector(0 to max_degree - 1);  -- erasure flags of alphas ('1': erased)
    end record aset;
    constant aset_uninitialized: aset := (m1 => 0, m2 => 0, i => 0, as => (others => 'X'), sp => 'X', ae => (others => 'X'));
    type aset_vector is array(natural range <>) of aset;

    type integer_matrix is array(integer range <>, integer range <>) of integer;

    -- compact matrices of quasi-cyclic LDPC codes in 802.11.n
    -- z: size of block matrices
    -- m: number of block rows
    -- n: number of block columns
    -- d: degree of block rows (number of non-zero block matrices per block row)
    -- q: matrix of block matrices characteristic integers
    -- a m x n compact matrix contains one characteristic integer i per corresponding z x z block matrix of the m*z x n*z full parity check matrix:
    --   i=0: z x z identity
    --   i>0: z x z identity, right-rotated by i positions
    --   i<0: z x z zero matrix
    type compact_matrix is record
        z: positive;
        m: positive;
        n: positive;
        d: integer_vector(0 to max_num_block_rows - 1);
        q: integer_matrix(0 to max_num_block_rows - 1, 0 to max_num_block_cols - 1);
    end record compact_matrix;

    type compact_matrix_vector is array(natural range <>) of compact_matrix;

    -- naming convention qc_802_11n_Rrr_Nnnnn_Zzz:
    --   rr:   rate (12: 1/2, 23: 2/3, 34: 3/4, 56: 5/6)
    --   nnnn: code length
    --   zz:   size of block matrices
    constant qc_802_11n_R12_Z27: compact_matrix;
    constant qc_802_11n_R12_Z54: compact_matrix;
    constant qc_802_11n_R12_Z81: compact_matrix;
    constant qc_802_11n_R23_Z27: compact_matrix;
    constant qc_802_11n_R23_Z54: compact_matrix;
    constant qc_802_11n_R23_Z81: compact_matrix;
    constant qc_802_11n_R34_Z27: compact_matrix;
    constant qc_802_11n_R34_Z54: compact_matrix;
    constant qc_802_11n_R34_Z81: compact_matrix;
    constant qc_802_11n_R56_Z27: compact_matrix;
    constant qc_802_11n_R56_Z54: compact_matrix;
    constant qc_802_11n_R56_Z81: compact_matrix;

    constant num_compact_matrix: positive := 12;

    constant compact_matrix_list: compact_matrix_vector(0 to num_compact_matrix - 1);

    constant cmd_vector_none: std_logic_vector(bitfield_width - 1 downto 0) := (others => '0');
    constant cmd_none: bitfield_type := bitfield_slice(cmd_vector_none);

    constant to_z:    integer_vector(0 to 2**cmd_none.code'length - 1);
    constant to_z012: integer_vector(0 to 2**cmd_none.code'length - 1);
    constant to_g:    integer_vector(0 to 2**cmd_none.code'length - 1);
    constant to_gm1:  integer_vector(0 to 2**cmd_none.code'length - 1);
    constant to_m:    integer_vector(0 to 2**cmd_none.code'length - 1);
    constant to_mm1:  integer_vector(0 to 2**cmd_none.code'length - 1);
    constant to_n:    integer_vector(0 to 2**cmd_none.code'length - 1);
    constant to_nm1:  integer_vector(0 to 2**cmd_none.code'length - 1);

    -- convert z (size of block matrices) and q (matrix of block matrices characteristic integers) into compact_matrix
    function generate_compact_matrix(z: positive; q: integer_matrix) return compact_matrix;

    -- saturate value in [-2**(bit_width-1)+1...2**(bit_width-1)-1] range
    function scms_saturate(v: integer) return message;

    -- convert sign of message to '0'/'1'
    function scms_sign_zo(m: message) return std_ulogic;

end package ldpc_pkg;

package body ldpc_pkg is

    function generate_compact_matrix(z: positive; q: integer_matrix) return compact_matrix is
        constant m: positive := q'length(1);
        constant n: positive := q'length(2);
        constant p: integer_matrix(0 to m - 1, 0 to n - 1) := q;
        variable cm: compact_matrix;
    begin
        cm.z := z;
        cm.m := m;
        cm.n := n;
        for i in 0 to m - 1 loop
            cm.d(i) := 0;
            for j in 0 to n - 1 loop
                cm.q(i, j) := p(i, j);
                if cm.q(i, j) >= 0 then
                    cm.d(i) := cm.d(i) + 1;
                end if;
            end loop;
        end loop;
        return cm;
    end function generate_compact_matrix;

    constant qc_802_11n_R12_Z27_q: integer_matrix := (
        (0, -1, -1, -1, 0, 0, -1, -1, 0, -1, -1, 0, 1, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1),
        (22, 0, -1, -1, 17, -1, 0, 0, 12, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1),
        (6, -1, 0, -1, 10, -1, -1, -1, 24, -1, 0, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1),
        (2, -1, -1, 0, 20, -1, -1, -1, 25, 0, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1),
        (23, -1, -1, -1, 3, -1, -1, -1, 0, -1, 9, 11, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1),
        (24, -1, 23, 1, 17, -1, 3, -1, 10, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1),
        (25, -1, -1, -1, 8, -1, -1, -1, 7, 18, -1, -1, 0, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1),
        (13, 24, -1, -1, 0, -1, 8, -1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1),
        (7, 20, -1, 16, 22, 10, -1, -1, 23, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1),
        (11, -1, -1, -1, 19, -1, -1, -1, 13, -1, 3, 17, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1),
        (25, -1, 8, -1, 23, 18, -1, 14, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0),
        (3, -1, -1, -1, 16, -1, -1, 2, 25, 5, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0)
    );
    constant qc_802_11n_R12_Z27: compact_matrix := generate_compact_matrix(27, qc_802_11n_R12_Z27_q);

    constant qc_802_11n_R12_Z54_q: integer_matrix := (
        (40, -1, -1, -1, 22, -1, 49, 23, 43, -1, -1, -1, 1, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1),
        (50, 1, -1, -1, 48, 35, -1, -1, 13, -1, 30, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1),
        (39, 50, -1, -1, 4, -1, 2, -1, -1, -1, -1, 49, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1),
        (33, -1, -1, 38, 37, -1, -1, 4, 1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1),
        (45, -1, -1, -1, 0, 22, -1, -1, 20, 42, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1),
        (51, -1, -1, 48, 35, -1, -1, -1, 44, -1, 18, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1),
        (47, 11, -1, -1, -1, 17, -1, -1, 51, -1, -1, -1, 0, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1),
        (5, -1, 25, -1, 6, -1, 45, -1, 13, 40, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1),
        (33, -1, -1, 34, 24, -1, -1, -1, 23, -1, -1, 46, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1),
        (1, -1, 27, -1, 1, -1, -1, -1, 38, -1, 44, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1),
        (-1, 18, -1, -1, 23, -1, -1, 8, 0, 35, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0),
        (49, -1, 17, -1, 30, -1, -1, -1, 34, -1, -1, 19, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0)
    );
    constant qc_802_11n_R12_Z54: compact_matrix := generate_compact_matrix(54, qc_802_11n_R12_Z54_q);

    constant qc_802_11n_R12_Z81_q: integer_matrix := (
        (57, -1, -1, -1, 50, -1, 11, -1, 50, -1, 79, -1, 1, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1),
        (3, -1, 28, -1, 0, -1, -1, -1, 55, 7, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1),
        (30, -1, -1, -1, 24, 37, -1, -1, 56, 14, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1),
        (62, 53, -1, -1, 53, -1, -1, 3, 35, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1),
        (40, -1, -1, 20, 66, -1, -1, 22, 28, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1),
        (0, -1, -1, -1, 8, -1, 42, -1, 50, -1, -1, 8, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1),
        (69, 79, 79, -1, -1, -1, 56, -1, 52, -1, -1, -1, 0, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1),
        (65, -1, -1, -1, 38, 57, -1, -1, 72, -1, 27, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1),
        (64, -1, -1, -1, 14, 52, -1, -1, 30, -1, -1, 32, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1),
        (-1, 45, -1, 70, 0, -1, -1, -1, 77, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1),
        (2, 56, -1, 57, 35, -1, -1, -1, -1, -1, 12, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0),
        (24, -1, 61, -1, 60, -1, -1, 27, 51, -1, -1, 16, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0)
    );
    constant qc_802_11n_R12_Z81: compact_matrix := generate_compact_matrix(81, qc_802_11n_R12_Z81_q);

    constant qc_802_11n_R23_Z27_q: integer_matrix := (
        (25, 26, 14, -1, 20, -1, 2, -1, 4, -1, -1, 8, -1, 16, -1, 18, 1, 0, -1, -1, -1, -1, -1, -1),
        (10, 9, 15, 11, -1, 0, -1, 1, -1, -1, 18, -1, 8, -1, 10, -1, -1, 0, 0, -1, -1, -1, -1, -1),
        (16, 2, 20, 26, 21, -1, 6, -1, 1, 26, -1, 7, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1),
        (10, 13, 5, 0, -1, 3, -1, 7, -1, -1, 26, -1, -1, 13, -1, 16, -1, -1, -1, 0, 0, -1, -1, -1),
        (23, 14, 24, -1, 12, -1, 19, -1, 17, -1, -1, -1, 20, -1, 21, -1, 0, -1, -1, -1, 0, 0, -1, -1),
        (6, 22, 9, 20, -1, 25, -1, 17, -1, 8, -1, 14, -1, 18, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1),
        (14, 23, 21, 11, 20, -1, 24, -1, 18, -1, 19, -1, -1, -1, -1, 22, -1, -1, -1, -1, -1, -1, 0, 0),
        (17, 11, 11, 20, -1, 21, -1, 26, -1, 3, -1, -1, 18, -1, 26, -1, 1, -1, -1, -1, -1, -1, -1, 0)
    );
    constant qc_802_11n_R23_Z27: compact_matrix := generate_compact_matrix(27, qc_802_11n_R23_Z27_q);

    constant qc_802_11n_R23_Z54_q: integer_matrix := (
        (39, 31, 22, 43, -1, 40, 4, -1, 11, -1, -1, 50, -1, -1, -1, 6, 1, 0, -1, -1, -1, -1, -1, -1),
        (25, 52, 41, 2, 6, -1, 14, -1, 34, -1, -1, -1, 24, -1, 37, -1, -1, 0, 0, -1, -1, -1, -1, -1),
        (43, 31, 29, 0, 21, -1, 28, -1, -1, 2, -1, -1, 7, -1, 17, -1, -1, -1, 0, 0, -1, -1, -1, -1),
        (20, 33, 48, -1, 4, 13, -1, 26, -1, -1, 22, -1, -1, 46, 42, -1, -1, -1, -1, 0, 0, -1, -1, -1),
        (45, 7, 18, 51, 12, 25, -1, -1, -1, 50, -1, -1, 5, -1, -1, -1, 0, -1, -1, -1, 0, 0, -1, -1),
        (35, 40, 32, 16, 5, -1, -1, 18, -1, -1, 43, 51, -1, 32, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1),
        (9, 24, 13, 22, 28, -1, -1, 37, -1, -1, 25, -1, -1, 52, -1, 13, -1, -1, -1, -1, -1, -1, 0, 0),
        (32, 22, 4, 21, 16, -1, -1, -1, 27, 28, -1, 38, -1, -1, -1, 8, 1, -1, -1, -1, -1, -1, -1, 0)
    );
    constant qc_802_11n_R23_Z54: compact_matrix := generate_compact_matrix(54, qc_802_11n_R23_Z54_q);

    constant qc_802_11n_R23_Z81_q: integer_matrix := (
        (61, 75, 4, 63, 56, -1, -1, -1, -1, -1, -1, 8, -1, 2, 17, 25, 1, 0, -1, -1, -1, -1, -1, -1),
        (56, 74, 77, 20, -1, -1, -1, 64, 24, 4, 67, -1, 7, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1),
        (28, 21, 68, 10, 7, 14, 65, -1, -1, -1, 23, -1, -1, -1, 75, -1, -1, -1, 0, 0, -1, -1, -1, -1),
        (48, 38, 43, 78, 76, -1, -1, -1, -1, 5, 36, -1, 15, 72, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1),
        (40, 2, 53, 25, -1, 52, 62, -1, 20, -1, -1, 44, -1, -1, -1, -1, 0, -1, -1, -1, 0, 0, -1, -1),
        (69, 23, 64, 10, 22, -1, 21, -1, -1, -1, -1, -1, 68, 23, 29, -1, -1, -1, -1, -1, -1, 0, 0, -1),
        (12, 0, 68, 20, 55, 61, -1, 40, -1, -1, -1, 52, -1, -1, -1, 44, -1, -1, -1, -1, -1, -1, 0, 0),
        (58, 8, 34, 64, 78, -1, -1, 11, 78, 24, -1, -1, -1, -1, -1, 58, 1, -1, -1, -1, -1, -1, -1, 0)
    );
    constant qc_802_11n_R23_Z81: compact_matrix := generate_compact_matrix(81, qc_802_11n_R23_Z81_q);

    constant qc_802_11n_R34_Z27_q: integer_matrix := (
        (16, 17, 22, 24, 9, 3, 14, -1, 4, 2, 7, -1, 26, -1, 2, -1, 21, -1, 1, 0, -1, -1, -1, -1),
        (25, 12, 12, 3, 3, 26, 6, 21, -1, 15, 22, -1, 15, -1, 4, -1, -1, 16, -1, 0, 0, -1, -1, -1),
        (25, 18, 26, 16, 22, 23, 9, -1, 0, -1, 4, -1, 4, -1, 8, 23, 11, -1, -1, -1, 0, 0, -1, -1),
        (9, 7, 0, 1, 17, -1, -1, 7, 3, -1, 3, 23, -1, 16, -1, -1, 21, -1, 0, -1, -1, 0, 0, -1),
        (24, 5, 26, 7, 1, -1, -1, 15, 24, 15, -1, 8, -1, 13, -1, 13, -1, 11, -1, -1, -1, -1, 0, 0),
        (2, 2, 19, 14, 24, 1, 15, 19, -1, 21, -1, 2, -1, 24, -1, 3, -1, 2, 1, -1, -1, -1, -1, 0)
    );
    constant qc_802_11n_R34_Z27: compact_matrix := generate_compact_matrix(27, qc_802_11n_R34_Z27_q);

    constant qc_802_11n_R34_Z54_q: integer_matrix := (
        (39, 40, 51, 41, 3, 29, 8, 36, -1, 14, -1, 6, -1, 33, -1, 11, -1, 4, 1, 0, -1, -1, -1, -1),
        (48, 21, 47, 9, 48, 35, 51, -1, 38, -1, 28, -1, 34, -1, 50, -1, 50, -1, -1, 0, 0, -1, -1, -1),
        (30, 39, 28, 42, 50, 39, 5, 17, -1, 6, -1, 18, -1, 20, -1, 15, -1, 40, -1, -1, 0, 0, -1, -1),
        (29, 0, 1, 43, 36, 30, 47, -1, 49, -1, 47, -1, 3, -1, 35, -1, 34, -1, 0, -1, -1, 0, 0, -1),
        (1, 32, 11, 23, 10, 44, 12, 7, -1, 48, -1, 4, -1, 9, -1, 17, -1, 16, -1, -1, -1, -1, 0, 0),
        (13, 7, 15, 47, 23, 16, 47, -1, 43, -1, 29, -1, 52, -1, 2, -1, 53, -1, 1, -1, -1, -1, -1, 0)
    );
    constant qc_802_11n_R34_Z54: compact_matrix := generate_compact_matrix(54, qc_802_11n_R34_Z54_q);

    constant qc_802_11n_R34_Z81_q: integer_matrix := (
        (48, 29, 28, 39, 9, 61, -1, -1, -1, 63, 45, 80, -1, -1, -1, 37, 32, 22, 1, 0, -1, -1, -1, -1),
        (4, 49, 42, 48, 11, 30, -1, -1, -1, 49, 17, 41, 37, 15, -1, 54, -1, -1, -1, 0, 0, -1, -1, -1),
        (35, 76, 78, 51, 37, 35, 21, -1, 17, 64, -1, -1, -1, 59, 7, -1, -1, 32, -1, -1, 0, 0, -1, -1),
        (9, 65, 44, 9, 54, 56, 73, 34, 42, -1, -1, -1, 35, -1, -1, -1, 46, 39, 0, -1, -1, 0, 0, -1),
        (3, 62, 7, 80, 68, 26, -1, 80, 55, -1, 36, -1, 26, -1, 9, -1, 72, -1, -1, -1, -1, -1, 0, 0),
        (26, 75, 33, 21, 69, 59, 3, 38, -1, -1, -1, 35, -1, 62, 36, 26, -1, -1, 1, -1, -1, -1, -1, 0)
    );
    constant qc_802_11n_R34_Z81: compact_matrix := generate_compact_matrix(81, qc_802_11n_R34_Z81_q);

    constant qc_802_11n_R56_Z27_q: integer_matrix := (
        (17, 13, 8, 21, 9, 3, 18, 12, 10, 0, 4, 15, 19, 2, 5, 10, 26, 19, 13, 13, 1, 0, -1, -1),
        (3, 12, 11, 14, 11, 25, 5, 18, 0, 9, 2, 26, 26, 10, 24, 7, 14, 20, 4, 2, -1, 0, 0, -1),
        (22, 16, 4, 3, 10, 21, 12, 5, 21, 14, 19, 5, -1, 8, 5, 18, 11, 5, 5, 15, 0, -1, 0, 0),
        (7, 7, 14, 14, 4, 16, 16, 24, 24, 10, 1, 7, 15, 6, 10, 26, 8, 18, 21, 14, 1, -1, -1, 0)
    );
    constant qc_802_11n_R56_Z27: compact_matrix := generate_compact_matrix(27, qc_802_11n_R56_Z27_q);

    constant qc_802_11n_R56_Z54_q: integer_matrix := (
        (48, 29, 37, 52, 2, 16, 6, 14, 53, 31, 34, 5, 18, 42, 53, 31, 45, -1, 46, 52, 1, 0, -1, -1),
        (17, 4, 30, 7, 43, 11, 24, 6, 14, 21, 6, 39, 17, 40, 47, 7, 15, 41, 19, -1, -1, 0, 0, -1),
        (7, 2, 51, 31, 46, 23, 16, 11, 53, 40, 10, 7, 46, 53, 33, 35, -1, 25, 35, 38, 0, -1, 0, 0),
        (19, 48, 41, 1, 10, 7, 36, 47, 5, 29, 52, 52, 31, 10, 26, 6, 3, 2, -1, 51, 1, -1, -1, 0)
    );
    constant qc_802_11n_R56_Z54: compact_matrix := generate_compact_matrix(54, qc_802_11n_R56_Z54_q);

    constant qc_802_11n_R56_Z81_q: integer_matrix := (
        (13, 48, 80, 66, 4, 74, 7, 30, 76, 52, 37, 60, -1, 49, 73, 31, 74, 73, 23, -1, 1, 0, -1, -1),
        (69, 63, 74, 56, 64, 77, 57, 65, 6, 16, 51, -1, 64, -1, 68, 9, 48, 62, 54, 27, -1, 0, 0, -1),
        (51, 15, 0, 80, 24, 25, 42, 54, 44, 71, 71, 9, 67, 35, -1, 58, -1, 29, -1, 53, 0, -1, 0, 0),
        (16, 29, 36, 41, 44, 56, 59, 37, 50, 24, -1, 65, 4, 65, 52, -1, 4, -1, 73, 52, 1, -1, -1, 0)
    );
    constant qc_802_11n_R56_Z81: compact_matrix := generate_compact_matrix(81, qc_802_11n_R56_Z81_q);

    constant compact_matrix_list: compact_matrix_vector(0 to num_compact_matrix - 1) := (
        qc_802_11n_R12_Z27,
        qc_802_11n_R12_Z54,
        qc_802_11n_R12_Z81,
        qc_802_11n_R23_Z27,
        qc_802_11n_R23_Z54,
        qc_802_11n_R23_Z81,
        qc_802_11n_R34_Z27,
        qc_802_11n_R34_Z54,
        qc_802_11n_R34_Z81,
        qc_802_11n_R56_Z27,
        qc_802_11n_R56_Z54,
        qc_802_11n_R56_Z81
    );

    constant to_z:    integer_vector(0 to 2**cmd_none.code'length - 1) := (27, 54, 81, 27, 54, 81, 27, 54, 81, 27, 54, 81, others => 0);
    constant to_z012: integer_vector(0 to 2**cmd_none.code'length - 1) := (0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, others => 0);
    constant to_g:    integer_vector(0 to 2**cmd_none.code'length - 1) := (3, 6, 9, 3, 6, 9, 3, 6, 9, 3, 6, 9, others => 0);
    constant to_gm1:  integer_vector(0 to 2**cmd_none.code'length - 1) := (2, 5, 8, 2, 5, 8, 2, 5, 8, 2, 5, 8, others => 0);
    constant to_m:    integer_vector(0 to 2**cmd_none.code'length - 1) := (12, 12, 12, 8, 8, 8, 6, 6, 6, 4, 4 ,4, others => 0);
    constant to_mm1:  integer_vector(0 to 2**cmd_none.code'length - 1) := (11, 11, 11, 7, 7, 7, 5, 5, 5, 3, 3 ,3, others => 0);
    constant to_n:    integer_vector(0 to 2**cmd_none.code'length - 1) := (others => 24);
    constant to_nm1:  integer_vector(0 to 2**cmd_none.code'length - 1) := (others => 23);

    function is_mapped(add: std_ulogic_vector) return boolean is
        constant uadd: u_unsigned(add'range) := u_unsigned(add);
    begin
        return uadd < nrams * 256;
    end function is_mapped;

    function is_read_only(add: std_ulogic_vector) return boolean is
    begin
        return false;
    end function is_read_only;

    procedure check_param(cmd: in bitfield_type; err: out std_ulogic; status: out status_type) is
    begin
        err    := '0';
        status := (others => '0');
        if unsigned(cmd.code) >= num_compact_matrix then
            err                       := '1';
            status(ldpc_unknown_code) := '1';
-- pragma translate_off
            assert false report ldpc_unknown_code_message severity warning;
-- pragma translate_on
        end if;
    end procedure check_param;

    function scms_saturate(v: integer) return message is
    begin
        if v < message'low then
            return message'low;
        elsif v > message'high then
            return message'high;
        else
            return v;
        end if;
    end function scms_saturate;

    function scms_sign_zo(m: message) return std_ulogic is
    begin
        if m < 0 then
            return '1';
        else
            return '0';
        end if;
    end function scms_sign_zo;

end package body ldpc_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
