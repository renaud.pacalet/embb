--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- mss simulation environment


use std.env.all;
use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.global.all;
use global_lib.sim_utils.all;

use work.ldpc_pkg_local.all;
use work.ldpc_sim_pkg.all;

entity mss_sim is
    generic(
        n:     positive range 100 to positive'high := 10000; -- number of test vectors
        debug: boolean := false                              -- print debug information
    );
end entity mss_sim;

architecture sim of mss_sim is

    package mss2uc_fifo_pkg is new global_lib.fifo_pkg generic map(T => mss2uc_type);
    use mss2uc_fifo_pkg.all; -- fifo of expected mss responses to uc
    package mss2dma_fifo_pkg is new global_lib.fifo_pkg generic map(T => mss2dma_type);
    use mss2dma_fifo_pkg.all; -- fifo of expected mss responses to dma
    package mss2vci_fifo_pkg is new global_lib.fifo_pkg generic map(T => mss2vci_type);
    use mss2vci_fifo_pkg.all; -- fifo of expected mss responses to vci

    signal eosd:    boolean;        -- end of simulation
    signal eosv:    boolean;        -- end of simulation
    signal eosu:    boolean;        -- end of simulation
    signal eosp:    boolean;        -- end of simulation
    signal clk:     std_ulogic;
    signal srstn:   std_ulogic;
    signal css2mss: css2mss_type;
    signal mss2css: mss2css_type;
    signal pss2mss: pss2mss_type;
    signal mss2pss: mss2pss_type;

    alias vci2mss is css2mss.vci2mss;
    alias mss2vci is mss2css.mss2vci;
    alias dma2mss is css2mss.dma2mss;
    alias mss2dma is mss2css.mss2dma;
    alias uc2mss  is css2mss.uc2mss;
    alias mss2uc  is mss2css.mss2uc;

begin

    -- mss (design under test)
    dut: entity work.mss
        generic map(
            debug => debug
        )
        port map(
            clk     => clk,
            srstn   => srstn,
            css2mss => css2mss,
            mss2css => mss2css,
            pss2mss => pss2mss,
            mss2pss => mss2pss
        );

        -- clock
        process
        begin
            clk <= '0';
            wait for 1 ns;
            clk <= '1';
            wait for 1 ns;
        end process;

        -- reset
        rst_req: process
        begin
            srstn   <= '0';
            for i in 1 to 10 loop -- 10 clock cycles reset at beginning of simulation
                wait until rising_edge(clk);
            end loop;
            srstn <= '1';
            wait until eosp and eosd and eosv and eosu;
            report "Non-regression tests passed.";
            finish;
        end process rst_req;

        -- requests from pss
        pss_req: process
            variable adds: integer_vector(0 to npe - 1);   -- posterior message addresses
            type port_used_type is array(0 to nrams - 1, 0 to 1) of boolean;
            variable port_used: port_used_type;
            variable ram_index: natural range 0 to nrams - 1;
            variable wnr: std_ulogic;
        begin
            pss2mss <= pss2mss_none; -- initialize all channels to non-access
            wait until rising_edge(clk) and srstn = '1'; -- wait until end of reset and end of css pass 1
            for i in 1 to n loop -- for n test vectors
                for j in 1 to int_rnd(0, 3) loop
                    wait until rising_edge(clk);
                end loop;
                port_used := (others => (others => false));
                wnr := std_ulogic_rnd;
                pss2mss.en  <= '1';
                pss2mss.wnr <= wnr;
                for j in 0 to npe - 1 loop
                    l1: loop
                        adds(j) := int_rnd(0, 2**14 - 1);
                        ram_index := adds(j) mod 8;
                        next l1 when port_used(ram_index, 0) and port_used(ram_index, 1);
                        for k in 0 to j - 1 loop
                            next l1 when adds(j) = adds(k);
                        end loop;
                        exit l1;
                    end loop l1;
                    if not port_used(ram_index, 0) then
                        port_used(ram_index, 0) := true;
                    else
                        port_used(ram_index, 1) := true;
                    end if;
                    pss2mss.channels(j) <= (add => std_ulogic_vector(to_unsigned(adds(j), 14)), data => std_ulogic_vector_rnd(8));
                end loop;
                wait until rising_edge(clk);
                pss2mss <= pss2mss_none;
            end loop;
            for i in 1 to 10 loop
                wait until rising_edge(clk);
            end loop;
            eosp <= true;
            wait;
        end process pss_req;

        -- requests from uc
        uc_req: process
            variable add: natural range 0 to 2**16 - 1;
        begin
            uc2mss <= uc2mss_none;
            wait until rising_edge(clk) and srstn = '1';
            for i in 1 to 10 loop
                wait until rising_edge(clk);
            end loop;
            for i in 1 to n loop
                for j in 1 to int_rnd(0, 3) loop
                    wait until rising_edge(clk);
                end loop;
                if int_rnd(0, 9) = 0 then -- any range access
                    add := int_rnd(0, 2**13 - 1);
                else -- in range access
                    add := int_rnd(0, nrams * 256 - 1);
                end if;
                add := add * 8;
                uc2mss <= (en => '1', rnw => std_ulogic_rnd, add => std_ulogic_vector(to_unsigned(add, 16)), be => std_ulogic_vector_rnd(8), wdata => std_ulogic_vector_rnd(64));
                loop
                    wait until rising_edge(clk);
                    exit when mss2uc.oor = '1';
                    uc2mss.be <= uc2mss.be and (not mss2uc.gnt);
                    exit when or (uc2mss.be and (not mss2uc.gnt)) = '0';
                end loop;
                uc2mss <= uc2mss_none;
            end loop;
            eosu <= true;
            wait;
        end process uc_req;

        -- requests from dma
        dma_req: process
            variable add: natural range 0 to 2**17 - 1;
        begin
            dma2mss <= dma2mss_none;
            wait until rising_edge(clk) and srstn = '1';
            for i in 1 to 10 loop
                wait until rising_edge(clk);
            end loop;
            for i in 1 to n loop
                for j in 1 to int_rnd(0, 3) loop
                    wait until rising_edge(clk);
                end loop;
                if int_rnd(0, 9) = 0 then -- any range access
                    add := int_rnd(0, 2**17 - 1);
                else -- in range access
                    add := int_rnd(0, nrams * 256 - 1);
                end if;
                dma2mss <= (en => '1', rnw => std_ulogic_rnd, add => std_ulogic_vector(to_unsigned(add, 29)), be => std_ulogic_vector_rnd(8), wdata => std_ulogic_vector_rnd(64));
                loop
                    wait until rising_edge(clk);
                    exit when mss2dma.oor = '1';
                    dma2mss.be <= dma2mss.be and (not mss2dma.gnt);
                    exit when or (dma2mss.be and (not mss2dma.gnt)) = '0';
                end loop;
                dma2mss <= dma2mss_none;
            end loop;
            eosd <= true;
            wait;
        end process dma_req;

        -- requests from vci
        vci_req: process
            variable add: natural range 0 to 2**17 - 1;
        begin
            vci2mss <= vci2mss_none;
            wait until rising_edge(clk) and srstn = '1';
            for i in 1 to 10 loop
                wait until rising_edge(clk);
            end loop;
            for i in 1 to n loop
                for j in 1 to int_rnd(0, 3) loop
                    wait until rising_edge(clk);
                end loop;
                if int_rnd(0, 9) = 0 then -- any range access
                    add := int_rnd(0, 2**17 - 1);
                else -- in range access
                    add := int_rnd(0, nrams * 256 - 1);
                end if;
                vci2mss <= (en => '1', rnw => std_ulogic_rnd, add => std_ulogic_vector(to_unsigned(add, 29)), be => std_ulogic_vector_rnd(8), wdata => std_ulogic_vector_rnd(64));
                loop
                    wait until rising_edge(clk);
                    exit when mss2vci.oor = '1';
                    vci2mss.be <= vci2mss.be and (not mss2vci.gnt);
                    exit when or (vci2mss.be and (not mss2vci.gnt)) = '0';
                end loop;
                vci2mss <= vci2mss_none;
            end loop;
            eosv <= true;
            wait;
        end process vci_req;

        gdbg: if debug generate
            process
                variable l: line;
            begin
                wait until rising_edge(clk) and srstn = '1';
                write(l, pss2mss);
                writeline(output, l);
                write(l, mss2pss);
                writeline(output, l);
                write(l, dma2mss);
                writeline(output, l);
                write(l, mss2dma);
                writeline(output, l);
                write(l, vci2mss);
                writeline(output, l);
                write(l, mss2vci);
                writeline(output, l);
                write(l, uc2mss);
                writeline(output, l);
                write(l, mss2uc);
                writeline(output, l);
                writeline(output, l);
            end process;
        end generate gdbg;

        checker: process(clk)
            subtype byte is std_ulogic_vector(7 downto 0);
            type ram_type is array(natural range 0 to nrams * 2048 - 1) of byte;
            variable data_ram: ram_type := (others => (others => 'U'));
            type mss2pss_pipe_type is array(natural range 0 to 1) of mss2pss_type;
            variable mss2pss_pipe: mss2pss_pipe_type;
            variable mss2uc_v: mss2uc_type;
            variable mss2uc_fifo: mss2uc_fifo_pkg.fifo;
            variable mss2dma_v: mss2dma_type;
            variable mss2dma_fifo: mss2dma_fifo_pkg.fifo;
            variable mss2vci_v: mss2vci_type;
            variable mss2vci_fifo: mss2vci_fifo_pkg.fifo;
            variable pss_add: natural range 0 to 2**14 - 1;
            variable uc_add: natural range 0 to 2**16 - 1;
            variable dma_add: natural range 0 to 2**17 - 1;
            variable vci_add: natural range 0 to 2**17 - 1;
            variable write_conflicts: integer_vector(0 to 7);
            variable a0, a1, a2: integer range -3 to 2**14 - 1;
            variable l: line;
            variable cc: natural;
            variable ram_index: natural range 0 to 7;
        begin
            if rising_edge(clk) then
                if srstn = '0' then
                    mss2pss_pipe := (others => (others => (others => '-')));
                    mss2uc_fifo.free;
                    mss2dma_fifo.free;
                    mss2vci_fifo.free;
                    cc           := 0;
                else
                    cc := cc + 1;
                    -- check pss request
                    assert is_01(pss2mss.en) report "Invalid PSS request" severity failure;
                    if pss2mss.en = '1' then
                        assert is_01(pss2mss.wnr) report "Invalid PSS request" severity failure;
                        for i in 0 to npe - 1 loop
                            assert is_01(pss2mss.channels(i).add) and is_01(pss2mss.channels(i).data) report "Invalid PSS request" severity failure;
                        end loop;
                    end if;

                    -- check uc requests, mss acknowledge, out of range and read-only error flags
                    uc_add := to_integer(u_unsigned(uc2mss.add));
                    assert is_01(uc2mss.en) report "Invalid uc request" severity failure;
                    if uc2mss.en = '1' then
                        assert is_01(uc2mss.rnw) and is_01(uc2mss.add) and is_01(uc2mss.be) and is_01(uc2mss.wdata) report "Invalid uc request" severity failure;
                        if uc_add mod 8 /= 0 or uc_add >= nrams * 2048 then -- out of range
                            assert and mss2uc.gnt = '1' report "Wrong uc request acknowledge" severity failure;
                            assert mss2uc.oor = '1' report "Undetected uc out-of-range access" severity failure;
                        else
                            assert mss2uc.oor = '0' report "Wrong uc out-of-range error" severity failure;
                        end if;
                    end if;
                    uc_add := uc_add / 8;

                    dma_add := to_integer(u_unsigned(dma2mss.add));
                    assert is_01(dma2mss.en) report "Invalid dma request" severity failure;
                    if dma2mss.en = '1' then
                        assert is_01(dma2mss.rnw) and is_01(dma2mss.add) and is_01(dma2mss.be) and is_01(dma2mss.wdata) report "Invalid dma request" severity failure;
                        if dma_add >= nrams * 256 then -- out of range
                            assert and mss2dma.gnt = '1' report "Wrong dma request acknowledge" severity failure;
                            assert mss2dma.oor = '1' report "Undetected dma out-of-range access" severity failure;
                        else
                            assert mss2dma.oor = '0' report "Wrong dma out-of-range error" severity failure;
                        end if;
                    end if;

                    vci_add := to_integer(u_unsigned(vci2mss.add));
                    assert is_01(vci2mss.en) report "Invalid vci request" severity failure;
                    if vci2mss.en = '1' then
                        assert is_01(vci2mss.rnw) and is_01(vci2mss.add) and is_01(vci2mss.be) and is_01(vci2mss.wdata) report "Invalid vci request" severity failure;
                        if vci_add >= nrams * 256 then -- out of range
                            assert and mss2vci.gnt = '1' report "Wrong vci request acknowledge" severity failure;
                            assert mss2vci.oor = '1' report "Undetected vci out-of-range access" severity failure;
                        else
                            assert mss2vci.oor = '0' report "Wrong vci out-of-range error" severity failure;
                        end if;
                    end if;

                    -- check responses to css and pss against expected ones
                    if not check(mss2pss, mss2pss_pipe(1)) then
                        write(l, string'("Mismatch in MSS to PSS response"));
                        writeline(output, l);
                        write(l, string'("  Expected: "));
                        write(l, mss2pss_pipe(1));
                        writeline(output, l);
                        write(l, string'("  Got:      "));
                        write(l, mss2pss);
                        writeline(output, l);
                        assert false severity failure;
                    end if;
                    assert is_01(mss2uc.en) report "Wrong uc response" severity failure;
                    if mss2uc.en = '1' then
                        mss2uc_v := mss2uc_fifo.pop;
                        if not check(mss2uc, mss2uc_v) then
                            write(l, string'("Mismatch in MSS to uc response"));
                            writeline(output, l);
                            write(l, string'("  Expected: "));
                            write(l, mss2uc_v);
                            writeline(output, l);
                            write(l, string'("  Got:      "));
                            write(l, mss2uc);
                            writeline(output, l);
                            assert false severity failure;
                        end if;
                    end if;
                    assert is_01(mss2dma.en) report "Wrong dma response" severity failure;
                    if mss2dma.en = '1' then
                        mss2dma_v := mss2dma_fifo.pop;
                        if not check(mss2dma, mss2dma_v) then
                            write(l, string'("Mismatch in MSS to dma response"));
                            writeline(output, l);
                            write(l, string'("  Expected: "));
                            write(l, mss2dma_v);
                            writeline(output, l);
                            write(l, string'("  Got:      "));
                            write(l, mss2dma);
                            writeline(output, l);
                            assert false severity failure;
                        end if;
                    end if;
                    assert is_01(mss2vci.en) report "Wrong vci response" severity failure;
                    if mss2vci.en = '1' then
                        mss2vci_v := mss2vci_fifo.pop;
                        if not check(mss2vci, mss2vci_v) then
                            write(l, string'("Mismatch in MSS to vci response"));
                            writeline(output, l);
                            write(l, string'("  Expected: "));
                            write(l, mss2vci_v);
                            writeline(output, l);
                            write(l, string'("  Got:      "));
                            write(l, mss2vci);
                            writeline(output, l);
                            assert false severity failure;
                        end if;
                    end if;

                    -- update pipelines of expected responses
                    mss2pss_pipe(1) := mss2pss_pipe(0);
                    mss2pss_pipe(0) := (others => mss2pss_channel_void);
                    if pss2mss.en = '1' and pss2mss.wnr = '0' then
                        for i in 0 to npe - 1 loop
                            pss_add := to_integer(u_unsigned(pss2mss.channels(i).add));
                            mss2pss_pipe(0)(i) := data_ram(pss_add);
                        end loop;
                    else
                        for i in 0 to npe - 1 loop
                            mss2pss_pipe(0)(i) := (others => '-');
                        end loop;
                    end if;
                    if uc2mss.en = '1' and uc2mss.rnw = '1' and mss2uc.oor = '0' and (or (uc2mss.be and mss2uc.gnt) = '1') then
                        mss2uc_v.gnt   := (others => '-');
                        mss2uc_v.oor   := '-';
                        mss2uc_v.en    := '1';
                        mss2uc_v.be    := uc2mss.be and mss2uc.gnt;
                        mss2uc_v.rdata := (others => '-');
                        for i in 0 to 7 loop
                            if (uc2mss.be(i) and mss2uc.gnt(i)) = '1'  then
                                mss2uc_v.rdata(8 * i + 7 downto 8 * i) := data_ram(uc_add * 8 + i);
                            end if;
                        end loop;
                        mss2uc_fifo.push(mss2uc_v);
                    end if;
                    if dma2mss.en = '1' and dma2mss.rnw = '1' and mss2dma.oor = '0' and (or (dma2mss.be and mss2dma.gnt) = '1') then
                        mss2dma_v.gnt   := (others => '-');
                        mss2dma_v.oor   := '-';
                        mss2dma_v.en    := '1';
                        mss2dma_v.be    := dma2mss.be and mss2dma.gnt;
                        mss2dma_v.rdata := (others => '-');
                        for i in 0 to 7 loop
                            if (dma2mss.be(i) and mss2dma.gnt(i)) = '1'  then
                                mss2dma_v.rdata(8 * i + 7 downto 8 * i) := data_ram(dma_add * 8 + i);
                            end if;
                        end loop;
                        mss2dma_fifo.push(mss2dma_v);
                    end if;
                    if vci2mss.en = '1' and vci2mss.rnw = '1' and mss2vci.oor = '0' and (or (vci2mss.be and mss2vci.gnt) = '1') then
                        mss2vci_v.gnt   := (others => '-');
                        mss2vci_v.oor   := '-';
                        mss2vci_v.en    := '1';
                        mss2vci_v.be    := vci2mss.be and mss2vci.gnt;
                        mss2vci_v.rdata := (others => '-');
                        for i in 0 to 7 loop
                            if (vci2mss.be(i) and mss2vci.gnt(i)) = '1'  then
                                mss2vci_v.rdata(8 * i + 7 downto 8 * i) := data_ram(vci_add * 8 + i);
                            end if;
                        end loop;
                        mss2vci_fifo.push(mss2vci_v);
                    end if;

                    -- update memory
                    -- write conflict detection
                    write_conflicts := (others => -1);
                    for i in 0 to 7 loop
                        a0 := -1;
                        a1 := -2;
                        a2 := -3;
                        if pss2mss.en = '1' and pss2mss.wnr = '1' then
                            for j in 0 to npe - 1 loop
                                ram_index := to_integer(u_unsigned(pss2mss.channels(j).add(2 downto 0)));
                                if ram_index = i then
                                    if a0 = -1 then
                                        a0 := to_integer(u_unsigned(pss2mss.channels(j).add(13 downto 3)));
                                    else
                                        a1 := to_integer(u_unsigned(pss2mss.channels(j).add(13 downto 3)));
                                    end if;
                                end if;
                            end loop;
                        end if;
                        if a0 = a1 then
                            if debug then
                                write(l, string'("pss-pss write conflict in ram"));
                                write(l, i);
                                write(l, string'("@"));
                                write(l, a1);
                                writeline(output, l);
                            end if;
                            write_conflicts(i) := a1;
                        end if;
                        if uc2mss.en = '1' and mss2uc.oor = '0' and uc2mss.rnw = '0' and uc2mss.be(i) = '1' and mss2uc.gnt(i) ='1' then
                            a2 := uc_add;
                            if a1 = a2 or a0 = a2 then
                                if debug then
                                    write(l, string'("pss-uc write conflict in ram"));
                                    write(l, i);
                                    write(l, string'("@"));
                                    write(l, a2);
                                    writeline(output, l);
                                end if;
                                write_conflicts(i) := a2;
                            end if;
                        end if;
                        if dma2mss.en = '1' and mss2dma.oor = '0' and dma2mss.rnw = '0' and dma2mss.be(i) = '1' and mss2dma.gnt(i) ='1' then
                            a2 := dma_add;
                            if a1 = a2 or a0 = a2 then
                                if debug then
                                    write(l, string'("pss-dma write conflict in ram"));
                                    write(l, i);
                                    write(l, string'("@"));
                                    write(l, a2);
                                    writeline(output, l);
                                end if;
                                write_conflicts(i) := a2;
                            end if;
                        end if;
                        if vci2mss.en = '1' and mss2vci.oor = '0' and vci2mss.rnw = '0' and vci2mss.be(i) = '1' and mss2vci.gnt(i) ='1' then
                            a2 := vci_add;
                            if a1 = a2 or a0 = a2 then
                                if debug then
                                    write(l, string'("pss-vci write conflict in ram"));
                                    write(l, i);
                                    write(l, string'("@"));
                                    write(l, a2);
                                    writeline(output, l);
                                end if;
                                write_conflicts(i) := a2;
                            end if;
                        end if;
                    end loop;
                    -- css write requests
                    if uc2mss.en = '1' and uc2mss.rnw = '0' and mss2uc.oor = '0' then
                        for i in 0 to 7 loop
                            if (uc2mss.be(i) and mss2uc.gnt(i)) = '1' then
                                data_ram(uc_add * 8 + i) := uc2mss.wdata(8 * i + 7 downto 8 * i);
                            end if;
                        end loop;
                    end if;
                    if dma2mss.en = '1' and dma2mss.rnw = '0' and mss2dma.oor = '0' then
                        for i in 0 to 7 loop
                            if (dma2mss.be(i) and mss2dma.gnt(i)) = '1' then
                                data_ram(dma_add * 8 + i) := dma2mss.wdata(8 * i + 7 downto 8 * i);
                            end if;
                        end loop;
                    end if;
                    if vci2mss.en = '1' and vci2mss.rnw = '0' and mss2vci.oor = '0' then
                        for i in 0 to 7 loop
                            if (vci2mss.be(i) and mss2vci.gnt(i)) = '1' then
                                data_ram(vci_add * 8 + i) := vci2mss.wdata(8 * i + 7 downto 8 * i);
                            end if;
                        end loop;
                    end if;
                    -- pss write requests
                    if pss2mss.en = '1' and pss2mss.wnr = '1' then
                        for i in 0 to npe - 1 loop
                            pss_add := to_integer(u_unsigned(pss2mss.channels(i).add));
                            data_ram(pss_add) := pss2mss.channels(i).data;
                        end loop;
                    end if;
                    -- write conflict
                    for i in 0 to 7 loop
                        if write_conflicts(i) >= 0 then
                            data_ram(8 * write_conflicts(i) + i) := (others => 'X');
                        end if;
                    end loop;
                end if;
            end if;
        end process checker;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
