--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

use std.textio.all;
use std.env.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;

library random_lib;
use random_lib.rnd.all;

use work.ldpc_pkg_local.all;
use work.ldpc_sim_pkg.all;

entity pe_sim is
    generic(
        debug:               natural range 0 to 2 := 0; -- debug level
        asap:                boolean := false; -- stop decoding ASAP
        compact_matrix_name: string;
        received_file_name:  string;
        max_iterations:      positive := 16;
        sigma:               real     := 0.5
    );
end entity pe_sim;

architecture sim of pe_sim is

    constant cm: compact_matrix := string_to_compact_matrix(compact_matrix_name);
    constant num_rows: natural := cm.z * cm.m;
    constant num_cols: natural := cm.z * cm.n;

    signal clk:   std_ulogic;
    signal srstn: std_ulogic;
    signal start: std_ulogic;
    signal fi:    std_ulogic;
    signal fgir:  std_ulogic;
    signal gi:    message_vector(0 to npe - 1);
    signal go:    message_vector(0 to npe - 1);
    signal aset_ram_we: std_ulogic;                        -- write enable
    signal aset_ram_wa: natural range 0 to max_num_rows_per_pe - 1;   -- write address
    signal aset_ram_ra: natural range 0 to max_num_rows_per_pe - 1;   -- read address
    signal alpha_ram_we:    std_ulogic;                        -- write enable
    signal alpha_ram_wa:    natural range 0 to max_degree - 1; -- write address
    signal alpha_ram_ra:    natural range 0 to max_degree - 1; -- read address
    signal n:               natural range 0 to max_degree - 1; -- current node index
    signal phase:           natural range 1 to 2;              -- current phase

    shared variable sd: scmsla_data;

begin

    assert cm.z mod npe = 0 report "Invald configuration (Z mod P /= 0)" severity failure;

    gdut: for i in 0 to npe - 1 generate
        dut: entity work.pe
            port map(
                clk          => clk,
                srstn        => srstn,
                fi           => fi,
                fgir         => fgir,
                phase        => phase,
                n            => n,
                aset_ram_we  => aset_ram_we,
                aset_ram_wa  => aset_ram_wa,
                aset_ram_ra  => aset_ram_ra,
                alpha_ram_we => alpha_ram_we,
                alpha_ram_wa => alpha_ram_wa,
                alpha_ram_ra => alpha_ram_ra,
                gi           => gi(i),
                go           => go(i)
            );
    end generate gdut;

    ctrl: entity work.pe_ctrl
    generic map(
        compact_matrix_name => compact_matrix_name,
        max_iterations      => max_iterations
    )
    port map(
        clk          => clk,
        srstn        => srstn,
        start        => start,
        fgir         => fgir,
        fi           => fi,
        aset_ram_we  => aset_ram_we,
        aset_ram_wa  => aset_ram_wa,
        aset_ram_ra  => aset_ram_ra,
        alpha_ram_we => alpha_ram_we,
        alpha_ram_wa => alpha_ram_wa,
        alpha_ram_ra => alpha_ram_ra,
        n            => n,
        phase        => phase
    );

    process
    begin
        clk <= '0';
        wait for 1 ns;
        clk <= '1';
        wait for 1 ns;
    end process;

    in_p: process
        variable frame: message_vector(0 to num_cols - 1);
        file fr: text;
        variable l: line;
        variable r: real;
        variable status: file_open_status;
        variable nframes: natural;
        variable row: natural range 0 to num_rows - 1;
        variable col: natural range 0 to num_cols - 1;
        variable cnt: natural range 0 to max_degree;
        variable syndromes: std_ulogic_vector(1 to max_iterations);
        variable changed: std_ulogic_vector(1 to max_iterations);
        variable ok: natural;
    begin
        srstn <= '0';
        start <= '0';
        gi    <= (others => 0);
        for i in 1 to 10 loop
            wait until rising_edge(clk);
        end loop;
        srstn <= '1';
        for i in 1 to 10 loop
            wait until rising_edge(clk);
        end loop;

        nframes := 0;
        ok := 0;
        file_open(status, fr, received_file_name, read_mode);
        assert status = open_ok report "could not open file " & received_file_name severity failure;
        while not endfile(fr) loop
            readline(fr, l);
            for i in 0 to num_cols - 1 loop
                read(l, r);
                r := -2.0 * r / (sigma * sigma);
                frame(i) := scms_saturate(integer(round(r)));
            end loop;
            sd.init(frame);
            start <= '1';
            wait until rising_edge(clk);
            start <= '0';
            for i in 1 to max_iterations loop
                sd.set_syndrome('0');
                sd.set_changed(false);
                for block_row in 0 to cm.m - 1 loop
                    for r in 0 to cm.z / npe - 1 loop
                        row := cm.z * block_row + r * npe;
                        cnt := 0;
                        for block_col in 0 to cm.n - 1 loop
                            next when cm.q(block_row, block_col) < 0;
                            for p in 0 to npe - 1 loop
                                col   := cm.z * block_col + (cm.q(block_row, block_col) + r * npe + p) mod cm.z;
                                gi(p) <= sd.get_gamma(col);
                            end loop;
                            cnt := cnt + 1;
                            wait until rising_edge(clk);
                        end loop;
                        for p in 0 to npe - 1 loop
                            sd.iter(cm, row + p);
                        end loop;
                        for j in 0 to cnt - 1 loop
                            wait until rising_edge(clk);
                        end loop;
                    end loop;
                end loop;
                exit when sd.get_syndrome = '0' and (not sd.get_changed) and asap;
                syndromes(i) := sd.get_syndrome;
                changed(i)   := '1' when sd.get_changed else '0';
            end loop;
            if debug = 2 then
                write(l, string'("Frame #"));
                write(l, nframes);
                write(l, string'(":"));
                writeline(output, l);
                write(l, string'("  syndromes = "));
                write(l, syndromes);
                writeline(output, l);
                write(l, string'("  changed   = "));
                write(l, changed);
                writeline(output, l);
            elsif debug = 1 then
                assert sd.get_syndrome = '0' report "decoded frame is not a code word" severity warning;
                assert not sd.get_changed report "decoded frame changed during last iteration" severity warning;
            end if;
            nframes := nframes + 1;
            if sd.get_syndrome = '0' and (not sd.get_changed) then
                ok := ok + 1;
            end if;
            wait until rising_edge(clk) and srstn = '1';
        end loop;
        for i in 1 to 10 loop
            wait until rising_edge(clk);
        end loop;
        report "successfuly decoded " & integer'image(ok) & "/" & integer'image(nframes) & " frames";
        report "non regression test passed";

        stop;
    end process in_p;

    out_p: process
        variable row: natural range 0 to num_rows - 1;
        variable col: natural range 0 to num_cols - 1;
    begin
        for block_row in 0 to cm.m - 1 loop
            for r in 0 to cm.z / npe - 1 loop
                row := cm.z * block_row + r * npe;
                for block_col in 0 to cm.n - 1 loop
                    next when cm.q(block_row, block_col) < 0;
                    wait until rising_edge(clk) and srstn = '1' and phase = 2;
                    for p in 0 to npe - 1 loop
                        col := cm.z * block_col + (cm.q(block_row, block_col) + r * npe + p) mod cm.z;
                        assert go(p) = sd.get_gamma(col)
                            report "Mismatch at row " & to_string(row) & ", column " & to_string(col) & ": expected " & to_string(sd.get_gamma(col)) & ", got " & to_string(go(p))
                            severity failure;
                    end loop;
                end loop;
            end loop;
        end loop;
    end process out_p;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
