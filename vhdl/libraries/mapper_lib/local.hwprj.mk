#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

gh-IGNORE	+= mapper_lib.rams mapper_lib.mapper mapper_lib.pss_mss mapper_lib.pss_sim mapper_lib.mss_sim
gh-IGNORE	+= mapper_lib.mapper mapper_lib.mapper_sim mapper_lib.top mapper_lib.mapper_no_rams

mapper_lib.address_generator_sim: \
	random_lib.rnd \
	global_lib.sim_utils \
	mapper_lib.address_generator

mapper_lib.pss: \
	global_lib.global \
	mapper_lib.bitfield_pkg \
	mapper_lib.mapper_pkg \
	mapper_lib.fifo \
	mapper_lib.address_generator

mapper_lib.fifo: \
	global_lib.utils


mapper_lib.pss_sim: \
	global_lib.global \
	global_lib.css_emulator \
	memories_lib.ram_pkg \
	mapper_lib.mapper_pkg \
	mapper_lib.pss \
	mapper_lib.fifo \
	mapper_lib.rams \
	mapper_lib.mss

mapper_lib.mapper_no_rams: \
	global_lib.global \
	css_lib.css \
	mapper_lib.bitfield_pkg \
	mapper_lib.mapper_pkg \
	mapper_lib.mss \
	mapper_lib.pss

mapper_lib.mapper: \
	global_lib.global \
	mapper_lib.mapper_pkg \
	mapper_lib.mapper_no_rams \
	mapper_lib.rams

mapper_lib.mapper_pkg: \
	global_lib.global \
	global_lib.utils \
	memories_lib.ram_pkg \
	mapper_lib.bitfield_pkg

mapper_lib.mapper_sim: \
	global_lib.global \
	global_lib.hst_emulator \
	mapper_lib.bitfield_pkg \
	mapper_lib.mapper

mapper_lib.mss: \
	global_lib.global \
	global_lib.utils \
	memories_lib.ram_pkg \
	mapper_lib.mapper_pkg

mapper_lib.mss_sim: \
	random_lib.rnd \
	global_lib.global \
	memories_lib.ram_pkg  \
	memories_lib.ram_sim_pkg  \
	mapper_lib.mapper_pkg \
	mapper_lib.rams \
	mapper_lib.mss

mapper_lib.rams: \
	memories_lib.ram_pkg \
	memories_lib.tdpram \
	memories_lib.spram \
	mapper_lib.mapper_pkg

mapper_lib.top: \
	global_lib.global \
	global_lib.utils \
	mapper_lib.mapper

# Default input and output pipeline depths of MSS.
MAPPER_LIB_N0	?= 1
MAPPER_LIB_N1	?= 1

# Simulations

mapper_lib_csrc		:= $(hwprj_db_path.mapper_lib)/C
mapper_lib_c_cmdfile	:= $(mapper_lib_csrc)/c_mapper.cmd
mapper_lib_cmdfile	:= $(mapper_lib_csrc)/mapper.cmd
MAPPER_LIB_CMDNUM	:= 100
MAPPER_LIB_CMDGEN	:= mkcmdfile
mapper_lib_cmdgen	:= $(mapper_lib_csrc)/$(MAPPER_LIB_CMDGEN)
MAPPER_LIB_CMDGENFLAGS	:= --tests=$(MAPPER_LIB_CMDNUM)

ms-sim.mapper_lib.%: MSSIMFLAGS += -do 'run -all; quit' -t ps

ms-sim.mapper_lib.mapper_sim: $(mapper_lib_cmdfile)
ms-sim.mapper_lib.mapper_sim: MSSIMFLAGS += -Gcmdfile="$(mapper_lib_cmdfile)" -Gdebug=true -Gverbose=false -Gn0=$(MAPPER_LIB_N0) -Gn1=$(MAPPER_LIB_N1)
ms-sim.mapper_lib.pss_sim: $(mapper_lib_c_cmdfile)
ms-sim.mapper_lib.pss_sim: MSSIMFLAGS += -Gcmdfile="$(mapper_lib_c_cmdfile)" -Gn0=$(MAPPER_LIB_N0) -Gn1=$(MAPPER_LIB_N1)
ms-sim.mapper_lib.mss_sim: MSSIMFLAGS += -Gn=100000 -Gdebug=false -Gn0=$(MAPPER_LIB_N0) -Gn1=$(MAPPER_LIB_N1)
ms-sim.mapper_lib: $(addprefix ms-sim.mapper_lib.,mapper_sim pss_sim mss_sim)
ms-sim: ms-sim.mapper_lib

.PHONY: $(mapper_lib_cmdgen)

$(mapper_lib_c_cmdfile): $(mapper_lib_cmdgen)
	$(mapper_lib_cmdgen) --core $(MAPPER_LIB_CMDGENFLAGS) $@

$(mapper_lib_cmdfile): $(mapper_lib_cmdgen)
	$(mapper_lib_cmdgen) $(MAPPER_LIB_CMDGENFLAGS) $@

$(mapper_lib_cmdgen):
	$(MAKE) -C $(dir $@) $(notdir $@)

# Synthesis

$(addprefix pr-syn.mapper_lib.,pss mapper mapper_no_rams top): $(hwprj_db_path.mapper_lib)/bitfield_pkg.vhd
$(addprefix pr-syn.mapper_lib.,mapper mapper_no_rams top): $(hwprj_db_path.css_lib)/bitfield_pkg.vhd
pr-syn.mapper_lib: $(addprefix pr-syn.mapper_lib.,pss mapper mapper_no_rams top)
pr-syn: pr-syn.mapper_lib

rc-syn.mapper_lib.mapper_no_rams: $(hwprj_db_path.mapper_lib)/bitfield_pkg.vhd
rc-syn.mapper_lib.mapper_no_rams: $(hwprj_db_path.css_lib)/bitfield_pkg.vhd
rc-syn.mapper_lib: rc-syn.mapper_lib.mapper_no_rams
rc-syn: rc-syn.mapper_lib

hw-clean: mapper_lib_clean

mapper_lib_clean:
	$(MAKE) -C $(mapper_lib_csrc) clean
	rm -rf $(mapper_lib_cmdfile) $(mapper_lib_c_cmdfile)

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
