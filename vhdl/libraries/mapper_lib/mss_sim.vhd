--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for Mapper's memory subsystem
--*
--*  The mapper's memory subsystem simulation environment. Each client (PSS, UC, DMA and VCI) randomly sends requests to the memory sub-system (but
--* avoiding write conflicts). Write requests are recorded and read requests are checked against this record.
--* The generic parameters are the following:
--* - debug: when true, prints requests as soon as they are taken into account by the memory subsystem. Defaults to false.
--* - xilinx: when true, selects the Xilinx simulation model for the block RAMs, else the custom (and faster) simulation model. Defaults to true.
--* - n: number of simulated requests. Defaults to 100000.

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-- use ieee.std_logic_textio.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.global.all;
use global_lib.sim_utils.all;

library memories_lib;
use memories_lib.ram_pkg.all;
use memories_lib.ram_sim_pkg.all;

use work.mapper_pkg.all;

entity mss_sim is
  generic(debug: boolean := false;
          n0: positive := 1;  --* Number of input pipeline registers in MSS, including input registers of RAMs
          n1: positive := 1;  --* Number of output pipeline registers in MSS, including output registers of RAMs
          n: positive := 100000);
end entity mss_sim;

architecture sim of mss_sim is

  -- IOs of the memory subsystem
  signal clk: std_ulogic;
  signal css2mss: css2mss_type;
  signal mss2css: mss2css_type;
  alias vci2mss is css2mss.vci2mss;
  alias mss2vci is mss2css.mss2vci;
  alias dma2mss is css2mss.dma2mss;
  alias mss2dma is mss2css.mss2dma;
  alias uc2mss  is css2mss.uc2mss;
  alias mss2uc  is mss2css.mss2uc;
  signal pss2mss: pss2mss_type;
  signal mss2pss: mss2pss_type;

-- Local copies of UC, DMA and VCI request signals. Used for write conflict
-- avoidance. These 3 clients issue totally random requests on these signals.
-- They are then filtered by the "conflict" process that copies them on the real
-- request interfaces (uc2mss, dma2mss, vci2mss). In case a write conflict is
-- detected (two clients request a write operation on overlapping memory
-- addresses), the client with the lowest priority (PSS > UC > DMA > VCI) has its
-- write request changed into a read request.
  signal dma2mssl: dma2mss_type;
  signal vci2mssl: dma2mss_type;

	-- end of simulation
  signal eos: boolean := false;

  signal mss2rams: mss2rams_type;
  signal rams2mss: rams2mss_type;

begin

  -- Instanciation of the memory subsystem.
  mss: entity work.mss(rtl)
    port map(clk      => clk,
             css2mss  => css2mss,
             mss2css  => mss2css,
             pss2mss   => pss2mss,
             mss2pss   => mss2pss,
             rams2mss => rams2mss,
             mss2rams => mss2rams);

  i_rams: entity work.rams(rtl)
    port map(clk       => clk,
             rams2mss  => rams2mss,
             mss2rams  => mss2rams);

  -- Clock generator.
  process
  begin
    clk <= '0';
    wait for 1 ns;
    clk <= '1';
    wait for 1 ns;
		if eos then
			wait;
		end if;
  end process;

	-- This process generates n requests from PSS to MSS through the DI channel
	-- (read requests only). It also prints the percentage of simulation
	-- completion. After the last request it stops requesting, waits until all the
	-- other clients (including PSS through the other channels) also stop
	-- requesting and then, it ends the simulation.
  pss_di_pr: process
    variable l: line;
  begin
    pss2mss.eni <= '0';
    pss2mss.addi <= (others => '0');
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    for i in 1 to 100 loop
      for j in 1 to n / 100 loop
        if boolean_rnd then
          pss2mss.eni <= '1';
          pss2mss.addi <= std_ulogic_vector_rnd(12);
        else
          pss2mss.eni <= '0';
        end if;
        wait until rising_edge(clk);
      end loop;
      write(l, integer'image(i) & "%");
      writeline(output, l);
    end loop;
    pss2mss.eni <= '0';
    pss2mss.addi <= (others => '0');
    wait until rising_edge(clk) and pss2mss.enl = '0' and pss2mss.eni = '0' and
      uc2mss.en = '0' and dma2mss.en = '0' and vci2mss.en = '0';
    eos <= true;
    report "End of simulation";
    wait;
  end process pss_di_pr;

  -- This process generates n requests from PSS to MSS through the DO channel
  -- (write requests only). After the last request it stops requesting and it
  -- waits forever.
  pss_do_pr: process
  begin
    pss2mss.eno <= '0';
    pss2mss.addo <= (others => '0');
    pss2mss.do <= (others => '0');
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    for i in 1 to n loop
      if boolean_rnd then
        pss2mss.eno <= '1';
        pss2mss.addo <= std_ulogic_vector_rnd(12);
        pss2mss.do <= std_ulogic_vector_rnd(32);
      else
        pss2mss.eno <= '0';
      end if;
      wait until rising_edge(clk);
    end loop;
    pss2mss.eno <= '0';
    pss2mss.addo <= (others => '0');
    pss2mss.do <= (others => '0');
    wait;
  end process pss_do_pr;

	-- This process generates n requests from PSS to MSS through the LUT channel
	-- (read requests only). After the last request it stops requesting and it
	-- waits forever.
  pss_lut_pr: process
  begin
    pss2mss.enl <= '0';
    pss2mss.addlr <= (others => '0');
    pss2mss.addli <= (others => '0');
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    for i in 1 to n loop
      if boolean_rnd then
        pss2mss.enl <= '1';
        pss2mss.addlr <= std_ulogic_vector_rnd(10);
        pss2mss.addli <= std_ulogic_vector_rnd(10);
      else
        pss2mss.enl <= '0';
      end if;
      wait until rising_edge(clk);
    end loop;
    pss2mss.enl <= '0';
    pss2mss.addlr <= (others => '0');
    pss2mss.addli <= (others => '0');
    wait;
  end process pss_lut_pr;

  -- This process generates n requests from UC to MSS (single byte, read and
  -- write requests). After the last request it stops requesting and waits
  -- forever.
  uc_pr: process
    variable add: word16 := (others => '0');
  begin
    uc2mss <= (en => '0', be => (others => '0'), rnw => '1', add => (others => '0'),
    wdata => (others => '0'));
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    for i in 1 to n loop
      if boolean_rnd then
        add(11 downto 0) := std_ulogic_vector_rnd(12);
        uc2mss <= (en => '1', be => std_ulogic_vector_rnd(8), rnw => std_ulogic_rnd, add => add, wdata => std_ulogic_vector_rnd(64));
      else
        uc2mss.en <= '0';
      end if;
      wait until rising_edge(clk);
    end loop;
    uc2mss <= (en => '0', rnw => '1', be => (others => '0'), add => (others => '0'), wdata => (others => '0'));
    wait;
  end process uc_pr;

  -- This process generates n requests from DMA to MSS (8 bytes, read and write requests, with individual byte enables). Before issuing the next request it
  -- waits for the previous one, if any, to be granted. The grant of the last request is waited for and then, it stops requesting and waits forever.
  dma_pr: process
    variable cnt: natural;
    variable add: word64_address_type := (others => '0');
  begin
    dma2mssl <= (en => '0', rnw => '1', be => (others => '0'), add => (others => '0'), wdata => (others => '0'));
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    cnt := 0;
    l1: loop
      l2: loop
        if dma2mss.en = '1' and ((mss2dma.gnt and dma2mss.be) = dma2mss.be) then -- end of current access
          cnt := cnt + 1;
          add(11 downto 0) := std_ulogic_vector_rnd(12);
          if boolean_rnd and cnt /= n then -- new access
            dma2mssl <= (en => '1', rnw => std_ulogic_rnd, be => std_ulogic_vector_rnd(8), add => add, wdata => std_ulogic_vector_rnd(64));
          else -- no access
            dma2mssl.en <= '0';
          end if;
          wait until rising_edge(clk);
          exit l1 when cnt = n;
          next l1;
        elsif dma2mss.en = '1' then -- ongoing access
          dma2mssl.be <= (not mss2dma.gnt) and dma2mss.be;
        elsif boolean_rnd then -- no ongoing access, launch a new one
          add(11 downto 0) := std_ulogic_vector_rnd(12);
          dma2mssl <= (en => '1', rnw => std_ulogic_rnd, be => std_ulogic_vector_rnd(8), add => add, wdata => std_ulogic_vector_rnd(64));
        else
          dma2mssl.en <= '0';
        end if;
        wait until rising_edge(clk);
      end loop l2;
    end loop l1;
    wait;
  end process dma_pr;

  -- This process generates n requests from VCI to MSS (8 bytes, read and write requests, with individual byte enables). Before issuing the next request it
  -- waits for the previous one, if any, to be granted. The grant of the last request is waited for and then, it stops requesting and waits forever.
  vci_pr: process
    variable cnt: natural;
    variable add: word64_address_type := (others => '0');
  begin
    vci2mssl <= (en => '0', rnw => '1', be => (others => '0'), add => (others => '0'), wdata => (others => '0'));
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    cnt := 0;
    l1: loop
      l2: loop
        if vci2mss.en = '1' and ((mss2vci.gnt and vci2mss.be) = vci2mss.be) then
          cnt := cnt + 1;
          add(11 downto 0) := std_ulogic_vector_rnd(12);
          if boolean_rnd and cnt /= n then
            vci2mssl <= (en => '1', rnw => std_ulogic_rnd, be => std_ulogic_vector_rnd(8), add => add, wdata => std_ulogic_vector_rnd(64));
          else
            vci2mssl.en <= '0';
          end if;
          wait until rising_edge(clk);
          exit l1 when cnt = n;
          next l1;
        elsif vci2mss.en = '1' then
          vci2mssl.be <= (not mss2vci.gnt) and vci2mss.be;
        elsif boolean_rnd then
          add(11 downto 0) := std_ulogic_vector_rnd(12);
          vci2mssl <= (en => '1', rnw => std_ulogic_rnd, be => std_ulogic_vector_rnd(8), add => add, wdata => std_ulogic_vector_rnd(64));
        else
          vci2mssl.en <= '0';
        end if;
        wait until rising_edge(clk);
      end loop l2;
    end loop l1;
    wait;
  end process vci_pr;

  -- This process filters the requests from DMA and VCI. In case there is a write conflict between PSS (DO channel only) and DMA or VCI, the request of the client
  -- with the lowest priority is changed into a read request.
  conflict: process(pss2mss, dma2mssl, vci2mssl)
    variable addpss, adduc, adddma, addvci: integer;
  begin
    if pss2mss.eno = '1' then
      addpss := 4 * to_integer(u_unsigned(pss2mss.addo)) + 16#4000#;
    else
      addpss := -8;
    end if;
    if dma2mssl.en = '1' and dma2mssl.rnw = '0' then
      adddma := to_integer(u_unsigned(dma2mssl.add)) * 8;
    else
      adddma := -16;
    end if;
    if vci2mssl.en = '1' and vci2mssl.rnw = '0' then
      addvci := to_integer(u_unsigned(vci2mssl.add)) * 8;
    else
      addvci := -24;
    end if;
    dma2mss <= dma2mssl;
    if adddma <= addpss and addpss <= adddma + 7 then
      dma2mss.rnw <= '1';
    end if;
    vci2mss <= vci2mssl;
    if addvci <= addpss and addpss <= addvci + 7 then
      vci2mss.rnw <= '1';
    end if;
  end process conflict;

  -- This process monitors the (granted) write requests and records them all. It thus always has an up-to-date model of the memory subsystem content. This model
  -- is used to check the result of the requests. An error is issued when a mismatch is detected between the beheavior of the memory subsystem and what the
  -- model predicts.
  process(clk)

		type tmem is array(natural range <>) of word8;
    variable ram: tmem(0 to 16#7fff#) := (others => (others => 'U'));
    variable add: natural range 0 to 16#7fff#;
    variable addlr, addli: natural range 0 to 16#7fff#;
    variable val: std_ulogic_vector(63 downto 0);

    type ref_16_type is record
      valid: boolean;
      val: std_ulogic_vector(15 downto 0);
    end record;
	  type ref_16_type_array is array(1 to n0 + n1) of ref_16_type;
    type ref_32_type is record
      valid: boolean;
      val: std_ulogic_vector(31 downto 0);
    end record;
	  type ref_32_type_array is array(1 to n0 + n1) of ref_32_type;
    type ref_64_type is record
      valid: boolean;
      val: std_ulogic_vector(63 downto 0);
    end record;
	  type ref_64_type_array is array(1 to n0 + n1) of ref_64_type;

    variable uc_ref, vci_ref, dma_ref: ref_64_type_array;
    variable pss_ref_di:  ref_16_type_array;
    variable pss_ref_lut: ref_32_type_array;

    function compare(e, a, b: std_ulogic_vector) return boolean is
      constant n: natural := e'length;
      variable ve: std_ulogic_vector(n - 1 downto 0) := e;
      variable va: std_ulogic_vector(n * 8 - 1 downto 0) := a;
      variable vb: std_ulogic_vector(n * 8 - 1 downto 0) := b;
    begin
      va := to_x01(va xor vb);
      for i in n - 1 downto 0 loop
        if ve(i) = '1' and va(8 * i + 7 downto 8 * i) /= "00000000" and va(8 * i + 7 downto 8 * i) /= "XXXXXXXX" then
          return false;
        end if;
      end loop;
      return true;
    end function compare;

    function compare(a, b: std_ulogic_vector) return boolean is
      constant n: natural := a'length / 8;
      constant e: std_ulogic_vector(n - 1 downto 0) := (others => '1');
    begin
      return compare(e, a, b);
    end function compare;

    variable l: line;

  begin

    if rising_edge(clk) then
      -- Check correctness of last read operations
      if vci_ref(n0 + n1).valid and (not compare(mss2vci.be, mss2vci.rdata, vci_ref(n0 + n1).val)) then
        write(l, string'("VCI error: read "));
        hwrite(l, mss2vci.rdata);
        write(l, string'(", should be "));
        hwrite(l, vci_ref(n0 + n1).val);
        writeline(output, l);
        assert false severity failure;
      end if;
      if dma_ref(n0 + n1).valid and (not compare(mss2dma.be, mss2dma.rdata, dma_ref(n0 + n1).val)) then
        write(l, string'("DMA error: read "));
        hwrite(l, mss2dma.rdata);
        write(l, string'(", should be "));
        hwrite(l, dma_ref(n0 + n1).val);
        writeline(output, l);
        assert false severity failure;
      end if;
      if uc_ref(n0 + n1).valid and (not compare(mss2uc.be, mss2uc.rdata, uc_ref(n0 + n1).val)) then
        write(l, string'("UC error: read "));
        hwrite(l, mss2uc.rdata);
        write(l, string'(", should be "));
        hwrite(l, uc_ref(n0 + n1).val);
        writeline(output, l);
        assert false severity failure;
      end if;
      if pss_ref_di(n0 + n1).valid and (not compare(mss2pss.di, pss_ref_di(n0 + n1).val)) then
        write(l, string'("PSS.DI error: read "));
        hwrite(l, mss2pss.di);
        write(l, string'(", should be "));
        hwrite(l, pss_ref_di(n0 + n1).val);
        writeline(output, l);
        assert false severity failure;
      end if;
      if pss_ref_lut(n0 + n1).valid and (not compare(mss2pss.lut, pss_ref_lut(n0 + n1).val)) then
        write(l, string'("PSS.LUT error: read "));
        hwrite(l, mss2pss.lut);
        write(l, string'(", should be "));
        hwrite(l, pss_ref_lut(n0 + n1).val);
        writeline(output, l);
        assert false severity failure;
      end if;
			-- Move pipeline one stage ahead
      vci_ref(2 to n0 + n1) := vci_ref(1 to n0 + n1 - 1);
	 	  dma_ref(2 to n0 + n1) := dma_ref(1 to n0 + n1 - 1);
      uc_ref(2 to n0 + n1) := uc_ref(1 to n0 + n1 - 1);
      pss_ref_di(2 to n0 + n1) := pss_ref_di(1 to n0 + n1 - 1);
      pss_ref_lut(2 to n0 + n1) := pss_ref_lut(1 to n0 + n1 - 1);
      -- Read operations
      if vci2mss.en = '1' and vci2mss.rnw = '1' and mss2vci.gnt /= X"00" then
        add := to_integer(u_unsigned(vci2mss.add));
        val := (others => 'X');
        for i in 7 downto 0 loop
          if mss2vci.gnt(i) = '1' then
            val(8 * i + 7 downto 8 * i) := ram(8 * add + 7 - i);
          end if;
        end loop;
        vci_ref(1) := (valid => true, val => val);
        if debug then
          print("VCI", vci2mss.rnw, vci2mss.be, 8 * add, vci2mss.wdata, vci_ref(1).val);
        end if;
      else
        vci_ref(1).valid := false;
      end if;
      if dma2mss.en = '1' and dma2mss.rnw = '1' and mss2dma.gnt /= X"00" then
        add := to_integer(u_unsigned(dma2mss.add));
        val := (others => 'X');
        for i in 7 downto 0 loop
          if mss2dma.gnt(i) = '1' then
            val(8 * i + 7 downto 8 * i) := ram(8 * add + 7 - i);
          end if;
        end loop;
        dma_ref(1) := (valid => true, val => val);
        if debug then
          print("DMA", dma2mss.rnw, dma2mss.be, 8 * add, dma2mss.wdata, dma_ref(1).val);
        end if;
      else
        dma_ref(1).valid := false;
      end if;
      if uc2mss.en = '1' and uc2mss.rnw = '1' and mss2uc.gnt /= X"00" then
        add := to_integer(u_unsigned(uc2mss.add(15 downto 3)));
        val := (others => 'X');
        for i in 7 downto 0 loop
          if mss2uc.gnt(i) = '1' then
            val(8 * i + 7 downto 8 * i) := ram(8 * add + 7 - i);
          end if;
        end loop;
        uc_ref(1) := (valid => true, val => val);
        if debug then
          print("UC", uc2mss.rnw, uc2mss.be, 8 * add, uc2mss.wdata, uc_ref(1).val);
        end if;
      else
        uc_ref(1).valid := false;
      end if;
      if pss2mss.eni = '1' then
        add := 2 * to_integer(u_unsigned(pss2mss.addi)) + 16#2000#;
        pss_ref_di(1) := (valid => true, val => ram(add) & ram(add + 1));
        if debug then
          print("PSS.DI", '1', "11", 16#2000# + add, "", pss_ref_di(1).val);
        end if;
      else
        pss_ref_di(1).valid := false;
      end if;
      if pss2mss.enl = '1' then
        addlr := 4 * to_integer(u_unsigned(pss2mss.addlr)) + 16#1000#;
        addli := 4 * to_integer(u_unsigned(pss2mss.addli)) + 16#1002#;
        pss_ref_lut(1) := (valid => true, val => ram(addlr) & ram(addlr + 1) & ram(addli) & ram(addli + 1));
        if debug then
          print("PSS.LUTR", '1', "11", addlr, "", pss_ref_lut(1).val(31 downto 16));
          print("PSS.LUTI", '1', "11", addli, "", pss_ref_lut(1).val(15 downto 0));
        end if;
      else
        pss_ref_lut(1).valid := false;
      end if;
      -- Write operations
      if vci2mss.en = '1' and vci2mss.rnw = '0' and mss2vci.gnt /= X"00" then
        add := to_integer(u_unsigned(vci2mss.add));
        for i in 7 downto 0 loop
          if vci2mss.be(i) = '1' and mss2vci.gnt(i) = '1' then
            ram(8 * add + 7 - i) := vci2mss.wdata(8 * i + 7 downto 8 * i);
          end if;
        end loop;
      end if;
      if dma2mss.en = '1' and dma2mss.rnw = '0' and mss2dma.gnt /= X"00" then
        add := to_integer(u_unsigned(dma2mss.add));
        for i in 7 downto 0 loop
          if dma2mss.be(i) = '1' and mss2dma.gnt(i) = '1' then
            ram(8 * add + 7 - i) := dma2mss.wdata(8 * i + 7 downto 8 * i);
          end if;
        end loop;
      end if;
      if uc2mss.en = '1' and uc2mss.rnw = '0' and mss2uc.gnt /= X"00" then
        add := to_integer(u_unsigned(uc2mss.add(15 downto 3)));
        for i in 7 downto 0 loop
          if uc2mss.be(i) = '1' and mss2uc.gnt(i) = '1' then
            ram(8 * add + 7 - i) := uc2mss.wdata(8 * i + 7 downto 8 * i);
          end if;
        end loop;
      end if;
      if pss2mss.eno = '1' then
        add := 4 * to_integer(u_unsigned(pss2mss.addo)) + 16#4000#;
        for i in 0 to 3 loop
          ram(add + i) := pss2mss.do(31 - 8 * i downto 24 - 8 * i);
        end loop;
      end if;
    end if;
  end process;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
