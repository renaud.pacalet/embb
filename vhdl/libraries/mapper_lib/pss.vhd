--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;

use work.bitfield.all;
use work.mapper_pkg.all;

entity pss is
  generic(mss_pipeline_depth: positive range 2 to integer'high); --* Number of pipeline stages in MSS, including input and output registers of RAMs
  port(clk:    in  std_ulogic;  -- master clock
       css2pss: in  css2pss_type; -- inputs from vciinterface (but parameter)
       param:  in  std_ulogic_vector(cmdsize * 8 - 1 downto 0); -- parameter input from vciinterface
       pss2css: out pss2css_type;  -- outputs to vciinterface
       pss2mss: out pss2mss_type;  -- outputs to memory subsystem
       mss2pss: in  mss2pss_type); -- inputs from memory subsystem
end entity pss;

architecture rtl of pss is

  signal cmd : bitfield_type;
  
  signal fifo_s : fifo_if;

  type stage1a_type is record
    en    : std_ulogic;
    addi  : u_unsigned(11 downto 0); 
  end record;

  signal stage1a: stage1a_type;

  signal stage1b: std_ulogic_vector(1 to mss_pipeline_depth);

  type stage1c_type is record
    en  : std_ulogic;
    di  : std_ulogic_vector(15 downto 0);
  end record;

  signal stage1c: stage1c_type;

  type stage2a_type is record
    en    : std_ulogic; 
    addlr : std_ulogic_vector(9 downto 0);
    addli : std_ulogic_vector(9 downto 0);
    cnt   : u_unsigned(11 downto 0);
  end record;

  signal stage2a: stage2a_type;

  type stage2b_type is record
    en  : std_ulogic;
    eoc   : std_ulogic;
  end record;

  type stage2b_v is array (1 to mss_pipeline_depth) of stage2b_type;

  signal stage2b : stage2b_v;

  type stage2c_type is record
    en  : std_ulogic;
    lut  : std_ulogic_vector(31 downto 0);
    eoc   : std_ulogic;
  end record;
  
  signal stage2c: stage2c_type;
  

  signal bps1 :u_unsigned(3 downto 0);

  -- resulting multiplied values in Q2.30 of (a0 + jb0) * (a1 + jb1) before additions
  -- real and imaginary parts in Q3.30 of result after additions
  -- r = a0a1 - b0b1
  -- c = a0b1 + b0a1

  type stage3_type is record
    en    : std_ulogic;
    eoc   : std_ulogic;
    a0a1  : u_signed(32 downto 0);
    a0b1  : u_signed(32 downto 0);
    b0a1  : u_signed(32 downto 0);
    b0b1  : u_signed(32 downto 0);
  end record;
  
  signal stage3: stage3_type;

  type stage4_type is record
    en : std_ulogic;
    eoc : std_ulogic;
    r  : u_signed(33 downto 0);
    i  : u_signed(33 downto 0);
  end record;

  signal stage4: stage4_type;

  type stage5_type is record
    en    : std_ulogic; 
    eoc   : std_ulogic;
    do    : std_ulogic_vector(31 downto 0);
    err   : std_ulogic;
    status: status_type;
  end record;
  
  signal stage5: stage5_type;

  type pre_processing_type is record
    hbps : natural range 0 to 8;
    msk  : std_ulogic_vector(9 downto 0);
  end record;
 

  signal init, start, busy, run, load : std_ulogic;

  signal a_s : u_unsigned(11 downto 0);
  signal addi: u_unsigned(11 downto 0);
  signal shl : u_unsigned(4 downto 0);
  signal cnta, cntb : u_unsigned(3 downto 0);
  signal fcnt : u_unsigned(5 downto 0);
  signal status : status_type;
  signal err : std_ulogic;
    
  alias srstn is css2pss.srstn;
  alias ce is css2pss.ce;

  constant fifo_depth : natural := 4 + mss_pipeline_depth;

begin

  pss2css.data <= (others => '0'); -- MAPPER does not support reading PSS internal registers from CSS
  pss2css.eirq <= (others => '0'); -- MAPPER has zero extended interrupts

  fifo: entity work.fifo 
  generic map(
    fifo_depth => fifo_depth,
    fifo_width => 16)
  port map(
    clk         => clk,
    srstn       => fifo_s.srstn,
    ce          => ce,
    enr         => fifo_s.enr,
    enw         => fifo_s.enw,
    dataout     => fifo_s.dataout,
    datain      => fifo_s.datain,
    empty       => fifo_s.empty,
    full        => fifo_s.full
  );

  addr_gen: entity work.address_generator(rtl) 
  port map(
    clk   => clk,
    srstn => srstn,
    ce    => ce,
    load  => load,
    run   => run,
    b     => u_unsigned(cmd.oba),
    n     => u_unsigned(cmd.n),
    m     => u_unsigned(cmd.m),
    s     => cmd.s,
    a     => a_s
  );

 
  pipe: process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then 
        stage1b     <= (others => '0');
        stage1c.en  <= '0';
        stage1c.di  <= (others => '0');
        -- stage 2
        stage2c.en  <= '0';
        stage2c.eoc <= '0';
        stage2c.lut <= (others => '0');
        -- stage 3
        stage3.eoc <= '0';
        -- stage 4
        stage4.eoc <= '0';
        -- stage 5
      elsif ce = '1' then
          -- stage 1
          for i in 1 to mss_pipeline_depth - 1 loop
            stage1b(i + 1) <= stage1b(i);
          end loop;
          stage1b(1)  <= stage1a.en;
          stage1c.en  <= stage1b(mss_pipeline_depth);
          stage1c.di  <= mss2pss.di;
          -- stage 2
          stage2c.en  <= stage2b(mss_pipeline_depth).en;
          stage2c.eoc <= stage2b(mss_pipeline_depth).eoc;
          stage2c.lut <= mss2pss.lut;
          -- stage 3
          stage3.eoc <= stage2c.eoc;
          -- stage 4
          stage4.eoc <= stage3.eoc;
      end if;
    end if;
  end process pipe;

  store_cmd: process(clk)
  begin
    if rising_edge(clk) then
      start   <= '0';
      if srstn = '1' and ce = '1' and css2pss.exec = '1' then 
  -- Store command
        cmd   <= bitfield_slice(param);
        start <= '1';
      end if;
    end if;
  end process store_cmd;
  
  fifo_s.enw <= stage1c.en;
  fifo_s.datain <= stage1c.di;
  fifo_s.srstn <= srstn and not start;

  read_l: process(clk)

    variable index    : integer range 0 to 31;
    variable pdata    : pre_processing_type;
    variable ra       : std_ulogic_vector(9 downto 0); -- 20 bits bit-address in X mem
    variable ia       : std_ulogic_vector(9 downto 0); -- 20 bits bit-address in X mem
    variable oor      : std_ulogic;

  begin

    if rising_edge(clk) then
      if srstn = '0' then
        addi          <= (others => '0');
        cnta          <= (others => '0');
        fcnt          <= (others => '0');
        bps1          <= (others => '0');
        stage1a.addi  <= (others => '0');
        stage1a.en    <= '0';
        stage2a.addlr <= (others => '0');
        stage2a.addli <= (others => '0');
        init          <= '0';
        stage2a.cnt   <= (others => '0');
        busy          <= '0';
        err           <= '0';
        status        <= (others => '0');
        for i in 1 to mss_pipeline_depth loop
          stage2b(i).en  <= '0';
          stage2b(i).eoc  <= '0';
        end loop;
      elsif ce = '1' then
        stage2a.en  <= not init and busy;
        status      <= (others => '0');
        err         <= '0';
        oor         := '0';
        stage2b(1).eoc <= '0';
        stage2b(1).en  <= stage2a.en;
        for i in 1 to mss_pipeline_depth - 1 loop
          stage2b(i + 1) <= stage2b(i);
        end loop;
        if start = '1' then
          stage1a.addi <= u_unsigned(cmd.iba(15 downto 4));
          addi         <= u_unsigned(cmd.iba(15 downto 4)) + 1;
          bps1         <= u_unsigned(cmd.bpsm1);
          cnta         <= u_unsigned(cmd.iba(3 downto 0));
          init         <= '1';
          busy         <= '1';
          stage2a.cnt  <= (others => '0');
          fcnt         <= (others => '0');
          stage2b(1).eoc  <= '0';
          bpsm1_pre_processing(cmd.bpsm1, cmd.sym, pdata.hbps, pdata.msk);
          if cmd.sym = '0' and cmd.bpsm1(3) = '1' and cmd.bpsm1(2 downto 1) /= "00" then
            busy <= '0';
            err  <= '1'; 
            status(0) <= '1';
          elsif cmd.sym = '1' and cmd.bpsm1(0) = '0' then 
            busy <= '0';
            err <= '1'; 
            status(1) <= '1';
          end if;
          stage1a.en <= '1';
        elsif busy = '1' then
          if stage1a.en = '1' then
            addi <= addi + 1;
            stage1a.addi <= addi;
          end if;
          if init = '1' then 
            if fifo_s.full = '1' then 
              stage1a.en <= '1';
              init <= '0';
            end if;
            if stage1a.en = '1' then 
              fcnt <= fcnt + 1;
              if fcnt = fifo_depth - 1 then
                stage1a.en <= '0';
                fcnt <= (others => '0');
              end if;
            end if;
          elsif init = '0' then 
            stage1a.en <= fifo_s.enr;
            cnta <= cntb;
            compute_lut_addresses(to_integer(shl), pdata.hbps, pdata.msk, fifo_s.dataout, cmd.lba, ra, ia, oor);
          end if;    
          if oor = '1' then 
            busy <= '0';
            err  <= '1'; 
            status(2) <= '1';
          end if;
          stage2a.addlr <= ra;
          stage2a.addli <= ia;
          if stage2a.en = '1' then 
            if stage2a.cnt = u_unsigned(cmd.lenm1) then
          -- This is the last write in DO
              busy <= '0';
              stage2b(1).eoc <= '1';
              stage2a.en <= '0';
              stage2a.cnt <= (others => '0');
            else 
              stage2a.cnt <= stage2a.cnt + 1;
            end if;
          end if;
        else
          stage1a.en  <= '0';
        end if;
      end if; 
    end if;
  end process read_l;

  cntb <= shl(3 downto 0);

  process(cnta, bps1, init, fifo_s.full)
  
    variable sum : u_unsigned(4 downto 0);

    begin
      
      fifo_s.enr <= '0';
      sum := (others => '0');
      if init = '0' then 
        sum := '0' & cnta + bps1 + 1;
        fifo_s.enr <= sum(4);
      else 
        fifo_s.enr <= fifo_s.full;
      end if;
      shl <= sum;

  end process;
  
  mul: process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then -- reset
        load        <= '0';
        stage3.en   <= '0'; 
        stage4.en   <= '0';
        stage5.en   <= '0';
        stage5.eoc  <= '0';
        stage5.err  <= '0';
        stage5.status <= (others => '0');
        stage3.a0a1 <= (others => '0');
        stage3.a0b1 <= (others => '0');
        stage3.b0a1 <= (others => '0');
        stage3.b0b1 <= (others => '0');
        stage5.eoc  <= '0';
      elsif ce = '1' then
        stage4.en <= '0';
        stage3.en <= '0';
        load      <= '0';
        stage5.en <= '0';
        stage5.eoc  <= '0';
        stage5.en   <= '0';
        stage5.eoc  <= '0';
        stage5.err  <= '0';
        stage5.status <= (others => '0');
        if start = '1' then
          -- load input parameter to address generator
          load  <= '1';
        end if;
        if stage2c.en = '1' then
          -- a output data from LUT is available
          if cmd.men = '1' then
            -- multiplication needed
            stage3.a0a1 <= resize(u_signed(cmd.mult(31 downto 16)), 17) * u_signed(stage2c.lut(31 downto 16)); 
            stage3.a0b1 <= resize(u_signed(cmd.mult(31 downto 16)), 17) * u_signed(stage2c.lut(15 downto 0)); 
            stage3.b0a1 <= resize(u_signed(cmd.mult(15 downto 0)), 17) * u_signed(stage2c.lut(31 downto 16)); 
            stage3.b0b1 <= resize(u_signed(cmd.mult(15 downto 0)), 17) * u_signed(stage2c.lut(15 downto 0)); 
            stage3.en   <= '1'; 
          else
            stage5.do   <= stage2c.lut;
            stage5.eoc  <= stage2c.eoc; 
            stage5.en   <= '1';
          end if;
        end if;
        if stage3.en = '1' then
          stage4.r  <= (resize(stage3.a0a1, 34)) - stage3.b0b1; 
          -- real part
          stage4.i  <= (resize(stage3.a0b1, 34)) + stage3.b0a1; 
          -- imaginary part
          stage4.en <= '1';
        end if;
        if stage4.en = '1' then
          stage5.eoc <= stage4.eoc; 
          stage5.en <= '1';
            -- real part
          stage5.do(31 downto 16) <= std_ulogic_vector(sat(stage4.r(33 downto 15), 16)); 
            -- imaginary part
          stage5.do(15 downto 0) <= std_ulogic_vector(sat(stage4.i(33 downto 15), 16)); 
        end if;
        if err = '1' then
          stage5.eoc <= '1';
        end if;
        stage5.err <= err; 
        stage5.status <= status;
      end if;
    end if;
  end process mul;

  
  pss2mss.addi <= std_ulogic_vector(stage1a.addi);
  pss2mss.eni <= stage1a.en;
  pss2mss.addlr <= std_ulogic_vector(stage2a.addlr);
  pss2mss.addli <= std_ulogic_vector(stage2a.addli);
  pss2mss.enl <= stage2a.en;
  pss2mss.do <= stage5.do;
  pss2mss.eno <= stage5.en;
  pss2mss.addo <= std_ulogic_vector(a_s);
  pss2css.eoc <= stage5.eoc;
  pss2css.err <= stage5.err;
  pss2css.status <= stage5.status;
  run <= stage5.en;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
