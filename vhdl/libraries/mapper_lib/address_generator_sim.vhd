--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for output address generator

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.numeric_std.all;

-- Utility package
package address_generator_sim_pkg is

  -- Integer-based parameters
  type nparam is record
    b: natural range 0 to 2**12 - 1;
    s: natural range 0 to 1;
    n: natural range 0 to 127;
    m: natural range 0 to 255;
  end record;

  -- Vector-based parameters
  type sparam is record
    b: u_unsigned(11 downto 0);
    s: std_ulogic;
    n: u_unsigned(6 downto 0);
    m: u_unsigned(7 downto 0);
  end record;

  -- Conversion functions between nparam and sparam
  function n2s(n: nparam) return sparam;
  function s2n(s: sparam) return nparam;

  impure function nparam_rnd return nparam; -- Random generator of parameters
  -- The ad function computes a byte address according to the address generation algorithm. i is the position of the requested byte address in the sequence,
  -- starting at i=0.
  function ad(p: nparam; i: natural) return natural;
  -- Write "p / q" in l.
  procedure write(l: inout line; p: natural; q: natural);
  -- Write parameters p in l.
  procedure write(l: inout line; p: in nparam);

end package address_generator_sim_pkg;

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.sim_utils.all;

use work.address_generator_sim_pkg.all;

entity address_generator_sim is
  generic(nvec: natural := 1000; -- Number of random tests
          debug: natural := integer'high); -- Start verbose output at test #debug
end entity address_generator_sim;

architecture sim of address_generator_sim is
  
  signal clk, load, run, srstn, ce, dso :std_ulogic;
  signal eos: boolean;
  signal sp:  sparam;
  signal a:   u_unsigned(11 downto 0);
 
begin

  ag: entity work.address_generator(rtl)
    port map(clk   => clk,
             srstn => srstn,
             ce    => ce,
             load  => load,
             run   => run,
             b     => sp.b,
             s     => sp.s,
             n     => sp.n,
             m     => sp.m,
             a     => a);

  clock: process
  begin
    clk <= '0';
    wait for 1 ns;
    clk <= '1';
    wait for 1 ns;
    if eos then
      wait;
    end if;
  end process clock;

  -- Randomize the run signal
  run_pr: process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        run <= '0';
      else
        run <= std_ulogic_rnd;
      end if;
    end if;
  end process run_pr;

  stim: process
    variable np: nparam;
    variable cnt: natural;
    variable l: line;
  begin
    srstn <= '0';
    ce <= '0';
    load  <= '0';
    np := (0, 0, 0, 0);
    sp <= n2s(np);
    cnt := 0;
    for i in 1 to 10 loop -- 10 cycles reset
      wait until rising_edge(clk);
    end loop;
    srstn <= '1';
    ce <= '1';
    for i in 1 to 10 loop -- 10 idle cycles
      wait until rising_edge(clk);
    end loop;
    -- Run directed tests (b=0, n,m<=7)
    write(l, string'("Directed tests:"));
    writeline(output, l);
    for s in 0 to 1 loop
      for n in 0 to 7 loop
        for m in 0 to 7 loop
          cnt := cnt + 1;
          if cnt mod ((2 * 8 * 8) / 100) = 0 then
            progress(cnt, 2 * 8 * 8);
          end if;
          for j in 1 to int_rnd(0, 10) loop -- 0 to 10 idle cycles
            wait until rising_edge(clk);
          end loop;
          load <= '1';
          np := (0, s, n, m);
          sp <= n2s(np);
          wait until rising_edge(clk);
          load  <= '0';
          for i in 1 to 100 loop -- 100 cycles
            wait until rising_edge(clk);
          end loop;
        end loop;
      end loop;
    end loop;
    -- Run random tests
    cnt := 0;
    write(l, string'("Random tests:"));
    writeline(output, l);
    for i in 1 to nvec loop -- nvec random tests
      cnt := cnt + 1;
      if cnt mod (nvec / 100) = 0 then
        progress(cnt, nvec);
      end if;
      for j in 1 to int_rnd(0, 10) loop -- 0 to 10 idle cycles
        wait until rising_edge(clk);
      end loop;
      load <= '1';
      np := nparam_rnd;
      sp <= n2s(np);
      wait until rising_edge(clk);
      load  <= '0';
      for j in 1 to 10000 loop
        wait until rising_edge(clk);
      end loop;
    end loop;
    eos <= true; -- End of simulation
    wait;
  end process stim;

  -- Verifier
  process(clk)
    variable l: line;
    variable np: nparam;
    variable idx, i: natural;
    variable u, v: natural;
  begin
    if rising_edge(clk) then
      if srstn = '0' then -- Reset
        np := (b => 0, s => 0, n => 0, m => 0);
        idx := 0;
        i := 0;
        u := 0;
      elsif ce = '1' then
        if load = '1' then -- Load parameters
          i := i + 1;
          np := s2n(sp);
          idx := 0; -- Initialize indices counter
          if i >= debug then -- If debug print parameters
            write(l, np);
            writeline(output, l);
          end if;
          dso <= '1';
        else
          dso <= run;
          if dso = '1' then -- If valid output addresses
            u := to_integer(a);
            v := ad(np, idx); -- Compute reference first address
            if u /= v then -- If mismatch
              write(l, np); -- Print error information
              writeline(output, l);
              write(l, idx, u);
              write(l, string'(" should be: "));
              write(l, idx, v);
              assert false -- Crash simulation
              report l.all
              severity failure;
            end if;
            if i >= debug then -- Enter verbose debug section
              write(l, idx, u); -- Print indices and output addresses
              writeline(output, l);
            end if;
            idx := idx + 1; -- Increment indices counter
          end if;
        end if;
      end if;
    end if;
  end process;

end architecture sim;

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;

library random_lib;
use random_lib.rnd.all;

-- Utility package body
package body address_generator_sim_pkg is

  function n2s(n: nparam) return sparam is
    variable res: sparam;
  begin
    res.b := to_unsigned(n.b, 12);
    if n.s = 0 then res.s := '0'; else res.s := '1'; end if;
    res.n := to_unsigned(n.n, 7);
    res.m := to_unsigned(n.m, 8);
    return res;
  end function n2s;

  function s2n(s: sparam) return nparam is
    variable res: nparam;
  begin
    res.b := to_integer(s.b);
    if s.s = '0' then res.s := 0; else res.s := 1; end if;
    res.n := to_integer(s.n);
    res.m := to_integer(s.m);
    return res;
  end function s2n;

  function ad(p: nparam; i: natural) return natural is
    variable u: integer;
  begin
    u := i * p.n;
    if p.m /= 0 then
      u := u + i / p.m;
    end if;
    if p.s = 1 then
      u := -u;
    end if;
    u := (p.b + u) mod 2**11;
    return u;
  end function ad;

  procedure write(l: inout line; p: natural; q: natural) is
  begin
    write(l, p);
    write(l, string'("/"));
    write(l, q);
  end procedure write;

  procedure write(l: inout line; p: in nparam) is
  begin
    write(l, string'(", b="));
    write(l, p.b);
    write(l, string'(", s="));
    write(l, p.s);
    write(l, string'(", n="));
    write(l, p.n);
    write(l, string'(", m="));
    write(l, p.m);
  end procedure write;

  impure function nparam_rnd return nparam is
    variable p: nparam;
  begin
    p.b := int_rnd(0, 2**11-1);
    p.s := int_rnd(0, 1);
    p.n := int_rnd(0, 127);
    p.m := int_rnd(0, 254);
    return p;
  end function nparam_rnd;

end package body address_generator_sim_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
