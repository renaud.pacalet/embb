--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Mapper's error codes.

library ieee;
use ieee.std_logic_1164.all;

package errors is

  --------------
  -- error codes
  --------------
  -- total number of error codes
  constant mapper_num_error_codes: natural := 3;
  -- the combination of the start index in the input buffer, the bit-width of
	-- symbols and the total number of symbols in the input frame induce an out of
	-- range read access in the input buffer
  constant mapper_dioutofrange: natural := 0;
	-- the combination of the start index in the output buffer and the total
	-- number of symbols induce an out of range write access in the output buffer
  constant mapper_dooutofrange: natural := 1;
	-- the combination of the start index in the look-up table buffer and the
	-- total number of symbols induce an out of range read access in the look-up
	-- table buffer
  constant mapper_lutoutofrange: natural := 2;

-- pragma translate_off
  -----------------
  -- error messages
  -----------------
  constant mapper_dioutofrange_message: string := "the combination of the start index in the input buffer, the bit-width of symbols and the total number of symbols in the input frame induce an out of range read access in the input buffer"; 
  constant mapper_dooutofrange_message: string := "the combination of the start index in the output buffer and the total number of symbols induce an out of range write access in the output buffer"; 
  constant mapper_lutoutofrange_message: string := "the combination of the start index in the look-up table buffer and the total number of symbols induce an out of range read access in the look-up table buffer"; 
  constant mapper_unknown_message: string := "unknown error case"; 
-- pragma translate_on

end package errors;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
