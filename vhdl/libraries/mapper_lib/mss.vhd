--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Memory subsystem
--*
--*  The mapper's memory subsystem comprises the following ram blocks:
--* - UCR: one 1kx32 dual dual port RAM. The only memory area accessed by UC. PSS never accesses UCR. UC, DMA and VCI accesses are 64-bits wide (but the byte
--*   enables can be used to mask some bytes) and use the two ports simultaneously: port A for even addresses and port B for odd addresses. The accesses are
--*   exclusive: DMA can be prevented by UC and VCI by UC or DMA. In order to simplify the logic there is no optimization: if UC accesses but its byte enebales
--*   indicate that one port only is needed, the other port is blocked anyway and not used to partially perform another access by DMA or VCI.
--* - DI: four 1kx16 dual port RAMs. PSS has read-only access and reads in one RAM only at a time with port A. DMA and VCI read/write with port B on the
--*   four at a time. DMA is never prevented. VCI can be prevented by DMA. No optimization between DMA and VCI.
--* - LUT: two 1kx16 dual port RAMs. PSS has read-only access and reads in both RAMs at a time with port A. DMA and VCI read/write in both RAMs at a time
--*   with both ports: port A for even addresses and port B for odd addresses. DMA can be prevented by PSS. VCI can be prevented by PSS or DMA. The ports use is
--*   optimized: the two ports can be used to fully or partially perform two accesses from two different clients.
--* - DO: four 1kx32 dual port RAMs. PSS has write-only access and writes in one RAM only at a time with port A. DMA and VCI read/write with port B on two
--*   RAMs at a time. DMA is never prevented. VCI can be prevented by DMA. There is no optimization between DMA and VCI.
--*
--* The address mapping on UC point of view is 0x0000-0x1fff: UCR
--*
--* The address mapping on DMA and VCI points of view is:
--* - 0x00000-0x00fff: UCR   (4 kB) - Double-word addresses 0x0000 to 0x01ff
--* - 0x01000-0x01fff: LUT   (4 kB) - Double-word addresses 0x0200 to 0x03ff
--* - 0x02000-0x03fff: DI    (8 kB) - Double-word addresses 0x0400 to 0x07ff
--* - 0x04000-0x07fff: DO   (16 kB) - Double-word addresses 0x0800 to 0x0fff

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.utils.all;
use global_lib.global.all;

library memories_lib;
use memories_lib.ram_pkg.all;

use work.mapper_pkg.all;

entity mss is
  generic(n0: positive := 1;  --* Number of input pipeline registers in MSS, including input registers of RAMs
          n1: positive := 1); --* Number of output pipeline registers in MSS, including output registers of RAMs
  port(clk:      in  std_ulogic;
       css2mss:  in  css2mss_type;
       mss2css:  out mss2css_type;
       pss2mss:   in  pss2mss_type;
       mss2pss:   out mss2pss_type;
       mss2rams: out mss2rams_type;
       rams2mss: in  rams2mss_type);
end entity mss;

architecture rtl of mss is
  
  alias vci2mss is css2mss.vci2mss;
  alias mss2vci is mss2css.mss2vci;
  alias dma2mss is css2mss.dma2mss;
  alias mss2dma is mss2css.mss2dma;
  alias uc2mss  is css2mss.uc2mss;
  alias mss2uc  is mss2css.mss2uc;

  type block_rams is (di, lut, do0, do1, u);

  type dma_vci_mux_type is record
    mux: block_rams;
    be:  word64_be_type;
  end record;
  type mux_type is record
   pss:  natural range 0 to 3;
   uc:  word64_be_type;
   dma: dma_vci_mux_type;
   vci: dma_vci_mux_type;
  end record;
  type mux_pipe_type is array(1 to n0 + n1) of mux_type;

  signal mux_d, mux : mux_type;
  signal mux_pipe: mux_pipe_type;

  signal mss2rams_pipe: mss2rams_vector(1 to n0);
  signal rams2mss_pipe: rams2mss_vector(1 to n1);

  signal u_in: ucr_in_type;
  signal di_in: di_in_type;
  signal do_in: do_in_type;
  signal lut_in: lut_in_type;

  begin

  --* The arbiter combinatorial process computes the different requests each client sends to each block RAM. It then enables only one client per port accorwdatag
  --* to the fixed priority: PSS first, then UC, DMA and VCI. The arbiter also prepares the commands of the multiplexors that select block RAMs outputs to send to
  --* the clients.
  arbiter: process(css2mss, pss2mss)

    -- keeps track of what port of what block RAM is already in use by a client
    subtype is_u_used_type is boolean; -- for UC-DMA-VCI arbitration
    subtype is_di_used_type is boolean; -- for DMA-VCI arbitration
    type is_lut_used_type is array (0 to 1) of boolean; -- for PSS-DMA-VCI arbitration
    subtype is_do_used_type is boolean; -- for DMA-VCI arbitration

    variable is_u_used: is_u_used_type;
    variable is_lut_used: is_lut_used_type;
    variable is_di_used: is_di_used_type;
    variable is_do_used: is_do_used_type;

    variable u_in_v: ucr_in_type;
    variable di_in_v: di_in_type;
    variable do_in_v: do_in_type;
    variable lut_in_v: lut_in_type;
    variable idi: natural range 0 to 3;
    variable idov: std_ulogic_vector(1 downto 0);
    variable ido: natural range 0 to 3;

  begin

    -------------------------------------------------------
    ------------------ INITIALIZATION ---------------------
    -------------------------------------------------------

    is_u_used := false;
    is_di_used := false;
    for i in 0 to 1 loop
      u_in_v(i) := in_port_1kx32_default;
      is_lut_used(i) := false;
      for j in 0 to 1 loop
        lut_in_v(i, j) := in_port_1kx16_default;
      end loop;
      for j in 0 to 3 loop
        di_in_v(i, j) := in_port_1kx16_default;
        do_in_v(i, j) := in_port_1kx32_default;
      end loop;
    end loop;
    is_do_used := false;

    mss2uc.gnt <= (others => '1'); -- in this DSP unit UC is always granted
    mss2uc.oor <= '0'; -- by default we expect UC not to access out of range memory locations

    mss2dma.gnt <= (others => '0'); -- by default, don't grant DMA's request
    mss2dma.oor <= '0'; -- by default we expect DMA not to access out of range memory locations

    mss2vci.gnt <= (others => '0'); -- by default, don't grant VCI's request
    mss2vci.oor <= '0'; -- by default we expect VCI not to access out of range memory locations

    mux_d.pss  <= 0;
    mux_d.uc  <= (others => '0');
    mux_d.dma <= (mux => di, be => (others => '0'));
    mux_d.vci <= (mux => di, be => (others => '0'));

    idi := to_integer(u_unsigned(pss2mss.addi(1 downto 0)));
    idov := pss2mss.addo(11) & pss2mss.addo(0);
    ido := to_integer(u_unsigned(idov));

    -------------------------------------------------------
    ------------------ PSS REQUESTS ------------------------
    -------------------------------------------------------
    for i in 0 to 3 loop
      di_in_v(0, i).we := (others => '0');
      di_in_v(0, i).add := pss2mss.addi(11 downto 2);
      if idi = i and pss2mss.eni = '1' then
        di_in_v(0, i).en := '1';
        mux_d.pss <= i;
      end if;
    end loop;
    for i in 0 to 1 loop
      lut_in_v(0, i).we := "00";
      if pss2mss.enl = '1' then
        lut_in_v(0, i).en := '1';
        is_lut_used(0)  := true;
      end if;
    end loop;
    lut_in_v(0, 0).add := pss2mss.addlr;
    lut_in_v(0, 1).add := pss2mss.addli;
    for i in 0 to 3 loop
      do_in_v(0, i).we  := (others => '1');
      do_in_v(0, i).add := pss2mss.addo(10 downto 1);
      do_in_v(0, i).wdata := pss2mss.do;
      if ido = i and pss2mss.eno = '1' then
        do_in_v(0, i).en  := '1';
      end if;
    end loop;

    -------------------------------------------------------
    ------------------ UC REQUESTS ------------------------
    -------------------------------------------------------
    -- UC requests are always served.

    mss2uc.gnt <= (others => '1'); -- grant the access
    if uc2mss.en = '1' then
      -- Port A
      u_in_v(0).en    := '1';
      u_in_v(0).we    := band((not uc2mss.rnw), uc2mss.be(7 downto 4));
      u_in_v(0).add   := uc2mss.add(11 downto 3) & '0';
      u_in_v(0).wdata := uc2mss.wdata(63 downto 32);
      -- Port B
      u_in_v(1).en    := '1';
      u_in_v(1).we    := band((not uc2mss.rnw), uc2mss.be(3 downto 0));
      u_in_v(1).add   := uc2mss.add(11 downto 3) & '1';
      u_in_v(1).wdata := uc2mss.wdata(31 downto 0);
      is_u_used       := true;
      if uc2mss.rnw = '1' then
        mux_d.uc <= uc2mss.be;
      end if;
   
      -- If UC access is out of range stop the simulation and set the "oor" field
      -- of mss2uc.
      if or_reduce(uc2mss.add(15 downto 12)) = '1' then
-- pragma translate_off
        assert false
        report "UC: out of range memory access (" & integer'image(to_integer(u_unsigned(uc2mss.add))) & " >= 0x1000"
        severity error;
-- pragma translate_on
        mss2uc.oor <= '1';
      end if;
    end if;
     
    -------------------------------------------------------
    ------------------ DMA REQUESTS -----------------------
    -------------------------------------------------------

    if dma2mss.en = '1' then
      if dma2mss.add(11) = '1' then --DO (0x800 - 0x0fff)
        is_do_used := true;
        mss2dma.gnt <= dma2mss.be;
        if dma2mss.rnw = '1' then
          mux_d.dma.be <= dma2mss.be;
        end if;
        for i in 0 to 1 loop
          if to_i01(dma2mss.add(10)) = i then
            for j in 0 to 1 loop
              do_in_v(1, 2 * i + j).en := '1';
              do_in_v(1, 2 * i + j).we := (others => not dma2mss.rnw);
              do_in_v(1, 2 * i + j).we   := do_in_v(1, 2 * i + j).we and dma2mss.be(7 - 4 * j downto 4 - 4 * j);
              do_in_v(1, 2 * i + j).add := dma2mss.add(9 downto 0);
              do_in_v(1, 2 * i + j).wdata  := dma2mss.wdata(63 - 32 * j downto 32 - 32 * j);
            end loop;
            mux_d.dma.mux <= do0;
            if i = 1 then 
              mux_d.dma.mux <= do1;
            end if;
          end if;  
        end loop;
      elsif dma2mss.add(10) = '1' then  -- DI (0x400 - 0x7ff)
        mss2dma.gnt <= dma2mss.be;
        is_di_used := true;
        if dma2mss.rnw = '1' then
          mux_d.dma.be <= dma2mss.be;
        end if;
        for i in 0 to 3 loop
          di_in_v(1, i).en  := '1';
          di_in_v(1, i).we := (others => not dma2mss.rnw);
          di_in_v(1, i).we   := di_in_v(1, i).we and dma2mss.be(7 - 2 * i downto 6 - 2 * i);
          di_in_v(1, i).add := dma2mss.add(9 downto 0);
          di_in_v(1, i).wdata  := dma2mss.wdata(63 - 16 * i downto 48 - 16 * i);
        end loop;
        mux_d.dma.mux <= di;
      elsif dma2mss.add(9) = '1' then --LUT (0x200 - 0x3ff)
        for i in 0 to 1 loop -- ports
          if not is_lut_used(i)then 
            for j in 0 to 1 loop -- RAMs
              lut_in_v(i, j).en  := '1';
              lut_in_v(i, j).we  := (others => not dma2mss.rnw);
              lut_in_v(i, j).we  := lut_in_v(i, j).we and dma2mss.be(7 - 4 * i - 2 * j downto 6 - 4 * i - 2 * j);
              lut_in_v(i, j).add(9 downto 1) := dma2mss.add(8 downto 0);
              if i = 0 then
                lut_in_v(i, j).add(0) := '0';
              else
                lut_in_v(i, j).add(0) := '1';
              end if;
              lut_in_v(i, j).wdata  := dma2mss.wdata(63 - 32 * i - 16 * j downto 48 - 32 * i - 16 * j);
            end loop;
            mss2dma.gnt(7 - 4 * i downto 4 - 4 * i) <= dma2mss.be(7 - 4 * i downto 4 - 4 * i);
            is_lut_used(i) := true;
            if dma2mss.rnw = '1' then
              mux_d.dma.be(7 - 4 * i downto 4 - 4 * i) <= dma2mss.be(7 - 4 * i downto 4 - 4 * i);
            end if;
          end if;
        end loop;
        mux_d.dma.mux <= lut;
      else --UCR (0x1000 - 0x11ff)
        if not is_u_used then
          mux_d.dma.mux  <= u;
          mss2dma.gnt    <= (others => '1');
          is_u_used      := true;
          -- port a
          u_in_v(0).en    := '1';
          u_in_v(0).we    := band((not dma2mss.rnw), dma2mss.be(7 downto 4));
          u_in_v(0).wdata := dma2mss.wdata(63 downto 32);
          u_in_v(0).add   := dma2mss.add(8 downto 0) & '0';
          -- port b
          u_in_v(1).en    := '1';
          u_in_v(1).we    := band((not dma2mss.rnw), dma2mss.be(3 downto 0));
          u_in_v(1).wdata := dma2mss.wdata(31 downto 0);
          u_in_v(1).add   := dma2mss.add(8 downto 0) & '1';
          if dma2mss.rnw = '1' then
            mux_d.dma.be <= dma2mss.be;
          end if;
        end if;
      end if;

      -- Out of range accesses:
      if or_reduce(dma2mss.add(28 downto 12)) = '1' then
-- pragma translate_off
        assert false
          report "DMA: out of range memory access (" & integer'image(to_integer(u_unsigned(dma2mss.add))) & ")"
          severity error;
-- pragma translate_on
        mss2dma.oor <= '1';
      end if;
    end if; 

    -------------------------------------------------------
    ------------------ VCI REQUESTS -----------------------
    -------------------------------------------------------

    if vci2mss.en = '1' then
      if vci2mss.add(11) = '1' then --DO (0x800 - 0x0fff)
        if not is_do_used then
          mss2vci.gnt <= vci2mss.be;
          if vci2mss.rnw = '1' then
            mux_d.vci.be <= vci2mss.be;
          end if;
          for i in 0 to 1 loop
            if to_i01(vci2mss.add(10)) = i then
              for j in 0 to 1 loop
                do_in_v(1, 2 * i + j).en := '1';
                do_in_v(1, 2 * i + j).we := (others => not vci2mss.rnw);
                do_in_v(1, 2 * i + j).we   := do_in_v(1, 2 * i + j).we and vci2mss.be(7 - 4 * j downto 4 - 4 * j);
                do_in_v(1, 2 * i + j).add := vci2mss.add(9 downto 0);
                do_in_v(1, 2 * i + j).wdata  := vci2mss.wdata(63 - 32 * j downto 32 - 32 * j);
              end loop;
              mux_d.vci.mux <= do0;
              if i = 1 then 
                mux_d.vci.mux <= do1;
              end if;
            end if;  
          end loop;
        end if;
      elsif vci2mss.add(10) = '1' then  -- DI (0x400 - 0x7ff)
        if not is_di_used then
          mss2vci.gnt <= vci2mss.be;
          if vci2mss.rnw = '1' then
            mux_d.vci.be <= vci2mss.be;
          end if;
          for i in 0 to 3 loop
            di_in_v(1, i).en  := '1';
            di_in_v(1, i).we := (others => not vci2mss.rnw);
            di_in_v(1, i).we   := di_in_v(1, i).we and vci2mss.be(7 - 2 * i downto 6 - 2 * i);
            di_in_v(1, i).add := vci2mss.add(9 downto 0);
            di_in_v(1, i).wdata  := vci2mss.wdata(63 - 16 * i downto 48 - 16 * i);
          end loop;
          mux_d.vci.mux <= di;
        end if;
      elsif vci2mss.add(9) = '1' then --LUT (0x200 - 0x3ff)
        for i in 0 to 1 loop -- ports
          if not is_lut_used(i)then 
            for j in 0 to 1 loop -- RAMs
              lut_in_v(i, j).en  := '1';
              lut_in_v(i, j).we  := (others => not vci2mss.rnw);
              lut_in_v(i, j).we  := lut_in_v(i, j).we and vci2mss.be(7 - 4 * i - 2 * j downto 6 - 4 * i - 2 * j);
              lut_in_v(i, j).add(9 downto 1) := vci2mss.add(8 downto 0);
              if i = 0 then
                lut_in_v(i, j).add(0) := '0';
              else
                lut_in_v(i, j).add(0) := '1';
              end if;
              lut_in_v(i, j).wdata  := vci2mss.wdata(63 - 32 * i - 16 * j downto 48 - 32 * i - 16 * j);
            end loop;
            mss2vci.gnt(7 - 4 * i downto 4 - 4 * i) <= vci2mss.be(7 - 4 * i downto 4 - 4 * i);
            if vci2mss.rnw = '1' then
              mux_d.vci.be(7 - 4 * i downto 4 - 4 * i) <= vci2mss.be(7 - 4 * i downto 4 - 4 * i);
            end if;
          end if;
        end loop;
        mux_d.vci.mux <= lut;
      else --UCR (0x1000 - 0x11ff)
        if not is_u_used then
          mux_d.vci.mux  <= u;
          mss2vci.gnt    <= (others => '1');
          -- port a
          u_in_v(0).en    := '1';
          u_in_v(0).we    := band((not vci2mss.rnw), vci2mss.be(7 downto 4));
          u_in_v(0).wdata := vci2mss.wdata(63 downto 32);
          u_in_v(0).add   := vci2mss.add(8 downto 0) & '0';
          -- port b
          u_in_v(1).en    := '1';
          u_in_v(1).we    := band((not vci2mss.rnw), vci2mss.be(3 downto 0));
          u_in_v(1).wdata := vci2mss.wdata(31 downto 0);
          u_in_v(1).add   := vci2mss.add(8 downto 0) & '1';
          if vci2mss.rnw = '1' then
            mux_d.vci.be <= vci2mss.be;
          end if;
        end if;
      end if;

      -- Out of range accesses:
      if or_reduce(vci2mss.add(28 downto 12)) = '1' then
-- pragma translate_off
        assert false
          report "VCI: out of range memory access (" & integer'image(to_integer(u_unsigned(vci2mss.add))) & ")"
          severity error;
-- pragma translate_on
        mss2vci.oor <= '1';
      end if;
    end if; 

    di_in <= di_in_v;
    lut_in <= lut_in_v;
    do_in <= do_in_v;
    u_in <= u_in_v;
    
  end process arbiter;

  mss2rams_pipe(1) <= (di => di_in, lut => lut_in, do => do_in, ucr => u_in);
  gn0: if n0 > 1 generate
    process(clk)
    begin
      if rising_edge(clk) then
        mss2rams_pipe(2 to n0) <= mss2rams_pipe(1 to n0 - 1);
      end if;
    end process;
  end generate gn0;
  rams2mss_pipe(1) <= rams2mss;
  gn1: if n1 > 1 generate
    process(clk)
    begin
      if rising_edge(clk) then
        rams2mss_pipe(2 to n1) <= rams2mss_pipe(1 to n1 - 1);
      end if;
    end process;
  end generate gn1;

  --* Pipelines for output multiplexers (20 DFFs)
  regs: process(clk)
  begin
    if rising_edge(clk) then
      mux_pipe  <= mux_d & mux_pipe(1 to n0 + n1 - 1);
    end if;
  end process regs;

  mss2rams <= mss2rams_pipe(n0);
  mux <= mux_pipe(n0 + n1);

  --* Multiplexing of block RAM outputs
  muxes: process(mux, rams2mss_pipe(n1))
  begin

    mss2uc.rdata <= rams2mss_pipe(n1).ucr(0) & rams2mss_pipe(n1).ucr(1);
    mss2uc.be    <= mux.uc;

    case mux.pss is
      when 0      => mss2pss.di <= rams2mss_pipe(n1).di(0, 0);
      when 1      => mss2pss.di <= rams2mss_pipe(n1).di(0, 1);
      when 2      => mss2pss.di <= rams2mss_pipe(n1).di(0, 2);
      when 3      => mss2pss.di <= rams2mss_pipe(n1).di(0, 3);
    end case;

    mss2pss.lut <= rams2mss_pipe(n1).lut(0, 0) & rams2mss_pipe(n1).lut(0, 1);

    case mux.dma.mux is
      when di  => mss2dma.rdata <= rams2mss_pipe(n1).di(1, 0) & rams2mss_pipe(n1).di(1, 1) & rams2mss_pipe(n1).di(1, 2) & rams2mss_pipe(n1).di(1, 3);
      when lut => mss2dma.rdata <= rams2mss_pipe(n1).lut(0, 0) & rams2mss_pipe(n1).lut(0, 1) & rams2mss_pipe(n1).lut(1, 0) & rams2mss_pipe(n1).lut(1, 1);
      when do0 => mss2dma.rdata <= rams2mss_pipe(n1).do(1, 0) & rams2mss_pipe(n1).do(1, 1);
      when do1 => mss2dma.rdata <= rams2mss_pipe(n1).do(1, 2) & rams2mss_pipe(n1).do(1, 3);
      when u   => mss2dma.rdata <= rams2mss_pipe(n1).ucr(0) & rams2mss_pipe(n1).ucr(1);
    end case;
    mss2dma.be <= mux.dma.be;

    case mux.vci.mux is
      when di  => mss2vci.rdata <= rams2mss_pipe(n1).di(1, 0) & rams2mss_pipe(n1).di(1, 1) & rams2mss_pipe(n1).di(1, 2) & rams2mss_pipe(n1).di(1, 3);
      when lut => mss2vci.rdata <= rams2mss_pipe(n1).lut(0, 0) & rams2mss_pipe(n1).lut(0, 1) & rams2mss_pipe(n1).lut(1, 0) & rams2mss_pipe(n1).lut(1, 1);
      when do0 => mss2vci.rdata <= rams2mss_pipe(n1).do(1, 0) & rams2mss_pipe(n1).do(1, 1);
      when do1 => mss2vci.rdata <= rams2mss_pipe(n1).do(1, 2) & rams2mss_pipe(n1).do(1, 3);
      when u   => mss2vci.rdata <= rams2mss_pipe(n1).ucr(0) & rams2mss_pipe(n1).ucr(1);
    end case;
    mss2vci.be <= mux.vci.be;

  end process muxes;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
