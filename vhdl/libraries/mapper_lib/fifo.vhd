--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.utils.all;

entity fifo is
  generic(fifo_depth: natural := 5;
          fifo_width: natural := 64);
  port(clk:         in  std_ulogic;
       srstn:       in  std_ulogic;
       ce:          in  std_ulogic;
       enr:         in  std_ulogic;      --enable read,should be '0' when not in use.
       enw:         in  std_ulogic;      --enable write,should be '0' when not in use.
       dataout:     out std_ulogic_vector(2 * fifo_width -1 downto 0);    --output data
       datain:      in  std_ulogic_vector (fifo_width -1 downto 0);    --input data
       empty:       out std_ulogic;      --set as '1' when the queue is empty
       full:        out std_ulogic);      --set as '1' when the queue is full
end entity fifo;

architecture rtl of fifo is

  type memory_type is array (0 to fifo_depth-1) of std_ulogic_vector(fifo_width-1 downto 0);
  signal memory: memory_type; --memory for queue.
  signal readptr, writeptr, count: u_unsigned(log2(fifo_depth) downto 0); --read and write pointers.
  signal full_s, empty_s: std_ulogic;
  
  begin
    process(clk) -- synchronous reset
    begin
      if(clk'event and clk='1') then
        if srstn = '0' then 
          for i in 0 to fifo_depth - 1 loop 
            memory(i) <= (others => '0');
          end loop;
          dataout  <= (others => '0');
          empty_s  <= '1';
          full_s  <= '0';
          count    <= (others => '0');
          readptr  <= (others => '0');
          writeptr  <= (others => '0');
        elsif ce = '1' then
          if enr = '1' and enw = '0' then
            -- only read operation
            if count = 1 then
              empty_s <= '1';
            end if;
            if full_s = '1' then
              full_s <= '0';
            end if;
            count <= count - 1;
            if (readptr = fifo_depth-1) then
              readptr <= (others => '0');
              dataout <= memory(to_integer(u_unsigned(readptr))) & memory(0);
            else
              readptr <= readptr + 1;      --points to next address.
              dataout <= memory(to_integer(u_unsigned(readptr))) & memory(to_integer(u_unsigned(readptr) + 1));
            end if;
          elsif enw = '1' and enr = '0' then
            -- only write operation
            if count = fifo_depth - 1 then
              full_s <= '1';
            end if;
            if empty_s =  '1' then
              empty_s <= '0';
            end if;
            count <= count + 1;
            memory(to_integer(u_unsigned(writeptr))) <= datain;
            if (writeptr = fifo_depth-1) then
              writeptr <= (others => '0');
            else
              writeptr  <= writeptr + 1;  --points to next address.
            end if;
          elsif enw = '1' and enr = '1' then 
            -- write and read operation
            memory(to_integer(u_unsigned(writeptr))) <= datain;
            if (readptr = fifo_depth-1) then
              dataout <= memory(to_integer(u_unsigned(readptr))) & memory(0);
              readptr <= (others => '0');
            else
              dataout <= memory(to_integer(u_unsigned(readptr))) & memory(to_integer(u_unsigned(readptr) + 1));
              readptr   <= readptr + 1;      --points to next address.
            end if;
            if (writeptr = fifo_depth-1) then
              writeptr <= (others => '0');
            else
              writeptr  <= writeptr + 1;  --points to next address.
            end if;
          end if;
        end if;
      end if;
    end process;

    full  <= full_s;
    empty <= empty_s;

  end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
