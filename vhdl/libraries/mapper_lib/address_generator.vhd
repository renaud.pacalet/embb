--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Output address generator
--*
--* PLease see the MAPPER user guide for a complete description of the address generation algorithm. This module is synchronous on the rising edge of its clock
--* CLK. Its reset SRSTN is synchronous and active low. address_generator samples the address generation parameters and outputs the first address when its LOAD
--* input is asserted on the rising edge of CLK. On the following rising edges of CLK, if its RUN input is asserted, it outputs the next addresses. When RUN is
--* de-asserted address_generator is stalled. RUN is ignored when LOAD is asserted. The generated addresses are word addresses in DO, represented on 11 bits.
--*
--* Important note: this module is a delicate piece of control; it has been validated by extensive simulations. It synthesizes on a LX330 Virtex 5
--* 5VLX330FF1760-2 target with a 200 MHz clock frequency and 32 LUTs. On a 65 nm ASIC target, worst case corner, it synthesizes with a 620 MHz clock frequency
--* and 1250 square microns. Modify at your own risks. Do not use a modified version before running the regression tests:
--* $ make address_generator_sim.sim

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity address_generator is
  port(clk:   in  std_ulogic;
       srstn: in  std_ulogic;
       ce:    in  std_ulogic;
       load:  in  std_ulogic;
       run:   in  std_ulogic;
       b:     in  u_unsigned(11 downto 0);
       s:     in  std_ulogic;
       n:     in  u_unsigned(6 downto 0);
       m:     in  u_unsigned(7 downto 0);
       a:     out u_unsigned(11 downto 0));
end entity address_generator;

architecture rtl of address_generator is

  signal a_q, a_d: u_unsigned(11 downto 0);
  signal n_q: u_unsigned(6 downto 0);
  signal m_q, m0_q, m_d, mm1: u_unsigned(7 downto 0);
  signal meq0, meq1: boolean;
  signal s_q: std_ulogic;
  signal inc: u_unsigned(11 downto 0);
  signal ci: natural range 0 to 1;

begin

  mm1  <= m_q - 1;
  meq0 <= m_q = x"00";
  meq1 <= m_q = x"01";
  m_d  <= m0_q when meq0 or meq1 else
          mm1(7 downto 0);

  inc <= resize(n_q, 12) when s_q = '0' else
         not resize(n_q, 12);

  ci <= 0 when s_q = '0' and (not meq1) else
        1 when s_q = '0' and meq1 else
        0 when s_q = '1' and meq1 else
        1;

  a_d <= a_q + inc + ci;

  process(clk)
  begin
    if clk'event and clk = '1' then
      if srstn = '0' then
        n_q  <= (others => '0');
        m_q  <= (others => '0');
        m0_q <= (others => '0');
        s_q  <= '0';
        a_q  <= (others => '0');
      elsif ce = '1' then
        if load = '1' then
          n_q  <= n;
          m_q  <= m;
          m0_q <= m;
          s_q  <= s;
          a_q  <= b;
        elsif run = '1' then
          m_q <= m_d;
          a_q <= a_d;
        end if;
      end if;
    end if;
  end process;

  a <= a_q;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
