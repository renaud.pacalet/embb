--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;

library memories_lib;
use memories_lib.ram_pkg.all;

use work.mapper_pkg.all;

entity rams is
  port(clk:      in  std_ulogic;
       mss2rams: in  mss2rams_type;
       rams2mss: out rams2mss_type);
end entity rams;

architecture rtl of rams is

  begin
  
  --* -----------------------------------------------------
  --* ---------------- BLOCK RAM INSTANCES ----------------
  --* -----------------------------------------------------

  g_di: for i in 0 to 3 generate
    ram_di: entity memories_lib.tdpram(arc)
    generic map(registered => true,
                na         => 10,
                nb         => 2)
    port map(clk  => clk,
             ena   => mss2rams.di(0, i).en,
             enb   => mss2rams.di(1, i).en,
             wea   => mss2rams.di(0, i).we,
             web   => mss2rams.di(1, i).we,
             adda  => mss2rams.di(0, i).add,
             addb  => mss2rams.di(1, i).add,
             dina  => mss2rams.di(0, i).wdata,
             dinb  => mss2rams.di(1, i).wdata,
             douta => rams2mss.di(0, i),
             doutb => rams2mss.di(1, i));
  end generate g_di;

  g_lut: for i in 0 to 1 generate
    ram_lut: entity memories_lib.tdpram(arc)
    generic map(registered => true,
                na         => 10,
                nb         => 2)
    port map(clk  => clk,
             ena   => mss2rams.lut(0, i).en,
             enb   => mss2rams.lut(1, i).en,
             wea   => mss2rams.lut(0, i).we,
             web   => mss2rams.lut(1, i).we,
             adda  => mss2rams.lut(0, i).add,
             addb  => mss2rams.lut(1, i).add,
             dina  => mss2rams.lut(0, i).wdata,
             dinb  => mss2rams.lut(1, i).wdata,
             douta => rams2mss.lut(0, i),
             doutb => rams2mss.lut(1, i));
  end generate g_lut;

  g_do: for i in 0 to 3 generate
    ram_do: entity memories_lib.tdpram(arc)
    generic map(registered => true,
                na         => 10,
                nb         => 4)
    port map(clk  => clk,
             ena   => mss2rams.do(0, i).en,
             enb   => mss2rams.do(1, i).en,
             wea   => mss2rams.do(0, i).we,
             web   => mss2rams.do(1, i).we,
             adda  => mss2rams.do(0, i).add,
             addb  => mss2rams.do(1, i).add,
             dina  => mss2rams.do(0, i).wdata,
             dinb  => mss2rams.do(1, i).wdata,
             douta => rams2mss.do(0, i),
             doutb => rams2mss.do(1, i));
  end generate g_do;


  g_u : entity memories_lib.tdpram(arc)
    generic map(
-- pragma translate_off
                init_file     => "", -- initialization file
                si            => 0,  -- first address of initialization file
                instance_name => "ucr", -- Name of the RAM instance.
-- pragma translate_on
                registered => true,  -- control output registers
                na         => 10,    -- bit-width of address busses
                nb         => 4)     -- byte-width of data busses
    port map(clk   => clk,
             ena   => mss2rams.ucr(0).en,
             enb   => mss2rams.ucr(1).en,
             wea   => mss2rams.ucr(0).we,
             web   => mss2rams.ucr(1).we,
             adda  => mss2rams.ucr(0).add,
             addb  => mss2rams.ucr(1).add,
             dina  => mss2rams.ucr(0).wdata,
             dinb  => mss2rams.ucr(1).wdata,
             douta => rams2mss.ucr(0),
             doutb => rams2mss.ucr(1));

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
