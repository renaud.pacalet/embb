/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <fcntl.h>
#include <unistd.h>

#include <embb/mapper.h>
#include <embb/memory.h>
#include <assert.h>
#include <stdint.h>
#include <utils.h>
#include <tests.h>

#define INDATA_SIZE 32

#define DEBUG(...) fprintf(stderr, __VA_ARGS__);

#define ASSERT_EQ(a, b)							\
  do {									\
    typeof(a) a_ = a;							\
    typeof(a) b_ = b;							\
    if (a_ != b_) {							\
      DEBUG("error:%u:%s:%s %s equals to 0x%x, expected 0x%x\n", __LINE__, __FILE__, __func__, #a, a_, b_); \
      abort();								\
    }									\
  } while (0)

// Human-readable print of parameters
void print_params(FILE *f, MAPPER_CONTEXT *ctx) {
  fprintf(f, "# LENM1=%lld, LBA=%lld, OBA=%lld, IBA=%lld, MULT=0x%08llx, MEN=%lld, SYM=%lld, BPSM1=%lld, M=%lld, N=%lld, S=%lld\n", mapper_get_lenm1(ctx),
      mapper_get_lba(ctx), mapper_get_oba(ctx), mapper_get_iba(ctx), mapper_get_mult(ctx), mapper_get_men(ctx), mapper_get_sym(ctx), mapper_get_bpsm1(ctx),
      mapper_get_m(ctx), mapper_get_n(ctx), mapper_get_s(ctx));
}

// Print execute command
void print_ex(FILE *f, MAPPER_CONTEXT *ctx) {
  fprintf(f, "EX 0 0 0 0f\n%016llx\n%016llx\n", ctx->devmem->cmd._r1, ctx->devmem->cmd._r0);
}

// randomly generates context ctx. cst and val are two mapper_params structures expressing constraints on the random generation and must be initialized prior
// calling randctx. In cst field values are either 0 or 1; a 1 indicates that the corresponding field value must be taken as is from val; a 0 indicates that the
// field must be randomly picked between 0 (included) and the same filed's value in val (exluded). When randomly picked, each field is guaranteed to have a
// valid value with respect to the other ctx fields.
void randctx(MAPPER_CONTEXT *ctx, mapper_params cst, mapper_params val)
{
  uint16_t sym;
  uint16_t max;
  uint16_t bpsm1;

  if(cst.lenm1 == 1)
    mapper_set_lenm1(ctx, val.lenm1);
  else
    mapper_set_lenm1(ctx, do_rand() % val.lenm1);

  if(cst.oba == 1)
    mapper_set_oba(ctx, val.oba);
  else
    mapper_set_oba(ctx, do_rand() % val.oba);

  if(cst.iba == 1)
    mapper_set_iba(ctx, val.iba);
  else
    mapper_set_iba(ctx, do_rand() % val.iba);

  if(cst.mult == 1)
    mapper_set_mult(ctx, val.mult);
  else
    mapper_set_mult(ctx, do_rand() % val.mult);

  if(cst.men == 1)
    mapper_set_men(ctx, val.men);
  else
    mapper_set_men(ctx, do_rand() % val.men);

  if(cst.sym == 1)
    sym = val.sym;
  else
    sym = do_rand() % val.sym;
  mapper_set_sym(ctx, sym);

  if(cst.bpsm1 == 1)
    bpsm1 = val.bpsm1;
  else if(sym == 1) {
    if(val.bpsm1 < 2)
      ERROR(-1, "Symmetrical mode. Cannot find a valid BPSM1 value less than %d", val.bpsm1);
    bpsm1 = val.bpsm1 / 2;
    bpsm1 = (do_rand() % bpsm1) * 2 + 1;
  }
  else {
    bpsm1 = val.bpsm1 < 10 ? val.bpsm1 : 10;
    bpsm1 = do_rand() % bpsm1;
  }
  mapper_set_bpsm1(ctx, bpsm1);

  if(cst.lba == 1)
    mapper_set_lba(ctx, val.lba);
  else {
    if(sym == 1)
      max = (1 << ((bpsm1 + 1) / 2)) - 1;
    else
      max = (1 << (bpsm1 + 1)) - 1;
    max = (1 << 10) - max;
    max = max < val.lba ? max : val.lba;
    mapper_set_lba(ctx, do_rand() % max);
  }

  if(cst.m == 1)
    mapper_set_m(ctx, val.m);
  else
    mapper_set_m(ctx, do_rand() % val.m);

  if(cst.n == 1)
    mapper_set_n(ctx, val.n);
  else
    mapper_set_n(ctx, do_rand() % val.n);

  if(cst.s == 1)
    mapper_set_s(ctx, val.s);
  else
    mapper_set_s(ctx, do_rand() % val.s);
}

// Run one random test
void mktests(FILE *f, MAPPER_CONTEXT *ctx, mapper_params cst, mapper_params val)
{
  size_t l, sx, sy, sz, ss; // Vector length, section byte-lengths
  uint16_t bx, by, bz, bs;  // Base addresses
  int i, op;
  uint8_t data[0x4000];

  randctx(ctx, cst, val);
  print_params(f, ctx);
  mapper_start(ctx);
  print_ex(f, ctx);
  fprintf(f, "WE 10000\n");
  embb_ip2mem(data, (EMBB_CONTEXT*)ctx, 0x4000, 0x4000);
  fprintf(f, "DR 4000 2048 0 ff 1 0 ff\n");
  dump_buffer(f, data, 0x1000, 4, 0, 1, 8);
  fprintf(f, "WD 10000\n");
}
