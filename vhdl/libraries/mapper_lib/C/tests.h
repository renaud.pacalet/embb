/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <embb/mapper.h>
#include <embb/memory.h>
#include <stdint.h>

typedef struct {
  int16_t lenm1;
  int16_t lba;
  int16_t oba;
  int32_t iba;
  int64_t mult;
  int16_t men;
  int16_t sym;
  int16_t bpsm1;
  int16_t m;
  int16_t n;
  int16_t s;
} mapper_params;

// Run one random test
void mktests(FILE *f, MAPPER_CONTEXT *ctx, mapper_params cst, mapper_params val);
