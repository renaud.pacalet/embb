/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <fcntl.h>
#include <unistd.h>
#include <math.h>

#include <embb/mapper.h>
#include <embb/memory.h>
#include <assert.h>
#include <stdint.h>
#ifdef EMBB_CSSTEST_ENABLE
#include <config.h>
#endif 

uint32_t do_crc32(void *data_, int len);
uint32_t do_rand();
void do_srand(uint32_t seed);
uint16_t iof = 0x0000;
uint32_t lof = 0x2000;
uint32_t oof = 0x4000;
uint16_t din[4096];
uint32_t dlut[1024]; 
uint32_t dout[4096];

#define INDATA_SIZE 32

#define DEBUG(...) fprintf(stderr, __VA_ARGS__);

#define DUMP_BUFFERS              \
  for (i = 0; i < count; i++)            \
    DEBUG("%5x %02x %5x %02x\n", i, indata[i], pdata[i], outdata[i]);

#define ASSERT_EQ(a, b)              \
  do {                  \
    typeof(a) a_ = a;              \
    typeof(a) b_ = b;              \
    if (a_ != b_) {              \
      DEBUG("error:%u:%s:%s %s equals to 0x%x, expected 0x%x\n", __LINE__, __FILE__, __func__, #a, a_, b_); \
      abort();                \
    }                  \
  } while (0)

typedef struct {
  int16_t lenm1;
  int16_t lba;
  int16_t oba;
  int32_t iba;
  int32_t mult;
  int16_t men;
  int16_t sym;
  int16_t bpsm1;
  int16_t m;
  int16_t n;
  int16_t s;
} mapper_params;

// Hex dump buffer B of L values, BW bytes each, according source (SE) and destination (DE) endianness (0: little endian, 1: big endian). Written values are
// W-bytes long (that is, 2*W-hex digits).
void dump_buffer(FILE *f, uint8_t *b, int l, int bw, int se, int de, int w) {
  int i, j, n;

  for(i = 0, n = 0; i < l; i++) {
    if(se == de) {
      for(j = 0; j < bw; j++) {
        fprintf(f, "%02X", b[i * bw + j]);
        n += 1;
        if(n == w) {
          fprintf(f, "\n");
          n = 0;
        }
      }
    }
    else {
      for(j = bw - 1; j >= 0; j--) {
        fprintf(f, "%02X", b[i * bw + j]);
        n += 1;
        if(n == w) {
          fprintf(f, "\n");
          n = 0;
        }
      }
    }
  }
}

// Human-readable print of parameters
void print_params(FILE *f, MAPPER_CONTEXT *ctx) {
  fprintf(f, "# LENM1=%lld, LBA=%lld, OBA=%lld, IBA=%lld, MULT=%lld, MEM=%lld, SYM=%lld, BPSM1=%lld\n", mapper_get_lenm1(ctx), mapper_get_lba(ctx), mapper_get_oba(ctx),
      mapper_get_iba(ctx), mapper_get_mult(ctx), mapper_get_men(ctx), mapper_get_sym(ctx), mapper_get_bpsm1(ctx));
  fprintf(f, "# M=%lld, N=%lld, S=%lld\n", mapper_get_m(ctx), mapper_get_n(ctx), mapper_get_s(ctx));
}

// Print execute command
void print_ex(FILE *f, MAPPER_CONTEXT *ctx) {
  fprintf(f, "EX 0 0\n%016llx\n%016llx\n", ctx->devmem->cmd._r1, ctx->devmem->cmd._r0);
}

// Type to byte-width conversion
#define BW(t) ((t)==0?1:((t)==3?4:2))

// randomly generates context ctx. cst and val are two mapper_params structures expressing constraints on the random generation and must be initialized prior
// calling randctx. In cst field values are either 0 or 1; a 1 indicates that the corresponding field value must be taken as is from val; a 0 indicates that the
// field must be randomly picked between 0 (included) and the same filed's value in val (exluded). When randomly picked, each field is guaranteed to have a
// valid value with respect to the other ctx fields.
void randctx(MAPPER_CONTEXT *ctx, mapper_params cst, mapper_params val)
{
  if(cst.lenm1 == 1)
    mapper_set_lenm1(ctx, val.lenm1);
  else
    mapper_set_lenm1(ctx, do_rand() % val.lenm1);
  
  mapper_set_oba(ctx, 0);
  mapper_set_iba(ctx, 0);
  mapper_set_men(ctx, 0);
  mapper_set_m(ctx, 0);
  mapper_set_n(ctx, 1);
  mapper_set_s(ctx, 0);
  mapper_set_sym(ctx, 0);


  if(cst.oba == 1)
    mapper_set_oba(ctx, val.oba);
  else
    mapper_set_oba(ctx, do_rand() % val.oba);


  if(cst.mult == 1)
    mapper_set_mult(ctx, val.mult);
  else
    mapper_set_mult(ctx, do_rand() % val.mult);

  if(cst.men == 1)
    mapper_set_men(ctx, val.men);
  else
    mapper_set_men(ctx, do_rand() % val.men);


  if(cst.m == 1)
    mapper_set_m(ctx, val.m);
  else 
    mapper_set_m(ctx, do_rand() % val.m);

  if(cst.n == 1)
    mapper_set_n(ctx, val.n);
  else 
    mapper_set_n(ctx, do_rand() % val.n);

  if(cst.s == 1)
    mapper_set_s(ctx, val.s);
  else 
    mapper_set_s(ctx, do_rand() % val.s);
  
  if(cst.sym == 1)
    mapper_set_sym(ctx, val.sym);
  else
    mapper_set_sym(ctx, do_rand() % val.sym);

  if(cst.iba == 1)
    mapper_set_iba(ctx, val.iba);
  else
    mapper_set_iba(ctx, do_rand() % val.iba);


  if(cst.bpsm1 == 1)
    mapper_set_bpsm1(ctx, val.bpsm1);
  else{
    mapper_set_bpsm1(ctx, do_rand() % val.bpsm1);
    if (mapper_get_sym(ctx) == 0) {
      while (mapper_get_bpsm1(ctx) >= 10)
       mapper_set_bpsm1(ctx, do_rand() % val.bpsm1);}
    else{
      while (mapper_get_bpsm1(ctx) % 2 == 0)
       mapper_set_bpsm1(ctx, do_rand() % val.bpsm1);}}

  if(cst.lba == 1)
    mapper_set_lba(ctx, val.lba);
  else{
    if (mapper_get_sym(ctx) == 1){
       if (mapper_get_bpsm1(ctx) == 15)
         mapper_set_lba(ctx, do_rand() % 0x300);
       else if (mapper_get_bpsm1(ctx) == 13)
         mapper_set_lba(ctx, do_rand() % 0x380);
       else if (mapper_get_bpsm1(ctx) == 11)
         mapper_set_lba(ctx, do_rand() % 0x3C0);
       else if (mapper_get_bpsm1(ctx) == 9)
         mapper_set_lba(ctx, do_rand() % 0x3E0);
       else if (mapper_get_bpsm1(ctx) == 7)
         mapper_set_lba(ctx, do_rand() % 0x3F0);
       else if (mapper_get_bpsm1(ctx) == 5)
         mapper_set_lba(ctx, do_rand() % 0x3F8);
       else if (mapper_get_bpsm1(ctx) == 3)
         mapper_set_lba(ctx, do_rand() % 0x3FC);
       else if (mapper_get_bpsm1(ctx) == 1)
         mapper_set_lba(ctx, do_rand() % 0x3FE);}
    else{
     if (mapper_get_bpsm1(ctx) == 9)
       mapper_set_lba(ctx, 0);
     else if (mapper_get_bpsm1(ctx) == 8)
       mapper_set_lba(ctx, do_rand() % 0x200);
     else if (mapper_get_bpsm1(ctx) == 7)
       mapper_set_lba(ctx, do_rand() % 0x300);
     else if (mapper_get_bpsm1(ctx) == 6)
       mapper_set_lba(ctx, do_rand() % 0x380);
     else if (mapper_get_bpsm1(ctx) == 5)
       mapper_set_lba(ctx, do_rand() % 0x3C0);
     else if (mapper_get_bpsm1(ctx) == 4)
       mapper_set_lba(ctx, do_rand() % 0x3E0);
     else if (mapper_get_bpsm1(ctx) == 3)
       mapper_set_lba(ctx, do_rand() % 0x3F0);
     else if (mapper_get_bpsm1(ctx) == 2)
       mapper_set_lba(ctx, do_rand() % 0x3F8);
     else if (mapper_get_bpsm1(ctx) == 1)
       mapper_set_lba(ctx, do_rand() % 0x3FC);
     else if (mapper_get_bpsm1(ctx) == 0)
       mapper_set_lba(ctx, do_rand() % 0x3FE);}}
}


// Run one random test
void mapper_hw_tests(FILE *f, MAPPER_CONTEXT *ctx, mapper_params cst, mapper_params val)
{
  int i;
  int wrap = 0;

  uint32_t len64, start, end;

  randctx(ctx, cst, val);

  print_params(f, ctx);

  uint32_t oba = mapper_get_oba(ctx);
  
  start = oof/8 + (oba)/2;
  end = oof/8 + (oba + mapper_get_lenm1(ctx))/2;
  len64 = end - start + 1;

  //DO

  fprintf(f, "# D0\n");

  for (i = 0; i < 4096; i++)
    dout[i] = do_rand();

  embb_mem2ip((EMBB_CONTEXT*)ctx, oof, dout, 4096 * 4);

  fprintf(f, "WR %llx %d ff\n", oof/8, 2048);

  dump_buffer(f, (uint8_t *)dout, 4096, 4, 0, 1, 8);

  fprintf(f, "WD 10000\n");

  //START 

  mapper_start(ctx);

  print_ex(f, ctx);

  fprintf(f, "WE 10000\n");

  //D0

  embb_ip2mem(dout, (EMBB_CONTEXT*)ctx, oof, 4096 * 4);
  

  fprintf(f, "RD %llx %d ff\n", oof/8, 2048);

  dump_buffer(f, (uint8_t *)dout, 4096, 4, 0, 1, 8);

  fprintf(f, "WD 10000\n");

}

int main(int argc, char **argv)
{
  int i;
  FILE *f;
  int n;
  MAPPER_CONTEXT ctx; // Context
  mapper_params cst, val; // Parameters constraints

  if(argc != 3) {
    fprintf(stderr, "usage: mapper_vp_tests <cmd_file_name> <num_tests>\n");
    exit(-1);
  }
  f = fopen(argv[1], "w");
  n = atol(argv[2]);
  mapper_ctx_init(&ctx, 0);
  do_srand(1);

  // Initialize cst (randomly pick fields)
  memset(&cst, 0, sizeof(cst));

  // Initialize val
  val.lenm1  = (1 << 4);
  val.lba  = (1 << 10);
  val.oba  = (1 << 12);
  val.iba  = (1 << 16);
  val.mult = (1 << 31);
  val.men = 2;
  val.sym = 2;
  val.bpsm1 = 16;
  val.m = (1 << 8);
  val.n = (1 << 7);
  val.s = 2;
 
  // initialise input buffer 
  for (i = 0; i < 4096; i++)
    din[i] = do_rand() % 0xFFFF;
  // initialise lut buffer 
  for (i = 0; i < 1024; i++)
    dlut[i] = do_rand();
  // initialise output buffer 
  for (i = 0; i < 4096; i++)
    dout[i] = do_rand();

  //DI
   
  fprintf(f, "# DI\n");

  embb_mem2ip((EMBB_CONTEXT*)&ctx, iof, din, 8192);
  
  fprintf(f, "WR %llx %d ff\n", iof / 8, 1024);

  dump_buffer(f, (uint8_t *)din, 4096, 2, 0, 1, 8);

  fprintf(f, "WD 10000\n");

  //LUT

  fprintf(f, "# LUT\n");

  embb_mem2ip((EMBB_CONTEXT*)&ctx, lof, dlut, 4096);

  fprintf(f, "WR %llx %d ff\n", lof / 8, 512);

  dump_buffer(f, (uint8_t *)dlut, 1024, 4, 0, 1, 8);

  fprintf(f, "WD 10000\n");

 for(i = 0; i < n; i++) {
    fprintf(f, "#################\n");
    fprintf(f, "# Test #%d\n", i + 1);
    fprintf(f, "#################\n");
    fprintf(f, "RS 10\n");
    mapper_hw_tests(f, &ctx, cst, val);
    fprintf(f, "# End of test #%d\n\n", i + 1);
  }
  mapper_ctx_cleanup(&ctx);
  fclose(f);
  return 0;
}
