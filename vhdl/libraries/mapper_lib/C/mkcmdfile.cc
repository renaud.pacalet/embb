/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <embb/mapper.h>
#include <embb/memory.h>
#include <assert.h>
#include <stdint.h>
#include <utils.h>
#include <tests.h>

// #define _GNU_SOURCE
#include <getopt.h>

/* Parse command line options, initialize file names. */
void parse_options (int argc, char **argv);

FILE *f;
int n;
int core;

int main(int argc, char **argv)
{
  int i;
  MAPPER_CONTEXT ctx; // Context
  mapper_params cst, val; // Parameters constraints
  uint8_t data[0x8000];

  parse_options (argc, argv);

  mapper_ctx_init(&ctx, 0);
  do_srand(1);

  // Initialize cst (randomly pick fields)
  memset(&cst, 0, sizeof(cst));

  // Initialize val
  val.lenm1  = (1 << 12);
  val.lba    = (1 << 10);
  val.oba    = (1 << 12);
  val.iba    = (1 << 16);
  val.mult   = (1ULL << 32);
  val.men    = 2;
  val.sym    = 2;
  val.bpsm1  = (1 << 4);
  val.m      = (1 << 8);
  val.n      = (1 << 7);
  val.s      = 2;

  fprintf(f, "# Reset\n");
  fprintf(f, "RS 10\n");
  if(core == 0) {
    fprintf(f, "# Clear interrupt flags\n");
    fprintf(f, "VR fffb0 1 0 00 0 0\n");
    fprintf(f, "# Set resets, chip enables and interrupt enables\n");
    fprintf(f, "VW fffa8 1 0 ff\n");
    fprintf(f, "0000000007001636\n"); // No UC, no extended interrupts
  }
  for (i = 0; i < 0x8000; i++)
    data[i] = do_rand() % 256;
  // Initialize LUT
  embb_mem2ip((EMBB_CONTEXT*)&ctx, 0x1000, data + 0x1000, 0x1000);
  fprintf(f, "# Initialize LUT\n");
  fprintf(f, "DW 1000 512 0 ff\n");
  dump_buffer(f, data + 0x1000, 0x400, 4, 0, 1, 8);
  fprintf(f, "WD 10000\n");
  // Initialize DI
  embb_mem2ip((EMBB_CONTEXT*)&ctx, 0x2000, data + 0x2000, 0x2000);
  fprintf(f, "# Initialize DI\n");
  fprintf(f, "DW 2000 1024 0 ff\n");
  dump_buffer(f, data + 0x2000, 0x1000, 2, 0, 1, 8);
  fprintf(f, "WD 10000\n");
  // Initialize DO
  embb_mem2ip((EMBB_CONTEXT*)&ctx, 0x4000, data + 0x4000, 0x4000);
  fprintf(f, "# Initialize DO\n");
  fprintf(f, "DW 4000 2048 0 ff\n");
  dump_buffer(f, data + 0x4000, 0x1000, 4, 0, 1, 8);
  fprintf(f, "WD 10000\n");
  for(i = 0; i < n; i++) {
    fprintf(f, "#################\n");
    fprintf(f, "# Test #%d\n", i + 1);
    fprintf(f, "#################\n");
    if(core == 1) {
      fprintf(f, "RS 10\n");
    }
    val.lenm1 = 1 << (do_rand() % 13);
    mktests(f, &ctx, cst, val);
    fprintf(f, "# End of test #%d\n\n", i + 1);
  }
  mapper_ctx_cleanup(&ctx);
  fclose(f);
  return 0;
}

const char *version_string = "\
mkcmdfile 0.1\n\
\n\
Copyright (C) 2010 Institut Telecom\n\
\n\
This software is governed by the CeCILL license under French law and\n\
abiding by the rules of distribution of free software.  You can  use,\n\
modify and/ or redistribute the software under the terms of the CeCILL\n\
license as circulated by CEA, CNRS and INRIA at the following URL\n\
http://www.cecill.info\n\
\n\
Written by Renaud Pacalet <renaud.pacalet@telecom-paristech.fr>.\n\
";

const char *usage_string = "\
mkcmdfile generate commands for VHDL functional simulation\n\
of a DSP unit or an IP core.\n\
\n\
Usage: mkcmdfile [OPTION]... [OUTPUTFILE]\n\
\n\
Mandatory arguments to long options are mandatory for short options too.\n\
If OUTPUTFILE is not specified stdout is used. On success the exit status is 0.\n\
\n\
Options:\n\
  -c, --core            generate a command file for IP core simulation instead of whole DSP unit\n\
  -t, --tests=NUM       number of tests to generate (default=1)\n\
  -v, --version\n\
        Output version information and exit\n\
  -h, --help\n\
        Display this help and exit\n\
\n\
Please report bugs to <renaud.pacalet@telecom-paristech.fr>.\n\
";

void
parse_options (int argc, char **argv) {
  int o, option_index;
  struct option long_options[] = {
    {"core", no_argument, NULL, 'c'},
    {"tests", required_argument, NULL, 't'},
    {"version", no_argument, NULL, 'v'},
    {"help", no_argument, NULL, 'h'},
    {0, 0, 0, 0}
  };

  option_index = 0;
  core = 0;
  n = 1;
  while (1) {
    o =
      getopt_long (argc, argv, "ct:vh",
      	     long_options, &option_index);
    if (o == -1) {
      break;
    }
    switch (o) {
      case 'c':
        core = 1;
        break;
      case 't':
        n = atoi(optarg);
        if(n < 1) {
          fprintf (stderr, "*** Invalid number of tests: %d\n", n);
          ERROR(-1, "%s", usage_string);
        }
        break;
      case 'v':
        fprintf (stderr, "%s", version_string);
        exit (0);
        break;
      default:
        fprintf (stderr, "%s", usage_string);
        exit (0);
        break;
    }
  }
  f = stdout;
  if (optind < argc) {
    f = XFOPEN(argv[optind], "w");
    optind += 1;
  }
  if (optind < argc) {
    ERROR(-1, "%s", usage_string);
  }
}
