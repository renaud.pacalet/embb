--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
-- use ieee.std_logic_textio.all;

library global_lib;
use global_lib.global.all;

library memories_lib;
use memories_lib.ram_pkg.all;

use work.mapper_pkg.all;

entity pss_sim is
  generic(cmdfile: string := "mapper.cmd");
end entity pss_sim;

architecture sim of pss_sim is

  signal clk:     std_ulogic;
  signal css2pss:  css2pss_type;
  signal pss2css:  pss2css_type;
  signal param:   std_ulogic_vector(cmdsize * 8 - 1 downto 0);
  signal pss2mss:  pss2mss_type;
  signal mss2pss:  mss2pss_type;
  signal css2mss: css2mss_type;
  signal mss2css: mss2css_type;
  signal mss2rams: mss2rams_type;
  signal rams2mss: rams2mss_type;

  alias vci2mss is css2mss.vci2mss;
  alias mss2vci is mss2css.mss2vci;
  alias dma2mss is css2mss.dma2mss;
  alias mss2dma is mss2css.mss2dma;
  alias uc2mss  is css2mss.uc2mss;
  alias mss2uc  is mss2css.mss2uc;

  constant n0 : positive := 2;
  constant n1 : positive := 2;
 
begin

  i_pss: entity work.pss(rtl)
    generic map(mss_pipeline_depth => n0 + n1)
    port map(clk    => clk,
             css2pss => css2pss,
             param  => param,
             pss2css => pss2css,
             pss2mss => pss2mss,
             mss2pss => mss2pss);

  i_mss: entity work.mss(rtl)
    generic map(n0  => n0,
                n1  => n1)
    port map(clk     => clk,
             css2mss => css2mss,
             mss2css => mss2css,
             pss2mss  => pss2mss,
             mss2pss  => mss2pss,
             mss2rams => mss2rams,
             rams2mss => rams2mss);

  i_rams: entity work.rams(rtl)
    port map(clk => clk,
             mss2rams => mss2rams,
             rams2mss => rams2mss);

  i_css: entity global_lib.css_emulator(arc)
	  generic map(cmdsize => cmdsize,
		            cmdfile => cmdfile)
    port map(clk => clk,
             css2pss => css2pss,
             pss2css => pss2css,
             css2mss => css2mss,
             mss2css => mss2css,
             param => param);

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
