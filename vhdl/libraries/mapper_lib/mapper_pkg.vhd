--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Utility package

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.utils.all;
use global_lib.global.all;

library memories_lib;
use memories_lib.ram_pkg.all;

use work.bitfield.all;

package mapper_pkg is

  -- Compute hbps (half bits per symbol) and msk (a mask used to extract symbols and half symbols).
  -- hbps=(bpsm1+1)/2 if sym, else 0
  --  msk=(2^hbps)-1 if sym, else (2^(bpsm1+1))-1
  -- Assumes bpsm1(0)='1' if sym (even bits per symbol), else bpsm1(3)='0' (bits per symbol <= 8)
  procedure bpsm1_pre_processing(bpsm1: in std_ulogic_vector(3 downto 0); sym: in std_ulogic; hbps: out natural range 0 to 8; msk: out std_ulogic_vector(9 downto 0));
  -- Compute LUT addresses for the real and imaginary parts of output point.
  procedure compute_lut_addresses(shl: in positive range 1 to 31; shr: in natural range 0 to 8; msk: in std_ulogic_vector(9 downto 0); di: in word32;
                                  lba: in std_ulogic_vector(9 downto 0); ra: out std_ulogic_vector(9 downto 0); ia: out std_ulogic_vector(9 downto 0); oor : out std_ulogic);

  --* Number of byte of command register
  constant cmdsize: integer := bitfield_width / 8;

  type pss2mss_type is record
    eni: std_ulogic; -- enable read channel di
    eno: std_ulogic; -- enable write channel do
    enl: std_ulogic; -- enable read channels lut
    addi: std_ulogic_vector(11 downto 0); -- address for channel di
    addo: std_ulogic_vector(11 downto 0); -- address for channel do
    addlr: std_ulogic_vector(9 downto 0);  -- address for channel lut real part
    addli: std_ulogic_vector(9 downto 0);  -- address for channel lut imaginary part
    do: std_ulogic_vector(31 downto 0); -- write data for channel do
  end record;

  type mss2pss_type is record
    di: std_ulogic_vector(15 downto 0); -- read data from channel di
    lut: std_ulogic_vector(31 downto 0); -- read data from channel lut
  end record;
  
  --RAMS interfaces
  type ucr_in_type is array(0 to 1) of in_port_1kx32;
  type ucr_out_type is array(0 to 1) of word32;
  type di_in_type is array(0 to 1, 0 to 3) of in_port_1kx16;
  type di_out_type is array(0 to 1, 0 to 3) of word16;
  type lut_in_type is array(0 to 1, 0 to 1) of in_port_1kx16;
  type lut_out_type is array(0 to 1, 0 to 1) of word16;
  type do_in_type is array(0 to 1, 0 to 3) of in_port_1kx32;
  type do_out_type is array(0 to 1, 0 to 3) of word32;

  type mss2rams_type is record
    ucr: ucr_in_type;
    di : di_in_type;
    lut : lut_in_type;
    do : do_in_type;
  end record;
  type mss2rams_vector is array(natural range <>) of mss2rams_type;

  type rams2mss_type is record
    ucr : ucr_out_type;
    di : di_out_type;
    lut : lut_out_type;
    do : do_out_type;
  end record;
  type rams2mss_vector is array(natural range <>) of rams2mss_type;

  type fifo_if is record
            srstn       : std_ulogic;      
            enr         : std_ulogic;      
            enw         : std_ulogic;     
            dataout     : std_ulogic_vector(31 downto 0);  
            datain      : std_ulogic_vector (15 downto 0);
            empty       : std_ulogic;    
            full        : std_ulogic;   
  end record;

  function sat(l: u_signed; i: positive) return u_signed;

end package mapper_pkg;

package body mapper_pkg is

  procedure bpsm1_pre_processing(bpsm1: in std_ulogic_vector(3 downto 0); sym: in std_ulogic; hbps: out natural range 0 to 8; msk: out std_ulogic_vector(9 downto 0)) is
    variable tmp: natural range 0 to 9;
    variable val: u_unsigned(25 downto 0);
  begin
    hbps := 0;
    msk  := (others => '0');
    val  := "00" & X"01FFFF";
    if sym = '1' then                                         -- Symmetrical, bits per symbol is even (bpsm1(0) = '1')
      tmp  := to_integer(u_unsigned(bpsm1(3 downto 1)));  -- (bits per symbol - 1) / 2 = (bits per symbol) / 2 - 1
      hbps := tmp + 1;                                  -- (bits per symbol) / 2
      val  := shift_left(val, tmp);
      msk  := std_ulogic_vector(val(25 downto 16)); -- Half symbol mask
    else                                                -- Non symmetrical, bits per symbol is less or equal 9 (bpsm1(3) = '0')
      tmp := to_integer(u_unsigned(bpsm1(3 downto 0)));   -- (bits per symbol) - 1
      val  := shift_left(val, tmp);
      msk  := std_ulogic_vector(val(25 downto 16)); -- Full symbol mask
    end if;
  end procedure bpsm1_pre_processing;

  procedure compute_lut_addresses(shl: in positive range 1 to 31; shr: in natural range 0 to 8; msk: in std_ulogic_vector(9 downto 0); di: in word32;
                                  lba: in std_ulogic_vector(9 downto 0); ra: out std_ulogic_vector(9 downto 0);
                                  ia: out std_ulogic_vector(9 downto 0); oor : out std_ulogic) is
    variable tmp48: std_ulogic_vector(47 downto 0);
    variable tmp16: word16;
    variable ro, io: u_unsigned(10 downto 0); -- Offsets in LUT (real and imaginary halves)
  begin
    tmp48 := x"0000" & di;
    tmp48 := shift_left(tmp48, shl);
    tmp16 := tmp48(47 downto 32);                         -- tmp16 contains the full symbol, left padded with zeros
    io    := u_unsigned('0' & (tmp16(9 downto 0) and msk)); -- io is the 10 bits offset for imaginary part
    io    := io + u_unsigned(lba);        
    tmp16 := shift_right(tmp16, shr);
    ro    := u_unsigned('0' & (tmp16(9 downto 0) and msk));       -- ro is the 10 bits offset for real part
    ro    := ro + u_unsigned(lba);         
    ia    := std_ulogic_vector(io(9 downto 0));       -- add LUT base address
    ra    := std_ulogic_vector(ro(9 downto 0));       -- add LUT base address
    oor   := io(10) or ro(10);
  end procedure compute_lut_addresses;

  function sat(l: u_signed; i: positive) return u_signed is
    constant n: natural := l'length;
    variable lv: u_signed(n - 1 downto 0) := l;
    variable res: u_signed(i - 1 downto 0);
  begin
-- pragma translate_off
    assert i <= n
      report "sat: invalid parameters"
      severity failure;
-- pragma translate_on
    if (or_reduce(lv(n - 1 downto i - 1)) /= '0') and (lv(n - 1) = '0') then -- l > 2^(i-1)-1
      res := (others => '1');
      res(i - 1) := '0'; -- res = 2^(i-1)-1
    elsif (and_reduce(lv(n - 1 downto i - 1)) /= '1') and (lv(n - 1) = '1') then -- l < -2^(i-1)
      res := (others => '0');
      res(i - 1) := '1'; -- res = -2^(i-1)
    else
      res := lv(i - 1 downto 0); -- res = l
    end if;
    return res;
  end function sat;

end package body mapper_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
