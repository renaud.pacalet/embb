--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for incoming requests FIFO.

library ieee;
use ieee.std_logic_1164.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.global.all;
use global_lib.sim_utils.all;

entity requests_fifo_sim is
  generic(depth: positive := 3;
          n: positive := 1000);
end entity requests_fifo_sim;

architecture sim of requests_fifo_sim is

  signal clk, srstn, ce, ack, cmdack: std_ulogic;
  signal req_in, req_out: vci_request_type;
  signal eos: boolean := false;

begin

  rf: entity work.requests_fifo(rtl)
    generic map(depth => depth)
    port map(clk     => clk,
             srstn   => srstn,
             ce      => ce,
             req_in  => req_in,
             req_out => req_out,
             ack     => ack,
             cmdack  => cmdack);
  process
  begin
    clk <= '0';
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
    if eos then
      wait;
    end if;
  end process;

  process
  begin
    srstn  <= '0';
    ce     <= '1';
    ack    <= '0';
    req_in <= vci_request_none;
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    srstn <= '1';
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    for i in 1 to 100 loop
      if cmdack = '1' then
        req_in <= vci_request_rnd;
        req_in.cmdval <= '1';
      end if;
      ack <= std_ulogic_rnd;
      wait until rising_edge(clk);
    end loop;
    ack <= '1';
    for i in 1 to 100 loop
      if cmdack = '1' then
        req_in <= vci_request_rnd;
        req_in.cmdval <= '1';
      end if;
      wait until rising_edge(clk);
    end loop;
    for i in 1 to 100 loop
      if cmdack = '1' then
        req_in <= vci_request_rnd;
      end if;
      wait until rising_edge(clk);
    end loop;
    for i in 1 to n loop
      if cmdack = '1' then
        req_in <= vci_request_rnd;
      end if;
      ack <= std_ulogic_rnd;
      wait until rising_edge(clk);
      progress(i, n);
    end loop;
    eos <= true;
    report "Regression test passed";
    wait;
  end process;

  process(clk)
    variable pipe: vci_request_vector(0 to depth - 1);
    variable cnt: natural range 0 to depth;
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        pipe := (others => vci_request_none);
        cnt  := 0;
      else
        assert (pipe(depth - 1).cmdval = '0' and req_out.cmdval = '0') or pipe(depth - 1) = req_out
          report "Mismatch on output request"
          severity failure;
        assert (cnt = depth and cmdack = '0') or (cnt /= depth and cmdack = '1')
          report "Mismatch on CMDACK"
          severity failure;
        if pipe(depth - 1).cmdval = '1' and ack = '1' then
          pipe := vci_request_none & pipe(0 to depth - 2);
          cnt := cnt - 1;
        end if;
        if cmdack = '1' and req_in.cmdval = '1' then
          pipe(depth - 1 - cnt) := req_in;
          cnt := cnt + 1;
        end if;
      end if;
    end if;
            
  end process;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
