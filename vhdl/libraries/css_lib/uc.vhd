--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Microcontroller toplevel
--*
--* Wrapper around the Free6502 8-bits microncontroller. Read-write accesses
--* target 3 possible destinations: the memory subsystem (MSS), the VCIInterface
--* internal registers (VCI) and the DMA engine internal registers (DMA). The 
--* address map is the following:
--*
--* 0x0000-0xFEFF: MSS
--* 0xFF00-0xFF7F: DMA internal registers
--* 0xFF80-0xFFFF: VCIInterface internal registers
--*
--* The VCIInterface module also provides the maskable and non-maskable
--* interrupts, the reset and a chip enable. Upon read-write requests each of
--* the 3 targets return an active high grant and an out-of-range indicator. The
--* former is used to drive the CS (chip select) input of the 6502 and to freeze
--* it on slow answers. The latter is used to raise the NMI on invalid accesses. A
--* fast and simple mask-based address decoding is used to select the read-write
--* targets, both for requests and responses.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;

use work.css_pkg.all;

entity uc is
-- pragma translate_off
  generic (
    debug:   boolean
  );
-- pragma translate_on
  port(
    clk: in std_ulogic;        -- master clock
    srstn: in std_ulogic;      -- synchronous active low reset
    uca2uc: in uca2uc_type;    -- uca2uc port
    uc2uca: out uc2uca_type;   -- uc2uca port
    ctrl2uc: in ctrl2uc_type;  -- ctrl2uc port
    uc2ctrl: out uc2ctrl_type  -- uc2ctrl port
  );
end entity uc;

architecture rtl of uc is

  signal reset, cs, we, rd: std_ulogic;

begin

  reset <= not (ctrl2uc.srstn and srstn);
  cs <= ctrl2uc.ce and uca2uc.rdy;
  uc2uca.rnw  <= rd;
  uc2uca.en   <= rd or we;

  f6502: entity work.free6502(rtl)
-- pragma translate_off
    generic map(debug => debug)
-- pragma translate_on
    port map(
      clk      => clk,
      reset    => reset,
      irq      => ctrl2uc.irq,
      nmi      => uca2uc.nmi,
      add      => uc2uca.add,
      din      => uca2uc.rdata,
      dout     => uc2uca.wdata,
      dout_oe  => open,
      we       => we,
      rd       => rd,
      cs       => cs,
      sync     => uc2ctrl.sync,
      pc       => uc2ctrl.pc,
      sp       => uc2ctrl.sp,
      a        => uc2ctrl.a, 
      x        => uc2ctrl.x, 
      y        => uc2ctrl.y, 
      c        => uc2ctrl.c, 
      z        => uc2ctrl.z, 
      i        => uc2ctrl.i, 
      d        => uc2ctrl.d, 
      b        => uc2ctrl.b, 
      v        => uc2ctrl.v, 
      n        => uc2ctrl.n); 

  
end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
