--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

entity mssif is
  port (
    clk:           in std_ulogic;               -- master clock
    srstn:         in std_ulogic;               -- synchronous active low reset
    vci_in:        in vci_i2t_type;       -- AVCI target input port
    vci_out:       out vci_t2i_type;     -- AVCI target output port
    mss2vci:       in mss2vci_type;             -- mss2vci port
    vci2mss:       out dma2mss_type            -- vci2mss port
  );
end entity mssif;

architecture rtl of mssif is
  
  signal mss2vci_r: mss2vci_type; -- latched signal from mss
  signal vci2mss_r: dma2mss_type; -- latched signals to mss
  signal vci2mss_c: dma2mss_type; -- signal to mss

  signal cmdack: std_ulogic;  -- command acknowledge
  signal rspval: std_ulogic;  -- response valid
  signal rcempty: std_ulogic;  -- vcicommand/vci2mss register empty
  signal rcempty_r: std_ulogic;  -- vcicommand/vci2mss register empty
  signal s, eop : std_ulogic;
  signal pktid : std_ulogic_vector(vci_p - 1 downto 0);
  signal srcid : std_ulogic_vector(vci_s - 1 downto 0);
  signal trdid : std_ulogic_vector(vci_t - 1 downto 0);

  type stagea_t is record
   en : std_ulogic;
   eop : std_ulogic;
   oor : std_ulogic;
   pktid : std_ulogic_vector(vci_p - 1 downto 0);
   srcid : std_ulogic_vector(vci_s - 1 downto 0);
   trdid : std_ulogic_vector(vci_t - 1 downto 0);
   rnw : std_ulogic;
   rack : std_ulogic;
   gnt : std_ulogic_vector(7 downto 0);
   add : std_ulogic_vector(28 downto 0);
   wdata : std_ulogic_vector(63 downto 0);
   be : std_ulogic_vector(7 downto 0);
  end record;

  signal stagea : stagea_t;

  type stageb_t is record
   en : std_ulogic;
   eop : std_ulogic;
   rnw : std_ulogic;
   rack : std_ulogic;
   gnt : std_ulogic_vector(7 downto 0);
   be : std_ulogic_vector(7 downto 0);
   data : std_ulogic_vector(63 downto 0);
  end record;

  signal stageb : stageb_t;
  
  -- i2t
  signal i2t_c    : vci_i2t_type;  
  -- t2i
  signal t2i_c    : vci_t2i_type;  
  signal t2i_r    : vci_t2i_type;  
begin

  i2t_c <= vci_in;
  vci_out <= t2i_c;

  latch : process(clk)
  begin
    if (clk'event and clk = '1') then
      if srstn = '0' then 
        vci2mss_r <= dma2mss_none; 
         stagea.en <= '0';
         stageb.data <= (others => '0');
         eop <= '0';
         pktid <= (others => '0');
      else

        vci2mss_r <= dma2mss_none; 
        mss2vci_r <= mss2vci;
         
         stagea.en <= '0';
         stagea.oor <= '0';
         stagea.rack <= '0';
         stagea.eop <= eop;
         stagea.pktid <= pktid;
         stagea.srcid <= srcid;
         stagea.trdid <= trdid;
         stageb.en <= stagea.en;
         stageb.rack <= stagea.rack;
         stageb.rnw <= stagea.rnw;
         stageb.eop <= stagea.eop;

         t2i_r <= t2i_c;

         if stagea.rack = '1' and t2i_c.rsp.rspval = '1' then 
           stageb.data <= (others => '0');
         end if;

         if i2t_c.req.cmdval = '1' and t2i_c.cmdack = '1' then 
            eop <= i2t_c.req.eop;
            pktid <= i2t_c.req.pktid;
            trdid <= i2t_c.req.trdid;
            srcid <= i2t_c.req.srcid;
            vci2mss_r.en <= '1';
            vci2mss_r.add <= std_ulogic_vector(resize(shift_right(u_unsigned(i2t_c.req.address), log2_vci_b), 29));
            vci2mss_r.be <= i2t_c.req.be;
            vci2mss_r.wdata <= i2t_c.req.wdata;
            if i2t_c.req.cmd = vci_cmd_read then 
              vci2mss_r.rnw <= '1';
            else
              vci2mss_r.rnw <= '0';
            end if;
          end if;
          
  
           if vci2mss_r.en = '1' then        
             stagea.oor <= mss2vci.oor; 
             stagea.rnw <= vci2mss_r.rnw;
             stagea.gnt <= mss2vci.gnt;
             stagea.be  <= vci2mss_r.be;
             stagea.en <= '1';
             if vci2mss_r.be = mss2vci.gnt then
               stagea.rack <= '1';
             end if;
          end if;
          

          if vci2mss_r.en = '1' then
	          if (vci2mss_r.be /= mss2vci.gnt) then 
              vci2mss_r <= vci2mss_r;
              eop <= eop;
              trdid <= trdid;
              srcid <= srcid;
              pktid <= pktid;
	            vci2mss_r.be <= vci2mss_r.be and not (mss2vci.gnt);
            end if; 
          end if;
       
          s <= '0';

          if t2i_c.rsp.rspval = '1' and i2t_c.rspack = '0' then
            s <= '1';
            vci2mss_r <= vci2mss_r;
          end if;

      end if;
    end if;
  end process;
  
  vci2mss <= vci2mss_r;

  cmdack_pr : process (t2i_c.rsp.rspval, i2t_c, mss2vci.gnt, vci2mss_r)
  begin
    t2i_c.cmdack <= '1';
    if i2t_c.req.cmdval = '1' and vci2mss_r.en = '1' and vci2mss_r.be /= mss2vci.gnt then 
      t2i_c.cmdack <= '0';
    end if;
    if t2i_c.rsp.rspval = '1' and i2t_c.rspack = '0' then 
      t2i_c.cmdack <= '0';
    end if;
  end process;


  rsp_pr : process (s, stagea, mss2vci, t2i_r)
  begin

    t2i_c.rsp.rerror <= (others => '0');

    t2i_c.rsp.rspval <= stagea.en and (stagea.rack or stagea.oor);
    t2i_c.rsp.rerror(0) <= stagea.oor;
    t2i_c.rsp.rdata <= mss2vci.rdata;
    t2i_c.rsp.reop <= stagea.eop;
    t2i_c.rsp.rpktid <= stagea.pktid;
    t2i_c.rsp.rsrcid <= stagea.srcid;
    t2i_c.rsp.rtrdid <= stagea.trdid;

    if s = '1' then  
     t2i_c.rsp <= t2i_r.rsp;
    end if;
   end process;
  

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
