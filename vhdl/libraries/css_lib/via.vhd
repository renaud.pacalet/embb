--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Target VCI arbiter
--*
--* VIA is the arbiter between the input requests FIFO (REQ), the output responses FIFO (RSP), CTRL (the module containing control and status
--* registers) and MSS. It decodes the addresses in the VCI requests comming from REQ and routes the request to either MSS or CTRL. In case the request carries
--* an invalid address (neither in CTRL nor MSS addresses spaces), it generates a response with the error flag set. It also arbitrates between responses comming
--* from MSS, CTRL or itself and sends them to RSP. VCI requests to CTRL are handled in one cycle only (when acknowledged) but requests to MSS can be split in
--* several consecutive MSS accesses when all target bytes cannot be accessed in the same clock cycle. Moreover, the CTRL read latency is 0 clock cycles while
--* it is 4 for MSS (2 MSS internal pipeline barriers plus two external for speed optimization).

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

use work.css_pkg.all;

entity via is
  generic(rsp_depth: positive; --* Depth of responses FIFO
          n0:        positive; --* Number of input pipeline registers of MSS
          n1:        positive; --* Number of output pipeline registers of MSS
          with_mss:  boolean); --* With or without MSS
  port(clk:      in  std_ulogic;
       srstn:    in  std_ulogic; --* Active low, synchronous, reset
       ce:       in  std_ulogic; --* Chip enable
       ctrl2via: in  ctrl2via_type; --* Inputs from CTRL
       via2ctrl: out via2ctrl_type; --* Outputs to CTRL
       req2via:  in  vci_request_type; --* Inputs from input requests FIFO
       via2req:  out std_ulogic;       --* Acknowloedge to input requests FIFO
       rsp2via:  in  natural range 0 to rsp_depth; --* Input from output responses FIFO (number of free places in FIFO)
       via2rsp:  out vci_response_type;            --* Outputs to output responses FIFO
       mss2via:  in  mss2vci_type;  --* Inputs from MSS
       via2mss:  out dma2mss_type); --* Output to MSS
end entity via;

architecture rtl of via is

  --* MSS (n0+n1 internal pipeline barriers) is wrapped inside a 2+n0+n1 stages pipeline.
  --* Input stage stores the input requests to MSS, the End-Of-Packet (EOP) flag and the 3 IDs of the-corresponding VCI request.
  type mss_pipe_input_stage is record
    en:     std_ulogic;
    rnw:    std_ulogic;
    be:     word64_be_type;
    add:    word64_address_type;
    wdata:  word64;
    reop:   std_ulogic;
    rsrcid: vci_srcid_type;
    rtrdid: vci_trdid_type;
    rpktid: vci_pktid_type;
  end record;

  --* The n0+n1 internal stages are identical. They correspond to the MSS internal barriers. They comprise an enable flag indicating whether a MSS access is active
  --* at this pipeline position, an 8-bits byte enables used in output stage to retrieve actually read bytes from the 64-bits MSS output, a done flag indicating the
  --* last MSS access of a VCI request, an error flag for out-of-MSS-range accesses and the VCI EOP flag and IDs.
  type mss_pipe_internal_stage is record
    en:     std_ulogic;
    be:     word64_be_type;
    done:   std_ulogic;
    rerror: std_ulogic;
    reop:   std_ulogic;
    rsrcid: vci_srcid_type;
    rtrdid: vci_trdid_type;
    rpktid: vci_pktid_type;
  end record;
  type mss_pipe_internal_stage_vector is array(0 to n0 + n1 - 1) of mss_pipe_internal_stage; -- n0+n1 internal stages.

  --* The output stage is used to assemble the data read in MSS (up to 8 different accesses can be necessary to read a full 8 bytes word). It also stores the
  --* done flag indicating the last MSS access of a VCI request, an error flag for out-of-MSS-range accesses and the VCI EOP flag and IDs.
  type mss_pipe_output_stage is record
    done:   std_ulogic;
    rdata:  word64;
    rerror: std_ulogic;
    reop:   std_ulogic;
    rsrcid: vci_srcid_type;
    rtrdid: vci_trdid_type;
    rpktid: vci_pktid_type;
  end record;

  type mss_pipe_type is record
    s0: mss_pipe_input_stage;
    s1: mss_pipe_internal_stage_vector;
    s2: mss_pipe_output_stage;
  end record;

  signal via2req_local:  std_ulogic;
  signal via2ctrl_local: via2ctrl_type;
  signal via2rsp_local:  vci_response_type;
  --* Counter of active requests. Shall always be less than rsp2via (the number of free places in responses FIFO).
  signal req_cnt:        natural range 0 to rsp_depth;
  signal mss_pipe:       mss_pipe_type;

  --* Each bit of ungnted_be indicates an enabled byte in a MSS access that is not granted and must thus be enabled again in the next clock cycle.
  signal ungnted_be: word64_be_type;
  --* End of MSS request
  signal eo_mss_req: boolean;

  --* A VCI request in the registers is a VCI read or write request which address matches the registers addresses space.
  function is_reg_req(r: vci_request_type) return boolean is
  begin
    return r.cmdval = '1' and (r.cmd = vci_cmd_read or r.cmd = vci_cmd_write) and hst_is_in_regs(r.address);
  end is_reg_req;

  --* A VCI request in MSS is a VCI read or write request which address matches the MSS addresses space.
  function is_mss_req(r: vci_request_type) return boolean is
  begin
    return r.cmdval = '1' and (r.cmd = vci_cmd_read or r.cmd = vci_cmd_write) and hst_is_in_mss(r.address);
  end is_mss_req;

  --* An erroneous VCI request in a VCI request which is neither for the registers nor the MSS.
  function is_err_req(r: vci_request_type) return boolean is
    variable res: boolean;
  begin
    if with_mss then
      res := r.cmdval = '1' and (not (is_reg_req(r) or is_mss_req(r)));
    else
      res := r.cmdval = '1' and (not (is_reg_req(r)));
    end if;
    return res;
  end is_err_req;

begin

  --* Ungranted bytes are enabled bytes in an MSS access that are not granted by MSS.
  ungnted_be <= mss_pipe.s0.be and (not mss2via.gnt) when with_mss else
                word64_be_none;
  --* A MSS access ends when MSS signals an out-of-range address error or when all enabled bytes are granted
  eo_mss_req <= mss2via.oor = '1' or or_reduce(ungnted_be) = '0' when with_mss else
                false;

  --* Forward incomming request to CTRL
  via2ctrl_p: process(via2req_local, req2via)
  begin
    via2ctrl_local <= via2ctrl_none; -- By default do not request CTRL
    if is_reg_req(req2via) and via2req_local = '1' then -- If request for registers and acknowledged
      -- Send read/write request to CTRL
      via2ctrl_local.en <= '1';
      if req2via.cmd = vci_cmd_read then
        via2ctrl_local.rnw <= '1';
      else
        via2ctrl_local.rnw <= '0';
      end if;
      via2ctrl_local.be    <= req2via.be;
      via2ctrl_local.add   <= hst_add2idx(req2via.address);
      via2ctrl_local.wdata <= req2via.wdata;
    end if;
  end process via2ctrl_p;
  via2ctrl <= via2ctrl_local;

  --* An incomming request from REQ is acknowledged unless:
  --* - It is a CTRL request but CTRL does not acknowledge
  --* - It is a CTRL or erroneous request but an MSS response is ready to send
  --* - It is an MSS request but there is a ongoing one
  --* - And there are not enough free places in the responses FIFO
  via2req_p: process(ctrl2via, req2via, mss_pipe, eo_mss_req, req_cnt, rsp2via)
  begin
    via2req_local <= '1'; -- By default, acknowledge requests from requests FIFO
    if is_reg_req(req2via) and ctrl2via.ack = '0' then
      via2req_local <= '0';
    end if;
    if with_mss and (is_reg_req(req2via) or is_err_req(req2via)) and mss_pipe.s2.done = '1' then -- Not a MSS request and MSS response ready
      via2req_local <= '0';
    end if;
    if with_mss and is_mss_req(req2via) and mss_pipe.s0.en = '1' and (not eo_mss_req) then -- Request to MSS but there is already an ongoing one
      via2req_local <= '0';
    end if;
    if req_cnt >= rsp2via then -- Never more active requests than free places in responses FIFO
      via2req_local <= '0';
    end if;
  end process via2req_p;
  via2req <= via2req_local;

  --* Send responses to the responses FIFO
  via2rsp_p: process(mss_pipe.s2, via2ctrl_local, ctrl2via, req2via, via2req_local)
  begin
    via2rsp_local <= vci_response_none; -- By default do not send a response
    if with_mss and mss_pipe.s2.done = '1' then -- If there is a MSS response at the output of the MSS pipeline, send it to RSP
      via2rsp_local.rspval    <= '1';
      via2rsp_local.rdata     <= mss_pipe.s2.rdata;
      via2rsp_local.rerror(0) <= mss_pipe.s2.rerror;
      via2rsp_local.reop      <= mss_pipe.s2.reop;
      via2rsp_local.rsrcid    <= mss_pipe.s2.rsrcid;
      via2rsp_local.rtrdid    <= mss_pipe.s2.rtrdid;
      via2rsp_local.rpktid    <= mss_pipe.s2.rpktid;
    elsif via2ctrl_local.en = '1' then -- Else, if there is an active request to CTRL (response in the same clock cycle), send its response to RSP
      via2rsp_local.rspval    <= ctrl2via.ack;
      via2rsp_local.rdata     <= ctrl2via.rdata;
      via2rsp_local.rerror(0) <= ctrl2via.oor;
      via2rsp_local.reop      <= req2via.eop;
      via2rsp_local.rsrcid    <= req2via.srcid;
      via2rsp_local.rtrdid    <= req2via.trdid;
      via2rsp_local.rpktid    <= req2via.pktid;
    elsif is_err_req(req2via) and via2req_local = '1' then -- Else, if there is an erroneous request, create and send an error response to RSP
      via2rsp_local.rspval    <= '1';
      via2rsp_local.rerror(0) <= '1';
      via2rsp_local.reop      <= req2via.eop;
      via2rsp_local.rsrcid    <= req2via.srcid;
      via2rsp_local.rtrdid    <= req2via.trdid;
      via2rsp_local.rpktid    <= req2via.pktid;
    end if;
  end process via2rsp_p;
  via2rsp <= via2rsp_local;

  --* Pipeline around MSS. 2+n0+n1 barriers.
  mss_pipe_p: process(clk)
  begin
    if rising_edge(clk) then
      if not with_mss or srstn = '0' then -- reset active
        mss_pipe.s0 <= (en => '0', rnw => '0', be => (others => '0'), add => (others => '0'), wdata => (others => '0'), reop => '0',
                         rsrcid => (others => '0'), rtrdid => (others => '0'), rpktid => (others => '0'));
        mss_pipe.s1 <= (others => (en => '0', be => (others => '0'), done => '0', rerror => '0', reop => '0', rsrcid => (others => '0'),
                       rtrdid => (others => '0'), rpktid => (others => '0')));
        mss_pipe.s2 <= (done => '0', rdata => (others => '0'), rerror => '0', reop => '0', rsrcid => (others => '0'), rtrdid => (others => '0'),
                       rpktid => (others => '0'));
      elsif with_mss and ce = '1' then -- chip enabled
        if is_mss_req(req2via) and via2req_local = '1' then -- Acknowledged request to MSS. Translate VCI request into read/write access to MSS
          mss_pipe.s0.en <= '1';
          if req2via.cmd = vci_cmd_read then
            mss_pipe.s0.rnw <= '1';
          else
            mss_pipe.s0.rnw   <= '0';
            mss_pipe.s0.wdata <= req2via.wdata; -- Sample write data only upon write access (power saving)
          end if;
          mss_pipe.s0.be     <= req2via.be;
          mss_pipe.s0.add    <= req2via.address(31 downto 3) and (not hst_mss_mask(31 downto 3));
          mss_pipe.s0.reop   <= req2via.eop;
          mss_pipe.s0.rsrcid <= req2via.srcid;
          mss_pipe.s0.rtrdid <= req2via.trdid;
          mss_pipe.s0.rpktid <= req2via.pktid;
        elsif mss_pipe.s0.en = '1' then -- There is an ongoing MSS access
          if eo_mss_req then -- If it terminates (all enabled bytes granted or out-of-range address error)
            mss_pipe.s0.en <= '0'; -- Deassert enable
          else -- The VCI request will continue with another MSS access on next clock cycle
            mss_pipe.s0.be <= ungnted_be; -- Clear byte enables of granted bytes; next access is on not-yet accessed bytes only until all bytes are done
          end if;
        end if;
        mss_pipe.s1(0).en     <= '0'; -- By default no MSS access in first internal stage
        mss_pipe.s1(0).done   <= '0'; -- By default no last MSS access of a VCI request in first internal stage
        if mss_pipe.s0.en = '1' then  -- If there is a MSS access at the input of MSS
          mss_pipe.s1(0).en   <= '1'; -- Advance in first internal stage
          if mss_pipe.s0.rnw = '0' then -- If write access
            mss_pipe.s1(0).be <= (others => '0'); -- Clear byte enables (no need of them to build output rdata at the end of MSS pipeline)
          else -- Read access
               -- Propagate read and granted bytes. Used to extract actually read bytes at the end of MSS pipeline)
            mss_pipe.s1(0).be <= mss_pipe.s0.be and (not ungnted_be);
          end if;
          if eo_mss_req then -- If last MSS access of VCI request
            mss_pipe.s1(0).done <= '1'; -- Set done flag
          end if;
          mss_pipe.s1(0).rerror <= mss2via.oor; -- Out of range error
          -- Just copy the other fields from input stage
          mss_pipe.s1(0).reop   <= mss_pipe.s0.reop;
          mss_pipe.s1(0).rsrcid <= mss_pipe.s0.rsrcid;
          mss_pipe.s1(0).rtrdid <= mss_pipe.s0.rtrdid;
          mss_pipe.s1(0).rpktid <= mss_pipe.s0.rpktid;
        end if;
        for i in 1 to n0 + n1 - 1 loop -- For all internal stages but the first one
          mss_pipe.s1(i).en   <= '0'; -- By default no MSS access in ith stage
          mss_pipe.s1(i).done <= '0'; -- By default no last MSS access of a VCI request in ith stage
          if mss_pipe.s1(i - 1).en = '1' then -- If there is a MSS access in previous stage (test for power saving)
            mss_pipe.s1(i) <= mss_pipe.s1(i - 1); -- Advance in ith stage
          end if;
        end loop;
        mss_pipe.s2.done <= '0'; -- By default no last MSS access of a VCI request in output stage
        if mss_pipe.s1(n0 + n1 - 1).en = '1' then -- If there is a MSS access in last internal stage (test for power saving)
          mss_pipe.s2.done <= mss_pipe.s1(n0 + n1 - 1).done; -- Advance in output stage
          for i in 0 to 7 loop -- For all bytes
            if mss_pipe.s1(n0 + n1 - 1).be(i) = '1' then -- If byte enabled, update corresponding mss_pipe.s2.rdata byte with MSS output
              mss_pipe.s2.rdata(8 * i + 7 downto 8 * i) <= mss2via.rdata(8 * i + 7 downto 8 * i);
            end if;
          end loop;
            -- Just copy the other fields from last internal stage
          mss_pipe.s2.rerror <= mss_pipe.s1(n0 + n1 - 1).rerror;
          mss_pipe.s2.reop   <= mss_pipe.s1(n0 + n1 - 1).reop;
          mss_pipe.s2.rsrcid <= mss_pipe.s1(n0 + n1 - 1).rsrcid;
          mss_pipe.s2.rtrdid <= mss_pipe.s1(n0 + n1 - 1).rtrdid;
          mss_pipe.s2.rpktid <= mss_pipe.s1(n0 + n1 - 1).rpktid;
        end if;
      end if;
    end if;
  end process mss_pipe_p;
  --* Inputs of MSS are taken from input stage of MSS pipeline
  via2mss.en    <= mss_pipe.s0.en;
  via2mss.rnw   <= mss_pipe.s0.rnw;
  via2mss.be    <= mss_pipe.s0.be;
  via2mss.add   <= mss_pipe.s0.add;
  via2mss.wdata <= mss_pipe.s0.wdata;

  --* REQ_CNT is the number of VCI requests currently acknowledged by CSS and for which a response has not yet been stored in the responses FIFO. RSP2VIA is the
  --* number of free places in responses FIFO. Incoming requests are acknowledged only if REQ_CNT < RSP2VIA, which guarantees that there is enough room in
  --* responses FIFO to store the corresponding response.
  req_cnt_p: process(clk)
    variable ri, ro: boolean;
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        req_cnt <= 0;
      elsif ce = '1' then
        ri := false;
        ro := false;
        if via2rsp_local.rspval = '1' then -- If outgoing response is sent to RSP
          ro := true;
        end if;
        if req2via.cmdval = '1' and via2req_local = '1' then -- If incomming request is acknowledged
          ri := true;
        end if;
        if ri and (not ro) then -- One input, no output
          req_cnt <= req_cnt + 1; -- Increment counter
        elsif (not ri) and ro then -- No input, one output
          req_cnt <= req_cnt - 1; -- Decrement counter
        end if;
      end if;
    end if;
  end process req_cnt_p;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
