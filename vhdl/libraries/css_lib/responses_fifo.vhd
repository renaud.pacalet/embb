--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Outgoing responses FIFO.
--*
--*  Responses FIFO located between the internals of CSS and the AVCI crossbar target port. Up to DEPTH incoming responses are stored in the FIFO. The
--* ACK acknowledge signal sent back to the internals of CSS is asserted by default. It is de-asserted only when the FIFO is full. On the local AVCI crossbar side,
--* the first received and not yet transmitted response, if any, is output on RSP_OUT. When acknowledged (RSPACK input asserted) it is removed from the FIFO and the
--* next response, if any, is output on RSP_OUT. The most important criteria for this design is the delay between internal registers and RSP_OUT. As a
--* consequence, RSP_OUT is directly wired to the first stage of the FIFO (RESPONSES(0)). The incomming responses are stored at the first available location
--* starting from this same stage. To avoid long combinatorial pathes, the ACK signal does not depend on RSPACK: it is asserted if and only if there is a free
--* place in the FIFO, not when the FIFO is full and the currently outgoing response is acknowledged.

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

entity responses_fifo is
  generic(depth: positive := 6);
  port(
    clk:     in  std_ulogic;  --* master clock
    srstn:   in  std_ulogic;  --* synchronous active low reset
    ce:      in  std_ulogic;  --* chip enable
    rsp_in:  in  vci_response_type; --* AVCI incoming responses (from local DSP unit)
    rsp_out: out vci_response_type; --* AVCI outgoing responses (to host system)
    rspack:  in  std_ulogic;        --* Acknowledge of current outgoing response
    ack:     out natural range 0 to depth); --* Number of free places in FIFO (to local DSP unit)

end entity responses_fifo;

architecture rtl of responses_fifo is

  --* responses FIFO; inputs at DEPTH - 1, output at 0
  signal responses: vci_response_vector(0 to depth - 1);
  signal ack_local: natural range 0 to depth;

begin

  process(clk)
    variable responsesv: vci_response_vector(0 to depth - 1);
    variable valv: std_ulogic;
    variable ri, ro: boolean;
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        for i in 0 to depth - 1 loop
          responsesv(i) := vci_response_none;
        end loop;
        ack_local <= depth;
      elsif ce = '1' then
        responsesv := responses;
        valv       := rsp_in.rspval;
        ri         := false;
        ro         := false;
        if rspack = '1' and responsesv(0).rspval = '1' then -- Current outgoing response acknowledged
          for i in 0 to depth - 2 loop -- Move FIFO ahead but not inactive places (to save power)
            if responsesv(i + 1).rspval = '1' then
              responsesv(i) := responsesv(i + 1);
            end if;
            responsesv(i).rspval := responsesv(i + 1).rspval;
          end loop;
          responsesv(depth - 1).rspval := '0';
          ro := true;
        end if;
        for i in 0 to depth - 1 loop
          if valv = '1' and ack_local /= 0 and responsesv(i).rspval = '0' then -- Can accept incoming response
            responsesv(i)  := rsp_in; -- Store incoming response
            valv           := '0';    -- Clear incoming response
            ri             := true;
          end if;
        end loop;
        if ri and not ro then
          ack_local <= ack_local - 1;
        elsif not ri and ro then
          ack_local <= ack_local + 1;
        end if;
      end if;
      responses <= responsesv;
    end if;
  end process;

  ack     <= ack_local;
  rsp_out <= responses(0);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
