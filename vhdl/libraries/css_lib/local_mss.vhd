--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

library memories_lib;
use memories_lib.ram_pkg.all;

entity local_mss is
  generic (
    n0 : positive := 1;
    n1 : positive := 1);
  port(clk: in std_ulogic;
       run : in std_ulogic;
       -- Control subsystem interface (VCIInterface, DMA ans UC)
       mssin: in dma2mss_type;
       mssout: out mss2dma_type);
end entity local_mss;

architecture rtl of local_mss is
  
  type din_type is array(0 to 1) of in_port_1kx32;
  signal din, din0, din1: din_type;
  
  type din_p_type is array(integer range <>) of din_type;
  signal din_p: din_p_type(n0 - 1 downto 0);

  type dout_type is array(0 to 1) of word32;
  signal dout, dout0, dout1: dout_type;

  type dout_p_type is array(integer range <>) of dout_type;
  signal dout_p: dout_p_type(n1 - 1 downto 0);

  type gnt_p_type is array(integer range <>) of std_ulogic_vector(7 downto 0);

  signal gnt_p: gnt_p_type(n0 + n1 downto 0);

  signal gnt : std_ulogic_vector(7 downto 0);


  begin

 process(clk)

   variable rndv : std_ulogic;

   begin

   if (clk'event and clk = '1') then

     rndv := std_ulogic_rnd;

     if rndv = '1' then 
       gnt <= X"FF";
     else
       gnt <= std_ulogic_vector_rnd(8);
     end if;

   end if;
 end process;

  arbiter: process(mssin, gnt, run)

    variable add: std_ulogic_vector(15 downto 0);
    variable din_v: din_type;
    variable gntv : std_ulogic_vector(7 downto 0);

  begin

    din_v(0) := in_port_1kx32_default;
    din_v(1) := in_port_1kx32_default;

    gntv := X"00";
    mssout.oor <= '0';

    if mssin.en = '1' then 
      add(9 downto 0) := mssin.add(9 downto 0); 
      if u_unsigned(mssin.add) >= 16#400# then 
	 mssout.oor <= '1';
      else
      -- Requests on DI
        for i in 0 to 1 loop
          -- Requests on DI[0]
          din_v(i).en    := '1';
          din_v(i).we    := (others => not mssin.rnw);
          din_v(i).add   := add(9 downto 0);
          din_v(i).wdata := mssin.wdata(63 - i * 32 downto 32 - i * 32);
          din_v(i).we    := din_v(i).we and mssin.be(7 - 4 * i downto 4 - 4 * i);
          if run = '0' then  
            gntv := (X"FF" and mssin.be);
          else
            gntv := (gnt and mssin.be);
          end if;
        end loop;
      end if;
    end if;

    mssout.gnt <= gntv;
    gnt_p(0) <= gntv;
    din   <= din_v;
    
  end process arbiter;

  dout <= dout0 when n1 = 1 else dout1;
  din0 <= din when n0 = 1 else din1;

  -- The muxes process implements the multiplexing of block RAM outputs
  muxes: process(dout, gnt_p(n0 + n1))
  begin
    mssout.rdata <= (others => '0');
    for i in 0 to 3 loop
      if gnt_p(n0 + n1)(7 - i)  = '1' then
        mssout.rdata(63 - i * 8 downto 56 - i * 8) <= dout(0)(31 - i * 8 downto 24 - i * 8);
      end if;
      if gnt_p(n0 + n1)(3 - i)  = '1' then
        mssout.rdata(31 - i * 8 downto 24 - i * 8) <= dout(1)(31 - i * 8 downto 24 - i * 8);
      end if;
    end loop;
  end process muxes;
  
  pipe0 : if n0 /= 1 generate
    p0: process(clk)
    begin
      if (clk'event and clk = '1') then
        din_p(0) <= din;
        for i in 0 to n0 - 2 loop
          din_p(i + 1) <= din_p(i);
        end loop;
      end if;
    end process p0;
    din1 <= din_p(n0 - 2);
  end generate;
 
  pipe1 : if n1 /= 1 generate
    p1: process(clk)
    begin
      if (clk'event and clk = '1') then
        dout_p(0) <= dout0;
        for i in 0 to n1 - 2 loop
          dout_p(i + 1) <= dout_p(i);
        end loop;
      end if;
    end process p1;
    dout1 <= dout_p(n1 - 2);
  end generate;


  gnt_pipe : for i in 0 to (n0 + n1 - 1) generate
    dd: process(clk)
    begin
      if (clk'event and clk = '1') then
          gnt_p(i + 1) <= gnt_p(i);
      end if;
    end process dd;
  end generate;

  -------------------------------------------------------
  ------------------ BLOCK RAM INSTANCES ----------------
  -------------------------------------------------------

  ram_dig: for n in 0 to 1 generate
    ram_di: entity memories_lib.spram(arc)
     generic map(registered => true,
                 na         => 10,
                 nb         => 4)
      port map(clk => clk,
               en  => din0(n).en,
               we  => din0(n).we,
               add => din0(n).add,
               din => din0(n).wdata,
               dout=> dout0(n));
  end generate ram_dig;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
