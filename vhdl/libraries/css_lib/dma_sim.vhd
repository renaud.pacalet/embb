--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library memories_lib;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;
use global_lib.sim_utils.all;
use global_lib.css_emulator_pkg.all;

use work.dma_pkg.all;
use work.css_pkg.all;

entity dma_sim is
  generic(n: natural := 1000);
end entity dma_sim;

architecture arc_sim of dma_sim is

  constant lsize : natural := 13;
  signal clk: std_ulogic;
  signal srstn: std_ulogic;
  signal vci_init_out: vci_i2t_type;
  signal vci_init_in:  vci_t2i_type;
  signal ctrl2dma : ctrl2dma_type;
  signal dma2ctrl : dma2ctrl_type;
  signal mss2dma, local_mss_out, ref_mss_out: mss2dma_type;
  signal dma2mss, local_mss_in, sim2local, ref_mss_in: dma2mss_type;
  signal mss2vci, ext_mss_out: mss2vci_type;
  signal vci2mss, ext_mss_in, sim2ext: dma2mss_type;
  signal eos: boolean;
  signal shiften : std_ulogic;
  signal left : std_ulogic;
  signal run : std_ulogic;
  signal size : natural;
  signal gnt_ext: std_ulogic_vector(1 downto 0);
  signal gnt_ref: std_ulogic_vector(1 downto 0);
  constant n0 : positive := 1;
  constant n1 : positive := 1;


  signal gnt_local: std_ulogic_vector(n0 + n1 - 1 downto 0);

  procedure init_mss(variable ctrl : in ctrl2dma_type;
          signal clk: in std_ulogic;
          signal ref_mss_in : out dma2mss_type;
          signal ref_mss_out: in mss2dma_type;
          signal sim2local : out dma2mss_type;
          signal local_mss_out : in mss2dma_type;
          signal sim2ext : out dma2mss_type;
          signal ext_mss_out : in mss2dma_type;
          signal shiften : out std_ulogic;
          signal left : out std_ulogic;
          signal size : out natural) is

      variable src : natural := to_integer(shift_right(u_unsigned(ctrl.src), 3));
      variable dst : natural := to_integer(shift_right(u_unsigned(ctrl.dst), 3));
      variable data : std_ulogic_vector(63 downto 0);
      variable wtmp : u_unsigned(31 downto 0):= shift_right(u_unsigned(ctrl.dst) + u_unsigned(ctrl.lenm1), 3);
      variable rtmp : u_unsigned(31 downto 0):= shift_right(u_unsigned(ctrl.src) + u_unsigned(ctrl.lenm1), 3);
      variable w64_dst : integer := to_integer(wtmp - shift_right(u_unsigned(ctrl.dst), 3));
      variable w64_src : integer := to_integer(rtmp - shift_right(u_unsigned(ctrl.src), 3));
              -- Byte enable 
      variable first_ref_be : std_ulogic_vector(7 downto 0);
      variable last_ref_be : std_ulogic_vector(7 downto 0);
      variable isrc : std_ulogic_vector(2 downto 0) := (others => '0');

    begin 
       
      isrc := ctrl.src(2 downto 0);

      -- byte enable
      if ctrl.dst(2 downto 0) = "000" then 
        first_ref_be := X"00";
      else 
        first_ref_be := set_be(ctrl.dst(2 downto 0), '1');
      end if;
      if u_unsigned(ctrl.dst(2 downto 0)) + u_unsigned(ctrl.lenm1(2 downto 0)) + 1 = "000" then  
        last_ref_be := X"00";
      else
        last_ref_be := set_be(std_ulogic_vector(u_unsigned(ctrl.dst(2 downto 0)) + u_unsigned(ctrl.lenm1(2 downto 0)) + 1), '0');
      end if;

     --initialize source memory and reference memory 
      if ctrl.cs = '1' then
        isrc := (others => '0'); 
        for i in 0 to w64_src loop
          data := ctrl.cst;
          write (std_ulogic_vector(to_unsigned(i, 29)) , data, X"FF", clk, ref_mss_out, ref_mss_in); 
        end loop;
      elsif ctrl.ls = '1' then 
        for i in 0 to w64_src loop
          data := std_ulogic_vector_rnd(64);
          write(std_ulogic_vector(to_unsigned(src + i, 29)), data, X"FF", clk, local_mss_out, sim2local); 
          write(std_ulogic_vector(to_unsigned(i, 29)), data, X"FF", clk, ref_mss_out, ref_mss_in); 
        end loop;
      else
        for i in 0 to w64_src loop
          data := std_ulogic_vector_rnd(64);
          write(std_ulogic_vector(to_unsigned(src + i, 29)), data, X"FF", clk, ext_mss_out, sim2ext); 
          write(std_ulogic_vector(to_unsigned(i, 29)), data, X"FF", clk, ref_mss_out, ref_mss_in); 
        end loop;
      end if;

      -- shift reference memory  
      if ctrl.dst(2 downto 0) /= isrc then 
        shiften <= '1';
        if ctrl.dst(2 downto 0) > isrc then 
          left <= '0';
          size <=  8 * to_integer(u_unsigned(ctrl.dst(2 downto 0)) - u_unsigned(isrc)); 
        else  
          left <= '1';
          size <= 8 * to_integer(u_unsigned(isrc) - u_unsigned(ctrl.dst(2 downto 0))); 
        end if;
      end if;
      wait until rising_edge(clk);
      shiften <= '0';

     --initialize destination memory and reference memory 
      if ctrl.ld = '1' then 
        for i in 0 to w64_dst loop
          data := std_ulogic_vector_rnd(64);
          write(std_ulogic_vector(to_unsigned(dst + i, 29)), data, X"FF", clk, local_mss_out, sim2local);
          if w64_dst = 0 then 
            write(std_ulogic_vector(to_unsigned(i, 29)), data, first_ref_be or last_ref_be, clk, ref_mss_out, ref_mss_in); 
          elsif i = 0 then 
            write(std_ulogic_vector(to_unsigned(i, 29)), data, first_ref_be, clk, ref_mss_out, ref_mss_in); 
          elsif i = w64_dst then 
            write(std_ulogic_vector(to_unsigned(i, 29)), data, last_ref_be, clk, ref_mss_out, ref_mss_in); 
          end if;
        end loop;
      else
        for i in 0 to w64_dst loop
          data := std_ulogic_vector_rnd(64);
          write(std_ulogic_vector(to_unsigned(dst + i, 29)), data, X"FF", clk, ext_mss_out, sim2ext); 
          if w64_dst = 0 then 
            write(std_ulogic_vector(to_unsigned(i, 29)), data, first_ref_be or last_ref_be, clk, ref_mss_out, ref_mss_in); 
          elsif i = 0 then 
            write(std_ulogic_vector(to_unsigned(i, 29)), data, first_ref_be, clk,  ref_mss_out, ref_mss_in); 
          elsif i = w64_dst then 
            write(std_ulogic_vector(to_unsigned(i, 29)), data, last_ref_be, clk, ref_mss_out, ref_mss_in); 
          end if;
        end loop;
      end if;
   

    end procedure;
  
  procedure check_mss(variable ctrl : in ctrl2dma_type;
          signal clk: in std_ulogic;
          signal ref_mss_in : out dma2mss_type;
          signal ref_mss_out: in mss2dma_type;
          signal sim2local : out dma2mss_type;
          signal local_mss_out : in mss2dma_type;
          signal sim2ext : out dma2mss_type;
          signal ext_mss_out : in mss2dma_type) is

      variable dst : natural := to_integer(shift_right(u_unsigned(ctrl.dst), 3));
      variable data, refdata : std_ulogic_vector(63 downto 0);
      variable wtmp : u_unsigned(31 downto 0):= shift_right(u_unsigned(ctrl.dst) + u_unsigned(ctrl.lenm1), 3);
      variable w64_dst : integer := to_integer(wtmp - shift_right(u_unsigned(ctrl.dst), 3));
      variable l: line;

    begin 
     
      if ctrl.ld = '1' then
       -- local destinaton 
        for i in 0 to w64_dst loop
          read(std_ulogic_vector(to_unsigned(dst + i, 29)), clk, local_mss_out, sim2local);
           
          wait until rising_edge(clk) and gnt_local(n0 + n1 - 1) = '1'; 
          data := local_mss_out.rdata;
          read(std_ulogic_vector(to_unsigned(i, 29)), clk, ref_mss_out, ref_mss_in); 
          wait until rising_edge(clk) and gnt_ref(1) = '1'; 
          refdata := ref_mss_out.rdata;
          if refdata /= data then
            assert false
            report "DMA: Read value does not match expected one got " &
            vec2hexstr(data) & ", expected " &
            vec2hexstr(refdata) &
            " on data " & integer'image(i) & "  with  : lenm1 : " & vec2hexstr(ctrl.lenm1) &            
            " src : " & vec2hexstr(ctrl.src) &            
            " dst : " & vec2hexstr(ctrl.dst) &            
            " ls : " & std_ulogic'image(ctrl.ls) &            
            " ld : " & std_ulogic'image(ctrl.ld) &            
            " ce : " & std_ulogic'image(ctrl.ce) &            
            " cs : " & std_ulogic'image(ctrl.cs) & "."           
            severity failure;
          end if;
        end loop;
      else
       -- ext destinaton 
        for i in 0 to w64_dst loop
          read(std_ulogic_vector(to_unsigned(dst + i, 29)), clk, ext_mss_out, sim2ext); 
          wait until rising_edge(clk) and gnt_ext(1) = '1'; 
          data := ext_mss_out.rdata;
          read(std_ulogic_vector(to_unsigned(i, 29)), clk, ref_mss_out, ref_mss_in); 
          wait until rising_edge(clk) and gnt_ref(1) = '1'; 
          refdata := ref_mss_out.rdata;
          if refdata /= data then
            assert false
            report "DMA: Read value does not match expected one got " &
            vec2hexstr(data) & ", expected " &
            vec2hexstr(refdata) &
            " on data " & integer'image(i) & "  with lenm1 : " & vec2hexstr(ctrl.lenm1) &            
            " src : " & vec2hexstr(ctrl.src) &            
            " dst : " & vec2hexstr(ctrl.dst) &            
            " ls : " & std_ulogic'image(ctrl.ls) &            
            " ld : " & std_ulogic'image(ctrl.ld) &            
            " ce : " & std_ulogic'image(ctrl.ce) &            
            " cs : " & std_ulogic'image(ctrl.cs) & "."           
            severity failure;
          end if;
        end loop;
      end if;


    end procedure;

  impure function dma_cfg_rnd return ctrl2dma_type is 
  -- generate random configuration for dma transferts
    --variable lenm1 : std_ulogic_vector(12 downto 0) := std_ulogic_vector_rnd(13);
    variable lenm1 : std_ulogic_vector(lsize - 1 downto 0) := std_ulogic_vector_rnd(lsize);
    variable src : std_ulogic_vector(12 downto 0) := std_ulogic_vector_rnd(13);
    variable endsrc : u_unsigned(13 downto 0) := shift_right(u_unsigned('0' & src) + u_unsigned(lenm1), 3);
    variable dst : std_ulogic_vector(12 downto 0) := std_ulogic_vector_rnd(13);
    variable enddst : u_unsigned(13 downto 0) := shift_right(u_unsigned('0' & dst) + u_unsigned(lenm1), 3);
    variable ctrl : ctrl2dma_type;
    variable mode : std_ulogic;

    begin 

      ctrl.ls  := std_ulogic_rnd;
      ctrl.ld  := std_ulogic_rnd;

      mode := not (ctrl.ld xor ctrl.ls);

      if mode = '1' then 
        while (((u_unsigned(src) < u_unsigned(dst)) and (endsrc >= shift_right(u_unsigned('0' & dst), 3))) or ((u_unsigned(dst) < u_unsigned(src)) and (enddst >=
        shift_right(u_unsigned('0' & src), 3))) or (u_unsigned(src) = u_unsigned(dst)) or ((u_unsigned('0' & src) + u_unsigned(lenm1)) >= X"400") or ((u_unsigned('0' & dst)
        + u_unsigned(lenm1)) >= X"400")) loop
          lenm1 := std_ulogic_vector_rnd(lsize);
          src := std_ulogic_vector_rnd(13);
          dst := std_ulogic_vector_rnd(13);
          endsrc := shift_right(u_unsigned('0' & src) + u_unsigned(lenm1), 3);
          enddst := shift_right(u_unsigned('0' & dst) + u_unsigned(lenm1), 3);
        end loop;
      else 
        while  u_unsigned('0' & src) + u_unsigned(lenm1) >= X"400"  or
        u_unsigned('0' & dst) + u_unsigned(lenm1) >= X"400" loop 
          lenm1 := std_ulogic_vector_rnd(lsize);
          src := std_ulogic_vector_rnd(13);
          dst := std_ulogic_vector_rnd(13);
        end loop;
      end if;

      ctrl.exec := '0'; 
      ctrl.srstn := '1'; 
      ctrl.ce := '1'; 
      ctrl.fd := '0'; 
      ctrl.fs := '0'; 
      ctrl.src := X"0000" & "000" & src;
      ctrl.dst := X"0000" & "000" & dst;
      ctrl.lenm1(31 downto lsize) := (others => '0');
      ctrl.lenm1(lsize - 1 downto 0):= lenm1;
      ctrl.cst := std_ulogic_vector_rnd(64);
      ctrl.be := X"FF";
      ctrl.cs := std_ulogic_rnd and std_ulogic_rnd and std_ulogic_rnd;

      return ctrl;
  
    end function dma_cfg_rnd;

    constant clock_period: time := 10 ns;

  begin
   
  ref_mss_out.oor <= '0'; 
  ref_mss_out.gnt <= X"FF"; 

  mss2dma <= local_mss_out;
  mss2vci <= ext_mss_out;


   -- component instantiation
  u_dma: entity work.dma(rtl) 
    generic map(
      n0 => n0,
      n1 => n1)
    port map (
      clk      => clk,
      vci_in   => vci_init_in,
      vci_out  => vci_init_out, 
      dma2ctrl => dma2ctrl,
      ctrl2dma => ctrl2dma,
      mss2dma  => mss2dma,
      dma2mss  => dma2mss
    );

  ref_mss: entity work.spshiftram(arc)
    port map(
      clk     => clk,
      en       => ref_mss_in.en,
      shen    => shiften,
      lnr     => left,
      size    => size,
      rnw     => ref_mss_in.rnw,
      we      => ref_mss_in.be,
      add     => ref_mss_in.add(9 downto 0),
      din     => ref_mss_in.wdata,
      dout    => ref_mss_out.rdata 
    );

  local_mss: entity work.local_mss(rtl)
    generic map(
      n0 => n0,
      n1 => n1)
    port map(
      clk     => clk,
      run     => run,
      mssin   => local_mss_in,
      mssout  => local_mss_out
    );

  ext_mss : entity memories_lib.memory 
  generic map(
    registered => false)
  port map (
    clk   =>  clk,
    run   =>  run,
    en    =>  ext_mss_in.en,   
    rnw   =>  ext_mss_in.rnw,  
    be    =>  ext_mss_in.be,   
    add   =>  ext_mss_in.add(9 downto 0),  
    wdata =>  ext_mss_in.wdata,
    gnt   =>  ext_mss_out.gnt,  
    rdata =>  ext_mss_out.rdata
  );
 
  ext_mss_out.oor <= '0';
  
  -- Destination mss interface
  u_mssif: entity work.mssif(rtl)
  port map(
  clk,
  srstn,
  vci_init_out,
  vci_init_in,
  mss2vci,
  vci2mss
);
  ---------------------------
  -- clock generation process
  ---------------------------
  process
  begin
    clk <= '1';
    wait for clock_period / 2;
    clk <= '0';
    wait for clock_period / 2;
   if eos then
     wait;
   end if;
  end process;

  process(dma2mss, sim2local)
  begin
    if dma2mss.en = '1' then 
      local_mss_in <= dma2mss;
    else
      local_mss_in <= sim2local;
    end if;
  end process;

  process(vci2mss, sim2ext)
  begin
    if vci2mss.en = '1' then 
      ext_mss_in <= vci2mss;
    else
      ext_mss_in <= sim2ext;
    end if;
  end process;
  ---------------------------
  -- DMA command
  ---------------------------

  dma: process
    variable ctrl  : ctrl2dma_type;
    variable tmp : dma2mss_type;
  begin

    srstn <= '0';
    ctrl2dma.srstn <= '0';
    for i in 1 to n loop
      for i in 1 to 10 loop
        wait until rising_edge(clk);
      end loop;
      run <= '0';
      srstn   <= '1';
      ctrl2dma.srstn <= '1';
      ctrl2dma <= dma_cfg_rnd;
      wait until rising_edge(clk);
      ctrl := ctrl2dma;
      init_mss(ctrl, clk,ref_mss_in, ref_mss_out, sim2local, local_mss_out, sim2ext, ext_mss_out, shiften, left, size); 
      run <= '1';
      ctrl2dma.exec <= '1'; 
      wait until rising_edge(clk);
      ctrl2dma.exec <= '0'; 
      wait until dma2ctrl.eot = '1' and rising_edge(clk) for 100000 * clock_period;
      assert dma2ctrl.eot = '1' and rising_edge(clk)
        report "DMA: TEST terminated on timeout exceeded with inputs parameters : lenm1 : " & vec2hexstr(ctrl.lenm1) &            
               " src : " & vec2hexstr(ctrl.src) &            
               " dst : " & vec2hexstr(ctrl.dst) &            
               " ls : " & std_ulogic'image(ctrl.ls) &            
               " ld : " & std_ulogic'image(ctrl.ld) &            
               " ce : " & std_ulogic'image(ctrl.ce) &            
               " cs : " & std_ulogic'image(ctrl.cs) & 
               " and output parameters : exit status : " & vec2hexstr(dma2ctrl.status) & 
               " error flag : " & std_ulogic'image(dma2ctrl.err) & "."
        severity failure;
      run <= '0';
      if dma2ctrl.err = '0' then
        check_mss(ctrl, clk,ref_mss_in, ref_mss_out, sim2local, local_mss_out, sim2ext, ext_mss_out); 
      end if;
      report "DMA: TEST terminated with inputs parameters : lenm1 : " & vec2hexstr(ctrl.lenm1) &            
             " src : " & vec2hexstr(ctrl.src) &            
             " dst : " & vec2hexstr(ctrl.dst) &            
             " ls : " & std_ulogic'image(ctrl.ls) &            
             " ld : " & std_ulogic'image(ctrl.ld) &            
             " ce : " & std_ulogic'image(ctrl.ce) &            
             " cs : " & std_ulogic'image(ctrl.cs) & 
             " and output parameters : exit status : " & vec2hexstr(dma2ctrl.status) & 
             " error flag : " & std_ulogic'image(dma2ctrl.err) & ".";           
    end loop;
    if dma2ctrl.err = '1' then
      assert false
      severity failure;
    end if;
    report "DMA: Regression test passed ";
    eos <= true;
    wait;
  end process dma;

  process(clk)
  begin
    if (clk'event and clk = '1') then
      -- ack from ref memory
      gnt_local <=  (others => '0');
      gnt_ext <=  (others => '0');
      gnt_ref(0) <=  ref_mss_in.en;
      gnt_ref(1) <=  gnt_ref(0);
      -- ack from dma memory
      if sim2local.en = '1' then  
        gnt_local(0) <=  '1';
      end if;
      for i in 0 to n0 + n1 - 2 loop 
        gnt_local(i + 1)<=  gnt_local(i);
      end loop;
      -- ack from vci memory
      if ext_mss_out.gnt = X"FF" and sim2ext.en = '1' then  
        gnt_ext(0) <=  '1';
      end if;
      gnt_ext(1) <=  gnt_ext(0);
    end if;
  end process;

end architecture;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
