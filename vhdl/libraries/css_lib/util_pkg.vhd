--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Microcontroller simulation utility package

library ieee;
use ieee.std_logic_1164.all;

package util_pkg is

  function hex2nat(val: string) return natural;

end package util_pkg;

package body util_pkg is

  function hex2nat(val: string) return natural is
    variable res: natural := 0;
  begin
    for i in val'range loop
      res := res * 16;
      case val(i) is
        when '0' to '9' => res := res + character'pos(val(i)) -
          character'pos('0');
        when 'A' to 'F' => res := res + character'pos(val(i)) -
          character'pos('A') + 10;
        when 'a' to 'f' => res := res + character'pos(val(i)) -
          character'pos('a') + 10;
        when others => assert false
          report "not an hexadecimal character: '" & val & "'"
          severity failure;
      end case;
    end loop;
    return res;
  end function hex2nat;

end package body util_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
