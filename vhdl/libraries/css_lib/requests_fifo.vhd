--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Incoming requests FIFO.
--*
--*  Request FIFO located between the AVCI crossbar target port and the internals of CSS. Up to DEPTH incoming requests are stored in the FIFO. The
--* CMDACK request acknowledge signal sent back to the AVCI crossbar is asserted by default. It is de-asserted only when the FIFO is full. On the local DSP unit side,
--* the first received and not yet transmitted request, if any, is output on REQ_OUT. When acknowledged (ACK input asserted) it is removed from the FIFO and the
--* next request, if any, is output on REQ_OUT.

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

entity requests_fifo is
  generic(depth: positive := 2);
  port(
    clk:     in  std_ulogic;  --* master clock
    srstn:   in  std_ulogic;  --* synchronous active low reset
    ce:      in  std_ulogic;  --* chip enable
    req_in:  in  vci_request_type; --* AVCI incoming requests (from host system)
    req_out: out vci_request_type; --* AVCI outgoing requests (to local DSP unit)
    ack:     in  std_ulogic;  --* Acknowledge of current outgoing request
    cmdack:  out std_ulogic); --* Acknowledge of current incoming request (to host system)

end entity requests_fifo;

architecture rtl of requests_fifo is

  --* Requests FIFO; inputs at 0, outputs at DEPTH-1
  signal requests: vci_request_vector(0 to depth - 1);
  signal cmdack_local: std_ulogic;

  --* Logarithmic, priority-based selector among a vector of requests. The rightmost request has the highest priority.
  function req_selector(r: vci_request_vector) return vci_request_type is
    constant n: natural := r'length;
    variable rv: vci_request_vector(0 to n - 1) := r;
    variable tmpr, tmpl, res: vci_request_type;
  begin
    if n = 0 then      -- if no request...
      res := vci_request_none; -- ...return default request (no request)
    elsif n = 1 then   -- else if one request...
      res := rv(0);    -- ...return that one
    else               -- else if two or more requests
      tmpl := req_selector(rv(0 to n / 2 - 1));         -- highest priority request from the left half
      tmpr := req_selector(rv(n / 2 to n - 1)); -- highest priority request from the right half
      if tmpr.cmdval = '1' then -- if request from right half is active...
        res := tmpr;            -- ...return that one
      else                      -- else...
        res := tmpl;            -- ...return the one from the left half
      end if;
    end if;
    return res;
  end function req_selector;

begin

  --* The FIFO of requests. Incoming requests, if any, are always stored in the input place of the FIFO (REQUESTS(0)) if there is at least one free place in the
  --* FIFO. Active requests are always moved ahead from input to output if the next place is free. The current pending request, if any, is always the active
  --* request which place is the closest from the output.
  process(clk)
    variable requestsv: vci_request_vector(0 to depth - 1);
    variable ackv: std_ulogic;
  begin
    if rising_edge(clk) then
      if srstn = '0' then -- if reset active...
        for i in 0 to depth - 1 loop
          requestsv(i) := vci_request_none; -- set all FIFO places to "no request"
        end loop;
      elsif ce = '1' then -- reset inactive and chip enabled
        requestsv := requests;
        ackv      := ack;
        for i in depth - 1 downto 0 loop -- for all FIFO places from output to input
          if ackv = '1' and requestsv(i).cmdval = '1' then -- if pending active request acknowledged
            ackv                := '0'; -- clear acknowledge flag
            requestsv(i).cmdval := '0'; -- clear active request flag
          end if;
        end loop;
          -- shift FIFO ahead by one place whenever possible (but save power by not moving inactive requests)
        for i in depth - 1 downto 1 loop -- for all FIFO places from output to input, but the input place
          if requestsv(i).cmdval = '0' and requestsv(i - 1).cmdval = '1' then -- if current place is empty and previous is active
            requestsv(i) := requestsv(i - 1); -- shift request
            requestsv(i - 1).cmdval := '0';   -- clear active request flag in previous FIFO place
          end if;
        end loop;
        if cmdack_local = '1' and req_in.cmdval = '1' then -- if active incoming request acknowledged
          requestsv(0) := req_in; -- store incoming request in FIFO
        end if;
      end if;
      requests <= requestsv;
    end if;
  end process;

  process(requests, ce)
    variable tmp: std_ulogic_vector(0 to depth - 1);
  begin
    for i in 0 to depth - 1 loop
      tmp(i) := requests(i).cmdval;
    end loop;
    cmdack_local <= (not and_reduce(tmp)) and ce;
  end process;

  cmdack  <= cmdack_local;
  req_out <= req_selector(requests);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
