--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for outgoing responses FIFO.

library ieee;
use ieee.std_logic_1164.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.global.all;
use global_lib.sim_utils.all;

entity responses_fifo_sim is
  generic(depth: positive := 3;
          n: positive := 1000);
end entity responses_fifo_sim;

architecture sim of responses_fifo_sim is

  signal clk, srstn, ce, rspack: std_ulogic;
  signal ack: natural range 0 to depth;
  signal rsp_in, rsp_out: vci_response_type;
  signal eos: boolean := false;

begin

  rf: entity work.responses_fifo(rtl)
    generic map(depth => depth)
    port map(clk     => clk,
             srstn   => srstn,
             ce      => ce,
             rsp_in  => rsp_in,
             rsp_out => rsp_out,
             ack     => ack,
             rspack  => rspack);
  process
  begin
    clk <= '0';
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
    if eos then
      wait;
    end if;
  end process;

  process
  begin
    srstn  <= '0';
    ce     <= '1';
    rspack <= '0';
    rsp_in <= vci_response_none;
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    srstn <= '1';
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    for i in 1 to 100 loop
      if ack /= 0 then
        rsp_in <= vci_response_rnd;
        rsp_in.rspval <= '1';
      end if;
      rspack <= std_ulogic_rnd;
      wait until rising_edge(clk);
    end loop;
    rspack <= '1';
    for i in 1 to 100 loop
      if ack /= 0 then
        rsp_in <= vci_response_rnd;
        rsp_in.rspval <= '1';
      end if;
      wait until rising_edge(clk);
    end loop;
    for i in 1 to 100 loop
      if ack /= 0 then
        rsp_in <= vci_response_rnd;
      end if;
      wait until rising_edge(clk);
    end loop;
    for i in 1 to n loop
      if ack /= 0 then
        rsp_in <= vci_response_rnd;
      end if;
      rspack <= std_ulogic_rnd;
      wait until rising_edge(clk);
      progress(i, n);
    end loop;
    eos <= true;
    report "Regression test passed";
    wait;
  end process;

  process(clk)
    variable pipe: vci_response_vector(0 to depth - 1);
    variable cnt: natural range 0 to depth;
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        pipe := (others => vci_response_none);
        cnt  := 0;
      else
        assert (pipe(0).rspval = '0' and rsp_out.rspval = '0') or pipe(0) = rsp_out
          report "Mismatch on output response"
          severity failure;
        assert cnt = depth - ack
          report "Mismatch on ACK"
          severity failure;
        if pipe(0).rspval = '1' and rspack = '1' then
          pipe := pipe(1 to depth - 1) & vci_response_none;
          cnt := cnt - 1;
        end if;
        if ack /= 0 and rsp_in.rspval = '1' then
          pipe(cnt) := rsp_in;
          cnt := cnt + 1;
        end if;
      end if;
    end if;
            
  end process;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
