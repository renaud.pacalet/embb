--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for Control Sub-System

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;
use global_lib.sim_utils.all;

use work.bitfield.all;
use work.css_pkg.all;

package css_sim_pkg is

  constant n_pa_regs: natural           := 3;
  constant pa_rmask:  std_ulogic_vector := x"ffffffffffffffffffffffffffffffffffffffffffffffff";
  constant pa_wmask:  std_ulogic_vector := x"ffffffffffffffffffffffffffffffffffffffffffffffff";
  constant mss_depth: natural           := 2**16;
  constant ucr_depth: natural           := 2**11;

  type fifo_entry_type is record
    rsp: vci_response_type;
    be: word64_be_type;
  end record;
  constant fifo_entry_none: fifo_entry_type := (rsp => vci_response_none, be => (others => '0'));
  type fifo_type is array(natural range <>) of fifo_entry_type;
  procedure write(l: inout line; f: in fifo_entry_type);
  procedure print(f: in fifo_entry_type);

  impure function word64_vector_rnd(n: natural) return word64_vector;
  function match(rdata: word64; ref: fifo_entry_type) return boolean;
--  procedure run_dma(cfg: in word64; cst: in word64; add: in word64);

  constant is_host_mapped: boolean_vector(0 to 31) := gen_is_host_mapped(n_pa_regs);
  constant is_uc_mapped:   boolean_vector(0 to 31) := gen_is_uc_mapped(n_pa_regs);

  constant reg_access_pktid: vci_pktid_type := vci_pktid_type(to_unsigned(0, vci_p));
  constant mss_access_pktid: vci_pktid_type := vci_pktid_type(to_unsigned(1, vci_p));
  constant error_pktid: vci_pktid_type := vci_pktid_type(to_unsigned(3, vci_p));

end package css_sim_pkg;

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;
use global_lib.sim_utils.all;

use work.css_pkg.all;
use work.bitfield.all;
use work.css_sim_pkg.all;

entity css_sim is
  generic(n: positive := 100000);
end entity css_sim;

architecture sim of css_sim is

  signal clk:      std_ulogic;
  signal srstn:    std_ulogic;
  signal ce:       std_ulogic;
  signal hirq:     std_ulogic;
  signal uirq:     std_ulogic;
  signal pirq:     std_ulogic;
  signal dirq:     std_ulogic;
  signal pss2css:   pss2css_type;
  signal css2pss:   css2pss_type;
  signal param:    std_ulogic_vector(n_pa_regs * 64 - 1 downto 0);
  signal mss2css:  mss2css_type;
  signal css2mss:  css2mss_type;
  signal tvci_in:  vci_i2t_type;
  signal tvci_out: vci_t2i_type;
  signal ivci_in:  vci_t2i_type;
  signal ivci_out: vci_i2t_type;
  signal eos:      boolean := false;

begin

  icss: entity work.css(rtl)
    generic map(n_pa_regs   => n_pa_regs,
                pa_rmask    => pa_rmask,
                pa_wmask    => pa_wmask)
    port map(clk      => clk,
             srstn    => srstn,
             ce       => ce,
             hirq     => hirq,
             uirq     => uirq,
             pirq     => pirq,
             dirq     => dirq,
             eirq     => open,
             pss2css  => pss2css,
             css2pss  => css2pss,
             param    => param,
             mss2css  => mss2css,
             css2mss  => css2mss,
             tvci_in  => tvci_in,
             tvci_out => tvci_out,
             ivci_in  => ivci_in,
             ivci_out => ivci_out);

  clock: process
  begin
    clk <= '0';
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
    if eos then
      wait;
    end if;
  end process clock;

  mss_comb_p: process(css2mss)
    variable add: natural;
  begin
    mss2css.mss2dma.oor <= '0';
    if css2mss.dma2mss.en = '1' then
      if not hst_is_in_mss(css2mss.dma2mss.add & "000") then
        mss2css.mss2dma.oor <= '1';
      else
        add := to_integer(u_unsigned(css2mss.dma2mss.add));
        if add >= mss_depth / 8 then
          mss2css.mss2dma.oor <= '1';
        end if;
      end if;
    end if;

    mss2css.mss2vci.oor <= '0';
    if css2mss.vci2mss.en = '1' then
      if not hst_is_in_mss(css2mss.vci2mss.add & "000") then
        mss2css.mss2vci.oor <= '1';
      else
        add := to_integer(u_unsigned(css2mss.vci2mss.add));
        if add >= mss_depth / 8 then
          mss2css.mss2vci.oor <= '1';
        end if;
      end if;
    end if;

    mss2css.mss2uc.oor <= '0';
    if css2mss.uc2mss.en = '1' then
      if not uc_is_in_mss(css2mss.uc2mss.add) then
        mss2css.mss2uc.oor <= '1';
      else
        add := to_integer(u_unsigned(css2mss.uc2mss.add));
        if add >= ucr_depth then
          mss2css.mss2uc.oor <= '1';
        end if;
      end if;
    end if;
  end process mss_comb_p;

  mss_sync_p: process(clk)
    variable mss_tvci, mss_dma: word64_vector(0 to mss_depth / 8 - 1) := (others => (others => 'X'));
    variable mss_uc: word64_vector(0 to ucr_depth / 8 - 1) := (others => (others => 'X'));
    variable add: natural;
    variable off: natural range 0 to 7;
    variable ucpipe: word64;
    variable dmapipe: vci_data_type;
    variable vcipipe: vci_data_type;
  begin
    if rising_edge(clk) then
      mss2css.mss2dma.gnt <= vci_be_rnd;
      mss2css.mss2vci.gnt <= vci_be_rnd;
      mss2css.mss2uc.gnt  <= vci_be_rnd;

      mss2css.mss2uc.rdata <= ucpipe;
      if css2mss.uc2mss.en = '1' and mss2css.mss2uc.gnt = X"FF" and mss2css.mss2uc.oor = '0' and css2mss.uc2mss.rnw = '1' then
        add := to_integer(u_unsigned(css2mss.uc2mss.add));
        ucpipe := mss_uc(add / 8);
      end if;
      mss2css.mss2dma.rdata <= dmapipe;
      if css2mss.dma2mss.en = '1' and mss2css.mss2dma.oor = '0' and css2mss.dma2mss.rnw = '1' then
        add := to_integer(u_unsigned(css2mss.dma2mss.add));
        dmapipe := vci_data_rnd;
        word64_update(dmapipe, css2mss.dma2mss.be and mss2css.mss2dma.gnt, mss_dma(add));
      end if;
      mss2css.mss2vci.rdata <= vcipipe;
      if css2mss.vci2mss.en = '1' and mss2css.mss2vci.oor = '0' and css2mss.vci2mss.rnw = '1' then
        add := to_integer(u_unsigned(css2mss.vci2mss.add));
        vcipipe := vci_data_rnd;
        word64_update(vcipipe, css2mss.vci2mss.be and mss2css.mss2vci.gnt, mss_tvci(add));
      end if;
      if css2mss.vci2mss.en = '1' and mss2css.mss2vci.oor = '0' and css2mss.vci2mss.rnw = '0' then
        add := to_integer(u_unsigned(css2mss.vci2mss.add));
        word64_update(mss_tvci(add), css2mss.vci2mss.be and mss2css.mss2vci.gnt, css2mss.vci2mss.wdata);
      end if;
      if css2mss.dma2mss.en = '1' and mss2css.mss2dma.oor = '0' and css2mss.dma2mss.rnw = '0' then
        word64_update(mss_dma(add), css2mss.dma2mss.be and mss2css.mss2dma.gnt, css2mss.dma2mss.wdata);
      end if;
      if css2mss.uc2mss.en = '1' and mss2css.mss2uc.gnt = X"FF" and mss2css.mss2uc.oor = '0' and css2mss.uc2mss.rnw = '0' then
        add := to_integer(u_unsigned(css2mss.uc2mss.add));
        off := add mod 8;
        add := add / 8;
        mss_uc(add)(63 - 8 * off downto 56 - 8 * off) := css2mss.uc2mss.wdata;
      end if;
    end if;
  end process mss_sync_p;

  pss_p: process(clk)
    variable pssbusy: boolean;
  begin
    if rising_edge(clk) then
      if css2pss.ce = '1' then
        if css2pss.srstn = '0' then
          pssbusy := false;
          pss2css.eoc <= '0';
          pss2css.err <= '0';
          pss2css.status <= (others => '0');
          pss2css.data <= (others => '0');
        else
          pss2css.eoc    <= '0';
          if pssbusy then
            if integer_rnd(1, 100) = 100 then
              pss2css.eoc    <= '1';
              pss2css.err    <= std_ulogic_rnd;
              pss2css.status <= status_rnd;
              pss2css.data <= data_rnd;
              pssbusy        := false;
            end if;
          elsif css2pss.exec = '1' then
            pssbusy := true;
          end if;
        end if;
      end if;
    end if;
  end process pss_p;

  ivci_in_p: process
    variable fifo_depth: positive := 10;
    variable rsp: vci_response_vector(1 to fifo_depth) := (others => vci_response_none);
    variable r: natural range 0 to fifo_depth := 0;
    variable add: natural;
    variable mss: word64_vector(0 to mss_depth / 8 - 1) := word64_vector_rnd(mss_depth / 8);
  begin
    ivci_in.rsp <= vci_response_none;
    ivci_in.cmdack <= '0';
    loop
      wait until rising_edge(clk);
      if ivci_out.req.cmdval = '1' and ivci_in.cmdack = '1' then
        assert ivci_out.req.address(2 downto 0) = "000" report "Unaligned DMA access" severity failure;
        add := to_integer(u_unsigned(ivci_out.req.address(31 downto 3))) mod (mss_depth / 8);
        rsp(2 to fifo_depth) := rsp(1 to fifo_depth - 1);
        rsp(1).rspval := '1';
        rsp(1).rdata  := vci_data_rnd;
        word64_update(rsp(1).rdata, ivci_out.req.be, mss(add));
        rsp(1).rerror := (others => '0');
--        if int_rnd(1, 1000) = 1 then
--          rsp(1).rerror    := vci_error_rnd;
--          rsp(1).rerror(0) := '1';
--        end if;
        rsp(1).reop   := ivci_out.req.eop;
        rsp(1).rsrcid := ivci_out.req.srcid;
        rsp(1).rtrdid := ivci_out.req.trdid;
        rsp(1).rpktid := ivci_out.req.pktid;
        r := r + 1;
      end if;
      if ivci_in.rsp.rspval /= '1' or (ivci_in.rsp.rspval = '1' and ivci_out.rspack = '1') then
        if ivci_in.rsp.rspval = '1' then
          rsp(r) := vci_response_none;
          ivci_in.rsp.rspval <= '0';
          r := r - 1;
        end if;
        if r /= 0 and boolean_rnd then
          ivci_in.rsp <= rsp(r);
        end if;
      end if;
      if r = fifo_depth then
        ivci_in.cmdack <= '0';
      else
        ivci_in.cmdack <= std_ulogic_rnd;
      end if;
    end loop;
  end process ivci_in_p;

  tvci_in_p: process
    variable r: regs_type;
    variable w32: word32;
    variable w8: word8;
    variable vufpd: natural;
    variable vufpl: natural;
    variable vufpo: natural;
    variable vufpb: natural;
    variable tmp: natural := 0;
    variable req: vci_request_type;
    variable add: natural;
  begin
    srstn <= '0';
    ce    <= '0';
    hirq  <= '0';
    tvci_in <= vci_i2t_none;
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    ce    <= '1';
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    srstn <= '1';
    r := (others => (others => '0'));
    set_flag(r, urst_field_def, '0');
    set_flag(r, prst_field_def, '0');
    set_flag(r, drst_field_def, '0');
    set_flag(r, uce_field_def, '1');
    set_flag(r, pce_field_def, '1');
    set_flag(r, dce_field_def, '1');
    set_flag(r, udbg_field_def, '0');
    set_flag(r, u2hie_field_def, '1');
    set_flag(r, p2hie_field_def, '1');
    set_flag(r, d2hie_field_def, '1');
    set_flag(r, hie_field_def, '1');
    set_flag(r, p2uie_field_def, '1');
    set_flag(r, d2uie_field_def, '1');
    set_flag(r, h2uie_field_def, '1');
    set_flag(r, uie_field_def, '1');
    req.cmdval  := '1';
    req.address := idx2hst_add(ctrl_idx);
    req.be      := (others => '1');
    req.cmd     := vci_cmd_write;
    req.wdata   := r(ctrl_idx);
    req.srcid   := vci_srcid_rnd;
    req.trdid   := vci_trdid_rnd;
    req.pktid   := reg_access_pktid; -- reg access
    req.eop     := std_ulogic_rnd;
    tvci_in.req    <= req;
    tvci_in.rspack <= '1';
    wait until rising_edge(clk) and tvci_out.cmdack = '1';
    tvci_in.req.cmdval <= '0';
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    set_flag(r, uce_field_def, '0');
    set_flag(r, urst_field_def, '0');
    set_flag(r, pce_field_def, '1');
    set_flag(r, prst_field_def, '1');
    set_flag(r, dce_field_def, '1');
    set_flag(r, drst_field_def, '1');
    req.wdata := r(ctrl_idx);
    tvci_in.req    <= req;
    wait until rising_edge(clk) and tvci_out.cmdack = '1';
    tvci_in.req.cmdval <= '0';
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    for i in 1 to n loop
      if i mod (n / 100) = 0 then
        progress(i, n);
      end if;
      if pirq = '1' or dirq = '1' or uirq = '1' then
        req.cmdval  := '1';
        req.be      := (others => '1');
        req.cmd     := vci_cmd_read;
        req.wdata   := (others => '0');
        req.srcid   := vci_srcid_rnd;
        req.trdid   := vci_trdid_rnd;
        req.pktid   := reg_access_pktid; -- reg access
        req.eop     := std_ulogic_rnd;
        if pirq = '1' then
          req.address := idx2hst_add(pgost_idx);
        elsif dirq = '1' then
          req.address := idx2hst_add(dgost_idx);
        else
          req.address := idx2hst_add(irq_idx);
        end if;
        tvci_in.req    <= req;
        tvci_in.rspack <= '1';
        wait until rising_edge(clk) and tvci_out.cmdack = '1';
        tvci_in.req.cmdval <= '0';
      else
        req := vci_request_rnd;
        if boolean_rnd then
          req.cmd := vci_cmd_read;
        else
          req.cmd := vci_cmd_write;
        end if;
        tmp := int_rnd(1, 100);
        if tmp = 1 then
          req.address := vci_address_rnd;
        elsif tmp = 2 then
          req.address := (vci_address_rnd and (not hst_mss_mask)) or (hst_mss_ba and hst_mss_mask);
        elsif tmp <= 50 then
          req.address := std_ulogic_vector(to_unsigned(int_rnd(0, mss_depth - 1), vci_n));
          req.address(2 downto 0) := (others => '0');
        else
          req.address := (vci_address_rnd and (not hst_regs_mask)) or (hst_regs_ba and hst_regs_mask);
          if tmp /= 51 then
            add := integer_rnd(0, n_pa_regs + n_cs_regs - 1);
            if add >= n_pa_regs then
              add := fidx2idx(add - n_pa_regs);
            end if;
            if add = uregs_idx then
              req.cmd := vci_cmd_read;
            end if;
            req.address(7 downto 3) := std_ulogic_vector(to_unsigned(add, 5));
          end if;
        end if;
        if hst_is_in_regs(req.address) then
          add := hst_add2idx(req.address);
          if req.cmd = vci_cmd_write then
            r(add) := req.wdata;
            if add = ctrl_idx then
              set_flag(r, uce_field_def, '0');
              set_flag(r, urst_field_def, '1');
              set_flag(r, pce_field_def, '1');
              set_flag(r, prst_field_def, '1');
              set_flag(r, dce_field_def, '1');
              set_flag(r, drst_field_def, '1');
              req.wdata := r(add);
            end if;
            if add = dcfg_idx then
              w32 := get_field(r, dlenm1_field_def);
              w32(31 downto 8) := x"000000";
              set_field(r, dlenm1_field_def, w32);
              req.wdata := r(add);
            end if;
            if add = dadd_idx then
              w32 := get_field(r, dsrc_field_def);
              w32(31 downto 16) := x"0000";
              set_field(r, dsrc_field_def, w32);
              w32 := get_field(r, ddst_field_def);
              w32(31 downto 16) := x"0000";
              set_field(r, ddst_field_def, w32);
              req.wdata := r(add);
            end if;
            if add = uprphs_idx then
              vufpl := int_rnd(1, n_pa_regs);
              vufpd := int_rnd(0, n_pa_regs - vufpl);
              vufpb := int_rnd(0, (ucr_depth - 1) / 2048);
              vufpo := ucr_depth - 2048 * vufpb;
              if vufpo >= 2048 then
                vufpo := 2048;
              end if;
              vufpo := vufpo / 64 - vufpl;
              vufpo := int_rnd(0, vufpo);
              set_field(r, ufpd_field_def, std_ulogic_vector(to_unsigned(vufpd, 8)));
              set_field(r, ufpl_field_def, std_ulogic_vector(to_unsigned(vufpl, 8)));
              set_field(r, ufpo_field_def, std_ulogic_vector(to_unsigned(vufpo, 8)));
              set_field(r, ufpb_field_def, std_ulogic_vector(to_unsigned(vufpb, 8)));
              req.wdata := r(add);
            end if;
          end if;
        end if;
        req.pktid := error_pktid;
        if hst_is_in_mss(req.address) then
          req.pktid := mss_access_pktid; -- mss
        elsif hst_is_in_regs(req.address) then
          req.pktid := reg_access_pktid; -- reg
        end if;
        tvci_in.req <= req;
      end if;
      tvci_in.rspack <= std_ulogic_rnd;
      wait until rising_edge(clk);
    end loop;
    tvci_in.req.cmdval  <= '0';
    tvci_in.rspack <= '1';
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    print("Regression test passed");
    eos   <= true;
    wait;
  end process tvci_in_p;

  check1_p: process(clk)
    constant fifo_depth: positive := 10;
    variable r: natural range 0 to fifo_depth := 0;
    variable i: natural range 0 to fifo_depth;
    variable fifo: fifo_type(1 to fifo_depth) := (others => fifo_entry_none);
    variable add: natural;
    variable sev: severity_level;
    variable l: line;
    variable mss: word64_vector(0 to mss_depth / 8 - 1) := (others => (others => 'X'));
  begin
    if rising_edge(clk) then
      if tvci_in.req.cmdval = '1' and tvci_out.cmdack = '1' then
        add := to_integer(u_unsigned(tvci_in.req.address(31 downto 3)));
        fifo(2 to fifo_depth) := fifo(1 to fifo_depth - 1);
        fifo(1).rsp.rspval := '1';
        fifo(1).rsp.rerror := (others => '0');
        fifo(1).rsp.reop   := tvci_in.req.eop;
        fifo(1).rsp.rsrcid := tvci_in.req.srcid;
        fifo(1).rsp.rtrdid := tvci_in.req.trdid;
        fifo(1).rsp.rpktid := tvci_in.req.pktid;
        fifo(1).be := (others => '0');
        if tvci_in.req.pktid = mss_access_pktid then --mss
          if add < mss_depth / 8 then -- in range
            if tvci_in.req.cmd = vci_cmd_read then
              fifo(1).be := tvci_in.req.be;
              fifo(1).rsp.rdata := mss(add);
            else
              word64_update(mss(add), tvci_in.req.be, tvci_in.req.wdata);
            end if;
          else -- out of range
            fifo(1).rsp.rerror(0) := '1';
          end if;
        elsif tvci_in.req.pktid = reg_access_pktid then -- reg
          add := add mod 32;
          if (not is_host_mapped(add)) or (add = uregs_idx and tvci_in.req.cmd = vci_cmd_write) then
            fifo(1).rsp.rerror(0) := '1';
          end if;
        elsif tvci_in.req.pktid = error_pktid then -- error
          fifo(1).rsp.rerror(0) := '1';
        end if;
        r := r + 1;
      end if;
      if tvci_out.rsp.rspval = '1' and tvci_in.rspack = '1' then
        if tvci_out.rsp.rpktid = reg_access_pktid then -- reg
          sev := warning;
        else
          sev := failure;
        end if;
        i := r;
        loop
          exit when fifo(i).rsp.rpktid = tvci_out.rsp.rpktid;
          i := i - 1;
        end loop;
        if not match(tvci_out.rsp.rdata, fifo(i)) then
          print("*** RDATA mismatch: ");
          print(tvci_out.rsp);
          print("*** expected:");
          print(fifo(i));
          assert false severity sev;
        end if;
        if fifo(i).rsp.rerror /= tvci_out.rsp.rerror then
          print(l, "RERROR mismatch: ");
          print(tvci_out.rsp);
          print("*** expected:");
          print(fifo(i));
          assert false severity failure;
        end if;
        if fifo(i).rsp.reop /= tvci_out.rsp.reop then
          print(l, "REOP mismatch: ");
          print(tvci_out.rsp);
          print("*** expected:");
          print(fifo(i));
          assert false severity failure;
        end if;
        if fifo(i).rsp.rsrcid /= tvci_out.rsp.rsrcid then
          print(l, "RSRCID mismatch: ");
          print(tvci_out.rsp);
          print("*** expected:");
          print(fifo(i));
          assert false severity failure;
        end if;
        if fifo(i).rsp.rtrdid /= tvci_out.rsp.rtrdid then
          print(l, "RTRDID mismatch: ");
          print(tvci_out.rsp);
          print("*** expected:");
          print(fifo(i));
          assert false severity failure;
        end if;
        fifo(i).rsp.rspval := '0';
        fifo(i to fifo_depth) := fifo(i + 1 to fifo_depth) & fifo_entry_none;
        r := r - 1;
      end if;
    end if;
  end process check1_p;

--    check2_p: process(clk)
--      variable cfg, cst, add: word64;
--    begin
--      if rising_edge(clk) then
--        if ce = '1' then
--          if srstn = '0' then
--            cfg := (others => '0');
--            cst := (others => '0');
--            add := (others => '0');
--          else
--            if tvci_in.req.cmdval = '1' and tvci_out.cmdack = '1' then
--              if tvci_in.req.rnw = '0' and tvci_in_req.address then
-- 
--                case tvci_in.req.address is
--                  when idx2hst_add(dcfg_idx) => word64_update(cfg, tvci_in.req.be, tvci_in.req.wdata);
--                  when idx2hst_add(dcst_idx) => word64_update(cst, tvci_in.req.be, tvci_in.req.wdata);
--                  when idx2hst_add(dadd_idx) => word64_update(add, tvci_in.req.be, tvci_in.req.wdata);
--                  when idx2hst_add(dgost_idx) => run_dma(cfg, cst, add);
--                  when others => null;
--                end case;
--              end if;
--            end if;
--          end if;
--        end if;
--      end if;
--    end process check2_p;

end architecture sim;

package body css_sim_pkg is

  procedure write(l: inout line; f: in fifo_entry_type) is
  begin
    print(l, "response = ");
    write(l, f.rsp);
    print(l, "be = ");
    hwrite(l, f.be);
  end procedure write;

  procedure print(f: in fifo_entry_type) is
    variable l: line;
  begin
    write(l, f);
    writeline(output, l);
  end procedure print;

  impure function word64_vector_rnd(n: natural) return word64_vector is
    variable res: word64_vector(0 to n - 1);
  begin
    for i in 0 to n - 1 loop
      res(i) := std_ulogic_vector_rnd(64);
    end loop;
    return res;
  end function word64_vector_rnd;

  function match(rdata: word64; ref: fifo_entry_type) return boolean is
    variable tmp1, tmp2: word64 := (others => '0');
  begin
    word64_update(tmp1, ref.be, rdata);
    word64_update(tmp2, ref.be, ref.rsp.rdata);
    return tmp1 = tmp2;
  end function match;

  procedure run_dma(r: in regs_type; mssl: inout word64_vector; msse: inout word64_vector) is
    variable vda, vsa, vlenm1: word32;
    variable nda, nsa, nlenm1, da, do, sa, so: natural;
    variable be, byte: word8;
    variable cs, ls, ld, fs, fd: boolean;
    variable cst: word64 := (others => '0');
  begin
    vda    := get_field(r, ddst_field_def);
    vsa    := get_field(r, dsrc_field_def);
    vlenm1 := get_field(r, dlenm1_field_def);
    be     := get_field(r, dbe_field_def);
    nda    := to_integer(u_unsigned(vda));
    nsa    := to_integer(u_unsigned(vsa));
    nlenm1 := to_integer(u_unsigned(vlenm1));
    cs     := get_flag(r, dcs_field_def) = '1';
    ls     := get_flag(r, dls_field_def) = '1';
    ld     := get_flag(r, dld_field_def) = '1';
    fs     := get_flag(r, dfs_field_def) = '1';
    fd     := get_flag(r, dfd_field_def) = '1';
    if cs then
      cst  := get_field(r, dcsth_field_def) & get_field(r, dcstl_field_def);
      nsa  := 0;
    end if;
    sa := nsa / 8;
    so := nsa mod 8;
    da := nda / 8;
    do := nda mod 8;
    for i in 0 to nlenm1 loop
      if be(7 - do) = '1' then
        if cs then
          byte := cst(63 - 8 * so downto 56 - 8 * so);
        elsif ls then
          byte := mssl(sa)(63 - 8 * so downto 56 - 8 * so);
        else
          byte := msse(sa)(63 - 8 * so downto 56 - 8 * so);
        end if;
        if ld then
          mssl(da)(63 - 8 * do downto 56 - 8 * do) := byte;
        else
          msse(da)(63 - 8 * do downto 56 - 8 * do) := byte;
        end if;
      end if;
      if cs or fs then
        so := (so + 1) mod 8;
      else
        so := so + 1;
        if so = 8 then
          sa := sa + 1;
          so := 0;
        end if;
      end if;
      if fd then
        do := (do + 1) mod 8;
      else
        do := do + 1;
        if do = 8 then
          da := da + 1;
          do := 0;
        end if;
      end if;
    end loop;
  end procedure run_dma;

end package body css_sim_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
