--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package free6502_pkg is

  component free6502 is
    generic(debug: boolean := false);
    port(clk:     in std_ulogic;
         reset:   in std_ulogic;
         irq:     in std_ulogic;
         nmi:     in std_ulogic;
         add:     out std_ulogic_vector(15 downto 0);
         din:     in  std_ulogic_vector(7 downto 0);
         dout:    out std_ulogic_vector(7 downto 0);
         dout_oe: out std_ulogic;
         we:      out std_ulogic;
         rd:      out std_ulogic;
         sync:    out std_ulogic;
         cs:      in  std_ulogic);
  end component free6502;

  -- constants
  constant reset_opcode: u_unsigned (7 downto 0) := x"03";
  constant irq_opcode: u_unsigned (7 downto 0) := "01000011";
  constant nmi_opcode: u_unsigned (7 downto 0) := "00110011";
  constant stack_page: u_unsigned (7 downto 0) := "00000001";
  constant vect_nmi1: u_unsigned (15 downto 0) := "1111111111111010";
  constant vect_nmi2: u_unsigned (15 downto 0) := "1111111111111011";
  constant vect_reset1: u_unsigned (15 downto 0) := "1111111111111100";
  constant vect_reset2: u_unsigned (15 downto 0) := "1111111111111101";
  constant vect_irq1: u_unsigned (15 downto 0) := "1111111111111110";
  constant vect_irq2: u_unsigned (15 downto 0) := "1111111111111111";

  -- internal state info
  type states is (reset1, reset2, fetch, start_irq, start_nmi, run);

  -- pragma translate_off
  type optype is record
    mnemonic: string(1 to 5);
    addressing_mode: string(1 to 8);
    original_cycles: natural;
    free6502_cycles: natural;
    flags: string(1 to 6);
  end record;

  type istype is array(natural range 0 to 255) of optype;

  constant instruction_set: istype;
  -- pragma translate_on

end package free6502_pkg;

package body free6502_pkg is

  -- pragma translate_off
  constant instruction_set: istype := 
   (16#6D# => ("  ADC", "     ABS", 4, 5, "  NVZC"),
    16#7D# => ("  ADC", "   ABS,X", 4, 5, "  NVZC"),
    16#79# => ("  ADC", "   ABS,Y", 4, 5, "  NVZC"),
    16#69# => ("  ADC", "     IMM", 2, 3, "  NVZC"),
    16#61# => ("  ADC", " (IND,X)", 6, 7, "  NVZC"),
    16#71# => ("  ADC", " (IND),Y", 5, 7, "  NVZC"),
    16#65# => ("  ADC", "  Z-PAGE", 3, 4, "  NVZC"),
    16#75# => ("  ADC", "Z-PAGE,X", 4, 4, "  NVZC"),
    16#2D# => ("  AND", "     ABS", 4, 5, "    NZ"),
    16#3D# => ("  AND", "   ABS,X", 4, 5, "    NZ"),
    16#39# => ("  AND", "   ABS,Y", 4, 5, "    NZ"),
    16#29# => ("  AND", "     IMM", 2, 3, "    NZ"),
    16#21# => ("  AND", " (IND,X)", 6, 7, "    NZ"),
    16#31# => ("  AND", " (IND),Y", 5, 7, "    NZ"),
    16#25# => ("  AND", "  Z-PAGE", 3, 4, "    NZ"),
    16#35# => ("  AND", "Z-PAGE,X", 4, 4, "    NZ"),
    16#0E# => ("  ASL", "     ABS", 6, 8, "   NZC"),
    16#1E# => ("  ASL", "   ABS,X", 7, 8, "   NZC"),
    16#0A# => ("  ASL", "     ACC", 2, 2, "   NZC"),
    16#06# => ("  ASL", "  Z-PAGE", 5, 7, "   NZC"),
    16#16# => ("  ASL", "Z-PAGE,X", 6, 7, "   NZC"),
    16#90# => ("  BCC", "     REL", 2, 4, "      "),
    16#B0# => ("  BCS", "     REL", 2, 4, "      "),
    16#F0# => ("  BEQ", "     REL", 2, 4, "      "),
    16#2C# => ("  BIT", "     ABS", 4, 5, "   BIT"),
    16#24# => ("  BIT", "  Z-PAGE", 3, 4, "   BIT"),
    16#30# => ("  BMI", "     REL", 2, 4, "      "),
    16#D0# => ("  BNE", "     REL", 2, 4, "      "),
    16#10# => ("  BPL", "     REL", 2, 4, "      "),
    16#00# => ("  BRK", "     IMP", 7, 7, "  SETI"),
    16#50# => ("  BVC", "     REL", 2, 4, "      "),
    16#70# => ("  BVS", "     REL", 2, 4, "      "),
    16#18# => ("  CLC", "     IMP", 2, 2, "CLEARC"),
    16#D8# => ("  CLD", "     IMP", 2, 2, "CLEARD"),
    16#58# => ("  CLI", "     IMP", 2, 2, "CLEARI"),
    16#B8# => ("  CLV", "     IMP", 2, 2, "CLEARV"),
    16#CD# => ("  CMP", "     ABS", 4, 5, "   NZC"),
    16#DD# => ("  CMP", "   ABS,X", 4, 5, "   NZC"),
    16#D9# => ("  CMP", "   ABS,Y", 4, 5, "   NZC"),
    16#C9# => ("  CMP", "     IMM", 2, 3, "   NZC"),
    16#C1# => ("  CMP", " (IND,X)", 6, 7, "   NZC"),
    16#D1# => ("  CMP", " (IND),Y", 5, 7, "   NZC"),
    16#C5# => ("  CMP", "  Z-PAGE", 3, 4, "   NZC"),
    16#D5# => ("  CMP", "Z-PAGE,X", 4, 4, "   NZC"),
    16#EC# => ("  CPX", "     ABS", 4, 5, "   NZC"),
    16#E0# => ("  CPX", "     IMM", 2, 3, "   NZC"),
    16#E4# => ("  CPX", "  Z-PAGE", 3, 4, "   NZC"),
    16#CC# => ("  CPY", "     ABS", 4, 5, "   NZC"),
    16#C0# => ("  CPY", "     IMM", 2, 3, "   NZC"),
    16#C4# => ("  CPY", "  Z-PAGE", 3, 4, "   NZC"),
    16#CE# => ("  DEC", "     ABS", 6, 8, "    NZ"),
    16#DE# => ("  DEC", "   ABS,X", 7, 8, "    NZ"),
    16#C6# => ("  DEC", "  Z-PAGE", 5, 7, "    NZ"),
    16#D6# => ("  DEC", "Z-PAGE,X", 6, 7, "    NZ"),
    16#CA# => ("  DEX", "     IMP", 2, 2, "    NZ"),
    16#88# => ("  DEY", "     IMP", 2, 2, "    NZ"),
    16#4D# => ("  EOR", "     ABS", 4, 5, "    NZ"),
    16#5D# => ("  EOR", "   ABS,X", 4, 5, "    NZ"),
    16#59# => ("  EOR", "   ABS,Y", 4, 5, "    NZ"),
    16#49# => ("  EOR", "     IMM", 2, 3, "    NZ"),
    16#41# => ("  EOR", " (IND,X)", 6, 7, "    NZ"),
    16#51# => ("  EOR", " (IND),Y", 5, 7, "    NZ"),
    16#45# => ("  EOR", "  Z-PAGE", 3, 4, "    NZ"),
    16#55# => ("  EOR", "Z-PAGE,X", 4, 4, "    NZ"),
    16#EE# => ("  INC", "     ABS", 6, 8, "    NZ"),
    16#FE# => ("  INC", "   ABS,X", 7, 8, "    NZ"),
    16#E6# => ("  INC", "  Z-PAGE", 5, 7, "    NZ"),
    16#F6# => ("  INC", "Z-PAGE,X", 6, 7, "    NZ"),
    16#E8# => ("  INX", "     IMP", 2, 2, "    NZ"),
    16#C8# => ("  INY", "     IMP", 2, 2, "    NZ"),
    16#4C# => ("  JMP", "     ABS", 3, 5, "      "),
    16#6C# => ("  JMP", "   (IND)", 5, 8, "      "),
    16#20# => ("  JSR", "     ABS", 6, 5, "      "),
    16#AD# => ("  LDA", "     ABS", 4, 5, "    NZ"),
    16#BD# => ("  LDA", "   ABS,X", 4, 5, "    NZ"),
    16#B9# => ("  LDA", "   ABS,Y", 4, 5, "    NZ"),
    16#A9# => ("  LDA", "     IMM", 2, 3, "    NZ"),
    16#A5# => ("  LDA", "  Z-PAGE", 4, 4, "    NZ"),
    16#A1# => ("  LDA", " (IND,X)", 6, 7, "    NZ"),
    16#B1# => ("  LDA", " (IND),Y", 5, 7, "    NZ"),
    16#B5# => ("  LDA", "Z-PAGE,X", 4, 4, "    NZ"),
    16#AE# => ("  LDX", "     ABS", 4, 5, "    NZ"),
    16#BE# => ("  LDX", "   ABS,Y", 4, 5, "    NZ"),
    16#A2# => ("  LDX", "     IMM", 2, 3, "    NZ"),
    16#A6# => ("  LDX", "  Z-PAGE", 3, 4, "    NZ"),
    16#B6# => ("  LDX", "Z-PAGE,Y", 4, 4, "    NZ"),
    16#AC# => ("  LDY", "     ABS", 4, 5, "    NZ"),
    16#BC# => ("  LDY", "   ABS,X", 4, 5, "    NZ"),
    16#A0# => ("  LDY", "     IMM", 2, 3, "    NZ"),
    16#A4# => ("  LDY", "  Z-PAGE", 3, 4, "    NZ"),
    16#B4# => ("  LDY", "Z-PAGE,X", 4, 4, "    NZ"),
    16#4E# => ("  LSR", "     ABS", 6, 8, "   NZC"),
    16#5E# => ("  LSR", "   ABS,X", 7, 8, "   NZC"),
    16#4A# => ("  LSR", "     ACC", 2, 2, "   NZC"),
    16#46# => ("  LSR", "  Z-PAGE", 5, 7, "   NZC"),
    16#56# => ("  LSR", "Z-PAGE,X", 6, 7, "   NZC"),
    16#EA# => ("  NOP", "     IMP", 2, 2, "      "),
    16#0D# => ("  ORA", "     ABS", 4, 5, "    NZ"),
    16#1D# => ("  ORA", "   ABS,X", 4, 5, "    NZ"),
    16#19# => ("  ORA", "   ABS,Y", 4, 5, "    NZ"),
    16#09# => ("  ORA", "     IMM", 2, 3, "    NZ"),
    16#01# => ("  ORA", " (IND,X)", 6, 7, "    NZ"),
    16#11# => ("  ORA", " (IND),Y", 5, 7, "    NZ"),
    16#05# => ("  ORA", "  Z-PAGE", 3, 4, "    NZ"),
    16#15# => ("  ORA", "Z-PAGE,X", 4, 4, "    NZ"),
    16#48# => ("  PHA", "     IMP", 3, 3, "      "),
    16#08# => ("  PHP", "     IMP", 3, 3, "      "),
    16#68# => ("  PLA", "     IMP", 4, 4, "    NZ"),
    16#28# => ("  PLP", "     IMP", 4, 4, "   DIN"),
    16#03# => ("RESET", "INTERNAL", 0, 6, "  SETI"),
    16#2E# => ("  ROL", "     ABS", 6, 8, "   NZC"),
    16#3E# => ("  ROL", "   ABS,X", 7, 8, "   NZC"),
    16#2A# => ("  ROL", "     ACC", 2, 2, "   NZC"),
    16#26# => ("  ROL", "  Z-PAGE", 5, 7, "   NZC"),
    16#36# => ("  ROL", "Z-PAGE,X", 6, 7, "   NZC"),
    16#6E# => ("  ROR", "     ABS", 6, 8, "   NZC"),
    16#7E# => ("  ROR", "   ABS,X", 7, 8, "   NZC"),
    16#6A# => ("  ROR", "     ACC", 2, 2, "   NZC"),
    16#66# => ("  ROR", "  Z-PAGE", 5, 7, "   NZC"),
    16#76# => ("  ROR", "Z-PAGE,X", 6, 7, "   NZC"),
    16#40# => ("  RTI", "     IMP", 6, 8, "   DIN"),
    16#60# => ("  RTS", "     IMP", 6, 8, "      "),
    16#ED# => ("  SBC", "     ABS", 4, 5, "  NVZC"),
    16#FD# => ("  SBC", "   ABS,X", 4, 5, "  NVZC"),
    16#F9# => ("  SBC", "   ABS,Y", 4, 5, "  NVZC"),
    16#E9# => ("  SBC", "     IMM", 2, 3, "  NVZC"),
    16#E1# => ("  SBC", " (IND,X)", 6, 7, "  NVZC"),
    16#F1# => ("  SBC", " (IND),Y", 5, 7, "  NVZC"),
    16#E5# => ("  SBC", "  Z-PAGE", 3, 4, "  NVZC"),
    16#F5# => ("  SBC", "Z-PAGE,X", 4, 4, "  NVZC"),
    16#38# => ("  SEC", "     IMP", 2, 2, "  SETC"),
    16#F8# => ("  SED", "     IMP", 2, 2, "  SETD"),
    16#78# => ("  SEI", "     IMP", 2, 2, "  SETI"),
    16#8D# => ("  STA", "     ABS", 4, 5, "      "),
    16#99# => ("  STA", "   ABS,Y", 5, 5, "      "),
    16#81# => ("  STA", " (IND,X)", 6, 7, "      "),
    16#91# => ("  STA", " (IND),Y", 6, 7, "      "),
    16#85# => ("  STA", "  Z-PAGE", 3, 4, "      "),
    16#95# => ("  STA", "Z-PAGE,X", 4, 4, "      "),
    16#9D# => ("  STA", "   ABS,X", 5, 5, "      "),
    16#43# => ("S_IRQ", "INTERNAL", 0, 8, "  SETI"),
    16#33# => ("S_NMI", "INTERNAL", 0, 8, "  SETI"),
    16#8E# => ("  STX", "     ABS", 4, 5, "      "),
    16#86# => ("  STX", "  Z-PAGE", 3, 4, "      "),
    16#96# => ("  STX", "Z-PAGE,Y", 4, 4, "      "),
    16#8C# => ("  STY", "     ABS", 4, 5, "      "),
    16#84# => ("  STY", "  Z-PAGE", 3, 4, "      "),
    16#94# => ("  STY", "Z-PAGE,X", 4, 4, "      "),
    16#AA# => ("  TAX", "     IMP", 2, 2, "    NZ"),
    16#A8# => ("  TAY", "     IMP", 2, 2, "    NZ"),
    16#BA# => ("  TSX", "     IMP", 2, 2, "    NZ"),
    16#8A# => ("  TXA", "     IMP", 2, 2, "    NZ"),
    16#9A# => ("  TXS", "     IMP", 2, 2, "      "),
    16#98# => ("  TYA", "     IMP", 2, 3, "    NZ"),
    others => ("     ", "        ", 0, 0, "      "));
  -- pragma translate_on

end package body free6502_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
