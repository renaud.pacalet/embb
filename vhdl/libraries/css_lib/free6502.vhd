----------------------------------------------------------------------------
--  The Free IP Project
--  VHDL DES Core
--  (c) 1999, The Free IP Project and David Kessner
--
--
--  FREE IP GENERAL PUBLIC LICENSE
--  TERMS AND CONDITIONS FOR USE, COPYING, DISTRIBUTION, AND MODIFICATION
--
--  1.  You may copy and distribute verbatim copies of this core, as long
--      as this file, and the other associated files, remain intact and
--      unmodified.  Modifications are outlined below.  Also, see the
--      import/export warning above for further restrictions on
--      distribution.
--  2.  You may use this core in any way, be it academic, commercial, or
--      military.  Modified or not.  
--  3.  Distribution of this core must be free of charge.  Charging is
--      allowed only for value added services.  Value added services
--      would include copying fees, modifications, customizations, and
--      inclusion in other products.
--  4.  If a modified source code is distributed, the original unmodified
--      source code must also be included (or a link to the Free IP web
--      site).  In the modified source code there must be clear
--      identification of the modified version.
--  5.  Visit the Free IP web site for additional information.
--      http://www.free-ip.com
--
----------------------------------------------------------------------------

--pragma translate_off
use std.textio.all;
--pragma translate_on

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.microcode.all;
use work.free6502_pkg.all;
--pragma translate_off
use work.util_pkg.all;
--pragma translate_on

entity free6502 is
--pragma translate_off
  generic(debug: boolean := false);
--pragma translate_on
  port(clk:     in  std_ulogic;
       reset:   in  std_ulogic;
       irq:     in  std_ulogic;
       nmi:     in  std_ulogic;
       add:     out std_ulogic_vector(15 downto 0);
       din:     in  std_ulogic_vector(7 downto 0);
       dout:    out std_ulogic_vector(7 downto 0);
       dout_oe: out std_ulogic;
       we:      out std_ulogic;
       rd:      out std_ulogic;
       cs:      in  std_ulogic;
       pc:      out std_ulogic_vector(15 downto 0);     
       sp:      out std_ulogic_vector(7 downto 0);      
       a:       out std_ulogic_vector(7 downto 0);      
       x:       out std_ulogic_vector(7 downto 0);      
       y:       out std_ulogic_vector(7 downto 0);      
       c:       out std_ulogic; 
       z:       out std_ulogic; 
       i:       out std_ulogic; 
       d:       out std_ulogic; 
       b:       out std_ulogic; 
       v:       out std_ulogic; 
       n:       out std_ulogic; 
       sync:    out std_ulogic);
end free6502;

architecture rtl of free6502 is

  -- internal state info
  signal state: states;
  signal step: u_unsigned(2 downto 0);

  -- registered inputs
  signal nmi_reg1: std_ulogic;
  signal nmi_reg2: std_ulogic;
  signal irq_reg: std_ulogic;
  signal data_in: u_unsigned(7 downto 0);

  -- microcode rom outputs
  signal done: mct_done;
  signal addr_op: mct_addr_op;
  signal din_le: mct_din_le;
  signal rd_en: mct_rd_en;
  signal dout_op: mct_dout_op;
  signal dint1_op: mct_dint1_op;
  signal dint2_op: mct_dint2_op;
  signal dint3_op: mct_dint3_op;
  signal pc_op: mct_pc_op;
  signal sp_op: mct_sp_op;
  signal alu1: mct_alu1;
  signal alu2: mct_alu2;
  signal alu_op: mct_alu_op;
  signal a_le: mct_a_le;
  signal x_le: mct_x_le;
  signal y_le: mct_y_le;
  signal flag_op: mct_flag_op;

  -- internal registers
  signal a_reg: u_unsigned(7 downto 0);
  signal x_reg: u_unsigned(7 downto 0);
  signal y_reg: u_unsigned(7 downto 0);
  signal dint1: u_unsigned(7 downto 0);
  signal dint2: u_unsigned(7 downto 0);
  signal dint3: u_unsigned(7 downto 0);
  signal opcode_reg: u_unsigned(7 downto 0);
  signal sp_s: u_unsigned(7 downto 0);
  signal pc_s: u_unsigned(15 downto 0);
  signal n_flag: std_ulogic;
  signal v_flag: std_ulogic;
  signal b_flag: std_ulogic;
  signal d_flag: std_ulogic;
  signal i_flag: std_ulogic;
  signal z_flag: std_ulogic;
  signal c_flag: std_ulogic;

  -- combinotorial signals
  signal opcode: u_unsigned(7 downto 0);
  signal alu_in1: u_unsigned(8 downto 0);
  signal alu_in2: u_unsigned(8 downto 0);
  signal alu_out: u_unsigned(8 downto 0);
  signal alu_add: u_unsigned(8 downto 0);
  signal alu_add_in2: u_unsigned(8 downto 0);
  signal alu_add_cin: std_ulogic;
--  signal addr_out_d: u_unsigned(15 downto 0);

  -- misc signals
  signal first_run: std_ulogic;
  signal fetch_d: std_ulogic;
  signal data_out: u_unsigned(7 downto 0);
  signal data_oe: std_ulogic;
  signal addr_out: u_unsigned(15 downto 0);
  signal we_out: std_ulogic;
  signal nmi_event: std_ulogic;
  signal sync_local: std_ulogic;
  -- pragma synthesis_off
  signal instruction: optype;
  -- pragma synthesis_on

begin

  sp <= std_ulogic_vector(sp_s);
  pc <= std_ulogic_vector(pc_s);
  a <= std_ulogic_vector(a_reg);
  x <= std_ulogic_vector(x_reg);
  y <= std_ulogic_vector(y_reg);
  c <= c_flag;
  z <= z_flag;
  i <= i_flag;
  d <= d_flag;
  b <= b_flag;
  v <= v_flag;
  n <= n_flag;

  -- the sync output
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        sync_local <= '0';
      else
        if cs = '1' then
          case state is
            when reset1 =>
              sync_local <= '0';
            when reset2 =>
              sync_local <= '0';
            when fetch =>
              sync_local <= '0';
            when start_irq =>
              sync_local <= '0';
            when start_nmi =>
              sync_local <= '0';
            when run =>
              if done = mc_done then
                if nmi_event = '1' then
                  sync_local <= '0';
                elsif i_flag = '0' and irq_reg = '1' then
                  sync_local <= '0';
                else
                  sync_local <= '1';
                end if;
              end if;
            when others =>
              sync_local <= '0';
          end case;
        end if;
      end if;
    end if;
  end process;
  sync <= sync_local;

  -- the main state machine
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        state <= reset1;
      else
        if cs = '1' then
          case state is
            when reset1 =>
              state <= reset2;
            when reset2 =>
              state <= run;
            when fetch =>
              state <= run;
            when start_irq =>
              state <= run;
            when start_nmi =>
              state <= run;
            when run =>
              if done = mc_done then
                if nmi_event = '1' then
                  state <= start_nmi;
                elsif i_flag = '0' and irq_reg = '1' then
                  state <= start_irq;
                else
                  state <= fetch;
                end if;
              end if;
            when others =>
              state <= reset1;
          end case;
        end if;
      end if;
    end if;
  end process;

  -- the microcode step counter
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        step <= "000";
      else
        if cs = '1' then
          case state is
            when reset1 =>
              step <= "000";
            when reset2 =>
              step <= "000";
            when fetch =>
              step <= "000";
            when start_irq =>
              step <= "000";
            when start_nmi =>
              step <= "000";
            when run =>
              step <= step + 1;
            when others =>
              step <= step + 1;
          end case;
        end if;
      end if;
    end if;
  end process;

  -- the input registers
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        data_in <= "00000000";
      else
        if cs = '1' then
          if din_le = mc_en or state = fetch then
            data_in <= u_unsigned(din);
          end if;
        end if;
      end if;
    end if;
  end process;

  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        irq_reg <= '0';
      else
        irq_reg <= irq;
      end if;
    end if;
  end process;

  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        nmi_reg1 <= '0';
        nmi_reg2 <= '0';
      else
        nmi_reg1 <= nmi;
        nmi_reg2 <= nmi_reg1;
      end if;
    end if;
  end process;

  -- the nmi __rising_edge__ detect
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        nmi_event <= '0';
      else
        if nmi_reg1 = '1' and nmi_reg2 = '0' then
          nmi_event <= '1';
        elsif state = start_nmi then
          nmi_event <= '0';
        end if;
      end if;
    end if;
  end process;

  -- the first run signal.  active on the first run clock
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        first_run <= '0';
      else
        if cs = '1' then
          case state is
            when reset2 =>
              first_run <= '0';
            when fetch =>
              first_run <= '1';
            when start_irq =>
              first_run <= '0';
          -- was '1' -- bjs 09/05/99
            when start_nmi =>
              first_run <= '0';
          -- was '1' -- bjs 09/05/99
            when run =>
              first_run <= '0';
            when others =>
              first_run <= '0';
          end case;
        end if;
      end if;
    end if;
  end process;

  -- the fetch_d signal.  active one clock after a fetch cycle
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        fetch_d <= '0';
      else
        if cs = '1' then
          if state = fetch then
            fetch_d <= '1';
          else
            fetch_d <= '0';
          end if;
        end if;
      end if;
    end if;
  end process;

  -- the opcode register and opcode decode logic
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        opcode_reg <= "00000000";
      else
        if cs = '1' then
          case state is
            when reset2 =>
              opcode_reg <= reset_opcode;
            when start_irq =>
              opcode_reg <= irq_opcode;
            when start_nmi =>
              opcode_reg <= nmi_opcode;
            when run =>
              if first_run = '1' then
                opcode_reg <= data_in;
              end if;
            when others =>
              null; -- do nothing
          end case;
        end if;
      end if;
    end if;
  end process;
  opcode <=   data_in when fetch_d = '1' else opcode_reg;
  -- debug helper
  -- pragma synthesis_off
  g_debug: if debug generate
    instruction <= instruction_set(to_integer(opcode));
    process
      variable l: line;
    begin
      wait until rising_edge(clk) and sync_local = '1' and cs = '1';
      wait until falling_edge(clk);
      write(l, instruction.mnemonic);
      writeline(output, l);
    end process;
  end generate g_debug;
  -- pragma synthesis_on

  -- the microcode rom
  mc_rom0: entity work.mc_rom
    port map(
      opcode   => opcode,
      step     => step,
      done     => done,
      addr_op  => addr_op,
      din_le   => din_le,
      rd_en    => rd_en,
      dout_op  => dout_op,
      dint1_op => dint1_op,
      dint2_op => dint2_op,
      dint3_op => dint3_op,
      pc_op    => pc_op,
      sp_op    => sp_op,
      alu1     => alu1,
      alu2     => alu2,
      alu_op   => alu_op,
      a_le     => a_le,
      x_le     => x_le,
      y_le     => y_le,
      flag_op  => flag_op
    );

  -- the program counter
  process(clk)
    variable pc_add: u_unsigned(15 downto 0);
    variable pc_inc: u_unsigned(15 downto 0);
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        pc_s <= "0000000000000000";
      else
        if cs = '1' then
        -- sept 23, 1999 -- fixed by david kessner, reported by bill seiler
        -- was:  pc_add <= pc_s + data_in
        -- now:  pc_add <= pc_s + a sign extended version of data_in
          pc_add := pc_s + (data_in(7) & data_in(7) & data_in(7) & data_in(7) &
                    data_in(7) & data_in(7) & data_in(7) & data_in(7) & data_in);
          pc_inc := pc_s + 1;
          if state = fetch then
            pc_s <= pc_inc;
          else
            case pc_op is
              when mc_nop =>
                null; -- do nothing
              when mc_inc =>
                pc_s <= pc_inc;
              when mc_bcc =>
                if c_flag = '0' then
                  pc_s <= pc_add;
                end if;
              when mc_bcs =>
                if c_flag = '1' then
                  pc_s <= pc_add;
                end if;
              when mc_beq =>
                if z_flag = '1' then
                  pc_s <= pc_add;
                end if;
              when mc_bne =>
                if z_flag = '0' then
                  pc_s <= pc_add;
                end if;
              when mc_bmi =>
                if n_flag = '1' then
                  pc_s <= pc_add;
                end if;
              when mc_bpl =>
                if n_flag = '0' then
                  pc_s <= pc_add;
                end if;
              when mc_bvc =>
                if v_flag = '0' then
                  pc_s <= pc_add;
                end if;
              when mc_bvs =>
                if v_flag = '1' then
                  pc_s <= pc_add;
                end if;
              when mc_split =>
                pc_s <= data_in & dint1;
              when others =>
                null; -- do nothing
            end case;
          end if;
        end if;
      end if;
    end if;
  end process;

  -- data output logic
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        data_out <= "00000000";
        data_oe <= '0';
        we_out <= '0';
      else
        if cs = '1' then
          case dout_op is
            when mc_nop =>
              data_oe <= '0';
              we_out <= '0';
              data_out <= dint3;
            when mc_dint3 =>
              data_oe <= '1';
              we_out <= '1';
              data_out <= dint3;
            when mc_pch =>
              data_oe <= '1';
              we_out <= '1';
              data_out <= pc_s(15 downto 8);
            when mc_pcl =>
              data_oe <= '1';
              we_out <= '1';
              data_out <= pc_s(7 downto 0);
            when mc_p_reg =>
              data_oe <= '1';
              we_out <= '1';
              data_out <= n_flag & v_flag & '1' & b_flag & d_flag &
                          i_flag & z_flag & c_flag;
            when mc_a_reg =>
              data_oe <= '1';
              we_out <= '1';
              data_out <= a_reg;
            when mc_x_reg =>
              data_oe <= '1';
              we_out <= '1';
              data_out <= x_reg;
            when mc_y_reg =>
              data_oe <= '1';
              we_out <= '1';
              data_out <= y_reg;
            when others =>
              data_oe <= '0';
              we_out <= '0';
              data_out <= dint3;
          end case;
        end if;
      end if;
    end if;
  end process;
  dout <=  std_ulogic_vector(data_out);
  dout_oe <=  data_oe;
  we <=  we_out;

  -- generate the rd signals
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        rd <= '0';
      else
        if cs = '1' then
          case state is
            when reset1 =>
              rd <= '0';
            when reset2 =>
              rd <= '0';
            when fetch =>
              rd <= '1';
            when start_irq =>
              rd <= '0';
            when start_nmi =>
              rd <= '0';
            when run =>
              if done = mc_done then
                if nmi_event = '1' then
                  rd <= '0';
                elsif i_flag = '0' and irq_reg = '1' then
                  rd <= '0';
                else
                  rd <= '1';
                end if;
              elsif rd_en = mc_read then
                rd <= '1';
              else
                rd <= '0';
              end if;
            when others =>
              rd <= '0';
          end case;
        end if;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- revised address output block
  -------------------------------
  -- these revisons to the address output block are intended to improve
  -- synthesis results for both area and speed.  they intended to
  -- accomplish the following:
  -- - make the use of a single adder very clear to the synthesis tool.
  -- - move all decoding and muxing in front of the adder.  this allows
  --   the synthesis more flexibility in optimizing an balancing propagation
  --   paths.
  -- ed beers (sreeb@beers.nu) 9/14/99
  -----------------------------------------------------------------------------
  -- the address output logic
  process(clk)
    variable addr_add_1: u_unsigned(15 downto 0);
    variable addr_add_2: u_unsigned(7 downto 0);
    variable addr_add_cin: std_ulogic;
    variable addr_out_low: u_unsigned(8 downto 0);
    variable addr_out_high: u_unsigned(7 downto 0);
    variable eight_bit_flag: std_ulogic;
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        addr_out <= (others => '0');
      else
        if cs = '1' then
        -- default for adder
          addr_add_2 := (others => '0');
          addr_add_cin := '0';
          eight_bit_flag := '0';
          if done = mc_done then
          --addr_out <= pc_s;
            addr_add_1 := pc_s;
          elsif state = fetch then
          --addr_out <= pc_s + 1;
            addr_add_1 := pc_s;
            addr_add_cin := '1';
          else
            case addr_op is
              when mc_nop =>
              --addr_out <= pc_s;
                addr_add_1 := pc_s;
              when mc_pc_p =>
              --addr_out <= pc_s + 1;
                addr_add_1 := pc_s;
                addr_add_cin := '1';
              when mc_split =>
              --addr_out <= data_in & dint1;
                addr_add_1 := data_in & dint1;
              when mc_split_p =>
              --addr_out <= (data_in & dint1) + 1;
                addr_add_1 := data_in & dint1;
                addr_add_cin := '1';
              when mc_split_x =>
              --addr_out <= (data_in & dint1) + x_reg;
                addr_add_1 := data_in & dint1;
                addr_add_2 := x_reg;
              when mc_split_y =>
              --addr_out <= (data_in & dint1) + y_reg;
                addr_add_1 := data_in & dint1;
                addr_add_2 := y_reg;
              when mc_din_z =>
              --addr_out <= ("00000000" & data_in);
                addr_add_1 := "00000000" & data_in;
              when mc_din_zp =>
              --addr_out <= ("00000000" & data_in) + 1;
                addr_add_1 := "00000000" & data_in;
                addr_add_cin := '1';
              when mc_din_zx =>
              --addr_out <= ("00000000" & data_in) + x_reg;
                addr_add_1 := "00000000" & data_in;
                addr_add_2 := x_reg;
                eight_bit_flag := '1';
              when mc_din_zxp =>
              --addr_out <= ("00000000" & data_in) + x_reg + 1;
                addr_add_1 := "00000000" & data_in;
                addr_add_2 := x_reg;
                addr_add_cin := '1';
                eight_bit_flag := '1';
              when mc_din_zy =>
              --addr_out <= ("00000000" & data_in) + y_reg;
                addr_add_1 := "00000000" & data_in;
                addr_add_2 := y_reg;
                eight_bit_flag := '1';
              when mc_dint16 =>
              --addr_out <= dint2 & dint1;
                addr_add_1 := dint2 & dint1;
              when mc_dint16_x =>
              --addr_out <= dint2 & dint1 + x_reg;
                addr_add_1 := dint2 & dint1;
                addr_add_2 := x_reg;
              when mc_dint1_z =>
              --addr_out_d <= ("00000000" & dint1);
                addr_add_1 := "00000000" & dint1;
              when mc_dint1_zx =>
              --addr_out <= ("00000000" & dint1) + x_reg;
                addr_add_1 := "00000000" & dint1;
                addr_add_2 := x_reg;
              when mc_sp =>
              --addr_out <= stack_page & sp_s;
                addr_add_1 := stack_page & sp_s;
              when mc_v_nmi1 =>
              --addr_out <= vect_nmi1;
                addr_add_1 := vect_nmi1;
              when mc_v_nmi2 =>
              --addr_out <= vect_nmi2;
                addr_add_1 := vect_nmi2;
              when mc_v_reset1 =>
              --addr_out <= vect_reset1;
                addr_add_1 := vect_reset1;
              when mc_v_reset2 =>
              --addr_out <= vect_reset2;
                addr_add_1 := vect_reset2;
              when mc_v_irq1 =>
              --addr_out <= vect_irq1;
                addr_add_1 := vect_irq1;
              when mc_v_irq2 =>
              --addr_out <= vect_irq2;
                addr_add_1 := vect_irq2;
              when others =>
              --addr_out <= pc_s;
                addr_add_1 := pc_s;
            end case;
          end if;
        --addr_out <= addr_add_1 + addr_add_2 + addr_add_cin;      
          addr_out_low := ("0" & addr_add_1(7 downto 0)) + ("0" & addr_add_2) +
          addr_add_cin;
          addr_out_high := addr_add_1(15 downto 8) +
          (addr_out_low(8) and not eight_bit_flag);
          addr_out <= addr_out_high(7 downto 0) & addr_out_low(7 downto 0);
        end if;
      end if;
    end if;
  end process;
  add <= std_ulogic_vector(addr_out);

  -- the dint registers
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        dint1 <= "00000000";
      else
        if cs = '1' then
          if dint1_op = mc_din then
            dint1 <= data_in;
          end if;
        end if;
      end if;
    end if;
  end process;

  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        dint2 <= "00000000";
      else
        if cs = '1' then
          if dint2_op = mc_din then
            dint2 <= data_in;
          end if;
        end if;
      end if;
    end if;
  end process;

  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        dint3 <= "00000000";
      else
        if cs = '1' then
          if dint3_op = mc_alu then
            dint3 <= alu_out(7 downto 0);
          end if;
        end if;
      end if;
    end if;
  end process;

  -- the stack pointer
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        sp_s <= "11111111";
      else
        if cs = '1' then
          case sp_op is
            when mc_nop =>
              null; -- do nothing
            when mc_push =>
              sp_s <= sp_s - 1;
            when mc_pop =>
              sp_s <= sp_s + 1;
            when mc_x_reg =>
              sp_s <= x_reg;
            when others =>
              null; -- do nothing
          end case;
        end if;
      end if;
    end if;
  end process;

  -- the registers
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        a_reg <= "00000000";
      else
        if cs = '1' then
          if a_le = mc_le then
            a_reg <= alu_out(7 downto 0);
          end if;
        end if;
      end if;
    end if;
  end process;

  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        x_reg <= "00000000";
      else
        if cs = '1' then
          if x_le = mc_le then
            x_reg <= alu_out(7 downto 0);
          end if;
        end if;
      end if;
    end if;
  end process;

  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        y_reg <= "00000000";
      else
        if cs = '1' then
          if y_le = mc_le then
            y_reg <= alu_out(7 downto 0);
          end if;
        end if;
      end if;
    end if;
  end process;

  -- the alu input muxes
  process(a_reg, alu1, data_in, x_reg, y_reg)
  begin
    case alu1 is
      when mc_a_reg =>
        alu_in1 <= ("0" & a_reg);
      when mc_x_reg =>
        alu_in1 <= ("0" & x_reg);
      when mc_y_reg =>
        alu_in1 <= ("0" & y_reg);
      when others =>
        alu_in1 <= ("0" & data_in);
    end case;
  end process;

  process(alu2, data_in, sp_s)
  begin
    case alu2 is
      when mc_one =>
        alu_in2 <= "000000001";
      when mc_sp_reg =>
        alu_in2 <= ("0" & sp_s);
      when others =>
        alu_in2 <= ("0" & data_in);
    end case;
  end process;

  -- the alu adder
  alu_add <=  alu_in1 + alu_add_in2 + alu_add_cin;

  -- the alu itself    this is purely combinatorial logic
  process(alu_add, alu_in1, alu_in2, alu_op, c_flag)
  begin
    -- default for alu_add inputs
    alu_add_in2 <= alu_in2;
    alu_add_cin <= c_flag;
    case alu_op is
      when mc_pass1 =>
        alu_out <= alu_in1;
      when mc_pass2 =>
        alu_out <= alu_in2;
      when mc_add =>
        --alu_out <= alu_in1 + alu_in2;
        alu_add_in2 <= alu_in2;
        alu_add_cin <= '0';
        alu_out <= alu_add;
      when mc_addc =>
        --alu_out <= alu_in1 + alu_in2 + ("00000000" & c_flag);
        alu_add_in2 <= alu_in2;
        alu_add_cin <= c_flag;
        alu_out <= alu_add;
      when mc_sub =>
        --alu_out <= alu_in1 - alu_in2;
        alu_add_in2 <= not alu_in2;
        alu_add_cin <= '1';
        alu_out <= (not alu_add(8)) & alu_add(7 downto 0);
      when mc_subb =>
        --alu_out <= alu_in1 - alu_in2 - ("00000000" & c_flag);
        alu_add_in2 <= not alu_in2;
        alu_add_cin <= c_flag;
        alu_out <= (not alu_add(8)) & alu_add(7 downto 0);
      when mc_bit_and =>
        alu_out <= alu_in1 and alu_in2;
      when mc_bit_or =>
        alu_out <= alu_in1 or alu_in2;
      when mc_bit_xor =>
        alu_out <= alu_in1 xor alu_in2;
      when mc_bit_asl =>
        alu_out <= alu_in1(7 downto 0) & "0";
      when mc_bit_lsr =>
        alu_out <= alu_in1(0) & "0" & alu_in1(7 downto 1);
      when mc_bit_rol =>
        alu_out <= alu_in1(7 downto 0) & c_flag;
      when mc_bit_ror =>
        alu_out <= alu_in1(0) & c_flag & alu_in1(7 downto 1);
      when others =>
        alu_out <= alu_in1;
    end case;
  end process;

  -- the flag stuff
  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        n_flag <= '0';
      else
        if cs = '1' then
          case flag_op is
            when mc_nop =>
              null; -- do nothing
            when mc_nvzc =>
              n_flag <= alu_out(7);
            when mc_nz =>
              n_flag <= alu_out(7);
            when mc_nzc =>
              n_flag <= alu_out(7);
            when mc_bit =>
              n_flag <= alu_out(7);
            when mc_din =>
              n_flag <= data_in(7);
            when others =>
              null; -- do nothing
          end case;
        end if;
      end if;
    end if;
  end process;

  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        v_flag <= '0';
      else
        if cs = '1' then
          case flag_op is
            when mc_nop =>
              null; -- do nothing
            when mc_nvzc =>
              if alu_op = mc_add or alu_op = mc_addc then
                if alu_in1(7) = alu_in2(7) and
                alu_in1(7) /= alu_add(7) then
                  v_flag <= '1';
                else
                  v_flag <= '0';
                end if;
              elsif alu_op = mc_sub or alu_op = mc_subb then
                if alu_in1(7) /= alu_in2(7) and
                alu_in1(7) /= alu_add(7) then
                  v_flag <= '1';
                else
                  v_flag <= '0';
                end if;
              end if;
            when mc_clearv =>
              v_flag <= '0';
            when mc_din =>
              v_flag <= data_in(6);
            when mc_bit =>
              v_flag <= data_in(6);
            when others =>
              null; -- do nothing
          end case;
        end if;
      end if;
    end if;
  end process;

  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        b_flag <= '0';
      else
        if cs = '1' then
          case flag_op is
            when mc_nop =>
              null; -- do nothing
            when mc_setb =>
              b_flag <= '1';
            when mc_din =>
              b_flag <= data_in(4);
            when others =>
              null; -- do nothing
          end case;
        end if;
      end if;
    end if;
  end process;

  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        d_flag <= '0';
      else
        if cs = '1' then
          case flag_op is
            when mc_nop =>
              null; -- do nothing
            when mc_cleard =>
              d_flag <= '0';
            when mc_din =>
              d_flag <= data_in(3);
            when mc_setd =>
              d_flag <= '1';
            when others =>
              null; -- do nothing
          end case;
        end if;
      end if;
    end if;
  end process;

  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        i_flag <= '1';
      else
        if cs = '1' then
          case flag_op is
            when mc_nop =>
              null; -- do nothing
            when mc_seti =>
              i_flag <= '1';
            when mc_cleari =>
              i_flag <= '0';
            when mc_din =>
              i_flag <= data_in(2);
            when others =>
              null; -- do nothing
          end case;
        end if;
      end if;
    end if;
  end process;

  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        z_flag <= '1';
      else
        if cs = '1' then
          case flag_op is
            when mc_nop =>
              null; -- do nothing
            when mc_nvzc =>
              -- pragma synthesis_off
              if not is_x(alu_out) then
              -- pragma synthesis_on
                if alu_out(7 downto 0) = "00000000" then
                  z_flag <= '1';
                else
                  z_flag <= '0';
                end if;
              -- pragma synthesis_off
              end if;
              -- pragma synthesis_on
            when mc_nz =>
              -- pragma synthesis_off
              if not is_x(alu_out) then
              -- pragma synthesis_on
                if alu_out(7 downto 0) = "00000000" then
                  z_flag <= '1';
                else
                  z_flag <= '0';
                end if;
              -- pragma synthesis_off
              end if;
              -- pragma synthesis_on
            when mc_nzc =>
              -- pragma synthesis_off
              if not is_x(alu_out) then
              -- pragma synthesis_on
                if alu_out(7 downto 0) = "00000000" then
                  z_flag <= '1';
                else
                  z_flag <= '0';
                end if;
              -- pragma synthesis_off
              end if;
              -- pragma synthesis_on
            when mc_bit =>
              -- pragma synthesis_off
              if not is_x(alu_out) then
              -- pragma synthesis_on
                if alu_out(7 downto 0) = "00000000" then
                  z_flag <= '1';
                else
                  z_flag <= '0';
                end if;
              -- pragma synthesis_off
              end if;
              -- pragma synthesis_on
            when mc_din =>
              z_flag <= data_in(1);
            when others =>
              null; -- do nothing
          end case;
        end if;
      end if;
    end if;
  end process;

  process(clk)
  begin
    if clk'event and clk = '1' then
      if reset = '1' then
        c_flag <= '0';
      else
        if cs = '1' then
          case flag_op is
            when mc_nop =>
              null; -- do nothing
            when mc_nvzc =>
              c_flag <= alu_out(8);
            when mc_nzc =>
              c_flag <= alu_out(8);
            when mc_clearc =>
              c_flag <= '0';
            when mc_din =>
              c_flag <= data_in(0);
            when mc_setc =>
              c_flag <= '1';
            when others =>
              null; -- do nothing
          end case;
        end if;
      end if;
    end if;
  end process;

end architecture rtl;
