#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

gh-IGNORE	+= css_lib.dma css_lib.dma_sim css_lib.css css_lib.css_sim

css_lib.css: \
	global_lib.global \
	css_lib.bitfield_pkg \
	css_lib.css_pkg \
	css_lib.requests_fifo \
	css_lib.responses_fifo \
	css_lib.via \
	css_lib.ctrl \
	css_lib.dma \
	css_lib.uca \
	css_lib.uc

css_lib.css_pkg: \
	global_lib.global \
	global_lib.sim_utils \
	css_lib.bitfield_pkg

css_lib.css_sim: \
	random_lib.rnd \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils \
	css_lib.bitfield_pkg \
	css_lib.css_pkg \
	css_lib.css

css_lib.ctrl: \
	global_lib.global \
	global_lib.utils \
	css_lib.bitfield_pkg \
	css_lib.css_pkg

css_lib.dma: \
	global_lib.global \
	global_lib.utils \
	css_lib.css_pkg \
	css_lib.dma_pkg \
        global_lib.fifo \
	css_lib.shifter

css_lib.dma_pkg : \
	global_lib.global \
	css_lib.css_pkg

css_lib.dma_sim : \
	random_lib.rnd \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils \
	global_lib.css_emulator_pkg \
	memories_lib.memory \
	css_lib.dma_pkg \
	css_lib.css_pkg \
	css_lib.dma \
	css_lib.spshiftram \
	css_lib.local_mss \
	css_lib.mssif

css_lib.free6502: \
	css_lib.microcode \
	css_lib.free6502_pkg \
	css_lib.util_pkg

css_lib.local_mss: \
	random_lib.rnd \
	global_lib.global \
	global_lib.utils \
	memories_lib.ram_pkg \
	memories_lib.spram

css_lib.mssif: \
	random_lib.rnd \
	global_lib.global \
	global_lib.utils

css_lib.requests_fifo: \
	global_lib.global \
	global_lib.utils

css_lib.requests_fifo_sim: \
	random_lib.rnd \
	global_lib.global \
	global_lib.sim_utils \
	css_lib.requests_fifo

css_lib.responses_fifo: \
	global_lib.global \
	global_lib.utils

css_lib.responses_fifo_sim: \
	random_lib.rnd \
	global_lib.global \
	global_lib.sim_utils \
	css_lib.responses_fifo

css_lib.shifter: \
	global_lib.global \
        css_lib.dma_pkg \
	global_lib.utils

css_lib.spram: \
	global_lib.global \
	global_lib.utils \
	memories_lib.ram_pkg

css_lib.spshiftram: \
	global_lib.global \
	global_lib.utils \
	memories_lib.ram_pkg

css_lib.uc: \
	global_lib.global \
	css_lib.bitfield_pkg \
	css_lib.css_pkg \
	css_lib.free6502

css_lib.uca: \
	global_lib.global \
	css_lib.css_pkg

css_lib.via: \
	global_lib.global \
	global_lib.utils \
	css_lib.css_pkg

$(addprefix ms-sim.css_lib.,requests_fifo_sim responses_fifo_sim css_sim dma_sim): MSSIMFLAGS += -do 'run -all; quit'
ms-sim.css_lib: $(addprefix ms-sim.css_lib.,requests_fifo_sim responses_fifo_sim css_sim dma_sim)
ms-sim: ms-sim.css_lib

$(addprefix pr-syn.css_lib.,dma css ctrl): $(hwprj_db_path.css_lib)/bitfield_pkg.vhd
pr-syn.css_lib: $(addprefix pr-syn.css_lib.,dma css ctrl b free6502 requests_fifo responses_fifo shifter uc uca via)
pr-syn: pr-syn.css_lib

$(addprefix rc-syn.css_lib.,dma css ctrl): $(hwprj_db_path.css_lib)/bitfield_pkg.vhd
rc-syn.css_lib: $(addprefix rc-syn.css_lib.,dma css ctrl b free6502 requests_fifo responses_fifo shifter uc uca via)
rc-syn: rc-syn.css_lib

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
