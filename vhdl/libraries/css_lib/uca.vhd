--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief UC arbiter

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;

use work.css_pkg.all;

entity uca is
  generic(n0 :natural := 1;
          n1 :natural := 1);
  port(clk:      in  std_ulogic;
       srstn:    in std_ulogic;
       ce:       in std_ulogic;
       ctrl2uca: in  ctrl2uca_type;
       uca2ctrl: out uca2ctrl_type;
       uc2uca:   in  uc2uca_type;
       uca2uc:   out uca2uc_type;
       mss2uca:  in  mss2uc_type;
       uca2mss:  out uc2mss_type);
end entity uca;

architecture rtl of uca is

constant ctrl_pipeline_depth : natural := 0;
constant mss_pipeline_depth : natural := n0 + n1;


signal eot, rdy, rdy_r : std_ulogic;

subtype mux_type is natural range 0 to 7;

type stage_type is record
  en : std_ulogic;
  dma : std_ulogic;
  mux : mux_type;
end record;

type stage_v is array (natural range <>) of stage_type;

signal mss_stage_c, mss_stage_r : stage_v(1 to mss_pipeline_depth);
signal ctrl_stage_c, ctrl_stage_r : stage_v(0 to ctrl_pipeline_depth + 1);

-- Fast PAth dedicated signals
type dma_registers_type is record
  src : natural range 0 to 255;
  dst : natural range 0 to 31;
  lenm1 : natural range 0 to 31;
  req_cnt : natural range 0 to 31;
  rsp_cnt : natural range 0 to 31;
  busy: std_ulogic;
  req_done: std_ulogic;
end record;

signal dma_registers, dma_registers_c : dma_registers_type;

begin
  

  p0 : process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then 
        dma_registers.busy <= '0';
        dma_registers.req_done <= '1';
        rdy_r <= '1';
        for i in 1 to mss_pipeline_depth loop
          mss_stage_r(i).en <= '0';
        end loop;
        for i in 0 to ctrl_pipeline_depth + 1 loop
          ctrl_stage_r(i).en <= '0';
        end loop;
      elsif ce = '1' then 
        rdy_r <= rdy or eot;
        mss_stage_r <= mss_stage_c;
        ctrl_stage_r <= ctrl_stage_c;
        dma_registers <= dma_registers_c;
        if ctrl2uca.exec = '1' then 
          dma_registers.src <= ctrl2uca.bs * 256 + ctrl2uca.off;
          dma_registers.dst <= ctrl2uca.dst;
          dma_registers.lenm1 <= ctrl2uca.lenm1;
          dma_registers.busy <= '1';
          dma_registers.req_done <= '0';
          dma_registers.req_cnt <= 0;
          dma_registers.rsp_cnt <= 0;
        end if;
      end if;
      
    end if;
   
  end process p0;


  p1 : process (dma_registers, rdy_r, mss2uca, ctrl2uca, uc2uca, ctrl_stage_r, mss_stage_r)

  variable add : std_ulogic_vector(15 downto 0);
  variable eot_v, rdy_v, nmi : std_ulogic;
  variable dma_registers_v : dma_registers_type;
  variable muxa, muxb : mux_type;
  variable mss_stage_v : stage_v(1 to mss_pipeline_depth);
  variable ctrl_stage_v : stage_v(0 to ctrl_pipeline_depth + 1);

  begin

    add := uc2uca.add;
    nmi := '0';
    uca2mss <= uc2mss_none;
    uca2ctrl <= uca2ctrl_none;
    uca2uc <= uca2uc_none;
    rdy_v := rdy_r;
    eot_v := '0';
    muxa := to_integer(u_unsigned(uc2uca.add(2 downto 0)));
    muxb := 0;
    dma_registers_v := dma_registers;

    uca2mss.wdata <= uc2uca.wdata & uc2uca.wdata & uc2uca.wdata & uc2uca.wdata & uc2uca.wdata & uc2uca.wdata & uc2uca.wdata & uc2uca.wdata;
    uca2ctrl.wdata <= uc2uca.wdata & uc2uca.wdata & uc2uca.wdata & uc2uca.wdata & uc2uca.wdata & uc2uca.wdata & uc2uca.wdata & uc2uca.wdata;

    for i in 1 to mss_pipeline_depth - 1 loop
      mss_stage_v(i + 1) := mss_stage_r(i);
    end loop;
    for i in 0 to ctrl_pipeline_depth loop
      ctrl_stage_v(i + 1) := ctrl_stage_r(i);
    end loop;

    mss_stage_v(1).en := '0';
    ctrl_stage_v(0).en := '0';
    ctrl_stage_v(0).dma := '0';
    mss_stage_v(1).dma := '0';
    mss_stage_v(1).mux := muxa;
    ctrl_stage_v(0).mux := muxa;

    if dma_registers.busy = '1' then
    -- FAST PATH
      rdy_v := '0';
    -- mss request 
      if dma_registers.req_done = '0' then 
        dma_registers_v.src := dma_registers.src + 1; 
        dma_registers_v.req_cnt := dma_registers.req_cnt + 1; 
        uca2mss.en <= '1'; 
        uca2mss.add <= std_ulogic_vector(shift_left((to_unsigned(dma_registers.src, 16)), 3));
        uca2mss.be <= X"FF";  
        uca2mss.rnw <= '1';
        mss_stage_v(1).en := '1'; 
        mss_stage_v(1).dma := '1'; 
        if dma_registers.req_cnt = dma_registers.lenm1 then 
          dma_registers_v.req_done := '1';
        end if;
      end if;
    -- mss responses
      if mss_stage_r(mss_pipeline_depth).en = '1' and mss_stage_r(mss_pipeline_depth).dma = '1' then
        uca2ctrl.en <= '1'; 
        uca2ctrl.rnw <= '0';
        uca2ctrl.be <= x"FF";
        uca2ctrl.add <= dma_registers.dst; 
        uca2ctrl.wdata <=  mss2uca.rdata;
        dma_registers_v.dst := dma_registers.dst + 1; 
        dma_registers_v.rsp_cnt := dma_registers.rsp_cnt + 1; 
        if dma_registers.rsp_cnt = dma_registers.lenm1 then 
          dma_registers_v.busy := '0';
          eot_v := '1';
        end if;
      end if;
    -- END FAST PATH
    elsif uc2uca.en = '1' and rdy_r = '1' then 
    -- REGULAR ACCESSES    
      rdy_v := '1';
      if (add(15 downto 11) = "00000") then 
      -- 2kB area
        uca2mss.rnw <= uc2uca.rnw;
        uca2mss.en <= '1'; 
        uca2mss.add <= uc2uca.add; 
        uca2mss.be(7 - muxa) <= '1'; 
        if uc2uca.rnw = '1' then 
         mss_stage_v(1).en := '1';
         mss_stage_v(1).dma := '0'; 
         rdy_v := '0';
        end if;
      elsif add(15 downto 8) = X"FF" then 
      -- 256B area
        uca2ctrl.rnw <= uc2uca.rnw;
        uca2ctrl.en <= '1'; 
        uca2ctrl.add <= to_integer(u_unsigned(add(7 downto 3))); 
        uca2ctrl.be(7 - muxa) <= '1'; 
        if uc2uca.rnw = '1' then 
          ctrl_stage_v(0).en := '1';
          ctrl_stage_v(0).dma := '0'; 
          rdy_v := '0';
        end if;
      else 
        nmi := '1';
      end if;    
    -- END REGULAR ACCESSES
    end if;


    if ctrl_stage_v(ctrl_pipeline_depth).en = '1' and ctrl_stage_v(ctrl_pipeline_depth).dma = '0' then
      muxb := ctrl_stage_v(ctrl_pipeline_depth).mux;
      rdy_v := '1';
      for i in 0 to 7 loop
        if i = muxb then
          uca2uc.rdata <= ctrl2uca.rdata(63 - i * 8 downto 56 - i * 8);
        end if;
      end loop;
    elsif mss_stage_r(mss_pipeline_depth).en = '1' and mss_stage_r(mss_pipeline_depth).dma = '0' then 
      muxb := mss_stage_r(mss_pipeline_depth).mux;
      rdy_v := '1';
      for i in 0 to 7 loop
        if i = muxb then
          uca2uc.rdata <= mss2uca.rdata(63 - i * 8 downto 56 - i * 8);
        end if;
      end loop;
    end if; 
    

    uca2uc.rdy <= rdy_v;
    rdy <= rdy_v;
    eot <= eot_v;
    uca2ctrl.eot <= eot_v;
    uca2uc.nmi <= nmi or ctrl2uca.nmi or mss2uca.oor;
    dma_registers_c <= dma_registers_v;
    mss_stage_c <= mss_stage_v;
    ctrl_stage_c <= ctrl_stage_v;

  end process p1;

    
end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
