--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--------------------------------------------------------------------------------
-- Description: a single port synchronous shiftable RAM model
--------------------------------------------------------------------------------

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

library memories_lib;
use memories_lib.ram_pkg.all;

entity spshiftram is
  generic(na: natural := 10; -- bit-width of address busses
          nb: positive := 64 -- bit-width of data busses
  );
  port(clk: in  std_ulogic; -- Clock. The RAM samples its inputs on the rising edge of clk.
	 en: in std_ulogic; -- Active high enables
	 shen: in std_ulogic; -- Active high shift enable
	 lnr: in std_ulogic; -- left not right shift
	 size: in natural; -- shift size
	 rnw: in std_ulogic; -- shift size
	 we: in std_ulogic_vector(nb / 8 - 1 downto 0); -- Active high write enables
	 add: in std_ulogic_vector(na - 1 downto 0); -- Address busses
	 din: in std_ulogic_vector(nb - 1 downto 0); -- Input data busses
	 dout: out std_ulogic_vector(nb - 1 downto 0)); -- Output data busses
end entity spshiftram;

architecture arc of spshiftram is

  subtype tmem is std_ulogic_vector(nb * (2**na) - 1 downto 0);
  signal smem: tmem;
  signal dout1: std_ulogic_vector(nb - 1 downto 0);

begin
  
  dout <= dout1;

  process(clk)
    -- Declaration and initialization of the variable representing the spshiftram
    -- array.
    variable m: std_ulogic_vector(nb - 1 downto 0);
    variable mem: tmem;
    variable addv: natural range 0 to 2**na - 1;   -- Integer addresses
    constant no_write: std_ulogic_vector(nb / 8 - 1 downto 0) :=
      (others => '0');
  begin
    if rising_edge(clk) then
      mem := smem;
      -- read operation
      addv := to_integer(u_unsigned(add));
      if en = '1' and rnw = '1' then
        dout1 <= mem(nb * (2**na) - 1 - addv * nb downto nb * (2**na) - (addv + 1) * nb); -- read
      end if;
      -- write operation
      m := mask(din, mem(nb * (2**na) - 1 - addv * nb downto nb * (2**na) - (addv + 1) * nb), we); -- new value
      if en = '1' and rnw = '0' then -- port write
        mem(nb * (2**na) - 1 - addv * nb downto nb * (2**na) - (addv + 1) * nb) := m;
      end if;
      if shen = '1' then 
	if lnr = '1' then 
	 mem := std_ulogic_vector(shift_left(u_unsigned(mem), size));
	else
	 mem := std_ulogic_vector(shift_right(u_unsigned(mem), size));
	end if;
      end if;
      smem <= mem;
    end if;
  end process;



end architecture arc;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
