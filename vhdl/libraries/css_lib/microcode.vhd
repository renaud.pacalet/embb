----------------------------------------------------------------------------
--  The Free IP Project
--  VHDL DES Core
--  (c) 1999, The Free IP Project and David Kessner
--
--
--  FREE IP GENERAL PUBLIC LICENSE
--  TERMS AND CONDITIONS FOR USE, COPYING, DISTRIBUTION, AND MODIFICATION
--
--  1.  You may copy and distribute verbatim copies of this core, as long
--      as this file, and the other associated files, remain intact and
--      unmodified.  Modifications are outlined below.  Also, see the
--      import/export warning above for further restrictions on
--      distribution.
--  2.  You may use this core in any way, be it academic, commercial, or
--      military.  Modified or not.  
--  3.  Distribution of this core must be free of charge.  Charging is
--      allowed only for value added services.  Value added services
--      would include copying fees, modifications, customizations, and
--      inclusion in other products.
--  4.  If a modified source code is distributed, the original unmodified
--      source code must also be included (or a link to the Free IP web
--      site).  In the modified source code there must be clear
--      identification of the modified version.
--  5.  Visit the Free IP web site for additional information.
--      http://www.free-ip.com
--
----------------------------------------------------------------------------
--
----------------------------------------------------------------------------
--  Microcode VHDL file for 6502 CPU.  Created by mcgen.exe
----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package microcode is
  type mct_done is (mc_nop, mc_done);  -- 2 types
  type mct_addr_op is (mc_nop, mc_pc_p, mc_split, mc_split_x, mc_split_y,
    mc_din_zx, mc_din_zxp, mc_din_z, mc_din_zp, mc_dint16, mc_dint16_x,
    mc_dint1_z, mc_dint1_zx, mc_v_irq1, mc_v_irq2, mc_sp, mc_split_p, mc_din_zy,
    mc_v_reset1, mc_v_reset2, mc_v_nmi1, mc_v_nmi2);  -- 22 types
  type mct_din_le is (mc_nop, mc_en);  -- 2 types
  type mct_rd_en is (mc_nop, mc_read);  -- 2 types
  type mct_dout_op is (mc_nop, mc_dint3, mc_pch, mc_pcl, mc_p_reg, mc_a_reg, mc_x_reg, mc_y_reg);  -- 8 types
  type mct_dint1_op is (mc_nop, mc_din);  -- 2 types
  type mct_dint2_op is (mc_nop, mc_din);  -- 2 types
  type mct_dint3_op is (mc_nop, mc_alu);  -- 2 types
  type mct_pc_op is (mc_nop, mc_inc, mc_bcc, mc_bcs, mc_beq, mc_bmi, mc_bne, mc_bpl, mc_split, mc_bvc, mc_bvs);  -- 11 types
  type mct_sp_op is (mc_nop, mc_push, mc_pop, mc_x_reg);  -- 4 types
  type mct_alu1 is (mc_a_reg, mc_din, mc_x_reg, mc_y_reg);  -- 4 types
  type mct_alu2 is (mc_din, mc_one, mc_sp_reg);  -- 3 types
  type mct_alu_op is (mc_pass1, mc_addc, mc_bit_and, mc_bit_asl, mc_sub, mc_bit_xor, mc_add, mc_pass2, mc_bit_lsr, mc_bit_or, mc_bit_rol, mc_bit_ror, mc_subb);  -- 13 types
  type mct_a_le is (mc_nop, mc_le);  -- 2 types
  type mct_x_le is (mc_nop, mc_le);  -- 2 types
  type mct_y_le is (mc_nop, mc_le);  -- 2 types
  type mct_flag_op is (mc_nop, mc_nvzc, mc_nz, mc_nzc, mc_bit, mc_setb, mc_seti, mc_clearc, mc_cleard, mc_cleari, mc_clearv, mc_din, mc_setc, mc_setd);  -- 14 types

  -- microcode is approxamately 35 bits wide.

  component mc_rom
    port (opcode     :in u_unsigned (7 downto 0);
          step       :in u_unsigned (2 downto 0);
          done       :out mct_done;
          addr_op    :out mct_addr_op;
          din_le     :out mct_din_le;
          rd_en      :out mct_rd_en;
          dout_op    :out mct_dout_op;
          dint1_op   :out mct_dint1_op;
          dint2_op   :out mct_dint2_op;
          dint3_op   :out mct_dint3_op;
          pc_op      :out mct_pc_op;
          sp_op      :out mct_sp_op;
          alu1       :out mct_alu1;
          alu2       :out mct_alu2;
          alu_op     :out mct_alu_op;
          a_le       :out mct_a_le;
          x_le       :out mct_x_le;
          y_le       :out mct_y_le;
          flag_op    :out mct_flag_op
         );
  end component;

  component done_rom
    port (addr       :in u_unsigned (10 downto 0);
          done       :out mct_done
         );
  end component;

  component addr_op_rom
    port (addr       :in u_unsigned (10 downto 0);
          addr_op    :out mct_addr_op
         );
  end component;

  component din_le_rom
    port (addr       :in u_unsigned (10 downto 0);
          din_le     :out mct_din_le
         );
  end component;

  component rd_en_rom
    port (addr       :in u_unsigned (10 downto 0);
          rd_en      :out mct_rd_en
         );
  end component;

  component dout_op_rom
    port (addr       :in u_unsigned (10 downto 0);
          dout_op    :out mct_dout_op
         );
  end component;

  component dint1_op_rom
    port (addr       :in u_unsigned (10 downto 0);
          dint1_op   :out mct_dint1_op
         );
  end component;

  component dint2_op_rom
    port (addr       :in u_unsigned (10 downto 0);
          dint2_op   :out mct_dint2_op
         );
  end component;

  component dint3_op_rom
    port (addr       :in u_unsigned (10 downto 0);
          dint3_op   :out mct_dint3_op
         );
  end component;

  component pc_op_rom
    port (addr       :in u_unsigned (10 downto 0);
          pc_op      :out mct_pc_op
         );
  end component;

  component sp_op_rom
    port (addr       :in u_unsigned (10 downto 0);
          sp_op      :out mct_sp_op
         );
  end component;

  component alu1_rom
    port (addr       :in u_unsigned (10 downto 0);
          alu1       :out mct_alu1
         );
  end component;

  component alu2_rom
    port (addr       :in u_unsigned (10 downto 0);
          alu2       :out mct_alu2
         );
  end component;

  component alu_op_rom
    port (addr       :in u_unsigned (10 downto 0);
          alu_op     :out mct_alu_op
         );
  end component;

  component a_le_rom
    port (addr       :in u_unsigned (10 downto 0);
          a_le       :out mct_a_le
         );
  end component;

  component x_le_rom
    port (addr       :in u_unsigned (10 downto 0);
          x_le       :out mct_x_le
         );
  end component;

  component y_le_rom
    port (addr       :in u_unsigned (10 downto 0);
          y_le       :out mct_y_le
         );
  end component;

  component flag_op_rom
    port (addr       :in u_unsigned (10 downto 0);
          flag_op    :out mct_flag_op
         );
  end component;

end microcode;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity done_rom is
    port (addr       :in u_unsigned (10 downto 0);
          done       :out mct_done
         );
end done_rom;

architecture done_rom_arch of done_rom is
begin
  process (addr)
  begin
    case addr is
        when "01101101011" =>    done <= mc_done;
        when "01111101011" =>    done <= mc_done;
        when "01111001011" =>    done <= mc_done;
        when "01101001001" =>    done <= mc_done;
        when "01100001101" =>    done <= mc_done;
        when "01110001101" =>    done <= mc_done;
        when "01100101010" =>    done <= mc_done;
        when "01110101010" =>    done <= mc_done;
        when "00101101011" =>    done <= mc_done;
        when "00111101011" =>    done <= mc_done;
        when "00111001011" =>    done <= mc_done;
        when "00101001001" =>    done <= mc_done;
        when "00100001101" =>    done <= mc_done;
        when "00110001101" =>    done <= mc_done;
        when "00100101010" =>    done <= mc_done;
        when "00110101010" =>    done <= mc_done;
        when "00001110110" =>    done <= mc_done;
        when "00011110110" =>    done <= mc_done;
        when "00001010000" =>    done <= mc_done;
        when "00000110101" =>    done <= mc_done;
        when "00010110101" =>    done <= mc_done;
        when "10010000010" =>    done <= mc_done;
        when "10110000010" =>    done <= mc_done;
        when "11110000010" =>    done <= mc_done;
        when "00101100011" =>    done <= mc_done;
        when "00100100010" =>    done <= mc_done;
        when "00110000010" =>    done <= mc_done;
        when "11010000010" =>    done <= mc_done;
        when "00010000010" =>    done <= mc_done;
        when "00000000101" =>    done <= mc_done;
        when "01010000010" =>    done <= mc_done;
        when "01110000010" =>    done <= mc_done;
        when "00011000000" =>    done <= mc_done;
        when "11011000000" =>    done <= mc_done;
        when "01011000000" =>    done <= mc_done;
        when "10111000000" =>    done <= mc_done;
        when "11001101011" =>    done <= mc_done;
        when "11011101011" =>    done <= mc_done;
        when "11011001011" =>    done <= mc_done;
        when "11001001001" =>    done <= mc_done;
        when "11000001101" =>    done <= mc_done;
        when "11010001101" =>    done <= mc_done;
        when "11000101010" =>    done <= mc_done;
        when "11010101010" =>    done <= mc_done;
        when "11101100011" =>    done <= mc_done;
        when "11100000001" =>    done <= mc_done;
        when "11100100010" =>    done <= mc_done;
        when "11001100011" =>    done <= mc_done;
        when "11000000001" =>    done <= mc_done;
        when "11000100010" =>    done <= mc_done;
        when "11001110110" =>    done <= mc_done;
        when "11011110110" =>    done <= mc_done;
        when "11000110101" =>    done <= mc_done;
        when "11010110101" =>    done <= mc_done;
        when "11001010000" =>    done <= mc_done;
        when "10001000000" =>    done <= mc_done;
        when "01001101011" =>    done <= mc_done;
        when "01011101011" =>    done <= mc_done;
        when "01011001011" =>    done <= mc_done;
        when "01001001001" =>    done <= mc_done;
        when "01000001101" =>    done <= mc_done;
        when "01010001101" =>    done <= mc_done;
        when "01000101010" =>    done <= mc_done;
        when "01010101010" =>    done <= mc_done;
        when "11101110110" =>    done <= mc_done;
        when "11111110110" =>    done <= mc_done;
        when "11100110101" =>    done <= mc_done;
        when "11110110101" =>    done <= mc_done;
        when "11101000000" =>    done <= mc_done;
        when "11001000000" =>    done <= mc_done;
        when "01001100011" =>    done <= mc_done;
        when "01101100110" =>    done <= mc_done;
        when "00100000011" =>    done <= mc_done;
        when "10101101011" =>    done <= mc_done;
        when "10111101011" =>    done <= mc_done;
        when "10111001011" =>    done <= mc_done;
        when "10101001001" =>    done <= mc_done;
        when "10100101010" =>    done <= mc_done;
        when "10100001101" =>    done <= mc_done;
        when "10110001101" =>    done <= mc_done;
        when "10110101010" =>    done <= mc_done;
        when "10101110011" =>    done <= mc_done;
        when "10111110011" =>    done <= mc_done;
        when "10100010001" =>    done <= mc_done;
        when "10100110010" =>    done <= mc_done;
        when "10110110010" =>    done <= mc_done;
        when "10101100011" =>    done <= mc_done;
        when "10111100011" =>    done <= mc_done;
        when "10100000001" =>    done <= mc_done;
        when "10100100010" =>    done <= mc_done;
        when "10110100010" =>    done <= mc_done;
        when "01001110110" =>    done <= mc_done;
        when "01011110110" =>    done <= mc_done;
        when "01001010000" =>    done <= mc_done;
        when "01000110101" =>    done <= mc_done;
        when "01010110101" =>    done <= mc_done;
        when "11101010000" =>    done <= mc_done;
        when "00001101011" =>    done <= mc_done;
        when "00011101011" =>    done <= mc_done;
        when "00011001011" =>    done <= mc_done;
        when "00001001001" =>    done <= mc_done;
        when "00000001101" =>    done <= mc_done;
        when "00010001101" =>    done <= mc_done;
        when "00000101010" =>    done <= mc_done;
        when "00010101010" =>    done <= mc_done;
        when "01001000001" =>    done <= mc_done;
        when "00001000001" =>    done <= mc_done;
        when "01101000010" =>    done <= mc_done;
        when "00101000010" =>    done <= mc_done;
        when "00000011100" =>    done <= mc_done;
        when "00101110110" =>    done <= mc_done;
        when "00111110110" =>    done <= mc_done;
        when "00101010000" =>    done <= mc_done;
        when "00100110101" =>    done <= mc_done;
        when "00110110101" =>    done <= mc_done;
        when "01101110110" =>    done <= mc_done;
        when "01111110110" =>    done <= mc_done;
        when "01101010000" =>    done <= mc_done;
        when "01100110101" =>    done <= mc_done;
        when "01110110101" =>    done <= mc_done;
        when "01000000110" =>    done <= mc_done;
        when "01100000110" =>    done <= mc_done;
        when "11101101011" =>    done <= mc_done;
        when "11111101011" =>    done <= mc_done;
        when "11111001011" =>    done <= mc_done;
        when "11101001001" =>    done <= mc_done;
        when "11100001101" =>    done <= mc_done;
        when "11110001101" =>    done <= mc_done;
        when "11100101010" =>    done <= mc_done;
        when "11110101010" =>    done <= mc_done;
        when "00111000000" =>    done <= mc_done;
        when "11111000000" =>    done <= mc_done;
        when "01111000000" =>    done <= mc_done;
        when "10001101011" =>    done <= mc_done;
        when "10011001011" =>    done <= mc_done;
        when "10000001101" =>    done <= mc_done;
        when "10010001101" =>    done <= mc_done;
        when "10000101010" =>    done <= mc_done;
        when "10010101010" =>    done <= mc_done;
        when "10011101011" =>    done <= mc_done;
        when "01000011110" =>    done <= mc_done;
        when "00110011110" =>    done <= mc_done;
        when "10001110011" =>    done <= mc_done;
        when "10000110010" =>    done <= mc_done;
        when "10010110010" =>    done <= mc_done;
        when "10001100011" =>    done <= mc_done;
        when "10000100010" =>    done <= mc_done;
        when "10010100010" =>    done <= mc_done;
        when "10101010000" =>    done <= mc_done;
        when "10101000000" =>    done <= mc_done;
        when "10111010000" =>    done <= mc_done;
        when "10001010000" =>    done <= mc_done;
        when "10011010000" =>    done <= mc_done;
        when "10011000000" =>    done <= mc_done;
        when others =>    done <= mc_nop;
    end case;
  end process;
end done_rom_arch;


----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity addr_op_rom is
    port (addr       :in u_unsigned (10 downto 0);
          addr_op    :out mct_addr_op
         );
end addr_op_rom;

architecture addr_op_rom_arch of addr_op_rom is
begin
  process (addr)
  begin
    case addr is
        when "01101101000" =>    addr_op <= mc_pc_p;
        when "01101101010" =>    addr_op <= mc_split;
        when "01111101000" =>    addr_op <= mc_pc_p;
        when "01111101010" =>    addr_op <= mc_split_x;
        when "01111001000" =>    addr_op <= mc_pc_p;
        when "01111001010" =>    addr_op <= mc_split_y;
        when "01100001001" =>    addr_op <= mc_din_zx;
        when "01100001010" =>    addr_op <= mc_din_zxp;
        when "01100001100" =>    addr_op <= mc_split;
        when "01110001001" =>    addr_op <= mc_din_z;
        when "01110001010" =>    addr_op <= mc_din_zp;
        when "01110001100" =>    addr_op <= mc_split_y;
        when "01100101001" =>    addr_op <= mc_din_z;
        when "01110101001" =>    addr_op <= mc_din_zx;
        when "00101101000" =>    addr_op <= mc_pc_p;
        when "00101101010" =>    addr_op <= mc_split;
        when "00111101000" =>    addr_op <= mc_pc_p;
        when "00111101010" =>    addr_op <= mc_split_x;
        when "00111001000" =>    addr_op <= mc_pc_p;
        when "00111001010" =>    addr_op <= mc_split_y;
        when "00100001001" =>    addr_op <= mc_din_zx;
        when "00100001010" =>    addr_op <= mc_din_zxp;
        when "00100001100" =>    addr_op <= mc_split;
        when "00110001001" =>    addr_op <= mc_din_z;
        when "00110001010" =>    addr_op <= mc_din_zp;
        when "00110001100" =>    addr_op <= mc_split_y;
        when "00100101001" =>    addr_op <= mc_din_z;
        when "00110101001" =>    addr_op <= mc_din_zx;
        when "00001110000" =>    addr_op <= mc_pc_p;
        when "00001110010" =>    addr_op <= mc_split;
        when "00001110101" =>    addr_op <= mc_dint16;
        when "00011110000" =>    addr_op <= mc_pc_p;
        when "00011110010" =>    addr_op <= mc_split_x;
        when "00011110101" =>    addr_op <= mc_dint16_x;
        when "00000110001" =>    addr_op <= mc_din_z;
        when "00000110100" =>    addr_op <= mc_dint1_z;
        when "00010110001" =>    addr_op <= mc_din_zx;
        when "00010110100" =>    addr_op <= mc_dint1_zx;
        when "00101100000" =>    addr_op <= mc_pc_p;
        when "00101100010" =>    addr_op <= mc_split;
        when "00100100001" =>    addr_op <= mc_din_z;
        when "00000000000" =>    addr_op <= mc_v_irq1;
        when "00000000001" =>    addr_op <= mc_v_irq2;
        when "00000000010" =>    addr_op <= mc_sp;
        when "00000000011" =>    addr_op <= mc_sp;
        when "00000000100" =>    addr_op <= mc_sp;
        when "11001101000" =>    addr_op <= mc_pc_p;
        when "11001101010" =>    addr_op <= mc_split;
        when "11011101000" =>    addr_op <= mc_pc_p;
        when "11011101010" =>    addr_op <= mc_split_x;
        when "11011001000" =>    addr_op <= mc_pc_p;
        when "11011001010" =>    addr_op <= mc_split_y;
        when "11000001001" =>    addr_op <= mc_din_zx;
        when "11000001010" =>    addr_op <= mc_din_zxp;
        when "11000001100" =>    addr_op <= mc_split;
        when "11010001001" =>    addr_op <= mc_din_z;
        when "11010001010" =>    addr_op <= mc_din_zp;
        when "11010001100" =>    addr_op <= mc_split_y;
        when "11000101001" =>    addr_op <= mc_din_z;
        when "11010101001" =>    addr_op <= mc_din_zx;
        when "11101100000" =>    addr_op <= mc_pc_p;
        when "11101100010" =>    addr_op <= mc_split;
        when "11100100001" =>    addr_op <= mc_din_z;
        when "11001100000" =>    addr_op <= mc_pc_p;
        when "11001100010" =>    addr_op <= mc_split;
        when "11000100001" =>    addr_op <= mc_din_z;
        when "11001110000" =>    addr_op <= mc_pc_p;
        when "11001110010" =>    addr_op <= mc_split;
        when "11001110101" =>    addr_op <= mc_dint16;
        when "11011110000" =>    addr_op <= mc_pc_p;
        when "11011110010" =>    addr_op <= mc_split_x;
        when "11011110101" =>    addr_op <= mc_dint16_x;
        when "11000110001" =>    addr_op <= mc_din_z;
        when "11000110100" =>    addr_op <= mc_dint1_z;
        when "11010110001" =>    addr_op <= mc_din_zx;
        when "11010110100" =>    addr_op <= mc_dint1_zx;
        when "01001101000" =>    addr_op <= mc_pc_p;
        when "01001101010" =>    addr_op <= mc_split;
        when "01011101000" =>    addr_op <= mc_pc_p;
        when "01011101010" =>    addr_op <= mc_split_x;
        when "01011001000" =>    addr_op <= mc_pc_p;
        when "01011001010" =>    addr_op <= mc_split_y;
        when "01000001001" =>    addr_op <= mc_din_zx;
        when "01000001010" =>    addr_op <= mc_din_zxp;
        when "01000001100" =>    addr_op <= mc_split;
        when "01010001001" =>    addr_op <= mc_din_z;
        when "01010001010" =>    addr_op <= mc_din_zp;
        when "01010001100" =>    addr_op <= mc_split_y;
        when "01000101001" =>    addr_op <= mc_din_z;
        when "01010101001" =>    addr_op <= mc_din_zx;
        when "11101110000" =>    addr_op <= mc_pc_p;
        when "11101110010" =>    addr_op <= mc_split;
        when "11101110101" =>    addr_op <= mc_dint16;
        when "11111110000" =>    addr_op <= mc_pc_p;
        when "11111110010" =>    addr_op <= mc_split_x;
        when "11111110101" =>    addr_op <= mc_dint16_x;
        when "11100110001" =>    addr_op <= mc_din_z;
        when "11100110100" =>    addr_op <= mc_dint1_z;
        when "11110110001" =>    addr_op <= mc_din_zx;
        when "11110110100" =>    addr_op <= mc_dint1_zx;
        when "01001100000" =>    addr_op <= mc_pc_p;
        when "01101100000" =>    addr_op <= mc_pc_p;
        when "01101100010" =>    addr_op <= mc_split;
        when "01101100011" =>    addr_op <= mc_split_p;
        when "00100000000" =>    addr_op <= mc_pc_p;
        when "00100000001" =>    addr_op <= mc_sp;
        when "00100000010" =>    addr_op <= mc_sp;
        when "10101101000" =>    addr_op <= mc_pc_p;
        when "10101101010" =>    addr_op <= mc_split;
        when "10111101000" =>    addr_op <= mc_pc_p;
        when "10111101010" =>    addr_op <= mc_split_x;
        when "10111001000" =>    addr_op <= mc_pc_p;
        when "10111001010" =>    addr_op <= mc_split_y;
        when "10100101001" =>    addr_op <= mc_din_z;
        when "10100001001" =>    addr_op <= mc_din_zx;
        when "10100001010" =>    addr_op <= mc_din_zxp;
        when "10100001100" =>    addr_op <= mc_split;
        when "10110001001" =>    addr_op <= mc_din_z;
        when "10110001010" =>    addr_op <= mc_din_zp;
        when "10110001100" =>    addr_op <= mc_split_y;
        when "10110101001" =>    addr_op <= mc_din_zx;
        when "10101110000" =>    addr_op <= mc_pc_p;
        when "10101110010" =>    addr_op <= mc_split;
        when "10111110000" =>    addr_op <= mc_pc_p;
        when "10111110010" =>    addr_op <= mc_split_y;
        when "10100110001" =>    addr_op <= mc_din_z;
        when "10110110001" =>    addr_op <= mc_din_zy;
        when "10101100000" =>    addr_op <= mc_pc_p;
        when "10101100010" =>    addr_op <= mc_split;
        when "10111100000" =>    addr_op <= mc_pc_p;
        when "10111100010" =>    addr_op <= mc_split_x;
        when "10100100001" =>    addr_op <= mc_din_z;
        when "10110100001" =>    addr_op <= mc_din_zx;
        when "01001110000" =>    addr_op <= mc_pc_p;
        when "01001110010" =>    addr_op <= mc_split;
        when "01001110101" =>    addr_op <= mc_dint16;
        when "01011110000" =>    addr_op <= mc_pc_p;
        when "01011110010" =>    addr_op <= mc_split_x;
        when "01011110101" =>    addr_op <= mc_dint16_x;
        when "01000110001" =>    addr_op <= mc_din_z;
        when "01000110100" =>    addr_op <= mc_dint1_z;
        when "01010110001" =>    addr_op <= mc_din_zx;
        when "01010110100" =>    addr_op <= mc_dint1_zx;
        when "00001101000" =>    addr_op <= mc_pc_p;
        when "00001101010" =>    addr_op <= mc_split;
        when "00011101000" =>    addr_op <= mc_pc_p;
        when "00011101010" =>    addr_op <= mc_split_x;
        when "00011001000" =>    addr_op <= mc_pc_p;
        when "00011001010" =>    addr_op <= mc_split_y;
        when "00000001001" =>    addr_op <= mc_din_zx;
        when "00000001010" =>    addr_op <= mc_din_zxp;
        when "00000001100" =>    addr_op <= mc_split;
        when "00010001001" =>    addr_op <= mc_din_z;
        when "00010001010" =>    addr_op <= mc_din_zp;
        when "00010001100" =>    addr_op <= mc_split_y;
        when "00000101001" =>    addr_op <= mc_din_z;
        when "00010101001" =>    addr_op <= mc_din_zx;
        when "01001000000" =>    addr_op <= mc_sp;
        when "00001000000" =>    addr_op <= mc_sp;
        when "01101000001" =>    addr_op <= mc_sp;
        when "00101000001" =>    addr_op <= mc_sp;
        when "00000011000" =>    addr_op <= mc_v_reset1;
        when "00000011001" =>    addr_op <= mc_v_reset2;
        when "00101110000" =>    addr_op <= mc_pc_p;
        when "00101110010" =>    addr_op <= mc_split;
        when "00101110101" =>    addr_op <= mc_dint16;
        when "00111110000" =>    addr_op <= mc_pc_p;
        when "00111110010" =>    addr_op <= mc_split_x;
        when "00111110101" =>    addr_op <= mc_dint16_x;
        when "00100110001" =>    addr_op <= mc_din_z;
        when "00100110100" =>    addr_op <= mc_dint1_z;
        when "00110110001" =>    addr_op <= mc_din_zx;
        when "00110110100" =>    addr_op <= mc_dint1_zx;
        when "01101110000" =>    addr_op <= mc_pc_p;
        when "01101110010" =>    addr_op <= mc_split;
        when "01101110101" =>    addr_op <= mc_dint16;
        when "01111110000" =>    addr_op <= mc_pc_p;
        when "01111110010" =>    addr_op <= mc_split_x;
        when "01111110101" =>    addr_op <= mc_dint16_x;
        when "01100110001" =>    addr_op <= mc_din_z;
        when "01100110100" =>    addr_op <= mc_dint1_z;
        when "01110110001" =>    addr_op <= mc_din_zx;
        when "01110110100" =>    addr_op <= mc_dint1_zx;
        when "01000000001" =>    addr_op <= mc_sp;
        when "01000000010" =>    addr_op <= mc_sp;
        when "01000000011" =>    addr_op <= mc_sp;
        when "01100000001" =>    addr_op <= mc_sp;
        when "01100000010" =>    addr_op <= mc_sp;
        when "11101101000" =>    addr_op <= mc_pc_p;
        when "11101101010" =>    addr_op <= mc_split;
        when "11111101000" =>    addr_op <= mc_pc_p;
        when "11111101010" =>    addr_op <= mc_split_x;
        when "11111001000" =>    addr_op <= mc_pc_p;
        when "11111001010" =>    addr_op <= mc_split_y;
        when "11100001001" =>    addr_op <= mc_din_zx;
        when "11100001010" =>    addr_op <= mc_din_zxp;
        when "11100001100" =>    addr_op <= mc_split;
        when "11110001001" =>    addr_op <= mc_din_z;
        when "11110001010" =>    addr_op <= mc_din_zp;
        when "11110001100" =>    addr_op <= mc_split_y;
        when "11100101001" =>    addr_op <= mc_din_z;
        when "11110101001" =>    addr_op <= mc_din_zx;
        when "10001101000" =>    addr_op <= mc_pc_p;
        when "10001101010" =>    addr_op <= mc_split;
        when "10011001000" =>    addr_op <= mc_pc_p;
        when "10011001010" =>    addr_op <= mc_split_y;
        when "10000001001" =>    addr_op <= mc_din_zx;
        when "10000001010" =>    addr_op <= mc_din_zxp;
        when "10000001100" =>    addr_op <= mc_split;
        when "10010001001" =>    addr_op <= mc_din_z;
        when "10010001010" =>    addr_op <= mc_din_zp;
        when "10010001100" =>    addr_op <= mc_split_y;
        when "10000101001" =>    addr_op <= mc_din_z;
        when "10010101001" =>    addr_op <= mc_din_zx;
        when "10011101000" =>    addr_op <= mc_pc_p;
        when "10011101010" =>    addr_op <= mc_split_x;
        when "01000011000" =>    addr_op <= mc_v_irq1;
        when "01000011001" =>    addr_op <= mc_v_irq2;
        when "01000011010" =>    addr_op <= mc_sp;
        when "01000011011" =>    addr_op <= mc_sp;
        when "01000011100" =>    addr_op <= mc_sp;
        when "00110011000" =>    addr_op <= mc_v_nmi1;
        when "00110011001" =>    addr_op <= mc_v_nmi2;
        when "00110011010" =>    addr_op <= mc_sp;
        when "00110011011" =>    addr_op <= mc_sp;
        when "00110011100" =>    addr_op <= mc_sp;
        when "10001110000" =>    addr_op <= mc_pc_p;
        when "10001110010" =>    addr_op <= mc_split;
        when "10000110001" =>    addr_op <= mc_din_z;
        when "10010110001" =>    addr_op <= mc_din_zy;
        when "10001100000" =>    addr_op <= mc_pc_p;
        when "10001100010" =>    addr_op <= mc_split;
        when "10000100001" =>    addr_op <= mc_din_z;
        when "10010100001" =>    addr_op <= mc_din_zx;
        when others =>    addr_op <= mc_nop;
    end case;
  end process;
end addr_op_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity din_le_rom is
    port (addr       :in u_unsigned (10 downto 0);
          din_le     :out mct_din_le
         );
end din_le_rom;

architecture din_le_rom_arch of din_le_rom is
begin
  process (addr)
  begin
    case addr is
        when "01101101000" =>    din_le <= mc_en;
        when "01101101001" =>    din_le <= mc_en;
        when "01101101011" =>    din_le <= mc_en;
        when "01111101000" =>    din_le <= mc_en;
        when "01111101001" =>    din_le <= mc_en;
        when "01111101011" =>    din_le <= mc_en;
        when "01111001000" =>    din_le <= mc_en;
        when "01111001001" =>    din_le <= mc_en;
        when "01111001011" =>    din_le <= mc_en;
        when "01101001000" =>    din_le <= mc_en;
        when "01100001000" =>    din_le <= mc_en;
        when "01100001010" =>    din_le <= mc_en;
        when "01100001011" =>    din_le <= mc_en;
        when "01100001101" =>    din_le <= mc_en;
        when "01110001000" =>    din_le <= mc_en;
        when "01110001010" =>    din_le <= mc_en;
        when "01110001011" =>    din_le <= mc_en;
        when "01110001101" =>    din_le <= mc_en;
        when "01100101000" =>    din_le <= mc_en;
        when "01100101010" =>    din_le <= mc_en;
        when "01110101000" =>    din_le <= mc_en;
        when "01110101010" =>    din_le <= mc_en;
        when "00101101000" =>    din_le <= mc_en;
        when "00101101001" =>    din_le <= mc_en;
        when "00101101011" =>    din_le <= mc_en;
        when "00111101000" =>    din_le <= mc_en;
        when "00111101001" =>    din_le <= mc_en;
        when "00111101011" =>    din_le <= mc_en;
        when "00111001000" =>    din_le <= mc_en;
        when "00111001001" =>    din_le <= mc_en;
        when "00111001011" =>    din_le <= mc_en;
        when "00101001000" =>    din_le <= mc_en;
        when "00100001000" =>    din_le <= mc_en;
        when "00100001010" =>    din_le <= mc_en;
        when "00100001011" =>    din_le <= mc_en;
        when "00100001101" =>    din_le <= mc_en;
        when "00110001000" =>    din_le <= mc_en;
        when "00110001010" =>    din_le <= mc_en;
        when "00110001011" =>    din_le <= mc_en;
        when "00110001101" =>    din_le <= mc_en;
        when "00100101000" =>    din_le <= mc_en;
        when "00100101010" =>    din_le <= mc_en;
        when "00110101000" =>    din_le <= mc_en;
        when "00110101010" =>    din_le <= mc_en;
        when "00001110000" =>    din_le <= mc_en;
        when "00001110001" =>    din_le <= mc_en;
        when "00001110011" =>    din_le <= mc_en;
        when "00011110000" =>    din_le <= mc_en;
        when "00011110001" =>    din_le <= mc_en;
        when "00011110011" =>    din_le <= mc_en;
        when "00000110000" =>    din_le <= mc_en;
        when "00000110010" =>    din_le <= mc_en;
        when "00010110000" =>    din_le <= mc_en;
        when "00010110010" =>    din_le <= mc_en;
        when "10010000000" =>    din_le <= mc_en;
        when "10110000000" =>    din_le <= mc_en;
        when "11110000000" =>    din_le <= mc_en;
        when "00101100000" =>    din_le <= mc_en;
        when "00101100001" =>    din_le <= mc_en;
        when "00101100011" =>    din_le <= mc_en;
        when "00100100000" =>    din_le <= mc_en;
        when "00100100010" =>    din_le <= mc_en;
        when "00110000000" =>    din_le <= mc_en;
        when "11010000000" =>    din_le <= mc_en;
        when "00010000000" =>    din_le <= mc_en;
        when "00000000001" =>    din_le <= mc_en;
        when "00000000010" =>    din_le <= mc_en;
        when "01010000000" =>    din_le <= mc_en;
        when "01110000000" =>    din_le <= mc_en;
        when "11001101000" =>    din_le <= mc_en;
        when "11001101001" =>    din_le <= mc_en;
        when "11001101011" =>    din_le <= mc_en;
        when "11011101000" =>    din_le <= mc_en;
        when "11011101001" =>    din_le <= mc_en;
        when "11011101011" =>    din_le <= mc_en;
        when "11011001000" =>    din_le <= mc_en;
        when "11011001001" =>    din_le <= mc_en;
        when "11011001011" =>    din_le <= mc_en;
        when "11001001000" =>    din_le <= mc_en;
        when "11000001000" =>    din_le <= mc_en;
        when "11000001010" =>    din_le <= mc_en;
        when "11000001011" =>    din_le <= mc_en;
        when "11000001101" =>    din_le <= mc_en;
        when "11010001000" =>    din_le <= mc_en;
        when "11010001010" =>    din_le <= mc_en;
        when "11010001011" =>    din_le <= mc_en;
        when "11010001101" =>    din_le <= mc_en;
        when "11000101000" =>    din_le <= mc_en;
        when "11000101010" =>    din_le <= mc_en;
        when "11010101000" =>    din_le <= mc_en;
        when "11010101010" =>    din_le <= mc_en;
        when "11101100000" =>    din_le <= mc_en;
        when "11101100001" =>    din_le <= mc_en;
        when "11101100011" =>    din_le <= mc_en;
        when "11100000000" =>    din_le <= mc_en;
        when "11100100000" =>    din_le <= mc_en;
        when "11100100010" =>    din_le <= mc_en;
        when "11001100000" =>    din_le <= mc_en;
        when "11001100001" =>    din_le <= mc_en;
        when "11001100011" =>    din_le <= mc_en;
        when "11000000000" =>    din_le <= mc_en;
        when "11000100000" =>    din_le <= mc_en;
        when "11000100010" =>    din_le <= mc_en;
        when "11001110000" =>    din_le <= mc_en;
        when "11001110001" =>    din_le <= mc_en;
        when "11001110011" =>    din_le <= mc_en;
        when "11011110000" =>    din_le <= mc_en;
        when "11011110001" =>    din_le <= mc_en;
        when "11011110011" =>    din_le <= mc_en;
        when "11000110000" =>    din_le <= mc_en;
        when "11000110010" =>    din_le <= mc_en;
        when "11010110000" =>    din_le <= mc_en;
        when "11010110010" =>    din_le <= mc_en;
        when "01001101000" =>    din_le <= mc_en;
        when "01001101001" =>    din_le <= mc_en;
        when "01001101011" =>    din_le <= mc_en;
        when "01011101000" =>    din_le <= mc_en;
        when "01011101001" =>    din_le <= mc_en;
        when "01011101011" =>    din_le <= mc_en;
        when "01011001000" =>    din_le <= mc_en;
        when "01011001001" =>    din_le <= mc_en;
        when "01011001011" =>    din_le <= mc_en;
        when "01001001000" =>    din_le <= mc_en;
        when "01000001000" =>    din_le <= mc_en;
        when "01000001010" =>    din_le <= mc_en;
        when "01000001011" =>    din_le <= mc_en;
        when "01000001101" =>    din_le <= mc_en;
        when "01010001000" =>    din_le <= mc_en;
        when "01010001010" =>    din_le <= mc_en;
        when "01010001011" =>    din_le <= mc_en;
        when "01010001101" =>    din_le <= mc_en;
        when "01000101000" =>    din_le <= mc_en;
        when "01000101010" =>    din_le <= mc_en;
        when "01010101000" =>    din_le <= mc_en;
        when "01010101010" =>    din_le <= mc_en;
        when "11101110000" =>    din_le <= mc_en;
        when "11101110001" =>    din_le <= mc_en;
        when "11101110011" =>    din_le <= mc_en;
        when "11111110000" =>    din_le <= mc_en;
        when "11111110001" =>    din_le <= mc_en;
        when "11111110011" =>    din_le <= mc_en;
        when "11100110000" =>    din_le <= mc_en;
        when "11100110010" =>    din_le <= mc_en;
        when "11110110000" =>    din_le <= mc_en;
        when "11110110010" =>    din_le <= mc_en;
        when "01001100000" =>    din_le <= mc_en;
        when "01001100001" =>    din_le <= mc_en;
        when "01101100000" =>    din_le <= mc_en;
        when "01101100001" =>    din_le <= mc_en;
        when "01101100011" =>    din_le <= mc_en;
        when "01101100100" =>    din_le <= mc_en;
        when "00100000000" =>    din_le <= mc_en;
        when "00100000001" =>    din_le <= mc_en;
        when "10101101000" =>    din_le <= mc_en;
        when "10101101001" =>    din_le <= mc_en;
        when "10101101011" =>    din_le <= mc_en;
        when "10111101000" =>    din_le <= mc_en;
        when "10111101001" =>    din_le <= mc_en;
        when "10111101011" =>    din_le <= mc_en;
        when "10111001000" =>    din_le <= mc_en;
        when "10111001001" =>    din_le <= mc_en;
        when "10111001011" =>    din_le <= mc_en;
        when "10101001000" =>    din_le <= mc_en;
        when "10100101000" =>    din_le <= mc_en;
        when "10100101010" =>    din_le <= mc_en;
        when "10100001000" =>    din_le <= mc_en;
        when "10100001010" =>    din_le <= mc_en;
        when "10100001011" =>    din_le <= mc_en;
        when "10100001101" =>    din_le <= mc_en;
        when "10110001000" =>    din_le <= mc_en;
        when "10110001010" =>    din_le <= mc_en;
        when "10110001011" =>    din_le <= mc_en;
        when "10110001101" =>    din_le <= mc_en;
        when "10110101000" =>    din_le <= mc_en;
        when "10110101010" =>    din_le <= mc_en;
        when "10101110000" =>    din_le <= mc_en;
        when "10101110001" =>    din_le <= mc_en;
        when "10101110011" =>    din_le <= mc_en;
        when "10111110000" =>    din_le <= mc_en;
        when "10111110001" =>    din_le <= mc_en;
        when "10111110011" =>    din_le <= mc_en;
        when "10100010000" =>    din_le <= mc_en;
        when "10100110000" =>    din_le <= mc_en;
        when "10100110010" =>    din_le <= mc_en;
        when "10110110000" =>    din_le <= mc_en;
        when "10110110010" =>    din_le <= mc_en;
        when "10101100000" =>    din_le <= mc_en;
        when "10101100001" =>    din_le <= mc_en;
        when "10101100011" =>    din_le <= mc_en;
        when "10111100000" =>    din_le <= mc_en;
        when "10111100001" =>    din_le <= mc_en;
        when "10111100011" =>    din_le <= mc_en;
        when "10100000000" =>    din_le <= mc_en;
        when "10100100000" =>    din_le <= mc_en;
        when "10100100010" =>    din_le <= mc_en;
        when "10110100000" =>    din_le <= mc_en;
        when "10110100010" =>    din_le <= mc_en;
        when "01001110000" =>    din_le <= mc_en;
        when "01001110001" =>    din_le <= mc_en;
        when "01001110011" =>    din_le <= mc_en;
        when "01011110000" =>    din_le <= mc_en;
        when "01011110001" =>    din_le <= mc_en;
        when "01011110011" =>    din_le <= mc_en;
        when "01000110000" =>    din_le <= mc_en;
        when "01000110010" =>    din_le <= mc_en;
        when "01010110000" =>    din_le <= mc_en;
        when "01010110010" =>    din_le <= mc_en;
        when "00001101000" =>    din_le <= mc_en;
        when "00001101001" =>    din_le <= mc_en;
        when "00001101011" =>    din_le <= mc_en;
        when "00011101000" =>    din_le <= mc_en;
        when "00011101001" =>    din_le <= mc_en;
        when "00011101011" =>    din_le <= mc_en;
        when "00011001000" =>    din_le <= mc_en;
        when "00011001001" =>    din_le <= mc_en;
        when "00011001011" =>    din_le <= mc_en;
        when "00001001000" =>    din_le <= mc_en;
        when "00000001000" =>    din_le <= mc_en;
        when "00000001010" =>    din_le <= mc_en;
        when "00000001011" =>    din_le <= mc_en;
        when "00000001101" =>    din_le <= mc_en;
        when "00010001000" =>    din_le <= mc_en;
        when "00010001010" =>    din_le <= mc_en;
        when "00010001011" =>    din_le <= mc_en;
        when "00010001101" =>    din_le <= mc_en;
        when "00000101000" =>    din_le <= mc_en;
        when "00000101010" =>    din_le <= mc_en;
        when "00010101000" =>    din_le <= mc_en;
        when "00010101010" =>    din_le <= mc_en;
        when "01101000010" =>    din_le <= mc_en;
        when "00101000010" =>    din_le <= mc_en;
        when "00000011001" =>    din_le <= mc_en;
        when "00000011010" =>    din_le <= mc_en;
        when "00101110000" =>    din_le <= mc_en;
        when "00101110001" =>    din_le <= mc_en;
        when "00101110011" =>    din_le <= mc_en;
        when "00111110000" =>    din_le <= mc_en;
        when "00111110001" =>    din_le <= mc_en;
        when "00111110011" =>    din_le <= mc_en;
        when "00100110000" =>    din_le <= mc_en;
        when "00100110010" =>    din_le <= mc_en;
        when "00110110000" =>    din_le <= mc_en;
        when "00110110010" =>    din_le <= mc_en;
        when "01101110000" =>    din_le <= mc_en;
        when "01101110001" =>    din_le <= mc_en;
        when "01101110011" =>    din_le <= mc_en;
        when "01111110000" =>    din_le <= mc_en;
        when "01111110001" =>    din_le <= mc_en;
        when "01111110011" =>    din_le <= mc_en;
        when "01100110000" =>    din_le <= mc_en;
        when "01100110010" =>    din_le <= mc_en;
        when "01110110000" =>    din_le <= mc_en;
        when "01110110010" =>    din_le <= mc_en;
        when "01000000010" =>    din_le <= mc_en;
        when "01000000011" =>    din_le <= mc_en;
        when "01000000100" =>    din_le <= mc_en;
        when "01100000010" =>    din_le <= mc_en;
        when "01100000011" =>    din_le <= mc_en;
        when "11101101000" =>    din_le <= mc_en;
        when "11101101001" =>    din_le <= mc_en;
        when "11101101011" =>    din_le <= mc_en;
        when "11111101000" =>    din_le <= mc_en;
        when "11111101001" =>    din_le <= mc_en;
        when "11111101011" =>    din_le <= mc_en;
        when "11111001000" =>    din_le <= mc_en;
        when "11111001001" =>    din_le <= mc_en;
        when "11111001011" =>    din_le <= mc_en;
        when "11101001000" =>    din_le <= mc_en;
        when "11100001000" =>    din_le <= mc_en;
        when "11100001010" =>    din_le <= mc_en;
        when "11100001011" =>    din_le <= mc_en;
        when "11100001101" =>    din_le <= mc_en;
        when "11110001000" =>    din_le <= mc_en;
        when "11110001010" =>    din_le <= mc_en;
        when "11110001011" =>    din_le <= mc_en;
        when "11110001101" =>    din_le <= mc_en;
        when "11100101000" =>    din_le <= mc_en;
        when "11100101010" =>    din_le <= mc_en;
        when "11110101000" =>    din_le <= mc_en;
        when "11110101010" =>    din_le <= mc_en;
        when "10001101000" =>    din_le <= mc_en;
        when "10001101001" =>    din_le <= mc_en;
        when "10011001000" =>    din_le <= mc_en;
        when "10011001001" =>    din_le <= mc_en;
        when "10000001000" =>    din_le <= mc_en;
        when "10000001010" =>    din_le <= mc_en;
        when "10000001011" =>    din_le <= mc_en;
        when "10010001000" =>    din_le <= mc_en;
        when "10010001010" =>    din_le <= mc_en;
        when "10010001011" =>    din_le <= mc_en;
        when "10000101000" =>    din_le <= mc_en;
        when "10010101000" =>    din_le <= mc_en;
        when "10011101000" =>    din_le <= mc_en;
        when "10011101001" =>    din_le <= mc_en;
        when "01000011001" =>    din_le <= mc_en;
        when "01000011010" =>    din_le <= mc_en;
        when "00110011001" =>    din_le <= mc_en;
        when "00110011010" =>    din_le <= mc_en;
        when "10001110000" =>    din_le <= mc_en;
        when "10001110001" =>    din_le <= mc_en;
        when "10000110000" =>    din_le <= mc_en;
        when "10010110000" =>    din_le <= mc_en;
        when "10001100000" =>    din_le <= mc_en;
        when "10001100001" =>    din_le <= mc_en;
        when "10000100000" =>    din_le <= mc_en;
        when "10010100000" =>    din_le <= mc_en;
        when others =>    din_le <= mc_nop;
    end case;
  end process;
end din_le_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity rd_en_rom is
    port (addr       :in u_unsigned (10 downto 0);
          rd_en      :out mct_rd_en
         );
end rd_en_rom;

architecture rd_en_rom_arch of rd_en_rom is
begin
  process (addr)
  begin
    case addr is
        when "01101101000" =>    rd_en <= mc_read;
        when "01101101010" =>    rd_en <= mc_read;
        when "01111101000" =>    rd_en <= mc_read;
        when "01111101010" =>    rd_en <= mc_read;
        when "01111001000" =>    rd_en <= mc_read;
        when "01111001010" =>    rd_en <= mc_read;
        when "01100001001" =>    rd_en <= mc_read;
        when "01100001010" =>    rd_en <= mc_read;
        when "01100001100" =>    rd_en <= mc_read;
        when "01110001001" =>    rd_en <= mc_read;
        when "01110001010" =>    rd_en <= mc_read;
        when "01110001100" =>    rd_en <= mc_read;
        when "01100101001" =>    rd_en <= mc_read;
        when "01110101001" =>    rd_en <= mc_read;
        when "00101101000" =>    rd_en <= mc_read;
        when "00101101010" =>    rd_en <= mc_read;
        when "00111101000" =>    rd_en <= mc_read;
        when "00111101010" =>    rd_en <= mc_read;
        when "00111001000" =>    rd_en <= mc_read;
        when "00111001010" =>    rd_en <= mc_read;
        when "00100001001" =>    rd_en <= mc_read;
        when "00100001010" =>    rd_en <= mc_read;
        when "00100001100" =>    rd_en <= mc_read;
        when "00110001001" =>    rd_en <= mc_read;
        when "00110001010" =>    rd_en <= mc_read;
        when "00110001100" =>    rd_en <= mc_read;
        when "00100101001" =>    rd_en <= mc_read;
        when "00110101001" =>    rd_en <= mc_read;
        when "00001110000" =>    rd_en <= mc_read;
        when "00001110010" =>    rd_en <= mc_read;
        when "00011110000" =>    rd_en <= mc_read;
        when "00011110010" =>    rd_en <= mc_read;
        when "00000110001" =>    rd_en <= mc_read;
        when "00010110001" =>    rd_en <= mc_read;
        when "00101100000" =>    rd_en <= mc_read;
        when "00101100010" =>    rd_en <= mc_read;
        when "00100100001" =>    rd_en <= mc_read;
        when "00000000000" =>    rd_en <= mc_read;
        when "00000000001" =>    rd_en <= mc_read;
        when "11001101000" =>    rd_en <= mc_read;
        when "11001101010" =>    rd_en <= mc_read;
        when "11011101000" =>    rd_en <= mc_read;
        when "11011101010" =>    rd_en <= mc_read;
        when "11011001000" =>    rd_en <= mc_read;
        when "11011001010" =>    rd_en <= mc_read;
        when "11000001001" =>    rd_en <= mc_read;
        when "11000001010" =>    rd_en <= mc_read;
        when "11000001100" =>    rd_en <= mc_read;
        when "11010001001" =>    rd_en <= mc_read;
        when "11010001010" =>    rd_en <= mc_read;
        when "11010001100" =>    rd_en <= mc_read;
        when "11000101001" =>    rd_en <= mc_read;
        when "11010101001" =>    rd_en <= mc_read;
        when "11101100000" =>    rd_en <= mc_read;
        when "11101100010" =>    rd_en <= mc_read;
        when "11100100001" =>    rd_en <= mc_read;
        when "11001100000" =>    rd_en <= mc_read;
        when "11001100010" =>    rd_en <= mc_read;
        when "11000100001" =>    rd_en <= mc_read;
        when "11001110000" =>    rd_en <= mc_read;
        when "11001110010" =>    rd_en <= mc_read;
        when "11011110000" =>    rd_en <= mc_read;
        when "11011110010" =>    rd_en <= mc_read;
        when "11000110001" =>    rd_en <= mc_read;
        when "11010110001" =>    rd_en <= mc_read;
        when "01001101000" =>    rd_en <= mc_read;
        when "01001101010" =>    rd_en <= mc_read;
        when "01011101000" =>    rd_en <= mc_read;
        when "01011101010" =>    rd_en <= mc_read;
        when "01011001000" =>    rd_en <= mc_read;
        when "01011001010" =>    rd_en <= mc_read;
        when "01000001001" =>    rd_en <= mc_read;
        when "01000001010" =>    rd_en <= mc_read;
        when "01000001100" =>    rd_en <= mc_read;
        when "01010001001" =>    rd_en <= mc_read;
        when "01010001010" =>    rd_en <= mc_read;
        when "01010001100" =>    rd_en <= mc_read;
        when "01000101001" =>    rd_en <= mc_read;
        when "01010101001" =>    rd_en <= mc_read;
        when "11101110000" =>    rd_en <= mc_read;
        when "11101110010" =>    rd_en <= mc_read;
        when "11111110000" =>    rd_en <= mc_read;
        when "11111110010" =>    rd_en <= mc_read;
        when "11100110001" =>    rd_en <= mc_read;
        when "11110110001" =>    rd_en <= mc_read;
        when "01001100000" =>    rd_en <= mc_read;
        when "01101100000" =>    rd_en <= mc_read;
        when "01101100010" =>    rd_en <= mc_read;
        when "01101100011" =>    rd_en <= mc_read;
        when "00100000000" =>    rd_en <= mc_read;
        when "10101101000" =>    rd_en <= mc_read;
        when "10101101010" =>    rd_en <= mc_read;
        when "10111101000" =>    rd_en <= mc_read;
        when "10111101010" =>    rd_en <= mc_read;
        when "10111001000" =>    rd_en <= mc_read;
        when "10111001010" =>    rd_en <= mc_read;
        when "10100101001" =>    rd_en <= mc_read;
        when "10100001001" =>    rd_en <= mc_read;
        when "10100001010" =>    rd_en <= mc_read;
        when "10100001100" =>    rd_en <= mc_read;
        when "10110001001" =>    rd_en <= mc_read;
        when "10110001010" =>    rd_en <= mc_read;
        when "10110001100" =>    rd_en <= mc_read;
        when "10110101001" =>    rd_en <= mc_read;
        when "10101110000" =>    rd_en <= mc_read;
        when "10101110010" =>    rd_en <= mc_read;
        when "10111110000" =>    rd_en <= mc_read;
        when "10111110010" =>    rd_en <= mc_read;
        when "10100110001" =>    rd_en <= mc_read;
        when "10110110001" =>    rd_en <= mc_read;
        when "10101100000" =>    rd_en <= mc_read;
        when "10101100010" =>    rd_en <= mc_read;
        when "10111100000" =>    rd_en <= mc_read;
        when "10111100010" =>    rd_en <= mc_read;
        when "10100100001" =>    rd_en <= mc_read;
        when "10110100001" =>    rd_en <= mc_read;
        when "01001110000" =>    rd_en <= mc_read;
        when "01001110010" =>    rd_en <= mc_read;
        when "01011110000" =>    rd_en <= mc_read;
        when "01011110010" =>    rd_en <= mc_read;
        when "01000110001" =>    rd_en <= mc_read;
        when "01010110001" =>    rd_en <= mc_read;
        when "00001101000" =>    rd_en <= mc_read;
        when "00001101010" =>    rd_en <= mc_read;
        when "00011101000" =>    rd_en <= mc_read;
        when "00011101010" =>    rd_en <= mc_read;
        when "00011001000" =>    rd_en <= mc_read;
        when "00011001010" =>    rd_en <= mc_read;
        when "00000001001" =>    rd_en <= mc_read;
        when "00000001010" =>    rd_en <= mc_read;
        when "00000001100" =>    rd_en <= mc_read;
        when "00010001001" =>    rd_en <= mc_read;
        when "00010001010" =>    rd_en <= mc_read;
        when "00010001100" =>    rd_en <= mc_read;
        when "00000101001" =>    rd_en <= mc_read;
        when "00010101001" =>    rd_en <= mc_read;
        when "01101000001" =>    rd_en <= mc_read;
        when "00101000001" =>    rd_en <= mc_read;
        when "00000011000" =>    rd_en <= mc_read;
        when "00000011001" =>    rd_en <= mc_read;
        when "00101110000" =>    rd_en <= mc_read;
        when "00101110010" =>    rd_en <= mc_read;
        when "00111110000" =>    rd_en <= mc_read;
        when "00111110010" =>    rd_en <= mc_read;
        when "00100110001" =>    rd_en <= mc_read;
        when "00110110001" =>    rd_en <= mc_read;
        when "01101110000" =>    rd_en <= mc_read;
        when "01101110010" =>    rd_en <= mc_read;
        when "01111110000" =>    rd_en <= mc_read;
        when "01111110010" =>    rd_en <= mc_read;
        when "01100110001" =>    rd_en <= mc_read;
        when "01110110001" =>    rd_en <= mc_read;
        when "01000000001" =>    rd_en <= mc_read;
        when "01000000010" =>    rd_en <= mc_read;
        when "01000000011" =>    rd_en <= mc_read;
        when "01100000001" =>    rd_en <= mc_read;
        when "01100000010" =>    rd_en <= mc_read;
        when "11101101000" =>    rd_en <= mc_read;
        when "11101101010" =>    rd_en <= mc_read;
        when "11111101000" =>    rd_en <= mc_read;
        when "11111101010" =>    rd_en <= mc_read;
        when "11111001000" =>    rd_en <= mc_read;
        when "11111001010" =>    rd_en <= mc_read;
        when "11100001001" =>    rd_en <= mc_read;
        when "11100001010" =>    rd_en <= mc_read;
        when "11100001100" =>    rd_en <= mc_read;
        when "11110001001" =>    rd_en <= mc_read;
        when "11110001010" =>    rd_en <= mc_read;
        when "11110001100" =>    rd_en <= mc_read;
        when "11100101001" =>    rd_en <= mc_read;
        when "11110101001" =>    rd_en <= mc_read;
        when "10001101000" =>    rd_en <= mc_read;
        when "10011001000" =>    rd_en <= mc_read;
        when "10000001001" =>    rd_en <= mc_read;
        when "10000001010" =>    rd_en <= mc_read;
        when "10010001001" =>    rd_en <= mc_read;
        when "10010001010" =>    rd_en <= mc_read;
        when "10011101000" =>    rd_en <= mc_read;
        when "01000011000" =>    rd_en <= mc_read;
        when "01000011001" =>    rd_en <= mc_read;
        when "00110011000" =>    rd_en <= mc_read;
        when "00110011001" =>    rd_en <= mc_read;
        when "10001110000" =>    rd_en <= mc_read;
        when "10001100000" =>    rd_en <= mc_read;
        when others =>    rd_en <= mc_nop;
    end case;
  end process;
end rd_en_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity dout_op_rom is
    port (addr       :in u_unsigned (10 downto 0);
          dout_op    :out mct_dout_op
         );
end dout_op_rom;

architecture dout_op_rom_arch of dout_op_rom is
begin
  process (addr)
  begin
    case addr is
        when "00001110101" =>    dout_op <= mc_dint3;
        when "00011110101" =>    dout_op <= mc_dint3;
        when "00000110100" =>    dout_op <= mc_dint3;
        when "00010110100" =>    dout_op <= mc_dint3;
        when "00000000010" =>    dout_op <= mc_pch;
        when "00000000011" =>    dout_op <= mc_pcl;
        when "00000000100" =>    dout_op <= mc_p_reg;
        when "11001110101" =>    dout_op <= mc_dint3;
        when "11011110101" =>    dout_op <= mc_dint3;
        when "11000110100" =>    dout_op <= mc_dint3;
        when "11010110100" =>    dout_op <= mc_dint3;
        when "11101110101" =>    dout_op <= mc_dint3;
        when "11111110101" =>    dout_op <= mc_dint3;
        when "11100110100" =>    dout_op <= mc_dint3;
        when "11110110100" =>    dout_op <= mc_dint3;
        when "00100000001" =>    dout_op <= mc_pch;
        when "00100000010" =>    dout_op <= mc_pcl;
        when "01001110101" =>    dout_op <= mc_dint3;
        when "01011110101" =>    dout_op <= mc_dint3;
        when "01000110100" =>    dout_op <= mc_dint3;
        when "01010110100" =>    dout_op <= mc_dint3;
        when "01001000000" =>    dout_op <= mc_a_reg;
        when "00001000000" =>    dout_op <= mc_p_reg;
        when "00101110101" =>    dout_op <= mc_dint3;
        when "00111110101" =>    dout_op <= mc_dint3;
        when "00100110100" =>    dout_op <= mc_dint3;
        when "00110110100" =>    dout_op <= mc_dint3;
        when "01101110101" =>    dout_op <= mc_dint3;
        when "01111110101" =>    dout_op <= mc_dint3;
        when "01100110100" =>    dout_op <= mc_dint3;
        when "01110110100" =>    dout_op <= mc_dint3;
        when "10001101010" =>    dout_op <= mc_a_reg;
        when "10011001010" =>    dout_op <= mc_a_reg;
        when "10000001100" =>    dout_op <= mc_a_reg;
        when "10010001100" =>    dout_op <= mc_a_reg;
        when "10000101001" =>    dout_op <= mc_a_reg;
        when "10010101001" =>    dout_op <= mc_a_reg;
        when "10011101010" =>    dout_op <= mc_a_reg;
        when "01000011010" =>    dout_op <= mc_pch;
        when "01000011011" =>    dout_op <= mc_pcl;
        when "01000011100" =>    dout_op <= mc_p_reg;
        when "00110011010" =>    dout_op <= mc_pch;
        when "00110011011" =>    dout_op <= mc_pcl;
        when "00110011100" =>    dout_op <= mc_p_reg;
        when "10001110010" =>    dout_op <= mc_x_reg;
        when "10000110001" =>    dout_op <= mc_x_reg;
        when "10010110001" =>    dout_op <= mc_x_reg;
        when "10001100010" =>    dout_op <= mc_y_reg;
        when "10000100001" =>    dout_op <= mc_y_reg;
        when "10010100001" =>    dout_op <= mc_y_reg;
        when others =>    dout_op <= mc_nop;
    end case;
  end process;
end dout_op_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity dint1_op_rom is
    port (addr       :in u_unsigned (10 downto 0);
          dint1_op   :out mct_dint1_op
         );
end dint1_op_rom;

architecture dint1_op_rom_arch of dint1_op_rom is
begin
  process (addr)
  begin
    case addr is
        when "01101101001" =>    dint1_op <= mc_din;
        when "01111101001" =>    dint1_op <= mc_din;
        when "01111001001" =>    dint1_op <= mc_din;
        when "01100001011" =>    dint1_op <= mc_din;
        when "01110001011" =>    dint1_op <= mc_din;
        when "00101101001" =>    dint1_op <= mc_din;
        when "00111101001" =>    dint1_op <= mc_din;
        when "00111001001" =>    dint1_op <= mc_din;
        when "00100001011" =>    dint1_op <= mc_din;
        when "00110001011" =>    dint1_op <= mc_din;
        when "00001110001" =>    dint1_op <= mc_din;
        when "00011110001" =>    dint1_op <= mc_din;
        when "00000110001" =>    dint1_op <= mc_din;
        when "00010110001" =>    dint1_op <= mc_din;
        when "00101100001" =>    dint1_op <= mc_din;
        when "00000000010" =>    dint1_op <= mc_din;
        when "11001101001" =>    dint1_op <= mc_din;
        when "11011101001" =>    dint1_op <= mc_din;
        when "11011001001" =>    dint1_op <= mc_din;
        when "11000001011" =>    dint1_op <= mc_din;
        when "11010001011" =>    dint1_op <= mc_din;
        when "11101100001" =>    dint1_op <= mc_din;
        when "11001100001" =>    dint1_op <= mc_din;
        when "11001110001" =>    dint1_op <= mc_din;
        when "11011110001" =>    dint1_op <= mc_din;
        when "11000110001" =>    dint1_op <= mc_din;
        when "11010110001" =>    dint1_op <= mc_din;
        when "01001101001" =>    dint1_op <= mc_din;
        when "01011101001" =>    dint1_op <= mc_din;
        when "01011001001" =>    dint1_op <= mc_din;
        when "01000001011" =>    dint1_op <= mc_din;
        when "01010001011" =>    dint1_op <= mc_din;
        when "11101110001" =>    dint1_op <= mc_din;
        when "11111110001" =>    dint1_op <= mc_din;
        when "11100110001" =>    dint1_op <= mc_din;
        when "11110110001" =>    dint1_op <= mc_din;
        when "01001100001" =>    dint1_op <= mc_din;
        when "01101100001" =>    dint1_op <= mc_din;
        when "01101100100" =>    dint1_op <= mc_din;
        when "00100000001" =>    dint1_op <= mc_din;
        when "10101101001" =>    dint1_op <= mc_din;
        when "10111101001" =>    dint1_op <= mc_din;
        when "10111001001" =>    dint1_op <= mc_din;
        when "10100001011" =>    dint1_op <= mc_din;
        when "10110001011" =>    dint1_op <= mc_din;
        when "10101110001" =>    dint1_op <= mc_din;
        when "10111110001" =>    dint1_op <= mc_din;
        when "10101100001" =>    dint1_op <= mc_din;
        when "10111100001" =>    dint1_op <= mc_din;
        when "01001110001" =>    dint1_op <= mc_din;
        when "01011110001" =>    dint1_op <= mc_din;
        when "01000110001" =>    dint1_op <= mc_din;
        when "01010110001" =>    dint1_op <= mc_din;
        when "00001101001" =>    dint1_op <= mc_din;
        when "00011101001" =>    dint1_op <= mc_din;
        when "00011001001" =>    dint1_op <= mc_din;
        when "00000001011" =>    dint1_op <= mc_din;
        when "00010001011" =>    dint1_op <= mc_din;
        when "00000011010" =>    dint1_op <= mc_din;
        when "00101110001" =>    dint1_op <= mc_din;
        when "00111110001" =>    dint1_op <= mc_din;
        when "00100110001" =>    dint1_op <= mc_din;
        when "00110110001" =>    dint1_op <= mc_din;
        when "01101110001" =>    dint1_op <= mc_din;
        when "01111110001" =>    dint1_op <= mc_din;
        when "01100110001" =>    dint1_op <= mc_din;
        when "01110110001" =>    dint1_op <= mc_din;
        when "01000000100" =>    dint1_op <= mc_din;
        when "01100000011" =>    dint1_op <= mc_din;
        when "11101101001" =>    dint1_op <= mc_din;
        when "11111101001" =>    dint1_op <= mc_din;
        when "11111001001" =>    dint1_op <= mc_din;
        when "11100001011" =>    dint1_op <= mc_din;
        when "11110001011" =>    dint1_op <= mc_din;
        when "10001101001" =>    dint1_op <= mc_din;
        when "10011001001" =>    dint1_op <= mc_din;
        when "10000001011" =>    dint1_op <= mc_din;
        when "10010001011" =>    dint1_op <= mc_din;
        when "10011101001" =>    dint1_op <= mc_din;
        when "01000011010" =>    dint1_op <= mc_din;
        when "00110011010" =>    dint1_op <= mc_din;
        when "10001110001" =>    dint1_op <= mc_din;
        when "10001100001" =>    dint1_op <= mc_din;
        when others =>    dint1_op <= mc_nop;
    end case;
  end process;
end dint1_op_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity dint2_op_rom is
    port (addr       :in u_unsigned (10 downto 0);
          dint2_op   :out mct_dint2_op
         );
end dint2_op_rom;

architecture dint2_op_rom_arch of dint2_op_rom is
begin
  process (addr)
  begin
    case addr is
        when "00001110010" =>    dint2_op <= mc_din;
        when "00011110010" =>    dint2_op <= mc_din;
        when "11001110010" =>    dint2_op <= mc_din;
        when "11011110010" =>    dint2_op <= mc_din;
        when "11101110010" =>    dint2_op <= mc_din;
        when "11111110010" =>    dint2_op <= mc_din;
        when "01001110010" =>    dint2_op <= mc_din;
        when "01011110010" =>    dint2_op <= mc_din;
        when "00101110010" =>    dint2_op <= mc_din;
        when "00111110010" =>    dint2_op <= mc_din;
        when "01101110010" =>    dint2_op <= mc_din;
        when "01111110010" =>    dint2_op <= mc_din;
        when others =>    dint2_op <= mc_nop;
    end case;
  end process;
end dint2_op_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity dint3_op_rom is
    port (addr       :in u_unsigned (10 downto 0);
          dint3_op   :out mct_dint3_op
         );
end dint3_op_rom;

architecture dint3_op_rom_arch of dint3_op_rom is
begin
  process (addr)
  begin
    case addr is
        when "00001110100" =>    dint3_op <= mc_alu;
        when "00011110100" =>    dint3_op <= mc_alu;
        when "00000110011" =>    dint3_op <= mc_alu;
        when "00010110011" =>    dint3_op <= mc_alu;
        when "11001110100" =>    dint3_op <= mc_alu;
        when "11011110100" =>    dint3_op <= mc_alu;
        when "11000110011" =>    dint3_op <= mc_alu;
        when "11010110011" =>    dint3_op <= mc_alu;
        when "11101110100" =>    dint3_op <= mc_alu;
        when "11111110100" =>    dint3_op <= mc_alu;
        when "11100110011" =>    dint3_op <= mc_alu;
        when "11110110011" =>    dint3_op <= mc_alu;
        when "01001110100" =>    dint3_op <= mc_alu;
        when "01011110100" =>    dint3_op <= mc_alu;
        when "01000110011" =>    dint3_op <= mc_alu;
        when "01010110011" =>    dint3_op <= mc_alu;
        when "00101110100" =>    dint3_op <= mc_alu;
        when "00111110100" =>    dint3_op <= mc_alu;
        when "00100110011" =>    dint3_op <= mc_alu;
        when "00110110011" =>    dint3_op <= mc_alu;
        when "01101110100" =>    dint3_op <= mc_alu;
        when "01111110100" =>    dint3_op <= mc_alu;
        when "01100110011" =>    dint3_op <= mc_alu;
        when "01110110011" =>    dint3_op <= mc_alu;
        when others =>    dint3_op <= mc_nop;
    end case;
  end process;
end dint3_op_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity pc_op_rom is
    port (addr       :in u_unsigned (10 downto 0);
          pc_op      :out mct_pc_op
         );
end pc_op_rom;

architecture pc_op_rom_arch of pc_op_rom is
begin
  process (addr)
  begin
    case addr is
        when "01101101000" =>    pc_op <= mc_inc;
        when "01101101001" =>    pc_op <= mc_inc;
        when "01111101000" =>    pc_op <= mc_inc;
        when "01111101001" =>    pc_op <= mc_inc;
        when "01111001000" =>    pc_op <= mc_inc;
        when "01111001001" =>    pc_op <= mc_inc;
        when "01101001000" =>    pc_op <= mc_inc;
        when "01100001000" =>    pc_op <= mc_inc;
        when "01110001000" =>    pc_op <= mc_inc;
        when "01100101000" =>    pc_op <= mc_inc;
        when "01110101000" =>    pc_op <= mc_inc;
        when "00101101000" =>    pc_op <= mc_inc;
        when "00101101001" =>    pc_op <= mc_inc;
        when "00111101000" =>    pc_op <= mc_inc;
        when "00111101001" =>    pc_op <= mc_inc;
        when "00111001000" =>    pc_op <= mc_inc;
        when "00111001001" =>    pc_op <= mc_inc;
        when "00101001000" =>    pc_op <= mc_inc;
        when "00100001000" =>    pc_op <= mc_inc;
        when "00110001000" =>    pc_op <= mc_inc;
        when "00100101000" =>    pc_op <= mc_inc;
        when "00110101000" =>    pc_op <= mc_inc;
        when "00001110000" =>    pc_op <= mc_inc;
        when "00001110001" =>    pc_op <= mc_inc;
        when "00011110000" =>    pc_op <= mc_inc;
        when "00011110001" =>    pc_op <= mc_inc;
        when "00000110000" =>    pc_op <= mc_inc;
        when "00010110000" =>    pc_op <= mc_inc;
        when "10010000000" =>    pc_op <= mc_inc;
        when "10010000001" =>    pc_op <= mc_bcc;
        when "10110000000" =>    pc_op <= mc_inc;
        when "10110000001" =>    pc_op <= mc_bcs;
        when "11110000000" =>    pc_op <= mc_inc;
        when "11110000001" =>    pc_op <= mc_beq;
        when "00101100000" =>    pc_op <= mc_inc;
        when "00101100001" =>    pc_op <= mc_inc;
        when "00100100000" =>    pc_op <= mc_inc;
        when "00110000000" =>    pc_op <= mc_inc;
        when "00110000001" =>    pc_op <= mc_bmi;
        when "11010000000" =>    pc_op <= mc_inc;
        when "11010000001" =>    pc_op <= mc_bne;
        when "00010000000" =>    pc_op <= mc_inc;
        when "00010000001" =>    pc_op <= mc_bpl;
        when "00000000000" =>    pc_op <= mc_inc;
        when "00000000011" =>    pc_op <= mc_split;
        when "01010000000" =>    pc_op <= mc_inc;
        when "01010000001" =>    pc_op <= mc_bvc;
        when "01110000000" =>    pc_op <= mc_inc;
        when "01110000001" =>    pc_op <= mc_bvs;
        when "11001101000" =>    pc_op <= mc_inc;
        when "11001101001" =>    pc_op <= mc_inc;
        when "11011101000" =>    pc_op <= mc_inc;
        when "11011101001" =>    pc_op <= mc_inc;
        when "11011001000" =>    pc_op <= mc_inc;
        when "11011001001" =>    pc_op <= mc_inc;
        when "11001001000" =>    pc_op <= mc_inc;
        when "11000001000" =>    pc_op <= mc_inc;
        when "11010001000" =>    pc_op <= mc_inc;
        when "11000101000" =>    pc_op <= mc_inc;
        when "11010101000" =>    pc_op <= mc_inc;
        when "11101100000" =>    pc_op <= mc_inc;
        when "11101100001" =>    pc_op <= mc_inc;
        when "11100000000" =>    pc_op <= mc_inc;
        when "11100100000" =>    pc_op <= mc_inc;
        when "11001100000" =>    pc_op <= mc_inc;
        when "11001100001" =>    pc_op <= mc_inc;
        when "11000000000" =>    pc_op <= mc_inc;
        when "11000100000" =>    pc_op <= mc_inc;
        when "11001110000" =>    pc_op <= mc_inc;
        when "11001110001" =>    pc_op <= mc_inc;
        when "11011110000" =>    pc_op <= mc_inc;
        when "11011110001" =>    pc_op <= mc_inc;
        when "11000110000" =>    pc_op <= mc_inc;
        when "11010110000" =>    pc_op <= mc_inc;
        when "01001101000" =>    pc_op <= mc_inc;
        when "01001101001" =>    pc_op <= mc_inc;
        when "01011101000" =>    pc_op <= mc_inc;
        when "01011101001" =>    pc_op <= mc_inc;
        when "01011001000" =>    pc_op <= mc_inc;
        when "01011001001" =>    pc_op <= mc_inc;
        when "01001001000" =>    pc_op <= mc_inc;
        when "01000001000" =>    pc_op <= mc_inc;
        when "01010001000" =>    pc_op <= mc_inc;
        when "01000101000" =>    pc_op <= mc_inc;
        when "01010101000" =>    pc_op <= mc_inc;
        when "11101110000" =>    pc_op <= mc_inc;
        when "11101110001" =>    pc_op <= mc_inc;
        when "11111110000" =>    pc_op <= mc_inc;
        when "11111110001" =>    pc_op <= mc_inc;
        when "11100110000" =>    pc_op <= mc_inc;
        when "11110110000" =>    pc_op <= mc_inc;
        when "01001100000" =>    pc_op <= mc_inc;
        when "01001100001" =>    pc_op <= mc_inc;
        when "01001100010" =>    pc_op <= mc_split;
        when "01101100000" =>    pc_op <= mc_inc;
        when "01101100101" =>    pc_op <= mc_split;
        when "00100000000" =>    pc_op <= mc_inc;
        when "00100000010" =>    pc_op <= mc_split;
        when "10101101000" =>    pc_op <= mc_inc;
        when "10101101001" =>    pc_op <= mc_inc;
        when "10111101000" =>    pc_op <= mc_inc;
        when "10111101001" =>    pc_op <= mc_inc;
        when "10111001000" =>    pc_op <= mc_inc;
        when "10111001001" =>    pc_op <= mc_inc;
        when "10101001000" =>    pc_op <= mc_inc;
        when "10100101000" =>    pc_op <= mc_inc;
        when "10100001000" =>    pc_op <= mc_inc;
        when "10110001000" =>    pc_op <= mc_inc;
        when "10110101000" =>    pc_op <= mc_inc;
        when "10101110000" =>    pc_op <= mc_inc;
        when "10101110001" =>    pc_op <= mc_inc;
        when "10111110000" =>    pc_op <= mc_inc;
        when "10111110001" =>    pc_op <= mc_inc;
        when "10100010000" =>    pc_op <= mc_inc;
        when "10100110000" =>    pc_op <= mc_inc;
        when "10110110000" =>    pc_op <= mc_inc;
        when "10101100000" =>    pc_op <= mc_inc;
        when "10101100001" =>    pc_op <= mc_inc;
        when "10111100000" =>    pc_op <= mc_inc;
        when "10111100001" =>    pc_op <= mc_inc;
        when "10100000000" =>    pc_op <= mc_inc;
        when "10100100000" =>    pc_op <= mc_inc;
        when "10110100000" =>    pc_op <= mc_inc;
        when "01001110000" =>    pc_op <= mc_inc;
        when "01001110001" =>    pc_op <= mc_inc;
        when "01011110000" =>    pc_op <= mc_inc;
        when "01011110001" =>    pc_op <= mc_inc;
        when "01000110000" =>    pc_op <= mc_inc;
        when "01010110000" =>    pc_op <= mc_inc;
        when "00001101000" =>    pc_op <= mc_inc;
        when "00001101001" =>    pc_op <= mc_inc;
        when "00011101000" =>    pc_op <= mc_inc;
        when "00011101001" =>    pc_op <= mc_inc;
        when "00011001000" =>    pc_op <= mc_inc;
        when "00011001001" =>    pc_op <= mc_inc;
        when "00001001000" =>    pc_op <= mc_inc;
        when "00000001000" =>    pc_op <= mc_inc;
        when "00010001000" =>    pc_op <= mc_inc;
        when "00000101000" =>    pc_op <= mc_inc;
        when "00010101000" =>    pc_op <= mc_inc;
        when "00000011011" =>    pc_op <= mc_split;
        when "00101110000" =>    pc_op <= mc_inc;
        when "00101110001" =>    pc_op <= mc_inc;
        when "00111110000" =>    pc_op <= mc_inc;
        when "00111110001" =>    pc_op <= mc_inc;
        when "00100110000" =>    pc_op <= mc_inc;
        when "00110110000" =>    pc_op <= mc_inc;
        when "01101110000" =>    pc_op <= mc_inc;
        when "01101110001" =>    pc_op <= mc_inc;
        when "01111110000" =>    pc_op <= mc_inc;
        when "01111110001" =>    pc_op <= mc_inc;
        when "01100110000" =>    pc_op <= mc_inc;
        when "01110110000" =>    pc_op <= mc_inc;
        when "01000000101" =>    pc_op <= mc_split;
        when "01100000100" =>    pc_op <= mc_split;
        when "01100000101" =>    pc_op <= mc_inc;
        when "11101101000" =>    pc_op <= mc_inc;
        when "11101101001" =>    pc_op <= mc_inc;
        when "11111101000" =>    pc_op <= mc_inc;
        when "11111101001" =>    pc_op <= mc_inc;
        when "11111001000" =>    pc_op <= mc_inc;
        when "11111001001" =>    pc_op <= mc_inc;
        when "11101001000" =>    pc_op <= mc_inc;
        when "11100001000" =>    pc_op <= mc_inc;
        when "11110001000" =>    pc_op <= mc_inc;
        when "11100101000" =>    pc_op <= mc_inc;
        when "11110101000" =>    pc_op <= mc_inc;
        when "10001101000" =>    pc_op <= mc_inc;
        when "10001101001" =>    pc_op <= mc_inc;
        when "10011001000" =>    pc_op <= mc_inc;
        when "10011001001" =>    pc_op <= mc_inc;
        when "10000001000" =>    pc_op <= mc_inc;
        when "10010001000" =>    pc_op <= mc_inc;
        when "10000101000" =>    pc_op <= mc_inc;
        when "10010101000" =>    pc_op <= mc_inc;
        when "10011101000" =>    pc_op <= mc_inc;
        when "10011101001" =>    pc_op <= mc_inc;
        when "01000011011" =>    pc_op <= mc_split;
        when "00110011011" =>    pc_op <= mc_split;
        when "10001110000" =>    pc_op <= mc_inc;
        when "10001110001" =>    pc_op <= mc_inc;
        when "10000110000" =>    pc_op <= mc_inc;
        when "10010110000" =>    pc_op <= mc_inc;
        when "10001100000" =>    pc_op <= mc_inc;
        when "10001100001" =>    pc_op <= mc_inc;
        when "10000100000" =>    pc_op <= mc_inc;
        when "10010100000" =>    pc_op <= mc_inc;
        when others =>    pc_op <= mc_nop;
    end case;
  end process;
end pc_op_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity sp_op_rom is
    port (addr       :in u_unsigned (10 downto 0);
          sp_op      :out mct_sp_op
         );
end sp_op_rom;

architecture sp_op_rom_arch of sp_op_rom is
begin
  process (addr)
  begin
    case addr is
        when "00000000010" =>    sp_op <= mc_push;
        when "00000000011" =>    sp_op <= mc_push;
        when "00000000100" =>    sp_op <= mc_push;
        when "00100000001" =>    sp_op <= mc_push;
        when "00100000010" =>    sp_op <= mc_push;
        when "01001000000" =>    sp_op <= mc_push;
        when "00001000000" =>    sp_op <= mc_push;
        when "01101000000" =>    sp_op <= mc_pop;
        when "00101000000" =>    sp_op <= mc_pop;
        when "01000000000" =>    sp_op <= mc_pop;
        when "01000000001" =>    sp_op <= mc_pop;
        when "01000000010" =>    sp_op <= mc_pop;
        when "01100000000" =>    sp_op <= mc_pop;
        when "01100000001" =>    sp_op <= mc_pop;
        when "01000011010" =>    sp_op <= mc_push;
        when "01000011011" =>    sp_op <= mc_push;
        when "01000011100" =>    sp_op <= mc_push;
        when "00110011010" =>    sp_op <= mc_push;
        when "00110011011" =>    sp_op <= mc_push;
        when "00110011100" =>    sp_op <= mc_push;
        when "10011010000" =>    sp_op <= mc_x_reg;
        when others =>    sp_op <= mc_nop;
    end case;
  end process;
end sp_op_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity alu1_rom is
    port (addr       :in u_unsigned (10 downto 0);
          alu1       :out mct_alu1
         );
end alu1_rom;

architecture alu1_rom_arch of alu1_rom is
begin
  process (addr)
  begin
    case addr is
        when "01101101100" =>    alu1 <= mc_a_reg;
        when "01111101100" =>    alu1 <= mc_a_reg;
        when "01111001100" =>    alu1 <= mc_a_reg;
        when "01101001001" =>    alu1 <= mc_a_reg;
        when "01100001110" =>    alu1 <= mc_a_reg;
        when "01110001110" =>    alu1 <= mc_a_reg;
        when "01100101011" =>    alu1 <= mc_a_reg;
        when "01110101011" =>    alu1 <= mc_a_reg;
        when "00101101100" =>    alu1 <= mc_a_reg;
        when "00111101100" =>    alu1 <= mc_a_reg;
        when "00111001100" =>    alu1 <= mc_a_reg;
        when "00101001001" =>    alu1 <= mc_a_reg;
        when "00100001110" =>    alu1 <= mc_a_reg;
        when "00110001110" =>    alu1 <= mc_a_reg;
        when "00100101011" =>    alu1 <= mc_a_reg;
        when "00110101011" =>    alu1 <= mc_a_reg;
        when "00001110100" =>    alu1 <= mc_din;
        when "00011110100" =>    alu1 <= mc_din;
        when "00001010000" =>    alu1 <= mc_a_reg;
        when "00000110011" =>    alu1 <= mc_din;
        when "00010110011" =>    alu1 <= mc_din;
        when "00101100100" =>    alu1 <= mc_a_reg;
        when "00100100011" =>    alu1 <= mc_a_reg;
        when "11001101100" =>    alu1 <= mc_a_reg;
        when "11011101100" =>    alu1 <= mc_a_reg;
        when "11011001100" =>    alu1 <= mc_a_reg;
        when "11001001001" =>    alu1 <= mc_a_reg;
        when "11000001110" =>    alu1 <= mc_a_reg;
        when "11010001110" =>    alu1 <= mc_a_reg;
        when "11000101011" =>    alu1 <= mc_a_reg;
        when "11010101011" =>    alu1 <= mc_a_reg;
        when "11101100100" =>    alu1 <= mc_x_reg;
        when "11100000001" =>    alu1 <= mc_x_reg;
        when "11100100011" =>    alu1 <= mc_x_reg;
        when "11001100100" =>    alu1 <= mc_y_reg;
        when "11000000001" =>    alu1 <= mc_y_reg;
        when "11000100011" =>    alu1 <= mc_y_reg;
        when "11001110100" =>    alu1 <= mc_din;
        when "11011110100" =>    alu1 <= mc_din;
        when "11000110011" =>    alu1 <= mc_din;
        when "11010110011" =>    alu1 <= mc_din;
        when "11001010000" =>    alu1 <= mc_x_reg;
        when "10001000000" =>    alu1 <= mc_y_reg;
        when "01001101100" =>    alu1 <= mc_a_reg;
        when "01011101100" =>    alu1 <= mc_a_reg;
        when "01011001100" =>    alu1 <= mc_a_reg;
        when "01001001001" =>    alu1 <= mc_a_reg;
        when "01000001110" =>    alu1 <= mc_a_reg;
        when "01010001110" =>    alu1 <= mc_a_reg;
        when "01000101011" =>    alu1 <= mc_a_reg;
        when "01010101011" =>    alu1 <= mc_a_reg;
        when "11101110100" =>    alu1 <= mc_din;
        when "11111110100" =>    alu1 <= mc_din;
        when "11100110011" =>    alu1 <= mc_din;
        when "11110110011" =>    alu1 <= mc_din;
        when "11101000000" =>    alu1 <= mc_x_reg;
        when "11001000000" =>    alu1 <= mc_y_reg;
        when "01001110100" =>    alu1 <= mc_din;
        when "01011110100" =>    alu1 <= mc_din;
        when "01001010000" =>    alu1 <= mc_a_reg;
        when "01000110011" =>    alu1 <= mc_din;
        when "01010110011" =>    alu1 <= mc_din;
        when "00001101100" =>    alu1 <= mc_a_reg;
        when "00011101100" =>    alu1 <= mc_a_reg;
        when "00011001100" =>    alu1 <= mc_a_reg;
        when "00001001001" =>    alu1 <= mc_a_reg;
        when "00000001110" =>    alu1 <= mc_a_reg;
        when "00010001110" =>    alu1 <= mc_a_reg;
        when "00000101011" =>    alu1 <= mc_a_reg;
        when "00010101011" =>    alu1 <= mc_a_reg;
        when "00101110100" =>    alu1 <= mc_din;
        when "00111110100" =>    alu1 <= mc_din;
        when "00101010000" =>    alu1 <= mc_a_reg;
        when "00100110011" =>    alu1 <= mc_din;
        when "00110110011" =>    alu1 <= mc_din;
        when "01101110100" =>    alu1 <= mc_din;
        when "01111110100" =>    alu1 <= mc_din;
        when "01101010000" =>    alu1 <= mc_a_reg;
        when "01100110011" =>    alu1 <= mc_din;
        when "01110110011" =>    alu1 <= mc_din;
        when "11101101100" =>    alu1 <= mc_a_reg;
        when "11111101100" =>    alu1 <= mc_a_reg;
        when "11111001100" =>    alu1 <= mc_a_reg;
        when "11101001001" =>    alu1 <= mc_a_reg;
        when "11100001110" =>    alu1 <= mc_a_reg;
        when "11110001110" =>    alu1 <= mc_a_reg;
        when "11100101011" =>    alu1 <= mc_a_reg;
        when "11110101011" =>    alu1 <= mc_a_reg;
        when "10101010000" =>    alu1 <= mc_a_reg;
        when "10101000000" =>    alu1 <= mc_a_reg;
        when "10001010000" =>    alu1 <= mc_x_reg;
        when "10011000000" =>    alu1 <= mc_y_reg;
        when others =>    alu1 <= mc_a_reg;
    end case;
  end process;
end alu1_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity alu2_rom is
    port (addr       :in u_unsigned (10 downto 0);
          alu2       :out mct_alu2
         );
end alu2_rom;

architecture alu2_rom_arch of alu2_rom is
begin
  process (addr)
  begin
    case addr is
        when "01101101100" =>    alu2 <= mc_din;
        when "01111101100" =>    alu2 <= mc_din;
        when "01111001100" =>    alu2 <= mc_din;
        when "01101001001" =>    alu2 <= mc_din;
        when "01100001110" =>    alu2 <= mc_din;
        when "01110001110" =>    alu2 <= mc_din;
        when "01100101011" =>    alu2 <= mc_din;
        when "01110101011" =>    alu2 <= mc_din;
        when "00101101100" =>    alu2 <= mc_din;
        when "00111101100" =>    alu2 <= mc_din;
        when "00111001100" =>    alu2 <= mc_din;
        when "00101001001" =>    alu2 <= mc_din;
        when "00100001110" =>    alu2 <= mc_din;
        when "00110001110" =>    alu2 <= mc_din;
        when "00100101011" =>    alu2 <= mc_din;
        when "00110101011" =>    alu2 <= mc_din;
        when "00101100100" =>    alu2 <= mc_din;
        when "00100100011" =>    alu2 <= mc_din;
        when "11001101100" =>    alu2 <= mc_din;
        when "11011101100" =>    alu2 <= mc_din;
        when "11011001100" =>    alu2 <= mc_din;
        when "11001001001" =>    alu2 <= mc_din;
        when "11000001110" =>    alu2 <= mc_din;
        when "11010001110" =>    alu2 <= mc_din;
        when "11000101011" =>    alu2 <= mc_din;
        when "11010101011" =>    alu2 <= mc_din;
        when "11101100100" =>    alu2 <= mc_din;
        when "11100000001" =>    alu2 <= mc_din;
        when "11100100011" =>    alu2 <= mc_din;
        when "11001100100" =>    alu2 <= mc_din;
        when "11000000001" =>    alu2 <= mc_din;
        when "11000100011" =>    alu2 <= mc_din;
        when "11001110100" =>    alu2 <= mc_one;
        when "11011110100" =>    alu2 <= mc_one;
        when "11000110011" =>    alu2 <= mc_one;
        when "11010110011" =>    alu2 <= mc_one;
        when "11001010000" =>    alu2 <= mc_one;
        when "10001000000" =>    alu2 <= mc_one;
        when "01001101100" =>    alu2 <= mc_din;
        when "01011101100" =>    alu2 <= mc_din;
        when "01011001100" =>    alu2 <= mc_din;
        when "01001001001" =>    alu2 <= mc_din;
        when "01000001110" =>    alu2 <= mc_din;
        when "01010001110" =>    alu2 <= mc_din;
        when "01000101011" =>    alu2 <= mc_din;
        when "01010101011" =>    alu2 <= mc_din;
        when "11101110100" =>    alu2 <= mc_one;
        when "11111110100" =>    alu2 <= mc_one;
        when "11100110011" =>    alu2 <= mc_one;
        when "11110110011" =>    alu2 <= mc_one;
        when "11101000000" =>    alu2 <= mc_one;
        when "11001000000" =>    alu2 <= mc_one;
        when "10101101100" =>    alu2 <= mc_din;
        when "10111101100" =>    alu2 <= mc_din;
        when "10111001100" =>    alu2 <= mc_din;
        when "10101001001" =>    alu2 <= mc_din;
        when "10100101011" =>    alu2 <= mc_din;
        when "10100001110" =>    alu2 <= mc_din;
        when "10110001110" =>    alu2 <= mc_din;
        when "10110101011" =>    alu2 <= mc_din;
        when "10101110100" =>    alu2 <= mc_din;
        when "10111110100" =>    alu2 <= mc_din;
        when "10100010001" =>    alu2 <= mc_din;
        when "10100110011" =>    alu2 <= mc_din;
        when "10110110011" =>    alu2 <= mc_din;
        when "10101100100" =>    alu2 <= mc_din;
        when "10111100100" =>    alu2 <= mc_din;
        when "10100000001" =>    alu2 <= mc_din;
        when "10100100011" =>    alu2 <= mc_din;
        when "10110100011" =>    alu2 <= mc_din;
        when "00001101100" =>    alu2 <= mc_din;
        when "00011101100" =>    alu2 <= mc_din;
        when "00011001100" =>    alu2 <= mc_din;
        when "00001001001" =>    alu2 <= mc_din;
        when "00000001110" =>    alu2 <= mc_din;
        when "00010001110" =>    alu2 <= mc_din;
        when "00000101011" =>    alu2 <= mc_din;
        when "00010101011" =>    alu2 <= mc_din;
        when "01101000011" =>    alu2 <= mc_din;
        when "11101101100" =>    alu2 <= mc_din;
        when "11111101100" =>    alu2 <= mc_din;
        when "11111001100" =>    alu2 <= mc_din;
        when "11101001001" =>    alu2 <= mc_din;
        when "11100001110" =>    alu2 <= mc_din;
        when "11110001110" =>    alu2 <= mc_din;
        when "11100101011" =>    alu2 <= mc_din;
        when "11110101011" =>    alu2 <= mc_din;
        when "10111010000" =>    alu2 <= mc_sp_reg;
        when others =>    alu2 <= mc_din;
    end case;
  end process;
end alu2_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity alu_op_rom is
    port (addr       :in u_unsigned (10 downto 0);
          alu_op     :out mct_alu_op
         );
end alu_op_rom;

architecture alu_op_rom_arch of alu_op_rom is
begin
  process (addr)
  begin
    case addr is
        when "01101101100" =>    alu_op <= mc_addc;
        when "01111101100" =>    alu_op <= mc_addc;
        when "01111001100" =>    alu_op <= mc_addc;
        when "01101001001" =>    alu_op <= mc_addc;
        when "01100001110" =>    alu_op <= mc_addc;
        when "01110001110" =>    alu_op <= mc_addc;
        when "01100101011" =>    alu_op <= mc_addc;
        when "01110101011" =>    alu_op <= mc_addc;
        when "00101101100" =>    alu_op <= mc_bit_and;
        when "00111101100" =>    alu_op <= mc_bit_and;
        when "00111001100" =>    alu_op <= mc_bit_and;
        when "00101001001" =>    alu_op <= mc_bit_and;
        when "00100001110" =>    alu_op <= mc_bit_and;
        when "00110001110" =>    alu_op <= mc_bit_and;
        when "00100101011" =>    alu_op <= mc_bit_and;
        when "00110101011" =>    alu_op <= mc_bit_and;
        when "00001110100" =>    alu_op <= mc_bit_asl;
        when "00011110100" =>    alu_op <= mc_bit_asl;
        when "00001010000" =>    alu_op <= mc_bit_asl;
        when "00000110011" =>    alu_op <= mc_bit_asl;
        when "00010110011" =>    alu_op <= mc_bit_asl;
        when "00101100100" =>    alu_op <= mc_bit_and;
        when "00100100011" =>    alu_op <= mc_bit_and;
        when "11001101100" =>    alu_op <= mc_sub;
        when "11011101100" =>    alu_op <= mc_sub;
        when "11011001100" =>    alu_op <= mc_sub;
        when "11001001001" =>    alu_op <= mc_sub;
        when "11000001110" =>    alu_op <= mc_sub;
        when "11010001110" =>    alu_op <= mc_sub;
        when "11000101011" =>    alu_op <= mc_sub;
        when "11010101011" =>    alu_op <= mc_sub;
        when "11101100100" =>    alu_op <= mc_sub;
        when "11100000001" =>    alu_op <= mc_sub;
        when "11100100011" =>    alu_op <= mc_sub;
        when "11001100100" =>    alu_op <= mc_sub;
        when "11000000001" =>    alu_op <= mc_sub;
        when "11000100011" =>    alu_op <= mc_sub;
        when "11001110100" =>    alu_op <= mc_sub;
        when "11011110100" =>    alu_op <= mc_sub;
        when "11000110011" =>    alu_op <= mc_sub;
        when "11010110011" =>    alu_op <= mc_sub;
        when "11001010000" =>    alu_op <= mc_sub;
        when "10001000000" =>    alu_op <= mc_sub;
        when "01001101100" =>    alu_op <= mc_bit_xor;
        when "01011101100" =>    alu_op <= mc_bit_xor;
        when "01011001100" =>    alu_op <= mc_bit_xor;
        when "01001001001" =>    alu_op <= mc_bit_xor;
        when "01000001110" =>    alu_op <= mc_bit_xor;
        when "01010001110" =>    alu_op <= mc_bit_xor;
        when "01000101011" =>    alu_op <= mc_bit_xor;
        when "01010101011" =>    alu_op <= mc_bit_xor;
        when "11101110100" =>    alu_op <= mc_add;
        when "11111110100" =>    alu_op <= mc_add;
        when "11100110011" =>    alu_op <= mc_add;
        when "11110110011" =>    alu_op <= mc_add;
        when "11101000000" =>    alu_op <= mc_add;
        when "11001000000" =>    alu_op <= mc_add;
        when "10101101100" =>    alu_op <= mc_pass2;
        when "10111101100" =>    alu_op <= mc_pass2;
        when "10111001100" =>    alu_op <= mc_pass2;
        when "10101001001" =>    alu_op <= mc_pass2;
        when "10100101011" =>    alu_op <= mc_pass2;
        when "10100001110" =>    alu_op <= mc_pass2;
        when "10110001110" =>    alu_op <= mc_pass2;
        when "10110101011" =>    alu_op <= mc_pass2;
        when "10101110100" =>    alu_op <= mc_pass2;
        when "10111110100" =>    alu_op <= mc_pass2;
        when "10100010001" =>    alu_op <= mc_pass2;
        when "10100110011" =>    alu_op <= mc_pass2;
        when "10110110011" =>    alu_op <= mc_pass2;
        when "10101100100" =>    alu_op <= mc_pass2;
        when "10111100100" =>    alu_op <= mc_pass2;
        when "10100000001" =>    alu_op <= mc_pass2;
        when "10100100011" =>    alu_op <= mc_pass2;
        when "10110100011" =>    alu_op <= mc_pass2;
        when "01001110100" =>    alu_op <= mc_bit_lsr;
        when "01011110100" =>    alu_op <= mc_bit_lsr;
        when "01001010000" =>    alu_op <= mc_bit_lsr;
        when "01000110011" =>    alu_op <= mc_bit_lsr;
        when "01010110011" =>    alu_op <= mc_bit_lsr;
        when "00001101100" =>    alu_op <= mc_bit_or;
        when "00011101100" =>    alu_op <= mc_bit_or;
        when "00011001100" =>    alu_op <= mc_bit_or;
        when "00001001001" =>    alu_op <= mc_bit_or;
        when "00000001110" =>    alu_op <= mc_bit_or;
        when "00010001110" =>    alu_op <= mc_bit_or;
        when "00000101011" =>    alu_op <= mc_bit_or;
        when "00010101011" =>    alu_op <= mc_bit_or;
        when "01101000011" =>    alu_op <= mc_pass2;
        when "00101110100" =>    alu_op <= mc_bit_rol;
        when "00111110100" =>    alu_op <= mc_bit_rol;
        when "00101010000" =>    alu_op <= mc_bit_rol;
        when "00100110011" =>    alu_op <= mc_bit_rol;
        when "00110110011" =>    alu_op <= mc_bit_rol;
        when "01101110100" =>    alu_op <= mc_bit_ror;
        when "01111110100" =>    alu_op <= mc_bit_ror;
        when "01101010000" =>    alu_op <= mc_bit_ror;
        when "01100110011" =>    alu_op <= mc_bit_ror;
        when "01110110011" =>    alu_op <= mc_bit_ror;
        when "11101101100" =>    alu_op <= mc_subb;
        when "11111101100" =>    alu_op <= mc_subb;
        when "11111001100" =>    alu_op <= mc_subb;
        when "11101001001" =>    alu_op <= mc_subb;
        when "11100001110" =>    alu_op <= mc_subb;
        when "11110001110" =>    alu_op <= mc_subb;
        when "11100101011" =>    alu_op <= mc_subb;
        when "11110101011" =>    alu_op <= mc_subb;
        when "10101010000" =>    alu_op <= mc_pass1;
        when "10101000000" =>    alu_op <= mc_pass1;
        when "10111010000" =>    alu_op <= mc_pass2;
        when "10001010000" =>    alu_op <= mc_pass1;
        when "10011000000" =>    alu_op <= mc_pass1;
        when others =>    alu_op <= mc_pass1;
    end case;
  end process;
end alu_op_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity a_le_rom is
    port (addr       :in u_unsigned (10 downto 0);
          a_le       :out mct_a_le
         );
end a_le_rom;

architecture a_le_rom_arch of a_le_rom is
begin
  process (addr)
  begin
    case addr is
        when "01101101100" =>    a_le <= mc_le;
        when "01111101100" =>    a_le <= mc_le;
        when "01111001100" =>    a_le <= mc_le;
        when "01101001001" =>    a_le <= mc_le;
        when "01100001110" =>    a_le <= mc_le;
        when "01110001110" =>    a_le <= mc_le;
        when "01100101011" =>    a_le <= mc_le;
        when "01110101011" =>    a_le <= mc_le;
        when "00101101100" =>    a_le <= mc_le;
        when "00111101100" =>    a_le <= mc_le;
        when "00111001100" =>    a_le <= mc_le;
        when "00101001001" =>    a_le <= mc_le;
        when "00100001110" =>    a_le <= mc_le;
        when "00110001110" =>    a_le <= mc_le;
        when "00100101011" =>    a_le <= mc_le;
        when "00110101011" =>    a_le <= mc_le;
        when "00001010000" =>    a_le <= mc_le;
        when "01001101100" =>    a_le <= mc_le;
        when "01011101100" =>    a_le <= mc_le;
        when "01011001100" =>    a_le <= mc_le;
        when "01001001001" =>    a_le <= mc_le;
        when "01000001110" =>    a_le <= mc_le;
        when "01010001110" =>    a_le <= mc_le;
        when "01000101011" =>    a_le <= mc_le;
        when "01010101011" =>    a_le <= mc_le;
        when "10101101100" =>    a_le <= mc_le;
        when "10111101100" =>    a_le <= mc_le;
        when "10111001100" =>    a_le <= mc_le;
        when "10101001001" =>    a_le <= mc_le;
        when "10100101011" =>    a_le <= mc_le;
        when "10100001110" =>    a_le <= mc_le;
        when "10110001110" =>    a_le <= mc_le;
        when "10110101011" =>    a_le <= mc_le;
        when "01001010000" =>    a_le <= mc_le;
        when "00001101100" =>    a_le <= mc_le;
        when "00011101100" =>    a_le <= mc_le;
        when "00011001100" =>    a_le <= mc_le;
        when "00001001001" =>    a_le <= mc_le;
        when "00000001110" =>    a_le <= mc_le;
        when "00010001110" =>    a_le <= mc_le;
        when "00000101011" =>    a_le <= mc_le;
        when "00010101011" =>    a_le <= mc_le;
        when "01101000011" =>    a_le <= mc_le;
        when "00101010000" =>    a_le <= mc_le;
        when "01101010000" =>    a_le <= mc_le;
        when "11101101100" =>    a_le <= mc_le;
        when "11111101100" =>    a_le <= mc_le;
        when "11111001100" =>    a_le <= mc_le;
        when "11101001001" =>    a_le <= mc_le;
        when "11100001110" =>    a_le <= mc_le;
        when "11110001110" =>    a_le <= mc_le;
        when "11100101011" =>    a_le <= mc_le;
        when "11110101011" =>    a_le <= mc_le;
        when "10001010000" =>    a_le <= mc_le;
        when "10011000000" =>    a_le <= mc_le;
        when others =>    a_le <= mc_nop;
    end case;
  end process;
end a_le_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity x_le_rom is
    port (addr       :in u_unsigned (10 downto 0);
          x_le       :out mct_x_le
         );
end x_le_rom;

architecture x_le_rom_arch of x_le_rom is
begin
  process (addr)
  begin
    case addr is
        when "11001010000" =>    x_le <= mc_le;
        when "11101000000" =>    x_le <= mc_le;
        when "10101110100" =>    x_le <= mc_le;
        when "10111110100" =>    x_le <= mc_le;
        when "10100010001" =>    x_le <= mc_le;
        when "10100110011" =>    x_le <= mc_le;
        when "10110110011" =>    x_le <= mc_le;
        when "10101010000" =>    x_le <= mc_le;
        when "10111010000" =>    x_le <= mc_le;
        when others =>    x_le <= mc_nop;
    end case;
  end process;
end x_le_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity y_le_rom is
    port (addr       :in u_unsigned (10 downto 0);
          y_le       :out mct_y_le
         );
end y_le_rom;

architecture y_le_rom_arch of y_le_rom is
begin
  process (addr)
  begin
    case addr is
        when "10001000000" =>    y_le <= mc_le;
        when "11001000000" =>    y_le <= mc_le;
        when "10101100100" =>    y_le <= mc_le;
        when "10111100100" =>    y_le <= mc_le;
        when "10100000001" =>    y_le <= mc_le;
        when "10100100011" =>    y_le <= mc_le;
        when "10110100011" =>    y_le <= mc_le;
        when "10101000000" =>    y_le <= mc_le;
        when others =>    y_le <= mc_nop;
    end case;
  end process;
end y_le_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity flag_op_rom is
    port (addr       :in u_unsigned (10 downto 0);
          flag_op    :out mct_flag_op
         );
end flag_op_rom;

architecture flag_op_rom_arch of flag_op_rom is
begin
  process (addr)
  begin
    case addr is
        when "01101101100" =>    flag_op <= mc_nvzc;
        when "01111101100" =>    flag_op <= mc_nvzc;
        when "01111001100" =>    flag_op <= mc_nvzc;
        when "01101001001" =>    flag_op <= mc_nvzc;
        when "01100001110" =>    flag_op <= mc_nvzc;
        when "01110001110" =>    flag_op <= mc_nvzc;
        when "01100101011" =>    flag_op <= mc_nvzc;
        when "01110101011" =>    flag_op <= mc_nvzc;
        when "00101101100" =>    flag_op <= mc_nz;
        when "00111101100" =>    flag_op <= mc_nz;
        when "00111001100" =>    flag_op <= mc_nz;
        when "00101001001" =>    flag_op <= mc_nz;
        when "00100001110" =>    flag_op <= mc_nz;
        when "00110001110" =>    flag_op <= mc_nz;
        when "00100101011" =>    flag_op <= mc_nz;
        when "00110101011" =>    flag_op <= mc_nz;
        when "00001110100" =>    flag_op <= mc_nzc;
        when "00011110100" =>    flag_op <= mc_nzc;
        when "00001010000" =>    flag_op <= mc_nzc;
        when "00000110011" =>    flag_op <= mc_nzc;
        when "00010110011" =>    flag_op <= mc_nzc;
        when "00101100100" =>    flag_op <= mc_bit;
        when "00100100011" =>    flag_op <= mc_bit;
        when "00000000001" =>    flag_op <= mc_setb;
        when "00000000101" =>    flag_op <= mc_seti;
        when "00011000000" =>    flag_op <= mc_clearc;
        when "11011000000" =>    flag_op <= mc_cleard;
        when "01011000000" =>    flag_op <= mc_cleari;
        when "10111000000" =>    flag_op <= mc_clearv;
        when "11001101100" =>    flag_op <= mc_nzc;
        when "11011101100" =>    flag_op <= mc_nzc;
        when "11011001100" =>    flag_op <= mc_nzc;
        when "11001001001" =>    flag_op <= mc_nzc;
        when "11000001110" =>    flag_op <= mc_nzc;
        when "11010001110" =>    flag_op <= mc_nzc;
        when "11000101011" =>    flag_op <= mc_nzc;
        when "11010101011" =>    flag_op <= mc_nzc;
        when "11101100100" =>    flag_op <= mc_nzc;
        when "11100000001" =>    flag_op <= mc_nzc;
        when "11100100011" =>    flag_op <= mc_nzc;
        when "11001100100" =>    flag_op <= mc_nzc;
        when "11000000001" =>    flag_op <= mc_nzc;
        when "11000100011" =>    flag_op <= mc_nzc;
        when "11001110100" =>    flag_op <= mc_nz;
        when "11011110100" =>    flag_op <= mc_nz;
        when "11000110011" =>    flag_op <= mc_nz;
        when "11010110011" =>    flag_op <= mc_nz;
        when "11001010000" =>    flag_op <= mc_nz;
        when "10001000000" =>    flag_op <= mc_nz;
        when "01001101100" =>    flag_op <= mc_nz;
        when "01011101100" =>    flag_op <= mc_nz;
        when "01011001100" =>    flag_op <= mc_nz;
        when "01001001001" =>    flag_op <= mc_nz;
        when "01000001110" =>    flag_op <= mc_nz;
        when "01010001110" =>    flag_op <= mc_nz;
        when "01000101011" =>    flag_op <= mc_nz;
        when "01010101011" =>    flag_op <= mc_nz;
        when "11101110100" =>    flag_op <= mc_nz;
        when "11111110100" =>    flag_op <= mc_nz;
        when "11100110011" =>    flag_op <= mc_nz;
        when "11110110011" =>    flag_op <= mc_nz;
        when "11101000000" =>    flag_op <= mc_nz;
        when "11001000000" =>    flag_op <= mc_nz;
        when "10101101100" =>    flag_op <= mc_nz;
        when "10111101100" =>    flag_op <= mc_nz;
        when "10111001100" =>    flag_op <= mc_nz;
        when "10101001001" =>    flag_op <= mc_nz;
        when "10100101011" =>    flag_op <= mc_nz;
        when "10100001110" =>    flag_op <= mc_nz;
        when "10110001110" =>    flag_op <= mc_nz;
        when "10110101011" =>    flag_op <= mc_nz;
        when "10101110100" =>    flag_op <= mc_nz;
        when "10111110100" =>    flag_op <= mc_nz;
        when "10100010001" =>    flag_op <= mc_nz;
        when "10100110011" =>    flag_op <= mc_nz;
        when "10110110011" =>    flag_op <= mc_nz;
        when "10101100100" =>    flag_op <= mc_nz;
        when "10111100100" =>    flag_op <= mc_nz;
        when "10100000001" =>    flag_op <= mc_nz;
        when "10100100011" =>    flag_op <= mc_nz;
        when "10110100011" =>    flag_op <= mc_nz;
        when "01001110100" =>    flag_op <= mc_nzc;
        when "01011110100" =>    flag_op <= mc_nzc;
        when "01001010000" =>    flag_op <= mc_nzc;
        when "01000110011" =>    flag_op <= mc_nzc;
        when "01010110011" =>    flag_op <= mc_nzc;
        when "00001101100" =>    flag_op <= mc_nz;
        when "00011101100" =>    flag_op <= mc_nz;
        when "00011001100" =>    flag_op <= mc_nz;
        when "00001001001" =>    flag_op <= mc_nz;
        when "00000001110" =>    flag_op <= mc_nz;
        when "00010001110" =>    flag_op <= mc_nz;
        when "00000101011" =>    flag_op <= mc_nz;
        when "00010101011" =>    flag_op <= mc_nz;
        when "01101000011" =>    flag_op <= mc_nz;
        when "00101000011" =>    flag_op <= mc_din;
        when "00000011000" =>    flag_op <= mc_seti;
        when "00101110100" =>    flag_op <= mc_nzc;
        when "00111110100" =>    flag_op <= mc_nzc;
        when "00101010000" =>    flag_op <= mc_nzc;
        when "00100110011" =>    flag_op <= mc_nzc;
        when "00110110011" =>    flag_op <= mc_nzc;
        when "01101110100" =>    flag_op <= mc_nzc;
        when "01111110100" =>    flag_op <= mc_nzc;
        when "01101010000" =>    flag_op <= mc_nzc;
        when "01100110011" =>    flag_op <= mc_nzc;
        when "01110110011" =>    flag_op <= mc_nzc;
        when "01000000011" =>    flag_op <= mc_din;
        when "11101101100" =>    flag_op <= mc_nvzc;
        when "11111101100" =>    flag_op <= mc_nvzc;
        when "11111001100" =>    flag_op <= mc_nvzc;
        when "11101001001" =>    flag_op <= mc_nvzc;
        when "11100001110" =>    flag_op <= mc_nvzc;
        when "11110001110" =>    flag_op <= mc_nvzc;
        when "11100101011" =>    flag_op <= mc_nvzc;
        when "11110101011" =>    flag_op <= mc_nvzc;
        when "00111000000" =>    flag_op <= mc_setc;
        when "11111000000" =>    flag_op <= mc_setd;
        when "01111000000" =>    flag_op <= mc_seti;
        when "01000011101" =>    flag_op <= mc_seti;
        when "00110011101" =>    flag_op <= mc_seti;
        when "10101010000" =>    flag_op <= mc_nz;
        when "10101000000" =>    flag_op <= mc_nz;
        when "10111010000" =>    flag_op <= mc_nz;
        when "10001010000" =>    flag_op <= mc_nz;
        when "10011000000" =>    flag_op <= mc_nz;
        when others =>    flag_op <= mc_nop;
    end case;
  end process;
end flag_op_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------

----------------------------------------------------------------------------
----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.microcode.all;

entity mc_rom is
    port (opcode     :in u_unsigned (7 downto 0);
          step       :in u_unsigned (2 downto 0);
          done       :out mct_done;
          addr_op    :out mct_addr_op;
          din_le     :out mct_din_le;
          rd_en      :out mct_rd_en;
          dout_op    :out mct_dout_op;
          dint1_op   :out mct_dint1_op;
          dint2_op   :out mct_dint2_op;
          dint3_op   :out mct_dint3_op;
          pc_op      :out mct_pc_op;
          sp_op      :out mct_sp_op;
          alu1       :out mct_alu1;
          alu2       :out mct_alu2;
          alu_op     :out mct_alu_op;
          a_le       :out mct_a_le;
          x_le       :out mct_x_le;
          y_le       :out mct_y_le;
          flag_op    :out mct_flag_op
         );
end mc_rom;

architecture mc_rom_arch of mc_rom is
  signal addr :u_unsigned (10 downto 0);
begin

  addr <= opcode & step;

  u00: entity work.done_rom port map (addr, done);
  u01: entity work.addr_op_rom port map (addr, addr_op);
  u02: entity work.din_le_rom port map (addr, din_le);
  u03: entity work.rd_en_rom port map (addr, rd_en);
  u04: entity work.dout_op_rom port map (addr, dout_op);
  u05: entity work.dint1_op_rom port map (addr, dint1_op);
  u06: entity work.dint2_op_rom port map (addr, dint2_op);
  u07: entity work.dint3_op_rom port map (addr, dint3_op);
  u08: entity work.pc_op_rom port map (addr, pc_op);
  u09: entity work.sp_op_rom port map (addr, sp_op);
  u10: entity work.alu1_rom port map (addr, alu1);
  u11: entity work.alu2_rom port map (addr, alu2);
  u12: entity work.alu_op_rom port map (addr, alu_op);
  u13: entity work.a_le_rom port map (addr, a_le);
  u14: entity work.x_le_rom port map (addr, x_le);
  u15: entity work.y_le_rom port map (addr, y_le);
  u16: entity work.flag_op_rom port map (addr, flag_op);

end mc_rom_arch;

----------------------------------------------------------------------------
----------------------------------------------------------------------------
--,Free-6502 Opcode Summary
--  
--,Neumonic,Addr Mode,Opcode,Original Clks,Free-6502 Clks,Flags
--,ADC,"ABS",6D,4,5,NVZC
--,ADC,"ABS,X",7D,4,5,NVZC
--,ADC,"ABS,Y",79,4,5,NVZC
--,ADC,"IMM",69,2,3,NVZC
--,ADC,"(IND,X)",61,6,7,NVZC
--,ADC,"(IND),Y",71,5,7,NVZC
--,ADC,"Z-PAGE",65,3,4,NVZC
--,ADC,"Z-PAGE,X",75,4,4,NVZC
--,AND,"ABS",2D,4,5,NZ
--,AND,"ABS,X",3D,4,5,NZ
--,AND,"ABS,Y",39,4,5,NZ
--,AND,"IMM",29,2,3,NZ
--,AND,"(IND,X)",21,6,7,NZ
--,AND,"(IND),Y",31,5,7,NZ
--,AND,"Z-PAGE",25,3,4,NZ
--,AND,"Z-PAGE,X",35,4,4,NZ
--,ASL,"ABS",0E,6,8,NZC
--,ASL,"ABS,X",1E,7,8,NZC
--,ASL,"ACC",0A,2,2,NZC
--,ASL,"Z-PAGE",06,5,7,NZC
--,ASL,"Z-PAGE,X",16,6,7,NZC
--,BCC,"REL",90,2,4,
--,BCS,"REL",B0,2,4,
--,BEQ,"REL",F0,2,4,
--,BIT,"ABS",2C,4,5,BIT
--,BIT,"Z-PAGE",24,3,4,BIT
--,BMI,"REL",30,2,4,
--,BNE,"REL",D0,2,4,
--,BPL,"REL",10,2,4,
--,BRK,"IMP",00,7,7,SETI
--,BVC,"REL",50,2,4,
--,BVS,"REL",70,2,4,
--,CLC,"IMP",18,2,2,CLEARC
--,CLD,"IMP",D8,2,2,CLEARD
--,CLI,"IMP",58,2,2,CLEARI
--,CLV,"IMP",B8,2,2,CLEARV
--,CMP,"ABS",CD,4,5,NZC
--,CMP,"ABS,X",DD,4,5,NZC
--,CMP,"ABS,Y",D9,4,5,NZC
--,CMP,"IMM",C9,2,3,NZC
--,CMP,"(IND,X)",C1,6,7,NZC
--,CMP,"(IND),Y",D1,5,7,NZC
--,CMP,"Z-PAGE",C5,3,4,NZC
--,CMP,"Z-PAGE,X",D5,4,4,NZC
--,CPX,"ABS",EC,4,5,NZC
--,CPX,"IMM",E0,2,3,NZC
--,CPX,"Z-PAGE",E4,3,4,NZC
--,CPY,"ABS",CC,4,5,NZC
--,CPY,"IMM",C0,2,3,NZC
--,CPY,"Z-PAGE",C4,3,4,NZC
--,DEC,"ABS",CE,6,8,NZ
--,DEC,"ABS,X",DE,7,8,NZ
--,DEC,"Z-PAGE",C6,5,7,NZ
--,DEC,"Z-PAGE,X",D6,6,7,NZ
--,DEX,"IMP",CA,2,2,NZ
--,DEY,"IMP",88,2,2,NZ
--,EOR,"ABS",4D,4,5,NZ
--,EOR,"ABS,X",5D,4,5,NZ
--,EOR,"ABS,Y",59,4,5,NZ
--,EOR,"IMM",49,2,3,NZ
--,EOR,"(IND,X)",41,6,7,NZ
--,EOR,"(IND),Y",51,5,7,NZ
--,EOR,"Z-PAGE",45,3,4,NZ
--,EOR,"Z-PAGE,X",55,4,4,NZ
--,INC,"ABS",EE,6,8,NZ
--,INC,"ABS,X",FE,7,8,NZ
--,INC,"Z-PAGE",E6,5,7,NZ
--,INC,"Z-PAGE,X",F6,6,7,NZ
--,INX,"IMP",E8,2,2,NZ
--,INY,"IMP",C8,2,2,NZ
--,JMP,"ABS",4C,3,5,
--,JMP,"(IND)",6C,5,8,
--,JSR,"ABS",20,6,5,
--,LDA,"ABS",AD,4,5,NZ
--,LDA,"ABS,X",BD,4,5,NZ
--,LDA,"ABS,Y",B9,4,5,NZ
--,LDA,"IMM",A9,2,3,NZ
--,LDA,"Z-PAGE",A5,4,4,NZ
--,LDA,"(IND,X)",A1,6,7,NZ
--,LDA,"(IND),Y",B1,5,7,NZ
--,LDA,"Z-PAGE,X",B5,4,4,NZ
--,LDX,"ABS",AE,4,5,NZ
--,LDX,"ABS,Y",BE,4,5,NZ
--,LDX,"IMM",A2,2,3,NZ
--,LDX,"Z-PAGE",A6,3,4,NZ
--,LDX,"Z-PAGE,Y",B6,4,4,NZ
--,LDY,"ABS",AC,4,5,NZ
--,LDY,"ABS,X",BC,4,5,NZ
--,LDY,"IMM",A0,2,3,NZ
--,LDY,"Z-PAGE",A4,3,4,NZ
--,LDY,"Z-PAGE,X",B4,4,4,NZ
--,LSR,"ABS",4E,6,8,NZC
--,LSR,"ABS,X",5E,7,8,NZC
--,LSR,"ACC",4A,2,2,NZC
--,LSR,"Z-PAGE",46,5,7,NZC
--,LSR,"Z-PAGE,X",56,6,7,NZC
--,NOP,"IMP",EA,2,2,
--,ORA,"ABS",0D,4,5,NZ
--,ORA,"ABS,X",1D,4,5,NZ
--,ORA,"ABS,Y",19,4,5,NZ
--,ORA,"IMM",09,2,3,NZ
--,ORA,"(IND,X)",01,6,7,NZ
--,ORA,"(IND),Y",11,5,7,NZ
--,ORA,"Z-PAGE",05,3,4,NZ
--,ORA,"Z-PAGE,X",15,4,4,NZ
--,PHA,"IMP",48,3,3,
--,PHP,"IMP",08,3,3,
--,PLA,"IMP",68,4,4,NZ
--,PLP,"IMP",28,4,4,DIN
--,RESET,"INTERNAL",03,0,6,SETI
--,ROL,"ABS",2E,6,8,NZC
--,ROL,"ABS,X",3E,7,8,NZC
--,ROL,"ACC",2A,2,2,NZC
--,ROL,"Z-PAGE",26,5,7,NZC
--,ROL,"Z-PAGE,X",36,6,7,NZC
--,ROR,"ABS",6E,6,8,NZC
--,ROR,"ABS,X",7E,7,8,NZC
--,ROR,"ACC",6A,2,2,NZC
--,ROR,"Z-PAGE",66,5,7,NZC
--,ROR,"Z-PAGE,X",76,6,7,NZC
--,RTI,"IMP",40,6,8,DIN
--,RTS,"IMP",60,6,8,
--,SBC,"ABS",ED,4,5,NVZC
--,SBC,"ABS,X",FD,4,5,NVZC
--,SBC,"ABS,Y",F9,4,5,NVZC
--,SBC,"IMM",E9,2,3,NVZC
--,SBC,"(IND,X)",E1,6,7,NVZC
--,SBC,"(IND),Y",F1,5,7,NVZC
--,SBC,"Z-PAGE",E5,3,4,NVZC
--,SBC,"Z-PAGE,X",F5,4,4,NVZC
--,SEC,"IMP",38,2,2,SETC
--,SED,"IMP",F8,2,2,SETD
--,SEI,"IMP",78,2,2,SETI
--,STA,"ABS",8D,4,5,
--,STA,"ABS,Y",99,5,5,
--,STA,"(IND,X)",81,6,7,
--,STA,"(IND),Y",91,6,7,
--,STA,"Z-PAGE",85,3,4,
--,STA,"Z-PAGE,X",95,4,4,
--,STA,"ABS,X",9D,5,5,
--,S_IRQ,"INTERNAL",43,0,8,SETI
--,S_NMI,"INTERNAL",33,0,8,SETI
--,STX,"ABS",8E,4,5,
--,STX,"Z-PAGE",86,3,4,
--,STX,"Z-PAGE,Y",96,4,4,
--,STY,"ABS",8C,4,5,
--,STY,"Z-PAGE",84,3,4,
--,STY,"Z-PAGE,X",94,4,4,
--,TAX,"IMP",AA,2,2,NZ
--,TAY,"IMP",A8,2,2,NZ
--,TSX,"IMP",BA,2,2,NZ
--,TXA,"IMP",8A,2,2,NZ
--,TXS,"IMP",9A,2,2,
--,TYA,"IMP",98,2,3,NZ
--
--
----------------------------------------------------------------------------
----------------------------------------------------------------------------
