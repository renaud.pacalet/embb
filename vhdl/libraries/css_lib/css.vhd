--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Control Sub-System

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;

use work.bitfield.all;
use work.css_pkg.all;

entity css is
  generic(
-- pragma translate_off
          debug:     boolean := true;       --* Print debug information
          verbose:   boolean := false;      --* Print more debug information
          name:      string  := "DSP Unit"; --* Name of surrounding DSP Unit
-- pragma translate_on
          n_pa_regs: natural range 0 to 15; --* Number of 64-bits parameters registers
          pa_rmask:  std_ulogic_vector;
          pa_wmask:  std_ulogic_vector;
          with_dma:  boolean := true;
          with_pss:   boolean := true;
          with_mss:  boolean := true;
          with_uc:   boolean := true;
          n0:        positive := 1;         --* Number of input pipeline registers in MSS, including input registers of RAMs
          n1:        positive := 1;         --* Number of output pipeline registers in MSS, including output registers of RAMs
          neirq:     natural range 0 to 32 := 0); --* Number of extended interrupt requests
  port(clk:      in  std_ulogic;
       srstn:    in  std_ulogic;
       ce:       in  std_ulogic;
       hirq:     in  std_ulogic;
       uirq:     out std_ulogic;      --* Output interrupt request from UC to host system
       pirq:     out std_ulogic;      --* Output interrupt request from PSS to host system
       dirq:     out std_ulogic;      --* Output interrupt request from DMA to host system
       eirq:     out std_ulogic_vector(neirq - 1 downto 0); --* Output interrupt requests to host system
       pss2css:  in  pss2css_type;
       css2pss:  out css2pss_type;
       param:    out std_ulogic_vector(n_pa_regs * 64 - 1 downto 0);
       mss2css:  in  mss2css_type;
       css2mss:  out css2mss_type;
       tvci_in:  in  vci_i2t_type;
       tvci_out: out vci_t2i_type;
       ivci_in:  in  vci_t2i_type;
       ivci_out: out vci_i2t_type);
  constant wdma: boolean := with_mss and with_dma; -- No DMA if no MSS
  constant wpss:  boolean := with_pss;
  constant wmss: boolean := with_mss;
  constant wuc:  boolean := with_mss and with_uc;  -- No UC if no MSS
  --* Depth of target AVCI requests FIFO
  constant req_fifo_depth: natural := 2;
  --* Depth of target AVCI responses FIFO. Must be deep enough to store responses of all acknowledged requests, that is: REQ_FIFO_DEPTH+N0+N1+2 (the +2
  --* corresponds to the output register of VIA in which access requests to MSS are latched and to the input register of VIA in which MSS read data are latched.
  constant rsp_fifo_depth: natural := req_fifo_depth + n0 + n1 + 2;

end entity css;

architecture rtl of css is

  signal ctrl2dma:   ctrl2dma_type;
  signal dma2ctrl:   dma2ctrl_type;
  signal dma2ctrl_w: dma2ctrl_type;
  signal dma2mss_w:  dma2mss_type;
  signal ivci_out_w: vci_i2t_type;
  signal uc2uca:     uc2uca_type;
  signal uca2uc:     uca2uc_type;
  signal ctrl2uca:   ctrl2uca_type;
  signal uca2ctrl:   uca2ctrl_type;
  signal uca2ctrl_w: uca2ctrl_type;
  signal ctrl2uc:    ctrl2uc_type;
  signal uc2ctrl:    uc2ctrl_type;
  signal uc2ctrl_w:  uc2ctrl_type;
  signal uc2mss_w:   uc2mss_type;
  signal ctrl2via:   ctrl2via_type;
  signal via2ctrl:   via2ctrl_type;
  signal req2via:    vci_request_type;
  signal via2req:    std_ulogic;
  signal rsp2via:    natural range 0 to rsp_fifo_depth;
  signal via2rsp:    vci_response_type;

begin

  req: entity work.requests_fifo(rtl)
    generic map(depth => req_fifo_depth)
    port map(clk     => clk,
             srstn   => srstn,
             ce      => ce,
             req_in  => tvci_in.req,
             req_out => req2via,
             ack     => via2req,
             cmdack  => tvci_out.cmdack);

  rsp: entity work.responses_fifo(rtl)
    generic map(depth => rsp_fifo_depth)
    port map(clk     => clk,
             srstn   => srstn,
             ce      => ce,
             rsp_in  => via2rsp,
             rsp_out => tvci_out.rsp,
             rspack  => tvci_in.rspack,
             ack     => rsp2via);

  via: entity work.via(rtl)
    generic map(rsp_depth => rsp_fifo_depth,
                n0        => n0,
                n1        => n1,
                with_mss  => wmss)
    port map(clk      => clk,
             srstn    => srstn,
             ce       => ce,
             ctrl2via => ctrl2via,
             via2ctrl => via2ctrl,
             req2via  => req2via,
             via2req  => via2req,
             rsp2via  => rsp2via,
             via2rsp  => via2rsp,
             mss2via  => mss2css.mss2vci,
             via2mss  => css2mss.vci2mss);

  ctrl: entity work.ctrl(rtl)
    generic map(
-- pragma translate_off
                debug     => debug,
                verbose   => verbose,
                name      => name,
-- pragma translate_on
                hst_level_interrupts => false,
                uc_level_interrupts  => false,
                n_pa_regs => n_pa_regs,
                pa_rmask  => pa_rmask,
                pa_wmask  => pa_wmask,
                with_dma  => wdma,
                with_pss  => wpss,
                with_uc   => wuc,
                neirq     => neirq)
    port map(clk      => clk,
             srstn    => srstn,
             ce       => ce,
             via2ctrl => via2ctrl,
             ctrl2via => ctrl2via,
             hirq     => hirq,
             uirq     => uirq,
             pirq     => pirq,
             dirq     => dirq,
             eirq     => eirq,
             dma2ctrl => dma2ctrl,
             ctrl2dma => ctrl2dma,
             uca2ctrl => uca2ctrl,
             ctrl2uca => ctrl2uca,
             uc2ctrl  => uc2ctrl,
             ctrl2uc  => ctrl2uc,
             pss2ctrl => pss2css,
             ctrl2pss => css2pss,
             param    => param);

    g_dma: if wdma generate
      dma: entity work.dma(rtl)
      generic map(n0 => n0,
                  n1 => n1)
      port map(clk      => clk,
               ctrl2dma => ctrl2dma,
               dma2ctrl => dma2ctrl_w,
               mss2dma  => mss2css.mss2dma,
               dma2mss  => dma2mss_w,
               vci_in   => ivci_in,
               vci_out  => ivci_out_w);
    end generate g_dma;
    dma2ctrl <= dma2ctrl_w when wdma else
                dma2ctrl_none;
    css2mss.dma2mss <= dma2mss_w when wdma else
                       dma2mss_none;
    ivci_out <= ivci_out_w when wdma else
                vci_i2t_none;

    g_uc: if wuc generate
      uca: entity work.uca(rtl)
      generic map(n0 => n0,
                  n1 => n1)
      port map(clk      => clk,
               srstn    => srstn,
               ce       => ce,
               ctrl2uca => ctrl2uca,
               uca2ctrl => uca2ctrl_w,
               uc2uca   => uc2uca,
               uca2uc   => uca2uc,
               mss2uca  => mss2css.mss2uc,
               uca2mss  => uc2mss_w);

      uc: entity work.uc(rtl)
-- pragma translate_off
      generic map(debug => debug)
-- pragma translate_on
      port map(clk      => clk,
               srstn    => srstn,
               ctrl2uc  => ctrl2uc,
               uc2ctrl  => uc2ctrl_w,
               uca2uc   => uca2uc,
               uc2uca   => uc2uca);
    end generate g_uc;
    uca2ctrl <= uca2ctrl_w when wuc else
                uca2ctrl_none;
    css2mss.uc2mss <= uc2mss_w when wuc else
                      uc2mss_none;
    uc2ctrl <= uc2ctrl_w when wuc else
               uc2ctrl_none;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
