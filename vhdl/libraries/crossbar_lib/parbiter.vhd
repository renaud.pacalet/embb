--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Crossbar arbiter for VCI initiators
--*
--* This module implements response arbiter logic. Priority is almost fixed
--* (based on index in array of input responses, 0 has highest priority, n-1
--* the lowest). A rolling token alters this by raising to highest the priority
--* of the target holding the token. The token changes after each response
--* completion (end of VCI packet). The module also duplicates the initiator's
--* requests to send a copy to each connected target. At most one of the copies
--* has the cmdval flag set: the one sent to the target which address range
--* match the request's address. IRQ is raised when a request is issued at an
--* unmapped address. Byte address a match target with base address ba and
--* address mask msk iff:
--*
--*   a(vci_n - 1 downto vci_n - xtag_len) and msk = ba

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

use work.crossbar_pkg.all;

entity parbiter is
  generic(
    tmaps: tmapv;  --* list of all target maps (id, base address and mask)
    tlist: natv;   --* list of IDs of connected targets, sorted from highest (targets(0)) to lowest priority
    id:    natural --* initiator's id
  );
  port (
    clk:   in  std_ulogic;                       --* clock
    srstn: in  std_ulogic;                       --* synchronous reset
    x2ps:  in  vci_t2i_vector(0 to tlist'length - 1); --* inputs from targets (through crossbar)
    p2i:   out vci_t2i_type;                        --* output to initiator
    i2p:   in  vci_i2t_type;                        --* input from initiator
    p2xs:  out vci_i2t_vector(0 to tlist'length - 1); --* Request to targets
    irq:   out std_ulogic                        --* raised upon unmapped address in a request issued by initiator
  );
  constant n: positive := tlist'length;                             --* Number of connected targets
  constant vid: vci_srcid_type := vci_srcid_type(to_unsigned(id, vci_s)); --* Vector version of initiator's ID
end entity parbiter;

architecture rtl of parbiter is

  signal tok, next_tok: std_ulogic_vector(0 to n - 1); --* token
  signal irql: std_ulogic;                             --* local copy of irq output

begin

  main: process(x2ps, tok, i2p)
    variable x2pv:    vci_t2i_type;
    variable sels:    std_ulogic_vector(0 to n - 1);
    variable cmdack:  std_ulogic_vector(0 to n - 1);
    variable add:     std_ulogic_vector(xtag_len - 1 downto 0);
    variable ba:      xtag;    -- target base address tag
    variable msk:     xtag;    -- target mask tag
    variable tmp:     std_ulogic;
  begin
    -- Default outputs
    p2i <= vci_t2i_none;
    p2xs <= (others => vci_i2t_none);

    -- Arbitrate between incoming responses
    -- Select highest priority input response, if any
    pn(src => x2ps, it  => tok, dst => x2pv, ot  => tmp, sel => sels);
    -- next value of token: if the selected input is requesting and is
    -- acknowledged by initiator and it is an end of packet, pass token to next
    -- target
    if (x2pv.rsp.rspval and i2p.rspack and x2pv.rsp.reop) = '1' and n > 1 then
      next_tok <= tok(1 to n-1) & tok(0);
    else
      next_tok <= tok;
    end if;
    -- transmit selected response (x2pv) to initiator; note: cmdack is not what
    -- it should be and still needs to be computed
    p2i <= x2pv;
    p2i.cmdack <= '0';

    -- Outgoing requests
    -- Address tag of request
    add := i2p.req.address(vci_n - 1 downto vci_n - xtag_len);
    -- by default assume address in initiator's request (if any) match none of
    -- targets addresses ranges; so, raise interrupt if initiator is requesting
    irql <= i2p.req.cmdval;
    -- Intermediate variable. All cmdacks from targets are blocked ('0') but the
    -- one from the requested target. This variable could be avoided but the
    -- synthesizer has no way to know that at most one target is requested; as a
    -- consequence the following for loop would infer a slow linear mux to
    -- compute the actual p2i.cmdack. Thanks to this trick, the faster or_reduce
    -- logarithmic or will be used instead.
    cmdack := (others => '0');
    for i in 0 to n - 1 loop -- for all targets
      ba  := tmaps(tlist(i)).ba;  -- target's base address tag
      msk := tmaps(tlist(i)).msk; -- target's mask tag
      -- if requesting and request address MSBs match target's address range
      if ((add and msk) = ba) and (i2p.req.cmdval = '1') then
        p2xs(i) <= i2p; -- transmit request to target
        p2xs(i).req.srcid <= vid; -- force source id
        irql <= '0';    -- clear irql flag
        cmdack(i) := x2ps(i).cmdack; -- copy cmdack of requested target
      end if;
    end loop;
    p2i.cmdack <= or_reduce(cmdack); -- cmdack to initiator is now OK

    -- Transmit rspack to target which response was selected
    for i in 0 to n - 1 loop -- for all targets
      p2xs(i).rspack <= i2p.rspack and sels(i);
    end loop;
  end process main;

  --* Raise interrupt if initiator submits a request but address match none of
  --* targets address ranges
  irq <= irql;

  --* Token
  token: process(clk)
  begin
    if clk'event and clk = '1' then
      if srstn = '0' then -- initialize token: pass token to initiator 0
        tok <= (others => '0');
        tok(0) <= '1';
      else
        tok <= next_tok;
      end if;
    end if;
  end process token;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
