--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Crossbar arbiter for VCI targets
--*
--*  This module implements request arbiter logic. Priority is almost
--* fixed (based on index in array of input requests, 0 has highest priority, n-1
--* the lowest). A rolling token alters this by raising to highest the priority
--* of the initiator holding the token. The token changes after each request
--* completion (end of VCI packet). The module also duplicates the target
--* responses to send a copy to each connected initiator. At most one of the
--* copies has the rspval flag set: the one sent to the initiator which id match
--* the rsrcid field. IRQ is raised when a response carries a rsrcid field that
--* match none of the ids of the connected initiators.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

use work.crossbar_pkg.all;

entity qarbiter is
  generic(
    ilist: natv; --* list of ids of connected initiator, sorted from highest (initiators(0)) to lowest priority
    msk:   xtag  --* mask used when comparing addresses MSBs for addresses decoding
  );
  port(
    clk:    in  std_ulogic;                       --* clock
    srstn:  in  std_ulogic;                       --* synchronous reset
    x2qs:   in  vci_i2t_vector(0 to ilist'length - 1); --* inputs from initiators (through crossbar)
    q2t:    out vci_i2t_type;                        --* output to target
    t2q:    in  vci_t2i_type;                        --* input from target
    q2xs:   out vci_t2i_vector(0 to ilist'length - 1); --* Response request to initiators
    irq:    out std_ulogic                        --* raised upon out of range rsrcid in a response issued by target
  );
  constant n: positive := ilist'length;
end entity qarbiter;

architecture rtl of qarbiter is

  signal tok, next_tok: std_ulogic_vector(0 to n - 1); --* token

begin

  --pragma translate_off
  checker: process
  begin
    assert ilist'length = n and ilist'left = 0 and ilist'right = n-1
      report "Invalid generic parameters"
      severity failure;
    wait;
  end process checker;
  --pragma translate_on
    
  main: process(x2qs, tok, t2q)
    variable x2qv:    vci_i2t_type;
    variable sels:    std_ulogic_vector(0 to n - 1);
    variable rspack:  std_ulogic_vector(0 to n - 1);
    variable rsrcid:  natural range 0 to 2**vci_s - 1;
    variable miss:    std_ulogic;
    variable tmp:     std_ulogic;
  begin
    -- Default outputs
    q2t <= vci_i2t_none;
    q2xs <= (others => vci_t2i_none);

    -- Arbitrate between incoming requests
    -- select highest priority input request, if any
    qn(src => x2qs, it  => tok, dst => x2qv, ot  => tmp, sel => sels);
    -- erase base address
    x2qv.req.address(vci_n - 1 downto vci_n - xtag_len) :=
      x2qv.req.address(vci_n - 1 downto vci_n - xtag_len) and (not msk);
    -- next value of token: if the selected input is requesting and is
    -- acknowledged by target and it is an end of packet, pass token to next
    -- initiator
    if (x2qv.req.cmdval and t2q.cmdack and x2qv.req.eop) = '1' and n > 1 then
      next_tok <= tok(1 to n-1) & tok(0);
    else
      next_tok <= tok;
    end if;
    -- transmit selected request (x2qv) to target; note: rspack is not what it
    -- should be and still needs to be computed
    q2t <= x2qv;
    q2t.rspack <= '0';

    -- Outgoing responses
    -- by default assume rscrid in target response (if any) match none of
    -- initiators ids
    miss := t2q.rsp.rspval;
    -- Intermediate variable. All rspacks from initiators are blocked ('0') but
    -- the one we send a response. This variable could be avoided but the
    -- synthesizer has no way to know that at most one initiator is sent a
    -- response; as a consequence the following for loop would infer a slow
    -- linear mux to compute the actual q2t.rspack. Thanks to this trick, the
    -- faster or_reduce logarithmic or will be used instead.
    rspack := (others => '0');
    for i in 0 to n - 1 loop -- for all initiators
      -- if responding and rsrcid match initiator's id
      if t2q.rsp.rspval = '1' then
        -- Response's source id
        rsrcid := to_integer(u_unsigned(t2q.rsp.rsrcid));
        if (ilist(i) = rsrcid) and (t2q.rsp.rspval = '1') then
          q2xs(i) <= t2q; -- transmit response to initiator
          miss := '0';    -- clear miss flag
          rspack(i) := x2qs(i).rspack; -- copy rspack form responded initiator
        end if;
      end if;
    end loop;
    q2t.rspack <= or_reduce(rspack); -- rspack to target is now OK

    -- Transmit cmdack to initiator which request was selected
    for i in 0 to n - 1 loop -- for all initiators
      q2xs(i).cmdack <= t2q.cmdack and sels(i);
    end loop;

    -- Raise interrupt if target submits a response but rsrcid match none of
    -- initiators id
    irq <= miss;
  end process main;

  --* Token
  token: process(clk)
  begin
    if clk'event and clk = '1' then
      if srstn = '0' then -- initialize token: pass token to initiator 0
        tok <= (others => '0');
        tok(0) <= '1';
      else
        tok <= next_tok;
      end if;
    end if;
  end process token;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
