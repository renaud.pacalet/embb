--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Crossbar utility package
--*
--*   Defines types for address tags and masks that are used in addresses
--* decoding. Defines QN and PN procedures, two building blocks for the qarbiter
--* (arbiter between requests at input of targets) and parbiter (arbiter of
--* responses at input of initiators).

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

package crossbar_pkg is

  --* Number of addresses MSBs used when decoding VCI requests addresses
  constant xtag_len: positive := 12;
  --* Type of base addresses and addresses masks
  subtype xtag is std_ulogic_vector(xtag_len - 1 downto 0);

  --* target map: identifier, base address and address mask
  type tmap is record
    id: natural;
    ba, msk: xtag;
  end record;
  --* vectors of target maps
  type tmapv is array(natural range <>) of tmap;

  --* vectors of identifiers
  type natv is array(natural range <>) of natural;

  --* @brief Arbitration between requests.
  --* Arbitrate between incoming requests (SRC, a N-requests array) based on a
  --* fixed priority scheme: the request with the lowest index (in array SRC) has
  --* the highest priority. This mechanism is slightly modified to avoid the
  --* starvation of low priority requesters: a rolling token (IT, a N-bits
  --* vector) temporarily raises the priority of the corresponding requester to
  --* the highest level. Each time a packet transfer completes, the token is
  --* passed to the next requester. DST is a copy of the selected incoming
  --* request. OT indicates if the selected request was that of the requester
  --* that has the token (useful for building large arbiters from smaller ones).
  --* SEL is a N-bits vector one-hot encoding the selected request if any.
  procedure qn(src: in  vci_i2t_vector; -- inputs
               -- input token (one-hot encoding)
               it:  in  std_ulogic_vector;
               dst: out vci_i2t_type; -- copy of selected input
               -- output token (that of the selected input)
               ot:  out std_ulogic;
               -- selection flags
               sel: out std_ulogic_vector);

  --* @brief Arbitration between responses. Similar to qn.
  procedure pn(src: in  vci_t2i_vector; -- inputs
               -- input token (one-hot encoding)
               it:  in  std_ulogic_vector;
               dst: out vci_t2i_type; -- copy of selected input
               -- output token (that of the selected input)
               ot:  out std_ulogic;
               -- selection flags
               sel: out std_ulogic_vector);


end package crossbar_pkg;

package body crossbar_pkg is

  procedure qn(src: in  vci_i2t_vector; -- inputs
               -- input token (one-hot encoding)
               it:  in  std_ulogic_vector;
               dst: out vci_i2t_type; -- copy of selected input
               -- output token (that of the selected input)
               ot:  out std_ulogic;
               -- selection flags
                sel: out std_ulogic_vector) is

    constant n:     positive := src'length;
    variable srcv:  vci_i2t_vector(0 to n - 1) := src;
    variable itv:   std_ulogic_vector(0 to n - 1) := it;
    variable dstn:  vci_i2t_vector(0 to 1);
    variable otn:   std_ulogic_vector(0 to 1);
    variable seln:  std_ulogic_vector(0 to n - 1);
    variable seln2: std_ulogic_vector(0 to 1);

  begin
    -- pragma translate_off
    assert it'length = n and sel'length = n
      report "qn: invalid input-output parameter lengths; src, it and sel must have same length"
      severity failure;
    -- pragma translate_on
    dst.req := vci_request_none; -- no request by default
    ot      := '0';
    seln    := (others => '0');
    if n = 1 then
      if srcv(0).req.cmdval = '1' then
        dst     := srcv(0); -- route input #0
        ot      := itv(0);
        seln    := "1";     -- select #0
      end if;
    elsif n = 2 then -- if two inputs (#0 and #1)
      -- if #1 requests and (#1 has token or #0 does not request)
      if srcv(1).req.cmdval = '1' and (itv(1) = '1' or srcv(0).req.cmdval = '0') then
        dst    := srcv(1); -- route input #1
        ot     := itv(1);
        seln   := "01";    -- select #1
      -- else, if #0 requests
      elsif srcv(0).req.cmdval = '1' then
        dst     := srcv(0); -- route input #0
        ot      := itv(0);
        seln    := "10";    -- select #0
      end if;
    elsif n > 2 then -- more than two inputs
      qn(src => srcv(0 to n / 2 - 1), -- arbitrate first half
         it  => itv(0 to n / 2 - 1),
         dst => dstn(0),
         ot  => otn(0),
         sel => seln(0 to n / 2 - 1));
      qn(src => srcv(n / 2 to n - 1), -- arbitrate second half
         it  => itv(n / 2 to n - 1),
         dst => dstn(1),
         ot  => otn(1),
         sel => seln(n / 2 to n - 1));
      qn(src => dstn, -- arbitrate between the two halves
         it  => otn,
         dst => dst,
         ot  => ot,
         sel => seln2);
      -- compute selection flags
         seln := band(seln(0 to n / 2 - 1), seln2(0)) & band(seln(n / 2 to n - 1), seln2(1));
    end if;
    sel := seln;
  end procedure qn;

  procedure pn(src: in  vci_t2i_vector; -- inputs
               -- input token (one-hot encoding)
               it:  in  std_ulogic_vector;
               dst: out vci_t2i_type; -- copy of selected input
               -- output token (that of the selected input)
               ot:  out std_ulogic;
               -- selection flags
                sel: out std_ulogic_vector) is

    constant n:     positive := src'length;
    variable srcv:  vci_t2i_vector(0 to n - 1) := src;
    variable itv:   std_ulogic_vector(0 to n - 1) := it;
    variable dstn:  vci_t2i_vector(0 to 1);
    variable otn:   std_ulogic_vector(0 to 1);
    variable seln:  std_ulogic_vector(0 to n - 1);
    variable seln2: std_ulogic_vector(0 to 1);

  begin
    -- pragma translate_off
    assert it'length = n and sel'length = n
      report "pn: invalid input-output parameter lengths; src, it and sel must have same length"
      severity failure;
    -- pragma translate_on
    dst     := vci_t2i_none; -- no response by default
    ot      := '0';
    seln    := (others => '0');
    if n = 1 then
      if srcv(0).rsp.rspval = '1' then
        dst    := srcv(0); -- route input #0
        ot     := itv(0);
        seln   := "1";    -- select #0
      end if;
    elsif n = 2 then -- if two inputs (#0 and #1)
      -- if #1 requests and (#1 has token or #0 does not request)
      if srcv(1).rsp.rspval = '1' and (itv(1) = '1' or srcv(0).rsp.rspval = '0') then
        dst    := srcv(1); -- route input #1
        ot     := itv(1);
        seln   := "01";    -- select #1
      -- else if #0 requests
      elsif srcv(0).rsp.rspval = '1' then
        dst    := srcv(0); -- route input #0
        ot     := itv(0);
        seln   := "10";    -- select #0
      end if;
    elsif n > 2 then -- more than two inputs
      pn(src => srcv(0 to n / 2 - 1), -- arbitrate first half
         it  => itv(0 to n / 2 - 1),
         dst => dstn(0),
         ot  => otn(0),
         sel => seln(0 to n / 2 - 1));
      pn(src => srcv(n / 2 to n - 1), -- arbitrate second half
         it  => itv(n / 2 to n - 1),
         dst => dstn(1),
         ot  => otn(1),
         sel => seln(n / 2 to n - 1));
      pn(src => dstn, -- arbitrate between the two halves
         it  => otn,
         dst => dst,
         ot  => ot,
         sel => seln2);
      -- update selection flags
      seln := band(seln(0 to n / 2 - 1), seln2(0)) & band(seln(n / 2 to n - 1), seln2(1));
    end if;
    sel := seln;
  end procedure pn;

end package body crossbar_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
