--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Top level of parbiter for synthesis tests

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;

use global_lib.global.all;
use global_lib.utils.all;
use global_lib.sim_utils.all;

use work.crossbar_pkg.all;

entity parbiter_top is
  generic(id: natural := 0);
  port (
    clk:    in  std_ulogic;
    srstn:  in  std_ulogic;

    x2ps:   in  vci_t2i_vector(0 to 9);
    p2i:    out vci_t2i_type;
    i2p:    in  vci_i2t_type;

    p2xs:   out vci_i2t_vector(0 to 9);

    irq:    out std_ulogic
  );
end entity parbiter_top;

architecture rtl of parbiter_top is

  constant tmaps : tmapv(0 to 9) := (
    0 => (id => 0, ba => X"000", msk => X"FFF"),
    1 => (id => 1, ba => X"001", msk => X"FFF"),
    2 => (id => 2, ba => X"002", msk => X"FFF"),
    3 => (id => 3, ba => X"003", msk => X"FFF"),
    4 => (id => 4, ba => X"004", msk => X"FFF"),
    5 => (id => 5, ba => X"005", msk => X"FFF"),
    6 => (id => 6, ba => X"006", msk => X"FFF"),
    7 => (id => 7, ba => X"007", msk => X"FFF"),
    8 => (id => 8, ba => X"008", msk => X"FFF"),
    9 => (id => 9, ba => X"009", msk => X"FFF"));

  constant tlist : natv(0 to 9) := (
    0 => 0,
    1 => 1,
    2 => 2,
    3 => 3,
    4 => 4,
    5 => 5,
    6 => 6,
    7 => 7,
    8 => 8,
    9 => 9);

begin

  iparbiter: entity work.parbiter
    generic map(tmaps => tmaps,
                tlist => tlist,
                id => id)
    port map(clk    => clk,
             srstn  => srstn,
             x2ps   => x2ps,
             p2i    => p2i,
             i2p    => i2p,
             p2xs   => p2xs,
             irq    => irq
           );

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
