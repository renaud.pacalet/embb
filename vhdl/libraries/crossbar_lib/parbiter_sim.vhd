--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for crossbar arbiter for VCI initiators

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;

use global_lib.global.all;
use global_lib.utils.all;
use global_lib.sim_utils.all;

use work.crossbar_pkg.all;

entity parbiter_sim is
  generic(id: natural := 0;
          n: natural := 10;
          n_tests: positive := 10000);
  constant vid: vci_srcid_type := vci_srcid_type(to_unsigned(id, vci_s));
end entity parbiter_sim;

architecture sim of parbiter_sim is
  signal clk:     std_ulogic;
  signal srstn:   std_ulogic;
  signal x2ps:    vci_t2i_vector(0 to n - 1);
  signal p2i:     vci_t2i_type;
  signal i2p:     vci_i2t_type;
  signal p2xs:    vci_i2t_vector(0 to n - 1);
  signal irq:     std_ulogic;
  signal r_p2i:   vci_t2i_type;
  signal r_p2xs:  vci_i2t_vector(0 to n - 1);
  signal r_irq:   std_ulogic;
  signal tok:     natural range 0 to n - 1;
  signal eos:     boolean := false;

  constant tmaps : tmapv(0 to n - 1) := (
    0 => (id => 0, ba => X"000", msk => X"FFF"),
    1 => (id => 1, ba => X"001", msk => X"FFF"),
    2 => (id => 2, ba => X"002", msk => X"FFF"),
    3 => (id => 3, ba => X"003", msk => X"FFF"),
    4 => (id => 4, ba => X"004", msk => X"FFF"),
    5 => (id => 5, ba => X"005", msk => X"FFF"),
    6 => (id => 6, ba => X"006", msk => X"FFF"),
    7 => (id => 7, ba => X"007", msk => X"FFF"),
    8 => (id => 8, ba => X"008", msk => X"FFF"),
    9 => (id => 9, ba => X"009", msk => X"FFF"));

  constant tlist : natv(0 to n - 1) := (0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

begin

  iparbiter: entity work.parbiter
    generic map(tmaps => tmaps,
                tlist => tlist,
                id => id)
    port map(clk    => clk,
             srstn  => srstn,
             x2ps   => x2ps,
             p2i    => p2i,
             i2p    => i2p,
             p2xs   => p2xs,
             irq    => irq
           );

  process
    variable l: line;
  begin
    clk <= '0';
    wait for 1 ns;
    clk <= '1';
    wait for 1 ns;
    if eos then
      write(l, string'("End of simulation, regression test PASSED"));
      writeline(output, l);
      wait;
    end if;
  end process;

  main: process
    variable l: line;
    variable tid: natural;
    variable ba: xtag;
  begin
    srstn <= '0';
    tok <= 0;
    for i in 0 to n - 1 loop
      x2ps(i) <= vci_t2i_none;
    end loop;
    i2p <= vci_i2t_none;
    for j in 1 to 9 loop
      wait until rising_edge(clk);
    end loop;
    srstn <= '1';
    for j in 1 to n_tests loop
      assert p2i = r_p2i
        report "Error: p2i mismatch"
        severity failure;
      assert p2xs = r_p2xs
        report "Error: p2x mismatch"
        severity failure;
      assert irq = r_irq
        report "Error: irq mismatch"
        severity failure;
      for i in 0 to n - 1 loop
        x2ps(i) <= vci_t2i_rnd;
      end loop;
      i2p <= vci_i2t_rnd;
      tid := int_rnd(0, n - 1);
      ba  := tmaps(tlist(tid)).ba;
      if boolean_rnd then
        i2p.req.address(vci_n - 1 downto vci_n - xtag_len) <= ba;
      end if;
      if (i2p.rspack and r_p2i.rsp.rspval and r_p2i.rsp.reop) = '1' then
        tok <= (tok - 1) mod n;
      end if;
      if j /= 0 and j mod (n_tests / 100) = 0 then
        write(l, (j * 100) / n_tests);
        write(l, string'("%"));
        writeline(output, l);
      end if;
      wait until rising_edge(clk);
    end loop;
    eos <= true;
    wait;
  end process main;

  ref: process(x2ps, i2p, tok)
    variable in_rsp: integer range -1 to n - 1;
    variable out_req: integer range -1 to n - 1;
    variable cmdack: std_ulogic;
    variable add, ba, msk: xtag;
  begin
    r_p2i <= vci_t2i_none;
    r_p2xs <= (others => vci_i2t_none);

    add := i2p.req.address(vci_n - 1 downto vci_n - xtag_len);
    out_req := -1;
    cmdack := '0';
    r_irq <= i2p.req.cmdval;
    for i in 0 to n - 1 loop
      ba := tmaps(tlist(i)).ba;
      msk := tmaps(tlist(i)).msk;
      if (add and msk) = ba and i2p.req.cmdval = '1' then
        cmdack := x2ps(i).cmdack;
        out_req := i;
      end if;
    end loop;
    if out_req /= -1 then
      r_p2xs(out_req) <= i2p;
      r_p2xs(out_req).req.srcid <= vid;
      r_p2xs(out_req).rspack <= '0';
      r_irq <= '0';
    end if;

    in_rsp := -1;
    for i in 0 to n - 1 loop
      if (in_rsp = -1 or tok = i) and x2ps(i).rsp.rspval = '1' then
        in_rsp := i;
      end if;
    end loop;
    if in_rsp /= -1 then
      r_p2i <= x2ps(in_rsp);
      r_p2i.cmdack <= '0';
    end if;

    r_p2i.cmdack <= cmdack;
    if in_rsp /= -1 then
      r_p2xs(in_rsp).rspack <= i2p.rspack;
    end if;
  end process ref;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
