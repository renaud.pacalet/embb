--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Top level of qarbiter for synthesis tests

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

use work.crossbar_pkg.all;

entity qarbiter_top is
  generic(msk: xtag := (others => '1'));
  port (
    clk:    in  std_ulogic;
    srstn:  in  std_ulogic;

    x2qs:   in  vci_i2t_vector(0 to 9);
    q2t:    out vci_i2t_type;
    t2q:    in  vci_t2i_type;

    q2xs:   out vci_t2i_vector(0 to 9);

    irq:    out std_ulogic
  );
end entity qarbiter_top;

architecture rtl of qarbiter_top is

  constant ilist : natv(0 to 9) := (
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

begin

  iqarbiter: entity work.qarbiter
    generic map(ilist => ilist,
                msk => msk)
    port map(clk    => clk,
             srstn  => srstn,
             x2qs   => x2qs,
             q2t    => q2t,
             t2q    => t2q,
             q2xs   => q2xs,
             irq    => irq
           );

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
