--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for crossbar arbiter for VCI targets

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;

use global_lib.global.all;
use global_lib.utils.all;
use global_lib.sim_utils.all;

use work.crossbar_pkg.all;

entity qarbiter_sim is
  generic(n: natural := 10;
          msk: xtag := (others => '1');
          n_tests: positive := 10000);
end entity qarbiter_sim;

architecture sim of qarbiter_sim is
  signal clk:     std_ulogic;
  signal srstn:   std_ulogic;
  signal x2qs:    vci_i2t_vector(0 to n - 1);
  signal q2t:     vci_i2t_type;
  signal t2q:     vci_t2i_type;
  signal q2xs:    vci_t2i_vector(0 to n - 1);
  signal irq:     std_ulogic;
  signal r_q2t:   vci_i2t_type;
  signal r_q2xs:  vci_t2i_vector(0 to n - 1);
  signal r_irq:   std_ulogic;
  signal tok:     natural range 0 to n - 1;
  signal eos:     boolean := false;

  constant ilist : natv(0 to n - 1) := (
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

begin

  iqarbiter: entity work.qarbiter
    generic map(ilist => ilist,
                msk => msk)
    port map(clk    => clk,
             srstn  => srstn,
             x2qs   => x2qs,
             q2t    => q2t,
             t2q    => t2q,
             q2xs   => q2xs,
             irq    => irq
           );

  process
    variable l: line;
  begin
    clk <= '0';
    wait for 1 ns;
    clk <= '1';
    wait for 1 ns;
    if eos then
      write(l, string'("End of simulation, regression test PASSED"));
      writeline(output, l);
      wait;
    end if;
  end process;

  main: process
    variable l: line;
  begin
    srstn <= '0';
    tok <= 0;
    for i in 0 to n - 1 loop
      x2qs(i) <= vci_i2t_none;
    end loop;
    t2q <= vci_t2i_none;
    for j in 1 to 9 loop
      wait until rising_edge(clk);
    end loop;
    srstn <= '1';
    for j in 1 to n_tests loop
      assert q2t = r_q2t
        report "Error: q2t mismatch"
        severity failure;
      assert q2xs = r_q2xs
        report "Error: q2x mismatch"
        severity failure;
      assert irq = r_irq
        report "Error: irq mismatch"
        severity failure;
      for i in 0 to n - 1 loop
        x2qs(i) <= vci_i2t_rnd;
      end loop;
      t2q <= vci_t2i_rnd;
      t2q.rsp.rsrcid <= std_ulogic_vector(to_unsigned(int_rnd(0, n), vci_s));
      if (t2q.cmdack and r_q2t.req.cmdval and r_q2t.req.eop) = '1' then
        tok <= (tok - 1) mod n;
      end if;
      if j /= 0 and j mod (n_tests / 100) = 0 then
        write(l, (j * 100) / n_tests);
        write(l, string'("%"));
        writeline(output, l);
      end if;
      wait until rising_edge(clk);
    end loop;
    eos <= true;
    wait;
  end process main;

  ref: process(x2qs, t2q, tok)
    variable in_req: integer range -1 to n - 1;
    variable out_rsp: integer range -1 to n - 1;
    variable rspack: std_ulogic;
    variable rsrcid: natural range 0 to 2**vci_s - 1;
  begin
    r_q2t <= vci_i2t_none;
    r_q2xs <= (others => vci_t2i_none);

    rsrcid := to_integer(u_unsigned(t2q.rsp.rsrcid));
    out_rsp := -1;
    rspack := '0';
    r_irq <= t2q.rsp.rspval;
    for i in 0 to n - 1 loop
      if t2q.rsp.rspval = '1' and rsrcid = ilist(i) then
        rspack := x2qs(i).rspack;
        out_rsp := i;
      end if;
    end loop;
    if out_rsp /= -1 then
      r_q2xs(out_rsp) <= t2q;
      r_q2xs(out_rsp).cmdack <= '0';
      r_irq <= '0';
    end if;

    in_req := -1;
    for i in 0 to n - 1 loop
      if (in_req = -1 or tok = i) and x2qs(i).req.cmdval = '1' then
        in_req := i;
      end if;
    end loop;
    if in_req /= -1 then
      r_q2t <= x2qs(in_req);
      r_q2t.req.address(vci_n - 1 downto vci_n - xtag_len) <=
        x2qs(in_req).req.address(vci_n - 1 downto vci_n - xtag_len) and (not msk);
      r_q2t.rspack <= '0';
    end if;

    r_q2t.rspack <= rspack;
    if in_req /= -1 then
      r_q2xs(in_req).cmdack <= t2q.cmdack;
    end if;
  end process ref;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
