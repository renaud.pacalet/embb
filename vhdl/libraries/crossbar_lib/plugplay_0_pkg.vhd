--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.crossbar_pkg.all;

package plugplay_0_pkg is
  
  constant INS_MAX : natural := 256;--256;  --* Maximum device instances
  constant IRQ_MAX : natural := 256;--65536;  --* Maximum interruption number
  constant ROM_SIZE : natural := 1024;--395272;  --* Maximum size in 16-bit word
  
  subtype rom_block is std_ulogic_vector(15 downto 0);
  type rom_t is array (natural range 0 to ROM_SIZE - 1) of rom_block;

  constant rom_none : rom_t := (others => x"0000");

  --VENDOR 
  constant TELECOM_PARISTECH: natural range 0 to 255 := 1;
  constant EURECOM: natural range 0 to 255 := 2;
  -- DEVICE ID
  constant CROSSBAR : natural range 0 to 255 := 1;
  constant BRIDGE : natural range 0 to 255 := 2;
  constant FEP : natural range 0 to 255 := 3;
  constant PRE_PROCESSOR : natural range 0 to 255 := 4;
  constant INTL : natural range 0 to 255 := 5;
  constant MAPPER : natural range 0 to 255 := 6;
  constant RAM : natural range 0 to 255 := 7;
  constant CHANNEL_DECODER : natural range 0 to 255 := 8;
  constant AGRF : natural range 0 to 255 := 9;
  constant ADCSPI : natural range 0 to 255 := 10;
  constant GPIO : natural range 0 to 255 := 11;
  constant ADAIF : natural range 0 to 255 := 12;
  constant LIMESPI : natural range 0 to 255 := 13;
  constant ADAIFEM1 : natural range 0 to 255 := 14;
  -- DMA, UC, PSS
  constant ND : std_ulogic := '0'; --* No DMA
  constant WD : std_ulogic := '1'; --* With DMA
  constant NU : std_ulogic := '0'; --* No UC
  constant WU : std_ulogic := '1'; --* With UC
  constant NC : std_ulogic := '0'; --* No PSS
  constant WC : std_ulogic := '1'; --* With PSS

  type instance is record                   --* instance definition
    ven : natural range 0 to 255;           --* vendor
    d : natural range 0 to 255;             --* device
    c : std_ulogic_vector(2 downto 0);      --* device configuration
    ver : natural range 0 to 255;           --* version
    nirq: natural range 0 to 255;           --* number of interruptions
    nt: natural range 0 to INS_MAX - 1;     --* number of connected targets
    t: natv(0 to INS_MAX - 1);              --* list of IDs of connected targets in decreasing order of fixed priority; n to nt-1 are ignored
    ni: natural range 0 to INS_MAX - 1;     --* number of connected Initiator
    i: natv(0 to INS_MAX - 1);              --* list of IDs of connected initiators in decreasing order of fixed priority; n to nt-1 are ignored
    msk : std_ulogic_vector(11 downto 0);   --* address mask
    addr: std_ulogic_vector(11 downto 0);   --* address 
  end record;
  
  constant no_instance : instance := (
    ven  => 0,
    d    => 0,
    c    => "000",
    ver  => 0,
    nirq => 0,
    msk  => X"000",
    addr => X"000",
    nt   => 0,
    t    => (others => 0),
    ni   => 0,
    i    => (others => 0));

  type vinstance is array(natural range 0 to INS_MAX - 1) of instance; --* vector of instance definitions

  function set_entry(vendor, device, version, dnirq : natural range 0 to 255; conf: std_ulogic_vector(2 downto 0); msk, addr : std_ulogic_vector(11 downto 0)) return std_ulogic_vector; 
  function get_ni(vi : vinstance) return natural;
  function get_nt(vi : vinstance) return natural;
  function get_nins(vi : vinstance) return natural ;
  function get_nirq(vi : vinstance) return natural;
  function generate_rom(vi : vinstance; nper : natural range 0 to INS_MAX - 1 ; nirq : natural range 0 to IRQ_MAX - 1) return rom_t; 

end package plugplay_0_pkg;

package body plugplay_0_pkg is

  function set_entry(vendor, device, version, dnirq : natural range 0 to 255; conf: std_ulogic_vector(2 downto 0); msk, addr : std_ulogic_vector(11 downto 0)) return std_ulogic_vector is
    variable a, b, d : std_ulogic_vector(7 downto 0);
    variable c : std_ulogic_vector(15 downto 0);
  begin

    a :=  std_ulogic_vector(to_unsigned(vendor, 8));
    b :=  std_ulogic_vector(to_unsigned(version, 8));
    c :=  std_ulogic_vector(to_unsigned(device, 13)) & conf ;
    d :=  std_ulogic_vector(to_unsigned(dnirq, 8));

    return (a & b & c & d & addr & msk);

  end function set_entry;  

  function get_nins(vi : vinstance) return natural is
  -- Return the number of instance 
    variable nins: natural range 0 to INS_MAX - 1;

  begin

    nins := 0;
    l : for i in 0 to INS_MAX - 1 loop
      exit l when vi(i) = no_instance;
      nins := nins + 1;
    end loop;
    return nins;
  end function get_nins;
  
  function get_ni(vi : vinstance) return natural is
  -- Return the number of initiator 
    variable n: natural range 0 to INS_MAX - 1;

  begin

    n := 0;
    l : for i in 0 to INS_MAX - 1 loop
      exit l when vi(i) = no_instance;
      if vi(i).nt /= 0 then
        n := n + 1;
      end if;
    end loop;
    return n;
  end function get_ni;

  function get_nt(vi : vinstance) return natural is
  -- Return the number of target 
    variable n: natural range 0 to INS_MAX - 1;

  begin

    n := 0;
    l : for i in 0 to INS_MAX - 1 loop
      exit l when vi(i) = no_instance;
      if vi(i).ni /= 0 then 
        n := n + 1;
      end if;
    end loop;
    return n;
  end function get_nt;

  function get_nirq(vi : vinstance) return natural is
  -- Return the number of interruption 
    variable irq : natural range 0 to IRQ_MAX - 1;

  begin

    irq := 0;
    l : for i in 0 to INS_MAX - 1 loop
      exit l when vi(i) = no_instance;
        irq := irq + vi(i).nirq;
    end loop;

    return irq;

  end function get_nirq;

  function generate_rom(vi : vinstance; nper : natural range 0 to INS_MAX - 1 ; nirq : natural range 0 to IRQ_MAX - 1) return rom_t is
  -- Build a Plug and Play ROM 
    variable r : rom_t; 
    variable t : std_ulogic_vector(63 downto 0); 
    variable k : natural; 

  begin

    k := 0;
    for i in 0 to ROM_SIZE - 1 loop
      r(i) := (others => '0');
    end loop;

    -- Table 1 
    r(0) := X"00" & std_ulogic_vector(to_unsigned(nper, 8));
    r(1) := std_ulogic_vector(to_unsigned(nirq, 16));
    k := 2;
    -- Table 2 
    for i in 0 to nper - 1 loop
      t := set_entry(vi(i).ven, vi(i).d, vi(i).ver, vi(i).nirq, vi(i).c, vi(i).msk, vi(i).addr);
      for j in 0 to 3 loop 
        r(k + j) := t(63 - j * 16 downto 48 - j * 16);
      end loop;
      k := k + 4;
    end loop;
    -- Table 3 
    for i in 0 to nper - 1 loop
      if vi(i).nirq /= 0 then 
        for j in 0 to vi(i).nirq - 1 loop
          r(k) := X"00" & std_ulogic_vector(to_unsigned(i, 8)); 
          r(k + 1) := std_ulogic_vector(to_unsigned(j, 16)); 
          k := k + 2;
        end loop;
      end if;
    end loop;
    -- Table 4
    for i in 0 to nper - 1 loop
      if vi(i).nt /= 0 then 
        for j in 0 to vi(i).nt - 1 loop
          r(k) := std_ulogic_vector(to_unsigned(i, 8)) & std_ulogic_vector(to_unsigned(vi(i).t(j), 8)); 
          k := k + 1;
        end loop;
      end if;
    end loop;
    return r;
  end function generate_rom;

end package body plugplay_0_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
