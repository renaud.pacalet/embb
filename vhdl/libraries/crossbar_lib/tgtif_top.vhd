--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Top level of tgtif for synthesis tests

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;

use work.plugplay_0_pkg.all;

entity tgtif_top is
  port(
    clk : in std_ulogic;
    srstnin : in std_ulogic;
    srstnout : out std_ulogic_vector(2 downto 0);
    ce : out std_ulogic_vector(2 downto 0);
    tvci_in : in vci_i2t_type;
    tvci_out : out vci_t2i_type;
    irqo : out std_ulogic;
    hirq : out std_ulogic_vector(2 downto 0);
    irqi : in std_ulogic_vector(1 downto 0)
  );
end entity tgtif_top;

architecture rtl of tgtif_top is

  constant ADC_ENABLE : integer := 0;
  constant LIME_ENABLE : integer := 0;
  constant PRE_PROCESSOR_ENABLE : integer := 0;
  constant ADAIF_ENABLE : integer := 0;
  constant CHANNEL_DECODER_ENABLE : integer := 0;
  constant RAM_ENABLE : integer := 1;
  constant FEP_ENABLE : integer := 0;
  constant MAPPER_ENABLE : integer := 0;
  constant INTL_ENABLE : integer := 0;

  constant icross : natural range 0 to INS_MAX - 1 := 0;                                --* Instance number:Crossbar
  constant ibri   : natural range 0 to INS_MAX - 1 := 1;                                --* Instance number:Bridge
  constant ifep   : natural range 0 to INS_MAX - 1 := ibri   + FEP_ENABLE;              --* Instance number:Front eINS_MAX processor
  constant iram   : natural range 0 to INS_MAX - 1 := ifep   + RAM_ENABLE;              --* Instance number:VCI RAM
  constant iintl  : natural range 0 to INS_MAX - 1 := iram   + INTL_ENABLE;             --* Instance number:Interleaver
  constant imapp  : natural range 0 to INS_MAX - 1 := iintl  + MAPPER_ENABLE;           --* Instance number:Mapper
  constant iadc   : natural range 0 to INS_MAX - 1 := imapp  + ADC_ENABLE;              --* Instance number:SPI controller interface for adc
  constant ichdc  : natural range 0 to INS_MAX - 1 := iadc   + CHANNEL_DECODER_ENABLE;  --* Instance number:Channel decoder
  constant ipp    : natural range 0 to INS_MAX - 1 := ichdc  + PRE_PROCESSOR_ENABLE;    --* Instance number:Preprocessor
  constant iadaif : natural range 0 to INS_MAX - 1 := ipp    + ADAIF_ENABLE;            --* Instance number:ADA interface

  constant cross : instance := (ven => TELECOM_PARISTECH, d => CROSSBAR, c => ND & NU & NC, ver => 0, nirq => 1, msk => X"FFF", addr => X"400", nt => 0, ni => 1, t => (others => 0),                i => (ibri, others => 0));
  constant ram   : instance := (ven => TELECOM_PARISTECH, d => RAM,      c => WD & NU & NC, ver => 0, nirq => 1, msk => X"FFF", addr => X"401", nt => 1, ni => 1, t => (ibri, others => 0),          i => (ibri, others => 0));
  constant bri   : instance := (ven => TELECOM_PARISTECH, d => BRIDGE,   c => ND & NU & NC, ver => 0, nirq => 0, msk => X"C00", addr => X"800", nt => 2, ni => 1, t => (icross, iram, others => 0),  i => (iram, others => 0));

  constant instances : vinstance := (icross => cross, ibri => bri, iram => ram, others => no_instance);

  constant nins: natural := get_nins(instances);       --* Instance number
  constant ni:   natural := get_ni(instances);         --* Number of initiators
  constant nt:   natural := get_nt(instances);         --* Number of targets
  constant nirq: natural := get_nirq(instances);       --* Number of irqs

  constant rom: rom_t := generate_rom(instances, nins, nirq); --* Plug&Play rom   

begin

  itgtif: entity work.tgtif
  generic map(
    rom  => rom,
    nins => nins,
    nirq => nirq)
  port map(
    clk      => clk,
    srstnin  => srstnin,
    srstnout => srstnout,
    ce       => ce,
    tvci_in  => tvci_in,
    tvci_out => tvci_out,
    irqo     => irqo,
    hirq     => hirq,
    irqi     => irqi
  );

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
