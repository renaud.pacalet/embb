--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

use work.bitfield.all;

use work.crossbar_pkg.all;
use work.plugplay_0_pkg.all;

entity tgtif is
  generic(rom : rom_t;
          nins : natural;
          nirq : natural);
  port (
    clk : in std_ulogic;
    srstnin : in std_ulogic;
    srstnout : out std_ulogic_vector(nins - 1 downto 0);
    ce : out std_ulogic_vector(nins - 1 downto 0);
    tvci_in : in vci_i2t_type;
    tvci_out : out vci_t2i_type;
    irqo : out std_ulogic;
    hirq : out std_ulogic_vector(nins - 1 downto 0);
    irqi : in std_ulogic_vector(nirq - 1 downto 0)
  );
end entity tgtif;


architecture rtl of tgtif is

  type crossbar_regs_type is record
    config : std_ulogic_vector(config_width - 1 downto 0);
    srstn  : std_ulogic_vector(srstn_width - 1 downto 0);
    hirq   : std_ulogic_vector(0 to hirq_width - 1);
    status : std_ulogic_vector(status_width - 1 downto 0);
    ce     : std_ulogic_vector(ce_width - 1 downto 0);
    irqmsk : std_ulogic_vector(irqmsk_width - 1 downto 0);
  end record;

  constant crossbar_regs_none : crossbar_regs_type :=(
    config => (others => '0'),
    srstn  => (others => '0'),
    hirq   => (others => '0'),
    status => (others => '0'),
    ce     => (others => '0'),
    irqmsk => (others => '0'));

  signal r, rin : crossbar_regs_type;
  signal pstatus : std_ulogic_vector(31 downto 0);

  type rsp_fifo_in is record
    enr : std_ulogic;
    enw : std_ulogic;
    di  : std_ulogic_vector(vci_response_length - 1 downto 0);
  end record;

  type rsp_fifo_out is record
    empty : std_ulogic;
    full  : std_ulogic;
    do    : std_ulogic_vector(vci_response_length - 1 downto 0);
  end record;

  type req_fifo_in is record
    enr : std_ulogic;
    enw : std_ulogic;
    di  : std_ulogic_vector(vci_request_length - 1 downto 0);
  end record;

  type req_fifo_out is record
    empty : std_ulogic;
    full  : std_ulogic;
    do    : std_ulogic_vector(vci_request_length - 1 downto 0);
  end record;

  signal freqi : req_fifo_in;
  signal freqo : req_fifo_out;
  signal frspi : rsp_fifo_in;
  signal frspo : rsp_fifo_out;
  signal vci_in : vci_request_type;
  signal vci_out : vci_response_type;


begin
   -- VCI fifo

   fifo_req: entity global_lib.fifo 
   generic map (
     T      =>  std_ulogic_vector(vci_request_length - 1 downto 0),
     depth  =>  2)
   port map (
     srstn =>  srstnin,
     clk   =>  clk,
     read_en  =>  freqi.enr,
     write_en =>  freqi.enw, 
     dout  =>  freqo.do,
     din   =>  freqi.di,
     empty =>  freqo.empty,
     full  =>  freqo.full
   );

   fifo_rsp: entity global_lib.fifo 
   generic map (
     T      =>  std_ulogic_vector(vci_response_length - 1 downto 0),
     depth  =>  2)
   port map (
     srstn =>  srstnin,
     clk   =>  clk,
     read_en  =>  frspi.enr,
     write_en =>  frspi.enw, 
     dout  =>  frspo.do,
     din   =>  frspi.di,
     empty =>  frspo.empty,
     full  =>  frspo.full
   );

   -- IRQ ctrl

  irqctrl: entity work.irqctrl
  generic map(nirq => nirq)
  port map(
    clk    => clk,
    srstn  => srstnin,
    m      => r.config(0),
    irqi   => irqi,
    mski   => r.irqmsk(nirq - 1 downto 0),
    irqo   => irqo,
    status => pstatus
  );

  ce <= r.ce(nins - 1 downto 0);
  srstnout <= r.srstn(nins - 1 downto 0);
  hirq <= r.hirq(0 to nins - 1);

  process(tvci_in, freqo)
  begin
    freqi.di <= vci_req_to_vector(tvci_in.req);
    freqi.enw <= tvci_in.req.cmdval and not freqo.full;

    tvci_out.cmdack <= not freqo.full;

    vci_in <= vci_request_none;
    if freqo.empty = '0' then 
      vci_in <= vector_to_vci_req(freqo.do);
    end if;
  end process;

  process(tvci_in, frspo, vci_out) 
  begin 
    frspi.di <= vci_rsp_to_vector(vci_out);
    frspi.enr <= tvci_in.rspack and not frspo.empty;

    tvci_out.rsp  <= vci_response_none;
    if frspo.empty = '0' then 
      tvci_out.rsp <= vector_to_vci_rsp(frspo.do);
    end if;
  end process;

  process (frspo, pstatus, r, vci_in)
    
    variable mem : rom_t := rom;
    variable d : std_ulogic_vector(63 downto 0);
    variable a : integer;
    variable idx : natural;
    variable v : crossbar_regs_type;
    variable t : vci_address_type; 

  begin

    v := r;
    
    freqi.enr <= '0';
    frspi.enw <= '0';

    vci_out <= vci_response_none;

    v.status(31 downto 0) := pstatus;
    v.irqmsk              := (others => '1');

    idx := to_integer(u_unsigned(vci_in.address(4 downto 3)));

    if vci_in.cmdval = '1' and frspo.full = '0' then

      freqi.enr <= '1'; 
      frspi.enw <= '1'; 

      vci_out.reop   <= vci_in.eop;
      vci_out.rpktid <= vci_in.pktid;
      vci_out.rtrdid <= vci_in.trdid;
      vci_out.rsrcid <= vci_in.srcid;
      vci_out.rspval <= '1';

      if vci_in.address < X"00021804" then 
      -- START ROM ACCESS 
        t := vci_in.address(vci_n - 1 downto 3) & "000";
        a := to_integer(u_unsigned(t(18 downto 1)));
        d := mem(a) & mem(a + 1) & mem(a + 2) & mem(a + 3); 
        for i in 0 to 7 loop
          if vci_in.be(7 - i) = '1' then 
              vci_out.rdata(63 - 8 * i downto 56 - 8 * i) <= d(63 - 8 * i downto 56 - 8 * i);
          end if;
        end loop;
      -- END ROM ACCESS 
      elsif (vci_in.address and X"ffffff07") = X"000fff00" then               
      -- START REGISTER ACCESS 
--       if vci_in.address(7) = irqmsk_address(7) then 
--         idx := to_integer(u_unsigned(vci_in.address(6 downto 3)));
--         if vci_in.cmd = vci_cmd_read then
--           for i in 0 to 7 loop
--             if vci_in.be(7 - i) = '1' then 
--                 vci_out.rdata(63 - 8 * i downto 56 - 8 * i) <= r.irqmsk(63 + 64 * idx - 8 * i downto 56 + 64 * idx - 8 * i);
--             end if;
--           end loop;
--         elsif vci_in.cmd = vci_cmd_write then
--           for i in 0 to 7 loop
--             if vci_in.be(7 - i) = '1' then 
--                 v.irqmsk(63 + 64 * idx - 8 * i downto 56 + 64 * idx - 8 * i) := vci_in.wdata(63 - 8 * i downto 56 - 8 * i);
--             end if;
--           end loop;
--         end if;
--       else
          if vci_in.address(6 downto 5) = srstn_address(6 downto 5) then
            if vci_in.cmd = vci_cmd_read then
              for i in 0 to 7 loop
                if vci_in.be(7 - i) = '1' then 
                    vci_out.rdata(63 - 8 * i downto 56 - 8 * i) <= r.srstn(63 + 64 * idx - 8 * i downto 56 + 64 * idx - 8 * i);
                end if;
              end loop;
            elsif vci_in.cmd = vci_cmd_write then
              for i in 0 to 7 loop
                if vci_in.be(7 - i) = '1' then 
                    v.srstn(63 + 64 * idx - 8 * i downto 56 + 64 * idx - 8 * i) := vci_in.wdata(63 - 8 * i downto 56 - 8 * i);
                end if;
              end loop;
            end if;
--         elsif vci_in.address(6 downto 5) = hirq_address(6 downto 5) then 
--           if vci_in.cmd = vci_cmd_read then
--             for i in 0 to 7 loop
--               if vci_in.be(7 - i) = '1' then 
--                   vci_out.rdata(63 - 8 * i downto 56 - 8 * i) <= r.hirq(64 * idx + 8 * i  to 64 * idx + 8 * i + 8 - 1);
--               end if;
--             end loop;
--           elsif vci_in.cmd = vci_cmd_write then
--             for i in 0 to 7 loop
--               if vci_in.be(7 - i) = '1' then 
--                   v.hirq(64 * idx + 8 * i  to 64 * idx + 8 * i + 8 - 1) := vci_in.wdata(63 - 8 * i downto 56 - 8 * i);
--               end if;
--             end loop;
--           end if;
          elsif vci_in.address(6 downto 5) = ce_address(6 downto 5) then 
            if vci_in.cmd = vci_cmd_read then
              for i in 0 to 7 loop
                if vci_in.be(7 - i) = '1' then 
                    vci_out.rdata(63 - 8 * i downto 56 - 8 * i) <= r.ce(63 + 64 * idx - 8 * i downto 56 + 64 * idx - 8 * i);
                end if;
              end loop;
            elsif vci_in.cmd = vci_cmd_write then
              for i in 0 to 7 loop
                if vci_in.be(7 - i) = '1' then 
                    v.ce(63 + 64 * idx - 8 * i downto 56 + 64 * idx - 8 * i) := vci_in.wdata(63 - 8 * i downto 56 - 8 * i);
                end if;
              end loop;
            end if;
          elsif vci_in.address(6 downto 5) = status_address(6 downto 5) then 
            if vci_in.address(4 downto 3) = status_address(4 downto 3) then 
              if vci_in.cmd = vci_cmd_read then 
                for i in 0 to 7 loop
                  if vci_in.be(7 - i) = '1' then 
                      vci_out.rdata(63 - 8 * i downto 56 - 8 * i) <= r.status(63 - 8 * i downto 56 - 8 * i);
                  end if;
                end loop;
              end if;
            elsif vci_in.address(4 downto 3) = config_address(4 downto 3) then 
              if vci_in.cmd = vci_cmd_read then 
                for i in 0 to 7 loop
                  if vci_in.be(7 - i) = '1' then 
                      vci_out.rdata(63 - 8 * i downto 56 - 8 * i) <= r.config(63 - 8 * i downto 56 - 8 * i);
                  end if;
                end loop;
              elsif vci_in.cmd = vci_cmd_write then
                for i in 0 to 7 loop
                  if vci_in.be(7 - i) = '1' then 
                      v.config(63 - 8 * i downto 56 - 8 * i) := vci_in.wdata(63 - 8 * i downto 56 - 8 * i);
                  end if;
                end loop;
              end if;
            else
              vci_out.rerror(0) <= '1';
            end if;
          end if;
--        end if;
      else
        vci_out.rerror(0) <= '1';
      end if;
  -- END REGISTER ACCESS
    end if;


    rin <= v;
 
  end process;

  process (clk)

  begin

    if rising_edge(clk) then

      if srstnin = '0' then
        r <= crossbar_regs_none;
      else
        r <= rin;
      end if;

    end if;
  
  end process;


end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
