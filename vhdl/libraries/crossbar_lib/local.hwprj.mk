#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

gh-IGNORE	+= crossbar_lib.tgtif crossbar_lib.tgtif_top

crossbar_lib.crossbar_pkg: \
	global_lib.global \
	global_lib.utils

crossbar_lib.irqctrl: \
	global_lib.utils

crossbar_lib.parbiter: \
	global_lib.global \
	global_lib.utils \
	crossbar_lib.crossbar_pkg

crossbar_lib.parbiter_sim: \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils \
	random_lib.rnd \
	crossbar_lib.crossbar_pkg \
	crossbar_lib.parbiter

crossbar_lib.parbiter_top: \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils \
	crossbar_lib.crossbar_pkg \
	crossbar_lib.parbiter

crossbar_lib.plugplay_0_pkg: \
	crossbar_lib.crossbar_pkg

crossbar_lib.qarbiter: \
	global_lib.global \
	global_lib.utils \
	crossbar_lib.crossbar_pkg

crossbar_lib.qarbiter_sim: \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils \
	random_lib.rnd \
	crossbar_lib.crossbar_pkg \
	crossbar_lib.qarbiter

crossbar_lib.qarbiter_top: \
	global_lib.global \
	global_lib.utils \
	crossbar_lib.crossbar_pkg \
	crossbar_lib.qarbiter

crossbar_lib.tgtif: \
	global_lib.global \
	global_lib.utils \
	global_lib.fifo \
        crossbar_lib.plugplay_0_pkg \
        crossbar_lib.crossbar_pkg \
        crossbar_lib.irqctrl \
        crossbar_lib.bitfield_pkg

crossbar_lib.tgtif_top: \
	global_lib.global \
        crossbar_lib.plugplay_0_pkg \
	crossbar_lib.tgtif

ms-sim.crossbar_lib.qarbiter_sim: MSSIMFLAGS += -t ps -do 'run -all; quit'
ms-sim.crossbar_lib.parbiter_sim: MSSIMFLAGS += -t ps -do 'run -all; quit'
ms-sim.crossbar_lib: ms-sim.crossbar_lib.qarbiter_sim ms-sim.crossbar_lib.parbiter_sim
ms-sim: ms-sim.crossbar_lib

pr-syn.crossbar_lib.tgtif_top: bitfield_pkg.vhd
pr-syn.crossbar_lib: $(addprefix pr-syn.crossbar_lib.,irqctrl parbiter_top qarbiter_top tgtif_top)
pr-syn: pr-syn.crossbar_lib

rc-syn.crossbar_lib.tgtif_top: bitfield_pkg.vhd
rc-syn.crossbar_lib: $(addprefix rc-syn.crossbar_lib.,irqctrl parbiter_top qarbiter_top tgtif_top)
rc-syn: rc-syn.crossbar_lib

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
