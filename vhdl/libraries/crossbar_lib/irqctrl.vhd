--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.utils.all;

entity irqctrl is
  generic(nirq : natural);
  port (
    clk    : in  std_ulogic;
    srstn  : in  std_ulogic;
    m      : in  std_ulogic;
    irqi   : in  std_ulogic_vector(nirq - 1 downto 0);
    mski   : in  std_ulogic_vector(nirq - 1 downto 0);
    irqo   : out std_ulogic;
    status : out std_ulogic_vector(31 downto 0)
  );
end entity irqctrl;

architecture rtl of irqctrl is
  
 function log2(v: positive) return natural is
  begin
    if v = 1 then
      return 0;
    elsif v mod 2 = 1 then
      return 1 + log2((v + 1) / 2);
    else
      return 1 + log2(v / 2);
    end if;
  end function log2;

  procedure vffs(a: in std_ulogic_vector; irq: out std_ulogic; idx: out natural) is
    constant n: positive := a'length;
    constant l: natural := log2(n);
    constant p: positive := 2**l;
    variable va: std_ulogic_vector(n - 1 downto 0) := a;
    variable irql, irqh: std_ulogic;
    variable idxl, idxh: natural range 0 to n - 1;
  begin
    if n = 1 then
      idx := 0;
      irq := va(0);
    else
      vffs(va(n - 1 downto p / 2), irqh, idxh);
      vffs(va(p / 2 - 1 downto 0), irql, idxl);
      irq := irql or irqh;
      if irqh = '1' then
        idx := idxh + p / 2;
      else
        idx := idxl;
      end if;
    end if;
  end procedure vffs;

  procedure ffs(signal a: in std_ulogic_vector; irq: out std_ulogic; idx: out natural) is
    variable virq: std_ulogic;
    variable vidx: natural;
  begin
    vffs(a, virq, vidx);
    irq := virq;
    idx := vidx;
  end procedure ffs;

  signal irqr: std_ulogic_vector(nirq - 1 downto 0);
  signal msko: std_ulogic;

begin

  process (clk)

    variable irqv : std_ulogic;
    variable idx: natural;

  begin

    if rising_edge(clk) then

      if srstn = '0' then
        irqo <= '0';
        msko <= '0';
        idx := 0;
        irqr <= (others => '0');
        status <= (others => '0');
      else
        irqo <= '0';
        irqr <= irqi and mski;
        msko <=  not or_reduce(irqr);
        ffs(irqr, irqv, idx);
        if m = '0' then 
        -- edge-triggered
          irqo <= msko and irqv;
        else
        -- level-triggered
          irqo <= irqv;
        end if;
        status <= std_ulogic_vector(to_unsigned(idx, 32));
        status(31) <= irqv;
      end if;

    end if;
  
  end process;


end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
