--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.axi_pkg_m4.all;

-- Random AXI4 lite 32 bits master. For simulation only. Randomly issues read-write requests and checks the read responses against a virtual embedded RAM model.
-- Non yet written bytes are not checked upon read operations.
entity axi4lite32_master is
  generic(
    na:    positive range 2 to 16 := 16; -- Bit-width of addresses busses
    debug: boolean := false -- Print debug information
  );
  port(
    clk:         in  std_ulogic;
    srstn:       in  std_ulogic;
    axi_m2s:     out axilite_m2s;
    axi_s2m:     in  axilite_s2m
  );
end entity axi4lite32_master;

architecture rtl of axi4lite32_master is

  package axilite_m2s_fifo_pkg is new work.fifo_pkg generic map(T => axilite_m2s);
  package axilite_s2m_fifo_pkg is new work.fifo_pkg generic map(T => axilite_s2m);

  type axi4lite32_data_vector is array(natural range <>) of std_ulogic_vector(axi_d - 1 downto 0);
  signal axi_m2s_l: axilite_m2s;

begin

  axi_m2s <= axi_m2s_l;

  process(clk)
    variable tmp: axilite_m2s;
    variable l: line;
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        axi_m2s_l <= axilite_m2s_none;
      else
        if debug and (axi_m2s_l.awvalid = '1' or axi_m2s_l.wvalid = '1' or axi_m2s_l.arvalid = '1' or axi_m2s_l.bready = '1' or axi_m2s_l.rready = '1') then
          write(l, axi_m2s_l);
          writeline(output, l);
        end if;
        tmp := axilite_m2s_rnd;
        tmp.awaddr(31 downto na) := (others => '0');
        tmp.araddr(31 downto na) := (others => '0');
        if axi_m2s_l.awvalid = '0' or axi_s2m.awready = '1' then
          axi_m2s_l.awvalid <= tmp.awvalid;
          axi_m2s_l.awaddr <= tmp.awaddr;
          axi_m2s_l.awprot <= tmp.awprot;
        end if;
        if axi_m2s_l.wvalid = '0' or axi_s2m.wready = '1' then
          axi_m2s_l.wvalid <= tmp.wvalid;
          axi_m2s_l.wdata <= tmp.wdata;
          axi_m2s_l.wstrb <= tmp.wstrb;
        end if;
        if axi_m2s_l.arvalid = '0' or axi_s2m.arready = '1' then
          axi_m2s_l.arvalid <= tmp.arvalid;
          axi_m2s_l.araddr <= tmp.araddr;
          axi_m2s_l.arprot <= tmp.arprot;
        end if;
        axi_m2s_l.rready <= tmp.rready;
        axi_m2s_l.bready <= tmp.bready;
      end if;
    end if;
  end process;

  process(clk)
    variable ram: axi4lite32_data_vector(0 to 2**(na - 2) - 1) := (others => (others => '-'));
    variable add: natural range 0 to 2**(na - 2) - 1;
    variable wdata: std_ulogic_vector(31 downto 0);
    variable wstrb: std_ulogic_vector(3 downto 0);
    variable faw, fw: axilite_m2s_fifo_pkg.fifo;
    variable fb, fr: axilite_s2m_fifo_pkg.fifo;
    variable m2s: axilite_m2s;
    variable s2m: axilite_s2m;
    variable l: line;
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        ram := (others => (others => '-'));
        faw.free;
        fw.free;
        fb.free;
        fr.free;
      else
        if axi_m2s_l.awvalid = '1' and axi_s2m.awready = '1' then
          faw.push(axi_m2s_l);
        end if;
        if axi_m2s_l.wvalid = '1' and axi_s2m.wready = '1' then
          fw.push(axi_m2s_l);
          s2m := (rdata => (others => '-'), bresp => axi_resp_okay, rresp => (others => '-'), bvalid => '1', others => '-');
          fb.push(s2m);
        end if;
        if faw.count /= 0 and fw.count /= 0 then
          m2s := faw.pop;
          add := to_integer(u_unsigned(m2s.awaddr(na - 1 downto 2)));
          m2s := fw.pop;
          mask(ram(add), m2s.wdata, m2s.wstrb);
        end if;
        if axi_m2s_l.arvalid = '1' and axi_s2m.arready = '1' then
          add := to_integer(u_unsigned(axi_m2s_l.araddr(na - 1 downto 2)));
          s2m := (rdata => ram(add), rresp => axi_resp_okay, bresp => (others => '-'), rvalid => '1', others => '-');
          fr.push(s2m);
        end if;
        if axi_m2s_l.bready = '1' and axi_s2m.bvalid = '1' then
          s2m := fb.pop;
          if not check(s2m, axi_s2m) then
            write(l, string'("******* Error:"));
            writeline(output, l);
            write(l, string'("       got: "));
            write(l, axi_s2m);
            writeline(output, l);
            write(l, string'("  expected: "));
            write(l, s2m);
            writeline(output, l);
            faw.free;
            fw.free;
            fb.free;
            fr.free;
            assert false severity failure;
          end if;
        end if;
        if axi_m2s_l.rready = '1' and axi_s2m.rvalid = '1' then
          s2m := fr.pop;
          if not check(s2m, axi_s2m) then
            write(l, string'("******* Error:"));
            writeline(output, l);
            write(l, string'("       got: "));
            write(l, axi_s2m);
            writeline(output, l);
            write(l, string'("  expected: "));
            write(l, s2m);
            writeline(output, l);
            faw.free;
            fw.free;
            fb.free;
            fr.free;
            assert false severity failure;
          end if;
        end if;
      end if;
    end if;
  end process;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
