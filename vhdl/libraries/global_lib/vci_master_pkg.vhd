--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation utility package
--*
--* The functions defined in this package can be used to parse command files and drive simulations. The syntax of the command files is explained in the
--* following. The targeted simulations are those that simulate a complete DSP unit. The simulation environment sends VCI read-write requests on the VCI target port
--* of the DSP unit to access the MSS and the control and status registers. By accessing the control and status registers it can launch PSS operations and DMA
--* transfers. It also controls the interrupt enables, chip enables and resets of the DSP unit. It can also upload a UC image in the MSS part dedicated to UC and wake
--* up UC to run macros. The simulation environment also plays the role of the VCI target to which DMA sends read-write requests. It acknowledges and responds
--* DMA requests. The behavior is entirely defined in a command file. HST_EMULATOR_2 contains two independent VCI engines: VIE (VCI Initiator Emulator) and VTE
--* (VCI Target Emulator). VIE plays the role of a VCI initiator. It is connected to the target VCI interface of the DSP unit. It sends read/write requests and checks
--* the corresponding responses against expected ones. VTE acts as a distant VCI target and answers the requests issued by the DSP unit (that is, by its embedded DMA
--* engine). It also checks the requests against the expected ones. During the parsing of the command file, and depending on the encountered commands and their
--* parameters, new VCI requests and responses are added to VIE and VTE in a FIFO-like way. VIE and VTE "play" them in the same order and check the
--* corresponding outgoing responses and requests. The PIRQ, DIRQ, UIRQ and EIRQ interrupts are taken into account by HST_EMULATOR_2: they are the only way to
--* detect the end of a computation, DMA transfer, UC routine or extended interrupt firing by the PSS, and to synchronize with the sequence of operations
--* defined in the command file.
--*
--* Syntax of the command files:
--* Comments extend from the '#' character to the end of the line. The file contains a sequence of commands. Each command is made of one or several fields.
--* Fields are separated by blanks (spaces, tabs or end of lines). Numeric fields are in binary, decimal or hexadecimal form, as specified in the following.
--* Hexadecimal is the default. The first field of a command is a 2 characters command specifier. The following fields in a command, if any, are parameters. The
--* number of parameters depends on the command. They are mandatory and have thus no default value. Supported commands are RS, WR, RD, ER, EW, and QT.
--*
--* - RS N: ReSet, forces a N cycles reset (SRSTN=0) of DSP unit. Parameters:
--*   - N: number of reset cycles, decimal. Zero means 'forever'.
--*   No VCI request or response is sent to DSP unit during the reset period. At the end of the reset period SRSTN is asserted and the parsing of the command file
--*   continues. Note that the whole DSP unit is reset and that the previous content of the configuration and status registers is lost. A RS command must thus usually be
--*   followed by a series of WR/RD commands in order to configure the DSP unit (chip enables, resets, interrupt enables, etc.)
--*
--* - WR ADD LENGTH BE PAYLOAD: WRite. Performs a write operation from the environment to the DSP unit, at the target AVCI interface. Parameters:
--*   - ADD: byte ADDress in the memory space of the DSP unit, between 00000 and fffff; note that this address space is sparse and that out of range
--*     accesses will be trapped. The behaviour is undefined when ADD is not a multiple of 8. The first double-word is written at ADD, the second at ADD+8,...,
--*     the last at ADD+8*(LENGTH-1).
--*   - LENGTH: decimal, number of double-words to write. Must be greater or equal 1 and less than 2^29.
--*   - BE: an 8 bits byte enable from 00 to ff. Set bits indicate which bytes of each double-word to write.
--*   - PAYLOAD: the LENGTH double-words to write, separated by blanks.
--*
--* - RD ADD LENGTH BE MASK PAYLOAD: ReaD. Performs a read operation from the environment to the DSP unit, at the target AVCI interface. Parameters:
--*   - ADD: byte ADDress in the memory space of the DSP unit, between 00000 and fffff; note that this address space is sparse and that out of range
--*     accesses will be trapped. The behaviour is undefined when ADD is not a multiple of 8. The first double-word is read at ADD, the second at ADD+8,..., the
--*     last at ADD+8*(LENGTH-1).
--*   - LENGTH: decimal, number of double-words to read. Must be greater or equal 1 and less than 2^29.
--*   - BE: an 8 bits byte enable from 00 to ff. Set bits indicate which bytes of each double-word to read.
--*   - MASK: a double-word mask used for the comparison between read and expected values. Set bits indicate which bit to compare.
--*   - PAYLOAD: the LENGTH expected double-words, separated by blanks. Mismatches between read and expected values raise a simulation error.
--*
--* - ER/EW T: Wait until end of Read/Write. The next command will be taken into account only after all the transactions are done
--*   of them is raised. Parameters:
--*   - T: timeout in clock cycles, decimal. An assertion is fired with severity error if the timeout is exceeded. Zero means 'forever'.
--*
--* - QT: QuiT. Waits until completion of the pending operations and stops the clock. The simulation should end gracefully. If it does not, there is a problem
--*   somewhere... Parameters: none.
--*
--* Examples:
--* 
--* RS 10                   # Reset PSS for 10 cycles
--* 
--* WR 0 4 ff               # Write, starting at address 0, four double-word, every byte of them.
--* 
--* 0003000200010000        # The double-words to write.
--* 0007000600050004
--* 000b000a00090008
--* 000f000e000d000c
--* 
--* EW 10000                # Wait until end-of-transfer with a 10000 cycles timeout.
--* 
--* RD c000 2 ff ffffffffffffffff # Read at address c000 2 double-words, every byte of them, and compare every bit of them with the expected ones.
--* # The 2 expected double-words.
--* 0302010007060504 0b0a09080f0e0d0c
--* 
--* QT                      # Quit

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

use work.global.all;
use work.utils.all;
use work.sim_utils.all;

package vci_master_pkg is

  --------------------------
  -- Command file parsing --
  --------------------------

  -- Type defining the commands in a command file
  type cmd_type is (RS, WR, RD, ER, EW, QT);
  constant cmd_num: natural := cmd_type'pos(cmd_type'right) + 1; -- Number of different commands
  subtype cmd_name_type is string(1 to 2); -- Type of command names
  type cmd_names_type is array(0 to cmd_num - 1) of cmd_name_type; -- Type of array of all command names
  -- All command names
  constant cmd_names: cmd_names_type := ("RS", "WR", "RD", "ER", "EW", "QT");

  -- Return true if C is a blank character (space, tab or carriage return).
  function is_blank(c: character) return boolean;

  -- Find the next token in line L, starting at character CN (1 being the first character), skipping blanks and comments. L is unmodified. If returned EOL (end
  -- of line) is false, CN points to the first character of next token, else, end of line was encountered first and CN is unmodified.
  procedure next_token(variable l: in line; cn: inout natural; eol: out boolean);

  -- Same as the previous one but with two more parameters: F, a text file and LN a line counter. When end of line is encountered, increments LN, sets CN to 1,
  -- reads a new line in file F and continues until first token or end of file. If returned EOF (end of file) is false, L is the line containing the token and
  -- CN points to the first character of token in L, else, end of file was encountered first.
  procedure next_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; eof: out boolean);

  -- Convert a command to its string name
  function cmd2cmd_name(c: cmd_type) return cmd_name_type;

  -- Convert a command name to the corresponding command
  procedure cmd_name2cmd(n: in cmd_name_type; c: out cmd_type; good: out boolean);

  -- Reads a two characters command in the line L at character CN. On return L is unmodified. If GOOD is true, CN points to the first character after the
  -- command and CMD is the read command, else no command was found.
  procedure cmd_read(variable l: in line; cn: inout natural; cmd: out cmd_type; good: out boolean);

  -- Read an u_unsigned  hexadecimal value from line L. Stop at end of line or first non-hex character. The L parameter is unmodified. GOOD is an exit status set
  -- to false when the first character is non-hex, the line is empty or on overflow of the VAL output parameter. If returned GOOD is true CN points to the first
  -- non-hex character or after the end of line.
  procedure hex_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean);

  -- Read an u_unsigned decimal value from line L. Stop at end of line or first non-dec character. The L parameter is unmodified. GOOD is an exit status set to
  -- false when the first character is non-dec, the line is empty or on overflow of the VAL output parameter. If returned GOOD is true CN points to the first
  -- non-dec character or after the end of line.
  procedure dec_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean);

  -- Read an u_unsigned binary value from line L. Stop at end of line or first non-bit character. The L parameter is unmodified. GOOD is an exit status set to
  -- false when the first character is non-bit, the line is empty or on overflow of the VAL output parameter. If returned GOOD is true CN points to the first
  -- non-bit character or after the end of line.
  procedure bin_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean);

  -- Print a syntax error message
  procedure syntax_error(variable l: in line; ln: in natural; cn: in natural; str: in string);

  -- Advances to next token and reads it as a command
  procedure get_cmd_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out cmd_type; good: out boolean; eof: out boolean);
  procedure get_cmd_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out cmd_type; eof: out boolean);

  -- Advances to next token and reads it as an hex value
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural);

  -- Advances to next token and reads it as a decimal value
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural);

  -- Advances to next token and reads it as a binary value
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural);

  -----------------------------------------
  -- VCI requests and responses handling --
  -----------------------------------------

  -- All FIFOs have a top (pushed entries go on top) and a bottom (poped entries come from bottom). cnt counts the current number of entries. FIFOs are linked
  -- lists. The links are from bottom to top (in a 2-entries FIFO bot.nxt=top and top.nxt=null). All FIFOs have 4 associated procedures. init sets bot and top
  -- to null and cnt to 0. push pushes an entry in the FIFO. pop pops an entry. free deallocates the whole FIFO and leaves it in the init state.

  -- FIFO of VCI requests
  type vci_req_fifo_entry;
  type vci_req_fifo_entry_ptr is access vci_req_fifo_entry;
  type vci_req_fifo_entry is record
    req: vci_request_type;
    nxt: vci_req_fifo_entry_ptr;
  end record vci_req_fifo_entry;

  type vci_req_fifo is record
    bot, top: vci_req_fifo_entry_ptr;
    cnt: natural;
  end record vci_req_fifo;

  procedure init(f: inout vci_req_fifo);
  procedure push(f: inout vci_req_fifo; req: in vci_request_type);
  procedure pop(f: inout vci_req_fifo);
  procedure free(f: inout vci_req_fifo);

  -- FIFO of VCI responses, plus a mask used when checking the RDATA field of a response against the expected one. Set bits in the mask are checked while clear
  -- bits are ignored.
  type vci_rsp_fifo_entry;
  type vci_rsp_fifo_entry_ptr is access vci_rsp_fifo_entry;
  type vci_rsp_fifo_entry is record
    rsp: vci_response_type;
    msk: vci_data_type;
    nxt: vci_rsp_fifo_entry_ptr;
  end record vci_rsp_fifo_entry;

  type vci_rsp_fifo is record
    bot, top: vci_rsp_fifo_entry_ptr;
    cnt: natural;
  end record vci_rsp_fifo;

  procedure init(f: inout vci_rsp_fifo);
  procedure push(f: inout vci_rsp_fifo; rsp: in vci_response_type; msk: in vci_data_type);
  procedure pop(f: inout vci_rsp_fifo);
  procedure free(f: inout vci_rsp_fifo);

  -- The VCI Initiator Emulator contains a VCI requests FIFO and a VCI responses FIFO.
  type vci_initiator_emulator is record
    req_fifo: vci_req_fifo;
    rsp_fifo: vci_rsp_fifo;
  end record vci_initiator_emulator;
  procedure init(vie: inout vci_initiator_emulator);
  procedure free(vie: inout vci_initiator_emulator);
  procedure play(vie: inout vci_initiator_emulator; signal req: out vci_request_type);
  procedure check(vie: inout vci_initiator_emulator; rsp: in vci_response_type);
  procedure is_empty(variable vie: in vci_initiator_emulator; good: out boolean);

  -- FIFO of VCI data fields, plus a mask used when checking the data field against the expected one. Set bits in the mask are checked while clear bits are
  -- ignored.
  type vci_data_fifo_entry;
  type vci_data_fifo_entry_ptr is access vci_data_fifo_entry;
  type vci_data_fifo_entry is record
    data: vci_data_type;
    msk:  vci_data_type;
    nxt:  vci_data_fifo_entry_ptr;
  end record vci_data_fifo_entry;

  type vci_data_fifo is record
    bot, top: vci_data_fifo_entry_ptr;
    cnt: natural;
  end record vci_data_fifo;

  procedure init(f: inout vci_data_fifo);
  procedure push(f: inout vci_data_fifo; data: in vci_data_type; msk: in vci_data_type);
  procedure pop(f: inout vci_data_fifo);
  procedure free(f: inout vci_data_fifo);

  -- The VCI Target Emulator contains a VCI responses FIFO and a FIFO of data fields.
  type vci_target_emulator is record
    rsp_fifo: vci_rsp_fifo;
    data_fifo: vci_data_fifo;
  end record vci_target_emulator;
  procedure init(vte: inout vci_target_emulator);
  procedure free(vte: inout vci_target_emulator);
  procedure play(vte: inout vci_target_emulator; signal rsp: out vci_response_type);
  procedure check(vte: inout vci_target_emulator; req: in vci_request_type);

  procedure read_rs(file f: text; l: inout line; ln: inout natural; cn: inout natural; cnt: out natural);
  procedure read_wr(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator; cst: in boolean := false);
  procedure read_rd(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator; cst: in boolean := false);
  procedure read_qt(file f: text; l: inout line; ln: inout natural; cn: inout natural);

end package vci_master_pkg;

package body vci_master_pkg is

  function is_blank(c: character) return boolean is
  begin
    return c = ' ' or c = HT or c = CR;
  end function is_blank;

  procedure next_token(variable l: in line; cn: inout natural; eol: out boolean) is
  begin
    loop
      if l = null or l'length < cn or l(cn) = '#' then
        eol := true;
        return;
      elsif is_blank(l(cn)) then
        cn := cn + 1;
      else
        eol := false;
        return;
      end if;
    end loop;
  end procedure next_token;

  procedure next_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; eof: out boolean) is
    variable eol: boolean;
  begin
    loop
      next_token(l, cn, eol);
      if eol and endfile(f) then
        eof := true;
        return;
      elsif eol then
        readline(f, l);
        ln := ln + 1;
        cn := 1;
      else
        eof := false;
        return;
      end if;
    end loop;
  end procedure next_token;

  function cmd2cmd_name(c: cmd_type) return cmd_name_type is
  begin
    return cmd_names(cmd_type'pos(c));
  end function cmd2cmd_name;

  procedure cmd_name2cmd(n: in cmd_name_type; c: out cmd_type; good: out boolean) is
  begin
    for i in 0 to cmd_num - 1 loop
      if n = cmd_names(i) then
        good := true;
        c := cmd_type'val(i);
        return;
      end if;
    end loop;
    good := false;
  end procedure cmd_name2cmd;

  procedure cmd_read(variable l: in line; cn: inout natural; cmd: out cmd_type; good: out boolean) is
    variable s: string(1 to 2);
    variable tmp: boolean;
  begin
    good := false;
    if l'length < cn + 1 then -- Less than 2 characters left in line L
      return;
    else
      s(1 to 2) := l(cn to cn + 1);
      cmd_name2cmd(s, cmd, tmp);
      good := tmp;
      if tmp then
        cn := cn + 2;
      end if;
    end if;
  end procedure cmd_read;

  procedure hex_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean) is
    constant n: natural := val'length;
    variable res: u_unsigned(n + 3 downto 0) := (others => '0');
    variable v: integer range -1 to 15;
  begin
    good := false;
    if l = null or l'length < cn then -- Empty line or end of line
      return;
    end if;
    v := char2hex(l(cn));
    if v = -1 then -- First character is not hexadecimal
      return;
    end if;
    loop
      res := res(n - 1 downto 0) & to_unsigned(v, 4);
      if res(n + 3 downto n) /= x"0" then -- Overflow
        return;
      end if;
      cn := cn + 1;
      exit when l'length < cn; -- End of line
      v := char2hex(l(cn));
      exit when v = -1; -- Encountered first non hexadecimal character
    end loop;
    good := true;
    val := res(n - 1 downto 0);
  end procedure hex_read;

  procedure dec_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean) is
    constant n: natural := val'length;
    variable res: u_unsigned(n + 3 downto 0) := (others => '0');
    variable tmp: u_unsigned(2 * n + 7 downto 0) := (others => '0');
    variable v: integer range -1 to 9;
  begin
    good := false;
    if l = null or l'length < cn then -- Empty line or end of line
      return;
    end if;
    v := char2dec(l(cn));
    if v = -1 then -- First character is not decimal
      return;
    end if;
    loop
      tmp := res * 10 + v;
      res := tmp(n + 3 downto 0);
      if res(n + 3 downto n) /= x"0" then -- Overflow
        return;
      end if;
      cn := cn + 1;
      exit when l'length < cn; -- End of line
      v := char2dec(l(cn));
      exit when v = -1; -- Encountered first non decimal character
    end loop;
    good := true;
    val := res(n - 1 downto 0);
  end procedure dec_read;

  procedure bin_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean) is
    constant n: natural := val'length;
    variable res: u_unsigned(n downto 0) := (others => '0');
    variable v: integer range -1 to 1;
  begin
    good := false;
    if l = null or l'length < cn then -- Empty line or end of line
      return;
    end if;
    v := char2bin(l(cn));
    if v = -1 then -- First character is not binary
      return;
    end if;
    loop
      res := res(n - 1 downto 0) & to_unsigned(v, 1);
      if res(n) /= '0' then -- Overflow
        return;
      end if;
      cn := cn + 1;
      exit when l'length < cn; -- End of line
      v := char2bin(l(cn));
      exit when v = -1; -- Encountered first non binary character
    end loop;
    good := true;
    val := res(n - 1 downto 0);
  end procedure bin_read;

  procedure syntax_error(variable l: in line; ln: in natural; cn: in natural; str: in string) is
    variable dbg: line;
  begin
    write(dbg, ln);
    print(dbg, ":");
    write(dbg, cn);
    print(dbg, ": syntax error : ");
    write(dbg, str);
    writeline(output, dbg);
    write(dbg, l.all);
    writeline(output, dbg);
  end procedure syntax_error;

  procedure get_cmd_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out cmd_type; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
  begin
    next_token(f, l, ln, cn, tmp);
    if tmp then -- End of file
      good := true;
      eof  := true;
      return;
    end if;
    eof := false;
    cmd_read(l, cn, val, tmp);
    if not tmp then -- Next token is not a command 
      good := false;
      syntax_error(l, ln, cn, "not a command");
      return;
    end if;
    good := true;
  end procedure get_cmd_token;

  procedure get_cmd_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out cmd_type; eof: out boolean) is
    variable good: boolean;
  begin
    get_cmd_token(f, l, ln, cn, val, good, eof);
    assert good severity failure;
  end procedure get_cmd_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
  begin
    next_token(f, l, ln, cn, tmp);
    if tmp then -- End of file
      good := true;
      eof  := true;
      return;
    end if;
    eof := false;
    hex_read(l, cn, val, tmp);
    if not tmp then -- Next token is not hexadecimal or overflow
      good := false;
      syntax_error(l, ln, cn, "not an hexadecimal value or overflow");
      return;
    end if;
    good := true;
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_hex_token(f, l, ln, cn, tmp, good, eof);
    val := std_ulogic_vector(tmp);
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_hex_token(f, l, ln, cn, tmp, good, eof);
    val := to_integer(tmp);
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned) is
    variable good, eof: boolean;
  begin
    get_hex_token(f, l, ln, cn, val, good, eof);
    if eof then
      syntax_error(l, ln, cn, "unexpected end of file");
    end if;
    assert good and not eof severity failure;
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_hex_token(f, l, ln, cn, tmp);
    val := std_ulogic_vector(tmp);
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_hex_token(f, l, ln, cn, tmp);
    val := to_integer(tmp);
  end procedure get_hex_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
  begin
    next_token(f, l, ln, cn, tmp);
    if tmp then -- End of file
      good := true;
      eof  := true;
      return;
    end if;
    eof := false;
    dec_read(l, cn, val, tmp);
    if not tmp then -- Next token is not decimal or overflow
      good := false;
      syntax_error(l, ln, cn, "not a decimal value or overflow");
      return;
    end if;
    good := true;
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_dec_token(f, l, ln, cn, tmp, good, eof);
    val := std_ulogic_vector(tmp);
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_dec_token(f, l, ln, cn, tmp, good, eof);
    val := to_integer(tmp);
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned) is
    variable good, eof: boolean;
  begin
    get_dec_token(f, l, ln, cn, val, good, eof);
    if eof then
      syntax_error(l, ln, cn, "unexpected end of file");
    end if;
    assert good and not eof severity failure;
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_dec_token(f, l, ln, cn, tmp);
    val := std_ulogic_vector(tmp);
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_dec_token(f, l, ln, cn, tmp);
    val := to_integer(tmp);
  end procedure get_dec_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
  begin
    next_token(f, l, ln, cn, tmp);
    if tmp then -- End of file
      good := true;
      eof  := true;
      return;
    end if;
    eof := false;
    bin_read(l, cn, val, tmp);
    if not tmp then -- Next token is not binary or overflow
      good := false;
      syntax_error(l, ln, cn, "not a binary value or overflow");
      return;
    end if;
    good := true;
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_bin_token(f, l, ln, cn, tmp, good, eof);
    val := std_ulogic_vector(tmp);
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_bin_token(f, l, ln, cn, tmp, good, eof);
    val := to_integer(tmp);
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned) is
    variable good, eof: boolean;
  begin
    get_bin_token(f, l, ln, cn, val, good, eof);
    if eof then
      syntax_error(l, ln, cn, "unexpected end of file");
    end if;
    assert good and not eof severity failure;
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_bin_token(f, l, ln, cn, tmp);
    val := std_ulogic_vector(tmp);
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_bin_token(f, l, ln, cn, tmp);
    val := to_integer(tmp);
  end procedure get_bin_token;

  procedure check(mes: in string; got: in std_ulogic; exp: in std_ulogic) is
  begin
    assert got = exp report mes & ": got " & std2char(got) & ", expected " & std2char(exp) severity failure;
  end procedure check;

  procedure check(mes: in string; got: in std_ulogic; exp: in std_ulogic; msk: in std_ulogic) is
  begin
    assert (got and msk) = (exp and msk) report mes & ": got " & std2char(got) & ", expected " & std2char(exp) & " (mask: " & std2char(msk) & ")" severity failure;
  end procedure check;

  procedure check(mes: in string; got: in std_ulogic_vector; exp: in std_ulogic_vector) is
  begin
    assert got = exp report mes & ": got " & vec2hexstr(got) & ", expected " & vec2hexstr(exp) severity failure;
  end procedure check;

  procedure check(mes: in string; got: in std_ulogic_vector; exp: in std_ulogic_vector; msk: in std_ulogic_vector) is
  begin
    assert (got and msk) = (exp and msk) report mes & ": got " & vec2hexstr(got) & ", expected " & vec2hexstr(exp) & " (mask: " & vec2hexstr(msk) & ")" severity failure;
  end procedure check;

  procedure init(f: inout vci_req_fifo) is
  begin
    f.cnt := 0;
    f.top := null;
    f.bot := null;
  end procedure init;

  procedure push(f: inout vci_req_fifo; req: in vci_request_type) is
    variable tmp: vci_req_fifo_entry_ptr;
  begin
    tmp     := new vci_req_fifo_entry;
    tmp.nxt := null;
    tmp.req := req;
    if f.cnt = 0 then -- empty
      f.top := tmp;
      f.bot := tmp;
      f.cnt := 1;
    else
      f.top.nxt := tmp;
      f.top     := tmp;
      f.cnt     := f.cnt + 1;
    end if;
  end procedure push;

  procedure pop(f: inout vci_req_fifo) is
    variable tmp: vci_req_fifo_entry_ptr;
  begin
    assert f.cnt > 0 report "Empty FIFO" severity failure;
    tmp := f.bot;
    f.bot := tmp.nxt;
    deallocate(tmp);
    f.cnt := f.cnt - 1;
    if f.cnt = 0 then -- empty
      f.top := null;
    end if;
  end procedure pop;

  procedure free(f: inout vci_req_fifo) is
    variable tmp1, tmp2: vci_req_fifo_entry_ptr;
  begin
    tmp1 := f.bot;
    while tmp1 /= null loop
      tmp2 := tmp1.nxt;
      deallocate(tmp1);
      tmp1 := tmp2;
    end loop;
    init(f);
  end procedure free;

  procedure init(f: inout vci_rsp_fifo) is
  begin
    f.cnt := 0;
    f.top := null;
    f.bot := null;
  end procedure init;

  procedure push(f: inout vci_rsp_fifo; rsp: in vci_response_type; msk: in vci_data_type) is
    variable tmp: vci_rsp_fifo_entry_ptr;
  begin
    tmp     := new vci_rsp_fifo_entry;
    tmp.nxt := null;
    tmp.rsp := rsp;
    tmp.msk := msk;
    if f.cnt = 0 then -- empty
      f.top := tmp;
      f.bot := tmp;
      f.cnt := 1;
    else
      f.top.nxt := tmp;
      f.top     := tmp;
      f.cnt     := f.cnt + 1;
    end if;
  end procedure push;

  procedure pop(f: inout vci_rsp_fifo) is
    variable tmp: vci_rsp_fifo_entry_ptr;
  begin
    assert f.cnt > 0 report "Empty FIFO" severity failure;
    tmp := f.bot;
    f.bot := tmp.nxt;
    deallocate(tmp);
    f.cnt := f.cnt - 1;
    if f.cnt = 0 then -- empty
      f.top := null;
    end if;
  end procedure pop;

  procedure free(f: inout vci_rsp_fifo) is
    variable tmp1, tmp2: vci_rsp_fifo_entry_ptr;
  begin
    tmp1 := f.bot;
    while tmp1 /= null loop
      tmp2 := tmp1.nxt;
      deallocate(tmp1);
      tmp1 := tmp2;
    end loop;
    init(f);
  end procedure free;

  procedure init(f: inout vci_data_fifo) is
  begin
    f.cnt := 0;
    f.top := null;
    f.bot := null;
  end procedure init;

  procedure push(f: inout vci_data_fifo; data: in vci_data_type; msk: in vci_data_type) is
    variable tmp: vci_data_fifo_entry_ptr;
  begin
    tmp     := new vci_data_fifo_entry;
    tmp.nxt := null;
    tmp.data := data;
    tmp.msk  := msk;
    if f.cnt = 0 then -- empty
      f.top := tmp;
      f.bot := tmp;
      f.cnt := 1;
    else
      f.top.nxt := tmp;
      f.top     := tmp;
      f.cnt     := f.cnt + 1;
    end if;
  end procedure push;

  procedure pop(f: inout vci_data_fifo) is
    variable tmp: vci_data_fifo_entry_ptr;
  begin
    assert f.cnt > 0 report "Empty FIFO" severity failure;
    tmp := f.bot;
    f.bot := tmp.nxt;
    deallocate(tmp);
    f.cnt := f.cnt - 1;
    if f.cnt = 0 then -- empty
      f.top := null;
    end if;
  end procedure pop;

  procedure free(f: inout vci_data_fifo) is
    variable tmp1, tmp2: vci_data_fifo_entry_ptr;
  begin
    tmp1 := f.bot;
    while tmp1 /= null loop
      tmp2 := tmp1.nxt;
      deallocate(tmp1);
      tmp1 := tmp2;
    end loop;
    init(f);
  end procedure free;

  procedure init(vie: inout vci_initiator_emulator) is
  begin
    init(vie.req_fifo);
    init(vie.rsp_fifo);
  end procedure init;

  procedure free(vie: inout vci_initiator_emulator) is
  begin
    free(vie.req_fifo);
    free(vie.rsp_fifo);
  end procedure free;

  procedure is_empty(variable vie: in vci_initiator_emulator; good: out boolean) is
  begin
    good := vie.req_fifo.cnt = 0 and vie.rsp_fifo.cnt = 0;
  end procedure is_empty;

  procedure play(vie: inout vci_initiator_emulator; signal req: out vci_request_type) is
  begin
    req <= vie.req_fifo.bot.req;
    pop(vie.req_fifo);
  end procedure play;

  procedure check(vie: inout vci_initiator_emulator; rsp: in vci_response_type) is
    variable tmp: vci_rsp_fifo_entry_ptr;
  begin
    tmp := vie.rsp_fifo.bot;
    check("RSPVAL mismatch", rsp.rspval, tmp.rsp.rspval);
    check("RDATA mismatch",  rsp.rdata,  tmp.rsp.rdata, tmp.msk);
    check("RERROR mismatch", rsp.rerror, tmp.rsp.rerror);
    check("REOP mismatch",   rsp.reop,   tmp.rsp.reop);
    check("RSRCID mismatch", rsp.rsrcid, tmp.rsp.rsrcid);
    check("RTRDID mismatch", rsp.rtrdid, tmp.rsp.rtrdid);
    check("RPKTID mismatch", rsp.rpktid, tmp.rsp.rpktid);
    pop(vie.rsp_fifo);
  end procedure check;

  procedure init(vte: inout vci_target_emulator) is
  begin
    init(vte.data_fifo);
    init(vte.rsp_fifo);
  end procedure init;

  procedure free(vte: inout vci_target_emulator) is
  begin
    free(vte.data_fifo);
    free(vte.rsp_fifo);
  end procedure free;

  procedure play(vte: inout vci_target_emulator; signal rsp: out vci_response_type) is
  begin
    rsp <= vte.rsp_fifo.bot.rsp;
    pop(vte.rsp_fifo);
  end procedure play;

  procedure check(vte: inout vci_target_emulator; req: in vci_request_type) is
    variable rsp: vci_response_type := vci_response_none;
  begin
    rsp.rspval := '1';
    if req.cmd = vci_cmd_write then
      check("WDATA mismatch",  req.wdata, vte.data_fifo.bot.data, vte.data_fifo.bot.msk);
    else
      rsp.rdata  := vte.data_fifo.bot.data;
    end if;
    pop(vte.data_fifo);
    rsp.reop   := req.eop;
    rsp.rsrcid := req.srcid;
    rsp.rtrdid := req.trdid;
    rsp.rpktid := req.pktid;
    push(vte.rsp_fifo, rsp, (others => '0'));
  end procedure check;

  procedure read_rs(file f: text; l: inout line; ln: inout natural; cn: inout natural; cnt: out natural) is
  begin
    get_dec_token(f, l, ln, cn, cnt);
  end procedure read_rs;

  procedure read_wr(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator; cst: in boolean := false) is
    variable add: vci_address_type;
    variable len: natural;
    variable req: vci_request_type;
    variable rsp: vci_response_type;
    variable inc: natural;
  begin
    req := vci_request_none;
    req.srcid := vci_srcid_rnd;
    req.trdid := (others => '0');
    req.pktid := (others => '0');
    rsp := vci_response_none;
    rsp.rsrcid := req.srcid;
    rsp.rtrdid := req.trdid;
    rsp.rpktid := req.pktid;
    if cst then
      inc := 0;
    else
      inc := 8;
    end if;
    get_hex_token(f, l, ln, cn, add);
    get_dec_token(f, l, ln, cn, len);
    if len < 1 or len > 2**29 then
      syntax_error(l, ln, cn, "Invalid length");
      assert false severity failure;
    end if;
    req.cmdval  := '1';
    req.cmd     := vci_cmd_write;
    rsp.rspval  := '1';
    for i in 0 to len - 1 loop
      req.address := vci_address_type(u_unsigned(add) + inc * i);
      get_hex_token(f, l, ln, cn, req.wdata);
      get_hex_token(f, l, ln, cn, req.be);
      req.eop := '1';
      rsp.reop := '1';
      push(vie.req_fifo, req);
      push(vie.rsp_fifo, rsp, (others => '0'));
    end loop;
  end procedure read_wr;

  procedure read_rd(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator; cst: in boolean := false) is
    variable add: vci_address_type;
    variable len: natural;
    variable req: vci_request_type;
    variable rsp: vci_response_type;
    variable msk: vci_data_type;
    variable inc: natural;
  begin
    req := vci_request_none;
    req.srcid := vci_srcid_rnd;
    req.pktid := std_ulogic_vector(to_unsigned(1, vci_p));
    req.trdid := (others => '0');
    rsp := vci_response_none;
    rsp.rsrcid := req.srcid;
    rsp.rtrdid := req.trdid;
    rsp.rpktid := req.pktid;
    if cst then
      inc := 0;
    else
      inc := 8;
    end if;
    get_hex_token(f, l, ln, cn, add);
    get_dec_token(f, l, ln, cn, len);
    if len < 1 or len > 2**29 then
      syntax_error(l, ln, cn, "Invalid length");
      assert false severity failure;
    end if;
    req.cmdval  := '1';
    req.cmd     := vci_cmd_read;
    rsp.rspval  := '1';
    for i in 0 to len - 1 loop
      req.address := vci_address_type(u_unsigned(add) + inc * i);
      get_hex_token(f, l, ln, cn, rsp.rdata);
      req.eop := '1';
      rsp.reop := '1';
      get_hex_token(f, l, ln, cn, req.be);
      for i in 7 downto 0 loop
        msk := shift_left(msk, 8);
        if req.be(i) = '1' then
          msk(7 downto 0) := x"ff";
        end if;
      end loop;
      push(vie.req_fifo, req);
      push(vie.rsp_fifo, rsp, msk);
    end loop;
  end procedure read_rd;

  procedure read_qt(file f: text; l: inout line; ln: inout natural; cn: inout natural) is
  begin
  end procedure read_qt;

end package body vci_master_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
