--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Emulator of Control Sub-System (CSS).
--*
--*  Emulator of Control Sub-System (CSS). Used to simulate a PSS + Memory Sub-System pair. The emulator parses a command file and drive simulations.
--* The syntax of the command files is explained in the header comments of the source file of the css_emulator_pkg package (global/css_emulator_pkg.vhd). The
--* targetted simulations are those that simulate only an PSS and its Memory Sub-System. So, the simulation environment emulates the DMA engine, the
--* VCIInterface, the micro-controller and the surrounding system. The emulated DMA engine is used to store incoming data blocks in the MSS and to read data
--* blocks from the MSS. When it reads from the MSS the read values are compared with the expected one and errors are raised on mismatches. The emulated
--* VCIInterface is used to access the MSS as the emulated DMA engine and also to launch processing on the PSS and to get their exit status. The
--* micro-controller is used only to access the MSS.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.global.all;
use work.utils.all;
use work.sim_utils.all;
use work.css_emulator_pkg.all;

use std.textio.all;

entity css_emulator is
  generic(
    cmdsize: positive; -- byte length of the command register
    cmdfile: string    -- name of the command file
  );
  port(
    clk:     out std_ulogic;   -- master clock
    css2pss:  out css2pss_type;  -- output port to PSS
    pss2css:  in  pss2css_type;  -- input port from PSS
    css2mss: out css2mss_type; -- output port to MSS
    mss2css: in  mss2css_type; -- input port from MSS
    -- parameters to PSS
    param:   out std_ulogic_vector(8 * cmdsize - 1 downto 0)
  );
end entity css_emulator;

architecture arc of css_emulator is
  signal end_of_simulation: boolean := false;
  signal clk1, clk2:        std_ulogic;
  signal css2pssl:           css2pss_type;
  signal css2mssl:          css2mss_type;
  signal paraml:            std_ulogic_vector(8 * cmdsize - 1 downto 0);
  signal mss2dma_rdata:     vci_data_type;
  signal mss2vci_rdata:     vci_data_type;

  alias dma2mss is css2mssl.dma2mss;
  alias vci2mss is css2mssl.vci2mss;
  alias uc2mss  is css2mssl.uc2mss;
  alias mss2dma is mss2css.mss2dma;
  alias mss2vci is mss2css.mss2vci;
  alias mss2uc  is mss2css.mss2uc;
  alias exec    is css2pssl.exec;
  alias srstn   is css2pssl.srstn;
  alias ce      is css2pssl.ce;
  alias eoc     is pss2css.eoc;
  alias err     is pss2css.err;
  alias status  is pss2css.status;
  alias data    is pss2css.data;
begin

  clk <= clk2;
  clk1 <= clk2;
  css2pss <= css2pssl;
  css2mss <= css2mssl;
  param <= paraml;

  clock: process
    variable l: line;
  begin
    clk2 <= '0';
    wait for 1 ns;
    clk2 <= '1';
    wait for 1 ns;
    if end_of_simulation then
      for i in 1 to 10 loop
        clk2 <= '0';
        wait for 1 ns;
        clk2 <= '1';
        wait for 1 ns;
      end loop;
      write(l, string'("*** End of simulation @ "));
      write(l, now);
      writeline(output, l);
      write(l, string'("*** Regression test PASSED"));
      writeline(output, l);
      wait;
    end if;
  end process;

  mss_wrapper: process(clk1)
    variable dps1, dps2, vps1, vps2: vci_be_type := (others => '0');
  begin
    if falling_edge(clk1) then
      for i in 7 downto 0 loop
        if dps2(i) = '1' then
          mss2dma_rdata(8 * i + 7 downto 8 * i) <= mss2dma.rdata(8 * i + 7 downto 8 * i);
        end if;
        if vps2(i) = '1' then
          mss2vci_rdata(8 * i + 7 downto 8 * i) <= mss2vci.rdata(8 * i + 7 downto 8 * i);
        end if;
      end loop;
      dps2 := dps1;
      vps2 := vps1;
      if dma2mss.en = '1' and dma2mss.rnw = '1' then
        dps1 := dma2mss.be and mss2dma.gnt;
      end if;
      if vci2mss.en = '1' and vci2mss.rnw = '1' then
        vps1 := vci2mss.be and mss2vci.gnt;
      end if;
    end if;
  end process mss_wrapper;

  emu: process

    type state_type is (idle, resetting, counting, wait_dma, wait_vci, wait_uc, wait_pss, ending);

    variable state: state_type;
    variable dreq, vreq: dmavci_request_type;
    variable ureq: uc_request_type;
    variable ireq: pss_request_type;
    variable n, np: natural;
    variable ln, cn: natural;
    variable l: line;
    variable cmd: cmd_type;
    variable good, eof: boolean;
    variable fops: file_open_status;
    variable paramv: std_ulogic_vector(8 * cmdsize - 1 downto 0);
    variable prev_srstn: boolean;
    variable dma, vci, uc, pss, pssl: boolean;
    variable add: word64_address_type;

    file f: text; 

  begin
    srstn <= '0';
    ce <= '1';
    prev_srstn := false;
    css2mssl <= (vci2mss => dma2mss_none, dma2mss => dma2mss_none,
                  uc2mss => uc2mss_none);
    exec <= '0';
    paraml <= (others => '0');
    ireq := (status => (others => '0'), err => '0');
    paramv := (others => '0');
    dreq := (rdata => (others => (others => '0')),
             rdata_valid => (others => false), cmd => RD, add => (others => '0'),
             msk => x"00", n => 0, pld => null);
    vreq := (rdata => (others => (others => '0')),
             rdata_valid => (others => false), cmd => VR, add => (others => '0'),
             msk => x"00", n => 0, pld => null);
    ureq := (rdata => (others => (others => '0')),
             rdata_valid => (others => false), cmd => UR, add => (others => '0'),
             n => 0, pld => null);
    state := idle;
    file_open(fops, f, cmdfile, read_mode);
    ln := 1;
    cn := 1;
    n := 0;
    dma := false;
    vci := false;
    uc := false;
    pss := false;
    pssl := false;
    np := 0;
    assert fops = open_ok
      report "File " & cmdfile & " not found"
      severity failure;
    loop
      wait until rising_edge(clk1);
      exec <= '0';
      -- check PSS behavior against synchronous active low reset
      assert (not prev_srstn) or
             (eoc = '0' and or_reduce(status) = '0' and err = '0' and or_reduce(data) = '0')
        report "PSS reset ignored: eoc=" & std2char(eoc) &
               ", status=" & vec2hexstr(status) & ", err=" &
               std2char(err) & ", data=" &vec2hexstr(data)
        severity failure;
      if srstn = '0' then
        prev_srstn := true;
      else
        prev_srstn := false;
      end if;
      -- check out of range accesses to memory subsystem
      assert mss2dma.oor = '0'
        report "DMA: out of range memory access (" &
               integer'image(to_integer(u_unsigned(dma2mss.add))) & ")"
        severity failure;
      assert mss2vci.oor = '0'
        report "VCI: out of range memory access (" &
               integer'image(to_integer(u_unsigned(vci2mss.add))) & ")"
        severity failure;
      assert mss2uc.oor = '0'
        report "UC: out of range memory access (" &
               integer'image(to_integer(u_unsigned(uc2mss.add))) & ")"
        severity failure;
      -- check exec requests to PSS against PSS status and synchronous
      -- active low reset
      if exec = '1' then
        np := np + 1;
        assert not pssl
          report "Processing launched while PSS busy"
          severity failure;
        assert srstn = '1'
          report "Processing launched while PSS reset"
          severity failure;
        deallocate(l);
        write(l, string'("Processing #"));
        write(l, np);
        write(l, string'(" launched at "));
        write(l, now);
        write(l, string'(" with parameter = "));
        write(l, vec2hexstr(paraml));
        writeline(output, l);
        pssl := true;
        pss := true;
      end if;
      -- check end of computation from PSS against PSS status and
      -- expected exit status and error flag
      if eoc = '1' then
        assert pssl
          report "Processing ends while PSS not busy"
          severity failure;
        assert ireq.status = status and ireq.err = err
          report "PSS: return status/error does not match expected one: got " &
                 vec2hexstr(status) & "/" & std2char(err) & ", expected " &
                 vec2hexstr(ireq.status) & "/" & std2char(ireq.err)
          severity failure;
        pssl := false;
        pss := false;
        deallocate(l);
        write(l, string'("Processing #"));
        write(l, np);
        write(l, string'(" terminated at "));
        write(l, now);
        write(l, string'(" with exit status = "));
        write(l, vec2hexstr(status));
        write(l, string'(", error flag = "));
        write(l, std2char(err));
        write(l, string'(" and data = "));
        write(l, vec2hexstr(data));
        writeline(output, l);
      end if;
      -- check DMA-read value from MSS against expected value
      if dreq.rdata_valid(0) then
        assert masked_check(dreq.msk, dreq.rdata(0), mss2dma_rdata)
          report "DMA: output value does not match expected one: got " &
                 vec2hexstr(mss2dma_rdata) & ", expected " &
                 vec2hexstr(dreq.rdata(0)) &
                 ", (mask " & vec2hexstr(dreq.msk) & ")"
          severity failure;
      end if;
      -- advance DMA-read pipeline
      dreq.rdata_valid(0) := dreq.rdata_valid(1);
      dreq.rdata_valid(1) := false;
      dreq.rdata(0) := dreq.rdata(1);
      -- if DMA transfer in progress and current access granted
      if dma2mss.en = '1' then
        for i in 7 downto 0 loop -- For each byte
          if mss2dma.gnt(i) = '1' and dma2mss.be(i) = '1' then -- If byte enabled and granted
            dma2mss.be(i) <= '0'; -- Stop requesting this byte
          end if;
        end loop;
        if (mss2dma.gnt and dma2mss.be) = dma2mss.be then -- If all enabled bytes are granted, end of request
          if dreq.cmd = RD or dreq.cmd = DR then -- if DMA read
            dreq.rdata(1) := dreq.pld(dreq.n - 1); -- update DMA-read pipeline
            dreq.rdata_valid(1) := true;
          end if;
          if dreq.n = dreq.pld'length then -- if end of DMA transfer
            dreq.n := 0; -- reset DMA request
            deallocate(dreq.pld);
            dma2mss <= dma2mss_none;
            dma := false;
          else -- continue DMA transfer
            add := word64_address_type(u_unsigned(dreq.add) + dreq.n);
            if dreq.cmd = WR or dreq.cmd = DW then -- if DMA write
              write(add, dreq.pld(dreq.n), dreq.msk, dma2mss);
            else -- DMA read
              read(add, dma2mss);
            end if;
            dreq.n := dreq.n + 1; -- increment DMA counter
            dma := true;
          end if;
        end if;
      end if;
      -- check VCI-read value from MSS against expected value
      if vreq.rdata_valid(0) then
        assert masked_check(vreq.msk, vreq.rdata(0), mss2vci_rdata)
          report "VCI: output value does not match expected one: got " &
                 vec2hexstr(mss2vci_rdata) & ", expected " &
                 vec2hexstr(vreq.rdata(0)) &
                 ", (mask " & vec2hexstr(vreq.msk) & ")"
          severity failure;
      end if;
      -- advance VCI-read pipeline
      vreq.rdata_valid(0) := vreq.rdata_valid(1);
      vreq.rdata_valid(1) := false;
      vreq.rdata(0) := vreq.rdata(1);
      -- if VCI transfer in progress and current access granted
      if vci2mss.en = '1' then
        for i in 7 downto 0 loop -- For each byte
          if mss2vci.gnt(i) = '1' and vci2mss.be(i) = '1' then -- If byte enabled and granted
            vci2mss.be(i) <= '0'; -- Stop requesting this byte
          end if;
        end loop;
        if (mss2vci.gnt and vci2mss.be) = vci2mss.be then
          if vreq.cmd = VR then -- if VCI read
            vreq.rdata(1) := vreq.pld(vreq.n - 1); -- update VCI-read pipeline
            vreq.rdata_valid(1) := true;
          end if;
          if vreq.n = vreq.pld'length then -- if end of VCI transfer
            vreq.n := 0; -- reset VCI request
            deallocate(vreq.pld);
            vci2mss <= dma2mss_none;
            vci := false;
          else -- continue VCI transfer
            add := word64_address_type(u_unsigned(vreq.add) + vreq.n);
            if vreq.cmd = VW then -- if VCI write
              write(add, vreq.pld(vreq.n), vreq.msk, vci2mss);
            else -- VCI read
              read(add, vci2mss);
            end if;
            vreq.n := vreq.n + 1; -- increment VCI counter
            vci := true;
          end if;
        end if;
      end if;
      -- check UC-read value from MSS against expected value
      if ureq.rdata_valid(0) then
        assert ureq.rdata(0) = mss2uc.rdata
          report "UC: output value does not match expected one: got " &
                 vec2hexstr(mss2uc.rdata) & ", expected " &
                 vec2hexstr(ureq.rdata(0))
          severity failure;
      end if;
      -- advance UC-read pipeline
      ureq.rdata_valid(0) := ureq.rdata_valid(1);
      ureq.rdata_valid(1) := false;
      ureq.rdata(0) := ureq.rdata(1);
      -- if UC transfer in progress and current access granted
      if mss2uc.gnt = X"FF" and uc2mss.en = '1' then
        if ureq.cmd = UR then -- if UC read
          ureq.rdata(1) := ureq.pld(ureq.n - 1); -- update UC-read pipeline
          ureq.rdata_valid(1) := true;
        end if;
        if ureq.n = ureq.pld'length then -- if end of UC transfer
          ureq.n := 0; -- reset UC request
          deallocate(ureq.pld);
          uc2mss <= uc2mss_none;
          uc := false;
        else -- continue UC transfer
          add(15 downto 0) := uc_address_type(u_unsigned(ureq.add) + ureq.n);
          if ureq.cmd = UW then -- if UC write
            write(add(15 downto 0), ureq.pld(ureq.n), uc2mss);
          else -- UC read
            read(add(15 downto 0), uc2mss);
          end if;
          ureq.n := ureq.n + 1; -- increment UC counter
          uc := true;
        end if;
      end if;
      case state is
        when resetting =>
          if n = 1 then
            srstn <= '1';
            state := idle;
          else
            n := n - 1;
          end if;
        when counting =>
          if n = 1 then
           state := idle;
          else
            n := n - 1;
          end if;
        when wait_dma =>
          if not dma then
            state := idle;
          elsif n > 1 then
            n := n - 1;
          elsif n = 1 then
            assert false
              report "Timeout exceeded while waiting for DMA"
              severity failure;
          end if;
        when wait_vci =>
          if not vci then
            state := idle;
          elsif n > 1 then
            n := n - 1;
          elsif n = 1 then
            assert false
              report "Timeout exceeded while waiting for VCI"
              severity failure;
          end if;
        when wait_uc =>
          if not uc then
            state := idle;
          elsif n > 1 then
            n := n - 1;
          elsif n = 1 then
            assert false
              report "Timeout exceeded while waiting for UC"
              severity failure;
          end if;
        when wait_pss =>
          if not pss then
            state := idle;
          elsif n > 1 then
            n := n - 1;
          elsif n = 1 then
            assert false
              report "Timeout exceeded while waiting for PSS"
              severity failure;
          end if;
        when ending =>
          if not (or_reduce(dreq.rdata_valid) or or_reduce(vreq.rdata_valid) or or_reduce(ureq.rdata_valid)) then
            file_close(f);
            deallocate(dreq.pld);
            deallocate(vreq.pld);
            deallocate(ureq.pld);
            end_of_simulation <= true;
            wait;
          end if;
        when idle => null;
      end case;
      while state = idle loop
        read(f, ln, cn, l, cmd, good, eof);
        if eof then
          cmd := QT;
        else
          assert good
            report "*** " & cmdfile & ":" & integer'image(ln) & ":" &
              integer'image(cn) & ": syntax error: invalid command" &
              CR & "*** """ & l.all & """"
            severity failure;
        end if;
        case cmd is
          when RS =>
            srstn <= '0';
            nread(f, ln, cn, l, n, good);
            assert good and n > 0
              report "*** " & cmdfile & ":" & integer'image(ln) & ":" &
                integer'image(cn) &
                ": syntax error: invalid number of reset cycles" &
                CR & "*** """ & l.all & """"
              severity failure;
            state := resetting;
          when WR | DW | RD | DR =>
            if dma then
              state := wait_dma;
              n := 0;
              cn := cn - 2;
            else
              read(f, ln, cn, l, dreq, good);
              assert good
                report "*** " & cmdfile & ":" & integer'image(ln) & ":" &
                  integer'image(cn) &
                  ": syntax error: could not read DMA request" &
                  CR & "*** """ & l.all & """"
                severity failure;
              dreq.cmd := cmd;
              add := word64_address_type(u_unsigned(dreq.add) + dreq.n);
              if dreq.cmd = WR or dreq.cmd = DW then -- if DMA write
                write(add, dreq.pld(dreq.n), dreq.msk, dma2mss);
              else -- DMA read
                read(add, dma2mss);
              end if;
              dreq.n := dreq.n + 1; -- increment DMA counter
              dma := true;
            end if;
          when VR | VW =>
            if vci then
              state := wait_vci;
              n := 0;
              cn := cn - 2;
            else
              read(f, ln, cn, l, vreq, good);
              assert good
                report "*** " & cmdfile & ":" & integer'image(ln) & ":" &
                  integer'image(cn) &
                  ": syntax error: could not read VCI request" &
                  CR & "*** """ & l.all & """"
                severity failure;
              vreq.cmd := cmd;
              add := word64_address_type(u_unsigned(vreq.add) + vreq.n);
              if vreq.cmd = VW then -- if VCI write
                write(add, vreq.pld(dreq.n), vreq.msk, vci2mss);
              else -- VCI read
                read(add, vci2mss);
              end if;
              vreq.n := vreq.n + 1; -- increment VCI counter
              vci := true;
            end if;
          when UR | UW =>
            if uc then
              state := wait_uc;
              n := 0;
              cn := cn - 2;
            else
              read(f, ln, cn, l, ureq, good);
              assert good
                report "*** " & cmdfile & ":" & integer'image(ln) & ":" &
                  integer'image(cn) &
                  ": syntax error: could not read UC request" &
                  CR & "*** """ & l.all & """"
                severity failure;
              ureq.cmd := cmd;
              add(15 downto 0) := uc_address_type(u_unsigned(ureq.add) + ureq.n);
              if ureq.cmd = WR or ureq.cmd = DW then -- if UC write
                write(add(15 downto 0), ureq.pld(dreq.n), uc2mss);
              else -- UC read
                read(add(15 downto 0), uc2mss);
              end if;
              ureq.n := ureq.n + 1; -- increment UC counter
              uc := true;
            end if;
          when WD =>
            nread(f, ln, cn, l, n, good);
            assert good
              report "*** " & cmdfile & ":" & integer'image(ln) & ":" &
                integer'image(cn) &
                ": syntax error: could not read timeout" &
                CR & "*** """ & l.all & """"
              severity failure;
            if dma then
              state := wait_dma;
            end if;
          when WV =>
            nread(f, ln, cn, l, n, good);
            assert good
              report "*** " & cmdfile & ":" & integer'image(ln) & ":" &
                integer'image(cn) &
                ": syntax error: could not read timeout" &
                CR & "*** """ & l.all & """"
              severity failure;
            if vci then
              state := wait_vci;
            end if;
          when WU =>
            nread(f, ln, cn, l, n, good);
            assert good
              report "*** " & cmdfile & ":" & integer'image(ln) & ":" &
                integer'image(cn) &
                ": syntax error: could not read timeout" &
                CR & "*** """ & l.all & """"
              severity failure;
            if uc then
              state := wait_uc;
            end if;
          when EX =>
            if pss then
              state := wait_pss;
              cn := cn - 2;
            else
              hread(f, ln, cn, l, ireq.status, good);
              assert good
                report "*** " & cmdfile & ":" & integer'image(ln) & ":" & integer'image(cn) & ": syntax error: could not read exit status" & CR & "*** """ &
                       l.all & """"
                severity failure;
              nread(f, ln, cn, l, n, good);
              assert good and (n = 0 or n = 1)
                report "*** " & cmdfile & ":" & integer'image(ln) & ":" & integer'image(cn) & ": syntax error: could not read error flag" & CR & "*** """ &
                       l.all & """"
                severity failure;
              if n = 0 then
                ireq.err := '0';
              elsif n = 1 then
                ireq.err := '1';
              end if;
              for i in 0 to cmdsize / 8 - 1 loop
                paramv := shift_left(paramv, 64);
                hread(f, ln, cn, l, paramv(63 downto 0), good);
                assert good
                  report "*** " & cmdfile & ":" & integer'image(ln) & ":" & integer'image(cn) & ": syntax error: could not read exec parameter" & CR &
                         "*** """ & l.all & """"
                  severity failure;
              end loop;
              paraml <= paramv;
              exec <= '1';
              pss := true;
            end if;
          when WE =>
            nread(f, ln, cn, l, n, good);
            assert good
              report "*** " & cmdfile & ":" & integer'image(ln) & ":" &
                integer'image(cn) &
                ": syntax error: could not read timeout" &
                CR & "*** """ & l.all & """"
              severity failure;
            if pss then
              state := wait_pss;
            end if;
          when WC =>
            nread(f, ln, cn, l, n, good);
            assert good and n > 0
              report "*** " & cmdfile & ":" & integer'image(ln) & ":" &
                integer'image(cn) &
                ": syntax error: invalid number of cycles" &
                CR & "*** """ & l.all & """"
              severity failure;
            state := counting;
          when QT =>
            state := ending;
        end case;
      end loop;
    end loop;
  end process emu;

end architecture arc;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
