--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- pragma translate_off
library random_lib;
use random_lib.rnd.all;

use work.sim_utils.all;
-- pragma translate_on

package axi_pkg is

    generic(
        axi_m: positive := 4;  -- strb bit width
        axi_a: positive := 32; -- address bit width
        axi_i: positive := 6   -- id  bit width
    );

    -----------------------------------------------------
    -- Bit-widths of AXI fields at the PL-PS interface --
    -----------------------------------------------------

    -- Common to all AXI interfaces
    constant axi_l: positive := 8;  -- len bit width
    constant axi_b: positive := 2;  -- burst bit width
    constant axi_o: positive := 1;  -- lock bit width
    constant axi_p: positive := 3;  -- prot bit width
    constant axi_c: positive := 4;  -- cache bit width
    constant axi_r: positive := 2;  -- resp bit width
    constant axi_q: positive := 4;  -- qos bit width
    constant axi_s: positive := 3;  -- size bit width

    constant axi_resp_okay:   std_ulogic_vector(axi_r - 1 downto 0) := "00";
    constant axi_resp_exokay: std_ulogic_vector(axi_r - 1 downto 0) := "01";
    constant axi_resp_slverr: std_ulogic_vector(axi_r - 1 downto 0) := "10";
    constant axi_resp_decerr: std_ulogic_vector(axi_r - 1 downto 0) := "11";

    constant axi_burst_fixed: std_ulogic_vector(axi_b - 1 downto 0) := "00";
    constant axi_burst_incr:  std_ulogic_vector(axi_b - 1 downto 0) := "01";
    constant axi_burst_wrap:  std_ulogic_vector(axi_b - 1 downto 0) := "10";
    constant axi_burst_res:   std_ulogic_vector(axi_b - 1 downto 0) := "11";

    constant axi_d: positive := 8 * axi_m; -- data bit width

    -----------------------------------------------------------
    -- AXI ports. M2S: Master to slave. S2M: Slave to master --
    -----------------------------------------------------------

    type axi_m2s is record
        -- Read address channel
        arid:    std_ulogic_vector(axi_i - 1 downto 0);
        araddr:  std_ulogic_vector(axi_a - 1 downto 0);
        arlen:   std_ulogic_vector(axi_l - 1 downto 0);
        arsize:  std_ulogic_vector(axi_s - 1 downto 0);
        arburst: std_ulogic_vector(axi_b - 1 downto 0);
        arlock:  std_ulogic_vector(axi_o - 1 downto 0);
        arcache: std_ulogic_vector(axi_c - 1 downto 0);
        arprot:  std_ulogic_vector(axi_p - 1 downto 0);
        arqos:   std_ulogic_vector(axi_q - 1 downto 0);
        arvalid: std_ulogic;
        -- Read data channel
        rready:  std_ulogic;
        -- Write address channel
        awid:    std_ulogic_vector(axi_i - 1 downto 0);
        awaddr:  std_ulogic_vector(axi_a - 1 downto 0);
        awlen:   std_ulogic_vector(axi_l - 1 downto 0);
        awsize:  std_ulogic_vector(axi_s - 1 downto 0);
        awburst: std_ulogic_vector(axi_b - 1 downto 0);
        awlock:  std_ulogic_vector(axi_o - 1 downto 0);
        awcache: std_ulogic_vector(axi_c - 1 downto 0);
        awprot:  std_ulogic_vector(axi_p - 1 downto 0);
        awqos:   std_ulogic_vector(axi_q - 1 downto 0);
        awvalid: std_ulogic;
        -- Write data channel
        wdata:   std_ulogic_vector(axi_d - 1 downto 0);
        wstrb:   std_ulogic_vector(axi_m - 1 downto 0);
        wlast:   std_ulogic;
        wvalid:  std_ulogic;
        -- Write response channel
        bready:  std_ulogic;
    end record;

    constant axi_m2s_none: axi_m2s := (
        arid    => (others => '0'),
        araddr  => (others => '0'),
        arlen   => (others => '0'),
        arsize  => (others => '0'),
        arburst => (others => '0'),
        arlock  => (others => '0'),
        arcache => (others => '0'),
        arprot  => (others => '0'),
        arqos   => (others => '0'),
        arvalid => '0',
        rready  => '0',
        awid    => (others => '0'),
        awaddr  => (others => '0'),
        awlen   => (others => '0'),
        awsize  => (others => '0'),
        awburst => (others => '0'),
        awlock  => (others => '0'),
        awcache => (others => '0'),
        awprot  => (others => '0'),
        awqos   => (others => '0'),
        awvalid => '0',
        wdata   => (others => '0'),
        wstrb   => (others => '0'),
        wlast   => '0',
        wvalid  => '0',
        bready  => '0'
    );

    type axi_s2m is record
    -- Read address channel
        arready: std_ulogic;
    -- Read data channel
        rid:     std_ulogic_vector(axi_i - 1 downto 0);
        rdata:   std_ulogic_vector(axi_d - 1 downto 0);
        rresp:   std_ulogic_vector(axi_r - 1 downto 0);
        rlast:   std_ulogic;
        rvalid:  std_ulogic;
    -- Write address channel
        awready: std_ulogic;
    -- Write data channel
        wready:  std_ulogic;
    -- Write response channel
        bid:     std_ulogic_vector(axi_i - 1 downto 0);
        bvalid:  std_ulogic;
        bresp:   std_ulogic_vector(axi_r - 1 downto 0);
    end record;

    constant axi_s2m_none: axi_s2m := (
        arready => '0',
        rid     => (others => '0'),
        rdata   => (others => '0'),
        rresp   => (others => '0'),
        rlast   => '0',
        rvalid  => '0',
        awready => '0',
        wready  => '0',
        bid     => (others => '0'),
        bvalid  => '0',
        bresp   => (others => '0')
    );

    -- AXI ports (lite version)
    type axilite_m2s is record
        -- Read address channel
        araddr:  std_ulogic_vector(axi_a - 1 downto 0);
        arprot:  std_ulogic_vector(axi_p - 1 downto 0);
        arvalid: std_ulogic;
  -- Read data channel
        rready:  std_ulogic;
  -- Write address channel
        awaddr:  std_ulogic_vector(axi_a - 1 downto 0);
        awprot:  std_ulogic_vector(axi_p - 1 downto 0);
        awvalid: std_ulogic;
  -- Write data channel
        wdata:   std_ulogic_vector(axi_d - 1 downto 0);
        wstrb:   std_ulogic_vector(axi_m - 1 downto 0);
        wvalid:  std_ulogic;
  -- Write response channel
        bready:  std_ulogic;
    end record;

    constant axilite_m2s_none: axilite_m2s := (
        araddr  => (others => '0'),
        arprot  => (others => '0'),
        arvalid => '0',
        rready  => '0',
        awaddr  => (others => '0'),
        awprot  => (others => '0'),
        awvalid => '0',
        wdata   => (others => '0'),
        wstrb   => (others => '0'),
        wvalid  => '0',
        bready  => '0'
    );

  type axilite_s2m is record
        -- Read address channel
  arready: std_ulogic;
        -- Read data channel
  rdata:   std_ulogic_vector(axi_d - 1 downto 0);
  rresp:   std_ulogic_vector(axi_r - 1 downto 0);
  rvalid:  std_ulogic;
        -- Write address channel
  awready: std_ulogic;
        -- Write data channel
  wready:  std_ulogic;
        -- Write response channel
  bvalid:  std_ulogic;
  bresp:   std_ulogic_vector(axi_r - 1 downto 0);
    end record;

  constant axilite_s2m_none: axilite_s2m := (
  arready => '0',
  rdata   => (others => '0'),
  rresp   => (others => '0'),
  rvalid  => '0',
  awready => '0',
  wready  => '0',
  bvalid  => '0',
  bresp   => (others => '0')
  );

    ---------------------------------
    -- Split versions for AXI ports --
    ---------------------------------

    -- Address channels
  type axi_m2s_a is record
  aid:    std_ulogic_vector(axi_i - 1 downto 0);
  aaddr:  std_ulogic_vector(axi_a - 1 downto 0);
  alen:   std_ulogic_vector(axi_l - 1 downto 0);
  asize:  std_ulogic_vector(axi_s - 1 downto 0);
  aburst: std_ulogic_vector(axi_b - 1 downto 0);
  alock:  std_ulogic_vector(axi_o - 1 downto 0);
  acache: std_ulogic_vector(axi_c - 1 downto 0);
  aprot:  std_ulogic_vector(axi_p - 1 downto 0);
  aqos:   std_ulogic_vector(axi_q - 1 downto 0);
  avalid: std_ulogic;
    end record;

  constant axi_m2s_a_none: axi_m2s_a := (
  aid    => (others => '0'),
  aaddr  => (others => '0'),
  alen   => (others => '0'),
  asize  => (others => '0'),
  aburst => (others => '0'),
  alock  => (others => '0'),
  acache => (others => '0'),
  aprot  => (others => '0'),
  aqos   => (others => '0'),
  avalid => '0'
  );

  type axi_m2s_ar is record
  arid:    std_ulogic_vector(axi_i - 1 downto 0);
  araddr:  std_ulogic_vector(axi_a - 1 downto 0);
  arlen:   std_ulogic_vector(axi_l - 1 downto 0);
  arsize:  std_ulogic_vector(axi_s - 1 downto 0);
  arburst: std_ulogic_vector(axi_b - 1 downto 0);
  arlock:  std_ulogic_vector(axi_o - 1 downto 0);
  arcache: std_ulogic_vector(axi_c - 1 downto 0);
  arprot:  std_ulogic_vector(axi_p - 1 downto 0);
  arqos:   std_ulogic_vector(axi_q - 1 downto 0);
  arvalid: std_ulogic;
    end record;

  constant axi_m2s_ar_none: axi_m2s_ar :=  (
  arid    => (others => '0'),
  araddr  => (others => '0'),
  arlen   => (others => '0'),
  arsize  => (others => '0'),
  arburst => (others => '0'),
  arlock  => (others => '0'),
  arcache => (others => '0'),
  arprot  => (others => '0'),
  arqos   => (others => '0'),
  arvalid => '0'
  );

  type axi_s2m_ar is record
  arready: std_ulogic;
    end record;

    constant axi_s2m_ar_none: axi_s2m_ar := (arready => '0');

    type axi_m2s_aw is record
    awid:    std_ulogic_vector(axi_i - 1 downto 0);
    awaddr:  std_ulogic_vector(axi_a - 1 downto 0);
    awlen:   std_ulogic_vector(axi_l - 1 downto 0);
    awsize:  std_ulogic_vector(axi_s - 1 downto 0);
    awburst: std_ulogic_vector(axi_b - 1 downto 0);
    awlock:  std_ulogic_vector(axi_o - 1 downto 0);
    awcache: std_ulogic_vector(axi_c - 1 downto 0);
    awprot:  std_ulogic_vector(axi_p - 1 downto 0);
    awqos:   std_ulogic_vector(axi_q - 1 downto 0);
    awvalid: std_ulogic;
end record;

constant axi_m2s_aw_none: axi_m2s_aw := (
awid    => (others => '0'),
awaddr  => (others => '0'),
awlen   => (others => '0'),
awsize  => (others => '0'),
awburst => (others => '0'),
awlock  => (others => '0'),
awcache => (others => '0'),
awprot  => (others => '0'),
awqos   => (others => '0'),
awvalid => '0'
  );

  type axi_s2m_aw is record
  awready: std_ulogic;
    end record;

    constant axi_s2m_aw_none: axi_s2m_aw := (awready => '0');

    -- Data channels
    type axi_s2m_r is record
    rid:      std_ulogic_vector(axi_i - 1 downto 0);
    rdata:    std_ulogic_vector(axi_d - 1 downto 0);
    rresp:    std_ulogic_vector(axi_r - 1 downto 0);
    rlast:    std_ulogic;
    rvalid:   std_ulogic;
end record;

constant axi_s2m_r_none: axi_s2m_r := (
rid    => (others => '0'),
rdata  => (others => '0'),
rresp  => (others => '0'),
rlast  => '0',
rvalid => '0'
  );

  type axi_m2s_r is record
  rready:   std_ulogic;
    end record;

    constant axi_m2s_r_none: axi_m2s_r := (rready => '0');

    type axi_m2s_w is record
    wdata:    std_ulogic_vector(axi_d - 1 downto 0);
    wstrb:    std_ulogic_vector(axi_m - 1 downto 0);
    wlast:    std_ulogic;
    wvalid:   std_ulogic;
end record;

constant axi_m2s_w_none: axi_m2s_w := (
wdata  => (others => '0'),
wstrb  => (others => '0'),
wlast  => '0',
wvalid => '0'
  );

  type axi_s2m_w is record
  wready:   std_ulogic;
    end record;

    constant axi_s2m_w_none: axi_s2m_w := (wready => '0');

    -- Response channels
    type axi_s2m_b is record
    bid:      std_ulogic_vector(axi_i - 1 downto 0);
    bresp:    std_ulogic_vector(axi_r - 1 downto 0);
    bvalid:   std_ulogic;
end record;

constant axi_s2m_b_none: axi_s2m_b := (
bid    => (others => '0'),
bresp  => (others => '0'),
bvalid => '0'
  );

  type axi_m2s_b is record
  bready:   std_ulogic;
    end record;

    constant axi_m2s_b_none: axi_m2s_b := (bready => '0');
    -- End response channels

    type axi_m2s_split is record                 --Master to slave
    ar: axi_m2s_ar;   --Read address channel
    aw: axi_m2s_aw;  --Write address channel
    r: axi_m2s_r;      --Read data channel
    w: axi_m2s_w;     --Write data channel
    b: axi_m2s_b; --Write response channel
end record;

constant axi_m2s_split_none: axi_m2s_split := (
ar => axi_m2s_ar_none,
aw => axi_m2s_aw_none,
r => axi_m2s_r_none,
w => axi_m2s_w_none,
b => axi_m2s_b_none
  );

  type axi_s2m_split is record                 --Slave to master
  ar: axi_s2m_ar;   --Read address channel
  aw: axi_s2m_aw;  --Write address channel
  r: axi_s2m_r;      --Read data channel
  w: axi_s2m_w;     --Write data channel
  b: axi_s2m_b; --Write response channel
    end record;

  constant axi_s2m_split_none: axi_s2m_split := (
  ar => axi_s2m_ar_none,
  aw => axi_s2m_aw_none,
  r => axi_s2m_r_none,
  w => axi_s2m_w_none,
  b => axi_s2m_b_none
  );

  constant address_length:        natural := axi_i + axi_a + axi_l + axi_s + axi_b + axi_o + axi_c + axi_p + axi_q + 1;
  constant read_address_length:   natural := axi_i + axi_a + axi_l + axi_s + axi_b + axi_o + axi_c + axi_p + axi_q + 1;
  constant write_address_length:  natural := axi_i + axi_a + axi_l + axi_s + axi_b + axi_o + axi_c + axi_p + axi_q + 1;
  constant read_data_length:      natural := axi_i + axi_d + axi_r + 1 + 1;
  constant write_data_length:     natural := axi_d + axi_m + 1 + 1;
  constant write_response_length: natural := axi_i + axi_r + 1;

    -- Channel to vector functions

  function to_std_ulogic_vector(a: axi_m2s_a) return std_ulogic_vector;
  function to_std_ulogic_vector(ar: axi_m2s_ar) return std_ulogic_vector;
  function to_std_ulogic_vector(aw: axi_m2s_aw) return std_ulogic_vector;

  function to_axi_m2s_a(v: std_ulogic_vector) return axi_m2s_a;
  function to_axi_m2s_ar(v: std_ulogic_vector) return axi_m2s_ar;
  function to_axi_m2s_aw(v: std_ulogic_vector) return axi_m2s_aw;

  function to_std_ulogic_vector(r: axi_s2m_r) return std_ulogic_vector;

  function to_axi_s2m_r(v: std_ulogic_vector) return axi_s2m_r;

  function to_std_ulogic_vector(w: axi_m2s_w) return std_ulogic_vector;

  function to_axi_m2s_w(v: std_ulogic_vector) return axi_m2s_w;

  function to_std_ulogic_vector(b: axi_s2m_b) return std_ulogic_vector;

  function to_axi_s2m_b(v: std_ulogic_vector) return axi_s2m_b;

  function to_axi_m2s_ar(v: axi_m2s_a) return axi_m2s_ar;
  function to_axi_m2s_aw(v: axi_m2s_a) return axi_m2s_aw;
  function to_axi_m2s_a(v: axi_m2s_ar) return axi_m2s_a;
  function to_axi_m2s_a(v: axi_m2s_aw) return axi_m2s_a;

  function to_axi_m2s_split(v: axi_m2s) return axi_m2s_split;
  function to_axi_m2s(v: axi_m2s_split) return axi_m2s;
  function to_axi_s2m_split(v: axi_s2m) return axi_s2m_split;
  function to_axi_s2m(v: axi_s2m_split) return axi_s2m;

  procedure mask(d: inout std_ulogic_vector; n: std_ulogic_vector; b: std_ulogic_vector);
  procedure mask_s(signal d: inout std_ulogic_vector; n: std_ulogic_vector; b: std_ulogic_vector);

-- pragma translate_off
    impure function axilite_m2s_rnd return axilite_m2s;
    impure function axilite_s2m_rnd return axilite_s2m;

    procedure write(l: inout line; r: in axilite_m2s);
    procedure write(l: inout line; r: in axilite_s2m);

    function check(a, b: axilite_m2s) return boolean;
    function check(a, b: axilite_s2m) return boolean;
-- pragma translate_on

end package axi_pkg;

package body axi_pkg is

    function to_std_ulogic_vector(a: axi_m2s_a) return std_ulogic_vector is
    begin
        return a.aid & a.aaddr & a.alen & a.asize & a.aburst & a.alock & a.acache & a.aprot & a.aqos & a.avalid;
    end to_std_ulogic_vector;

    function to_axi_m2s_a(v: std_ulogic_vector) return axi_m2s_a is

    variable a:   axi_m2s_a;
    variable tmp: natural;

    begin

        a.avalid := v(0);
        tmp := 1;
        a.aqos  := v(tmp + axi_q - 1 downto tmp);
        tmp := tmp + axi_q;
        a.aprot  := v(tmp + axi_p - 1 downto tmp);
        tmp := tmp + axi_p;
        a.acache := v(tmp + axi_c - 1 downto tmp);
        tmp := tmp + axi_c;
        a.alock  := v(tmp + axi_o - 1 downto tmp);
        tmp := tmp + axi_o;
        a.aburst := v(tmp + axi_b - 1 downto tmp);
        tmp := tmp + axi_b;
        a.asize := v(tmp + axi_s - 1 downto tmp);
        tmp := tmp + axi_s;
        a.alen := v(tmp + axi_l - 1 downto tmp);
        tmp := tmp + axi_l;
        a.aaddr := v(tmp + axi_a - 1 downto tmp);
        tmp := tmp + axi_a;
        a.aid := v(tmp + axi_i - 1 downto tmp);

        return a;

    end to_axi_m2s_a;

    function to_std_ulogic_vector(ar: axi_m2s_ar) return std_ulogic_vector is
    begin
        return ar.arid & ar.araddr & ar.arlen & ar.arsize & ar.arburst & ar.arlock & ar.arcache & ar.arprot & ar.arqos & ar.arvalid;
    end to_std_ulogic_vector;

    function to_axi_m2s_ar(v: std_ulogic_vector) return axi_m2s_ar is

    variable ar: axi_m2s_ar;
    variable tmp: natural;

    begin

        ar.arvalid := v(0);
        tmp := 1;
        ar.arqos  := v(tmp + axi_q - 1 downto tmp);
        tmp := tmp + axi_q;
        ar.arprot  := v(tmp + axi_p - 1 downto tmp);
        tmp := tmp + axi_p;
        ar.arcache := v(tmp + axi_c - 1 downto tmp);
        tmp := tmp + axi_c;
        ar.arlock  := v(tmp + axi_o - 1 downto tmp);
        tmp := tmp + axi_o;
        ar.arburst := v(tmp + axi_b - 1 downto tmp);
        tmp := tmp + axi_b;
        ar.arsize := v(tmp + axi_s - 1 downto tmp);
        tmp := tmp + axi_s;
        ar.arlen := v(tmp + axi_l - 1 downto tmp);
        tmp := tmp + axi_l;
        ar.araddr := v(tmp + axi_a - 1 downto tmp);
        tmp := tmp + axi_a;
        ar.arid := v(tmp + axi_i - 1 downto tmp);

        return ar;

    end to_axi_m2s_ar;

    function to_std_ulogic_vector(aw: axi_m2s_aw) return std_ulogic_vector is
    begin
        return  aw.awid & aw.awaddr & aw.awlen & aw.awsize & aw.awburst & aw.awlock & aw.awcache & aw.awprot & aw.awqos & aw.awvalid;
    end to_std_ulogic_vector;

    function to_axi_m2s_aw (v: std_ulogic_vector) return axi_m2s_aw is

    variable aw: axi_m2s_aw;
    variable tmp: natural;

    begin

        aw.awvalid := v(0);
        tmp := 1;
        aw.awqos  := v(tmp + axi_q - 1 downto tmp);
        tmp := tmp + axi_q;
        aw.awprot  := v(tmp + axi_p - 1 downto tmp);
        tmp := tmp + axi_p;
        aw.awcache := v(tmp + axi_c - 1 downto tmp);
        tmp := tmp + axi_c;
        aw.awlock  := v(tmp + axi_o - 1 downto tmp);
        tmp := tmp + axi_o;
        aw.awburst := v(tmp + axi_b - 1 downto tmp);
        tmp := tmp + axi_b;
        aw.awsize := v(tmp + axi_s - 1 downto tmp);
        tmp := tmp + axi_s;
        aw.awlen := v(tmp + axi_l - 1 downto tmp);
        tmp := tmp + axi_l;
        aw.awaddr := v(tmp + axi_a - 1 downto tmp);
        tmp := tmp + axi_a;
        aw.awid := v(tmp + axi_i - 1 downto tmp);

        return aw;

    end to_axi_m2s_aw;

    function to_std_ulogic_vector(r: axi_s2m_r) return std_ulogic_vector is
    begin
        return r.rid & r.rdata & r.rresp & r.rlast & r.rvalid;
    end to_std_ulogic_vector;

    function to_axi_s2m_r(v: std_ulogic_vector) return axi_s2m_r is

    variable r: axi_s2m_r;
    variable tmp: natural;

    begin

        r.rvalid := v(0);
        tmp := 1;
        r.rlast := v(1);
        tmp := 2;
        r.rresp := v(tmp + axi_r - 1 downto tmp);
        tmp := tmp + axi_r;
        r.rdata := v(tmp + axi_d - 1 downto tmp);
        tmp := tmp + axi_d;
        r.rid := v(tmp + axi_i - 1 downto tmp);

        return r;

    end to_axi_s2m_r;

    function to_std_ulogic_vector(w: axi_m2s_w) return std_ulogic_vector is
    begin
        return w.wdata & w.wstrb & w.wlast & w.wvalid;
    end to_std_ulogic_vector;

    function to_axi_m2s_w(v: std_ulogic_vector) return axi_m2s_w is

    variable w: axi_m2s_w;
    variable tmp: natural;

    begin

        w.wvalid := v(0);
        tmp := 1;
        w.wlast := v(1);
        tmp := 2;
        w.wstrb := v(tmp + axi_m - 1 downto tmp);
        tmp := tmp + axi_m;
        w.wdata := v(tmp + axi_d - 1 downto tmp);

        return w;

    end to_axi_m2s_w;

    function to_std_ulogic_vector(b: axi_s2m_b) return std_ulogic_vector is
    begin
        return b.bid & b.bresp & b.bvalid;
    end to_std_ulogic_vector;

    function to_axi_s2m_b(v: std_ulogic_vector) return axi_s2m_b is

    variable b: axi_s2m_b;
    variable tmp: natural;

    begin

        b.bvalid := v(0);
        tmp := 1;
        b.bresp := v(tmp + axi_r - 1 downto tmp);
        tmp := tmp + axi_r;
        b.bid := v(tmp + axi_i - 1 downto tmp);

        return b;

    end to_axi_s2m_b;

    function to_axi_m2s_ar(v: axi_m2s_a) return axi_m2s_ar is
    variable r: axi_m2s_ar;
    begin
        r.arid    := v.aid;
        r.araddr  := v.aaddr;
        r.arlen   := v.alen;
        r.arsize  := v.asize;
        r.arburst := v.aburst;
        r.arlock  := v.alock;
        r.arcache := v.acache;
        r.arprot  := v.aprot;
        r.arqos   := v.aqos;
        r.arvalid := v.avalid;
        return r;
    end function to_axi_m2s_ar;

    function to_axi_m2s_aw(v: axi_m2s_a) return axi_m2s_aw is
    variable r: axi_m2s_aw;
    begin
        r.awid    := v.aid;
        r.awaddr  := v.aaddr;
        r.awlen   := v.alen;
        r.awsize  := v.asize;
        r.awburst := v.aburst;
        r.awlock  := v.alock;
        r.awcache := v.acache;
        r.awprot  := v.aprot;
        r.awqos   := v.aqos;
        r.awvalid := v.avalid;
        return r;
    end function to_axi_m2s_aw;

    function to_axi_m2s_a(v: axi_m2s_ar) return axi_m2s_a is
    variable r: axi_m2s_a;
    begin
        r.aid    := v.arid;
        r.aaddr  := v.araddr;
        r.alen   := v.arlen;
        r.asize  := v.arsize;
        r.aburst := v.arburst;
        r.alock  := v.arlock;
        r.acache := v.arcache;
        r.aprot  := v.arprot;
        r.aqos   := v.arqos;
        r.avalid := v.arvalid;
        return r;
    end function to_axi_m2s_a;

    function to_axi_m2s_a(v: axi_m2s_aw) return axi_m2s_a is
    variable r: axi_m2s_a;
    begin
        r.aid    := v.awid;
        r.aaddr  := v.awaddr;
        r.alen   := v.awlen;
        r.asize  := v.awsize;
        r.aburst := v.awburst;
        r.alock  := v.awlock;
        r.acache := v.awcache;
        r.aprot  := v.awprot;
        r.aqos   := v.awqos;
        r.avalid := v.awvalid;
        return r;
    end function to_axi_m2s_a;

    function to_axi_m2s_split(v: axi_m2s) return axi_m2s_split is
    variable r: axi_m2s_split;
    begin
        r.ar.arid    := v.arid;
        r.ar.araddr  := v.araddr;
        r.ar.arlen   := v.arlen;
        r.ar.arsize  := v.arsize;
        r.ar.arburst := v.arburst;
        r.ar.arlock  := v.arlock;
        r.ar.arcache := v.arcache;
        r.ar.arprot  := v.arprot;
        r.ar.arqos   := v.arqos;
        r.ar.arvalid := v.arvalid;
        r.r.rready   := v.rready;
        r.aw.awid    := v.awid;
        r.aw.awaddr  := v.awaddr;
        r.aw.awlen   := v.awlen;
        r.aw.awsize  := v.awsize;
        r.aw.awburst := v.awburst;
        r.aw.awlock  := v.awlock;
        r.aw.awcache := v.awcache;
        r.aw.awprot  := v.awprot;
        r.aw.awqos   := v.awqos;
        r.aw.awvalid := v.awvalid;
        r.w.wdata    := v.wdata;
        r.w.wstrb    := v.wstrb;
        r.w.wlast    := v.wlast;
        r.w.wvalid   := v.wvalid;
        r.b.bready   := v.bready;
        return r;
    end function to_axi_m2s_split;

    function to_axi_m2s(v: axi_m2s_split) return axi_m2s is
    variable r: axi_m2s;
    begin
        r.arid    := v.ar.arid;
        r.araddr  := v.ar.araddr;
        r.arlen   := v.ar.arlen;
        r.arsize  := v.ar.arsize;
        r.arburst := v.ar.arburst;
        r.arlock  := v.ar.arlock;
        r.arcache := v.ar.arcache;
        r.arprot  := v.ar.arprot;
        r.arqos   := v.ar.arqos;
        r.arvalid := v.ar.arvalid;
        r.rready  := v.r.rready;
        r.awid    := v.aw.awid;
        r.awaddr  := v.aw.awaddr;
        r.awlen   := v.aw.awlen;
        r.awsize  := v.aw.awsize;
        r.awburst := v.aw.awburst;
        r.awlock  := v.aw.awlock;
        r.awcache := v.aw.awcache;
        r.awprot  := v.aw.awprot;
        r.awqos   := v.aw.awqos;
        r.awvalid := v.aw.awvalid;
        r.wdata   := v.w.wdata;
        r.wstrb   := v.w.wstrb;
        r.wlast   := v.w.wlast;
        r.wvalid  := v.w.wvalid;
        r.bready  := v.b.bready;
        return r;
    end function to_axi_m2s;

    function to_axi_s2m_split(v: axi_s2m) return axi_s2m_split is
    variable r: axi_s2m_split;
    begin
        r.ar.arready := v.arready;
        r.r.rid      := v.rid;
        r.r.rdata    := v.rdata;
        r.r.rresp    := v.rresp;
        r.r.rlast    := v.rlast;
        r.r.rvalid   := v.rvalid;
        r.aw.awready := v.awready;
        r.w.wready   := v.wready;
        r.b.bid      := v.bid;
        r.b.bvalid   := v.bvalid;
        r.b.bresp    := v.bresp;
        return r;
    end function to_axi_s2m_split;

    function to_axi_s2m(v: axi_s2m_split) return axi_s2m is
    variable r: axi_s2m;
    begin
        r.arready := v.ar.arready;
        r.rid     := v.r.rid;
        r.rdata   := v.r.rdata;
        r.rresp   := v.r.rresp;
        r.rlast   := v.r.rlast;
        r.rvalid  := v.r.rvalid;
        r.awready := v.aw.awready;
        r.wready  := v.w.wready;
        r.bid     := v.b.bid;
        r.bvalid  := v.b.bvalid;
        r.bresp   := v.b.bresp;
        return r;
    end function to_axi_s2m;

    procedure mask(d: inout std_ulogic_vector; n: std_ulogic_vector; b: std_ulogic_vector) is
                   variable dv: std_ulogic_vector(d'length - 1 downto 0) := d;
                   variable nv: std_ulogic_vector(d'length - 1 downto 0) := n;
                   variable bv: std_ulogic_vector(d'length / 8 - 1 downto 0) := b;
    begin
        for i in 0 to 3 loop
            if bv(i) = '1' then
                dv(8 * i + 7 downto 8 * i) := nv(8 * i + 7 downto 8 * i);
            end if;
        end loop;
        d := dv;
    end procedure mask;

    procedure mask_s(signal d: inout std_ulogic_vector; n: std_ulogic_vector; b: std_ulogic_vector) is
                     variable dv: std_ulogic_vector(d'length - 1 downto 0) := d;
                     variable nv: std_ulogic_vector(d'length - 1 downto 0) := n;
                     variable bv: std_ulogic_vector(d'length / 8 - 1 downto 0) := b;
    begin
        for i in 0 to 3 loop
            if bv(i) = '1' then
                dv(8 * i + 7 downto 8 * i) := nv(8 * i + 7 downto 8 * i);
            end if;
        end loop;
        d <= dv;
    end procedure mask_s;

-- pragma translate_off
    impure function axilite_m2s_rnd return axilite_m2s is
    variable res: axilite_m2s;
    begin
        res.awprot  := std_ulogic_vector_rnd(axi_p);
        res.arprot  := std_ulogic_vector_rnd(axi_p);
        res.awvalid := std_ulogic_rnd;
        res.wvalid  := std_ulogic_rnd;
        res.arvalid := std_ulogic_rnd;
        res.rready  := std_ulogic_rnd;
        res.bready  := std_ulogic_rnd;
        res.wdata   := std_ulogic_vector_rnd(axi_d);
        res.wstrb   := std_ulogic_vector_rnd(axi_m);
        res.awaddr  := std_ulogic_vector_rnd(axi_a);
        res.araddr  := std_ulogic_vector_rnd(axi_a);
        return res;
    end function axilite_m2s_rnd;

    impure function axilite_s2m_rnd return axilite_s2m is
    variable res: axilite_s2m;
    begin
        res.awready := std_ulogic_rnd;
        res.wready  := std_ulogic_rnd;
        res.bvalid  := std_ulogic_rnd;
        res.arready := std_ulogic_rnd;
        res.rvalid  := std_ulogic_rnd;
        res.bresp   := std_ulogic_vector_rnd(axi_r);
        res.rresp   := std_ulogic_vector_rnd(axi_r);
        res.rdata   := std_ulogic_vector_rnd(axi_d);
        return res;
    end function axilite_s2m_rnd;

    procedure write(l: inout line; r: in axilite_m2s) is
    begin
        if r.awvalid = '1' then
            write(l, string'("AW @ 0x"));
            hwrite(l, r.awaddr);
            write(l, string'(" WPROT "));
            write(l, to_integer(u_unsigned(r.awprot)));
        else
            write(l, string'("                       "));
        end if;
        write(l, string'(" | "));
        if r.wvalid = '1' then
            write(l, string'("WDATA 0x"));
            hwrite(l, r.wdata);
            write(l, string'(" WSTRB 0x"));
            hwrite(l, r.wstrb);
        else
            write(l, string'("                           "));
        end if;
        write(l, string'(" | "));
        if r.arvalid = '1' then
            write(l, string'("AR @ 0x"));
            hwrite(l, r.araddr);
            write(l, string'(" RPROT "));
            write(l, to_integer(u_unsigned(r.arprot)));
        else
            write(l, string'("                       "));
        end if;
        write(l, string'(" | "));
        if r.bready = '1' then
            write(l, string'("BREADY"));
        else
            write(l, string'("      "));
        end if;
        write(l, string'(" | "));
        if r.rready = '1' then
            write(l, string'("RREADY"));
        else
            write(l, string'("      "));
        end if;
    end procedure write;

    procedure write(l: inout line; r: in axilite_s2m) is
    begin
        if r.rvalid = '1' then
            write(l, string'("RDATA 0x"));
            hwrite(l, r.rdata);
            write(l, string'(" RRESP "));
            write(l, to_integer(u_unsigned(r.rresp)));
        else
            write(l, string'("                "));
        end if;
        write(l, string'(" | "));
        if r.bvalid = '1' then
            write(l, string'("BVALID "));
            write(l, string'(" BRESP "));
            write(l, to_integer(u_unsigned(r.bresp)));
        else
            write(l, string'("               "));
        end if;
        write(l, string'(" | "));
        if r.awready = '1' then
            write(l, string'("AWREADY"));
        else
            write(l, string'("       "));
        end if;
        write(l, string'(" | "));
        if r.wready = '1' then
            write(l, string'("WREADY"));
        else
            write(l, string'("      "));
        end if;
        write(l, string'(" | "));
        if r.arready = '1' then
            write(l, string'("ARREADY"));
        else
            write(l, string'("       "));
        end if;
    end procedure write;

    function check(a, b: axilite_m2s) return boolean is
    begin
        return check(a.awprot, b.awprot) and
        check(a.arprot, b.arprot) and
        check(a.awvalid, b.awvalid) and
        check(a.wvalid, b.wvalid) and
        check(a.arvalid, b.arvalid) and
        check(a.rready, b.rready) and
        check(a.bready, b.bready) and
        check(a.wdata, b.wdata) and
        check(a.wstrb, b.wstrb) and
        check(a.awaddr, b.awaddr) and
        check(a.araddr, b.araddr);
    end function check;

    function check(a, b: axilite_s2m) return boolean is
    begin
        return check(a.awready, b.awready) and
        check(a.wready, b.wready) and
        check(a.bvalid, b.bvalid) and
        check(a.arready, b.arready) and
        check(a.rvalid, b.rvalid) and
        check(a.bresp, b.bresp) and
        check(a.rresp, b.rresp) and
        check(a.rdata, b.rdata);
    end function check;
-- pragma translate_on

end package body axi_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
