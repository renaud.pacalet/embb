--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

use work.bvci64_pkg.all;

entity bvci64_target is
  generic(
    debug: boolean                := false; -- Print debug information
    na:    positive range 4 to 16 := 16     -- Virtual RAM is 2**(na-3) x 64 bits
  );
  port(
    clk:     in  std_ulogic;
    srstn:   in  std_ulogic;
    vci_i2t: in  bvci64_i2t_type;
    vci_t2i: out bvci64_t2i_type
  );
end entity bvci64_target;

architecture rtl of bvci64_target is

  signal vci_t2i_l: bvci64_t2i_type;

begin

  vci_t2i <= vci_t2i_l;

  process(clk)
    type bvci64_data_vector is array(natural range <>) of bvci64_data_type;
    variable ram: bvci64_data_vector(0 to 2**na - 1) := (others => (others => '-'));
    variable r: bvci64_t2i_type;
    variable l: line;
    variable f: bvci64_t2i_fifo;
    variable add: natural range 0 to 2**(na - 3);
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        vci_t2i_l <= bvci64_t2i_none;
        init(f);
        ram := (others => (others => '-'));
      else
        vci_t2i_l.cmdack <= std_ulogic_rnd;
        if vci_t2i_l.cmdack = '1' and vci_i2t.cmdval = '1' then
          add := to_integer(u_unsigned(vci_i2t.address(na - 1 downto 3)));
          r := (rdata => (others => '-'), rspval => '1', rerror => '0', others => '-');
          if vci_i2t.cmd = bvci64_cmd_write then
            mask(ram(add), vci_i2t.wdata, vci_i2t.be);
          elsif vci_i2t.cmd /= bvci64_cmd_nop then
            mask(r.rdata, ram(add), vci_i2t.be);
          end if;
          push(f, r);
          if debug then
            write(l, vci_i2t);
            writeline(output, l);
          end if;
        end if;
        if vci_t2i_l.rspval = '0' or vci_i2t.rspack = '1' then
          if debug and vci_t2i_l.rspval = '1' then
            write(l, vci_t2i_l);
            writeline(output, l);
          end if;
          if f.cnt /= 0 and boolean_rnd then
            pop(f, r);
            vci_t2i_l <= r;
            vci_t2i_l.cmdack <= vci_t2i_l.cmdack;
            if debug then
              write(l, r);
              writeline(output, l);
            end if;
          else
            vci_t2i_l.rspval <= '0';
          end if;
        end if;
      end if;
    end if;
  end process;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
