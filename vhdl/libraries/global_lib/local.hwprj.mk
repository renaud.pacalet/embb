#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

gh-IGNORE	+= global_lib.fifo global_lib.axi4lite32_master global_lib.axi4lite32_register_sim

global_lib.ahb_master: \
	global_lib.global \
	global_lib.sim_utils \
	global_lib.ahb_pkg \
	global_lib.ahb_master_pkg

global_lib.ahb_master_pkg: \
	global_lib.global \
	global_lib.sim_utils \
	global_lib.ahb_pkg \
	global_lib.utils

global_lib.afifo: \
	global_lib.utils \
	global_lib.global

global_lib.afifo_tb: \
	random_lib.rnd \
	global_lib.afifo \
	global_lib.utils \
	global_lib.global

global_lib.axi4lite32_master: \
	global_lib.fifo_pkg \
	global_lib.axi_pkg_m4

global_lib.axi4lite32_register: \
	global_lib.axi_pkg_m4

global_lib.axi4lite32_register2: \
	global_lib.axi_pkg_m4 \
	global_lib.utils

global_lib.axi4lite32_register_sim: \
	global_lib.sim_utils \
	global_lib.axi4lite32_master \
	global_lib.axi_pkg_m4 \
	global_lib.axi4lite32_register

global_lib.axi_master: \
	global_lib.global \
	global_lib.sim_utils \
	global_lib.axi_pkg_m8 \
	global_lib.axi_master_pkg

global_lib.axi_master_pkg: \
	global_lib.sim_utils \
	global_lib.fifo_pkg \
	global_lib.axi_pkg_m8 \
	global_lib.utils

global_lib.bvci64_initiator: \
	global_lib.bvci64_pkg

global_lib.axi_pkg: \
	random_lib.rnd \
	global_lib.sim_utils

global_lib.axi_pkg_m4: \
	global_lib.axi_pkg

global_lib.axi_pkg_m8: \
	global_lib.axi_pkg

global_lib.bvci64_pkg: \
	random_lib.rnd \
	global_lib.global \
	global_lib.sim_utils

global_lib.bvci64_register: \
	global_lib.bvci64_pkg \
	global_lib.utils

global_lib.bvci64_register_sim: \
	global_lib.bvci64_pkg \
	global_lib.sim_utils \
	global_lib.bvci64_initiator \
	global_lib.bvci64_register

global_lib.bvci64_target: \
	random_lib.rnd \
	global_lib.bvci64_pkg

global_lib.css_emulator: \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils \
	global_lib.css_emulator_pkg

global_lib.css_emulator_pkg: \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils

global_lib.css_emulator_sim: \
	global_lib.global \
	global_lib.css_emulator

global_lib.fifo :\
	global_lib.global

global_lib.hst_emulator: \
	random_lib.rnd \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils \
	global_lib.hst_emulator_pkg

global_lib.hst_emulator_2: \
	random_lib.rnd \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils \
	global_lib.hst_emulator_2_pkg

global_lib.hst_emulator_2_pkg: \
	random_lib.rnd \
	css_lib.css_pkg \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils

global_lib.hst_emulator_pkg: \
	random_lib.rnd \
	css_lib.css_pkg \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils

global_lib.reset_generator: \
	global_lib.utils

global_lib.sim_utils: \
	random_lib.rnd \
	global_lib.global \
	global_lib.utils

global_lib.sim_utils_sim: \
	random_lib.rnd \
	global_lib.global \
	global_lib.sim_utils

global_lib.utils: \
	global_lib.global

global_lib.vci_initiator: \
	random_lib.rnd \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils

global_lib.vci_master: \
	global_lib.global \
	global_lib.sim_utils \
	global_lib.vci_master_pkg

global_lib.vci_master_pkg: \
	global_lib.global \
	global_lib.sim_utils \
	global_lib.utils

global_lib.vci_target: \
	random_lib.rnd \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
