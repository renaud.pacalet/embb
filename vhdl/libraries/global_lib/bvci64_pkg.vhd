--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

-- pragma translate_off
use std.textio.all;

library random_lib;
use random_lib.rnd.all;

use work.sim_utils.all;
-- pragma translate_on

library ieee;
use ieee.std_logic_1164.all;

use work.global.all;

package bvci64_pkg is

  subtype bvci64_address_type is std_ulogic_vector(31 downto 0);
  subtype bvci64_be_type is std_ulogic_vector(7 downto 0);
  subtype bvci64_data_type is std_ulogic_vector(63 downto 0);
  subtype bvci64_cmd_type is std_ulogic_vector(1 downto 0);
  constant bvci64_cmd_nop: bvci64_cmd_type := "00";
  constant bvci64_cmd_read: bvci64_cmd_type := "01";
  constant bvci64_cmd_write: bvci64_cmd_type := "10";
  constant bvci64_cmd_locked_read: bvci64_cmd_type := "11";

  -- Initiator to target. Useful address bits are LSBs with:
  --   - 3 LSBs ignored
  --   - number of used bits depend on range of address space
  type bvci64_i2t_type is record
    cmdval, rspack: std_ulogic;
    address: bvci64_address_type;
    be: bvci64_be_type;
    cmd: bvci64_cmd_type;
    wdata: bvci64_data_type;
  end record;
  constant bvci64_i2t_none: bvci64_i2t_type := (address => (others => '0'), be => (others => '0'), cmd => bvci64_cmd_nop, wdata => (others => '0'),
    others => '0');

  -- Target to initiator. rerror is one bit only.
  type bvci64_t2i_type is record
    cmdack, rspval, rerror: std_ulogic;
    rdata: bvci64_data_type;
  end record;
  constant bvci64_t2i_none: bvci64_t2i_type := (rdata => (others => '0'), others => '0');

  function bvci64_i2t_to_vci_i2t(i2t: bvci64_i2t_type) return vci_i2t_type;
  function vci_t2i_to_bvci64_t2i(t2i: vci_t2i_type) return bvci64_t2i_type;

  procedure mask(d: inout bvci64_data_type; n: bvci64_data_type; b: bvci64_be_type);
  procedure mask_s(signal d: inout bvci64_data_type; n: bvci64_data_type; b: bvci64_be_type);

  -- pragma translate_off

  impure function bvci64_address_rnd return bvci64_address_type;
  impure function bvci64_be_rnd return bvci64_be_type;
  impure function bvci64_cmd_rnd return bvci64_cmd_type;
  impure function bvci64_data_rnd return bvci64_data_type;
  impure function bvci64_i2t_rnd return bvci64_i2t_type;
  impure function bvci64_t2i_rnd return bvci64_t2i_type;
  procedure write(l: inout line; r: in bvci64_i2t_type);
  procedure write(l: inout line; r: in bvci64_t2i_type);

  type bvci64_t2i_fifo_entry;
  type bvci64_t2i_fifo_entry_ptr is access bvci64_t2i_fifo_entry;
  type bvci64_t2i_fifo_entry is record
    t2i: bvci64_t2i_type;
    nxt, prv: bvci64_t2i_fifo_entry_ptr;
  end record bvci64_t2i_fifo_entry;
  type bvci64_t2i_fifo is record
    head, tail: bvci64_t2i_fifo_entry_ptr;
    cnt: natural;
  end record bvci64_t2i_fifo;
  procedure init(f: inout bvci64_t2i_fifo);
  procedure free(f: inout bvci64_t2i_fifo);
  procedure push(f: inout bvci64_t2i_fifo; t2i: in bvci64_t2i_type);
  procedure pop(f: inout bvci64_t2i_fifo; t2i: out bvci64_t2i_type);

  type bvci64_i2t_fifo_entry;
  type bvci64_i2t_fifo_entry_ptr is access bvci64_i2t_fifo_entry;
  type bvci64_i2t_fifo_entry is record
    i2t: bvci64_i2t_type;
    nxt, prv: bvci64_i2t_fifo_entry_ptr;
  end record bvci64_i2t_fifo_entry;
  type bvci64_i2t_fifo is record
    head, tail: bvci64_i2t_fifo_entry_ptr;
    cnt: natural;
  end record bvci64_i2t_fifo;
  procedure init(f: inout bvci64_i2t_fifo);
  procedure free(f: inout bvci64_i2t_fifo);
  procedure push(f: inout bvci64_i2t_fifo; i2t: in bvci64_i2t_type);
  procedure pop(f: inout bvci64_i2t_fifo; i2t: out bvci64_i2t_type);

  function check(a, b: bvci64_i2t_type) return boolean;
  function check(a, b: bvci64_t2i_type) return boolean;

  -- pragma translate_on

end package bvci64_pkg;

package body bvci64_pkg is

  function bvci64_i2t_to_vci_i2t(i2t: bvci64_i2t_type) return vci_i2t_type is
    variable res: vci_i2t_type;
  begin
    res.req.cmdval  := i2t.cmdval;
    res.req.address := i2t.address;
    res.req.be      := i2t.be;
    res.req.eop     := '1';
    res.req.cmd     := i2t.cmd;
    res.req.wdata   := i2t.wdata;
    res.req.srcid   := (others => '0');
    res.req.trdid   := (others => '0');
    res.req.pktid   := (others => '0');
    res.rspack      := i2t.rspack;
    return res;
  end function bvci64_i2t_to_vci_i2t;

  function vci_t2i_to_bvci64_t2i(t2i: vci_t2i_type) return bvci64_t2i_type is
    variable res: bvci64_t2i_type;
  begin
    res.cmdack := t2i.cmdack;
    res.rspval := t2i.rsp.rspval;
    res.rerror := t2i.rsp.rerror(0);
    res.rdata  := t2i.rsp.rdata;
    return res;
  end function vci_t2i_to_bvci64_t2i;

  procedure mask(d: inout bvci64_data_type; n: bvci64_data_type; b: bvci64_be_type) is
  begin
    for i in 0 to 7 loop
      if b(i) = '1' then
        d(8 * i + 7 downto 8 * i) := n(8 * i + 7 downto 8 * i);
      end if;
    end loop;
  end procedure mask;

  procedure mask_s(signal d: inout bvci64_data_type; n: bvci64_data_type; b: bvci64_be_type) is
  begin
    for i in 0 to 7 loop
      if b(i) = '1' then
        d(8 * i + 7 downto 8 * i) <= n(8 * i + 7 downto 8 * i);
      end if;
    end loop;
  end procedure mask_s;

  -- pragma translate_off
  impure function bvci64_address_rnd return bvci64_address_type is
  begin
    return std_ulogic_vector_rnd(bvci64_address_type'length);
  end function bvci64_address_rnd;

  impure function bvci64_be_rnd return bvci64_be_type is
  begin
    return std_ulogic_vector_rnd(bvci64_be_type'length);
  end function bvci64_be_rnd;

  impure function bvci64_cmd_rnd return bvci64_cmd_type is
  begin
    return std_ulogic_vector_rnd(bvci64_cmd_type'length);
  end function bvci64_cmd_rnd;

  impure function bvci64_data_rnd return bvci64_data_type is
  begin
    return std_ulogic_vector_rnd(bvci64_data_type'length);
  end function bvci64_data_rnd;

  impure function bvci64_i2t_rnd return bvci64_i2t_type is
    variable res: bvci64_i2t_type;
  begin
    res.cmdval  := std_ulogic_rnd;
    res.rspack  := std_ulogic_rnd;
    res.address := bvci64_address_rnd;
    res.be      := bvci64_be_rnd;
    res.cmd     := bvci64_cmd_rnd;
    res.wdata   := bvci64_data_rnd;
    return res;
  end function bvci64_i2t_rnd;

  impure function bvci64_t2i_rnd return bvci64_t2i_type is
    variable res: bvci64_t2i_type;
  begin
    res.cmdack  := std_ulogic_rnd;
    res.rspval  := std_ulogic_rnd;
    res.rerror  := std_ulogic_rnd;
    res.rdata   := bvci64_data_rnd;
    return res;
  end function bvci64_t2i_rnd;

  procedure write(l: inout line; r: in bvci64_i2t_type) is
  begin
    if r.cmdval = '1' then
      case r.cmd is
        when bvci64_cmd_nop =>
          write(l, string'("NOP"));
        when bvci64_cmd_read =>
          write(l, string'("READ @ 0x"));
          hwrite(l, r.address);
          write(l, string'(" BE 0x"));
          hwrite(l, r.be);
        when bvci64_cmd_write =>
          write(l, string'("WRITE 0x"));
          hwrite(l, r.wdata);
          write(l, string'(" @ 0x"));
          hwrite(l, r.address);
          write(l, string'(" BE 0x"));
          hwrite(l, r.be);
        when bvci64_cmd_locked_read =>
          write(l, string'("LOCKED READ @ 0x"));
          hwrite(l, r.address);
          write(l, string'(" BE 0x"));
          hwrite(l, r.be);
        when others =>
          write(l, string'("COMMAND UNDEFINED"));
      end case;
    end if;
    if r.rspack = '1' then
      if r.cmdval = '1' then
        write(l, string'(" | "));
      end if;
      write(l, string'("RESPONSE ACKNOWLEDGED"));
    end if;
  end procedure write;

  procedure write(l: inout line; r: in bvci64_t2i_type) is
  begin
    if r.rspval = '1' then
      write(l, string'("RESPONSE 0x"));
      hwrite(l, r.rdata);
      if r.rerror = '1' then
        write(l, string'(" (ERROR)"));
      end if;
    end if;
    if r.cmdack = '1' then
      if r.rspval = '1' then
        write(l, string'(" | "));
      end if;
      write(l, string'("REQUEST ACKNOWLEDGED"));
    end if;
  end procedure write;

  procedure init(f: inout bvci64_t2i_fifo) is
  begin
    if f.cnt /= 0 then
      free(f);
    end if;
    f.head := null;
    f.tail := null;
    f.cnt := 0;
  end procedure init;

  procedure free(f: inout bvci64_t2i_fifo) is
    variable r: bvci64_t2i_type;
  begin
    while f.cnt /= 0 loop
      pop(f, r);
    end loop;
  end procedure free;

  procedure push(f: inout bvci64_t2i_fifo; t2i: in bvci64_t2i_type) is
    variable p: bvci64_t2i_fifo_entry_ptr;
  begin
    p := new bvci64_t2i_fifo_entry;
    p.t2i := t2i;
    p.prv := null;
    if f.cnt = 0 then
      p.nxt := null;
      f.head := p;
      f.tail := p;
    else
      p.nxt := f.head;
      f.head.prv := p;
      f.head := p;
    end if;
    f.cnt := f.cnt + 1;
  end procedure push;

  procedure pop(f: inout bvci64_t2i_fifo; t2i: out bvci64_t2i_type) is
    variable p: bvci64_t2i_fifo_entry_ptr;
  begin
    assert f.cnt /= 0 report "Cannot pop empty T2I FIFO" severity failure;
    p := f.tail;
    t2i := p.t2i;
    f.tail := p.prv;
    if f.tail = null then
      f.head := null;
    else
      f.tail.nxt := null;
    end if;
    deallocate(p);
    f.cnt := f.cnt - 1;
  end procedure pop;

  procedure init(f: inout bvci64_i2t_fifo) is
  begin
    if f.cnt /= 0 then
      free(f);
    end if;
    f.head := null;
    f.tail := null;
    f.cnt := 0;
  end procedure init;

  procedure free(f: inout bvci64_i2t_fifo) is
    variable r: bvci64_i2t_type;
  begin
    while f.cnt /= 0 loop
      pop(f, r);
    end loop;
  end procedure free;

  procedure push(f: inout bvci64_i2t_fifo; i2t: in bvci64_i2t_type) is
    variable p: bvci64_i2t_fifo_entry_ptr;
  begin
    p := new bvci64_i2t_fifo_entry;
    p.i2t := i2t;
    p.prv := null;
    if f.cnt = 0 then
      p.nxt := null;
      f.head := p;
      f.tail := p;
    else
      p.nxt := f.head;
      f.head.prv := p;
      f.head := p;
    end if;
    f.cnt := f.cnt + 1;
  end procedure push;

  procedure pop(f: inout bvci64_i2t_fifo; i2t: out bvci64_i2t_type) is
    variable p: bvci64_i2t_fifo_entry_ptr;
  begin
    assert f.cnt /= 0 report "Cannot pop empty I2T FIFO" severity failure;
    p := f.tail;
    i2t := p.i2t;
    f.tail := p.prv;
    if f.tail = null then
      f.head := null;
    else
      f.tail.nxt := null;
    end if;
    deallocate(p);
    f.cnt := f.cnt - 1;
  end procedure pop;

  function check(a, b: bvci64_i2t_type) return boolean is
  begin
    return check(a.cmdval, b.cmdval) and check(a.rspack, b.rspack) and check(a.address, b.address) and check(a.be, b.be) and check(a.cmd, b.cmd) and
           check(a.wdata, b.wdata);
  end function check;

  function check(a, b: bvci64_t2i_type) return boolean is
  begin
    return check(a.cmdack, b.cmdack) and check(a.rspval, b.rspval) and check(a.rerror, b.rerror) and check(a.rdata, b.rdata);
  end function check;

  -- pragma translate_on

end package body bvci64_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
