--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;

use work.global.all;
use work.axi_pkg_m8.all;
use work.sim_utils.all;

library random_lib;
use random_lib.rnd.all;

use work.axi_master_pkg.all;

entity axi_master is
  generic(cmdfile: string);         --* Name of command file
  port(clk      : in  std_ulogic;   --* clock
       srstn    : out std_ulogic;    --* Synchronous, active low, reset
       eos      : out boolean;       --* End of simulation
       hst2saxi : out axi_m2s_split;  --* Host initiator to target interface
       saxi2hst : in  axi_s2m_split   --* Target to host initiator interface
  );

end entity axi_master;

architecture arc of axi_master is

  signal hst2saxi_local:   axi_m2s_split; --* Local host initiator to target interface

  signal srstn_local:      std_ulogic; --* Local reset

begin

  srstn     <= srstn_local;
  hst2saxi  <= hst2saxi_local;

  process

    type state_type is (parsing, resetting, waiting_write, waiting_read, ending);

    variable state:        state_type;
    variable are:          axi_read_master_emulator;
    variable awe:          axi_write_master_emulator;
    variable cnt:          integer;
    variable good:         boolean;
    variable l:            line;
    variable ln:           natural;
    variable cn:           natural;
    variable eof:          boolean;
    variable cmd:          cmd_type;
    variable val:          std_ulogic;
    variable err:          std_ulogic;
    variable status:       status_type;
    variable data:         data_type;

    file cf:               text;

  begin

    -- Initializations
    hst2saxi_local <= axi_m2s_split_none;
    srstn_local    <= '0';
    eos            <= false;
    free(are);
    free(awe);
    state    := parsing;
    cnt      := 1000;
    good     := true;
    file_open(cf, cmdfile, read_mode);
    l   := null;
    ln  := 1;
    cn  := 1;
    eof := false;
    cmd := RS;

    for i in 1 to 10 loop -- 10 clock cycles reset
      wait until rising_edge(clk);
    end loop;
    srstn_local <= '1';
    wait until rising_edge(clk);

    -- Main loop
    loop

      -- AXI read address channel requests
      if hst2saxi_local.ar.arvalid = '1' and saxi2hst.ar.arready = '1' then -- If request acknowledged
        hst2saxi_local.ar.arvalid <= '0'; -- Stop requesting
      end if;
      if (hst2saxi_local.ar.arvalid = '0' or saxi2hst.ar.arready = '1') and are.raccnt /= 0 and int_rnd(1, 10) /= 1 then
        play(are, hst2saxi_local.ar); -- Submit next request and pop it out of requests FIFO
      end if;

      -- AXI write address channel requests
      if hst2saxi_local.aw.awvalid = '1' and saxi2hst.aw.awready = '1' then -- If request acknowledged
        hst2saxi_local.aw.awvalid <= '0'; -- Stop requesting
      end if;
      if (hst2saxi_local.aw.awvalid = '0' or saxi2hst.aw.awready = '1') and awe.waccnt /= 0 and int_rnd(1, 10) /= 1 then
        play(awe, hst2saxi_local.aw); -- Submit next request and pop it out of requests FIFO
      end if;

      -- AXI write data channel requests
      if hst2saxi_local.w.wvalid = '1' and saxi2hst.w.wready = '1' then -- If request acknowledged
        hst2saxi_local.w.wvalid <= '0'; -- Stop requesting
      end if;
      if (hst2saxi_local.w.wvalid = '0' or saxi2hst.w.wready = '1') and awe.wdccnt /= 0 and int_rnd(1, 10) /= 1 then
        play(awe, hst2saxi_local.w); -- Submit next request and pop it out of requests FIFO
      end if;

      -- Axi read data acknoledge

      if int_rnd(1, 10) = 1 then
        hst2saxi_local.r.rready <= '0';
      else
        hst2saxi_local.r.rready <= '1'; -- Randomly acknowledge responses
      end if;

      if saxi2hst.r.rvalid = '1' and hst2saxi_local.r.rready = '1' then -- If response acknowledged
        check(are, saxi2hst.r); -- Check reponse pop it out of responses FIFO
      end if;


      -- Axi write response acknoledge
      if int_rnd(1, 10) = 1 then
        hst2saxi_local.b.bready <= '0';
      else
        hst2saxi_local.b.bready <= '1'; -- Randomly acknowledge responses
      end if;

      if saxi2hst.b.bvalid = '1' and hst2saxi_local.b.bready = '1' then -- If response acknowledged
        check(awe, saxi2hst.b); -- Check reponse pop it out of responses FIFO
      end if;

      -- Reset
      if srstn_local = '0' then
        free(are);
        free(awe);
      end if;

      -- State machine
      case state is
        when resetting =>
          cnt := cnt - 1;
          if cnt = 0 then
            srstn_local <= '1';
            state := parsing;
          end if;

        when ending =>
          is_empty(are, good);
          if good then
            is_empty(awe, good);
            exit when good;
          end if;

        when waiting_write =>
          cnt := cnt - 1;
          is_empty(awe, good);
          if good then
            state := parsing;
          elsif cnt = 0 then
            assert false report "Timeout exceeded while waiting for end of target AXI transactions" severity failure;
          end if;
        
        when waiting_read =>
          cnt := cnt - 1;
          is_empty(are, good);
          if good then
            state := parsing;
          elsif cnt = 0 then
            assert false report "Timeout exceeded while waiting for end of target AXI transactions" severity failure;
          end if;

        when parsing => -- Parsing of command file
          while state = parsing loop
            get_cmd_token(cf, l, ln, cn, cmd, eof); -- Read next command
            if eof then -- End of command file
              state := ending;
              exit;
            end if;
            case cmd is
              when RS =>
                get_dec_token(cf, l, ln, cn, cnt);
                state := resetting;
                srstn_local <= '0';
                exit;
              when ER =>
              -- Wait end of read
                get_dec_token(cf, l, ln, cn, cnt);
                state := waiting_read;
                exit;
              when EW =>
              -- Wait end of write
                get_dec_token(cf, l, ln, cn, cnt);
                state := waiting_write;
                exit;
              when WR =>
                read_vw(cf, l, ln, cn, awe);
              when RD =>
                read_vr(cf, l, ln, cn, are);
              when QT =>
                state := ending;
                exit;
            end case;
          end loop;
      end case;
      wait until rising_edge(clk);
    end loop;
    eos <= true;
    wait;

  end process;

end architecture arc;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
