--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Host emulator for DSP units simulation
--*
--*  Emulates the surrounding host system of a DSP unit of the baseband processor. Reads commands in an ASCII command file which syntax is
--* described in the header of hst_emulator_pkg.vhd. Drives and checks the DSP unit through its target and initiator interfaces and the system interface (clock,
--* reset, chip enable and interrupt lines).

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

use work.global.all;
use work.utils.all;
use work.sim_utils.all;
use work.hst_emulator_pkg.all;

library css_lib;
use css_lib.css_pkg.all;
use css_lib.bitfield.all;

entity hst_emulator is
  generic(cmdfile: string;                    --* Name of command file
          n_pa_regs: natural range 0 to 15;   --* Number of parameter registers
          neirq: natural range 0 to 32 := 0); --* Number of extended interrupts
  port(clk:       out  std_ulogic;   --* Master clock
       srstn:     out std_ulogic;    --* Synchronous, active low, reset
       ce:        out std_ulogic;    --* Chip enable
       hirq:      out std_ulogic;    --* Host system interrupt
       pirq:      in  std_ulogic;    --* PSS irq,
       dirq:      in  std_ulogic;    --* DMA irq,
       uirq:      in  std_ulogic;    --* UC irq,
       eirq:      in  std_ulogic_vector(neirq - 1 downto 0); --* Extended interrupts
       hst2tvci:  out vci_i2t_type;  --* Host initiator to target interface
       tvci2hst:  in  vci_t2i_type;  --* Target to host initiator interface
       hst2ivci:  out vci_t2i_type;  --* Host target to initiator interface
       ivci2hst:  in  vci_i2t_type;  --* Initiator to host target interface
       gpodvalid: out boolean;       --* DMA General Purpose Output Valid indicator
       gpod:      out word64;        --* DMA General Purpose Output
       gpovvalid: out boolean;       --* VCI General Purpose Output Valid indicator
       gpov:      out word64;        --* VCI General Purpose Output
       eos:       out boolean);      --* End of simulation
end entity hst_emulator;

architecture arc of hst_emulator is

  signal clk_main:         std_ulogic; --* Output of clock generator
  signal clk_local:        std_ulogic; --* Local clock
  signal srstn_local:      std_ulogic; --* Local reset
  signal ce_local:         std_ulogic; --* Local chip enable
  signal hirq_local:       std_ulogic; --* Local host system interrupt
  signal hst2tvci_local:   vci_i2t_type; --* Local host initiator to target interface
  signal hst2ivci_local:   vci_t2i_type; --* Local host target to initiator interface
  signal eos_local:        boolean := false; --* End of simulation
  signal pirq_rising_edge: boolean;    --* Rising edge detector of PIRQ
  signal dirq_rising_edge: boolean;    --* Rising edge detector of DIRQ
  signal uirq_rising_edge: boolean;    --* Rising edge detector of UIRQ
  signal eirq_rising_edge: word32;     --* Rising edge detectors of EIRQ

begin

  -- Locals to outputs
  clk       <= clk_main; -- The output clock is a (one delta cycle delayed) copy of output of clock generator
  clk_local <= clk_main; -- The local clock (used in the following) is the same as the output clock
  srstn     <= srstn_local;
  ce        <= ce_local;
  hirq      <= hirq_local;
  hst2tvci  <= hst2tvci_local;
  hst2ivci  <= hst2ivci_local;
  eos       <= eos_local;

  -- Clock generator
  process
  begin
    clk_main <= '0';
    wait for 5 ns;
    clk_main <= '1';
    wait for 5 ns;
    if eos_local then -- If end of simulation
      print("End of simulation, regression test passed.");
      wait; -- Wait forever (simulation should end gracefully)
    end if;
  end process;

  -- Rising edge detectors. They are set to true on rising edges of the monitored interrupt and set back to false on the next rising edge of the local clock.
  process(clk_local, pirq)
  begin
    if rising_edge(pirq) then
      pirq_rising_edge <= true;
    elsif rising_edge(clk_local) then
      pirq_rising_edge <= false;
    end if;
  end process;

  process(clk_local, dirq)
  begin
    if rising_edge(dirq) then
      dirq_rising_edge <= true;
    elsif rising_edge(clk_local) then
      dirq_rising_edge <= false;
    end if;
  end process;

  process(clk_local, uirq)
  begin
    if rising_edge(uirq) then
      uirq_rising_edge <= true;
    elsif rising_edge(clk_local) then
      uirq_rising_edge <= false;
    end if;
  end process;

  gedge: for i in 0 to neirq - 1 generate
    process(clk_local, eirq(i))
    begin
      if rising_edge(eirq(i)) then
        eirq_rising_edge(i) <= '1';
      elsif rising_edge(clk_local) then
        eirq_rising_edge(i) <= '0';
      end if;
    end process;
  end generate gedge;

  gedge_not: for i in neirq to 31 generate
    eirq_rising_edge(i) <= '0';
  end generate gedge_not;

  -- Main process. Based on a VCI Initiator Emulator (VIE) and a VCI Target Emulator (VTE), two data structures defined in hst_emulator_pkg.vhd. VIE is used to:
  -- 1) Store the VCI requests that must be sent to the target VCI interface of the DSP unit. These requests are stored in a FIFO. When time has come to send the
  --    next request, it is popped out the FIFO and copied to hst2tvci_local.req. New requests are pushed in the FIFO when commands are parsed from the command
  --    file:
  --   - WR, DW, CW, RD, DR, CR: three write requests are pushed, one in the DCFG DMA configuration register, one in the DADD DMA addresses register and one in the
  --     DGOST DMA go and status register. The DMA transfer starts when the last of these three requests is sent to the DSP unit and processed.
  --   - VW and VR: each double-word of the payload produces a write (VW) or read (VR) request that is pushed in the FIFO.
  --   - EX: N_PA_REGS + 1 write requests are pushed, N_PA_REGS in the parameter registers plus one in the PGOST PS go and status register. The processing
  --     starts when the last of these requests is sent to the DSP unit and processed.
  --   In some particular cases emergency requests are generated and pushed on top of the FIFO. These emergency requests are sent to the DSP unit before the
  --   other requests. When PIRQ, DIRQ or UIRQ are raised (that is, when [PDU]IRQ_RISING_EDGE is true), one emergency read request in the IRQ register is pushed
  --   (to get the interrupt flags, check them and clear them), plus another read request in PGOST (if PIRQ) and/or DGOST (if DIRQ) to get and check the
  --   processing error flag and exit status. If PIRQ the DATA field of PGOST is also checked.
  -- 2) TBC
  process

    type state_type is (parsing, resetting, waiting, waiting_pirq, waiting_dirq, waiting_uirq, waiting_eirq, waiting_tvci, ending);

    variable state:        state_type;
    variable regs:         regs_type; -- Registers (values to be written or expected read values)
    variable regs_msk:     regs_type; -- Registers (masks used when comparing expected and actually read values)
    variable vie:          vci_initiator_emulator;
    variable vte:          vci_target_emulator;
    variable cnt:          integer;
    variable good:         boolean;
    variable l:            line;
    variable ln:           natural;
    variable cn:           natural;
    variable eof:          boolean;
    variable cmd:          cmd_type;
    variable val:          std_ulogic;
    variable err:          std_ulogic;
    variable status:       status_type;
    variable data:         data_type;
    variable new_response: boolean;
    variable response:     vci_response_type;
    variable new_request:  boolean;
    variable request:      vci_request_type;
    variable dma_reqs:     natural range 0 to 2;
    variable pss_reqs:     natural range 0 to 2;
    variable eirqmsk:      word32;
    variable tmp:          word64;
    variable be:           vci_be_type;

    file cf:               text;

  begin

    -- Initializations
    hst2tvci_local <= vci_i2t_none;
    hst2ivci_local <= vci_t2i_none;
    srstn_local    <= '0';
    ce_local       <= '0';
    hirq_local     <= '0';
    eos_local      <= false;
    gpodvalid      <= false;
    gpod           <= (others => '0');
    gpovvalid      <= false;
    gpov           <= (others => '0');
    regs     := (others => (others => '0'));
    regs_msk := (others => (others => '0'));
    free(vie);
    free(vte);
    state    := waiting_tvci;
    cnt      := 1000;
    good     := true;
    file_open(cf, cmdfile, read_mode);
    l   := null;
    ln  := 1;
    cn  := 1;
    eof := false;
    cmd := RS;
    for i in 0 to n_cs_regs - 1 loop
      regs_msk(fidx2idx(i)) := bitfield_rpadmask(64 * i + 63 downto 64 * i);
    end loop;
    set_flag(regs_msk, p2uirq_field_def, '0');
    set_flag(regs_msk, d2uirq_field_def, '0');
    set_flag(regs_msk, h2uirq_field_def, '0');
    set_flag(regs_msk, dbsy_field_def, '0');
    set_flag(regs_msk, drqp_field_def, '0');
    set_flag(regs_msk, pbsy_field_def, '0');
    set_flag(regs_msk, prqp_field_def, '0');
    set_field(regs_msk, eirq_field_def, eirq_none);
    regs_msk(uregs_idx) := (others => '0');
    new_response := true;
    response     := vci_response_none;
    new_request  := true;
    request      := vci_request_none;
    dma_reqs     := 0;
    pss_reqs      := 0;

    ce_local    <= '1';

    -- Main loop
    loop

      -- Interrupts
      if pirq_rising_edge or dirq_rising_edge or uirq_rising_edge then
        if pirq_rising_edge then
          pop_pss_error_status_data(vie, val, err, status, data, be);
          set_flag(regs, pval_field_def, '1');
          set_flag(regs, perr_field_def, err);
          set_field(regs, pst_field_def, status);
          set_field(regs, pdata_field_def, data);
          fast_push_regs_read_request(vie => vie, idx => pgost_idx, regs => regs, be => be, regs_msk => regs_msk);
          pss_reqs := pss_reqs - 1;
        end if;
        if dirq_rising_edge then
          pop_dma_error_status(vie, val, err, status);
          set_flag(regs, dval_field_def, '1');
          set_flag(regs, derr_field_def, err);
          set_field(regs, dst_field_def, status);
          fast_push_regs_read_request(vie => vie, idx => dgost_idx, regs => regs, regs_msk => regs_msk);
          dma_reqs := dma_reqs - 1;
        end if;
--        set_flag(regs, p2hirq_field_def, pirq);
--        set_flag(regs, d2hirq_field_def, dirq);
        set_flag(regs, u2hirq_field_def, uirq);
        fast_push_regs_read_request(vie => vie, idx => irq_idx, regs => regs, regs_msk => regs_msk);
      end if;

      -- TVCI requests
      if hst2tvci_local.req.cmdval = '1' and tvci2hst.cmdack = '1' then -- If request acknowledged
        hst2tvci_local.req.cmdval <= '0'; -- Stop requesting
      end if;
      if (hst2tvci_local.req.cmdval = '0' or tvci2hst.cmdack = '1') and vie.reqcnt /= 0 and int_rnd(1, 10) /= 1 then
        play(vie, hst2tvci_local.req); -- Submit next request and pop it out of requests FIFO
      end if;
      -- TVCI responses
      if int_rnd(1, 10) = 1 then
        hst2tvci_local.rspack <= '0';
      else
        hst2tvci_local.rspack <= '1'; -- Randomly acknowledge responses
      end if;
      if tvci2hst.rsp.rspval = '1' and hst2tvci_local.rspack = '1' then -- If response acknowledged
        check(vie, tvci2hst.rsp, gpovvalid, gpov); -- Check reponse, pop it out of responses FIFO, put RDATA on GPOV and raise GPOVVALID, if needed
      end if;

      -- IVCI responses
      if hst2ivci_local.rsp.rspval = '1' and ivci2hst.rspack = '1' then -- If response acknowledged
        hst2ivci_local.rsp.rspval <= '0'; -- Stop responding
      end if;
      if (hst2ivci_local.rsp.rspval = '0' or ivci2hst.rspack = '1') and vte.rsprdy /= 0 and int_rnd(1, 10) /= 1 then
        play(vte, hst2ivci_local.rsp); -- Submit next response and pop it out of responses FIFO
      end if;
      -- IVCI requests
      if int_rnd(1, 10) = 1 then
        hst2ivci_local.cmdack <= '0';
      else
        hst2ivci_local.cmdack <= '1'; -- Randomly acknowledge requests
      end if;
      if ivci2hst.req.cmdval = '1' and hst2ivci_local.cmdack = '1' then -- If request acknowledged
        check(vte, ivci2hst.req, gpodvalid, gpod); -- Check request, prepare corresponding response (REOP and RIDs), pop request out of requests FIFO, put
                                                   -- WDATA on GPOD and raise GPODVALID, if needed
      end if;

      -- Check TVCI responses stability
      if tvci2hst.rsp.rspval = '1' then -- If response on the target VCI interface
        -- Check response stability
        if new_response then -- First time we see this response
          response     := tvci2hst.rsp;
          new_response := false;
        elsif tvci2hst.rsp /= response then -- Response changed before being acknowledged
          print("Target VCI response changed before being acknowledged:");
          print(tvci2hst.rsp);
          print("On previous clock cycle was:");
          print(response);
          assert false severity failure;
        end if;
        if hst2tvci_local.rspack = '1' then -- If acknowledged response on the target VCI interface
          new_response := true;             -- Next response will be a new one
        end if;
      end if;

      -- Check IVCI requests stability
      if ivci2hst.req.cmdval = '1' then -- If request on the initiator VCI interface
        -- Check request stability
        if new_request then
          request := ivci2hst.req;
          new_request := false;
        elsif ivci2hst.req /= request then -- Request changed before being acknowledged
          print("DMA request changed before being acknowledged:");
          print(ivci2hst.req);
          print("On previous clock cycle was:");
          print(request);
          assert false severity failure;
        end if;
        if hst2ivci_local.cmdack = '1' then -- If acknowledged request on the initiator VCI interface
          new_request := true;              -- Next request will be a new one
        end if;
      end if;

      -- Reset
      if srstn_local = '0' then
        free(vie);
        free(vte);
        new_response := true;
        new_request  := true;
        pss_reqs      := 0;
        dma_reqs     := 0;
      end if;

      -- State machine
      case state is
        when resetting =>
          cnt := cnt - 1;
          if cnt = 0 then
            srstn_local <= '1';
            state := parsing;
          end if;
        when waiting =>
          cnt := cnt - 1;
          if cnt = 0 then
            state := parsing;
          end if;
        when waiting_pirq =>
          cnt := cnt - 1;
          if pirq_rising_edge then
            state := parsing;
          elsif cnt = 0 then
            assert false report "Timeout exceeded while waiting for end of computation" severity failure;
          end if;
        when waiting_dirq =>
          cnt := cnt - 1;
          if dirq_rising_edge then
            state := parsing;
          elsif cnt = 0 then
            assert false report "Timeout exceeded while waiting for end of DMA transfer" severity failure;
          end if;
        when waiting_uirq =>
          cnt := cnt - 1;
          if uirq_rising_edge then
            state := parsing;
          elsif cnt = 0 then
            assert false report "Timeout exceeded while waiting for UC interrupt" severity failure;
          end if;
        when waiting_eirq =>
          cnt := cnt - 1;
          if or_reduce(eirqmsk and eirq_rising_edge) = '1' then
            state := parsing;
          elsif cnt = 0 then
            assert false report "Timeout exceeded while waiting for extended interrupt" severity failure;
          end if;
        when waiting_tvci =>
          cnt := cnt - 1;
          is_responses_fifo_empty(vie, good);
          if good then
            state := parsing;
          elsif cnt = 0 then
            assert false report "Timeout exceeded while waiting for end of target VCI transactions" severity failure;
          end if;
        when ending =>
          is_empty(vie, good);
          if good then
            is_empty(vte, good);
            exit when good;
          end if;
        when parsing => -- Parsing of command file
          while state = parsing loop
            get_cmd_token(cf, l, ln, cn, cmd, eof); -- Read next command
            if eof then -- End of command file
              state := ending;
              exit;
            end if;
            case cmd is
              when RS =>
                get_dec_token(cf, l, ln, cn, cnt);
                state := resetting;
                srstn_local <= '0';
                exit;
              when DW =>
                if dma_reqs = 2 then
                  cn := cn - 2;
                  state := waiting_dirq;
                  exit;
                else
                  parse_dw(cf, l, ln, cn, vie, vte);
                  dma_reqs := dma_reqs + 1;
                end if;
              when DR =>
                if dma_reqs = 2 then
                  cn := cn - 2;
                  state := waiting_dirq;
                  exit;
                else
                  parse_dr(cf, l, ln, cn, vie, vte);
                  dma_reqs := dma_reqs + 1;
                end if;
              when GD =>
                get_hex_token(cf, l, ln, cn, tmp);
                gpod      <= tmp;
                gpodvalid <= true;
                cnt := 1;
                state := waiting;
              when VW =>
                parse_vw(cf, l, ln, cn, vie);
              when VR =>
                parse_vr(cf, l, ln, cn, vie);
              when GV =>
                get_hex_token(cf, l, ln, cn, tmp);
                gpov      <= tmp;
                gpovvalid <= true;
                cnt := 1;
                state := waiting;
              when UW | UR =>
                syntax_error(l, ln, cn, "command not supported in HST_EMULATOR");
                assert false severity failure;
              when WD =>
                get_dec_token(cf, l, ln, cn, cnt);
                state := waiting_dirq;
                exit;
              when WV =>
                get_dec_token(cf, l, ln, cn, cnt);
                state := waiting_tvci;
                exit;
              when WU =>
                get_dec_token(cf, l, ln, cn, cnt);
                state := waiting_uirq;
                exit;
              when EX | EN =>
                if pss_reqs = 2 then
                  cn := cn - 2;
                  state := waiting_pirq;
                  exit;
                elsif cmd = EX then
                  parse_ex(cf, l, ln, cn, vie, n_pa_regs);
                  pss_reqs := pss_reqs + 1;
                else
                  parse_en(cf, l, ln, cn, vie);
                  pss_reqs := pss_reqs + 1;
                end if;
              when WE =>
                get_dec_token(cf, l, ln, cn, cnt);
                state := waiting_pirq;
                exit;
              when WX =>
                get_hex_token(cf, l, ln, cn, eirqmsk);
                get_dec_token(cf, l, ln, cn, cnt);
                state := waiting_eirq;
                exit;
              when WC =>
                get_dec_token(cf, l, ln, cn, cnt);
                state := waiting;
                exit;
              when QT =>
                state := ending;
                exit;
            end case;
          end loop;
      end case;
      wait until rising_edge(clk_local);
      gpodvalid <= false;
      gpovvalid <= false;
    end loop;
    eos_local <= true;
    wait;

  end process;

end architecture arc;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
