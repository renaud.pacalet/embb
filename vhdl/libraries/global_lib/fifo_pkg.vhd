--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

package fifo_pkg is

    generic(type T);

    type fifo is protected
        procedure free;
        procedure push(val: in T);
        impure function pop return T;
        impure function empty return boolean;
        impure function count return natural;
    end protected fifo;

end package fifo_pkg;

package body fifo_pkg is

    type fifo is protected body

        type entry;
        type entry_pointer is access entry;
        type entry is record
            val: T;
            prv: entry_pointer;
            nxt: entry_pointer;
        end record entry;
    
        variable head, tail: entry_pointer;
        variable cnt: natural := 0;
    
        procedure free is
            variable tmp: entry_pointer;
        begin
            while cnt /= 0 loop
                tmp := tail;
                tail := tail.prv;
                deallocate(tmp);
                cnt := cnt - 1;
            end loop;
        end procedure free;
    
        procedure push(val: in T) is
            variable tmp: entry_pointer;
        begin
            tmp := new entry'(val => val, prv => null, nxt => head);
            if cnt = 0 then
                tail := tmp;
            else
                head.prv := tmp;
            end if;
            head := tmp;
            cnt := cnt + 1;
        end procedure push;
    
        impure function pop return T is
            variable tmp: entry_pointer;
            variable val: T;
        begin
            assert not empty report "Cannot pop empty FIFO" severity failure;
            tmp := tail;
            val := tmp.val;
            tail := tmp.prv;
            deallocate(tmp);
            cnt := cnt - 1;
            return val;
        end function pop;
    
        impure function empty return boolean is
        begin
            return cnt = 0;
        end function empty;
    
        impure function count return natural is
        begin
            return cnt;
        end function count;

    end protected body fifo;

end package body fifo_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
