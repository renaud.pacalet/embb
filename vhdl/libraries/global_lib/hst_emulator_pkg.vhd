--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation utility package
--*
--* The functions defined in this package can be used to parse command files and drive simulations. The syntax of the command files is explained in the
--* following. The targeted simulations are those that simulate a complete DSP unit. The simulation environment sends VCI read-write requests on the VCI target
--* port of the DSP unit to access the MSS and the control and status registers. By accessing the control and status registers it can launch PSS operations and
--* DMA transfers. It also controls the interrupt enables, chip enables and resets of the DSP unit. It can also upload a UC image in the MSS part dedicated to
--* UC and wake up UC to run macros. The simulation environment also plays the role of the VCI target to which DMA sends read-write requests. It acknowledges
--* and responds DMA requests. The behavior is entirely defined in a command file. HST_EMULATOR contains two independent VCI engines: VIE (VCI Initiator
--* Emulator) and VTE (VCI Target Emulator). VIE plays the role of a VCI initiator. It is connected to the target VCI interface of the DSP unit. It sends
--* read/write requests and checks the corresponding responses against expected ones. VTE acts as a distant VCI target and answers the requests issued by the
--* DSP unit (that is, by its embedded DMA engine). It also checks the requests against the expected ones. During the parsing of the command file, and depending
--* on the encountered commands and their parameters, new VCI requests and responses are added to VIE and VTE in a FIFO-like way. VIE and VTE "play" them in the
--* same order and check the corresponding outgoing responses and requests. The PIRQ, DIRQ and UIRQ interrupts are taken into account by HST_EMULATOR: they are
--* the only way to detect the end of a computation, DMA transfer or UC routine and to synchronize with the sequence of operations defined in the command file.
--* It is thus essential that interrupts are enabled. Every time an interrupt is raised, a VCI read request to the IRQ register is automatically added to VIE,
--* on top of the requests FIFO, plus a read request in DGOST if it was a DIRQ interrupt or in PGOST if it was a PIRQ interrupt. These "emergency" requests are
--* processed before the already pending ones in order to speed up reactions. The interrupts are cleared by the first request and the exit status / error flag
--* of the processing or DMA transfer is fetched by the second. Later on, when this second request is responded, the exit status / error flag are checked
--* against the expected ones.
--*
--* Syntax of the command files:
--* ----------------------------
--* Comments extend from the '#' character to the end of the line. The file contains a sequence of commands. Each command is made of one or several fields.
--* Fields are separated by spaces, tabs or end of lines. Numeric fields are in binary, decimal or hexadecimal form, as specified in the following. Hexadecimal
--* is the default. The first field of a command is a 2 characters command specifier. The following fields in a command, if any, are parameters. The number of
--* parameters depends on the command. They have no default value. Parameters listed between square brackets ([P Q]) indicated optional parameters that are
--* present or not depending on the value of preceding parameters. The other parameters are mandatory. Supported commands are:
--*
--* - RS N: ReSet, forces a N cycles reset (SRSTN=0) of DSP unit. Takes one parameter:
--*   - N: number of reset cycles, decimal. Zero means 'forever'.
--*   No VCI request or response is sent to DSP unit during the reset period. At the end of the reset period SRSTN is asserted and the parsing of the command file
--*   continues. Note that the whole DSP unit is reset and that the previous content of the configuration and status registers is lost. A RS command must thus usually be
--*   followed by a series of VW/VR commands in order to configure the DSP unit (chip enables, resets, interrupt enables, etc.)
--*
--* - DW ADD LENGTH CST MASK PAYLOAD: Dma Write. Programs a DMA transfer between a virtual external memory location and the memory subsystem to write a sequence
--*   of 64 bits values. This programming is done by writing to the DCFG, DADD and DGOST registers through the target VCI interface. Three consecutive requests
--*   (and their corresponding expected responses) are added to VIE. The list of requests that the DMA engine is supposed to issue is added to VTE with the
--*   responses to send back. Takes 4 + LENGTH parameters:
--*   - ADD: destination byte ADDress in the memory subsystem, between 00000 and 7ffff. The source address is chosen randomly but is always a multiple of 8
--*     (please see the documentation of the DMA engines of the DSP units to understand the consequences of this). Note that most MSS are less than 512 kBytes
--*     long and that out of range write accesses will raise assertions from the MSS.
--*   - LENGTH: decimal, number of double-words to write. Must be less than 2^29.
--*   - CST: a 1 bit flag indicating whether the write address is constant (1) or not (0).
--*   - MASK: an 8 bits byte enable MASK from 0 to ff. Set bits indicate which byte to write.
--*   - PAYLOAD: the LENGTH double-words to written, separated by blanks.
--*   Note: When a DMA transfer is already running and a second one is already pending, the DW command first waits until the running one ends. Else, the DMA
--*   transfer is programmed and the parsing of the command file continues. As the DIRQ interrupt is the only way for HST_EMULATOR to know that a DMA transfer
--*   ends, it is important that this interrupt is enabled by proper VW commands prior using DW. As soon as a DMA transfer ends (that is, as soon as the DIRQ
--*   interrupt is fired), a read request in the IRQ register is immediately sent to clear the interrupt and a read request in the DGOST register is sent to
--*   check for DMA errors and return status. The response to the first request is not checked (because interrupts could have been configured as edge-triggered
--*   and other interrupts could also have been fired) but an error is raised when the response to the second request is not the expected one.
--*
--* - DR ADD LENGTH CST MASKR CHECK GPO [MASKC PAYLOAD]: Dma Read, same as DW but the DMA transfer is from MSS to a virtual external memory location. The read
--*   values are optionally checked against a payload of expected values. They are also optionally sent to the GPOD general purpose output port; in this case,
--*   each valid value on GPOD is signaled by the GPODVALID data strobe. This allows an external custom checker to be plugged on HST_EMULATOR to run a dedicated
--*   checking algorithm. Takes 6 or 8 parameters:
--*   - ADD: source byte ADDress in the memory subsystem, between 00000 and 7ffff. The destination address is chosen randomly but is always a multiple of 8
--*     (please see the documentation of the DMA engines of the DSP units to understand the consequences of this). Note that most MSS are less than 512 kBytes
--*     long and that out of range read accesses will raise assertions from the MSS.
--*   - LENGTH: decimal, number of double-words to read. Must be less than 2^29.
--*   - CST: a 1 bit flag indicating whether the read address is constant (1) or not (0).
--*   - MASKR: an 8 bits read byte enable from 0 to ff. Set bits indicate which byte to read.
--*   - CHECK: a 1 bit flag indicating whether the read values must be checked against the payload of expected values (1) or not (0).
--*   - GPO: a 1 bit flag indicating whether the read values must be sent to the GPOD general purpose output port (1) or not (0).
--*   - MASKC: present only if CHECK=1. Check byte enable from 0 to ff. Set bits indicate which byte to check against the corresponding byte in payload.
--*   - PAYLOAD: present only if CHECK=1. LENGTH double-words of payload against which read values shall be checked, separated by blanks.
--*
--* - GD VAL: GpoD. Send a 64 bits value to GPOD general purpose output port. Can be used to send control informations to an observer plugged on the GPOD. Takes
--*   one parameter:
--*   - VAL: the value to send.
--*
--* - VW ADD LENGTH CST MASK PAYLOAD: Vci Write, same as DW but instead of using the DMA engine, the data transfer between a virtual external memory location
--*   and the DSP unit is performed directly through the target VCI interface. VIE is added all corresponding requests and expected responses. Note that VW can
--*   not only access the MSS but also the configuration and status registers (byte addresses fff00 to fffff). Note that not all 32 registers are mapped and
--*   that some are read-only. Assertions are raised on invalid register accesses or out of range read accesses in MSS.
--*
--* - VR ADD LENGTH CST MASKR CHECK GPO [MASKC PAYLOAD]: Vci Read, same as DR but instead of using the DMA engine, the data transfer between the DSP unit and a
--*   virtual external memory location is performed directly through the target VCI interface. VIE is added all corresponding requests and expected responses.
--*   If GPO is set the read values are sent to the GPOV general purpose output port; in this case, each valid value on GPOV is signaled by the GPOVVALID data
--*   strobe.
--*
--* - GV VAL: GpoV. Send a 64 bits value to GPOV general purpose output port. Can be used to send control informations to an observer plugged on the GPOV. Takes
--*   one parameter:
--*   - VAL: the value to send.
--*
--* - UW: Ucr Write. Not implemented yet. Reserved for later use.
--*
--* - UR: Ucr Read. Not implemented yet. Reserved for later use.
--*
--* - WD T: Wait until end of Dma read or write operation. The next command will be taken into account only after the DIRQ interrupt is raised. Takes one
--*   parameter:
--*   - T: timeout in clock cycles, decimal. An assertion is fired with severity error if the timeout is exceeded. Zero means 'forever'.
--*
--* - WV T: Wait until end of target Vci read or write operation. The next command will be taken into account only after VIE sends its last request and receives
--*   its last expected response. Takes one parameter:
--*   - T: timeout in clock cycles, decimal. An assertion is fired with severity error if the timeout is exceeded. Zero means 'forever'.
--*
--* - WU T: Wait until Uc interrupt. The next command will be taken into account only after the UIRQ interrupt is raised. Takes one parameter:
--*   - T: timeout in clock cycles, decimal. An assertion is fired with severity error if the timeout is exceeded. Zero means 'forever'.
--*
--* - EX STATUS ERROR DATA MASK PARAM: EXec command. Launches a processing on PSS. Takes five parameter:
--*   - STATUS: the 24 bits expected exit status.
--*   - ERROR: the expected error flag, decimal.
--*   - DATA: the 32 bits expected returned data.
--*   - MASK: check byte enable from 0 to ff. Set bits indicate which byte to check against the corresponding byte in the 64 bits PGOST register.
--*   - PARAM: the processing parameter that must be sent to the PSS, split in CMDSIZE/8 64 bits words, in hex form, MSB first.
--*   This is a short hand for a series of VCI read/write requests: the parameters are first written in the configuration registers; then, a write request in
--*   the PGOST register launches the processing. When the corresponding PIRQ interrupt is fired, a read request in the IRQ register is immediately sent to
--*   clear the interrupt (even if the PIRQ interrupt is level-triggered, while in this case it is useless) and a read request in the PGOST register is sent to
--*   check for error flag, return status and returned data. The response to the first request is not checked (because interrupts could have been configured as
--*   edge-triggered and other interrupts could also have been fired) but an error is raised when the response to the second request is not the expected one on
--*   one of the unmasked bytes. Note: When a processing is already running and a second one is already pending, the EX command first waits until the running
--*   one ends. Else, the processing is programmed and the parsing of the command file continues. As the PIRQ interrupt is the only way for HST_EMULATOR to know
--*   that a PSS processing ends, it is important that this interrupt is enabled by proper VW commands prior using EX.
--*
--* - EN STATUS ERROR DATA MASK: Exec No parameters command. Launches a processing on PSS. Same as EX but the processing parameters are not provided. Must thus
--*   be preceded by a series of VW commands to prepare the parameters.
--*
--* - WE T: Wait until end of Execution. The next command will be taken into account only after the PIRQ interrupt is raised. Takes one parameter:
--*   - T: timeout in clock cycles, decimal. An assertion is fired with severity error if the timeout is exceeded. Zero means 'forever'.
--*
--* - WX MASK T: Wait until extended interrupts are raised. The next command will be taken into account only after at least one of the expected extended
--*   interrupts is raised. Takes two parameter:
--*   - MASK: 32 bits extended interrupts mask. Set bits indicate which interrupts are waited for.
--*   - T: timeout in clock cycles, decimal. An assertion is fired with severity error if the timeout is exceeded. Zero means 'forever'.
--*
--* - WC: Wait Cycles. Used to wait for a given number of cycles before the next command is taken into account. Takes one parameter:
--*   - N: number of cycles to wait for, decimal. Zero means 'forever'.
--*
--* - QT: QuiT. Waits until completion of the pending operations and stops the clock. The simulation should end gracefully. If it does not, there is a problem
--*   somewhere... Takes no parameter.
--*
--* Example:
--* 
--* RS 10                   # Reset DSP unit for 10 cycles
--* 
--* DW 0 4 0 ff             # Write (DMA), starting at byte address 0, four double-words (with the 8 bytes enabled), with incrementing addresses.
--* 
--* 0003000200010000        # The double-words to write.
--* 0007000600050004
--* 000B000A00090008
--* 000F000E000D000C
--* 
--* WD 10000                # Wait until end of DMA write operation with a 10000 cycles timeout.
--* 
--* EX 0 0 0 ff             # Launch a processing in PSS. In this example the processing parameter is 00810000 (hex form), the expected exit status is 0,
--* 00810000                # the expected error flag is 0, the expected returned data is 0 and the 3 must be checked.
--* 
--* WE 1000000              # Wait until end of computation with a 1000000 cycles timeout.
--* 
--* VR C000 2 0 ff 1 1 fe   # Read (VCIInterface), at byte address C000, 2 double-words (with the 8 bytes enabled), with incrementing addresses, compare
--*                         # the 7 # most significant read bytes with those of the provided payload and send the read double-words to the GPOV general purpose
--*                         # output port.
--* 
--* # The 2 expected double-words.
--* 0302010007060504 0B0A09080F0E0D0C
--*
--* WC 1000                 # Wait for 1000 Cycles.
--* 
--* WV 1000000              # Wait until end of VCIInterface read operation with a 1000000 cycles timeout.
--* 
--* EX 1 1 0 0f             # Launch a processing in PSS. In this example the processing parameter is 810001 (hex form), the expected exit status is 1,
--* 00810001                # the expected error flag is 1 and the expected returned data is 0 but only the exit status and error flag are checked.
--* 
--* WE 1000000              # Wait until end of computation with a 1000000 cycles timeout.
--* QT                      # Quit

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

use work.global.all;
use work.utils.all;
use work.sim_utils.all;

library css_lib;
use css_lib.css_pkg.all;

package hst_emulator_pkg is

  --------------------------
  -- Command file parsing --
  --------------------------

  -- Type defining the commands in a command file
  type cmd_type is (RS, DW, DR, GD, VW, VR, GV, UW, UR, WD, WV, WU, EX, EN, WE, WX, WC, QT);
  constant cmd_num: natural := cmd_type'pos(cmd_type'right) + 1; -- Number of different commands
  subtype cmd_name_type is string(1 to 2); -- Type of command names
  type cmd_names_type is array(0 to cmd_num - 1) of cmd_name_type; -- Type of array of all command names
  -- All command names
  constant cmd_names: cmd_names_type := ("RS", "DW", "DR", "GD", "VW", "VR", "GV", "UW", "UR", "WD", "WV", "WU", "EX", "EN", "WE", "WX", "WC", "QT");

  -- Return true if C is a blank character (space, tab or carriage return).
  function is_blank(c: character) return boolean;

  -- Find the next token in line L, starting at character CN (1 being the first character), skipping blanks and comments. L is unmodified. If returned EOL (end
  -- of line) is false, CN points to the first character of next token, else, end of line was encountered first and CN is unmodified.
  procedure next_token(variable l: in line; cn: inout natural; eol: out boolean);

  -- Same as the previous one but with two more parameters: F, a text file and LN a line counter. When end of line is encountered, increments LN, sets CN to 1,
  -- reads a new line in file F and continues until first token or end of file. If returned EOF (end of file) is false, L is the line containing the token and
  -- CN points to the first character of token in L, else, end of file was encountered first.
  procedure next_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; eof: out boolean);

  -- Convert a command to its string name
  function cmd2cmd_name(c: cmd_type) return cmd_name_type;

  -- Convert a command name to the corresponding command
  procedure cmd_name2cmd(n: in cmd_name_type; c: out cmd_type; good: out boolean);

  -- Reads a two characters command in the line L at character CN. On return L is unmodified. If GOOD is true, CN points to the first character after the
  -- command and CMD is the read command, else no command was found.
  procedure cmd_read(variable l: in line; cn: inout natural; cmd: out cmd_type; good: out boolean);

  -- Read an u_unsigned  hexadecimal value from line L. Stop at end of line or first non-hex character. The L parameter is unmodified. GOOD is an exit status set
  -- to false when the first character is non-hex, the line is empty or on overflow of the VAL output parameter. If returned GOOD is true CN points to the first
  -- non-hex character or after the end of line.
  procedure hex_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean);

  -- Read an u_unsigned decimal value from line L. Stop at end of line or first non-dec character. The L parameter is unmodified. GOOD is an exit status set to
  -- false when the first character is non-dec, the line is empty or on overflow of the VAL output parameter. If returned GOOD is true CN points to the first
  -- non-dec character or after the end of line.
  procedure dec_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean);

  -- Read an u_unsigned binary value from line L. Stop at end of line or first non-bit character. The L parameter is unmodified. GOOD is an exit status set to
  -- false when the first character is non-bit, the line is empty or on overflow of the VAL output parameter. If returned GOOD is true CN points to the first
  -- non-bit character or after the end of line.
  procedure bin_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean);

  -- Read a boolean value (0=false, 1=true) from line L. The L parameter is unmodified. GOOD is an exit status set to false when the first character is
  -- non-boolean or the line is empty. If returned GOOD is true CN points to the first non-boolean character or after the end of line.
  procedure bool_read(variable l: in line; cn: inout natural; val: out boolean; good: out boolean);

  -- Print a syntax error message
  procedure syntax_error(variable l: in line; ln: in natural; cn: in natural; str: in string);

  -- Advances to next token and reads it as a command
  procedure get_cmd_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out cmd_type; good: out boolean; eof: out boolean);
  procedure get_cmd_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out cmd_type; eof: out boolean);

  -- Advances to next token and reads it as an hex value
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural);

  -- Advances to next token and reads it as a decimal value
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural);

  -- Advances to next token and reads it as a binary value
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural);

  -- Advances to next token and reads it as a boolean (0=false, 1=true) value
  procedure get_bool_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out boolean; good: out boolean; eof: out boolean);
  procedure get_bool_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out boolean);

  -----------------------------------------
  -- VCI requests and responses handling --
  -----------------------------------------

  constant mss_trdid: vci_trdid_type := vci_trdid_type(to_unsigned(0, vci_t));
  constant regs_trdid: vci_trdid_type := vci_trdid_type(to_unsigned(1, vci_t));
  constant fast_regs_trdid: vci_trdid_type := vci_trdid_type(to_unsigned(2, vci_t));

  type vie_requests_fifo_entry;
  type vie_requests_fifo_entry_ptr is access vie_requests_fifo_entry;
  type vie_requests_fifo_entry is record
    req: vci_request_type;
    nxt: vie_requests_fifo_entry_ptr;
    prv: vie_requests_fifo_entry_ptr;
  end record vie_requests_fifo_entry;

  type vie_responses_fifo_entry;
  type vie_responses_fifo_entry_ptr is access vie_responses_fifo_entry;
  type vie_responses_fifo_entry is record
    rsp: vci_response_type;
    msk: vci_data_type;
    gp:  boolean;
    nxt: vie_responses_fifo_entry_ptr;
    prv: vie_responses_fifo_entry_ptr;
  end record vie_responses_fifo_entry;

  type vie_pss_status_fifo_entry;
  type vie_pss_status_fifo_entry_ptr is access vie_pss_status_fifo_entry;
  type vie_pss_status_fifo_entry is record
    val:    std_ulogic;
    err:    std_ulogic;
    status: status_type;
    data:   data_type;
    be:     vci_be_type;
    nxt:    vie_pss_status_fifo_entry_ptr;
    prv:    vie_pss_status_fifo_entry_ptr;
  end record vie_pss_status_fifo_entry;

  type vie_dma_status_fifo_entry;
  type vie_dma_status_fifo_entry_ptr is access vie_dma_status_fifo_entry;
  type vie_dma_status_fifo_entry is record
    val:    std_ulogic;
    err:    std_ulogic;
    status: status_type;
    nxt:    vie_dma_status_fifo_entry_ptr;
    prv:    vie_dma_status_fifo_entry_ptr;
  end record vie_dma_status_fifo_entry;

  type vci_initiator_emulator is record
    reqbot: vie_requests_fifo_entry_ptr;
    reqtop: vie_requests_fifo_entry_ptr;
    reqcnt: natural;
    rspbot: vie_responses_fifo_entry_ptr;
    rsptop: vie_responses_fifo_entry_ptr;
    rspcnt: natural;
    dmabot: vie_dma_status_fifo_entry_ptr;
    dmatop: vie_dma_status_fifo_entry_ptr;
    dmacnt: natural;
    pssbot:  vie_pss_status_fifo_entry_ptr;
    psstop:  vie_pss_status_fifo_entry_ptr;
    psscnt:  natural;
  end record vci_initiator_emulator;

  procedure push_regs_read_request(variable vie: inout vci_initiator_emulator; idx: in natural range 0 to 31; be: in vci_be_type := (others => '1'); regs: in regs_type; regs_msk: in regs_type; gp: in boolean := false);
  procedure fast_push_regs_read_request(variable vie: inout vci_initiator_emulator; idx: in natural range 0 to 31; be: in vci_be_type := (others => '1'); regs: in regs_type; regs_msk: in regs_type; gp: in boolean := false);
  procedure push_regs_write_request(variable vie: inout vci_initiator_emulator; idx: in natural range 0 to 31; be: in vci_be_type := (others => '1'); regs: in regs_type);
  procedure free(variable vie: inout vci_initiator_emulator);
  procedure is_empty(variable vie: in vci_initiator_emulator; good: out boolean);
  procedure is_responses_fifo_empty(variable vie: in vci_initiator_emulator; good: out boolean);
  procedure pop_pss_error_status_data(variable vie: inout vci_initiator_emulator; val: out std_ulogic; err: out std_ulogic; status: out status_type; data: out data_type; be: out vci_be_type);
  procedure pop_dma_error_status(variable vie: inout vci_initiator_emulator; val: out std_ulogic; err: out std_ulogic; status: out status_type);
  procedure play(variable vie: inout vci_initiator_emulator; signal req: out vci_request_type);
  procedure check(variable vie: inout vci_initiator_emulator; rsp: in vci_response_type; signal gpovalid: out boolean; signal gpo: out word64);

  type vte_requests_fifo_entry;
  type vte_requests_fifo_entry_ptr is access vte_requests_fifo_entry;
  type vte_requests_fifo_entry is record
    req: vci_request_type;
    msk: vci_request_type;
    gp:  boolean;
    nxt: vte_requests_fifo_entry_ptr;
    prv: vte_requests_fifo_entry_ptr;
  end record vte_requests_fifo_entry;

  type vte_responses_fifo_entry;
  type vte_responses_fifo_entry_ptr is access vte_responses_fifo_entry;
  type vte_responses_fifo_entry is record
    rsp: vci_response_type;
    nxt: vte_responses_fifo_entry_ptr;
    prv: vte_responses_fifo_entry_ptr;
  end record vte_responses_fifo_entry;

  type vci_target_emulator is record
    reqbot: vte_requests_fifo_entry_ptr;
    reqtop: vte_requests_fifo_entry_ptr;
    reqcnt: natural;
    rspbot: vte_responses_fifo_entry_ptr;
    rsptop: vte_responses_fifo_entry_ptr;
    rspprp: vte_responses_fifo_entry_ptr;
    rspcnt: natural;
    rsprdy: natural;
  end record vci_target_emulator;

  procedure free(variable vte: inout vci_target_emulator);
  procedure is_empty(variable vte: in vci_target_emulator; good: out boolean);
  procedure play(variable vte: inout vci_target_emulator; signal rsp: out vci_response_type);
  procedure check(variable vte: inout vci_target_emulator; req: in vci_request_type; signal gpovalid: out boolean; signal gpo: out word64);

  procedure parse_dw(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator; vte: inout vci_target_emulator);
  procedure parse_dr(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator; vte: inout vci_target_emulator);
  procedure parse_vw(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator);
  procedure parse_vr(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator);
  procedure parse_ex(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator; n_pa_regs: natural range 0 to 15);
  procedure parse_en(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator);

end package hst_emulator_pkg;

package body hst_emulator_pkg is

  function is_blank(c: character) return boolean is
  begin
    return c = ' ' or c = HT or c = CR;
  end function is_blank;

  procedure next_token(variable l: in line; cn: inout natural; eol: out boolean) is
  begin
    loop
      if l = null or l'length < cn or l(cn) = '#' then
        eol := true;
        return;
      elsif is_blank(l(cn)) then
        cn := cn + 1;
      else
        eol := false;
        return;
      end if;
    end loop;
  end procedure next_token;

  procedure next_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; eof: out boolean) is
    variable eol: boolean;
  begin
    loop
      next_token(l, cn, eol);
      if eol and endfile(f) then
        eof := true;
        return;
      elsif eol then
        readline(f, l);
        ln := ln + 1;
        cn := 1;
      else
        eof := false;
        return;
      end if;
    end loop;
  end procedure next_token;

  function cmd2cmd_name(c: cmd_type) return cmd_name_type is
  begin
    return cmd_names(cmd_type'pos(c));
  end function cmd2cmd_name;

  procedure cmd_name2cmd(n: in cmd_name_type; c: out cmd_type; good: out boolean) is
  begin
    for i in 0 to cmd_num - 1 loop
      if n = cmd_names(i) then
        good := true;
        c := cmd_type'val(i);
        return;
      end if;
    end loop;
    good := false;
  end procedure cmd_name2cmd;

  procedure cmd_read(variable l: in line; cn: inout natural; cmd: out cmd_type; good: out boolean) is
    variable s: string(1 to 2);
    variable tmp: boolean;
  begin
    good := false;
    if l'length < cn + 1 then -- Less than 2 characters left in line L
      return;
    else
      s(1 to 2) := l(cn to cn + 1);
      cmd_name2cmd(s, cmd, tmp);
      good := tmp;
      if tmp then
        cn := cn + 2;
      end if;
    end if;
  end procedure cmd_read;

  procedure hex_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean) is
    constant n: natural := val'length;
    variable res: u_unsigned(n + 3 downto 0) := (others => '0');
    variable v: integer range -1 to 15;
  begin
    good := false;
    if l = null or l'length < cn then -- Empty line or end of line
      return;
    end if;
    v := char2hex(l(cn));
    if v = -1 then -- First character is not hexadecimal
      return;
    end if;
    loop
      res := res(n - 1 downto 0) & to_unsigned(v, 4);
      if res(n + 3 downto n) /= x"0" then -- Overflow
        return;
      end if;
      cn := cn + 1;
      exit when l'length < cn; -- End of line
      v := char2hex(l(cn));
      exit when v = -1; -- Encountered first non hexadecimal character
    end loop;
    good := true;
    val := res(n - 1 downto 0);
  end procedure hex_read;

  procedure dec_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean) is
    constant n: natural := val'length;
    variable res: u_unsigned(n + 3 downto 0) := (others => '0');
    variable tmp: u_unsigned(2 * n + 7 downto 0) := (others => '0');
    variable v: integer range -1 to 9;
  begin
    good := false;
    if l = null or l'length < cn then -- Empty line or end of line
      return;
    end if;
    v := char2dec(l(cn));
    if v = -1 then -- First character is not decimal
      return;
    end if;
    loop
      tmp := res * 10 + v;
      res := tmp(n + 3 downto 0);
      if res(n + 3 downto n) /= x"0" then -- Overflow
        return;
      end if;
      cn := cn + 1;
      exit when l'length < cn; -- End of line
      v := char2dec(l(cn));
      exit when v = -1; -- Encountered first non decimal character
    end loop;
    good := true;
    val := res(n - 1 downto 0);
  end procedure dec_read;

  procedure bin_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean) is
    constant n: natural := val'length;
    variable res: u_unsigned(n downto 0) := (others => '0');
    variable v: integer range -1 to 1;
  begin
    good := false;
    if l = null or l'length < cn then -- Empty line or end of line
      return;
    end if;
    v := char2bin(l(cn));
    if v = -1 then -- First character is not binary
      return;
    end if;
    loop
      res := res(n - 1 downto 0) & to_unsigned(v, 1);
      if res(n) /= '0' then -- Overflow
        return;
      end if;
      cn := cn + 1;
      exit when l'length < cn; -- End of line
      v := char2bin(l(cn));
      exit when v = -1; -- Encountered first non binary character
    end loop;
    good := true;
    val := res(n - 1 downto 0);
  end procedure bin_read;

  procedure bool_read(variable l: in line; cn: inout natural; val: out boolean; good: out boolean) is
    variable v: integer range -1 to 1;
  begin
    good := false;
    if l = null or l'length < cn then -- Empty line or end of line
      return;
    end if;
    v := char2bin(l(cn));
    cn := cn + 1;
    if v = -1 then -- First character is not boolean (0 or 1)
      return;
    end if;
    good := true;
    val := (v = 1);
  end procedure bool_read;

  procedure syntax_error(variable l: in line; ln: in natural; cn: in natural; str: in string) is
    variable dbg: line;
  begin
    write(dbg, ln);
    print(dbg, ":");
    write(dbg, cn);
    print(dbg, ": syntax error : ");
    write(dbg, str);
    writeline(output, dbg);
    write(dbg, l.all);
    writeline(output, dbg);
  end procedure syntax_error;

  procedure get_cmd_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out cmd_type; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
  begin
    next_token(f, l, ln, cn, tmp);
    if tmp then -- End of file
      good := true;
      eof  := true;
      return;
    end if;
    eof := false;
    cmd_read(l, cn, val, tmp);
    if not tmp then -- Next token is not a command 
      good := false;
      syntax_error(l, ln, cn, "not a command");
      return;
    end if;
    good := true;
  end procedure get_cmd_token;

  procedure get_cmd_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out cmd_type; eof: out boolean) is
    variable good: boolean;
  begin
    get_cmd_token(f, l, ln, cn, val, good, eof);
    assert good severity failure;
  end procedure get_cmd_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
  begin
    next_token(f, l, ln, cn, tmp);
    if tmp then -- End of file
      good := true;
      eof  := true;
      return;
    end if;
    eof := false;
    hex_read(l, cn, val, tmp);
    if not tmp then -- Next token is not hexadecimal or overflow
      good := false;
      syntax_error(l, ln, cn, "not an hexadecimal value or overflow");
      return;
    end if;
    good := true;
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_hex_token(f, l, ln, cn, tmp, good, eof);
    val := std_ulogic_vector(tmp);
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_hex_token(f, l, ln, cn, tmp, good, eof);
    val := to_integer(tmp);
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned) is
    variable good, eof: boolean;
  begin
    get_hex_token(f, l, ln, cn, val, good, eof);
    if eof then
      syntax_error(l, ln, cn, "unexpected end of file");
    end if;
    assert good and not eof severity failure;
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_hex_token(f, l, ln, cn, tmp);
    val := std_ulogic_vector(tmp);
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_hex_token(f, l, ln, cn, tmp);
    val := to_integer(tmp);
  end procedure get_hex_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
  begin
    next_token(f, l, ln, cn, tmp);
    if tmp then -- End of file
      good := true;
      eof  := true;
      return;
    end if;
    eof := false;
    dec_read(l, cn, val, tmp);
    if not tmp then -- Next token is not decimal or overflow
      good := false;
      syntax_error(l, ln, cn, "not a decimal value or overflow");
      return;
    end if;
    good := true;
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_dec_token(f, l, ln, cn, tmp, good, eof);
    val := std_ulogic_vector(tmp);
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_dec_token(f, l, ln, cn, tmp, good, eof);
    val := to_integer(tmp);
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned) is
    variable good, eof: boolean;
  begin
    get_dec_token(f, l, ln, cn, val, good, eof);
    if eof then
      syntax_error(l, ln, cn, "unexpected end of file");
    end if;
    assert good and not eof severity failure;
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_dec_token(f, l, ln, cn, tmp);
    val := std_ulogic_vector(tmp);
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_dec_token(f, l, ln, cn, tmp);
    val := to_integer(tmp);
  end procedure get_dec_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
  begin
    next_token(f, l, ln, cn, tmp);
    if tmp then -- End of file
      good := true;
      eof  := true;
      return;
    end if;
    eof := false;
    bin_read(l, cn, val, tmp);
    if not tmp then -- Next token is not binary or overflow
      good := false;
      syntax_error(l, ln, cn, "not a binary value or overflow");
      return;
    end if;
    good := true;
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_bin_token(f, l, ln, cn, tmp, good, eof);
    val := std_ulogic_vector(tmp);
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_bin_token(f, l, ln, cn, tmp, good, eof);
    val := to_integer(tmp);
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned) is
    variable good, eof: boolean;
  begin
    get_bin_token(f, l, ln, cn, val, good, eof);
    if eof then
      syntax_error(l, ln, cn, "unexpected end of file");
    end if;
    assert good and not eof severity failure;
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_bin_token(f, l, ln, cn, tmp);
    val := std_ulogic_vector(tmp);
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_bin_token(f, l, ln, cn, tmp);
    val := to_integer(tmp);
  end procedure get_bin_token;

  procedure get_bool_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out boolean; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
  begin
    next_token(f, l, ln, cn, tmp);
    if tmp then -- End of file
      good := true;
      eof  := true;
      return;
    end if;
    eof := false;
    bool_read(l, cn, val, tmp);
    if not tmp then -- Next token is not boolean (0 or 1)
      good := false;
      syntax_error(l, ln, cn, "not a boolean value (0 or 1)");
      return;
    end if;
    good := true;
  end procedure get_bool_token;

  procedure get_bool_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out boolean) is
    variable good, eof: boolean;
  begin
    get_bool_token(f, l, ln, cn, val, good, eof);
    if eof then
      syntax_error(l, ln, cn, "unexpected end of file");
    end if;
    assert good and not eof severity failure;
  end procedure get_bool_token;

  procedure push_request_response(variable vie: inout vci_initiator_emulator; req: in vci_request_type; rsp: in vci_response_type; msk: in vci_data_type; gp: in boolean := false) is
    variable tmp_req: vie_requests_fifo_entry_ptr;
    variable tmp_rsp: vie_responses_fifo_entry_ptr;
  begin
    tmp_req     := new vie_requests_fifo_entry;
    tmp_req.req := req;
    tmp_req.nxt := null;
    if vie.reqcnt = 0 then -- FIFO empty
      tmp_req.prv := null;
      vie.reqbot  := tmp_req;
      vie.reqtop  := tmp_req;
    else
      tmp_req.prv    := vie.reqbot;
      vie.reqbot.nxt := tmp_req;
      vie.reqbot     := tmp_req;
    end if;
    vie.reqcnt := vie.reqcnt + 1;
    tmp_rsp     := new vie_responses_fifo_entry;
    tmp_rsp.rsp := rsp;
    tmp_rsp.msk := msk;
    tmp_rsp.gp  := gp;
    tmp_rsp.nxt := null;
    if vie.rspcnt = 0 then
      tmp_rsp.prv := null;
      vie.rspbot  := tmp_rsp;
      vie.rsptop  := tmp_rsp;
    else
      tmp_rsp.prv    := vie.rspbot;
      vie.rspbot.nxt := tmp_rsp;
      vie.rspbot     := tmp_rsp;
    end if;
    vie.rspcnt := vie.rspcnt + 1;
  end procedure push_request_response;

  procedure fast_push_request_response(variable vie: inout vci_initiator_emulator; req: in vci_request_type; rsp: in vci_response_type; msk: in vci_data_type; gp: in boolean) is
    variable tmp_req: vie_requests_fifo_entry_ptr;
    variable tmp_rsp: vie_responses_fifo_entry_ptr;
  begin
    tmp_req     := new vie_requests_fifo_entry;
    tmp_req.req := req;
    tmp_req.prv := null;
    if vie.reqcnt = 0 then -- FIFO empty
      tmp_req.nxt := null;
      vie.reqbot  := tmp_req;
      vie.reqtop  := tmp_req;
    else
      tmp_req.nxt    := vie.reqtop;
      vie.reqtop.prv := tmp_req;
      vie.reqtop     := tmp_req;
    end if;
    vie.reqcnt := vie.reqcnt + 1;
    tmp_rsp     := new vie_responses_fifo_entry;
    tmp_rsp.rsp := rsp;
    tmp_rsp.msk := msk;
    tmp_rsp.gp  := gp;
    tmp_rsp.prv := null;
    if vie.rspcnt = 0 then
      tmp_rsp.nxt := null;
      vie.rspbot  := tmp_rsp;
      vie.rsptop  := tmp_rsp;
    else
      tmp_rsp.nxt    := vie.rsptop;
      vie.rsptop.prv := tmp_rsp;
      vie.rsptop     := tmp_rsp;
    end if;
    vie.rspcnt := vie.rspcnt + 1;
  end procedure fast_push_request_response;

  procedure push_regs_read_request(variable vie: inout vci_initiator_emulator; idx: in natural range 0 to 31; be: in vci_be_type := (others => '1'); regs: in regs_type; regs_msk: in regs_type; gp: in boolean := false) is
    variable req: vci_request_type;
    variable rsp: vci_response_type;
    variable msk: vci_data_type;
  begin
    req.cmdval   := '1';
    req.address  := idx2hst_add(idx);
    req.be       := be;
    req.cmd      := vci_cmd_read;
    req.wdata    := (others => '0');
    req.srcid    := vci_srcid_rnd;
    req.trdid    := regs_trdid;
    req.pktid    := vci_pktid_rnd;
    req.eop      := '1';
    rsp.rspval   := '1';
    rsp.rdata    := regs(idx);
    rsp.rerror   := (others => '0');
    rsp.reop     := req.eop;
    rsp.rsrcid   := req.srcid;
    rsp.rtrdid   := req.trdid;
    rsp.rpktid   := req.pktid;
    msk          := mask_bytes(regs_msk(idx), be);
    push_request_response(vie, req, rsp, msk, gp);
  end procedure push_regs_read_request;

  procedure fast_push_regs_read_request(variable vie: inout vci_initiator_emulator; idx: in natural range 0 to 31; be: in vci_be_type := (others => '1'); regs: in regs_type; regs_msk: in regs_type; gp: in boolean := false) is
    variable req: vci_request_type;
    variable rsp: vci_response_type;
    variable msk: vci_data_type;
  begin
    req.cmdval   := '1';
    req.address  := idx2hst_add(idx);
    req.be       := be;
    req.cmd      := vci_cmd_read;
    req.wdata    := (others => '0');
    req.srcid    := vci_srcid_rnd;
    req.trdid    := fast_regs_trdid;
    req.pktid    := vci_pktid_rnd;
    req.eop      := '1';
    rsp.rspval   := '1';
    rsp.rdata    := regs(idx);
    rsp.rerror   := (others => '0');
    rsp.reop     := req.eop;
    rsp.rsrcid   := req.srcid;
    rsp.rtrdid   := req.trdid;
    rsp.rpktid   := req.pktid;
    msk          := mask_bytes(regs_msk(idx), be);
    fast_push_request_response(vie, req, rsp, msk, gp);
  end procedure fast_push_regs_read_request;

  procedure push_regs_write_request(variable vie: inout vci_initiator_emulator; idx: in natural range 0 to 31; be: in vci_be_type := (others => '1'); regs: in regs_type) is
    variable req: vci_request_type;
    variable rsp: vci_response_type;
    variable msk: vci_data_type;
  begin
    req.cmdval   := '1';
    req.address  := idx2hst_add(idx);
    req.be       := be;
    req.cmd      := vci_cmd_write;
    req.wdata    := regs(idx);
    req.srcid    := vci_srcid_rnd;
    req.trdid    := vci_trdid_rnd;
    req.trdid    := regs_trdid;
    req.pktid    := vci_pktid_rnd;
    req.eop      := '1';
    rsp.rspval   := '1';
    rsp.rdata    := (others => '0');
    rsp.rerror   := (others => '0');
    rsp.reop     := req.eop;
    rsp.rsrcid   := req.srcid;
    rsp.rtrdid   := req.trdid;
    rsp.rpktid   := req.pktid;
    msk          := (others => '0');
    push_request_response(vie, req, rsp, msk);
  end procedure push_regs_write_request;

  procedure free(variable vie: inout vci_initiator_emulator) is
    variable req: vie_requests_fifo_entry_ptr;
    variable rsp: vie_responses_fifo_entry_ptr;
    variable pss_st:  vie_pss_status_fifo_entry_ptr;
    variable dma_st:  vie_dma_status_fifo_entry_ptr;
  begin
    while vie.reqcnt /= 0 loop
      req := vie.reqtop.nxt;
      deallocate(vie.reqtop);
      vie.reqcnt := vie.reqcnt - 1;
      vie.reqtop := req;
    end loop;
    while vie.rspcnt /= 0 loop
      rsp := vie.rsptop.nxt;
      deallocate(vie.rsptop);
      vie.rspcnt := vie.rspcnt - 1;
      vie.rsptop := rsp;
    end loop;
    while vie.dmacnt /= 0 loop
      dma_st := vie.dmatop.nxt;
      deallocate(vie.dmatop);
      vie.dmacnt := vie.dmacnt - 1;
      vie.dmatop := dma_st;
    end loop;
    while vie.psscnt /= 0 loop
      pss_st := vie.psstop.nxt;
      deallocate(vie.psstop);
      vie.psscnt := vie.psscnt - 1;
      vie.psstop := pss_st;
    end loop;
    vie.reqbot := null;
    vie.reqtop := null;
    vie.rspbot := null;
    vie.rsptop := null;
    vie.dmabot := null;
    vie.dmatop := null;
    vie.pssbot  := null;
    vie.psstop  := null;
  end procedure free;

  procedure is_empty(variable vie: in vci_initiator_emulator; good: out boolean) is
  begin
    good := vie.reqcnt = 0 and vie.rspcnt = 0 and vie.dmacnt = 0 and vie.psscnt = 0;
  end procedure is_empty;

  procedure is_responses_fifo_empty(variable vie: in vci_initiator_emulator; good: out boolean) is
  begin
    good := vie.rspcnt = 0;
  end procedure is_responses_fifo_empty;

  procedure push_pss_error_status_data(variable vie: inout vci_initiator_emulator; val: in std_ulogic; err: in std_ulogic; status: in status_type; data: in data_type; be: in vci_be_type) is
    variable tmp: vie_pss_status_fifo_entry_ptr;
  begin
    tmp        := new vie_pss_status_fifo_entry;
    tmp.val    := val;
    tmp.err    := err;
    tmp.status := status;
    tmp.data   := data;
    tmp.be     := be;
    tmp.nxt    := null;
    if vie.psscnt = 0 then -- FIFO empty
      tmp.prv   := null;
      vie.pssbot := tmp;
      vie.psstop := tmp;
    else
      tmp.prv       := vie.pssbot;
      vie.pssbot.nxt := tmp;
      vie.pssbot     := tmp;
    end if;
    vie.psscnt     := vie.psscnt + 1;
  end procedure push_pss_error_status_data;

  procedure push_dma_error_status(variable vie: inout vci_initiator_emulator; val: in std_ulogic; err: in std_ulogic; status: in status_type) is
    variable tmp: vie_dma_status_fifo_entry_ptr;
  begin
    tmp        := new vie_dma_status_fifo_entry;
    tmp.val    := val;
    tmp.err    := err;
    tmp.status := status;
    tmp.nxt    := null;
    if vie.dmacnt = 0 then -- FIFO empty
      tmp.prv    := null;
      vie.dmabot := tmp;
      vie.dmatop := tmp;
    else
      tmp.prv        := vie.dmabot;
      vie.dmabot.nxt := tmp;
      vie.dmabot     := tmp;
    end if;
    vie.dmacnt     := vie.dmacnt + 1;
  end procedure push_dma_error_status;

  procedure pop_pss_error_status_data(variable vie: inout vci_initiator_emulator; val: out std_ulogic; err: out std_ulogic; status: out status_type; data: out data_type; be: out vci_be_type) is
    variable tmp: vie_pss_status_fifo_entry_ptr;
  begin
    assert vie.psscnt /= 0 report "FIFO empty" severity failure;
    tmp           := vie.psstop;
    val           := tmp.val;
    err           := tmp.err;
    status        := tmp.status;
    data          := tmp.data;
    be            := tmp.be;
    if vie.psscnt = 1 then
      vie.psstop := null;
      vie.pssbot := null;
    else
      vie.psstop     := tmp.nxt;
      vie.psstop.prv := null;
    end if;
    vie.psscnt     := vie.psscnt - 1;
    deallocate(tmp);
  end procedure pop_pss_error_status_data;

  procedure pop_dma_error_status(variable vie: inout vci_initiator_emulator; val: out std_ulogic; err: out std_ulogic; status: out status_type) is
    variable tmp: vie_dma_status_fifo_entry_ptr;
  begin
    assert vie.dmacnt /= 0 report "FIFO empty" severity failure;
    tmp            := vie.dmatop;
    val            := tmp.val;
    err            := tmp.err;
    status         := tmp.status;
    if vie.dmacnt = 1 then
      vie.dmatop := null;
      vie.dmabot := null;
    else
      vie.dmatop     := tmp.nxt;
      vie.dmatop.prv := null;
    end if;
    vie.dmacnt     := vie.dmacnt - 1;
    deallocate(tmp);
  end procedure pop_dma_error_status;

  procedure pop_request(variable vie: inout vci_initiator_emulator; variable req: in vie_requests_fifo_entry_ptr) is
    variable tmp: vie_requests_fifo_entry_ptr := req;
  begin
    if tmp.prv /= null and tmp.nxt /= null then   -- Middle of list
      tmp.prv.nxt := tmp.nxt;
      tmp.nxt.prv := tmp.prv;
    elsif tmp.prv /= null and tmp.nxt = null then -- Last of list
      vie.reqbot     := tmp.prv;
      vie.reqbot.nxt := null;
    elsif tmp.prv = null and tmp.nxt /= null then -- First of list
      vie.reqtop     := tmp.nxt;
      vie.reqtop.prv := null;
    else -- Single in list
      vie.reqtop := null;
      vie.reqbot := null;
    end if;
    vie.reqcnt := vie.reqcnt - 1;
    deallocate(tmp);
  end procedure pop_request;

  procedure pop_response(variable vie: inout vci_initiator_emulator; variable rsp: in vie_responses_fifo_entry_ptr) is
    variable tmp: vie_responses_fifo_entry_ptr := rsp;
  begin
    if tmp.prv /= null and tmp.nxt /= null then   -- Middle of list
      tmp.prv.nxt := tmp.nxt;
      tmp.nxt.prv := tmp.prv;
    elsif tmp.prv /= null and tmp.nxt = null then -- Last of list
      vie.rspbot     := tmp.prv;
      vie.rspbot.nxt := null;
    elsif tmp.prv = null and tmp.nxt /= null then -- First of list
      vie.rsptop     := tmp.nxt;
      vie.rsptop.prv := null;
    else -- Single in list
      vie.rsptop := null;
      vie.rspbot := null;
    end if;
    vie.rspcnt := vie.rspcnt - 1;
    deallocate(tmp);
  end procedure pop_response;

  procedure play(variable vie: inout vci_initiator_emulator; signal req: out vci_request_type) is
  begin
    assert vie.reqcnt /= 0 report "FIFO empty" severity failure;
    req <= vie.reqtop.req;
    pop_request(vie, vie.reqtop);
  end procedure play;

  procedure check(mes: in string; got: in std_ulogic; exp: in std_ulogic) is
  begin
    assert got = exp report mes & ": got " & std2char(got) & ", expected " & std2char(exp) severity failure;
  end procedure check;

  procedure check(mes: in string; got: in std_ulogic; exp: in std_ulogic; msk: in std_ulogic) is
  begin
    assert (got and msk) = (exp and msk) report mes & ": got " & std2char(got) & ", expected " & std2char(exp) & " (mask: " & std2char(msk) & ")" severity failure;
  end procedure check;

  procedure check(mes: in string; got: in std_ulogic_vector; exp: in std_ulogic_vector) is
  begin
    assert got = exp report mes & ": got " & vec2hexstr(got) & ", expected " & vec2hexstr(exp) severity failure;
  end procedure check;

  procedure check(mes: in string; got: in std_ulogic_vector; exp: in std_ulogic_vector; msk: in std_ulogic_vector) is
  begin
    assert (got and msk) = (exp and msk) report mes & ": got " & vec2hexstr(got) & ", expected " & vec2hexstr(exp) & " (mask: " & vec2hexstr(msk) & ")" severity failure;
  end procedure check;

  procedure check(variable vie: inout vci_initiator_emulator; rsp: in vci_response_type; signal gpovalid: out boolean; signal gpo: out word64) is
    variable tmp: vie_responses_fifo_entry_ptr;
    variable gp:  boolean;
  begin
    assert vie.rspcnt /= 0 report "FIFO empty" severity failure;
    tmp := vie.rsptop;
    gp  := vie.rsptop.gp;
    while rsp.rtrdid /= tmp.rsp.rtrdid loop
      assert tmp.nxt /= null report "No response of this class in FIFO" severity failure;
      tmp := tmp.nxt;
    end loop;
    check("RSPVAL mismatch", rsp.rspval, tmp.rsp.rspval);
    if gp then
      gpovalid <= true;
      gpo      <= rsp.rdata;
    else
      gpovalid <= false;
      gpo      <= (others => '0');
      check("RDATA mismatch", rsp.rdata, tmp.rsp.rdata, tmp.msk);
    end if;
    check("RERROR mismatch", rsp.rerror, tmp.rsp.rerror);
    check("REOP mismatch", rsp.reop, tmp.rsp.reop);
    check("RSRCID mismatch", rsp.rsrcid, tmp.rsp.rsrcid);
    check("RTRDID mismatch", rsp.rtrdid, tmp.rsp.rtrdid);
    check("RPKTID mismatch", rsp.rpktid, tmp.rsp.rpktid);
    pop_response(vie, tmp);
  end procedure check;

  procedure push_request_response(variable vte: inout vci_target_emulator; req: in vci_request_type; msk: in vci_request_type; rsp: in vci_response_type; gp: in boolean := false) is
    variable tmp_req: vte_requests_fifo_entry_ptr;
    variable tmp_rsp: vte_responses_fifo_entry_ptr;
  begin
    tmp_req     := new vte_requests_fifo_entry;
    tmp_req.req := req;
    tmp_req.msk := msk;
    tmp_req.gp  := gp;
    tmp_req.nxt := null;
    if vte.reqcnt  = 0 then
      tmp_req.prv := null;
      vte.reqbot  := tmp_req;
      vte.reqtop  := tmp_req;
    else
      tmp_req.prv    := vte.reqbot;
      vte.reqbot.nxt := tmp_req;
      vte.reqbot     := tmp_req;
    end if;
    vte.reqcnt := vte.reqcnt + 1;
    tmp_rsp        := new vte_responses_fifo_entry;
    tmp_rsp.rsp    := rsp;
    tmp_rsp.nxt    := null;
    if vte.rspcnt  = 0 then
      tmp_rsp.prv := null;
      vte.rspbot  := tmp_rsp;
      vte.rsptop  := tmp_rsp;
      vte.rspprp  := tmp_rsp;
    else
      tmp_rsp.prv    := vte.rspbot;
      vte.rspbot.nxt := tmp_rsp;
      vte.rspbot     := tmp_rsp;
    end if;
    vte.rspcnt := vte.rspcnt + 1;
  end procedure push_request_response;

  procedure free(variable vte: inout vci_target_emulator) is
    variable req: vte_requests_fifo_entry_ptr;
    variable rsp: vte_responses_fifo_entry_ptr;
  begin
    while vte.reqcnt /= 0 loop
      req := vte.reqtop.nxt;
      deallocate(vte.reqtop);
      vte.reqcnt := vte.reqcnt - 1;
      vte.reqtop := req;
    end loop;
    while vte.rspcnt /= 0 loop
      rsp := vte.rsptop.nxt;
      deallocate(vte.rsptop);
      vte.rspcnt := vte.rspcnt - 1;
      vte.rsptop := rsp;
    end loop;
    vte.reqbot := null;
    vte.reqtop := null;
    vte.rspbot := null;
    vte.rsptop := null;
    vte.rspprp := null;
    vte.rsprdy := 0;
  end procedure free;

  procedure is_empty(variable vte: in vci_target_emulator; good: out boolean) is
  begin
    good := vte.reqcnt = 0 and vte.rspcnt = 0;
  end procedure is_empty;

  procedure pop_request(variable vte: inout vci_target_emulator; variable req: in vte_requests_fifo_entry_ptr) is
    variable tmp: vte_requests_fifo_entry_ptr := req;
  begin
    if tmp.prv /= null and tmp.nxt /= null then   -- Middle of list
      tmp.nxt.prv := tmp.prv;
      tmp.prv.nxt := tmp.nxt;
    elsif tmp.prv /= null and tmp.nxt = null then -- Last of list
      vte.reqbot     := tmp.prv;
      vte.reqbot.nxt := null;
    elsif tmp.prv = null and tmp.nxt /= null then -- First of list
      vte.reqtop     := tmp.nxt;
      vte.reqtop.prv := null;
    else -- Single in list
      vte.reqtop := null;
      vte.reqbot := null;
    end if;
    vte.reqcnt := vte.reqcnt - 1;
    deallocate(tmp);
  end procedure pop_request;

  procedure pop_response(variable vte: inout vci_target_emulator; variable rsp: in vte_responses_fifo_entry_ptr) is
    variable tmp: vte_responses_fifo_entry_ptr := rsp;
  begin
    if tmp.prv /= null and tmp.nxt /= null then   -- Middle of list
      tmp.nxt.prv := tmp.prv;
      tmp.prv.nxt := tmp.nxt;
    elsif tmp.prv /= null and tmp.nxt = null then -- Last of list
      vte.rspbot     := tmp.prv;
      vte.rspbot.nxt := null;
    elsif tmp.prv = null and tmp.nxt /= null then -- First of list
      vte.rsptop     := tmp.nxt;
      vte.rsptop.prv := null;
    else -- Single in list
      vte.rsptop := null;
      vte.rspbot := null;
    end if;
    vte.rspcnt := vte.rspcnt - 1;
    vte.rsprdy := vte.rsprdy - 1;
    deallocate(tmp);
  end procedure pop_response;

  procedure play(variable vte: inout vci_target_emulator; signal rsp: out vci_response_type) is
  begin
    assert vte.rsprdy /= 0 report "FIFO empty" severity failure;
    rsp <= vte.rsptop.rsp;
    pop_response(vte, vte.rsptop);
  end procedure play;

  procedure check(variable vte: inout vci_target_emulator; req: in vci_request_type; signal gpovalid: out boolean; signal gpo: out word64) is
    variable tmp: vci_request_type;
    variable msk: vci_request_type;
    variable rsp: vte_responses_fifo_entry_ptr;
    variable gp:  boolean;
  begin
    assert vte.reqcnt /= 0 report "Requests FIFO empty" severity failure;
    tmp := vte.reqtop.req;
    msk := vte.reqtop.msk;
    gp  := vte.reqtop.gp;
    check("CMDVAL mismatch", req.cmdval, tmp.cmdval, msk.cmdval);
    check("ADDRESS mismatch", req.address, tmp.address, msk.address);
    check("BE mismatch", req.be, tmp.be, msk.be);
    check("CMD mismatch", req.cmd, tmp.cmd, msk.cmd);
    if gp then
      gpovalid <= true;
      gpo      <= req.wdata;
    else
      gpovalid <= false;
      gpo      <= (others => '0');
      check("WDATA mismatch", req.wdata, tmp.wdata, msk.wdata);
    end if;
    check("SRCID mismatch", req.srcid, tmp.srcid, msk.srcid);
    check("TRDID mismatch", req.trdid, tmp.trdid, msk.trdid);
    check("PKTID mismatch", req.pktid, tmp.pktid, msk.pktid);
    check("EOP mismatch", req.eop, tmp.eop, msk.eop);
    pop_request(vte, vte.reqtop);
    assert vte.rspcnt /= 0 report "Responses FIFO empty" severity failure;
    rsp            := vte.rspprp;
    rsp.rsp.reop   := req.eop;
    rsp.rsp.rsrcid := req.srcid;
    rsp.rsp.rtrdid := req.trdid;
    rsp.rsp.rpktid := req.pktid;
    vte.rspprp     := rsp.nxt;
    vte.rsprdy     := vte.rsprdy + 1;
  end procedure check;

  procedure parse_dw(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator; vte: inout vci_target_emulator) is
    variable sadd: vci_address_type;
    variable dadd: vci_address_type;
    variable len:  natural;
    variable be:   vci_be_type;
    variable regs: regs_type;
    variable req:  vci_request_type;
    variable msk:  vci_request_type;
    variable rsp:  vci_response_type;
    variable w:    vci_data_type;
    variable cst:  boolean;
  begin
    get_hex_token(f, l, ln, cn, dadd);
    get_dec_token(f, l, ln, cn, len);
    if len = 0 then
      syntax_error(l, ln, cn, "null DMA transfer length");
      assert false severity failure;
    end if;
    get_bool_token(f, l, ln, cn, cst);
    get_hex_token(f, l, ln, cn, be);
    regs(dcfg_idx) := (others => '0');
    set_flag(regs, dls_field_def, '0');
    set_flag(regs, dld_field_def, '1');
    if cst then
      set_flag(regs, dfd_field_def, '1');
    end if;
    set_field(regs, dbe_field_def, be);
    set_field(regs, dlenm1_field_def, std_ulogic_vector(to_unsigned(8 * len - 1, 32))); -- Byte length
    push_regs_write_request(vie => vie, idx => dcfg_idx, regs => regs);
    sadd := std_ulogic_vector_rnd(29) & "000";
    set_field(regs, dsrc_field_def, sadd);
    set_field(regs, ddst_field_def, dadd);
    push_regs_write_request(vie => vie, idx => dadd_idx, regs => regs);
    regs(dgost_idx) := (others => '0');
    push_regs_write_request(vie => vie, idx => dgost_idx, regs => regs);
    push_dma_error_status(vie, '1', '0', status_none);
    req.cmdval  := '1';
    req.address := (others => '0');
    req.be      := be;
    req.cmd     := vci_cmd_read;
    msk.cmdval  := '1';
    msk.address := (others => '1');
    msk.be      := (others => '1');
    msk.cmd     := (others => '1');
    msk.wdata   := (others => '0');
    msk.srcid   := (others => '0');
    msk.trdid   := (others => '0');
    msk.pktid   := (others => '0');
    msk.eop     := '0';
    rsp.rspval  := '1';
    rsp.rerror  := (others => '0');
    rsp.reop    := '0';
    rsp.rsrcid  := (others => '0');
    rsp.rtrdid  := (others => '0');
    rsp.rpktid  := (others => '0');
    for i in 0 to len - 1 loop
      req.address := vci_address_type(u_unsigned(sadd) + 8 * i);
      get_hex_token(f, l, ln, cn, w);
      rsp.rdata := w;
      push_request_response(vte, req, msk, rsp);
    end loop;
  end procedure parse_dw;

  procedure parse_dr(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator; vte: inout vci_target_emulator) is
    variable sadd: vci_address_type;
    variable dadd: vci_address_type;
    variable len:  natural;
    variable ber:  vci_be_type;
    variable bec:  vci_be_type;
    variable regs: regs_type;
    variable req:  vci_request_type;
    variable msk:  vci_request_type;
    variable rsp:  vci_response_type;
    variable w:    vci_data_type;
    variable cst:  boolean;
    variable chk:  boolean;
    variable gp:   boolean;
  begin
    get_hex_token(f, l, ln, cn, sadd);
    get_dec_token(f, l, ln, cn, len);
    if len = 0 then
      syntax_error(l, ln, cn, "null DMA transfer length");
      assert false severity failure;
    end if;
    get_bool_token(f, l, ln, cn, cst);
    get_hex_token(f, l, ln, cn, ber);
    get_bool_token(f, l, ln, cn, chk);
    get_bool_token(f, l, ln, cn, gp);
    if chk then
      get_hex_token(f, l, ln, cn, bec);
    else
      bec := (others => '0');
    end if;
    regs(dcfg_idx) := (others => '0');
    set_flag(regs, dls_field_def, '1');
    if cst then
      set_flag(regs, dfs_field_def, '1');
    end if;
    set_flag(regs, dld_field_def, '0');
    set_field(regs, dbe_field_def, ber);
    set_field(regs, dlenm1_field_def, std_ulogic_vector(to_unsigned(8 * len - 1, 32))); -- Byte length
    push_regs_write_request(vie => vie, idx => dcfg_idx, regs => regs);
    dadd := std_ulogic_vector_rnd(29) & "000";
    set_field(regs, dsrc_field_def, sadd);
    set_field(regs, ddst_field_def, dadd);
    push_regs_write_request(vie => vie, idx => dadd_idx, regs => regs);
    push_regs_write_request(vie => vie, idx => dgost_idx, regs => regs);
    push_dma_error_status(vie, '1', '0', status_none);
    req.cmdval  := '1';
    req.be      := ber;
    req.cmd     := vci_cmd_write;
    msk.cmdval  := '1';
    msk.address := (others => '1');
    msk.be      := (others => '1');
    msk.cmd     := (others => '1');
    msk.wdata   := expand_bits_to_bytes(bec);
    msk.srcid   := (others => '0');
    msk.trdid   := (others => '0');
    msk.pktid   := (others => '0');
    msk.eop     := '0';
    rsp.rspval  := '1';
    rsp.rdata   := (others => '0');
    rsp.rerror  := (others => '0');
    rsp.reop    := '0';
    rsp.rsrcid  := (others => '0');
    rsp.rtrdid  := (others => '0');
    rsp.rpktid  := (others => '0');
    for i in 0 to len - 1 loop
      req.address := vci_address_type(u_unsigned(dadd) + 8 * i);
      if chk then
        get_hex_token(f, l, ln, cn, w);
        req.wdata := w;
      end if;
      push_request_response(vte, req, msk, rsp, gp);
    end loop;
  end procedure parse_dr;

  procedure parse_vw(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator) is
    variable dadd: vci_address_type;
    variable len:  natural;
    variable be:   vci_be_type;
    variable req:  vci_request_type;
    variable rsp:  vci_response_type;
    variable msk:  vci_data_type;
    variable w:    vci_data_type;
    variable cst:  boolean;
  begin
    get_hex_token(f, l, ln, cn, dadd);
    get_dec_token(f, l, ln, cn, len);
    if len = 0 then
      syntax_error(l, ln, cn, "null target VCI transfer length");
      assert false severity failure;
    end if;
    get_bool_token(f, l, ln, cn, cst);
    get_hex_token(f, l, ln, cn, be);
    req.cmdval  := '1';
    req.address := vci_address_type(u_unsigned(dadd));
    req.be      := be;
    req.cmd     := vci_cmd_write;
    req.eop     := '0';
    rsp.rspval  := '1';
    rsp.rerror  := (others => '0');
    msk         := (others => '0');
    for i in 0 to len - 1 loop
      if not cst then
        req.address := vci_address_type(u_unsigned(dadd) + 8 * i);
      end if;
      get_hex_token(f, l, ln, cn, w);
      req.wdata := w;
      req.srcid := vci_srcid_rnd;
      if hst_is_in_regs(dadd) then
        req.trdid := regs_trdid;
      else
        req.trdid := mss_trdid;
      end if;
      req.pktid := vci_pktid_rnd;
      if i = len - 1 then
        req.eop := '1';
      else
        req.eop := '0';
      end if;
      rsp.reop := req.eop;
      rsp.rsrcid := req.srcid;
      rsp.rtrdid := req.trdid;
      rsp.rpktid := req.pktid;
      push_request_response(vie, req, rsp, msk);
    end loop;
  end procedure parse_vw;

  procedure parse_vr(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator) is
    variable dadd: vci_address_type;
    variable len:  natural;
    variable ber:  vci_be_type;
    variable bec:  vci_be_type;
    variable req:  vci_request_type;
    variable rsp:  vci_response_type;
    variable msk:  vci_data_type;
    variable w:    vci_data_type;
    variable cst:  boolean;
    variable chk:  boolean;
    variable gp:   boolean;
  begin
    get_hex_token(f, l, ln, cn, dadd);
    get_dec_token(f, l, ln, cn, len);
    if len = 0 then
      syntax_error(l, ln, cn, "null target VCI transfer length");
      assert false severity failure;
    end if;
    get_bool_token(f, l, ln, cn, cst);
    get_hex_token(f, l, ln, cn, ber);
    get_bool_token(f, l, ln, cn, chk);
    get_bool_token(f, l, ln, cn, gp);
    if chk then
      get_hex_token(f, l, ln, cn, bec);
    else
      bec := (others => '0');
    end if;
    req.cmdval  := '1';
    req.address := vci_address_type(u_unsigned(dadd));
    req.be      := ber;
    req.cmd     := vci_cmd_read;
    req.eop     := '0';
    rsp.rspval  := '1';
    rsp.rerror  := (others => '0');
    if chk then
      msk := expand_bits_to_bytes(bec);
    else
      msk := (others => '0');
    end if;
    for i in 0 to len - 1 loop
      if not cst then
        req.address := vci_address_type(u_unsigned(dadd) + 8 * i);
      end if;
      if chk then
        get_hex_token(f, l, ln, cn, w);
        rsp.rdata  := w;
      end if;
      req.srcid := vci_srcid_rnd;
      if hst_is_in_regs(dadd) then
        req.trdid := regs_trdid;
      else
        req.trdid := mss_trdid;
      end if;
      req.pktid := vci_pktid_rnd;
      if i = len - 1 then
        req.eop := '1';
      else
        req.eop := '0';
      end if;
      rsp.reop   := req.eop;
      rsp.rsrcid := req.srcid;
      rsp.rtrdid := req.trdid;
      rsp.rpktid := req.pktid;
      push_request_response(vie, req, rsp, msk, gp);
    end loop;
  end procedure parse_vr;

  procedure parse_ex(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator; n_pa_regs: natural range 0 to 15) is
    variable status: status_type;
    variable data:   data_type;
    variable tmp:    std_ulogic_vector(0 downto 0);
    variable err:    std_ulogic;
    variable berr:   boolean;
    variable be:     vci_be_type;
    variable req:    vci_request_type;
    variable rsp:    vci_response_type;
    variable msk:    vci_data_type;
    variable w:      vci_data_type;
    variable regs:   regs_type;
  begin
    get_hex_token(f, l, ln, cn, status);
    get_bool_token(f, l, ln, cn, berr);
    if berr then
      err := '1';
    else
      err := '0';
    end if;
    get_hex_token(f, l, ln, cn, data);
    get_hex_token(f, l, ln, cn, be);
    req.cmdval  := '1';
    req.be      := (others => '1');
    req.cmd     := vci_cmd_write;
    req.eop     := '0';
    rsp.rspval  := '1';
    rsp.rdata   := (others => '0');
    rsp.rerror  := (others => '0');
    msk         := (others => '0');
    for i in n_pa_regs - 1 downto 0 loop
      req.address := idx2hst_add(i);
      get_hex_token(f, l, ln, cn, w);
      req.wdata := w;
      req.srcid := vci_srcid_rnd;
      req.trdid := regs_trdid;
      req.pktid := vci_pktid_rnd;
      if i = 0 then
        req.eop := '1';
      else
        req.eop := '0';
      end if;
      rsp.reop   := req.eop;
      rsp.rsrcid := req.srcid;
      rsp.rtrdid := req.trdid;
      rsp.rpktid := req.pktid;
      push_request_response(vie, req, rsp, msk);
    end loop;
    regs(pgost_idx) := (others => '0');
    push_regs_write_request(vie => vie, idx => pgost_idx, regs => regs);
    push_pss_error_status_data(vie, '1', err, status, data, be);
  end procedure parse_ex;

  procedure parse_en(file f: text; l: inout line; ln: inout natural; cn: inout natural; vie: inout vci_initiator_emulator) is
    variable status: status_type;
    variable data:   data_type;
    variable be:     vci_be_type;
    variable err:    std_ulogic;
    variable berr:   boolean;
    variable regs:   regs_type;
  begin
    get_hex_token(f, l, ln, cn, status);
    get_bool_token(f, l, ln, cn, berr);
    if berr then
      err := '1';
    else
      err := '0';
    end if;
    get_hex_token(f, l, ln, cn, data);
    get_hex_token(f, l, ln, cn, be);
    regs(pgost_idx) := (others => '0');
    push_regs_write_request(vie => vie, idx => pgost_idx, regs => regs);
    push_pss_error_status_data(vie, '1', err, status, data, be);
  end procedure parse_en;

end package body hst_emulator_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
