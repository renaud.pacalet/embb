--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation utility package
--*
--*  Result of a split of sim_utils between css_emulator specific declarations and general purpose simulation utilities. The functions defined in this
--* package can be used to parse command files and drive simulations. The syntax of the command files is explained in the following. The targetted simulations
--* are those that simulate only an PSS and its Memory SubSystem. So, the simulation environment emulates the DMA engine, the VCIInterface, the
--* micro-controller and the surrounding system. The emulated DMA engine can be used to store incoming data blocks in the MSS and to read data blocks from the
--* MSS. When it reads from the MSS the read values are compared with the expected one and errors are raised on mismatches. The emulated VCIInterface can be used
--* to launch processings on the PSS and to get their exit status. It can also be used for MSS I/O operations. The micro-controller can also be used for MSS
--* I/O operations. The host system is not (yet) emulated appart clock and reset generation.
--*
--* Syntax of the command files:
--* Comments extend from the '#' character to the end of the line. The file contains a sequence of commands. Each command is made of one or several fields.
--* Fields are separated by spaces, tabs or end of lines. Some numeric fields are in decimal form; all other numeric fields are in hex form. Unless specified as
--* decimal in the following, a numeric field is in hexadecimal form. The first field of a command is a 2 characters command specifier. The following fields in a
--* command, if any, are parameters. The number of parameters depends on the command. They are mandatory and have thus no default value. Supported commands are
--* RS, WR, DW, DR, VW, VR, UW, UR, WD, WV, WU, EX, WE, WC and QT:
--* - RS: ReSet, forces a N cycles reset of PSS. This command takes one parameter:
--*   - N: number of cycles, decimal.
--* - WR: dma WRite. The DMA interface with the memory subsystem is used to write a sequence of 64 bits values. This command takes 3 + LENGTH parameters:
--*   - ADD: start ADDress in the memory subsystem. Double-word address, between 0x00000000 and 0x1fffffff; note that most MSS are less than 0x20000000
--*     double-words long and that out of range accesses will raise assertions from the MSS.
--*   - LENGTH: number of double-words to write, decimal.
--*   - MASK: an 8 bits byte enable MASK. Set bits indicate which byte to write.
--*   - PAYLOAD: the LENGTH 64 bits values to write.
--*     Note: if a DMA transfer is already running the WR command first waits until it ends.
--* - DW: Dma Write, same as WR.
--* - VW: Vci Write. Same as WR and WD but VCIInterface performs the write access instead of DMA.
--* - UW: micro-controller (U) Write. The UC interface with the memory subsystem is used to write a sequence of 8 bits values. This command takes 2 + LENGTH
--*   parameters:
--*   - ADD: start ADDress in the memory subsystem. Byte address, between 0x0000 and 0xffff; note that some MSS are less than 0x10000 bytes long and that out of
--*     range accesses will raise assertions from the MSS.
--*   - LENGTH: number of bytes to write, decimal.
--*   - PAYLOAD: the LENGTH 8 bits values to write.
--*     Note: if a UC transfer is already running the UW command first waits until it ends.
--* - RD: dma ReaD. The DMA interface with the memory subsystem is used to read a sequence of 64 bits values. This command takes 3 + LENGTH parameters:
--*   - ADD: start ADDress in the memory subsystem. Double-word address, between 0x00000000 and 0x1fffffff); note that most MSS are less than 0x20000000
--*     double-words long and that out of range accesses will raise assertions from the MSS.
--*   - LENGTH: number of double-words to read, decimal.
--*   - MASK: an 8 bits byte enable MASK. Set bits indicate which read byte to compare with the expected values.
--*   - PAYLOAD: the LENGTH 64 bits expected values. Masked bytes are ignored. The others are compared with the read ones and assertions are raised on
--*     mismatches.
--*     Note: if a DMA transfer is already running the RD command first waits until it ends.
--* - DR: Dma Read, same as RD
--* - VR: Vci Read. Same as RD and DR but VCIInterface performs the read access instead of DMA.
--* - UR: micro-controller (U) Read. The UC interface with the memory subsystem is used to read a sequence of 8 bits values. This command takes 2 + LENGTH
--*   parameters:
--*   - ADD: start ADDress in the memory subsystem. Byte address, between 0x0000 and 0xffff); note that some MSS are less than 0x10000 bytes long and that out of
--*     range accesses will raise assertions from the MSS.
--*   - LENGTH: number of bytes to read, decimal.
--*   - PAYLOAD: the LENGTH 8 bits expected values. They are compared with the read ones and assertions are raised on mismatches.
--*     Note: if a UC transfer is already running the UR command first waits until it ends.
--* - WD: Wait until end of Dma read or write operation. The next command will be taken into account only after the currently ongoing DMA transfer (if any) ends.
--*   Ignored when there is no ongoing DMA transfer. This command takes one parameter:
--*   - T: timeout in cycles, decimal. If a DMA transfer is currently running and does not complete in T, an assertion is fired with severity error. Zero means
--*    'forever'.
--* - WV: Wait until end of Vci read or write operation. Same as WD but for VCIInterface.
--* - WU: Wait until end of Uc read or write operation. Same as WD but for UC.
--* - EX: EXec command. Launches a processing on PSS. This command takes 2 + CMDSIZE/8 parameters:
--*   - STATUS: the expected exit status.
--*   - ERROR: the expected error flag, decimal.
--*   - PARAM: the processing parameter that must be sent to the PSS, split in CMDSIZE/8 64 bits words, in hex form, MSB first. An error is raised if the
--*     exit status/error sent back by the PSS does not match the expected one. Note: if a processing is already running the EX command first waits until it
--*     ends.
--* - WE: Wait until end of Exec command. The next command will be taken into account only after the currently ongoing PSS processing (if any) ends. Ignored
--*   when there is no ongoing PSS processing. This command takes one parameter:
--*   - T: timeout in cycles, decimal. If a processing is currently running and does not complete in T cycles, an assertion is fired with severity error. Zero
--*     means 'forever'.
--* - WC: Wait Cycles. This command is used to wait for a given number of cycles before the next command is taken into account. It takes one parameter:
--*   - N: number of cycles to wait for, decimal.
--* - QT: QuiT. Stops the clock. The simulation should end gracefully. If it does not, there is a problem somewhere... This command takes no parameter.
--*
--* Example:
--* 
--* RS 10                   # Reset PSS for 10 cycles
--* 
--* WR 0 4 ff               # Write (DMA), at address 0, four double-word, every byte of them.
--* 
--* 0003000200010000        # The double-words to write.
--* 0007000600050004
--* 000B000A00090008
--* 000F000E000D000C
--* 
--* UW A000 16              # Write (UC), at byte address A000, 16 bytes.
--* 
--* 00 01 02 03 04 05 06 07 # The 16 bytes to write.
--* 08 09 0A 0B 0C 0D 0E 0F
--* 
--* WD 10000                # Wait until end of DMA write operation with a 10000 cycles timeout.
--* 
--* EX 0 0                  # Launch a processing in PSS. In this example the processing parameter is 00810000 (hex form), the expected exit status is 0 and
--* 00810000                # the expected error flag is 0.
--* 
--* WE 1000000              # Wait until end of computation or 1000000 cycles.
--* 
--* WU 10000                # Wait until end of UC write operation or 10000 cycles.
--* 
--* VR C000 2 ff            # Read (VCIInterface), at double-word address C000, 2 double-words and compare every byte of them with the expected ones.
--* 
--* # The 2 expected double-words.
--* 0302010007060504 0B0A09080F0E0D0C
--*
--* WC 1000                 # Wait for 1000 Cycles.
--* 
--* WV 1000000              # Wait until end of VCIInterface read operation or 1000000 cycles.
--* 
--* EX 1 1                  # Launch a processing in PSS. In this example the processing parameter is 810001 (hex form), the expected exit status is 1 and
--* 00810001                # the expected error flag is 1.
--* 
--* WE 1000000              # Wait until end of computation or 1000000 cycles.
--* QT                      # Quit

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

use work.global.all;
use work.utils.all;
use work.sim_utils.all;

package css_emulator_pkg is

  --* skip reads characters in line l and in file f, skipping spaces, tabulations and comments (from the character '#' to the end of the line). When it reaches
  --* the end of the line it reads a new line in file f. If eof is false, then cn points to the first non-space, non-comment character. If eof is true, then the
  --* end of the file was encountered. ln is incremented each time a new line is read, which can be used to keep track of the file line number. Similarly cn is
  --* incremented each time a new character is read in a line. The full line containing the first non-space character is returned as l.all.
  procedure skip(file f: text; ln: inout natural; cn: inout natural; l: inout line; eof: out boolean);

  --* same as sim_utils.hread but skips leading spaces and comments and leaves line unmodified.
  procedure hread(file f: text; ln: inout natural; cn: inout natural;
    l: inout line; val: out std_ulogic_vector; good: out boolean);

  --* same as sim_utils.hread but skips leading spaces and comments and leaves line unmodified.
  procedure hread(file f: text; ln: inout natural; cn: inout natural;
    l: inout line; val: out natural; good: out boolean);

  --* skips leading spaces and comments, reads a natural in decimal notation. leaves line unmodified.
  procedure nread(file f: text; ln: inout natural; cn: inout natural;
    l: inout line; val: out natural; good: out boolean);

  --* type defining the commands in a command file
  type cmd_type is (RS, WR, RD, DW, DR, VW, VR, UW, UR, WD, WV, WU, EX, WE, WC, QT);

  --* reads a two characters command in the line l / command file f; first skips the leading spaces and comments.
  procedure read(file f: text; ln: inout natural; cn: inout natural;
    l: inout line; cmd: out cmd_type; good: out boolean; eof: out boolean);

  --* reset forces a n cycles long reset of the PSS.
  procedure reset(signal clk: in std_ulogic; signal css2pss: out css2pss_type; n: in natural);

  --* write puts a write request on a dma2mss or vci2mss interface. Because this version does not wait until a rising edge of clk nor the grant of the MSS
  --* arbiter, and because it does not deassert the request, it is not intended to be used directly. Prefer the other one.
  procedure write(add: in word64_address_type; data: in word64; be: in word64_be_type; signal dma2mss: out dma2mss_type);

  --* write puts a write request on a dma2mss or vci2mss interface and waits until it is granted by the MSS arbiter.
  procedure write(add: in word64_address_type; data: in word64; be: in word64_be_type; signal clk: in std_ulogic; signal mss2dma: mss2dma_type;
                  signal dma2mss: out dma2mss_type);

  --* write puts a write request on the uc2mss interface. Because this version does not wait until a rising edge of clk nor the grant of the MSS arbiter, and
  --* because it does not deassert the request, it is not intended to be used directly. Prefer the other one.
  procedure write(add: in uc_address_type; data: in uc_data_type; signal uc2mss: out uc2mss_type);

  --* write puts a write request on the uc2mss interface and waits until it is granted by the MSS arbiter.
  procedure write(add: in uc_address_type; data: in uc_data_type; signal clk: in std_ulogic; signal mss2uc: mss2uc_type; signal uc2mss: out uc2mss_type);

  --* read puts a read request on a dma2mss or vci2mss interface. Because this version does not wait until a rising edge of clk nor the grant of the MSS arbiter,
  --* and because it does not deassert the request, it is not intended to be used directly. Prefer the other one.
  procedure read(add: in word64_address_type; signal dma2mss: out dma2mss_type);

  --* read puts a read request on a dma2mss or vci2mss interface and waits until it is granted by the MSS arbiter.
  procedure read(add: in word64_address_type; signal clk: in std_ulogic; signal mss2dma: mss2dma_type; signal dma2mss: out dma2mss_type);

  --* read puts a read request on the uc2mss interface. Because this version does not wait until a rising edge of clk nor the grant of the MSS arbiter, and
  --* because it does not deassert the request, it is not intended to be used directly. Prefer the other one.
  procedure read(add: in uc_address_type; signal uc2mss: out uc2mss_type);

  --* read puts a read request on the uc2mss interface and waits until it is granted by the MSS arbiter.
  procedure read(add: in uc_address_type; signal clk: in std_ulogic; signal mss2uc: mss2uc_type; signal uc2mss: out uc2mss_type);

  --* exec launches a processing by putting p on the param signal and raising the exec signal. It then waits for one rising edge of clk and deasserts exec and
  --* param.
  procedure exec(p: in std_ulogic_vector; signal clk: in std_ulogic; signal css2pss: out css2pss_type; signal param: out std_ulogic_vector);

  --* Pointer on array of word64
  type rw64_sequence_type is access word64_vector;

  --* DMA or target VCI request to MSS
  type dmavci_request_type is record
		rdata: word64_vector(0 to 1);
		rdata_valid: boolean_vector(0 to 1);
    cmd: cmd_type range WR to VR;
    add: word64_address_type;
    msk: word64_be_type;
      n: natural;
    pld: rw64_sequence_type;
  end record;

  --* Pointer on array of word8
  type rw8_sequence_type is access word8_vector;

  --* UC request to MSS
  type uc_request_type is record
		rdata: word64_vector(0 to 1);
		rdata_valid: boolean_vector(0 to 1);
    cmd: cmd_type range UW to UR;
    add: uc_address_type;
      n: natural;
    pld: rw64_sequence_type;
  end record;

  --* PSS request
  type pss_request_type is record
		status: status_type;
		err: std_ulogic;
  end record;

  --* Read a DMA or VCI request to MSS from command file
  procedure read(file f: text; ln: inout natural; cn: inout natural; l: inout line; r: inout dmavci_request_type; good: out boolean);

  --* Read a UC request to MSS from command file
  procedure read(file f: text; ln: inout natural; cn: inout natural; l: inout line; u: inout uc_request_type; good: out boolean);

end package css_emulator_pkg;

package body css_emulator_pkg is

  procedure skip(file f: text; ln: inout natural; cn: inout natural; l: inout line; eof: out boolean) is
    variable c: character;
  begin
    eof := true;
    l1: loop
      if l = null or cn > l'length then
        exit l1 when endfile(f);
        readline(f, l);
        cn := 1;
        ln := ln + 1;
        next l1;
      end if;
      l2: while l(cn) = ' ' or l(cn) = HT or l(cn) = CR loop
        cn := cn + 1;
        next l1 when cn > l'length;
      end loop l2;
      if l(cn) = '#' then
        exit l1 when endfile(f);
        readline(f, l);
        cn := 1;
        ln := ln + 1;
        next l1;
      end if;
      eof := false;
      exit l1;
    end loop l1;
  end procedure skip;

  procedure hread(file f: text; ln: inout natural; cn: inout natural; l: inout line; val: out std_ulogic_vector; good: out boolean) is
		constant rl: positive := val'length + 4;
    variable res: u_unsigned(rl - 1 downto 0);
		variable v: integer range -1 to 15;
		variable tmp: boolean;
  begin
		good := false;
		res := (others => '0');
    skip(f, ln, cn, l, tmp);
		if tmp then -- if end of file
			return;
		end if;
		v := char2hex(l(cn));
		-- first non-space character is not hexadecimal
		if v = -1 then
			return;
		end if;
		loop
			res := res(rl - 5 downto 0) & to_unsigned(v, 4);
			if res(rl - 1 downto rl - 4) /= x"0" then -- overflow
			  assert false
			    report "*** hread:" & integer'image(ln) & ":" & integer'image(cn) & ": overflow" & CR & "*** """ & l.all & """" & CR &
								 "*** could not convert hex value on " & integer'image(rl - 4) & " bits"
			    severity failure;
			  return;
			end if;
			cn := cn + 1;
			if cn > l'length then -- end of line
		    good := true;
				val := std_ulogic_vector(res(rl - 5 downto 0));
				return;
			end if;
		  v := char2hex(l(cn));
			if v = -1 then -- non-hexadecimal character
		    good := true;
				val := std_ulogic_vector(res(rl - 5 downto 0));
				return;
			end if;
		end loop;
  end procedure hread;

  procedure hread(file f: text; ln: inout natural; cn: inout natural; l: inout line; val: out natural; good: out boolean) is
    variable eof: boolean;
		variable v: std_ulogic_vector(30 downto 0);
  begin
		good := false;
    skip(f, ln, cn, l, eof);
    if not eof then
      hread(f, ln, cn, l, v, good);
      val := to_integer(u_unsigned(v));
    end if;
  end procedure hread;

  procedure nread(file f: text; ln: inout natural; cn: inout natural; l: inout line; val: out natural; good: out boolean) is
    variable res: u_unsigned(34 downto 0);
    variable resl: u_unsigned(69 downto 0);
		variable v: integer range -1 to 9;
		variable tmp: boolean;
  begin
		good := false;
		res := (others => '0');
    skip(f, ln, cn, l, tmp);
		if tmp then -- if end of file
			return;
		end if;
		v := char2dec(l(cn));
		-- first non-space character is not decimal
		if v = -1 then
			return;
		end if;
		loop
			resl := res * 10 + v;
			res := resl(34 downto 0);
			if res(34 downto 31) /= x"0" then -- overflow
			  assert false
			    report "*** nread:" & integer'image(ln) & ":" & integer'image(cn) & ": overflow" & CR & "*** """ & l.all & """" & CR &
								 "*** could not convert decimal value on 31 bits"
			    severity failure;
			  return;
			end if;
			cn := cn + 1;
			if cn > l'length then -- end of line
		    good := true;
				val := to_integer(res(30 downto 0));
				return;
			end if;
		  v := char2dec(l(cn));
			if v = -1 then -- non-hexadecimal character
		    good := true;
				val := to_integer(res(30 downto 0));
				return;
			end if;
		end loop;
  end procedure nread;

  procedure read(file f: text; ln: inout natural; cn: inout natural; l: inout line; cmd: out cmd_type; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
    subtype cmd_string is string(1 to 2);
    variable s: cmd_string;
  begin
    good := false;
    eof := true;
		skip(f, ln, cn, l, tmp); -- skip spaces, tmp == end of file
		-- if end of file
		if tmp then
			return;
		end if;
		eof := false;
		-- if last character of line (commands are 2 characters strings)
		if cn > l'length - 1 then
			return;
		end if;
    good := true;
		s := l(cn to cn + 1);
    case s is
      when "RS" => cmd := RS; cn := cn + 2;
      when "WR" => cmd := WR; cn := cn + 2;
      when "DW" => cmd := WR; cn := cn + 2;
      when "RD" => cmd := RD; cn := cn + 2;
      when "DR" => cmd := RD; cn := cn + 2;
      when "VR" => cmd := VR; cn := cn + 2;
      when "VW" => cmd := VW; cn := cn + 2;
      when "UR" => cmd := UR; cn := cn + 2;
      when "UW" => cmd := UW; cn := cn + 2;
      when "WD" => cmd := WD; cn := cn + 2;
      when "WV" => cmd := WD; cn := cn + 2;
      when "WU" => cmd := WD; cn := cn + 2;
      when "EX" => cmd := EX; cn := cn + 2;
      when "WE" => cmd := WE; cn := cn + 2;
      when "WC" => cmd := WC; cn := cn + 2;
      when "QT" => cmd := QT; cn := cn + 2;
      when others => good := false;
    end case;
  end procedure read;

  procedure reset(signal clk: in std_ulogic; signal css2pss: out css2pss_type; n: in natural) is
  begin
    css2pss.srstn <= '0';
    for i in 1 to n loop
      wait until rising_edge(clk);
    end loop;
    css2pss.srstn <= '1';
  end procedure reset;

  procedure write(add: in word64_address_type; data: in word64; be: in word64_be_type; signal dma2mss: out dma2mss_type) is
  begin
    dma2mss <= (en => '1', rnw => '0', be => be, add => add, wdata => data);
  end procedure write;

  procedure write(add: in word64_address_type; data: in word64; be: in word64_be_type; signal clk: in std_ulogic; signal mss2dma: mss2dma_type;
                  signal dma2mss: out dma2mss_type) is
  begin
    write(add, data, be, dma2mss);
    wait until rising_edge(clk) and mss2dma.gnt(0) = '1';
    dma2mss <= dma2mss_none;
  end procedure write;

  procedure write(add: in uc_address_type; data: in uc_data_type; signal uc2mss: out uc2mss_type) is
  begin
    uc2mss <= (en => '1', rnw => '0', be => (others => '1'), add => add, wdata => data);
  end procedure write;

  procedure write(add: in uc_address_type; data: in uc_data_type; signal clk: in std_ulogic; signal mss2uc: mss2uc_type; signal uc2mss: out uc2mss_type) is
  begin
    write(add, data, uc2mss);
    wait until rising_edge(clk) and mss2uc.gnt(0) = '1';
    uc2mss <= uc2mss_none;
  end procedure write;

  procedure read(add: in word64_address_type; signal dma2mss: out dma2mss_type) is
  begin
    dma2mss <= (en => '1', rnw => '1', be => (others => '1'), add => add, wdata => (others => '0'));
  end procedure read;

  procedure read(add: in word64_address_type; signal clk: in std_ulogic; signal mss2dma: mss2dma_type; signal dma2mss: out dma2mss_type) is
  begin
    read(add, dma2mss);
    wait until rising_edge(clk) and mss2dma.gnt(0) = '1';
    dma2mss <= dma2mss_none;
  end procedure read;

  procedure read(add: in uc_address_type; signal uc2mss: out uc2mss_type) is
  begin
    uc2mss <= (en => '1', rnw => '1', be => (others => '1'), add => add, wdata => (others => '0'));
  end procedure read;

  procedure read(add: in uc_address_type; signal clk: in std_ulogic; signal mss2uc: mss2uc_type; signal uc2mss: out uc2mss_type) is
  begin
    read(add, uc2mss);
    wait until rising_edge(clk) and mss2uc.gnt(0) = '1';
    uc2mss <= uc2mss_none;
  end procedure read;

  procedure exec(p: in std_ulogic_vector; signal clk: in std_ulogic; signal css2pss: out css2pss_type; signal param: out std_ulogic_vector) is
  begin
    param <= p;
    css2pss.exec <= '1';
    wait until rising_edge(clk);
    css2pss.exec <= '0';
    param <= (param'range => '0');
  end procedure exec;

  procedure read(file f: text; ln: inout natural; cn: inout natural; l: inout line; r: inout dmavci_request_type; good: out boolean) is
    variable tmp: boolean;
    variable n: natural;
    variable add: vci_address_type;
    variable msk: vci_be_type;
    variable data: vci_data_type;
  begin
    good := false;
    assert r.n = 0
      report "Already allocated DMA or VCI request"
      severity failure;
    hread(f, ln, cn, l, add, tmp);
    if not tmp then
			return;
		end if;
    r.add := add(28 downto 0);
    nread(f, ln, cn, l, n, tmp);
    if not tmp or n = 0 then
			return;
		end if;
    hread(f, ln, cn, l, msk, tmp);
    if not tmp then
			return;
		end if;
    r.msk := msk;
    r.pld := new word64_vector(0 to n - 1);
    r.n := 0;
    for i in 0 to n - 1 loop
      hread(f, ln, cn, l, data, tmp);
      if not tmp then
				return;
			end if;
      r.pld(i) := data;
    end loop;
    good := true;
  end procedure read;

  procedure read(file f: text; ln: inout natural; cn: inout natural; l: inout line; u: inout uc_request_type; good: out boolean) is
    variable tmp: boolean;
    variable n: natural;
    variable add: uc_address_type;
    variable data: uc_data_type;
  begin
    good := false;
    assert u.n = 0
      report "Already allocated UC request"
      severity failure;
    hread(f, ln, cn, l, add, tmp);
    if not tmp then
			return;
		end if;
    u.add := add;
    nread(f, ln, cn, l, n, tmp);
    if not tmp or n = 0 then
			return;
		end if;
    u.pld := new word64_vector(0 to n - 1);
    u.n := 0;
    for i in 0 to n - 1 loop
      hread(f, ln, cn, l, data, tmp);
      if not tmp then
				return;
			end if;
      u.pld(i) := data;
    end loop;
    good := true;
  end procedure read;

end package body css_emulator_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
