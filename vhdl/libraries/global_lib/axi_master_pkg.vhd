--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

use work.sim_utils.all;
use work.utils.all;
use work.axi_pkg_m8.all;

package axi_master_pkg is

  --------------------------
  -- Command file parsing --
  --------------------------

  -- Type defining the commands in a command file
  type cmd_type is (RS, WR, RD, EW, ER, QT);
  constant cmd_num: natural := cmd_type'pos(cmd_type'right) + 1; -- Number of different commands
  subtype cmd_name_type is string(1 to 2); -- Type of command names
  type cmd_names_type is array(0 to cmd_num - 1) of cmd_name_type; -- Type of array of all command names
  -- All command names
  constant cmd_names: cmd_names_type := ("RS", "WR", "RD", "EW", "ER", "QT");

  -- Return true if C is a blank character (space, tab or carriage return).
  function is_blank(c: character) return boolean;

  -- Find the next token in line L, starting at character CN (1 being the first character), skipping blanks and comments. L is unmodified. If returned EOL (end
  -- of line) is false, CN points to the first character of next token, else, end of line was encountered first and CN is unmodified.
  procedure next_token(variable l: in line; cn: inout natural; eol: out boolean);

  -- Same as the previous one but with two more parameters: F, a text file and LN a line counter. When end of line is encountered, increments LN, sets CN to 1,
  -- reads a new line in file F and continues until first token or end of file. If returned EOF (end of file) is false, L is the line containing the token and
  -- CN points to the first character of token in L, else, end of file was encountered first.
  procedure next_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; eof: out boolean);

  -- Convert a command to its string name
  function cmd2cmd_name(c: cmd_type) return cmd_name_type;

  -- Convert a command name to the corresponding command
  procedure cmd_name2cmd(n: in cmd_name_type; c: out cmd_type; good: out boolean);

  -- Reads a two characters command in the line L at character CN. On return L is unmodified. If GOOD is true, CN points to the first character after the
  -- command and CMD is the read command, else no command was found.
  procedure cmd_read(variable l: in line; cn: inout natural; cmd: out cmd_type; good: out boolean);

  -- Read an u_unsigned  hexadecimal value from line L. Stop at end of line or first non-hex character. The L parameter is unmodified. GOOD is an exit status set
  -- to false when the first character is non-hex, the line is empty or on overflow of the VAL output parameter. If returned GOOD is true CN points to the first
  -- non-hex character or after the end of line.
  procedure hex_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean);

  -- Read an u_unsigned decimal value from line L. Stop at end of line or first non-dec character. The L parameter is unmodified. GOOD is an exit status set to
  -- false when the first character is non-dec, the line is empty or on overflow of the VAL output parameter. If returned GOOD is true CN points to the first
  -- non-dec character or after the end of line.
  procedure dec_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean);

  -- Read an u_unsigned binary value from line L. Stop at end of line or first non-bit character. The L parameter is unmodified. GOOD is an exit status set to
  -- false when the first character is non-bit, the line is empty or on overflow of the VAL output parameter. If returned GOOD is true CN points to the first
  -- non-bit character or after the end of line.
  procedure bin_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean);

  -- Print a syntax error message
  procedure syntax_error(variable l: in line; ln: in natural; cn: in natural; str: in string);

  -- Advances to next token and reads it as a command
  procedure get_cmd_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out cmd_type; good: out boolean; eof: out boolean);
  procedure get_cmd_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out cmd_type; eof: out boolean);

  -- Advances to next token and reads it as an hex value
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector);
  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural);

  -- Advances to next token and reads it as a decimal value
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector);
  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural);

  -- Advances to next token and reads it as a binary value
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector);
  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural);


  -- Write address channel fifo

  type wac_fifo_entry;
  type wac_fifo_entry_ptr is access wac_fifo_entry;
  type wac_fifo_entry is record
    cur: axi_m2s_a;
    nxt: wac_fifo_entry_ptr;
    prv: wac_fifo_entry_ptr;
  end record wac_fifo_entry;

  -- Read address channel fifo

  type rac_fifo_entry;
  type rac_fifo_entry_ptr is access rac_fifo_entry;
  type rac_fifo_entry is record
    cur: axi_m2s_a;
    nxt: rac_fifo_entry_ptr;
    prv: rac_fifo_entry_ptr;
  end record rac_fifo_entry;

  -- Write data channel fifo

  type wdc_fifo_entry;
  type wdc_fifo_entry_ptr is access wdc_fifo_entry;
  type wdc_fifo_entry is record
    cur: axi_m2s_w;
    nxt: wdc_fifo_entry_ptr;
    prv: wdc_fifo_entry_ptr;
  end record wdc_fifo_entry;

  -- Read data channel fifo

  type rdc_fifo_entry;
  type rdc_fifo_entry_ptr is access rdc_fifo_entry;
  type rdc_fifo_entry is record
    cur: axi_s2m_r;
    nxt: rdc_fifo_entry_ptr;
    prv: rdc_fifo_entry_ptr; 
    msk: std_ulogic_vector(63 downto 0);
  end record rdc_fifo_entry;

  -- Write response channel fifo

  type wrc_fifo_entry;
  type wrc_fifo_entry_ptr is access wrc_fifo_entry;
  type wrc_fifo_entry is record
    cur: axi_s2m_b;
    nxt: wrc_fifo_entry_ptr;
    prv: wrc_fifo_entry_ptr;
  end record wrc_fifo_entry;

  type axi_write_master_emulator is record
    wacbot: wac_fifo_entry_ptr;
    wactop: wac_fifo_entry_ptr;
    waccnt: natural;
    wdcbot: wdc_fifo_entry_ptr;
    wdctop: wdc_fifo_entry_ptr;
    wdccnt: natural;
    wrcbot: wrc_fifo_entry_ptr;
    wrctop: wrc_fifo_entry_ptr;
    wrccnt: natural;
  end record axi_write_master_emulator;

  type axi_read_master_emulator is record
    racbot: rac_fifo_entry_ptr;
    ractop: rac_fifo_entry_ptr;
    raccnt: natural;
    rdcbot: rdc_fifo_entry_ptr;
    rdctop: rdc_fifo_entry_ptr;
    rdccnt: natural;
  end record axi_read_master_emulator;

  procedure push(variable awe: inout axi_write_master_emulator; wac: in axi_m2s_a);
  procedure push(variable awe: inout axi_write_master_emulator; wdc: in axi_m2s_w);
  procedure push(variable awe: inout axi_write_master_emulator; wrc: in axi_s2m_b);
  procedure push(variable are: inout axi_read_master_emulator; rac: in axi_m2s_a);
  procedure push(variable are: inout axi_read_master_emulator; rdc: in axi_s2m_r; msk : std_ulogic_vector);
  
  procedure pop(variable awe: inout axi_write_master_emulator; variable wa: in wac_fifo_entry_ptr);
  procedure pop(variable awe: inout axi_write_master_emulator; variable wd: in wdc_fifo_entry_ptr);
  procedure pop(variable awe: inout axi_write_master_emulator; variable wr: in wrc_fifo_entry_ptr);
  procedure pop(variable are: inout axi_read_master_emulator; variable ra: in rac_fifo_entry_ptr);
  procedure pop(variable are: inout axi_read_master_emulator; variable rd: in rdc_fifo_entry_ptr);

  procedure free(variable awe: inout axi_write_master_emulator);
  procedure free(variable are: inout axi_read_master_emulator);

  procedure is_empty(variable are: in axi_read_master_emulator; good: out boolean) ;
  procedure is_empty(variable awe: in axi_write_master_emulator; good: out boolean);
  
  procedure play(variable are: inout axi_read_master_emulator; signal ra: out axi_m2s_a) ;
  procedure play(variable are: inout axi_read_master_emulator; signal ra: out axi_m2s_ar) ;
  procedure play(variable awe: inout axi_write_master_emulator; signal wa: out axi_m2s_a) ;
  procedure play(variable awe: inout axi_write_master_emulator; signal wa: out axi_m2s_aw) ;
  procedure play(variable awe: inout axi_write_master_emulator; signal wd: out axi_m2s_w) ;
  
  procedure check(mes: in string; got: in std_ulogic; exp: in std_ulogic) ;
  procedure check(mes: in string; got: in std_ulogic; exp: in std_ulogic; msk: in std_ulogic) ;
  procedure check(mes: in string; got: in std_ulogic_vector; exp: in std_ulogic_vector) ;
  procedure check(mes: in string; got: in std_ulogic_vector; exp: in std_ulogic_vector; msk: in std_ulogic_vector) ;
  procedure check(variable are: inout axi_read_master_emulator; rd: in axi_s2m_r) ;
  procedure check(variable awe: inout axi_write_master_emulator; wr: in axi_s2m_b) ;
  

  procedure read_vw(file f: text; l: inout line; ln: inout natural; cn: inout natural; awe: inout axi_write_master_emulator);
  procedure read_vr(file f: text; l: inout line; ln: inout natural; cn: inout natural; are: inout axi_read_master_emulator);

end package axi_master_pkg;

package body axi_master_pkg is

  function is_blank(c: character) return boolean is
  begin
    return c = ' ' or c = HT or c = CR;
  end function is_blank;

  procedure next_token(variable l: in line; cn: inout natural; eol: out boolean) is
  begin
    loop
      if l = null or l'length < cn or l(cn) = '#' then
        eol := true;
        return;
      elsif is_blank(l(cn)) then
        cn := cn + 1;
      else
        eol := false;
        return;
      end if;
    end loop;
  end procedure next_token;

  procedure next_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; eof: out boolean) is
    variable eol: boolean;
  begin
    loop
      next_token(l, cn, eol);
      if eol and endfile(f) then
        eof := true;
        return;
      elsif eol then
        readline(f, l);
        ln := ln + 1;
        cn := 1;
      else
        eof := false;
        return;
      end if;
    end loop;
  end procedure next_token;

  function cmd2cmd_name(c: cmd_type) return cmd_name_type is
  begin
    return cmd_names(cmd_type'pos(c));
  end function cmd2cmd_name;

  procedure cmd_name2cmd(n: in cmd_name_type; c: out cmd_type; good: out boolean) is
  begin
    for i in 0 to cmd_num - 1 loop
      if n = cmd_names(i) then
        good := true;
        c := cmd_type'val(i);
        return;
      end if;
    end loop;
    good := false;
  end procedure cmd_name2cmd;

  procedure cmd_read(variable l: in line; cn: inout natural; cmd: out cmd_type; good: out boolean) is
    variable s: string(1 to 2);
    variable tmp: boolean;
  begin
    good := false;
    if l'length < cn + 1 then -- Less than 2 characters left in line L
      return;
    else
      s(1 to 2) := l(cn to cn + 1);
      cmd_name2cmd(s, cmd, tmp);
      good := tmp;
      if tmp then
        cn := cn + 2;
      end if;
    end if;
  end procedure cmd_read;

  procedure hex_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean) is
    constant n: natural := val'length;
    variable res: u_unsigned(n + 3 downto 0) := (others => '0');
    variable v: integer range -1 to 15;
  begin
    good := false;
    if l = null or l'length < cn then -- Empty line or end of line
      return;
    end if;
    v := char2hex(l(cn));
    if v = -1 then -- First character is not hexadecimal
      return;
    end if;
    loop
      res := res(n - 1 downto 0) & to_unsigned(v, 4);
      if res(n + 3 downto n) /= x"0" then -- Overflow
        return;
      end if;
      cn := cn + 1;
      exit when l'length < cn; -- End of line
      v := char2hex(l(cn));
      exit when v = -1; -- Encountered first non hexadecimal character
    end loop;
    good := true;
    val := res(n - 1 downto 0);
  end procedure hex_read;

  procedure dec_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean) is
    constant n: natural := val'length;
    variable res: u_unsigned(n + 3 downto 0) := (others => '0');
    variable tmp: u_unsigned(2 * n + 7 downto 0) := (others => '0');
    variable v: integer range -1 to 9;
  begin
    good := false;
    if l = null or l'length < cn then -- Empty line or end of line
      return;
    end if;
    v := char2dec(l(cn));
    if v = -1 then -- First character is not decimal
      return;
    end if;
    loop
      tmp := res * 10 + v;
      res := tmp(n + 3 downto 0);
      if res(n + 3 downto n) /= x"0" then -- Overflow
        return;
      end if;
      cn := cn + 1;
      exit when l'length < cn; -- End of line
      v := char2dec(l(cn));
      exit when v = -1; -- Encountered first non decimal character
    end loop;
    good := true;
    val := res(n - 1 downto 0);
  end procedure dec_read;

  procedure bin_read(variable l: in line; cn: inout natural; val: out u_unsigned; good: out boolean) is
    constant n: natural := val'length;
    variable res: u_unsigned(n downto 0) := (others => '0');
    variable v: integer range -1 to 1;
  begin
    good := false;
    if l = null or l'length < cn then -- Empty line or end of line
      return;
    end if;
    v := char2bin(l(cn));
    if v = -1 then -- First character is not binary
      return;
    end if;
    loop
      res := res(n - 1 downto 0) & to_unsigned(v, 1);
      if res(n) /= '0' then -- Overflow
        return;
      end if;
      cn := cn + 1;
      exit when l'length < cn; -- End of line
      v := char2bin(l(cn));
      exit when v = -1; -- Encountered first non binary character
    end loop;
    good := true;
    val := res(n - 1 downto 0);
  end procedure bin_read;

  procedure syntax_error(variable l: in line; ln: in natural; cn: in natural; str: in string) is
    variable dbg: line;
  begin
    write(dbg, ln);
    print(dbg, ":");
    write(dbg, cn);
    print(dbg, ": syntax error : ");
    write(dbg, str);
    writeline(output, dbg);
    write(dbg, l.all);
    writeline(output, dbg);
  end procedure syntax_error;

  procedure get_cmd_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out cmd_type; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
  begin
    next_token(f, l, ln, cn, tmp);
    if tmp then -- End of file
      good := true;
      eof  := true;
      return;
    end if;
    eof := false;
    cmd_read(l, cn, val, tmp);
    if not tmp then -- Next token is not a command 
      good := false;
      syntax_error(l, ln, cn, "not a command");
      return;
    end if;
    good := true;
  end procedure get_cmd_token;

  procedure get_cmd_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out cmd_type; eof: out boolean) is
    variable good: boolean;
  begin
    get_cmd_token(f, l, ln, cn, val, good, eof);
    assert good severity failure;
  end procedure get_cmd_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
  begin
    next_token(f, l, ln, cn, tmp);
    if tmp then -- End of file
      good := true;
      eof  := true;
      return;
    end if;
    eof := false;
    hex_read(l, cn, val, tmp);
    if not tmp then -- Next token is not hexadecimal or overflow
      good := false;
      syntax_error(l, ln, cn, "not an hexadecimal value or overflow");
      return;
    end if;
    good := true;
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_hex_token(f, l, ln, cn, tmp, good, eof);
    val := std_ulogic_vector(tmp);
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_hex_token(f, l, ln, cn, tmp, good, eof);
    val := to_integer(tmp);
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned) is
    variable good, eof: boolean;
  begin
    get_hex_token(f, l, ln, cn, val, good, eof);
    if eof then
      syntax_error(l, ln, cn, "unexpected end of file");
    end if;
    assert good and not eof severity failure;
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_hex_token(f, l, ln, cn, tmp);
    val := std_ulogic_vector(tmp);
  end procedure get_hex_token;

  procedure get_hex_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_hex_token(f, l, ln, cn, tmp);
    val := to_integer(tmp);
  end procedure get_hex_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
  begin
    next_token(f, l, ln, cn, tmp);
    if tmp then -- End of file
      good := true;
      eof  := true;
      return;
    end if;
    eof := false;
    dec_read(l, cn, val, tmp);
    if not tmp then -- Next token is not decimal or overflow
      good := false;
      syntax_error(l, ln, cn, "not a decimal value or overflow");
      return;
    end if;
    good := true;
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_dec_token(f, l, ln, cn, tmp, good, eof);
    val := std_ulogic_vector(tmp);
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_dec_token(f, l, ln, cn, tmp, good, eof);
    val := to_integer(tmp);
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned) is
    variable good, eof: boolean;
  begin
    get_dec_token(f, l, ln, cn, val, good, eof);
    if eof then
      syntax_error(l, ln, cn, "unexpected end of file");
    end if;
    assert good and not eof severity failure;
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_dec_token(f, l, ln, cn, tmp);
    val := std_ulogic_vector(tmp);
  end procedure get_dec_token;

  procedure get_dec_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_dec_token(f, l, ln, cn, tmp);
    val := to_integer(tmp);
  end procedure get_dec_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned; good: out boolean; eof: out boolean) is
    variable tmp: boolean;
  begin
    next_token(f, l, ln, cn, tmp);
    if tmp then -- End of file
      good := true;
      eof  := true;
      return;
    end if;
    eof := false;
    bin_read(l, cn, val, tmp);
    if not tmp then -- Next token is not binary or overflow
      good := false;
      syntax_error(l, ln, cn, "not a binary value or overflow");
      return;
    end if;
    good := true;
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_bin_token(f, l, ln, cn, tmp, good, eof);
    val := std_ulogic_vector(tmp);
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural; good: out boolean; eof: out boolean) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_bin_token(f, l, ln, cn, tmp, good, eof);
    val := to_integer(tmp);
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out u_unsigned) is
    variable good, eof: boolean;
  begin
    get_bin_token(f, l, ln, cn, val, good, eof);
    if eof then
      syntax_error(l, ln, cn, "unexpected end of file");
    end if;
    assert good and not eof severity failure;
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out std_ulogic_vector) is
    variable tmp: u_unsigned(val'length - 1 downto 0);
  begin
    get_bin_token(f, l, ln, cn, tmp);
    val := std_ulogic_vector(tmp);
  end procedure get_bin_token;

  procedure get_bin_token(file f: text; l: inout line; ln: inout natural; cn: inout natural; val: out natural) is
    variable tmp: u_unsigned(30 downto 0);
  begin
    get_bin_token(f, l, ln, cn, tmp);
    val := to_integer(tmp);
  end procedure get_bin_token;

  procedure push(variable are: inout axi_read_master_emulator; rac: in axi_m2s_a) is
    variable tmp: rac_fifo_entry_ptr;
  begin
    tmp     := new rac_fifo_entry;
    tmp.cur := rac;
    tmp.nxt := null;
    if are.raccnt = 0 then -- FIFO empty
      tmp.prv     := null;
      are.racbot  := tmp;
      are.ractop  := tmp;
    else
      tmp.prv        := are.racbot;
      are.racbot.nxt := tmp;
      are.racbot     := tmp;
    end if;
    are.raccnt := are.raccnt + 1;
  end procedure push;

  procedure push(variable awe: inout axi_write_master_emulator; wac: in axi_m2s_a) is
    variable tmp: wac_fifo_entry_ptr;
  begin
    tmp     := new wac_fifo_entry;
    tmp.cur := wac;
    tmp.nxt := null;
    if awe.waccnt = 0 then -- FIFO empty
      tmp.prv     := null;
      awe.wacbot  := tmp;
      awe.wactop  := tmp;
    else
      tmp.prv        := awe.wacbot;
      awe.wacbot.nxt := tmp;
      awe.wacbot     := tmp;
    end if;
    awe.waccnt := awe.waccnt + 1;
  end procedure push;

  procedure push(variable are: inout axi_read_master_emulator; rdc: in axi_s2m_r; msk : std_ulogic_vector) is
    variable tmp: rdc_fifo_entry_ptr;
  begin
    tmp     := new rdc_fifo_entry;
    tmp.cur := rdc;
    tmp.nxt := null;
    tmp.msk := msk;
    if are.rdccnt = 0 then -- FIFO empty
      tmp.prv := null;
      are.rdcbot  := tmp;
      are.rdctop  := tmp;
    else
      tmp.prv        := are.rdcbot;
      are.rdcbot.nxt := tmp;
      are.rdcbot     := tmp;
    end if;
    are.rdccnt := are.rdccnt + 1;
  end procedure push;

  procedure push(variable awe: inout axi_write_master_emulator; wdc: in axi_m2s_w) is
    variable tmp: wdc_fifo_entry_ptr;
  begin
    tmp     := new wdc_fifo_entry;
    tmp.cur := wdc;
    tmp.nxt := null;
    if awe.wdccnt = 0 then -- FIFO empty
      tmp.prv := null;
      awe.wdcbot  := tmp;
      awe.wdctop  := tmp;
    else
      tmp.prv        := awe.wdcbot;
      awe.wdcbot.nxt := tmp;
      awe.wdcbot     := tmp;
    end if;
    awe.wdccnt := awe.wdccnt + 1;
  end procedure push;

  procedure push(variable awe: inout axi_write_master_emulator; wrc: in axi_s2m_b) is
    variable tmp: wrc_fifo_entry_ptr;
  begin
    tmp     := new wrc_fifo_entry;
    tmp.cur := wrc;
    tmp.nxt := null;
    if awe.wrccnt = 0 then -- FIFO empty
      tmp.prv := null;
      awe.wrcbot  := tmp;
      awe.wrctop  := tmp;
    else
      tmp.prv        := awe.wrcbot;
      awe.wrcbot.nxt := tmp;
      awe.wrcbot     := tmp;
    end if;
    awe.wrccnt := awe.wrccnt + 1;
  end procedure push;

  procedure pop(variable awe: inout axi_write_master_emulator; variable wa: in wac_fifo_entry_ptr) is
    variable tmp: wac_fifo_entry_ptr := wa;
  begin
    if tmp.prv /= null and tmp.nxt /= null then   -- Middle of list
      tmp.nxt.prv    := tmp.prv;
      tmp.prv.nxt    := tmp.nxt;
    elsif tmp.prv /= null and tmp.nxt = null then -- Last of list
      awe.wacbot     := tmp.prv;
      awe.wacbot.nxt := null;
    elsif tmp.prv = null and tmp.nxt /= null then -- First of list
      awe.wactop     := tmp.nxt;
      awe.wactop.prv := null;
    else -- Single in list
      awe.wactop     := null;
      awe.wacbot     := null;
    end if;
    awe.waccnt := awe.waccnt - 1;
    deallocate(tmp);
  end procedure pop;

  procedure pop(variable awe: inout axi_write_master_emulator; variable wd: in wdc_fifo_entry_ptr) is
    variable tmp: wdc_fifo_entry_ptr := wd;
  begin
    if tmp.prv /= null and tmp.nxt /= null then   -- Middle of list
      tmp.nxt.prv    := tmp.prv;
      tmp.prv.nxt    := tmp.nxt;
    elsif tmp.prv /= null and tmp.nxt = null then -- Last of list
      awe.wdcbot     := tmp.prv;
      awe.wdcbot.nxt := null;
    elsif tmp.prv = null and tmp.nxt /= null then -- First of list
      awe.wdctop     := tmp.nxt;
      awe.wdctop.prv := null;
    else -- Single in list
      awe.wdctop     := null;
      awe.wdcbot     := null;
    end if;
    awe.wdccnt := awe.wdccnt - 1;
    deallocate(tmp);
  end procedure pop;

  procedure pop(variable awe: inout axi_write_master_emulator; variable wr: in wrc_fifo_entry_ptr) is
    variable tmp: wrc_fifo_entry_ptr := wr;
  begin
    if tmp.prv /= null and tmp.nxt /= null then   -- Middle of list
      tmp.nxt.prv    := tmp.prv;
      tmp.prv.nxt    := tmp.nxt;
    elsif tmp.prv /= null and tmp.nxt = null then -- Last of list
      awe.wrcbot     := tmp.prv;
      awe.wrcbot.nxt := null;
    elsif tmp.prv = null and tmp.nxt /= null then -- First of list
      awe.wrctop     := tmp.nxt;
      awe.wrctop.prv := null;
    else -- Single in list
      awe.wrctop     := null;
      awe.wrcbot     := null;
    end if;
    awe.wrccnt := awe.wrccnt - 1;
    deallocate(tmp);
  end procedure pop;

  procedure pop(variable are: inout axi_read_master_emulator; variable ra: in rac_fifo_entry_ptr) is
    variable tmp: rac_fifo_entry_ptr := ra;
  begin
    if tmp.prv /= null and tmp.nxt /= null then   -- Middle of list
      tmp.nxt.prv    := tmp.prv;
      tmp.prv.nxt    := tmp.nxt;
    elsif tmp.prv /= null and tmp.nxt = null then -- Last of list
      are.racbot     := tmp.prv;
      are.racbot.nxt := null;
    elsif tmp.prv = null and tmp.nxt /= null then -- First of list
      are.ractop     := tmp.nxt;
      are.ractop.prv := null;
    else -- Single in list
      are.ractop     := null;
      are.racbot     := null;
    end if;
    are.raccnt := are.raccnt - 1;
    deallocate(tmp);
  end procedure pop;

  procedure pop(variable are: inout axi_read_master_emulator; variable rd: in rdc_fifo_entry_ptr) is
    variable tmp: rdc_fifo_entry_ptr := rd;
  begin
    if tmp.prv /= null and tmp.nxt /= null then   -- Middle of list
      tmp.nxt.prv    := tmp.prv;
      tmp.prv.nxt    := tmp.nxt;
    elsif tmp.prv /= null and tmp.nxt = null then -- Last of list
      are.rdcbot     := tmp.prv;
      are.rdcbot.nxt := null;
    elsif tmp.prv = null and tmp.nxt /= null then -- First of list
      are.rdctop     := tmp.nxt;
      are.rdctop.prv := null;
    else -- Single in list
      are.rdctop     := null;
      are.rdcbot     := null;
    end if;
    are.rdccnt := are.rdccnt - 1;
    deallocate(tmp);
  end procedure pop;

  procedure free(variable awe: inout axi_write_master_emulator) is
    variable wac: wac_fifo_entry_ptr;
    variable wdc: wdc_fifo_entry_ptr;
    variable wrc: wrc_fifo_entry_ptr;
  begin
    while awe.waccnt /= 0 loop
      wac := awe.wactop.nxt;
      deallocate(awe.wactop);
      awe.waccnt := awe.waccnt - 1;
      awe.wactop := wac;
    end loop;
    while awe.wdccnt /= 0 loop
      wdc := awe.wdctop.nxt;
      deallocate(awe.wdctop);
      awe.wdccnt := awe.wdccnt - 1;
      awe.wdctop := wdc;
    end loop;
    while awe.wrccnt /= 0 loop
      wrc := awe.wrctop.nxt;
      deallocate(awe.wrctop);
      awe.wrccnt := awe.wrccnt - 1;
      awe.wrctop := wrc;
    end loop;

    awe.wacbot := null;
    awe.wactop := null;
    awe.wdcbot := null;
    awe.wdctop := null;
    awe.wrcbot := null;
    awe.wrctop := null;

  end procedure free;

  procedure free(variable are: inout axi_read_master_emulator) is
    variable rac: rac_fifo_entry_ptr;
    variable rdc: rdc_fifo_entry_ptr;
    variable wrc: wrc_fifo_entry_ptr;
  begin
    while are.raccnt /= 0 loop
      rac := are.ractop.nxt;
      deallocate(are.ractop);
      are.raccnt := are.raccnt - 1;
      are.ractop := rac;
    end loop;
    while are.rdccnt /= 0 loop
      rdc := are.rdctop.nxt;
      deallocate(are.rdctop);
      are.rdccnt := are.rdccnt - 1;
      are.rdctop := rdc;
    end loop;

    are.racbot := null;
    are.ractop := null;
    are.rdcbot := null;
    are.rdctop := null;

  end procedure free;

  procedure is_empty(variable are: in axi_read_master_emulator; good: out boolean) is
  begin
    good := are.raccnt = 0 and are.rdccnt = 0;
  end procedure is_empty;

  procedure play(variable are: inout axi_read_master_emulator; signal ra: out axi_m2s_a) is
  begin
    assert are.raccnt /= 0 report "FIFO empty" severity failure;
    ra <= are.ractop.cur;
    pop(are, are.ractop);
  end procedure play;

  procedure play(variable are: inout axi_read_master_emulator; signal ra: out axi_m2s_ar) is
  begin
    assert are.raccnt /= 0 report "FIFO empty" severity failure;
    ra <= to_axi_m2s_ar(are.ractop.cur);
    pop(are, are.ractop);
  end procedure play;

  procedure play(variable awe: inout axi_write_master_emulator; signal wa: out axi_m2s_a) is
  begin
    assert awe.waccnt /= 0 report "FIFO empty" severity failure;
    wa <= awe.wactop.cur;
    pop(awe, awe.wactop);
  end procedure play;

  procedure play(variable awe: inout axi_write_master_emulator; signal wa: out axi_m2s_aw) is
  begin
    assert awe.waccnt /= 0 report "FIFO empty" severity failure;
    wa <= to_axi_m2s_aw(awe.wactop.cur);
    pop(awe, awe.wactop);
  end procedure play;

  procedure play(variable awe: inout axi_write_master_emulator; signal wd: out axi_m2s_w) is
  begin
    assert awe.wdccnt /= 0 report "FIFO empty" severity failure;
    wd <= awe.wdctop.cur;
    pop(awe, awe.wdctop);
  end procedure play;

  procedure check(mes: in string; got: in std_ulogic; exp: in std_ulogic) is
  begin
    assert got = exp report mes & ": got " & std2char(got) & ", expected " & std2char(exp) severity failure;
  end procedure check;

  procedure check(mes: in string; got: in std_ulogic; exp: in std_ulogic; msk: in std_ulogic) is
  begin
    assert (got and msk) = (exp and msk) report mes & ": got " & std2char(got) & ", expected " & std2char(exp) & " (mask: " & std2char(msk) & ")" severity failure;
  end procedure check;

  procedure check(mes: in string; got: in std_ulogic_vector; exp: in std_ulogic_vector) is
  begin
    assert got = exp report mes & ": got " & vec2hexstr(got) & ", expected " & vec2hexstr(exp) severity failure;
  end procedure check;

  procedure check(mes: in string; got: in std_ulogic_vector; exp: in std_ulogic_vector; msk: in std_ulogic_vector) is
  begin
    assert (got and msk) = (exp and msk) report mes & ": got " & vec2hexstr(got) & ", expected " & vec2hexstr(exp) & " (mask: " & vec2hexstr(msk) & ")" severity failure;
  end procedure check;

  procedure check(variable are: inout axi_read_master_emulator; rd: in axi_s2m_r) is
    variable tmp: rdc_fifo_entry_ptr;
  begin
    assert are.rdccnt /= 0 report "Read data channel FIFO empty" severity failure;
    tmp := are.rdctop;
    while rd.rid /= tmp.cur.rid loop
      assert tmp.nxt /= null report "No response of this class in FIFO" severity failure;
      tmp := tmp.nxt;
    end loop;

    check("RVALID mismatch", rd.rvalid, tmp.cur.rvalid);
    check("RDATA  mismatch", rd.rdata, tmp.cur.rdata, tmp.msk);
    check("RRESP  mismatch", rd.rresp, tmp.cur.rresp);
    check("RLAST  mismatch", rd.rlast, tmp.cur.rlast);
    check("RID    mismatch", rd.rid, tmp.cur.rid);

    pop(are, tmp);

  end procedure check;

  procedure check(variable awe: inout axi_write_master_emulator; wr: in axi_s2m_b) is
    variable tmp: wrc_fifo_entry_ptr;
  begin
    assert awe.wrccnt /= 0 report "Write response channel FIFO empty" severity failure;
    tmp := awe.wrctop;
    while wr.bid /= tmp.cur.bid loop
      assert tmp.nxt /= null report "No write response of this class in FIFO" severity failure;
      tmp := tmp.nxt;
    end loop;

    check("BVALID mismatch", wr.bvalid, tmp.cur.bvalid);
    check("BRESP  mismatch", wr.bresp, tmp.cur.bresp);
    check("BID    mismatch", wr.bid, tmp.cur.bid);

    pop(awe, tmp);

  end procedure check;

  procedure is_empty(variable awe: in axi_write_master_emulator; good: out boolean) is
  begin
    good := (awe.waccnt = 0) and (awe.wdccnt = 0) and (awe.wrccnt = 0);
  end procedure is_empty;
  

  procedure read_vw(file f: text; l: inout line; ln: inout natural; cn: inout natural; awe: inout axi_write_master_emulator) is
    variable len    : natural;
    variable size   : natural;
    variable burst  : natural;
    variable addr   : std_ulogic_vector(axi_a - 1 downto 0);
    variable id     : std_ulogic_vector(axi_i - 1 downto 0);
    variable strb   : std_ulogic_vector(axi_m - 1 downto 0);
    variable w      : std_ulogic_vector(axi_d - 1 downto 0);
    variable wac    : axi_m2s_a;
    variable wdc    : axi_m2s_w;
    variable wrc    : axi_s2m_b;
  begin

    id := std_ulogic_vector_rnd(axi_i); 

    get_hex_token(f, l, ln, cn, addr);
    get_dec_token(f, l, ln, cn, len);
    if len = 0 then
      syntax_error(l, ln, cn, "Axi transfer length is null");
      assert false severity failure;
    end if;
    if len > 256 then
      syntax_error(l, ln, cn, "Axi does not support burst length greater than 256");
      assert false severity failure;
    end if;
    get_dec_token(f, l, ln, cn, size);
    if size > 3 then
      syntax_error(l, ln, cn, "AXI transfer size exceeds data bus width");
      assert false severity failure;
    end if;
    get_dec_token(f, l, ln, cn, burst);
    if burst = 2 then
      if len > 16 then
        syntax_error(l, ln, cn, "Axi does not support burst length greater than 16 in wrapping mode");
        assert false severity failure;
      end if;
    end if;
    if burst > 2 then
      syntax_error(l, ln, cn, "AXI transfer burst type not handled");
      assert false severity failure;
    end if;
      
    wac.aid    := id;
    wac.aaddr  := addr;
    wac.alen   := std_ulogic_vector(to_unsigned(len - 1, axi_l));
    wac.asize  := std_ulogic_vector(to_unsigned(size, axi_s));
    wac.aburst := std_ulogic_vector(to_unsigned(burst, axi_b));
    wac.alock  := (others => '0');
    wac.acache := (others => '0');
    wac.aprot  := (others => '0');
    wac.avalid := '1';

    push(awe, wac);

    for i in 0 to len - 1 loop

--      wdc.wid    := id;
      wdc.wvalid := '1';
      get_hex_token(f, l, ln, cn, w);
      wdc.wdata  := w;
      get_hex_token(f, l, ln, cn, strb);
      wdc.wstrb  := strb;
      wdc.wlast  := '0';
      if i = len - 1 then
        wdc.wlast := '1';
      end if;

      push(awe, wdc);

    end loop;

    wrc.bid    := id;
    wrc.bresp  := (others => '0');
    wrc.bvalid := '1';

    push(awe, wrc);

  end procedure read_vw;

  procedure read_vr(file f: text; l: inout line; ln: inout natural; cn: inout natural; are: inout axi_read_master_emulator) is
    variable len    : natural;
    variable size   : natural;
    variable burst  : natural;
    variable addr   : std_ulogic_vector(axi_a - 1 downto 0);
    variable id     : std_ulogic_vector(axi_i - 1 downto 0);
    variable strb   : std_ulogic_vector(axi_m - 1 downto 0);
    variable w      : std_ulogic_vector(axi_d - 1 downto 0);
    variable msk    : std_ulogic_vector(axi_d - 1 downto 0);
    variable rac    : axi_m2s_a;
    variable rdc    : axi_s2m_r;

  begin

    id := std_ulogic_vector_rnd(axi_i); 

    addr(2 downto 0) := "000";
    get_hex_token(f, l, ln, cn, addr);
    get_dec_token(f, l, ln, cn, len);
    if len = 0 then
      syntax_error(l, ln, cn, "Axi transfer length is null");
      assert false severity failure;
    end if;
    if len > 256 then
      syntax_error(l, ln, cn, "Axi does not support burst length greater than 256");
      assert false severity failure;
    end if;
    get_dec_token(f, l, ln, cn, size);
    if size > 3 then
      syntax_error(l, ln, cn, "AXI transfer size exceeds data bus width");
      assert false severity failure;
    end if;
    get_dec_token(f, l, ln, cn, burst);
    if burst = 2 then
      if len > 16 then
        syntax_error(l, ln, cn, "Axi does not support burst length greater than 16 in wrapping mode");
        assert false severity failure;
      end if;
    end if;
    if burst > 2 then
      syntax_error(l, ln, cn, "AXI transfer burst type not handled");
      assert false severity failure;
    end if;
      
    rac.aid    := id;
    rac.aaddr  := addr;
    rac.alen   := std_ulogic_vector(to_unsigned(len - 1, axi_l));
    rac.asize  := std_ulogic_vector(to_unsigned(size, axi_s));
    rac.aburst := std_ulogic_vector(to_unsigned(burst, axi_b));
    rac.alock  := (others => '0');
    rac.acache := (others => '0');
    rac.aprot  := (others => '0');
    rac.avalid := '1';

    push(are, rac);

    for i in 0 to len - 1 loop
      get_hex_token(f, l, ln, cn, w);
      rdc.rid    := id;
      rdc.rvalid := '1';
      rdc.rresp  := (others => '0');
      rdc.rdata := w;
      rdc.rlast  := '0';
      if i = len - 1 then
        rdc.rlast := '1';
      end if;
      get_hex_token(f, l, ln, cn, strb);
      for i in axi_m - 1 downto 0 loop
        msk := shift_left(msk, axi_m);
        if strb(i) = '1' then
          msk(axi_m - 1 downto 0) := x"ff";
        end if;
      end loop;

      push(are, rdc, msk);

    end loop;

  end procedure read_vr;

end package body axi_master_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
