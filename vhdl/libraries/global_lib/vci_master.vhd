--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Host emulator for baseband DSP unit simulation

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

use work.vci_master_pkg.all;

use work.global.all;
use work.utils.all;
use work.sim_utils.all;


entity vci_master is
  generic(cmdfile: string);          --* Name of command file
  port(clk      : in  std_ulogic;    --* Master clock
       srstn    : out std_ulogic;    --* Synchronous, active low, reset
       eos      : out boolean;       --* End of simulation
       hst2tvci : out vci_i2t_type;  --* Host initiator to target interface
       tvci2hst : in  vci_t2i_type); --* Target to host initiator interface
end entity vci_master;

architecture arc of vci_master is

  signal srstn_local:      std_ulogic;       --* Local reset
  signal hst2tvci_local:   vci_i2t_type;     --* Local host initiator to target interface

begin

  -- Locals to outputs
  srstn     <= srstn_local;
  hst2tvci  <= hst2tvci_local;

  -- Main process. Based on a VCI Initiator Emulator (VIE) and a VCI Target Emulator (VTE), two data structures defined in vci_master_pkg.vhd.
  process

    type state_type is (parsing, resetting, waiting, ending);

    variable state:        state_type;
    variable vie:          vci_initiator_emulator;
    variable cnt:          integer;
    variable good:         boolean;
    variable l:            line;
    variable ln:           natural;
    variable cn:           natural;
    variable eof:          boolean;
    variable cmd:          cmd_type;
    variable new_response: boolean;
    variable response:     vci_response_type;

    file cf:               text;

  begin

    -- Initializations
    hst2tvci_local <= vci_i2t_none;
    srstn_local    <= '0';
    eos            <= false;
    free(vie);
    init(vie);
    state    := parsing;
    cnt      := 1000;
    good     := true;
    file_open(cf, cmdfile, read_mode);
    l   := null;
    ln  := 1;
    cn  := 1;
    eof := false;
    cmd := RS;
    new_response := true;
    response     := vci_response_none;

    for i in 1 to 10 loop -- 10 clock cycles reset
      wait until rising_edge(clk);
    end loop;
    srstn_local <= '1';
    wait until rising_edge(clk);

    -- Main loop
    loop

      -- TVCI requests
      if hst2tvci_local.req.cmdval = '1' and tvci2hst.cmdack = '1' then -- If request acknowledged
        hst2tvci_local.req.cmdval <= '0'; -- Stop requesting
      end if;
      if (hst2tvci_local.req.cmdval = '0' or tvci2hst.cmdack = '1') and vie.req_fifo.cnt /= 0 and int_rnd(1, 10) /= 1 then
        play(vie, hst2tvci_local.req); --send request, pop it out request FIFO
      end if;
      -- TVCI responses
      if int_rnd(1, 10) = 1 then
        hst2tvci_local.rspack <= '0';
      else
        hst2tvci_local.rspack <= '1'; -- Randomly acknowledge responses
      end if;
      if tvci2hst.rsp.rspval = '1' and hst2tvci_local.rspack = '1' then -- If response acknowledged
        check(vie, tvci2hst.rsp); -- Check reponse, pop it out the responses FIFO
      end if;


      -- Check TVCI responses stability
      if tvci2hst.rsp.rspval = '1' then -- If response on the target VCI interface
        -- Check response stability
        if new_response then -- First time we see this response
          response     := tvci2hst.rsp;
          new_response := false;
        elsif tvci2hst.rsp /= response then -- Response changed before being acknowledged
          print("Target VCI response changed before being acknowledged:");
          print(tvci2hst.rsp);
          print("On previous clock cycle was:");
          print(response);
          assert false severity failure;
        end if;
        if hst2tvci_local.rspack = '1' then -- If acknowledged response on the target VCI interface
          new_response := true;             -- Next response will be a new one
        end if;
      end if;

      -- Reset
      if srstn_local = '0' then
        free(vie);
        new_response := true;
      end if;

      -- State machine
      case state is
        when resetting =>
          cnt := cnt - 1;
          if cnt = 0 then
            srstn_local <= '1';
            state := parsing;
          end if;
        when waiting =>
          cnt := cnt - 1;
          is_empty(vie, good);
          if good then
            state := parsing;
          elsif cnt = 0 then
            assert false report "Timeout exceeded while waiting for end of  VCI transactions" severity failure;
          end if;
        when ending =>
          exit when vie.req_fifo.cnt = 0 and vie.rsp_fifo.cnt = 0;
        when parsing => -- Parsing of command file
          while state = parsing loop
            get_cmd_token(cf, l, ln, cn, cmd, eof); -- Read next command
            if eof then -- End of command file
              state := ending;
              exit;
            end if;
            case cmd is
              when RS =>
                read_rs(cf, l, ln, cn, cnt);
                state := resetting;
                srstn_local <= '0';
                exit;
              when WR =>
                read_wr(cf, l, ln, cn, vie);
              when RD =>
                read_rd(cf, l, ln, cn, vie);
              when EW =>
              -- Wait end of read
                get_dec_token(cf, l, ln, cn, cnt);
                state := waiting;
                exit;
              when ER =>
              -- Wait end of write
                get_dec_token(cf, l, ln, cn, cnt);
                state := waiting;
                exit;
              when QT =>
                read_qt(cf, l, ln, cn);
                state := ending;
                exit;
            end case;
          end loop;
      end case;
      wait until rising_edge(clk);
    end loop;
    eos <= true;
    wait;

  end process;

end architecture arc;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
