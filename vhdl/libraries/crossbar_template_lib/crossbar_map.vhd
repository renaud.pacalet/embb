--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Crossbar mapping package
--*
--*  Defines a set of constants to configure the crossbar:
--* - The number of requesters (initiators),
--* - The number of targets,
--* - Their names and unique IDs (the two ID spaces are independent and an
--* initiator can have the same ID as a target),
--* - The base address and address mask of each target (xtag_len bits each, these
--* are used to decode the requests and route them to the right target). An
--* address a match a target with base address ba and address mask msk iff:
--*
--*   a(vci_n-1 downto --     vci_n-xtag_len) and msk = ba
--*
--* - The initiator to target connections and the fixed priorities between
--* incoming requests and responses.
--*
--* Usage
--* In order to configure a crossbar, search for "UPDATE THIS" and follow the
--* instructions:
--* - First indicate a number of initiators (NI constant) and a number of targets
--* (NT constant).
--* - Chose a short nickname for each of them.
--* - Declare a constant of type "natural range 0 to NI - 1" for each initiator
--* (name it with a short but meaningfull nickname) and assign it a unique ID. Do
--* the same for each target, with type "natural range 0 to NT - 1" (the two ID
--* spaces are independent and an initiator can have the same ID as a target).
--* - Declare and assign a constant of type TMAPV. This constant is a vector of
--* NT target maps, one per target. Each definition comprise  a target ID, a
--* target base address BA and an address mask MSK. The base address and the mask
--* are used to decode addresses, using their XTAG_LEN MSBs: the XTAG_LEN MSBs A
--* of an address match a target iff
--*
--*   A and MSK = BA and MSK
--* 
--* - Declare and assign a constant of type TDEFV. This constant is a vector of
--* NT target definitions, one per target. Each definition comprise a number N of
--* connected initiators and a list I of NI initiators IDs, in decreasing order
--* of requests priorities. The N first elements of the list are taken into
--* account for the routing. The NI-N last are ignored and their values can be
--* anything valid.
--* - Symmetrically define and assign a constant of type IDEFV. It is a vector of
--* NI initiators definitions, one per initiator. Each comprise an ID, a number N
--* of connected targets and a list T of target IDs, in decreasing order of
--* responses priorities. The N first elements of the list are taken into account
--* for the routing. The NT-N last are ignored and their values can be anything
--* valid.
--* - Finally, for debugging, adapt the TRGT_NAME and INIT_NAME function bodies
--* to your specific system.

library ieee;
use ieee.std_logic_1164.all;

library crossbar_lib;
use crossbar_lib.crossbar_pkg.all;
use crossbar_lib.plugplay_0_pkg.all;

package crossbar_map is

  -- UPDATE THIS
  constant BRIDGE_ENABLE : integer := 1;
  constant SPI_ENABLE : integer := 1;
  constant AGRF_ENABLE : integer := 0;
  constant PRE_PROCESSOR_ENABLE : integer := 0;
  constant ADAIF_ENABLE : integer := 0;
  constant CHANNEL_DECODER_ENABLE : integer := 0;
  constant CROSSBAR_ENABLE : integer := 1;
  constant GPIO_ENABLE : integer := 0;
  constant RAM_ENABLE : integer := 1;
  constant FEP_ENABLE : integer := 1;
  constant MAPPER_ENABLE : integer := 1;
  constant INTL_ENABLE : integer := 1;
  

  constant CROSSBAR_IRQ : integer := 0;
  constant INTL_PSS_IRQ : integer := 1;
  constant INTL_DMA_IRQ : integer := 2;
  constant INTL_UC_IRQ : integer := 3;
  constant FEP_PSS_IRQ : integer := 4;
  constant FEP_DMA_IRQ : integer := 5;
  constant FEP_UC_IRQ : integer := 6;
  constant MAPPER_PSS_IRQ : integer := 7;
  constant MAPPER_DMA_IRQ : integer := 8;
  constant MAPPER_UC_IRQ : integer := 9;
  constant RAM_DMA_IRQ : integer := 10;
  constant PRE_PROCESSOR_AD0_IRQ : integer := 0;
  constant PRE_PROCESSOR_AD1_IRQ : integer := 1;
  constant PRE_PROCESSOR_AD2_IRQ : integer := 2;
  constant PRE_PROCESSOR_AD3_IRQ : integer := 3;
  constant PRE_PROCESSOR_PSS_IRQ : integer := 4;
  constant PRE_PROCESSOR_DMA_IRQ : integer := 5;
  constant PRE_PROCESSOR_UC_IRQ : integer := 6;
  constant BRIDGE_IRQ : integer := 10;
  constant CHANNEL_DECODER_PSS_IRQ : integer := 19;
  constant CHANNEL_DECODER_DMA_IRQ : integer := 20;
  constant CHANNEL_DECODER_UC_IRQ : integer := 21;

  -- UPDATE THIS

  constant idcross: natural range 0 to INS_MAX - 1 := 0;                                 --* Instance number:Crossbar
  constant idbri:   natural range 0 to INS_MAX - 1 := idcross + BRIDGE_ENABLE;           --* Instance number:Bridge
  constant idfep :  natural range 0 to INS_MAX - 1 := idbri + FEP_ENABLE;                --* Instance number:Front eINS_MAX processor
  constant idram:   natural range 0 to INS_MAX - 1 := idfep + RAM_ENABLE;                --* Instance number:VCI RAM
  constant idintl:  natural range 0 to INS_MAX - 1 := idram + INTL_ENABLE;               --* Instance number:Interleaver
  constant idmapp : natural range 0 to INS_MAX - 1 := idintl + MAPPER_ENABLE;            --* Instance number:Mapper
  constant idspi:   natural range 0 to INS_MAX - 1 := idmapp + SPI_ENABLE;               --* Instance number:SPI controller interface
  constant idchdc:  natural range 0 to INS_MAX - 1 := idspi + CHANNEL_DECODER_ENABLE;    --* Instance number:Channel decoder
  constant idagrf:  natural range 0 to INS_MAX - 1 := idspi + AGRF_ENABLE;               --* Instance number:Agile RF control interface
  constant idpp  :  natural range 0 to INS_MAX - 1 := idagrf + PRE_PROCESSOR_ENABLE;     --* Instance number:Preprocessor
  constant idgpio : natural range 0 to INS_MAX - 1 := idpp + GPIO_ENABLE;                --* Instance number:HGPIO
  constant idadaif: natural range 0 to INS_MAX - 1 := idgpio + ADAIF_ENABLE;             --* Instance number:ADA interface


  constant cross : instance := (ven => TELECOM_PARISTECH, d => CROSSBAR, c => ND & NU & NC, ver => 0, nirq => 1, msk => X"FFF", addr => X"200", nt => 0, ni => 4, t => (others => 0),                                         i => (idbri, idintl, idmapp, idfep, others => 0));
  constant bri   : instance := (ven => TELECOM_PARISTECH, d => BRIDGE,   c => ND & NU & NC, ver => 0, nirq => 0, msk => X"000", addr => X"000", nt => 6, ni => 0, t => (idcross, idspi, idram, idintl, idmapp, idfep, others => 0), i => (others => 0));
  constant fep   : instance := (ven => TELECOM_PARISTECH, d => FEP,      c => WD & WU & WC, ver => 0, nirq => 3, msk => X"FFF", addr => X"206", nt => 3, ni => 4, t => (idintl, idmapp, idram, others => 0),                     i => (idbri, idram, idintl, idmapp, others => 0));
  constant ram   : instance := (ven => TELECOM_PARISTECH, d => RAM,      c => WD & NU & NC, ver => 0, nirq => 1, msk => X"FFF", addr => X"201", nt => 2, ni => 3, t => (idfep, idspi, others => 0),                             i => (idbri, idfep, idmapp, others => 0));
  constant intl  : instance := (ven => TELECOM_PARISTECH, d => INTL,     c => WD & WU & WC, ver => 0, nirq => 3, msk => X"FFF", addr => X"203", nt => 3, ni => 2, t => (idfep, idmapp, idram, others => 0),                      i => (idbri, idram, others => 0));
  constant mapp  : instance := (ven => TELECOM_PARISTECH, d => MAPPER,   c => WD & WU & WC, ver => 0, nirq => 3, msk => X"FFF", addr => X"206", nt => 3, ni => 1, t => (idfep, idintl, others => 0),                            i => (idbri, others => 0));
  constant spi   : instance := (ven => EURECOM,           d => SPI,      c => ND & NU & WC, ver => 0, nirq => 0, msk => X"FFF", addr => X"205", nt => 0, ni => 3, t => (others => 0),                                         i => (idbri, idintl, idmapp, others => 0));

  constant instances : vinstance := (
  idcross => cross,
  idbri => bri,
  idfep => fep,
  idram => ram,
  idintl => intl,
  idmapp => mapp,
  idspi => spi,
  others => no_instance);
  

end package crossbar_map;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
