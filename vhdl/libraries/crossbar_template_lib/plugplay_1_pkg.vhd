--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library crossbar_lib;
use crossbar_lib.plugplay_0_pkg.all;
use crossbar_lib.crossbar_pkg.all;

use work.crossbar_map.all;

package plugplay_1_pkg is
  
  constant nins: natural := get_nins(instances);       --* Instance number
  constant ni:   natural := get_ni(instances);         --* Number of initiators
  constant nt:   natural := get_nt(instances);         --* Number of targets
  constant nirq: natural := get_nirq(instances);       --* Number of irqs

  constant rom: rom_t := generate_rom(instances, nins, nirq); --* Plug&Play rom   

  subtype iid is natural range 0 to ni - 1;  --* Initiator id
  subtype tid is natural range 0 to nt - 1;  --* Target id

  type tdef is record          --* target definition
    n: positive range 1 to ni; --* number of connected initiators
    i: natv(0 to ni - 1);      --* list of IDs of connected initiators in decreasing order of fixed priority; n to ni-1 are ignored
  end record;
  type tdefv is array(natural range 0 to nt - 1) of tdef; --* vector of target definitions
  type idef is record              --* initiator definition
    id: iid; --* id
    n: positive range 1 to nt;     --* number of connected targets
    t: natv(0 to nt - 1);          --* list of IDs of connected targets in decreasing order of fixed priority; n to nt-1 are ignored
  end record;
  type idefv is array(natural range 0 to ni - 1) of idef; --* vector of initiator definitions

  type iid_table is array(natural range 0 to nins - 1) of iid;
  type tid_table is array(natural range 0 to nins - 1) of tid;
  subtype irqid is natural range 0 to nirq- 1;  --* irq id
  type irqidv is array(natural range 0 to nirq - 1) of irqid;
  type irqid_table is array(natural range 0 to nins - 1) of irqidv;

  -- Outputs a table T of cumulated number of targets: T(I) is the number of targets that initiators with id<=I can send requests to.
  function get_iidt(vi : vinstance) return iid_table; 
  -- Outputs a table T of cumulated number of initiators: T(I) is the number of initiators that targets with id<=I can receive requests from.
  function get_tidt(vi : vinstance) return tid_table; 
  function get_irqidt(vi : vinstance) return irqid_table; 
  function get_tdef(vi : vinstance; iidt : iid_table) return tdefv; 
  function get_idef(vi : vinstance; tidt : tid_table) return idefv;
  function get_tmap(vi : vinstance) return tmapv; 


end package plugplay_1_pkg;

package body plugplay_1_pkg is

  function get_irqidt(vi : vinstance) return irqid_table is 
    variable t: irqid_table;
    variable n: natural;
  begin

    n := 0;
    l0 : for i in 0 to INS_MAX - 1 loop
      exit l0 when vi(i) = no_instance;
      if vi(i).nirq /= 0 then
        l1 : for j in 0 to vi(i).nirq - 1 loop
          t(i)(j) := n;
          n := n + 1;
        end loop;
      end if;
    end loop;
    return t;
  end function get_irqidt;

  function get_iidt(vi : vinstance) return iid_table is 
    variable t: iid_table;
    variable n: natural;
  begin

    n := 0;
    l : for i in 0 to INS_MAX - 1 loop
      exit l when vi(i) = no_instance;
      if vi(i).nt /= 0 then
        t(i) := n;
        n := n + 1;
      end if;
    end loop;
    return t;
  end function get_iidt;

  function get_tidt(vi : vinstance) return tid_table is 
    variable t: tid_table;
    variable n: natural;
  begin

    n := 0;
    l : for i in 0 to INS_MAX - 1 loop
      exit l when vi(i) = no_instance;
      if vi(i).ni /= 0 then
        t(i) := n;
        n := n + 1;
      end if;
    end loop;
    return t;
  end function get_tidt;

 function get_tdef(vi : vinstance; iidt : iid_table) return tdefv is 
    variable tdef : tdefv;
    variable nt: natural;
  begin

    nt := 0;
    l0 : for i in 0 to INS_MAX - 1 loop
      exit l0 when vi(i) = no_instance;
      if vi(i).ni /= 0 then
        tdef(nt).n := vi(i).ni;
        l1 : for j in 0 to  ni - 1 loop
          tdef(nt).i(j) := iidt(vi(i).i(j));
        end loop;
        nt := nt + 1;
      end if;
    end loop;
    return tdef;
  end function get_tdef;

  function get_idef(vi : vinstance; tidt : tid_table) return idefv is 
    variable idef : idefv;
    variable n: natural;
  begin

    n := 0;
    l0 : for i in 0 to INS_MAX - 1 loop
      exit l0 when vi(i) = no_instance;
      if vi(i).nt /= 0 then
        idef(n).id := n;
        idef(n).n := vi(i).nt;
        l1 : for j in 0 to  nt - 1 loop
          idef(n).t(j) := tidt(vi(i).t(j));
        end loop;
        n := n + 1;
      end if;
    end loop;
    return idef;
  end function get_idef;

 function get_tmap(vi : vinstance) return tmapv is 
    variable tmap : tmapv(0 to nt - 1);
    variable n: natural;
  begin

    n := 0;
    l0 : for i in 0 to INS_MAX - 1 loop
      exit l0 when vi(i) = no_instance;
      if vi(i).ni /= 0 then
        tmap(n).id := n;
        tmap(n).ba := vi(i).addr;
        tmap(n).msk := vi(i).msk;
        n := n + 1;
      end if;
    end loop;
    return tmap;
  end function get_tmap;
end package body plugplay_1_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
