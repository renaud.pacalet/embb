--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for crossbar.

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;
use global_lib.sim_utils.all;

library crossbar_lib;
use crossbar_lib.crossbar_pkg.all;

use work.crossbar_map.all;

entity crossbar_sim is
  generic(n_tests: positive := 1000000);
end entity;

architecture sim of crossbar_sim is

  signal clk:   std_ulogic;
  signal srstn: std_ulogic;
  signal i2x:   vci_i2t_vector(0 to ni - 1);
  signal x2i:   vci_t2i_vector(0 to ni - 1);
  signal t2x:   vci_t2i_vector(0 to nt - 1);
  signal x2t:   vci_i2t_vector(0 to nt - 1);
  signal irq:   std_ulogic;
  signal eos:   boolean := false;

begin

  -- component instantiation
  u_crossbar: entity work.crossbar(rtl)
    generic map(idefs => idefs,
                tmaps => tmaps,
                tdefs => tdefs)
    port map(clk   => clk,
             srstn => srstn,
             i2x   => i2x,
             x2i   => x2i,
             t2x   => t2x,
             x2t   => x2t,
             irq   => irq);

  process
    variable l: line;
  begin
    clk <= '0';
    wait for 1 ns;
    clk <= '1';
    wait for 1 ns;
    if eos then
      write(l, string'("End of simulation, regression test PASSED"));
      writeline(output, l);
      wait;
    end if;
  end process;

  process
    variable l: line;
  begin
    srstn <= '0';
    for i in 1 to 5 loop
      wait until rising_edge(clk);
    end loop;
    srstn <= '1';
    for i in 1 to n_tests loop
      wait until rising_edge(clk);
      if i mod (n_tests / 100) = 0 then
        write(l, i / (n_tests / 100));
        write(l, string'("%"));
        writeline(output, l);
      end if;
    end loop;
    eos <= true;
  end process;

  gi: for i in 0 to ni - 1 generate
    init_pr: process(clk)
      constant t: natural := idefs(i).n;
      variable n: natural range 0 to t - 1;
      variable tid: natural range 0 to nt - 1;
      variable ba: xtag;
    begin
      if rising_edge(clk) then
        if srstn = '0' then
          i2x(i) <= vci_i2t_none;
        else
          i2x(i) <= vci_i2t_rnd;
          n := int_rnd(0, t - 1);
          tid := tmaps(idefs(i).t(n)).id;
          ba := tmaps(tid).ba;
          if int_rnd(1, 100) /= 1 then
            i2x(i).req.address(vci_n - 1 downto vci_n - xtag_len) <= ba;
          end if;
        end if;
      end if;
    end process init_pr;
  end generate gi;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
