#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

IGNORE	+= crossbar_template_lib

crossbar_template_lib.crossbar: \
	global_lib.global \
	global_lib.utils \
	crossbar_lib.crossbar_pkg \
	crossbar_lib.plugplay_0_pkg \
	crossbar_lib.tgtif \
	crossbar_lib.qarbiter \
	crossbar_lib.parbiter \
	crossbar_template_lib.plugplay_1_pkg \
	crossbar_template_lib.crossbar_def

crossbar_template_lib.crossbar_def: \
	crossbar_template_lib.plugplay_1_pkg \
	crossbar_template_lib.crossbar_map

crossbar_template_lib.crossbar_map: \
	crossbar_lib.crossbar_pkg \
	crossbar_lib.plugplay_0_pkg

crossbar_template_lib.crossbar_sim: \
	global_lib.global \
	global_lib.utils \
	global_lib.sim_utils \
	random_lib.rnd \
	crossbar_lib.crossbar_pkg \
	crossbar_template_lib.crossbar_map \
	crossbar_template_lib.crossbar

crossbar_template_lib.crossbar_sim_pkg: \
	global_lib.global \
	crossbar_lib.crossbar_pkg \
	random_lib.rnd \
	crossbar_template_lib.crossbar_map

crossbar_template_lib.crossbar_top: \
	global_lib.global \
	crossbar_lib.crossbar_pkg \
	crossbar_template_lib.crossbar_map \
	crossbar_template_lib.crossbar

crossbar_template_lib.plugplay_1_pkg: \
	crossbar_lib.plugplay_0_pkg \
	crossbar_template_lib.crossbar_map

ms-sim.crossbar_template_lib.crossbar_sim: MSSIMFLAGS += -do 'run -all; quit' -c -t ps
ms-sim.crossbar_template_lib: ms-sim.crossbar_template_lib.crossbar_sim
ms-sim: ms-sim.crossbar_template_lib

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
