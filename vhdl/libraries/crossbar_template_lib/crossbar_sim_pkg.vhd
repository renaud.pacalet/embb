--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Utility package for crossbar simulations

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use global_lib.global.all;

library crossbar_lib;
use crossbar_lib.crossbar_pkg.all;

use work.crossbar_map.all;

package crossbar_sim_pkg is

    type request_t;
    type node is access request_t;
    type request_t  is record 
      r: vci_i2t_type;
      n: node;
    end record request_t;
    procedure free_requests(h: inout node);
    procedure add_request(i2t: in vci_i2t_type; reqs: inout node);
    procedure check_response(t2i: in vci_t2i_type; reqs: inout node;
      good: out boolean);
--   -- VCI target types
--   type target_response_driver_type is (NODRVR, REG2);
--   type target_regs_type is record
--     empty: boolean;
--     id: vci_id_type;
--     eop: boolean;
--   end record;
-- 
--   -- VCI initiator types
--   -- number of maximum instructions for each VCI initiator component
--   constant max_inst: natural := 1000;
--   --type request_table_type is (transaction, target, burst, srcid, trdid, pktid, eop);
--   type request_table_t is array(natural range 0 to 6) of natural;
--   type request_table_t2 is array(natural range <>) of request_table_t;
--   type request_table_t3 is array(natural range <>) of request_table_t2(1 to max_inst);
--   procedure request_table_generator(
--     constant init: in natural range 0 to nb_init-1;
--     variable request_table: out request_table_t2(1 to max_inst));
--   procedure request(
--     add: in integer;
--     wdata: in u_unsigned(8*vci_b-1 downto 0);
--     srcid: in natural range 0 to 2**vci_s-1;
--     trdid: in natural range 0 to 2**vci_t-1;
--     pktid: in natural range 0 to 2**vci_p-1;
--     eop: in natural range 0 to 1;
--     signal vci_out: out vci_target_in_type);

end package crossbar_sim_pkg;

package body crossbar_sim_pkg is
    procedure free_requests(h: inout node) is
    begin
      if h /= null then
        free_requests(h.n);
        deallocate(h);
      end if;
    end procedure free_requests;
    procedure add_request(i2t: in vci_i2t_type; reqs: inout node) is
      variable n: node;
    begin
      if i2t.req.cmdval = '1' then
        n := new request_t;
        n.r := i2t;
        n.n := reqs;
        reqs := n;
      end if;
    end procedure add_request;
    procedure check_response(t2i: in vci_t2i_type; reqs: inout node;
      good: out boolean) is
      variable n, p: node;
    begin
      good := true;
      p := reqs;
      n := reqs;
      while n /= null loop
        if (t2i.rsp.rtrdid = n.r.req.trdid) and (t2i.rsp.rpktid = n.r.req.pktid) and
          (t2i.rsp.reop = n.r.req.eop) then -- match
          p.n := n.n;
          deallocate(n);
          if n = reqs then
            reqs := null;
          end if;
          return;
        end if;
        p := n;
        n := n.n;
      end loop;
      good := false;
    end procedure check_response;
--   procedure request_table_generator(
--     constant init: in natural range 0 to nb_init-1;
--     variable request_table: out request_table_t2(1 to max_inst)) is
--     variable rt: request_table_t2(1 to max_inst);
--     variable transaction: natural range 0 to 1;
--     variable target: natural range 0 to nb_target-1;
--     variable burst: natural; -- range 1 to max_burst;
--     variable t: natural range 0 to 2**vci_t-1;
--     variable p: natural range 0 to 2**vci_p-1;
--     variable eop: natural range 0 to 1;
--     variable ci: natural range 0 to nb_init;
--   begin
--     transaction := 0;
--     target := 0;
--     burst := 1;
--     t := 0;
--     p := 0;
--     rt := (others => (others => 0));
--     for inst in 1 to max_inst loop
--       -- First inst or No transaction in last inst
--       if inst = 1 or rt(inst-1)(0) = 0 then
--         if inst = max_inst then transaction := 0; else
--         transaction := int_rnd(0,1); end if;
--         target := int_rnd(0,nb_target-1);
--         burst := 8; --int_rnd(1,to_integer(mapping_table(target)(1)/vci_b));
--         t := (t + 1) mod 2**vci_t;
--         p := 0;
--         if burst = 1 then eop := 1; else eop := 0; end if;
--       -- Transaction in last inst, but no burst
--       elsif rt(inst-1)(0) = 1 and rt(inst-1)(2) = 1 then
--         transaction := 0;
--       -- Transaction in last inst and it was burst
--       elsif rt(inst-1)(0) = 1 and rt(inst-1)(2) > 1 then
--         if burst /= 1 then
--           if inst = max_inst then transaction := 0;
--           else transaction := 1; end if;
--           target := rt(inst-1)(1);
--           burst := rt(inst-1)(2) - 1;
--           p := (p + 1) mod 2**vci_p;
--           if burst = 1 then eop := 1;
--           else eop := 0; end if;
--         else
--           transaction := 0;
--         end if;
--       end if;
--       rt(inst) := (transaction, target, burst, init, t, p, eop);
--     end loop;
--     request_table := rt;
--   end procedure;
-- 
--   procedure request(add: in integer;
--                     wdata: in u_unsigned(8*vci_b-1 downto 0);
--                     srcid: in natural range 0 to 2**vci_s-1;
--                     trdid: in natural range 0 to 2**vci_t-1;
--                     pktid: in natural range 0 to 2**vci_p-1;
--                     eop: in natural range 0 to 1;
--                     signal vci_out: out vci_target_in_type) is
--   begin
--     vci_out.cmdval <= '1';
--     vci_out.address <= std_ulogic_vector(to_signed(add,vci_n));
--     vci_out.wdata <= std_ulogic_vector(wdata);
--     vci_out.cmd <= "00";
--     if vci_b = 1 then vci_out.be <= std_ulogic_vector(to_unsigned(1,vci_b));
--     elsif vci_b = 2 then vci_out.be <= std_ulogic_vector(to_unsigned(3,vci_b));
--     elsif vci_b = 4 then vci_out.be <= std_ulogic_vector(to_unsigned(15,vci_b));
--     else vci_out.be <= std_ulogic_vector(to_unsigned(255,vci_b));
--     end if;
--     vci_out.srcid <= std_ulogic_vector(to_unsigned(srcid,vci_s));
--     vci_out.trdid <= std_ulogic_vector(to_unsigned(trdid,vci_t));
--     vci_out.pktid <= std_ulogic_vector(to_unsigned(pktid,vci_p));
--     if eop = 1 then vci_out.eop <= '1'; else vci_out.eop <= '0'; end if;
--     vci_out.rspack <= '1';
--   end procedure;

end package body crossbar_sim_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
