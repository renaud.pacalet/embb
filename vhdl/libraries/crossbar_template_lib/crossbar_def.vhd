--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

library ieee;
use ieee.std_logic_1164.all;

use work.plugplay_1_pkg.all;
use work.crossbar_map.all;

library crossbar_lib;
use crossbar_lib.crossbar_pkg.all;

package crossbar_def is

	constant iidt:		iid_table	:= get_iidt(instances);
	constant tidt:		tid_table	:= get_tidt(instances);
	constant irqidt:	irqid_table	:= get_irqidt(instances);
	constant tmaps:		tmapv		:= get_tmap(instances);
	constant tdefs:		tdefv		:= get_tdef(instances, iidt);
	constant idefs:		idefv		:= get_idef(instances, tidt);

end package crossbar_def;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
