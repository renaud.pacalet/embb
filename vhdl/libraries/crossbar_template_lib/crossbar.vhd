--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief AVCI-compliant programmable partial crossbar
--*
--* This module implements a Advanced VCI compliant partial crossbar.
--* Depending on the configuration defined in the CROSSBAR_MAP package and on
--* this architecture, VCI requests issued by VCI initiators are routed to some
--* (or all) of the VCI targets and the responses are routed back from targets to
--* initiators. Conflicting requests at the input of a target are arbitrated by a
--* dedicated generic arbiter: QARBITER. Symmetrically, conflicting responses at
--* the input of an initiator are arbitrated by a dedicated generic arbiter:
--* PARBITER. The arbitration policy is a mixture of fixed priority and rolling
--* token (see the source code of Q and P arbiters). An interrupt requests (IRQ)
--* is raised when an initiator issues a request at an unmapped address or when a
--* target issues a response carrying a RSRCID tag that match none of the
--* initiators IDs. The crossbar is fully configured through its three generic
--* parameters.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

library crossbar_lib;
use crossbar_lib.crossbar_pkg.all; --* type definitions
use crossbar_lib.plugplay_0_pkg.all; --* Plug&Play definitions

use work.plugplay_1_pkg.all; --* Plug&Play definitions
use work.crossbar_def.all; --* Configuration of the crossbar

entity crossbar is
  generic(idefs: idefv;  --* initiators definitions
          tmaps: tmapv;  --* target maps
          tdefs: tdefv; --* targets definitions
          rom : rom_t;  --* Plug&Play ROM
          nirq : natural; --* IRQs number
          nins : natural); --* Instances number
  port (
    clk:   in  std_ulogic;             --* master clock
    srstn : in  std_ulogic;             --* synchronous, active low, reset
    tvci_in : in vci_i2t_type;            
    tvci_out : out vci_t2i_type;
    i2x:   in  vci_i2t_vector(0 to ni - 1);                 --* initiator incoming requests
    x2i:   out vci_t2i_vector(0 to ni - 1);                 --* outgoing responses to initiators
    t2x:   in  vci_t2i_vector(0 to nt - 1);                 --* target incoming responses
    x2t:   out vci_i2t_vector(0 to nt - 1);                 --* outgoing requests to targets
    srstnout : out std_ulogic_vector(nins - 1 downto 0);    --* Synchronous active low reset for each DSP unit
    ce :   out std_ulogic_vector(nins - 1  downto 0);       --* Chip enabled for each DSP unit
    irqi : in std_ulogic_vector(nirq - 1 downto 0);         --* Edge-triggered interruptions from DSP unit
    hirq : out std_ulogic_vector(nins - 1 downto 0);        --* Host interruption for each DSP unit
    cirq:  out std_ulogic;                                  --* crossbar interrupt request
    irq:   out std_ulogic                                   --* interrupt request
  );
end entity crossbar;

architecture rtl of crossbar is

  type x2qst is array(0 to nt - 1) of vci_i2t_vector(0 to ni - 1); --* crossbar to target arbiters
  type q2xst is array(0 to nt - 1) of vci_t2i_vector(0 to ni - 1); --* target arbiters to crossbar
  type x2pst is array(0 to ni - 1) of vci_t2i_vector(0 to nt - 1); --* crossbar to initiator arbiters
  type p2xst is array(0 to ni - 1) of vci_i2t_vector(0 to nt - 1); --* initiator arbiters to crossbar

  signal x2qs: x2qst; --* crossbar to target arbiters
  signal q2xs: q2xst; --* target arbiters to crossbar
  signal x2ps: x2pst; --* crossbar to initiator arbiters
  signal p2xs: p2xst; --* initiator arbiters to crossbar

  signal i_irqs: std_ulogic_vector(0 to ni - 1); --* initiator interrupt requests
  signal t_irqs: std_ulogic_vector(0 to nt - 1); --* target interrupt requests

  signal p2xsv: p2xst; --* 2D array of requests (initiator ID, target ID)
  attribute ram_block of p2xsv: signal is false;
  signal q2xsv: q2xst; --* 2D array of responses (target ID, initiator ID)
  attribute ram_block of q2xsv: signal is false;

begin

  --* The crossbar target interface is used to configure each DSP unit (reset, chip enabled), 
  --* to get Plug&Play information, to get last high-priority interruption and to generate
  --* interruptions on DSP unit.
  tgt: entity crossbar_lib.tgtif
    generic map(rom  => rom,
                nirq => nirq,
                nins => nins)
    port map(clk       => clk,                          -- Master clock
             srstnin   => srstn,                        -- Synchronous active low reset
             srstnout  => srstnout,                     -- Synchronous active low reset for each DSP unit
             ce        => ce,                           -- Chip enabled for each DSP unit
             tvci_in   => tvci_in,                      -- Target vci input signals
             tvci_out  => tvci_out,                     -- Target vci output signals
             irqo      => irq,                          -- Edge-triggered interruption to host
             hirq      => hirq,                         -- Host interruption to DSP unit
             irqi      => irqi                          -- Edge-triggered interruptions form DSP unit
            );

  --* Arbiters of requests from VCI initiators to VCI targets. One arbiter per
  --* target. Also duplicates target responses to send a copy to each connected
  --* initiator. At most one of the copies has the RSPVAL flag set: the one sent
  --* to the initiator which ID match the RSRCID field. IRQ is raised when a
  --* response carries a RSRCID field that match none of the ids of the connected
  --* initiators.
  qas: for t in 0 to nt - 1 generate
    qa: entity crossbar_lib.qarbiter
      generic map(ilist => tdefs(t).i(0 to tdefs(t).n - 1), -- list of connected initiators
                  msk   => tmaps(t).msk)                    -- target address mask
      port map(clk   => clk,                          -- master clock
               srstn => srstn,                        -- Synchronous active low reset
               x2qs  => x2qs(t)(0 to tdefs(t).n - 1), -- Incoming requests
               q2t   => x2t(t),                       -- Outgoing selected request
               t2q   => t2x(t),                       -- Incoming response
               q2xs  => q2xs(t)(0 to tdefs(t).n - 1), -- Outgoing copies of response
               irq   => t_irqs(t));                   -- Interrupt request
  end generate qas;

  --* Arbiters of responses from VCI targets to VCI initiators. One arbiter per
  --* initiator. Also duplicates initiator requests to send a copy to each
  --* connected target. At most one of the copies has the CMDVAL flag set: the
  --* one sent to the target which address range match the request address. IRQ
  --* is raised when a request address falls in none of the address ranges of the
  --* connected targets. Address decoding is done on the XTAG_LEN MSBs of byte
  --* addresses and according to a XTAG_LEN mask: if BA and MSK are the base
  --* address and mask of a target (each XTAG_LEN bits long) and if the initiator
  --* requests at address A, it matches iff:
  --*   A(VCI_N - 1 downto VCI_N - XTAG_LEN) and MSK = BA
  pas: for i in 0 to ni - 1 generate
    pa: entity crossbar_lib.parbiter
      generic map(tmaps => tmaps,                           -- Maps of all know targets
                  tlist => idefs(i).t(0 to idefs(i).n - 1), -- list of connected target IDs
                  id    => idefs(i).id)                     -- initiator's id
      port map(clk   => clk,                          -- master clock
               srstn => srstn,                        -- Synchronous active low reset
               x2ps  => x2ps(i)(0 to idefs(i).n - 1), -- Incoming responses
               p2i   => x2i(i),                       -- Outgoing selected response
               i2p   => i2x(i),                       -- Incoming request
               p2xs  => p2xs(i)(0 to idefs(i).n - 1), -- Outgoing copies of requests
               irq   => i_irqs(i));                   -- Interrupt request
  end generate pas;

  --* Routing of initiator requests to targets. We first build a NI times
  --* NT array of requests, addressed by initiators and targets ids. Then we
  --* distribute this array among the input request ports of the targets.
  p2q_routing_1i: for i in 0 to ni - 1 generate           -- for all initiators
    p2q_routing_1t: for t in 0 to idefs(i).n - 1 generate -- for all connected targets
      p2xsv(idefs(i).id)(idefs(i).t(t)) <= p2xs(i)(t);    -- initiator-to-target request
    end generate p2q_routing_1t;
  end generate p2q_routing_1i;
  p2q_routing_2t: for t in 0 to nt - 1 generate           -- for all targets
    p2q_routing_2i: for i in 0 to tdefs(t).n - 1 generate -- for all connected initiators
      x2qs(t)(i) <= p2xsv(tdefs(t).i(i))(tmaps(t).id);    -- initiator-to-target request
    end generate p2q_routing_2i;
  end generate p2q_routing_2t;

  --* Routing of targets responses to initiators. We first build a NT time
  --* NI array of responses, addressed by targets and initiators ids. Then we
  --* distribute this array among the input response ports of the initiators.
  q2p_routing_1t: for t in 0 to nt - 1 generate
    q2p_routing_1i: for i in 0 to tdefs(t).n - 1 generate
      q2xsv(tmaps(t).id)(tdefs(t).i(i)) <= q2xs(t)(i);
    end generate q2p_routing_1i;
  end generate q2p_routing_1t;
  q2p_routing_2i: for i in 0 to ni - 1 generate
    q2p_routing_2t: for t in 0 to idefs(i).n - 1 generate
      x2ps(i)(t) <= q2xsv(idefs(i).t(t))(idefs(i).id);
    end generate q2p_routing_2t;
  end generate q2p_routing_2i;

  --* Global IRQ is the or of all local IRQs
  cirq <= or_reduce(i_irqs & t_irqs);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
