--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Dummy top level for synthesis tests

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;

library crossbar_lib;
use crossbar_lib.crossbar_pkg.all;

use work.crossbar_map.all;

entity crossbar_top is
  port (
    clk:   in  std_ulogic;             --* master clock
    srstn : in  std_ulogic;             --* synchronous, active low, reset
    tvci_in : in vci_i2t_type;            
    tvci_out : out vci_t2i_type;
    i2x:   in  vci_i2t_vector(0 to ni - 1);                 --* initiator incoming requests
    x2i:   out vci_t2i_vector(0 to ni - 1);                 --* outgoing responses to initiators
    t2x:   in  vci_t2i_vector(0 to nt - 1);                 --* target incoming responses
    x2t:   out vci_i2t_vector(0 to nt - 1);                 --* outgoing requests to targets
    srstnout : out std_ulogic_vector(nins - 1 downto 0);    --* Synchronous active low reset for each DSP unit
    ce :   out std_ulogic_vector(nins - 1  downto 0);       --* Chip enabled for each DSP unit
    irqi : in std_ulogic_vector(nirq - 1 downto 0);         --* Edge-triggered interruptions from DSP unit
    hirq : out std_ulogic_vector(nins - 1 downto 0);        --* Host interruption for each DSP unit
    cirq:  out std_ulogic;                                  --* crossbar interrupt request
    irq:   out std_ulogic                                   --* interrupt request
  );
end entity crossbar_top;

architecture rtl of crossbar_top is

begin

  -- component instantiation
  u_crossbar: entity work.crossbar(rtl)
    generic map(idefs => idefs,
                tmaps => tmaps,
                tdefs => tdefs,
                rom => rom,
                nirq => nirq,
                nins => nins)
    port map(clk   => clk,
             srstn => srstn,
             tvci_in => tvci_in,            
             tvci_out => tvci_out,            
             i2x   => i2x,
             x2i   => x2i,
             t2x   => t2x,
             x2t   => x2t,
             srstnout => srstnout,
             ce => ce,    
             irqi => irqi, 
             hirq => hirq,
             cirq => cirq,
             irq => irq);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
