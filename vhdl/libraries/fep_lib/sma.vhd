--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Sum/Minmax/Argminargmax module of FEP
--*
--* PLease see the FEP user guide for a complete description of the extra values
--* computation. This module is synchronous on the rising edge of its clock CLK.
--* Its reset SRSTN is synchronous and active low.
--*
--* Important note: this module has been validated by extensive simulations. It
--* synthesizes on a LX330 Virtex 5 5VLX330FF1760-2 target with a 200 MHz clock
--* frequency and 225 LUTs. On a 65 nm ASIC target, worst case corner, it
--* synthesizes with a 450 MHz clock frequency and 4000 square microns. Modify at
--* your own risks. Do not use a modified version before running the regression
--* tests:
--* $ make sma_sim.sim

-- pragma translate_off
use std.textio.all;
-- pragma translate_on

library ieee;
use ieee.std_logic_1164.all;

library global_lib;

use ieee.numeric_std.all;
use global_lib.utils.all;

-- pragma translate_off
use global_lib.sim_utils.all;
-- pragma translate_on

use work.type_pkg.all;
use work.fep_pkg.all;

entity sma is
-- pragma translate_off
  generic(debug_sma: boolean := false);
-- pragma translate_on
  port(clk  : in  std_ulogic;
       srstn: in  std_ulogic;    -- synchronous, active low, reset
       ce:    in  std_ulogic;    -- synchronous, active high, chip enable
       ws   : in  boolean;       -- SMA active
       mul  : in  boolean;       -- CWP, CWM, CWS or CWL
       r    : in  natural range 0 to 7; -- final right shift
       dsi  : in  vp_components_left;       -- Remaining components (zero, one, two, more)
       z    : in  cpx66_vector(0 to 1); -- 2 input data
       wdata: out v128_vector(0 to 1);  -- 2 output data
       eoc  : out std_ulogic);              -- End Of Computation
end entity sma;

architecture rtl of sma is

  signal sum, sum_d: cpx94;
  signal zr: cpx66_vector(0 to 1);
  signal max, min, max_d, min_d: cpx66;
  signal argmax, argmin, argmax_d, argmin_d: cpx32;
  signal cnt: u_unsigned(13 downto 0);
  type state_type is (idle, running, last2, last1, post);
  signal state: state_type;

begin

  ctrl_p: process(clk) -- Control process
-- pragma translate_off
    variable l: line;
-- pragma translate_on
  begin
    if rising_edge(clk) then
-- pragma translate_off
      if debug_sma and ws and state /= idle then
        hwrite(l, cnt);
        write(l, string'(": z0=("));
        hwrite(l, zr(0).r);
        write(l, string'(","));
        hwrite(l, zr(0).i);
        write(l, string'("), z1=("));
        hwrite(l, zr(1).r);
        write(l, string'(","));
        hwrite(l, zr(1).i);
        write(l, string'("), sum=("));
        hwrite(l, sum.r);
        write(l, string'(","));
        hwrite(l, sum.i);
        write(l, string'("), max=("));
        hwrite(l, max.r);
        write(l, string'(","));
        hwrite(l, max.i);
        write(l, string'("), min=("));
        hwrite(l, min.r);
        write(l, string'(","));
        hwrite(l, min.i);
        write(l, string'("), argmax=("));
        hwrite(l, argmax.r);
        write(l, string'(","));
        hwrite(l, argmax.i);
        write(l, string'("), argmin=("));
        hwrite(l, argmin.r);
        write(l, string'(","));
        hwrite(l, argmin.i);
        write(l, string'(")"));
        writeline(output, l);
      end if;
-- pragma translate_on
      if srstn = '0' then
        eoc  <= '0';
        state  <= idle;
      elsif ce = '1' then
        eoc  <= '0';
        zr     <= (others => (others => (others => '0')));
        wdata  <= (others => (others => '0'));
        case state is
          when idle =>
            sum    <= (others => (others => '0'));
            max    <= (others => (32 => '1', others => '0'));
            min    <= (others => (32 => '0', others => '1'));
            argmax <= (others => (others => '0'));
            argmin <= (others => (others => '0'));
            cnt    <= (others => '0');
            if ws and dsi = more then
              state  <= running;
              zr     <= z;
            elsif ws and dsi = two then
              state <= last2;
              zr     <= z;
            elsif ws and dsi = one then
              state <= last1;
              zr(0) <= z(0);
            end if;
          when running =>
            sum    <= sum_d;
            max    <= max_d;
            argmax <= argmax_d;
            min    <= min_d;
            argmin <= argmin_d;
            -- pragma translate_off
            assert dsi /= zero
              report "SMA: invalid DSI value"
              severity failure;
            -- pragma translate_on
            cnt <= cnt + 2;
            zr(0) <= z(0);
            if dsi = one then
              state <= last1;
            else
              zr(1) <= z(1);
              if dsi = two then
                state <= last2;
              end if;
            end if;
          when last2 =>
            -- pragma translate_off
            assert dsi = zero
              report "SMA: invalid DSI value"
              severity failure;
            -- pragma translate_on
            sum    <= sum_d;
            max    <= max_d;
            argmax <= argmax_d;
            min    <= min_d;
            argmin <= argmin_d;
            state <= post;
          when last1 =>
            -- pragma translate_off
            assert dsi = zero
              report "SMA: invalid DSI value"
              severity failure;
            -- pragma translate_on
            sum    <= sum_d;
            max    <= max_d;
            argmax <= argmax_d;
            min    <= min_d;
            argmin <= argmin_d;
            state <= post;
          when post =>
            -- pragma translate_off
            assert dsi = zero
              report "SMA: invalid DSI value"
              severity failure;
            -- pragma translate_on
            wdata <= sma_output_conversion(sum, max, argmax, min, argmin, mul, r);
            state <= idle;
            eoc   <= '1';
        end case;
      end if;
    end if;
  end process ctrl_p;

  process(zr, sum, max, min, argmax, argmin, cnt, state)
    variable z1lz0, z0lmin, z1lmin: boolean;
    variable z1gz0, z0gmax, z1gmax: boolean;
  begin
    min_d    <= min;
    argmin_d <= argmin;
    max_d    <= max;
    argmax_d <= argmax;
    sum_d.r <= sum.r + zr(0).r + zr(1).r;
    sum_d.i <= sum.i + zr(0).i + zr(1).i;
    z0lmin := zr(0).r < min.r;
    z1lmin := zr(1).r < min.r;
    z1lz0 := zr(1).r < zr(0).r;
    if (state = last1 or (not z1lz0)) and z0lmin then
      min_d.r    <= zr(0).r;
      argmin_d.r <= u_signed(resize(cnt, 16));
    elsif (state = last2 or state = running) and z1lz0 and z1lmin then
      min_d.r    <= zr(1).r;
      argmin_d.r <= u_signed(resize(cnt(13 downto 1), 15)) & '1';
    end if;
    z0gmax := zr(0).r > max.r;
    z1gmax := zr(1).r > max.r;
    z1gz0 := zr(1).r > zr(0).r;
    if (state = last1 or (not z1gz0)) and z0gmax then
      max_d.r    <= zr(0).r;
      argmax_d.r <= u_signed(resize(cnt, 16));
    elsif (state = last2 or state = running) and z1gz0 and z1gmax then
      max_d.r    <= zr(1).r;
      argmax_d.r <= u_signed(resize(cnt(13 downto 1), 15)) & '1';
    end if;
    z0lmin := zr(0).i < min.i;
    z1lmin := zr(1).i < min.i;
    z1lz0 := zr(1).i < zr(0).i;
    if (state = last1 or (not z1lz0)) and z0lmin then
      min_d.i    <= zr(0).i;
      argmin_d.i <= u_signed(resize(cnt, 16));
    elsif (state = last2 or state = running) and z1lz0 and z1lmin then
      min_d.i    <= zr(1).i;
      argmin_d.i <= u_signed(resize(cnt(13 downto 1), 15)) & '1';
    end if;
    z0gmax := zr(0).i > max.i;
    z1gmax := zr(1).i > max.i;
    z1gz0 := zr(1).i > zr(0).i;
    if (state = last1 or (not z1gz0)) and z0gmax then
      max_d.i    <= zr(0).i;
      argmax_d.i <= u_signed(resize(cnt, 16));
    elsif (state = last2 or state = running) and z1gz0 and z1gmax then
      max_d.i    <= zr(1).i;
      argmax_d.i <= u_signed(resize(cnt(13 downto 1), 15)) & '1';
    end if;
  end process;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
