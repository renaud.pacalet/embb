#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

# In order to compile fftw_test, please define FFTWROOTDIR, either as an
# environment variable or as a Makefile variable by uncommenting and editing the
# following line:
# FFTWROOTDIR	= <someplace>
# $(FFTWROOTDIR)/include shall contain fftw3.h and $(FFTWROOTDIR)/lib shall
# contain libfftw3.a
#
# In order to compile mkcmdfiles, please define LIBEMBBEMUINSTALLDIR, either as an
# environment variable or as a Makefile variable by uncommenting and editing the
# following line:
LIBEMBBEMUINSTALLDIR	= /opt/embb
# $(LIBEMBBEMUINSTALLDIR)/include shall contain embb/fep.h and $(LIBEMBBEMUINSTALLDIR)/lib shall
# contain libembb.so and libembbemu.so

CC		= gcc
CFLAGS		= -c -g -Wall -pedantic -std=gnu99
INCLUDES	= -I. -I$(LIBEMBBEMUINSTALLDIR)/include
CXX		= g++
CXXFLAGS	= -c -g -std=c++0x
# CXXFLAGS	= -c -g -std=gnu++0x
LD		= g++
LDFLAGS		=
LIBS		=

## Bitfield source files generation
BFGEN		= ../../../../tools/bfgen/src/bfgen
BFGENFLAGS	= --backend cdefs cdefs_use_field_comment=0 cdefs_use_field_get=0 cdefs_use_field_set=0 cdefs_use_field_shifted_mask=0 cdefs_use_field_shifter=0 cdefs_use_field_values=0
BITFIELDSDIR	= ../../../../bitfields

FFTWINCLUDEDIR	= $(FFTWROOTDIR)/include
FFTWLIBDIR	= $(FFTWROOTDIR)/lib

EMBBINCLUDEDIR	= $(LIBEMBBEMUINSTALLDIR)/include
EMBBLIBDIR	= $(LIBEMBBEMUINSTALLDIR)/lib

CSRCS	= $(wildcard *.c)
CCSRCS	= $(wildcard *.cc)
COBJS	= $(patsubst %.c,%.o,$(CSRCS))
CCOBJS	= $(patsubst %.cc,%.o,$(CCSRCS))
OBJS	= $(COBJS) $(CCOBJS)
EXECS	= bank_mapping fftw_test ag mkcmdfile twiddle_generator

all: mkcmdfile

%.o: %.c
	$(CC) $(CFLAGS) $(INCLUDES) -c $<

%.o: %.cc
	$(CXX) $(CXXFLAGS) $(INCLUDES) -c $<

fep_addresses.h css_addresses.h : %_addresses.h : $(BITFIELDSDIR)/%.bitfield
	$(BFGEN) $(BFGENFLAGS) --input $< --output $@

bank_mapping: bank_mapping.o
	$(LD) $(LDLAGS) -o $@ $^ $(LIBS)

fftw_test.o: INCLUDES += -I$(FFTWINCLUDEDIR)
fftw_test: LDFLAGS += -L$(FFTWLIBDIR) -Wl,-rpath,$(FFTWLIBDIR)
fftw_test: LIBS += -lfftw3 -lm

fftw_test: fftw_test.o
	$(LD) $(LDFLAGS) -o $@ $< $(LIBS)

ag: ag.o
	$(LD) $(LDFLAGS) -o $@ $< $(LIBS)

tests.o mkcmdfile.o: INCLUDES += -I$(EMBBINCLUDEDIR)
tests.o mkcmdfile.o: utils.h tests.h fep_addresses.h css_addresses.h

mkcmdfile: LDFLAGS += -L$(EMBBLIBDIR) -Wl,-rpath,$(EMBBLIBDIR)
mkcmdfile: LIBS += -lembb -lembbemu

mkcmdfile: mkcmdfile.o tests.o utils.o
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

twiddle_generator: LIBS += -lm

twiddle_generator: twiddle_generator.o
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	rm -rf $(OBJS)

ultraclean:
	rm -rf $(OBJS) $(EXECS) fep_addresses.h css_addresses.h

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
