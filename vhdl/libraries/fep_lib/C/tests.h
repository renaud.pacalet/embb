/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#ifndef _TESTS_DEFS_
#define _TESTS_DEFS_

#include <embb/fep.h>
#include <embb/memory.h>
#include <stdint.h>

typedef struct {
  int16_t bx;
  int16_t qx;
  int16_t mx;
  int16_t nx;
  int16_t sx;
  int16_t px;
  int16_t wx;
  int16_t tx;
  int16_t vrx;
  int16_t vix;
  int16_t dx;
  int16_t tz;
  int16_t ri;
  int16_t ml;
  int16_t op;
  int16_t i;
  int16_t by;
  int16_t qy;
  int16_t my;
  int16_t ny;
  int16_t sy;
  int16_t py;
  int16_t wy;
  int16_t ty;
  int16_t vry;
  int16_t viy;
  int16_t dy;
  int16_t ls;
  int16_t li;
  int16_t ll;
  int16_t r;
  int16_t bz;
  int16_t qz;
  int16_t mz;
  int16_t nz;
  int16_t sz;
  int16_t wz;
  int16_t sma;
  int16_t bs;
  int16_t qs;
  int16_t l;
  uint64_t gpo;
} fep_params;

// Run one random test
void mktests(FILE *f, FEP_CONTEXT *ctx, fep_params cst, fep_params val);

#endif
