/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <stdio.h>

int main(int argc, char ** argv) {
  int ws = 0x1000; // Wraping section byte-length
  int bw = 2;      // Components byte-width
  int b = 0xfb1;   // Base component index
  int n = 0x5b;    // Integer increment
  int m = 0xbe;    // Fractional increment
  int s = 1;       // Increment sign
  int i, a, b0;

  int search = 7;

  ws /= bw;
  b0 = (b / ws) * ws;
  b -= b0;
  for(i = 0; i < 0x2b4a; i++) {
    a = (b + s * i * n + i / m) % ws;
    while(a < 0) a = a + ws;
    if(a == search) printf("%d\n", i);
  }
  return 0;
}
