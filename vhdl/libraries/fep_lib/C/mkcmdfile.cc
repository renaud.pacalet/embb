/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <embb/memory.h>
#include <embb/css.h>
#include <embb/fep.h>
#include <assert.h>
#include <stdio.h>
#include <stdarg.h>
#include <cstdint>
#include <cinttypes>
#include "utils.h"
#include "tests.h"
#include "css_addresses.h"
#include "fep_addresses.h"

// #define _GNU_SOURCE
#include <getopt.h>

/* Parse command line options, initialize file names. */
void parse_options (int argc, char **argv);

FILE *f;
int n;
int operations;
uint32_t seed;

CSS_FUNC_DECL

int main(int argc, char **argv)
{
  int i;
  FEP_CONTEXT ctx; // Context
  struct css_regs cr; // CSS registers
  fep_params cst, val; // Parameters constraints

  parse_options (argc, argv);

  fep_ctx_init(&ctx, 0);
  memset(&cr, 0, sizeof(struct css_regs));
  do_srand(seed);

  // Initialize cst (randomly pick fields)
  memset(&cst, 0, sizeof(cst));
  cst.bx  = 1;
  cst.qx  = 1;
  cst.mx  = 1;
  cst.nx  = 1;
  cst.sx  = 1;
  cst.px  = 1;
  cst.wx  = 1;
  cst.tx  = 1;
  cst.vrx = 1;
  cst.vix = 1;
  cst.dx  = 1;
  cst.tz  = 1;
  cst.ri  = 1;
  cst.ml  = 1;
  cst.op  = 1;
  cst.i   = 1;
  cst.by  = 1;
  cst.qy  = 1;
  cst.my  = 1;
  cst.ny  = 1;
  cst.sy  = 1;
  cst.py  = 1;
  cst.wy  = 1;
  cst.ty  = 1;
  cst.vry = 1;
  cst.viy = 1;
  cst.dy  = 1;
  cst.ls  = 1;
  cst.li  = 1;
  cst.ll  = 1;
  cst.r   = 1;
  cst.bz  = 1;
  cst.qz  = 1;
  cst.mz  = 1;
  cst.nz  = 1;
  cst.sz  = 1;
  cst.wz  = 1;
  cst.sma = 1;
  cst.bs  = 1;
  cst.qs  = 1;
  cst.l   = 0;
  cst.gpo = 1;

  // Initialize val
  memset(&val, 0, sizeof(val));
  val.nx  = 1;
  val.wx  = 3;
  val.tx  = 3;
  val.vrx = 3;
  val.vix = 3;
  val.tz  = 3;
  val.op  = 2;
  val.qz  = 1;
  val.nz  = 1;
  val.wz  = 3;
  val.sma = 3;
  val.bs  = 0;
  val.qs  = 2;
  val.l   = 10;

  fprintf(f, "# Reset\n");
  fprintf(f, "RS 10\n");
  fprintf(f, "# Clear interrupt flags\n");
  fprintf(f, "VR FFFB0 1 0 FF 0 0\n");
  fprintf(f, "# Set resets, chip enables and interrupt enables\n");
  fprintf(f, "VR %08" PRIX32 " 1 0 FF 0 0\n", CSS_IRQ_ADDR);
  fprintf(f, "# Set resets, chip enables and interrupt enables\n");
  css_set_prst(&cr, 1);  // De-assert PSS reset
  css_set_drst(&cr, 1);  // De-assert DMA reset
  css_set_pce(&cr, 1);   // Chip-enable PSS
  css_set_dce(&cr, 1);   // Chip-enable DMA
  css_set_p2hie(&cr, 1); // Enable PSS interrupts to host
  css_set_d2hie(&cr, 1); // Enable DMA interrupts to host
  css_set_e2hie(&cr, 1); // Enable extended interrupts to host
  css_set_hie(&cr, 1);   // Enable interrupts to host
  css_set_lhirq(&cr, 1); // Level-triggered PSS and DMA interrupts to host
  css_set_leirq(&cr, 1); // Level-triggered extended interrupts
  fprintf(f, "VW %08" PRIX32 " 1 0 FF %016" PRIX64 "\n", CSS_CTRL_ADDR, cr.ctrl);
  for(i = 0; i < n; i++) {
    fprintf(f, "#################\n");
    fprintf(f, "# Test #%d\n", i + 1);
    fprintf(f, "#################\n");
    mktests(f, &ctx, cst, val);
    fprintf(f, "# End of test #%d\n\n", i + 1);
  }
  fep_ctx_cleanup(&ctx);
  fclose(f);
  return 0;
}

const char *version_string = "\
mkcmdfile 0.1\n\
\n\
Copyright (C) 2010 Institut Telecom\n\
\n\
This software is governed by the CeCILL license under French law and\n\
abiding by the rules of distribution of free software.  You can  use,\n\
modify and/ or redistribute the software under the terms of the CeCILL\n\
license as circulated by CEA, CNRS and INRIA at the following URL\n\
http://www.cecill.info\n\
\n\
Written by Renaud Pacalet <renaud.pacalet@telecom-paristech.fr>.\n\
";

const char *usage_string = "\
mkcmdfile generate commands for VHDL functional simulation\n\
of a DSP unit.\n\
\n\
USAGE: mkcmdfile [OPTION]... [OUTPUTFILE]\n\
\n\
Mandatory arguments to long options are mandatory for short options too.\n\
If OUTPUTFILE is not specified stdout is used. On success the exit status is 0.\n\
\n\
OPTIONS:\n\
  -t, --tests=NUM       number of tests to generate (default=1)\n\
  -o, --operations=MSK  7-bits mask of allowed operations: FT,CWL,MOV,CWS,CWM,CWP,CWA (default=127)\n\
  -s, --seed=NUM        seed of random generator (default=1)\n\
  -v, --version\n\
        Output version information and exit\n\
  -h, --help\n\
        Display this help and exit\n\
\n\
EXAMPLES:\n\
\n\
  To generate a command file named ft.cmd driving the simulation for 10 Fourier\n\
  transforms:\n\
\n\
    mkcmdfile --tests=10 --operations=64 ft.cmd\n\
\n\
  To generate a command file named foo.cmd driving the simulation of the full FEP for 100 random\n\
  operations but CWL and MOV:\n\
\n\
    mkcmdfile --tests=100 --operations=79 foo.cmd\n\
\n\
Please report bugs to <renaud.pacalet@telecom-paristech.fr>.\n\
";

void
parse_options (int argc, char **argv) {
  int o, option_index, tmp;
  struct option long_options[] = {
    {"tests", required_argument, NULL, 't'},
    {"operations", required_argument, NULL, 'o'},
    {"seed", required_argument, NULL, 's'},
    {"version", no_argument, NULL, 'v'},
    {"help", no_argument, NULL, 'h'},
    {0, 0, 0, 0}
  };

  option_index = 0;
  n = 1;
  operations = 127;
  seed = 1;
  while (1) {
    o =
      getopt_long (argc, argv, "t:o:s:vh",
      	     long_options, &option_index);
    if (o == -1) {
      break;
    }
    switch (o) {
      case 't':
        n = atoi(optarg);
        if(n < 1) {
          fprintf (stderr, "*** Invalid number of tests: %d\n", n);
          ERROR(-1, "%s", usage_string);
        }
        break;
      case 'o':
        operations = atoi(optarg);
        if(operations < 1 || operations > 127) {
          fprintf (stderr, "*** Invalid operations mask: %d\n", operations);
          ERROR(-1, "%s", usage_string);
        }
        break;
      case 's':
        tmp = atoi(optarg);
        if(tmp < 0) {
          fprintf (stderr, "*** Invalid random seed: %d\n", tmp);
          ERROR(-1, "%s", usage_string);
        }
        seed = tmp;
        break;
      case 'v':
        fprintf (stderr, "%s", version_string);
        exit (0);
        break;
      default:
        fprintf (stderr, "%s", usage_string);
        exit (0);
        break;
    }
  }
  f = stdout;
  if (optind < argc) {
    f = XFOPEN(argv[optind], "w");
    optind += 1;
  }
  if (optind < argc) {
    ERROR(-1, "%s", usage_string);
  }
}
