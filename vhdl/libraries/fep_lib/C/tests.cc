/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <fcntl.h>
#include <unistd.h>

#include <embb/fep.h>
#include <embb/memory.h>
#include <assert.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include "utils.h"
#include "tests.h"

#define INDATA_SIZE 32

#define DEBUG(...) fprintf(stderr, __VA_ARGS__);

#define ASSERT_EQ(a, b)							\
  do {									\
    typeof(a) a_ = a;							\
    typeof(a) b_ = b;							\
    if (a_ != b_) {							\
      DEBUG("error:%u:%s:%s %s equals to 0x%X, expected 0x%X\n", __LINE__, __FILE__, __func__, #a, a_, b_); \
      abort();								\
    }									\
  } while (0)

// Human-readable print of parameters
void print_params(FILE *f, FEP_CONTEXT *ctx) {
  fprintf(f, "# OP=%lld, I=%lld, LS=%lld, LI=%lld, LL=%lld, R=%lld, SMA=%lld, BS=0x%04llX, QS=%lld, L=%lld\n", fep_get_op(ctx), fep_get_i(ctx), fep_get_ls(ctx),
      fep_get_li(ctx), fep_get_ll(ctx), fep_get_r(ctx), fep_get_sma(ctx), fep_get_bs(ctx), fep_get_qs(ctx), fep_get_l(ctx));
  fprintf(f, "# BX=0x%04llX, QX=%lld, MX=%lld, NX=%lld, SX=%lld, PX=%lld, WX=%lld, TX=%lld, VRX=%lld, VIX=%lld, DX=%lld\n", fep_get_bx(ctx), fep_get_qx(ctx), fep_get_mx(ctx),
      fep_get_nx(ctx), fep_get_sx(ctx), fep_get_px(ctx), fep_get_wx(ctx), fep_get_tx(ctx), fep_get_vrx(ctx), fep_get_vix(ctx), fep_get_dx(ctx));
  fprintf(f, "# BY=0x%04llX, QY=%lld, MY=%lld, NY=%lld, SY=%lld, PY=%lld, WY=%lld, TY=%lld, VRY=%lld, VIY=%lld, DY=%lld\n", fep_get_by(ctx), fep_get_qy(ctx), fep_get_my(ctx),
      fep_get_ny(ctx), fep_get_sy(ctx), fep_get_py(ctx), fep_get_wy(ctx), fep_get_ty(ctx), fep_get_vry(ctx), fep_get_viy(ctx), fep_get_dy(ctx));
  fprintf(f, "# BZ=0x%04llX, QZ=%lld, MZ=%lld, NZ=%lld, SZ=%lld, WZ=%lld, TZ=%lld, RI=%lld, ML=%lld, GPO=0x%016llX\n", fep_get_bz(ctx), fep_get_qz(ctx), fep_get_mz(ctx),
      fep_get_nz(ctx), fep_get_sz(ctx), fep_get_wz(ctx), fep_get_tz(ctx), fep_get_ri(ctx), fep_get_ml(ctx), fep_get_gpo(ctx));
}

// Print execute command
void print_ex(FILE *f, FEP_CONTEXT *ctx) {
  fprintf(f, "EX 0 0 0 0f\n%016llX\n%016llX\n%016llX\n%016llX\n", ctx->devmem->cmd._r3, ctx->devmem->cmd._r2, ctx->devmem->cmd._r1, ctx->devmem->cmd._r0);
}

// Randomly picks an op value according the mask where set bits indicate possible values.
int
rand_op(int mask) {
  int i, j, ops[7];

  for(i = 0, j = 0; i < 7; i++) {
    if(((mask >> i) & 0x1) == 0x1) {
      ops[j] = i;
      j += 1;
    }
  }
  return ops[do_rand() % j];
}

// randomly generates context ctx. cst and val are two fep_params structures expressing constraints on the random generation and must be initialized prior
// calling randctx. In cst field values are either 0 or 1; a 1 indicates that the corresponding field value must be taken as is from val; a 0 indicates that the
// field must be randomly picked between 0 (included) and the same filed's value in val (exluded). When randomly picked, each field is guaranteed to have a
// valid value with respect to the other ctx fields. Exception: when the op field is randomly picked (cst.op=0), val.op is a mask which set bits indicate
// possible op values: val.op=0x01 (bit 0 set) indicates that only op=0 is possible, while val.op=0x15 (bits 4, 2 and 0 set) indicates that op can be any of 0,
// 2 or 4.
void randctx(FEP_CONTEXT *ctx, fep_params cst, fep_params val)
{
  uint64_t tmp, tmp1;
  int op;

  if(cst.op == 1)
    op = val.op;
  else
    op = rand_op(val.op);
  fep_set_op(ctx, op);

  if(cst.i == 1)
    fep_set_i(ctx, val.i);
  else
    fep_set_i(ctx, do_rand() % val.i);

  if(cst.ls == 1)
    fep_set_ls(ctx, val.ls);
  else
    fep_set_ls(ctx, do_rand() % val.ls);

  if(cst.li == 1)
    fep_set_li(ctx, val.li);
  else
    fep_set_li(ctx, do_rand() % val.li);

  if(cst.ll == 1)
    fep_set_ll(ctx, val.ll);
  else if(op == FEP_OP_CWL) {
    do {
      tmp = do_rand() % val.ll;
    } while(tmp < 1 || tmp > 14);
    fep_set_ll(ctx, tmp);
  }
  else
    fep_set_ll(ctx, do_rand() % val.ll);

  if(cst.sma == 1)
    fep_set_sma(ctx, val.sma);
  else if(op != FEP_OP_FT) {
    do {
      tmp = do_rand() % val.sma;
    } while(tmp == 0);
    fep_set_sma(ctx, tmp);
  }
  else
    fep_set_sma(ctx, do_rand() % val.sma);

  if(cst.bs == 1)
    fep_set_bs(ctx, val.bs);
  else
    fep_set_bs(ctx, do_rand() % val.bs);

  if(cst.qs == 1)
    fep_set_qs(ctx, val.qs);
  else
    fep_set_qs(ctx, do_rand() % val.qs);

  if(cst.l == 1)
    fep_set_l(ctx, val.l);
  else if(op == FEP_OP_FT)
    do {
      fep_set_l(ctx, 1 << (do_rand() % 10 + 3));
    } while(fep_get_l(ctx) > val.l);
  else {
    do {
      tmp = do_rand() % val.l;
    } while(tmp < 1);
    fep_set_l(ctx, tmp);
  }

  if(cst.tx == 1)
    fep_set_tx(ctx, val.tx);
  else if(op == FEP_OP_CWL) {
    do {
      tmp = do_rand() % val.tx;
    } while(tmp > 1);
    fep_set_tx(ctx, tmp);
  }
  else
    fep_set_tx(ctx, do_rand() % val.tx);

  if(cst.bx == 1)
    fep_set_bx(ctx, val.bx);
  else if(op == FEP_OP_FT)
    do {
      fep_set_bx(ctx, (do_rand() % (1 << 9)) * 8);
    } while(fep_get_bx(ctx) > val.bx);
  else {
    tmp1 = (1 << 14) / BW(fep_get_tx(ctx));
    do {
      tmp = do_rand() % val.bx;
    } while(tmp >= tmp1);
    fep_set_bx(ctx, tmp);
  }

  if(cst.qx == 1)
    fep_set_qx(ctx, val.qx);
  else
    fep_set_qx(ctx, do_rand() % val.qx);

  if(cst.mx == 1)
    fep_set_mx(ctx, val.mx);
  else if(op != FEP_OP_FT) {
    do {
      tmp = do_rand() % val.mx;
    } while(tmp == 1);
    fep_set_mx(ctx, tmp);
  }
  else
    fep_set_mx(ctx, do_rand() % val.mx);

  if(cst.nx == 1)
    fep_set_nx(ctx, val.nx);
  else
    fep_set_nx(ctx, do_rand() % val.nx);

  if(cst.sx == 1)
    fep_set_sx(ctx, val.sx);
  else
    fep_set_sx(ctx, do_rand() % val.sx);

  if(cst.px == 1)
    fep_set_px(ctx, val.px);
  else
    fep_set_px(ctx, do_rand() % val.px);

  if(cst.wx == 1)
    fep_set_wx(ctx, val.wx);
  else
    fep_set_wx(ctx, do_rand() % val.wx);

  if(cst.vrx == 1)
    fep_set_vrx(ctx, val.vrx);
  else
    fep_set_vrx(ctx, do_rand() % val.vrx);

  if(cst.vix == 1)
    fep_set_vix(ctx, val.vix);
  else
    fep_set_vix(ctx, do_rand() % val.vix);

  if(cst.dx == 1)
    fep_set_dx(ctx, val.dx);
  else
    fep_set_dx(ctx, do_rand() % val.dx);

  if(cst.ty == 1)
    fep_set_ty(ctx, val.ty);
  else if(op == FEP_OP_CWL) {
    do {
      tmp = do_rand() % val.ty;
    } while(tmp > 1);
    fep_set_ty(ctx, tmp);
  }
  else
    fep_set_ty(ctx, do_rand() % val.ty);

  if(cst.by == 1)
    fep_set_by(ctx, val.by);
  else if((op == FEP_OP_CWA) || (op == FEP_OP_CWP) || (op == FEP_OP_CWL)) {
    tmp1 = (1 << 14) / BW(fep_get_ty(ctx));
    do {
      tmp = do_rand() % val.by;
    } while(tmp >= tmp1);
    fep_set_by(ctx, tmp);
  }
  else
    fep_set_by(ctx, do_rand() % val.by);

  if(cst.qy == 1)
    fep_set_qy(ctx, val.qy);
  else if((op == FEP_OP_CWA) || (op == FEP_OP_CWP) || (op == FEP_OP_CWL)) {
    do {
      tmp = do_rand() % val.qy;
    } while(tmp == fep_get_qx(ctx));
    fep_set_qy(ctx, tmp);
  }
  else
    fep_set_qy(ctx, do_rand() % val.qy);

  if(cst.my == 1)
    fep_set_my(ctx, val.my);
  else if((op == FEP_OP_CWA) || (op == FEP_OP_CWP)) {
    do {
      tmp = do_rand() % val.my;
    } while(tmp == 1);
    fep_set_my(ctx, tmp);
  }
  else
    fep_set_my(ctx, do_rand() % val.my);

  if(cst.ny == 1)
    fep_set_ny(ctx, val.ny);
  else
    fep_set_ny(ctx, do_rand() % val.ny);

  if(cst.sy == 1)
    fep_set_sy(ctx, val.sy);
  else
    fep_set_sy(ctx, do_rand() % val.sy);

  if(cst.py == 1)
    fep_set_py(ctx, val.py);
  else
    fep_set_py(ctx, do_rand() % val.py);

  if(cst.wy == 1)
    fep_set_wy(ctx, val.wy);
  else
    fep_set_wy(ctx, do_rand() % val.wy);

  if(cst.vry == 1)
    fep_set_vry(ctx, val.vry);
  else
    fep_set_vry(ctx, do_rand() % val.vry);

  if(cst.viy == 1)
    fep_set_viy(ctx, val.viy);
  else
    fep_set_viy(ctx, do_rand() % val.viy);

  if(cst.dy == 1)
    fep_set_dy(ctx, val.dy);
  else
    fep_set_dy(ctx, do_rand() % val.dy);

  if(cst.tz == 1)
    fep_set_tz(ctx, val.tz);
  else
    fep_set_tz(ctx, do_rand() % val.tz);

  if(cst.bz == 1)
    fep_set_bz(ctx, val.bz);
  else if(op == FEP_OP_FT)
    do {
      fep_set_bz(ctx, (do_rand() % (1 << 9)) * 8);
    } while(fep_get_bz(ctx) > val.bz);
  else if(fep_get_sma(ctx) == 2)
    fep_set_bz(ctx, do_rand() % val.bz);
  else {
    tmp1 = (1 << 14) / BW(fep_get_tz(ctx));
    do {
      tmp = do_rand() % val.bz;
    } while(tmp >= tmp1);
    fep_set_bz(ctx, tmp);
  }

  if(cst.qz == 1)
    fep_set_qz(ctx, val.qz);
  else if((fep_get_sma(ctx) == 2) || (op == FEP_OP_FT))
    fep_set_qz(ctx, do_rand() % val.qz);
  else if((op == FEP_OP_CWA) || (op == FEP_OP_CWP) || (op == FEP_OP_CWL)) {
    do {
      tmp = do_rand() % val.qz;
    } while(tmp == fep_get_qy(ctx) || tmp == fep_get_qx(ctx));
    fep_set_qz(ctx, tmp);
  }
  else {
    do {
      tmp = do_rand() % val.qz;
    } while(tmp == fep_get_qx(ctx));
    fep_set_qz(ctx, tmp);
  }

  if(cst.mz == 1)
    fep_set_mz(ctx, val.mz);
  else if((fep_get_sma(ctx) == 2) || (op == FEP_OP_FT))
    fep_set_mz(ctx, do_rand() % val.mz);
  else {
    do {
      tmp = do_rand() % val.mz;
    } while(tmp == 1);
    fep_set_mz(ctx, tmp);
  }

  if(cst.nz == 1)
    fep_set_nz(ctx, val.nz);
  else
    fep_set_nz(ctx, do_rand() % val.nz);

  if(cst.sz == 1)
    fep_set_sz(ctx, val.sz);
  else
    fep_set_sz(ctx, do_rand() % val.sz);

  if(cst.wz == 1)
    fep_set_wz(ctx, val.wz);
  else
    fep_set_wz(ctx, do_rand() % val.wz);

  if(cst.ri == 1)
    fep_set_ri(ctx, val.ri);
  else
    fep_set_ri(ctx, do_rand() % val.ri);

  if(cst.ml == 1)
    fep_set_ml(ctx, val.ml);
  else
    fep_set_ml(ctx, do_rand() % val.ml);

  if(cst.r == 1)
    fep_set_r(ctx, val.r);
  else
    fep_set_r(ctx, do_rand() % val.r);

  if(cst.gpo == 1)
    fep_set_gpo(ctx, val.gpo);
  else
    fep_set_gpo(ctx, do_rand64() % val.gpo);
}


// Run one random test
void mktests(FILE *f, FEP_CONTEXT *ctx, fep_params cst, fep_params val)
{
  size_t l, sx, sy, sz, ss; // Vector length, section byte-lengths
  uint16_t bx, by, bz, bs;  // Base addresses
  int i, op;

  randctx(ctx, cst, val);

  print_params(f, ctx);

  l = fep_get_l(ctx);
  op = fep_get_op(ctx);

  bx = 0;
  by = 0;
  bz = 0;
  bs = 0;

  sx = 1 << (fep_get_wx(ctx) + 11);
  sy = 1 << (fep_get_wy(ctx) + 11);
  sz = 1 << (fep_get_wz(ctx) + 11);
  ss = 24;

  uint8_t data[FEP_QSIZE * 4];
  uint8_t *datax;
  uint8_t *datay;
  uint8_t *dataz;
  uint8_t *datas;
  uint8_t datat0[24];
  uint8_t datat1[24];

  int tx, ty, tz, bwx, bwy, bwz;

  for (i = 0; i < FEP_QSIZE * 4; i++)
    data[i] = do_rand() % 256;
  tx = fep_get_tx(ctx);
  if(op == FEP_OP_FT)
    tx = 3;
  bwx = BW(tx);
  bx = fep_get_bx(ctx) * bwx;
  bx -= bx % sx;
  datax = data + FEP_QSIZE * fep_get_qx(ctx) + bx;
  embb_mem2ip((EMBB_CONTEXT*)ctx, FEP_QSIZE * fep_get_qx(ctx) + bx, datax, sx);
  fprintf(f, "DW %llX %d 0 FF\n", FEP_QSIZE * fep_get_qx(ctx) + bx, sx / 8);
  /*
  fprintf(f, "# DATAX=");
  for(i = 0; i < 8; i++)
    fprintf(f, "%02X", datax[i]);
  fprintf(f, "...\n");
  */
  dump_samples(f, datax, sx / bwx, tx, 0, 1, 8);
  fprintf(f, "WD 10000\n");

  if((op == FEP_OP_CWA) || (op == FEP_OP_CWP) || (op == FEP_OP_CWL)) {
    ty = fep_get_ty(ctx);
    bwy = BW(ty);
    by = fep_get_by(ctx) * bwy;
    by -= by % sy;
    datay = data + FEP_QSIZE * fep_get_qy(ctx) + by;
    embb_mem2ip((EMBB_CONTEXT*)ctx, FEP_QSIZE * fep_get_qy(ctx) + by, datay, sy);
    fprintf(f, "DW %llX %d 0 FF\n", FEP_QSIZE * fep_get_qy(ctx) + by, sy / 8);
    /*
    fprintf(f, "# DATAY=");
    for(i = 0; i < 8; i++)
      fprintf(f, "%02X", datay[i]);
    fprintf(f, "...\n");
    */
    dump_samples(f, datay, sy / bwy, ty, 0, 1, 8);
    fprintf(f, "WD 10000\n");
  }
  tz = fep_get_tz(ctx);
  if(op == FEP_OP_FT)
    tz = 3;
  bwz = BW(tz);
  if((fep_get_sma(ctx) != 2) || (op == FEP_OP_FT)) {
    bz = fep_get_bz(ctx) * bwz;
    bz -= bz % sz;
    dataz = data + FEP_QSIZE * fep_get_qz(ctx) + bz;
    embb_mem2ip((EMBB_CONTEXT*)ctx, FEP_QSIZE * fep_get_qz(ctx) + bz, dataz, sz);
    fprintf(f, "DW %llX %d 0 FF\n", FEP_QSIZE * fep_get_qz(ctx) + bz, sz / 8);
    dump_samples(f, dataz, sz / bwz, tz, 0, 1, 8);
    fprintf(f, "WD 10000\n");
  }
  if((fep_get_sma(ctx) != 1) && (op != FEP_OP_FT)) {
    bs = fep_get_bs(ctx) * 32;
    datas = data + FEP_QSIZE * fep_get_qs(ctx) + bs;
  }

  fep_start(ctx);

  print_ex(f, ctx);
  fprintf(f, "WE 10000\n");

  if((fep_get_sma(ctx) != 1) && (op != FEP_OP_FT)) {
    embb_ip2mem(datas, (EMBB_CONTEXT*)ctx, FEP_QSIZE * fep_get_qs(ctx) + bs, ss);
    fprintf(f, "DR %llX %d 0 FF 1 0 FF\n", FEP_QSIZE * fep_get_qs(ctx) + bs, ss / 8);
    dump_samples(f, datas, 1, 4, 0, 1, 8);
    dump_samples(f, datas + 8, 4, 3, 0, 1, 8);
/* 
    fprintf(f, "#");
    for(i = 0; i < 8; i++)
      fprintf(f, " %02X", datas[i]);
    fprintf(f, "\n");
    fprintf(f, "#");
    for(i = 0; i < 8; i++)
      fprintf(f, " %02X", datas[i + 16]);
    fprintf(f, "\n");
*/
    fprintf(f, "WD 10000\n");
  }
  if((fep_get_sma(ctx) != 2) || (op == FEP_OP_FT)) {
    embb_ip2mem(dataz, (EMBB_CONTEXT*)ctx, FEP_QSIZE * fep_get_qz(ctx) + bz, sz);
    fprintf(f, "DR %llX %d 0 FF 1 0 FF\n", FEP_QSIZE * fep_get_qz(ctx) + bz, sz / 8);
    if((fep_get_sma(ctx) == 3) && (op != FEP_OP_FT) && (fep_get_qz(ctx) == fep_get_qs(ctx))) {
      for(i = 0; i < 4; i++)
      //SUM : CPX64
      {
        datat0[i] = datas[3 - i];
        datat0[4 + i] = datas[7 - i];
      }
      for(i = 0; i < 8; i++)
      //MAX-MIN-ARGMAX-ARGMIN : CPX32
      {
        datat0[8 + 2 * i] = datas[8 + 2 * i + 1];
        datat0[8 + 2 * i + 1] = datas[8 + 2 * i];
      }
      switch(fep_get_tz(ctx)) {
        case 0: //INT8
                for(i = 0; i < 24; i++)
                  datat1[i] = datat0[i];
                break;
        case 1: 
                //INT16
                for(i = 0; i < 12; i++)
                {
                  datat1[2 * i] = datat0[2 * i + 1];
                  datat1[2 * i + 1] = datat0[2 * i];
                }
                break;
        case 2: 
                //CPX16
                for(i = 0; i < 24; i++)
                  datat1[i] = datat0[i];
                break;

        case 3: 
                //CPX32
                for(i = 0; i < 12; i++)
                {
                  datat1[2 * i] = datat0[2 * i + 1];
                  datat1[2 * i + 1] = datat0[2 * i];
                }
                break;
      }
      for(i = 0; i < 24; i++)
        datas[i] = datat1[i];
    }
    /*
    fprintf(f, "# DATAZ=");
    for(i = 0; i < 8; i++)
      fprintf(f, "%02X", dataz[i]);
    fprintf(f, "...\n");
    */
    dump_samples(f, dataz, sz / bwz, tz, 0, 1, 8);
    fprintf(f, "WD 10000\n");
  }
}
