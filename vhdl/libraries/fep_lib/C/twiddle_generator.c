/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

/* 
This file is used to generate Twiddle_factors in Q1.15 format, and the output is stored in another file called f_twiddle.txt.....
The methodology is to multiply each of cosine and sine values with (2^15 - 1), and then just taking the integer part 
(Its almost equivalent to shifting left 15 times). As integers are represnted in 32-bits; so 16-MSBs are masked. 
Data is written back in HEX format, with 16-MSBs represent the real part of the twiddle factors, and 16-LSBs imaginary part of TF.  

Dont Forget, Our algorithm will use only N/8 twiddle factors to compute N-point FFT ... 
As N-max is 4096, therefore 512 TFs are generated here; also TF[0] is not generated, the main program shld handle itself ( TF[0]= (1,0) ) 
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#define PI 3.141592653589793238462643383279 
// Source for PI	http://en.wikipedia.org/wiki/Pi

int main(void)
{
	int i =0; 
	int N = 4096;
	int N_by_8 = 512;
	int int_real = 0, int_imag = 0; 
	int two_power_15_minus_1 = 32767;
	//int two_power_15_minus_1 = 32768;
	FILE *f_twiddle; 
	if((f_twiddle=fopen("f_twiddle.txt", "w")) == NULL)
    	{
             printf("Error: cannot open the twiddle_output file\n");
             exit(1); 
    	} 

	
		//for (i=1;i<=N;i++)
		for (i=1;i<=N_by_8;i++)
		{
			int_real =  (int) two_power_15_minus_1 *     cos(2.0*PI*i/N);
			int_imag =  (int) two_power_15_minus_1 * -1* sin(2.0*PI*i/N);
			int_real =   int_real &  (0x0000ffff) ;
			int_imag =   int_imag &  (0x0000ffff) ;
			//fprintf(f_twiddle, "%d, %d \n ", int_real, int_imag);
			fprintf(f_twiddle, "%04x%04x \n", int_real, int_imag);
//			j++;
//			if (j==8)
//			{
//				fprintf(f_twiddle, "\n");		
//				j=0;
//			}
		}	
	fclose(f_twiddle);
	//printf("Pi = %5.16f \n", PI );
return 0; 
}
