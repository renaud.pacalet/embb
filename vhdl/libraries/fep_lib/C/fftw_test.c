/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

int
main (int argc, char **argv)
{
  int N = 2048;
  int i;
  double tmp, sq;
  fftw_complex *v1, *v2, *v3, *v4, *v5, *v6, *v7;
  fftw_plan p;

  srandom (0);
  sq = sqrt (N);
  v1 = (fftw_complex *) fftw_malloc (sizeof (fftw_complex) * N);
  v2 = (fftw_complex *) fftw_malloc (sizeof (fftw_complex) * N);
  v3 = (fftw_complex *) fftw_malloc (sizeof (fftw_complex) * N);
  v4 = (fftw_complex *) fftw_malloc (sizeof (fftw_complex) * N);
  v5 = (fftw_complex *) fftw_malloc (sizeof (fftw_complex) * N);
  v6 = (fftw_complex *) fftw_malloc (sizeof (fftw_complex) * N);
  v7 = (fftw_complex *) fftw_malloc (sizeof (fftw_complex) * N);
  p = fftw_plan_dft_1d (N, v1, v2, FFTW_FORWARD, FFTW_ESTIMATE);
  for (i = 0; i < N; i++)
    {
      v1[i][0] = (double) (random ());
      v1[i][1] = (double) (random ());
    }
  fftw_execute (p);
  for (i = 0; i < N; i++)
    {
      v5[i][0] = v2[i][0] / sq;
      v5[i][1] = v2[i][1] / sq;
    }
  p = fftw_plan_dft_1d (N, v2, v3, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute (p);
  p = fftw_plan_dft_1d (N, v5, v6, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute (p);
  for (i = 0; i < N; i++)
    {
      v6[i][0] /= sq;
      v6[i][1] /= sq;
    }
  p = fftw_plan_dft_1d (N, v2, v4, FFTW_FORWARD, FFTW_ESTIMATE);
  for (i = 0; i < N; i++)
    {
      v2[i][1] = -v2[i][1];
    }
  fftw_execute (p);
  p = fftw_plan_dft_1d (N, v5, v7, FFTW_FORWARD, FFTW_ESTIMATE);
  for (i = 0; i < N; i++)
    {
      v5[i][1] = -v5[i][1];
    }
  fftw_execute (p);
  for (i = 0; i < N; i++)
    {
      v7[i][0] /= sq;
      v7[i][1] /= sq;
    }
  for (i = 0; i < N; i++)
    {
      tmp =
        sqrt ((v3[i][0] * v3[i][0] +
               v3[i][1] * v3[i][1]) / (v1[i][0] * v1[i][0] +
                                       v1[i][1] * v1[i][1]));
      if (fabs (tmp - N) > 1e-6)
        {
          printf ("wrong v3/v1 scale factor: %f\n", tmp);
        }
      v3[i][0] -= v4[i][0];
      v3[i][1] += v4[i][1];
      tmp = sqrt (v3[i][0] * v3[i][0] + v3[i][1] * v3[i][1]);
      if (tmp > 1e-2)
        {
          printf ("too large v3/v4 difference: %f\n", tmp);
        }
      tmp =
        sqrt ((v6[i][0] * v6[i][0] +
               v6[i][1] * v6[i][1]) / (v1[i][0] * v1[i][0] +
                                       v1[i][1] * v1[i][1]));
      if (fabs (tmp - 1) > 1e-6)
        {
          printf ("wrong v6/v1 scale factor: %f\n", tmp);
        }
      v6[i][0] -= v7[i][0];
      v6[i][1] += v7[i][1];
      tmp = sqrt (v6[i][0] * v6[i][0] + v6[i][1] * v6[i][1]);
      if (tmp > 1e-2)
        {
          printf ("too large v6/v7 difference: %f\n", tmp);
        }
      v1[i][0] -= v7[i][0];
      v1[i][1] += v7[i][1];
      tmp = sqrt (v1[i][0] * v1[i][0] + v1[i][1] * v1[i][1]);
      if (tmp > 1e-2)
        {
          printf ("too large v1/v7 difference: %f\n", tmp);
        }
    }
  fftw_destroy_plan (p);
  fftw_free (v1);
  fftw_free (v2);
  fftw_free (v3);
  fftw_free (v4);
  fftw_free (v5);
  fftw_free (v6);
  fftw_free (v7);
  return 0;
}
