/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <stdio.h>

int bank_number(int address) {
  int i, e, o, p;

  address &= 0x7fff;
  e = 0;
  o = 0;
  p = address & 0x1;
  address >>= 1;
  for(i = 0; i < 6; i++) {
    e += (address & 0x1);
    address >>= 1;
    o += (address & 0x1);
    address >>= 1;
  }
  return ((2 * o + e) % 4) * 2 + p;
}

int check_addresses(int a[8]) {
  int i, j;

  for(i = 0; i < 7; i++) {
    for(j = i + 1; j < 8; j++) {
      if(a[i] == a[j]) {
        return 0;
      }
    }
  }
  for(i = 0; i < 8; i++) {
    if(a[i] < 0 || a[i] > 7) {
      return 0;
    }
  }
  return 1;
}

int main(int argc, char **argv) {
  int n, i, k, s, a[8];
  int b[8][4096], tmp1, tmp2;

  for(n = 8; n <= 4096; n <<= 1) {
    for(i = 0; i < 4096; i++) {
      for(k = 0; k < 8; k++) {
        b[k][i] = -1;
      }
    }
    for(s = 0; s <= 32768 - n; s += n) {
      for(i = 0; i < n / 8; i++) {
        for(k = 0; k < 4; k++) {
          tmp1 = s + 2 * i + k * n / 4;
          a[2 * k] = bank_number(tmp1);
          tmp2 = tmp1 % 4096;
          if(b[a[2 * k]][tmp2] != -1) {
            printf("n=%d, s=%d, i=%d, k=%d\n", n, s, i, k);
            printf("b[a[2*%d]][(2*%d+%d*%d/4)%%4096]=b[%d][%d]=%d\n", k, i, k, n, a[2 * k],
              tmp2, b[a[2 * k]][tmp2]);
            return -1;
          }
          b[a[2 *k]][tmp2] = tmp1;
          tmp1 = tmp1 + 1;
          a[2 * k + 1] = bank_number(tmp1);
          tmp2 = tmp1 % 4096;
          if(b[a[2 * k + 1]][tmp2] != -1) {
            printf("b[a[2*%d+1]][(2*%d+%d*%d/4+1)%%4096]=b[%d][%d]=%d\n", k, i, k, n, a[2 * k + 1],
              tmp2, b[a[2 * k + 1]][tmp2]);
            return -1;
          }
          b[a[2 * k + 1]][tmp2] = tmp1;
        }
        if(!check_addresses(a)) {
          printf("i=%d, n=%d, s=%d => {%d, %d, %d, %d, %d, %d, %d, %d}\n",
            i, n, s, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]);
          return -1;
        }
      }
    }
  }
  return 0;
}
