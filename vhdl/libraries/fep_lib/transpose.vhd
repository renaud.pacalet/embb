--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Addresses generator for Fourrier transform operations of FEP
--*
--*  Description :
--*   This module performs accumulation/transposition of input data.
--*   This module is used only for Fast Fourrier Transformation. It
--*   consists in two 2-dimension arrays accumulating input data
--*   and returning transposition of these matrixes. Several
--*   transposition modes are possile. The input signal 't' select the
--*   transformation mode. 't' format is
--*     -  bit 0 to 1 : transposition mode
--*     -  bit 2      : transpose only data to write in memory
--*     -  bit 3      : transpose only data read from memory
--*   The different transposition mode are :
--*   - "00" : bypass mode
--*   - "01" : only for 8-point DFT
--*   - "00" : only of power of 2 DFT
--*   - "00" : not used

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use ieee.numeric_std.all;
use global_lib.global.all;

use work.type_pkg.all;

entity transpose is
  port(
    clk  : in  std_ulogic;
    srstn: in  std_ulogic;
    ce:    in  std_ulogic;
    flush: in  std_ulogic;
    en   : in  std_ulogic;
    rnw  : in  std_ulogic;
    din  : in  cpx50_array(1 downto 0, 3 downto 0);
    dout : out cpx50_array(1 downto 0, 3 downto 0);
    t    : in  std_ulogic_vector(3 downto 0)
  );
end entity transpose;

architecture rtl of transpose is

  type matrix_type is array(0 to 3, 0 to 3) of cpx50;

  signal tq, td : std_ulogic_vector(3 downto 0);
  signal rq, rd : std_ulogic;
  signal mat  : matrix_type;
  signal cnt  : std_ulogic;
  signal rnc  : std_ulogic;

begin

  process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        cnt <= '0';
      elsif ce = '1' then
      -- rst cnt
        if flush = '1' then
          cnt <= '0';
        end if;
      -- shift
        if rnc = '1' then
          for r in 0 to 1 loop
            for c in 0 to 3 loop
              mat(r, c) <= mat(2 + r, c);
            end loop;
          end loop;
        else
          for c in 0 to 1 loop
            for r in 0 to 3 loop
              mat(r, c) <= mat(r, 2 + c);
            end loop;
          end loop;
        end if;
      -- din
        if en = '1' then
          cnt <= not cnt;
          for i in 0 to 3 loop
            if rnc = '1' then
              mat(2, i) <= din(0, i);
              mat(3, i) <= din(1, i);
            else
              mat(i, 2) <= din(0, i);
              mat(i, 3) <= din(1, i);
            end if;
          end loop;
        end if;
        if cnt = '1' and en = '0' then
          cnt <= '0';
        end if;
      end if;
    end if;
  end process;

  dout_p: process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        rnc <= '0';
        td  <= (others => '0');
        tq  <= (others => '0');
        rd  <= '0';
        rq  <= '0';
      elsif ce = '1' then
        td  <= t;
        tq  <= td;
        rd  <= rnw;
        rq  <= rd;
        for i in 0 to 3 loop
          if rnc = '1' then
            dout(0, i) <=  mat(0, i);
            dout(1, i) <=  mat(1, i);
          else
            dout(0, i) <=  mat(i, 0);
            dout(1, i) <=  mat(i, 1);
          end if;
        end loop;
        if (tq(3) = '1' and rq = '1') or (tq(2) = '1' and rq = '0') then
          if flush = '1' then
            rnc <= '0';
          elsif cnt = '1' then
            if t(1 downto 0) = "00" then -- transposition
              rnc <= not rnc;
            end if;
            if en = '0' then
              rnc <= '0';
            end if;
          end if;
          case tq(1 downto 0) is
            when "00" => -- l > 16
              for i in 0 to 3 loop
                if rnc = '1' then
                  dout(0, i) <=  mat(0, i);
                  dout(1, i) <=  mat(1, i);
                else
                  dout(0, i) <=  mat(i, 0);
                  dout(1, i) <=  mat(i, 1);
                end if;
              end loop;
            when "01" =>
              dout(0, 0)   <=  mat(0, 0);
              dout(0, 1)   <=  mat(2, 0);
              dout(0, 2)   <=  mat(0, 1);
              dout(0, 3)   <=  mat(2, 1);
              dout(1, 0)   <=  mat(1, 0);
              dout(1, 1)   <=  mat(3, 0);
              dout(1, 2)   <=  mat(1, 1);
              dout(1, 3)   <=  mat(3, 1);
            when "10" => -- l  = 8
              if rq = '1' then
                for i in 0 to 1 loop
                  dout(0, i)     <=  mat(i * 2, 0);
                  dout(0, i + 2) <=  mat(i * 2, 1);
                  dout(1, i)     <=  mat(i * 2 + 1, 0);
                  dout(1, i + 2) <=  mat(i * 2 + 1, 1);
                end loop;
              else
                if rnc = '1' then
                  for i in 0 to 1 loop
                    dout(0, i)     <=  mat(i, 0);
                    dout(0, i + 2) <=  mat(i, 1);
                    dout(1, i)     <=  mat(i, 2);
                    dout(1, i + 2) <=  mat(i, 3);
                  end loop;
                else
                  for i in 0 to 1 loop
                    dout(0, i)     <=  mat(0, i);
                    dout(0, i + 2) <=  mat(1, i);
                    dout(1, i)     <=  mat(2, i);
                    dout(1, i + 2) <=  mat(3, i);
                  end loop;
                end if;
              end if;
            when others =>
          end case;
        end if;
      end if;
    end if;
  end process dout_p;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
