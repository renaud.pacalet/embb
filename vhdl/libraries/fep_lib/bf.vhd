--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Butterfly of FEP processing unit
--*
--* This module is combinatorial. It implements the two possible FT butterflies:
--* 1) any stage of a 4^n points FT and any stage but the last of a 2.4^n points
--*    FT (output z4)
--* 2) the last stage of a 2.4^n points FT (output z2)
--* The following equations summarize BF's behaviour:
--*    z2(0) = u(0)+u(2)
--*    z2(1) = u(0)-u(2)
--*    z2(2) = u(1)+u(3)
--*    z2(3) = u(1)-u(3)
--*    z4(0) = u(0)+  u(1)+u(2)+  u(3)
--*    z4(1) = u(0)-j.u(1)+u(2)+j.u(3)
--*    z4(2) = u(0)-  u(1)+u(2)-  u(3)
--*    z4(3) = u(0)+j.u(1)-u(2)-j.u(3)
--*
--* This module has been validated by extensive simulations.
--*
--* Modify at your own risks. Do not use a modified version before running
--* the regression tests:
--* $ make bf_sim.sim

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.type_pkg.all;

entity bf is
  port(u:      in  cpx50_vector(0 to 3);
       z2, z4: out cpx50_vector(0 to 3));
end entity bf;

architecture rtl of bf is

  signal adda, addb, adds, suba, subb, subs: s25_vector(0 to 7);

begin

  gaddsub: for i in 0 to 7 generate
    adds(i) <= adda(i) + addb(i);
    subs(i) <= suba(i) - subb(i);
  end generate gaddsub;

  adda(0) <= u(0).r;
  addb(0) <= u(2).r;
  adda(1) <= u(0).i;
  addb(1) <= u(2).i;
  suba(0) <= u(0).r;
  subb(0) <= u(2).r;
  suba(1) <= u(0).i;
  subb(1) <= u(2).i;
  adda(2) <= u(1).r;
  addb(2) <= u(3).r;
  adda(3) <= u(1).i;
  addb(3) <= u(3).i;
  suba(2) <= u(1).r;
  subb(2) <= u(3).r;
  suba(3) <= u(1).i;
  subb(3) <= u(3).i;
  adda(4) <= adds(0);
  addb(4) <= adds(2);
  adda(5) <= adds(1);
  addb(5) <= adds(3);
  adda(6) <= subs(0);
  addb(6) <= subs(3);
  suba(4) <= subs(1);
  subb(4) <= subs(2);
  suba(5) <= adds(0);
  subb(5) <= adds(2);
  suba(6) <= adds(1);
  subb(6) <= adds(3);
  suba(7) <= subs(0);
  subb(7) <= subs(3);
  adda(7) <= subs(1);
  addb(7) <= subs(2);

  z2(0).r <= adds(0);
  z2(0).i <= adds(1);
  z2(1).r <= subs(0);
  z2(1).i <= subs(1);
  z2(2).r <= adds(2);
  z2(2).i <= adds(3);
  z2(3).r <= subs(2);
  z2(3).i <= subs(3);
  z4(0).r <= adds(4);
  z4(0).i <= adds(5);
  z4(1).r <= adds(6);
  z4(1).i <= subs(4);
  z4(2).r <= subs(5);
  z4(2).i <= subs(6);
  z4(3).r <= subs(7);
  z4(3).i <= adds(7);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
