--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for whole DSP unit

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;

use work.fep_pkg.all;
use work.bitfield.all;

entity fep_sim is
  generic(cmdfile: string := "fep.cmd";
          n0:      positive := 2;
          n1:      positive := 3;
          debug:   boolean := false;    --* Print debug information
          verbose: boolean := false);   --* Print more debug information
end entity fep_sim;

architecture sim of fep_sim is

  signal clk:      std_ulogic;
  signal srstn:    std_ulogic;
  signal ce:       std_ulogic;
  signal hirq:     std_ulogic;
  signal uirq:     std_ulogic;
  signal pirq:     std_ulogic;
  signal dirq:     std_ulogic;
  signal hst2tvci: vci_i2t_type;
  signal tvci2hst: vci_t2i_type;
  signal hst2ivci: vci_t2i_type;
  signal ivci2hst: vci_i2t_type;

begin

  i_fep: entity work.fep(rtl)
  generic map(
    debug     => debug,
    verbose   => verbose,
    n0        => n0,
    n1        => n1)
  port map(clk      => clk,
           srstn    => srstn,
           ce       => ce,
           hirq     => hirq,
           uirq     => uirq,
           pirq     => pirq,
           dirq     => dirq,
           tvci_in  => hst2tvci,
           tvci_out => tvci2hst,
           ivci_in  => hst2ivci,
           ivci_out => ivci2hst);

  i_hst: entity global_lib.hst_emulator(arc)
	generic map(cmdfile => cmdfile,
              n_pa_regs => bitfield_width / 64)
  port map(clk       => clk,
           srstn     => srstn,
           ce        => ce,
           hirq      => hirq,
           uirq      => uirq,
           pirq      => pirq,
           dirq      => dirq,
           eirq      => (others => '0'),
           hst2tvci  => hst2tvci,
           tvci2hst  => tvci2hst,
           hst2ivci  => hst2ivci,
           ivci2hst  => ivci2hst,
           gpodvalid => open,
           gpod      => open,
           gpovvalid => open,
           gpov      => open,
           eos       => open);

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
