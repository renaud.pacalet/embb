--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief FEP top level without RAMs
--*
--*  Synthesis results:
--* - CORE65GPLVT_nom_1.00V_25C
--*   - Minimum clock period: 1000 ps (1 GHz)
--*   - Silicon area : 421785.52 um²
--* - CORE65LPHVT_nom_1.20V_25C
--*   - Minimum clock period: 2274 ps (440 MHz)
--*   - Silicon area : 539665.36 um²

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use ieee.numeric_std.all;
use global_lib.global.all;

library css_lib;
use css_lib.css_pkg.all;

use work.bitfield.all; -- defines bitfield_width and bitfield_padmask constants
use work.fep_pkg.all;  -- defines the custom pss2mss_type and mss2pss_type types

entity fep_no_rams is
  generic(
-- pragma translate_off
    debug:    boolean  := false;  --* Print debug information
    verbose:  boolean  := false;  --* Print more debug information
-- pragma translate_on
    n0:       positive := 1;
    n1:       positive := 1);
  port(
    clk:      in  std_ulogic;  -- master clock
    srstn:    in  std_ulogic;  -- master reset
    ce:       in  std_ulogic;  -- chip enable
    hirq:     in  std_ulogic;  -- host input irq,
    uirq:     out std_ulogic;  -- UC output irq,
    pirq:     out std_ulogic;  -- PSS output irq,
    dirq:     out std_ulogic;  -- DMA output irq,
    tvci_in:  in  vci_i2t_type;  -- Target AVCI input
    tvci_out: out vci_t2i_type; -- Target AVCI output
    ivci_in:  in  vci_t2i_type;  -- Initiator AVCI input
    ivci_out: out vci_i2t_type;  -- Initiator AVCI output
    gpo:      out std_ulogic_vector(63 downto 0); -- General Purpose Output
    rams2mss: in  rams2mss_type;  -- Inputs from external RAMs
    mss2rams: out mss2rams_type); -- Outputs to external RAMs
end entity fep_no_rams;

architecture rtl of fep_no_rams is

  signal css2pss:   css2pss_type;
  signal pss2css:   pss2css_type;
  signal param:    std_ulogic_vector(bitfield_width - 1 downto 0);
  signal pss2mss:   pss2mss_type;
  signal mss2pss:   mss2pss_type;
  signal css2mss:  css2mss_type;
  signal mss2css:  mss2css_type;

begin

  gpo <= bitfield_slice(param).gpo;

  i_pss: entity work.pss(rtl)
  generic map(
    mss_pipeline_depth => n0 + n1)
  port map(
    clk    => clk,
    css2pss => css2pss,
    param  => param,
    pss2css => pss2css,
    pss2mss => pss2mss,
    mss2pss => mss2pss);

  i_css: entity css_lib.css(rtl)
  generic map(
-- pragma translate_off
    debug     => debug,
    verbose   => verbose,
    name      => "FEP",
-- pragma translate_on
    n_pa_regs => bitfield_width / 64,
    pa_rmask => bitfield_rpadmask,
    pa_wmask => bitfield_wpadmask,
    with_uc  => false,
    n0       => n0,
    n1       => n1)
  port map(
    clk            => clk,
    srstn          => srstn,
    ce             => ce,
    hirq           => hirq,
    uirq           => uirq,
    pirq           => pirq,
    dirq           => dirq,
    eirq           => open,
    pss2css        => pss2css,
    css2pss        => css2pss,
    param          => param,
    mss2css        => mss2css,
    css2mss        => css2mss,
    tvci_in        => tvci_in,
    tvci_out       => tvci_out,
    ivci_in        => ivci_in,
    ivci_out       => ivci_out);

  i_mss: entity work.mss(rtl)
  generic map(
    n0 => n0,
    n1 => n1)
  port map(
    clk       => clk,
    css2mss   => css2mss,
    mss2css   => mss2css,
    pss2mss    => pss2mss,
    mss2pss    => mss2pss,
    rams2mss  => rams2mss,
    mss2rams  => mss2rams);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
