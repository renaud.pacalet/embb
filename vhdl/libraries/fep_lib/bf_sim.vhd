--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for bf

use std.textio.all;
use std.env.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

use work.type_pkg.all;
use work.pu_pkg.all;

entity bf_sim is
  generic(n: positive := 1000000;
          debug: boolean := false);
end entity bf_sim;

architecture sim of bf_sim is

  signal u, z2, z4: cpx50_vector(0 to 3);

begin

  ibf: entity work.bf(rtl)
    port map(u  => u,
             z2 => z2,
             z4 => z4);

  stim: process
    variable l: line;
    variable p: natural;
    variable z2_ref, z4_ref: cpx50_vector(0 to 3);
  begin
    for i in 1 to n loop
      if debug then
        p := i mod (n / 100);
        if p = 0 then
          write(l, (i * 100) / n);
          write(l, string'("%"));
          writeline(output, l);
        end if;
      end if;
      for i in 0 to 3 loop
        u(i).r <= u_signed(std_ulogic_vector_rnd(25));
        u(i).i <= u_signed(std_ulogic_vector_rnd(25));
      end loop;
      wait for 1 ns;
      z2_ref(0) := u(0) + u(2);
      z2_ref(1) := u(0) - u(2);
      z2_ref(2) := u(1) + u(3);
      z2_ref(3) := u(1) - u(3);
      z4_ref(0) := z2_ref(0) + z2_ref(2);
      z4_ref(1) := z2_ref(1) - xj(z2_ref(3));
      z4_ref(2) := z2_ref(0) - z2_ref(2);
      z4_ref(3) := z2_ref(1) + xj(z2_ref(3));
      assert z2 = z2_ref and z4 = z4_ref
        report "error"
        severity failure;
    end loop;
    report "Regression test passed";
    finish(2);
  end process stim;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
