--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Companion package of FEP processing unit PU (pu.vhd)
--*
--*  This package defines the data types used in pu.vhd and in the
--* dsp.vhd. It also defines a set of non-synthesizable functions and procedures
--* used for the functional validation of PU.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.utils.all;

-- pragma translate_off
use std.textio.all;

library random_lib;
use random_lib.rnd.all;

use global_lib.sim_utils.all;

use work.fep_sim_pkg.all;
-- pragma translate_on

use work.type_pkg.all;

package pu_pkg is

  -- modifier applied to twiddle factors in order to build the whole unit circle
  -- from its first eighth
  type twiddle_modifier is record
    swap: boolean; -- swap real and imaginary parts if true
    negr: boolean; -- then, negate real part if true
    negi: boolean; -- then, negate imaginary part if true
  end record;
  -- vector of TWIDDLE_MODIFIER
  type twiddle_modifier_vector is array(natural range <>) of twiddle_modifier;

  -- operation mode of DSP (see dsp.vhd for detailed description)
  type dsp_mode is (cwa_dsp,   -- Component-wise addition
                    mov_dsp,   -- Move
                    cwl_dsp,   -- Component-wise lookup with interpolation or
                               -- extrapolation
                    ft_dsp,    -- Regular FT butterfly, component-wise product
                               -- or component-wise square of modulus
                    ftl2_dsp,  -- FT butterfly when last stage of 2.4^n points FT
                    ftl4_dsp); -- FT butterfly when last stage of 4^n points FT
  type dsp_mode_vector is array(natural range <>) of dsp_mode; -- vector of dsp_mode

  -- extended operation mode of PU (see pu.vhd for detailed description)
  type extended_op_code is (cwa,   -- Component-wise addition
                            cwp,   -- Component-wise product
                            cwm,   -- Component-wise square of modulus
                            cws,   -- Component-wise sqaure
                            mov,   -- Move
                            cwl,   -- Component-wise lookup
                            cwli,  -- Component-wise lookup with interpolation
                            cwle,  -- Component-wise lookup with extrapolation
                            ft,    -- Regular FT butterfly
                            ftl2,  -- FT butterfly when last stage of 2.4^n points FT
                            ftl4); -- FT butterfly when last stage of 4^n points FT
  -- vector of EXTENDED_OP_CODE
  type extended_op_code_vector is array(natural range <>) of extended_op_code;

  -- type of DSP inputs
  type dsp_in is record
    mode:   dsp_mode;   -- operation mode
    n0, n1: boolean;    -- negate flags
    a0, a1: s25;      -- A inputs
    b0, b1: s16;      -- B inputs
    c0, c1: s16;      -- C inputs
  end record;
  type dsp_in_vector is array(natural range <>) of dsp_in; -- vector of DSP_IN
  -- type of DSP outputs
  type dsp_out is record
    z0:  s43; -- first 43 bits u_signed output
    z1:  s43; -- second 43 bits u_signed output
  end record;
  -- vector of DSP_OUT
  type dsp_out_vector is array(natural range <>) of dsp_out;
  -- Initialisation value for DSP inputs (used at reset)
  constant dsp_di0: dsp_in :=
    (mode => cwa_dsp,
     n0 => false,
     n1 => false,
     a0 => (others => '0'),
     a1 => (others => '0'),
     b0 => (others => '0'),
     b1 => (others => '0'),
     c0 => (others => '0'),
     c1 => (others => '0'));

  -- the DSP module has DSP_PIPE_DEPTH internal register barriers
  constant dsp_pipe_depth: positive := 2;

  -- the PU module has PU_PIPE_DEPTH internal register barriers
  constant pu_pipe_depth: positive := dsp_pipe_depth + 1;

  -- inverse of square root of 2 in Q1.15 form (fixed point, 15 bits fractional
  -- part
  constant invsqrt2: s16 := to_signed(23170, 16);

  -- convert a 32 bits X or Y input according its type (T) and alignement (A):
  --  T  A RESULT
  -- 00 00 XY(31 downto 24) * (1 + J) * 256
  -- 00 01 XY(23 downto 16) * (1 + J) * 256
  -- 00 10 XY(15 downto 8) * (1 + J) * 256
  -- 00 11 XY(7 downto 0) * (1 + J) * 256
  -- 01 00 XY(31 downto 16) * (1 + J)
  -- 01 10 XY(15 downto 0) * (1 + J)
  -- 01 x1 forbidden
  -- 10 00 (XY(31 downto 24) + J.XY(23 downto 16)) * 256
  -- 10 10 (XY(15 downto 8) + J.XY(7 downto 0)) * 256
  -- 10 x1 forbidden
  -- 11 00 XY(31 downto 16) + J.XY(15 downto 0)
  -- 11 x1 forbidden
  -- 11 1x forbidden
  function xycnv(xy: std_ulogic_vector(31 downto 0);
                 t: std_ulogic_vector(1 downto 0);
                 a: std_ulogic_vector(1 downto 0)) return cpx32;

  -- value modification of (already converted) X or Y values, depending on VR,
  -- VI and D. same processing for real and imaginary parts. S is the output
  -- result and NR (NI) is a negate flag for the real (imaginary) part of the
  -- result:
  -- VR D          NR      S.R
  -- 00 0       FALSE      0
  -- 01 0        TRUE     XY.R
  -- 10 0  XY.R(15)=1     XY.R
  -- 11 0       FALSE   XY.R
  -- 00 1       FALSE      0
  -- 01 1        TRUE XY.R/256
  -- 10 1  XY.R(15)=1 XY.R/256
  -- 11 1       FALSE XY.R/256
  procedure xymod(xy: in cpx32; vr: in std_ulogic_vector(1 downto 0);
                  vi: in std_ulogic_vector(1 downto 0); d: in std_ulogic;
                  s: out cpx32; nr: out boolean; ni: out boolean);

  -- convert a 32 bits Y input according its type (T) and alignment (A); used
  -- in CWL operations to retrieve the Y0 and Y1 consecutive lookup table
  -- entries.
  -- T  A                    Y0                    Y1
  -- 0  0 Y(31 downto 24) * 256 Y(23 downto 16) * 256
  -- 0  1 Y(23 downto 16) * 256  Y(15 downto 8) * 256
  -- 1  x Y(31 downto 16)              Y(15 downto 0)
  procedure cwlcnv(y: in std_ulogic_vector(31 downto 0); t: in std_ulogic;
                   a: in std_ulogic; y0: out s16; y1: out s16);

  -- type of PU inputs
  type pu_in is record
    op:                 extended_op_code;                -- extended operation code
    i:                  std_ulogic;                      -- inverse DFT flag
    u:                  cpx50_vector(0 to 3);            -- 4 CPX50 (inputs of FT operations)
    t:                  cpx32_vector(1 to 3);            -- 3 twiddle factors
    tmod:               twiddle_modifier_vector(1 to 3); -- 3 twiddle modifiers
    x, y:               std_ulogic_vector(31 downto 0);  -- X and Y inputs
    tx, ty:             std_ulogic_vector(1 downto 0);   -- X and Y type indicators
    ax, ay:             std_ulogic_vector(1 downto 0);   -- X and Y alignments
    dx, dy:             std_ulogic;                      -- X and Y first value modifiers
    vrx, vix, vry, viy: std_ulogic_vector(1 downto 0);   -- X and Y second value modifiers
    c1:                 u15;                          -- CWL coefficient (for inter- extra-polation)
  end record;
  
  type pu_in_v is array (natural range <>) of pu_in;

  -- type of PU outputs
  type pu_out is record
    u:                  cpx50_vector(0 to 3); -- 4 CPX50 (outputs of FT operations)
    v:                  cpx66;                -- output of operations other than FT
  end record;
  type pu_out_v is array (natural range <>) of pu_out;
  -- Initialisation value for PU inputs (used at reset)
  constant pu_di_none: pu_in :=
    (op   => cwa,
     i    => '0',
     u    => (others => (others => (others => '0'))),
     t    => (others => (others => (others => '0'))),
     tmod => (others => (others => false)),
     x    => (others => '0'),
     y    => (others => '0'),
     tx   => (others => '0'),
     ty   => (others => '0'),
     ax   => (others => '0'),
     ay   => (others => '0'),
     vrx  => (others => '0'),
     vix  => (others => '0'),
     vry  => (others => '0'),
     viy  => (others => '0'),
     dx   => '0',
     dy   => '0',
     c1   => (others => '0'));

  -- pragma translate_off
  -- these functions are used for simulation only
  function "+"(l, r: cpx50) return cpx50; -- sum of 2 CPX50
  function "-"(l, r: cpx50) return cpx50; -- subtraction of 2 CPX50
  function "-"(a: cpx50) return cpx50;    -- negate a CPX50
  function xj(a: cpx50) return cpx50;     -- multiply a CPX50 by J

  -- reference functional model of the DSP module
  function dsp_ref(di: in dsp_in) return dsp_out;
  -- generates a valid random input for the DSP module
  impure function dsp_rnd return dsp_in;
  -- print a DSP input in human readable form
  procedure print(di: in dsp_in);
  -- print a DSP output in human readable form
  procedure print(do: in dsp_out);
  -- reference functional model of the PU module
  function pu_ref(di: in pu_in) return pu_out;
  -- generates a valid random input for the PU module
  impure function pu_rnd return pu_in;
  -- print a PU input in human readable form
  procedure print(di: in pu_in);
  -- print a PU output in human readable form
  procedure print(do: in pu_out);
  -- pragma translate_on

end package pu_pkg;

package body pu_pkg is

  function xycnv(xy: std_ulogic_vector(31 downto 0);
                 t: std_ulogic_vector(1 downto 0);
                 a: std_ulogic_vector(1 downto 0)) return cpx32 is
    variable tmp: std_ulogic_vector(31 downto 0);
    variable res: cpx32;
  begin
    tmp := xy;
    -- first, align input on the left of the 32-bits TMP
    case a is
      when "00" => null;
      when "01" => tmp := shift_left(tmp, 8);
      when "10" => tmp := shift_left(tmp, 16);
      when others => tmp := shift_left(tmp, 24);
    end case;
    -- next, type-convert
    case t is
      when "00" => res := (r => u_signed(tmp(31 downto 24)) & x"00", i => u_signed(tmp(31 downto 24)) & x"00");
      when "01" => res := (r => s16(tmp(31 downto 16)), i => s16(tmp(31 downto 16)));
      when "10" => res := (r => u_signed(tmp(31 downto 24)) & x"00", i => u_signed(tmp(23 downto 16)) & x"00");
      when others => res := (r => s16(tmp(31 downto 16)), i => s16(tmp(15 downto 0)));
    end case;
    return res;
  end function xycnv;

  procedure xymod(xy: in cpx32; vr: in std_ulogic_vector(1 downto 0);
                  vi: in std_ulogic_vector(1 downto 0); d: in std_ulogic;
                  s: out cpx32; nr: out boolean; ni: out boolean) is
    variable tmp: cpx32;
  begin
    nr := false; -- by default real part of result shall not be negated
    ni := false; -- by default imaginary part of result shall not be negated
    tmp := xy; -- by default result equals input
    if d = '1' then -- scale down by 256
      tmp.r := shift_right(tmp.r, 8);
      tmp.i := shift_right(tmp.i, 8);
    end if;
    case vr is
      when "00" => tmp.r := (others => '0'); -- zero real part
      when "01" => nr := true;               -- negate real part
      when "10" => nr := tmp.r(15) = '1';    -- absolute value of real part
      when others => null;                   -- do not modify real part
    end case;
    case vi is
      when "00" => tmp.i := (others => '0'); -- zero imaginary part
      when "01" => ni := true;               -- negate imaginary part
      when "10" => ni := tmp.i(15) = '1';    -- absolute value of imaginary part
      when others => null;                   -- do not modify imaginary part
    end case;
    s := tmp;
  end procedure xymod;

  procedure cwlcnv(y: in std_ulogic_vector(31 downto 0); t: in std_ulogic;
                   a: in std_ulogic; y0: out s16; y1: out s16) is
    variable tmp: std_ulogic_vector(31 downto 0);
  begin
    -- first, align input on the left of the 32-bits TMP
    if a = '0' then
      tmp := y;
    else
      tmp := shift_left(y, 8);
    end if;
    -- next, type-convert
    if t = '0' then -- type = INT8
      y0 := u_signed(tmp(31 downto 24)) & x"00";
      y1 := u_signed(tmp(23 downto 16)) & x"00";
    else -- type = INT16
      y0 := u_signed(tmp(31 downto 16));
      y1 := u_signed(tmp(15 downto 0));
    end if;
  end procedure cwlcnv;

-- pragma translate_off
  -- maximum bit-length of real and imaginary parts of the generic complex type
  -- CPX
  constant cpx_len: positive := 100;

  type cpx is record -- generic complex type
    r, i: u_signed(cpx_len - 1 downto 0);
  end record;
  type cpx_vector is array(natural range <>) of cpx; -- vector of CPX

  -- sum of two CPX
  function "+"(l, r: cpx) return cpx is
    variable res: cpx;
  begin
    res.r := l.r + r.r;
    res.i := l.i + r.i;
    return res;
  end function "+";

  -- subtraction of two CPX
  function "-"(l, r: cpx) return cpx is
    variable res: cpx;
  begin
    res.r := l.r - r.r;
    res.i := l.i - r.i;
    return res;
  end function "-";

  -- negation of a CPX
  function "-"(a: cpx) return cpx is
    variable res: cpx;
  begin
    res.r := - a.r;
    res.i := - a.i;
    return res;
  end function "-";

  -- conjugate of a CPX
  function conjugate(a: cpx) return cpx is
    variable res: cpx;
  begin
    res.r := a.r;
    res.i := -a.i;
    return res;
  end function conjugate;

  -- multiply of a CPX by J
  function xj(a: cpx) return cpx is
    variable res: cpx;
  begin
    res.r := - a.i;
    res.i := a.r;
    return res;
  end function xj;

  -- product of two CPX
  function "*"(l, r: cpx) return cpx is
    variable res: cpx;
    variable tmp: u_signed(cpx_len - 1 downto 0);
  begin
    res.r := resize(l.r * r.r, cpx_len) - resize(l.i * r.i, cpx_len);
    res.i := resize(l.r * r.i, cpx_len) + resize(l.i * r.r, cpx_len);
    return res;
  end function "*";

  -- product of a CPX by a u_signed
  function "*"(l: cpx; r: u_signed) return cpx is
    variable res: cpx;
  begin
    res.r := resize(l.r * r, cpx_len);
    res.i := resize(l.i * r, cpx_len);
    return res;
  end function "*";

  -- left shift of a CPX, zeroes enter to the right
  function shift_left(a: cpx; n: natural) return cpx is
    variable res: cpx;
  begin
    res.r := shift_left(a.r, n);
    res.i := shift_left(a.i, n);
    return res;
  end function shift_left;

  -- u_signed right shift of a CPX, sign-extension on the left
  function shift_right(a: cpx; n: natural) return cpx is
    variable res: cpx;
  begin
    res.r := shift_right(a.r, n);
    res.i := shift_right(a.i, n);
    return res;
  end function shift_right;

  -- resize of a CPX
  function resize(a: cpx; n: natural) return cpx is
    variable res: cpx;
  begin
    res.r := resize(a.r(n - 1 downto 0), cpx_len);
    res.i := resize(a.i(n - 1 downto 0), cpx_len);
    return res;
  end function resize;

  -- convert a cpx32 in a cpx (sign extension)
  function to_cpx(a: cpx32) return cpx is
    variable res: cpx;
  begin
    res.r := resize(a.r, cpx_len);
    res.i := resize(a.i, cpx_len);
    return res;
  end function to_cpx;

  -- convert a cpx50 in a cpx (sign extension)
  function to_cpx(a: cpx50) return cpx is
    variable res: cpx;
  begin
    res.r := resize(a.r, cpx_len);
    res.i := resize(a.i, cpx_len);
    return res;
  end function to_cpx;

  -- convert a cpx66 in a cpx (sign extension)
  function to_cpx(a: cpx66) return cpx is
    variable res: cpx;
  begin
    res.r := resize(a.r, cpx_len);
    res.i := resize(a.i, cpx_len);
    return res;
  end function to_cpx;

  -- convert a cpx in a cpx32 (truncation)
  function to_cpx32(a: cpx) return cpx32 is
    variable res: cpx32;
  begin
    res.r := a.r(15 downto 0);
    res.i := a.i(15 downto 0);
    return res;
  end function to_cpx32;

  -- convert a cpx in a cpx50 (truncation)
  function to_cpx50(a: cpx) return cpx50 is
    variable res: cpx50;
  begin
    res.r := a.r(24 downto 0);
    res.i := a.i(24 downto 0);
    return res;
  end function to_cpx50;

  -- convert a cpx in a cpx66 (truncation)
  function to_cpx66(a: cpx) return cpx66 is
    variable res: cpx66;
  begin
    res.r := a.r(32 downto 0);
    res.i := a.i(32 downto 0);
    return res;
  end function to_cpx66;

  -- sum of two cpx50
  function "+"(l, r: cpx50) return cpx50 is
    variable res: cpx50;
  begin
    res.r := l.r + r.r;
    res.i := l.i + r.i;
    return res;
  end function "+";

  -- subtraction of two cpx50
  function "-"(l, r: cpx50) return cpx50 is
    variable res: cpx50;
  begin
    res.r := l.r - r.r;
    res.i := l.i - r.i;
    return res;
  end function "-";

  -- negate a cpx50
  function "-"(a: cpx50) return cpx50 is
    variable res: cpx50;
  begin
    res.r := -a.r;
    res.i := -a.i;
    return res;
  end function "-";

  -- multiply a cpx50 by j
  function xj(a: cpx50) return cpx50 is
    variable res: cpx50;
  begin
    res.r := -a.i;
    res.i := a.r;
    return res;
  end function xj;

  -- butterfly of last stage of 2.4^n points FTs
  function bf2(a: cpx50_vector(0 to 3)) return cpx_vector is
    variable res: cpx_vector(0 to 3);
  begin
    res(0) := to_cpx(a(0) + a(1));
    res(1) := to_cpx(a(0) - a(1));
    res(2) := to_cpx(a(2) + a(3));
    res(3) := to_cpx(a(2) - a(3));
    return res;
  end function bf2;

  -- butterfly of all stages of 4^n points FTs and of all stages but the last of
  -- 2.4^n points FTs
  function bf4(a: cpx50_vector(0 to 3)) return cpx_vector is
    variable res: cpx_vector(0 to 3);
  begin
    res(0) := to_cpx(a(0) + a(1) + a(2) + a(3));
    res(1) := to_cpx(a(0) - xj(a(1)) - a(2) + xj(a(3)));
    res(2) := to_cpx(a(0) - a(1) + a(2) - a(3));
    res(3) := to_cpx(a(0) + xj(a(1)) - a(2) - xj(a(3)));
    return res;
  end function bf4;

  function dsp_ref(di: in dsp_in) return dsp_out is
    variable res: dsp_out;
  begin
    case di.mode is
      when cwa_dsp => if di.n1 then -- z0=c1-b1
                        res.z0 := resize(di.c1, 43) - di.b1;
                      else -- z0=c1+b1
                        res.z0 := resize(di.c1, 43) + di.b1;
                      end if;
                      if di.n0 then -- z0=-(c1+b1) or z0=b1-c1
                        res.z0 := -res.z0;
                      end if;
      when mov_dsp => if di.n0 then -- z0=-b0
                        res.z0 := -resize(di.b0, 43);
                      else -- z0=b0
                        res.z0 := resize(di.b0, 43);
                      end if;
      -- z0=2^15.c0 + a0.b0
      when cwl_dsp => res.z0 := shift_left(resize(di.c0, 43), 15) + di.a0 * di.b0;
      when ft_dsp => if di.n0 then -- z0=-a0.b0
                       res.z0 := -resize(di.a0 * di.b0, 43);
                     else -- z0=a0.b0
                       res.z0 := resize(di.a0 * di.b0, 43);
                     end if;
                     if di.n1 then -- z0 -= a1.b1
                       res.z0 := res.z0 -resize(di.a1 * di.b1, 43);
                     else -- z0 += a1.b1
                       res.z0 := res.z0 + resize(di.a1 * di.b1, 43);
                     end if;
      when ftl2_dsp => res.z0 := resize(di.a0 * di.b0, 43); -- z0=a0.b0
                       res.z1 := resize(di.a1 * di.b1, 43); -- z1=a1.b1
      when ftl4_dsp => res.z0 := shift_left(resize(di.a0, 43), 18); --z0=2^18.a0
      when others => assert false
                       report "invalid dsp mode"
                       severity failure;
    end case;
    return res;
  end function dsp_ref;

  impure function dsp_rnd return dsp_in is
    variable res: dsp_in;
  begin
    res.mode := dsp_mode'val(int_rnd(0, 5));
    res.n0 := boolean_rnd;
    res.n1 := boolean_rnd;
    res.a0 := u_signed(std_ulogic_vector_rnd(25));
    res.a1 := u_signed(std_ulogic_vector_rnd(25));
    res.b0 := u_signed(std_ulogic_vector_rnd(16));
    res.b1 := u_signed(std_ulogic_vector_rnd(16));
    res.c0 := u_signed(std_ulogic_vector_rnd(16));
    res.c1 := u_signed(std_ulogic_vector_rnd(16));
    return res;
  end function dsp_rnd;

  procedure print(di: in dsp_in) is
    variable l: line;
  begin
    write(l, string'("mode: "));
    case di.mode is
      when cwa_dsp  => write(l, string'("cwa"));
      when mov_dsp  => write(l, string'("mov_dsp"));
      when cwl_dsp  => write(l, string'("cwl_dsp"));
      when ft_dsp   => write(l, string'("ft_dsp"));
      when ftl2_dsp => write(l, string'("ftl2_dsp"));
      when ftl4_dsp => write(l, string'("ftl4_dsp"));
      when others => assert false
                       report "invalid dsp mode"
                       severity failure;
    end case;
    writeline(output, l);
    write(l, string'("n0="));
    write(l, di.n0);
    write(l, string'("- a0="));
    hwrite(l, "000" & std_ulogic_vector(di.a0));
    write(l, string'("- b0="));
    hwrite(l, std_ulogic_vector(di.b0));
    write(l, string'("- c0="));
    hwrite(l, std_ulogic_vector(di.c0));
    writeline(output, l);
    write(l, string'("n1="));
    write(l, di.n1);
    write(l, string'("- a1="));
    hwrite(l, "000" & std_ulogic_vector(di.a1));
    write(l, string'("- b1="));
    hwrite(l, std_ulogic_vector(di.b1));
    write(l, string'("- c1="));
    hwrite(l, std_ulogic_vector(di.c1));
    writeline(output, l);
  end procedure print;

  procedure print(do: in dsp_out) is
    variable l: line;
  begin
    write(l, string'("z0="));
    hwrite(l, '0' & std_ulogic_vector(do.z0));
    writeline(output, l);
    write(l, string'("z1="));
    hwrite(l, '0' & std_ulogic_vector(do.z1));
    writeline(output, l);
  end procedure print;

  function pu_ref(di: in pu_in) return pu_out is
    variable twd: cpx_vector(1 to 3);
    variable bfo: cpx_vector(0 to 3);
    variable tmp: cpx;
    variable xm, ym: cpx;
    variable s32: u_signed(31 downto 0);
    variable s33: u_signed(32 downto 0);
    variable y0, y1: s16;
    variable do: pu_out;
    variable idft: boolean;
  begin
    idft := di.i = '1';
    if di.op /= ft and di.op /= ftl2 and di.op /= ftl4 then
      -- compute X input
      assert di.ax = "00" or (di.ax(0) = '0' and (di.tx = "01" or di.tx = "10")) or di.tx = "00"
        report "pu_ref: invalid combination of X type and X alignment"
        severity warning;
      s32 := u_signed(di.x);
      -- alignment
      if di.ax = "01" then
        s32 := shift_left(s32, 8);
      elsif di.ax = "10" then
        s32 := shift_left(s32, 16);
      elsif di.ax = "11" then
        s32 := shift_left(s32, 24);
      end if;
      -- type conversion
      if di.tx = "00" then
        xm := (r => resize(s32(31 downto 24), cpx_len), i => resize(s32(31 downto 24), cpx_len));
        xm := shift_left(xm, 8);
      elsif di.tx = "01" then
        xm := (r => resize(s32(31 downto 16), cpx_len), i => resize(s32(31 downto 16), cpx_len));
      elsif di.tx = "10" then
        xm := (r => resize(s32(31 downto 24), cpx_len), i => resize(s32(23 downto 16), cpx_len));
        xm := shift_left(xm, 8);
      else
        xm := (r => resize(s32(31 downto 16), cpx_len), i => resize(s32(15 downto 0), cpx_len));
      end if;
      ym := xm;
      if di.dx = '1' then -- scale down by 256
        xm := shift_right(xm, 8);
      end if;
      if di.vrx = "00" then -- zero real part
        xm.r := (others => '0');
      elsif di.vrx = "01" or (di.vrx = "10" and xm.r < 0) then -- negate real part
        xm.r := -xm.r;
      end if;
      if di.vix = "00" then -- zero imaginary part
        xm.i := (others => '0');
      elsif di.vix = "01" or (di.vix = "10" and xm.i < 0) then -- negate imaginary part
        xm.i := -xm.i;
      end if;
      if di.op = cws then
        -- same for Y input
        if di.dy = '1' then
          ym := shift_right(ym, 8);
        end if;
        if di.vry = "00" then
          ym.r := (others => '0');
        elsif di.vry = "01" or (di.vry = "10" and ym.r < 0) then
          ym.r := -ym.r;
        end if;
        if di.viy = "00" then
          ym.i := (others => '0');
        elsif di.viy = "01" or (di.viy = "10" and ym.i < 0) then
          ym.i := -ym.i;
        end if;
      elsif di.op = cwa or di.op = cwp then
        -- same for Y input
        assert di.ay = "00" or (di.ay(0) = '0' and (di.ty = "01" or di.ty = "10")) or di.ty = "00"
          report "pu_ref: invalid combination of Y type and Y alignment"
          severity warning;
        s32 := u_signed(di.y);
        if di.ay = "01" then
          s32 := shift_left(s32, 8);
        elsif di.ay = "10" then
          s32 := shift_left(s32, 16);
        elsif di.ay = "11" then
          s32 := shift_left(s32, 24);
        end if;
        if di.ty = "00" then
          ym := (r => resize(s32(31 downto 24), cpx_len), i => resize(s32(31 downto 24), cpx_len));
          ym := shift_left(ym, 8);
        elsif di.ty = "01" then
          ym := (r => resize(s32(31 downto 16), cpx_len), i => resize(s32(31 downto 16), cpx_len));
        elsif di.ty = "10" then
          ym := (r => resize(s32(31 downto 24), cpx_len), i => resize(s32(23 downto 16), cpx_len));
          ym := shift_left(ym, 8);
        else
          ym := (r => resize(s32(31 downto 16), cpx_len), i => resize(s32(15 downto 0), cpx_len));
        end if;
        if di.dy = '1' then
          ym := shift_right(ym, 8);
        end if;
        if di.vry = "00" then
          ym.r := (others => '0');
        elsif di.vry = "01" or (di.vry = "10" and ym.r < 0) then
          ym.r := -ym.r;
        end if;
        if di.viy = "00" then
          ym.i := (others => '0');
        elsif di.viy = "01" or (di.viy = "10" and ym.i < 0) then
          ym.i := -ym.i;
        end if;
      elsif di.op = cwl or di.op = cwli or di.op = cwle then
        -- compute Y0 and Y1 (CWL only)
        assert di.ty(1) = '0'
          report "pu_ref: invalid Y type for CWL operation"
          severity warning;
        assert di.ay = "00" or (di.ty(0) = '0' and di.ay = "01")
          report "pu_ref: invalid combination of Y type and Y alignment for CWL operation"
          severity warning;
        s32 := u_signed(di.y);
        -- alignment
        if di.ay = "01" then
          s32 := shift_left(s32, 8);
        end if;
        -- type conversion
        if di.ty(0) = '0' then
          y0 := s32(31 downto 24) & x"00";
          y1 := s32(23 downto 16) & x"00";
        else
          y0 := s32(31 downto 16);
          y1 := s32(15 downto 0);
        end if;
        s32 := shift_left(resize(y0, 32), 15); -- s32 = 2^15.y0
        -- s33 = c1.(y1-y0)
        s33 := u_signed(resize(di.c1, 16)) * (resize(y1, 17) - resize(y0, 17));
      end if;
    elsif di.op = ft then
      -- apply twiddle modifiers
      for i in 1 to 3 loop
        twd(i) := to_cpx(di.t(i));
        if di.tmod(i).swap then -- swap real and imaginary parts
          twd(i) := (r => twd(i).i, i => twd(i).r);
        end if;
        if di.tmod(i).negr then -- negate real part
          twd(i).r := -twd(i).r;
        end if;
        if di.tmod(i).negi xor idft then -- negate imaginary part
          twd(i).i := -twd(i).i;
        end if;
      end loop;
    end if;
    case di.op is
      when ft => bfo := bf4(di.u);
                 if idft then
                   tmp := bfo(1);
                   bfo(1) := bfo(3);
                   bfo(3) := tmp;
                 end if;
                 do.u(0) := to_cpx50(shift_right(bfo(0), 1));
                 for i in 1 to 3 loop
                   do.u(i) := to_cpx50(shift_right(bfo(i) * twd(i), 16));
                 end loop;
      when ftl2 => bfo := bf2(di.u);
                   for i in 0 to 3 loop
                     do.u(i) := to_cpx50(shift_right(bfo(i) * invsqrt2, 15));
                   end loop;
      when ftl4 => bfo := bf4(di.u);
                   if idft then
                     tmp := bfo(1);
                     bfo(1) := bfo(3);
                     bfo(3) := tmp;
                   end if;
                   for i in 0 to 3 loop
                     do.u(i) := to_cpx50(shift_right(bfo(i), 1));
                   end loop;
      when cwa => do.v := to_cpx66(xm + ym);
      when cwp | cws => do.v := to_cpx66(xm * ym);
      when cwm => do.v.r := resize(xm.r * xm.r, 33) + resize(xm.i * xm.i, 33);
                  do.v.i := do.v.r;
      when mov => do.v := to_cpx66(xm);
      when cwl => s33 := resize(y0, 33);
                  s33 := shift_left(s33, 15);
                  do.v.r := s33;
                  do.v.i := s33;
      when cwli => s33 := s33 + s32; -- 2^15.y0 + c1.(y1-y0)
                   do.v.r := s33;
                   do.v.i := s33;
      when cwle => s32 := shift_left(resize(y1, 32), 15); -- 2^15.y1 + c1.(y1-y0)
                   s33 := s33 + s32;
                   do.v.r := s33;
                   do.v.i := s33;
      when others =>
        assert false
          report "invalid op code"
          severity failure;
    end case;
    return do;
  end function pu_ref;

  impure function cpx50_rnd return cpx50 is
  begin
    return (r => u_signed(std_ulogic_vector_rnd(25)),
            i => u_signed(std_ulogic_vector_rnd(25)));
  end function cpx50_rnd;

  impure function cpx32_rnd return cpx32 is
  begin
    return (r => u_signed(std_ulogic_vector_rnd(16)),
            i => u_signed(std_ulogic_vector_rnd(16)));
  end function cpx32_rnd;

  impure function pu_rnd return pu_in is
    variable res: pu_in;
  begin
    res.op := extended_op_code'val((int_rnd(0, 9)));
    res.i := std_ulogic_rnd;
    for i in 0 to 3 loop
      res.u(i) := cpx50_rnd;
    end loop;
    for i in 1 to 3 loop
      res.t(i) := cpx32_rnd;
      res.tmod(i).swap := boolean_rnd;
      res.tmod(i).negr := boolean_rnd;
      res.tmod(i).negi := boolean_rnd;
    end loop;
    res.x := std_ulogic_vector_rnd(32);
    res.y := std_ulogic_vector_rnd(32);
    res.tx := std_ulogic_vector_rnd(2);
    res.ty := std_ulogic_vector_rnd(2);
    if res.op = cwl or res.op = cwli or res.op = cwle then
      res.ty(1) := '0';
    end if;
    res.ax := std_ulogic_vector_rnd(2);
    if res.tx = "01" or res.tx = "10" then
      res.ax(0) := '0';
    elsif res.tx = "11" then
      res.ax := "00";
    end if;
    res.ay := std_ulogic_vector_rnd(2);
    if res.ty = "01" or res.ty = "10" then
      res.ay(0) := '0';
    elsif res.ty = "11" then
      res.ay := "00";
    end if;
    res.vrx := std_ulogic_vector_rnd(2);
    res.vix := std_ulogic_vector_rnd(2);
    res.vry := std_ulogic_vector_rnd(2);
    res.viy := std_ulogic_vector_rnd(2);
    res.dx := std_ulogic_rnd;
    res.dy := std_ulogic_rnd;
    res.c1 := u_unsigned(std_ulogic_vector_rnd(15));
    if res.op = cwl or res.op = cwli or res.op = cwle then
      res.ty(1) := '0';
      if res.ty = "01" then
        res.ay := "00";
      elsif res.ay = "11" then
        res.ay := std_ulogic_vector(to_unsigned(int_rnd(0, 2), 2));
      end if;
    end if;
    return res;
  end function pu_rnd;

  procedure print(di: in pu_in) is
    variable l: line;
  begin
    write(l, string'("op: "));
    case di.op is
      when cwa  => write(l, string'("cwa"));
      when cwp  => write(l, string'("cwp"));
      when cwm  => write(l, string'("cwm"));
      when cws  => write(l, string'("cws"));
      when mov  => write(l, string'("mov"));
      when cwl  => write(l, string'("cwl"));
      when cwli => write(l, string'("cwli"));
      when cwle => write(l, string'("cwle"));
      when ft   => write(l, string'("ft"));
      when ftl2 => write(l, string'("ftl2"));
      when ftl4 => write(l, string'("ftl4"));
      when others => assert false
                       report "invalid extended op code"
                       severity failure;
    end case;
    writeline(output, l);
    for i in 0 to 3 loop
      write(l, string'("u("));
      write(l, i);
      write(l, string'("): ("));
      hwrite(l, "000" & std_ulogic_vector(di.u(i).r));
      write(l, string'(", "));
      hwrite(l, "000" & std_ulogic_vector(di.u(i).i));
      write(l, string'(")"));
      writeline(output, l);
    end loop;
    for i in 1 to 3 loop
      write(l, string'("t("));
      write(l, i);
      write(l, string'("): ("));
      hwrite(l, std_ulogic_vector(di.t(i).r));
      write(l, string'(", "));
      hwrite(l, std_ulogic_vector(di.t(i).i));
      write(l, string'(") - tmod("));
      write(l, i);
      write(l, string'("): "));
      write(l, di.tmod(i).swap);
      write(l, string'(","));
      write(l, di.tmod(i).negr);
      write(l, string'(","));
      write(l, di.tmod(i).negi);
      writeline(output, l);
    end loop;
    write(l, string'("x: "));
    hwrite(l, di.x);
    write(l, string'(" - tx: "));
    write(l, to_bitvector(di.tx));
    write(l, string'(" - ax: "));
    write(l, to_bitvector(di.ax));
    write(l, string'(" - vrx: "));
    write(l, to_bitvector(di.vrx));
    write(l, string'(" - vix: "));
    write(l, to_bitvector(di.vix));
    write(l, string'(" - dx: "));
    write(l, to_bit(di.dx));
    writeline(output, l);
    write(l, string'("y: "));
    hwrite(l, di.y);
    write(l, string'(" - ty: "));
    write(l, to_bitvector(di.ty));
    write(l, string'(" - ay: "));
    write(l, to_bitvector(di.ay));
    write(l, string'(" - vry: "));
    write(l, to_bitvector(di.vry));
    write(l, string'(" - viy: "));
    write(l, to_bitvector(di.viy));
    write(l, string'(" - dy: "));
    write(l, to_bit(di.dy));
    writeline(output, l);
    write(l, string'("c1: "));
    hwrite(l, '0' & std_ulogic_vector(di.c1));
    writeline(output, l);
  end procedure print;

  procedure print(do: in pu_out) is
    variable l: line;
  begin
    for i in 0 to 3 loop
      write(l, string'("u("));
      write(l, i);
      write(l, string'("): ("));
      hwrite(l, "000" & std_ulogic_vector(do.u(i).r));
      write(l, string'(", "));
      hwrite(l, "000" & std_ulogic_vector(do.u(i).i));
      write(l, string'(")"));
      writeline(output, l);
    end loop;
    write(l, string'("v: ("));
    hwrite(l, "000" & std_ulogic_vector(do.v.r));
    write(l, string'(", "));
    hwrite(l, "000" & std_ulogic_vector(do.v.i));
    write(l, string'(")"));
    writeline(output, l);
  end procedure print;
-- pragma translate_on

end package body pu_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
