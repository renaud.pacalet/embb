--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Prototype vpxy_address generator for FEP vector operations, read part
--*
--* PLease see the FEP user guide for a complete description of the address
--* generation algorithm. This module is synchronous on the rising edge of its
--* clock CLK. Its reset SRSTN is synchronous and active low.
--* vpxy_address_generator samples the address generation parameters when its LOAD
--* input is asserted on the rising edge of CLK. On the following rising edge
--* of CLK, if its RUN input is asserted, it puts the two first byte
--* addresses on its two output busses A0 and A1. Every time RUN is
--* asserted vpxy_address_generator outputs the two next addresses. When RUN is
--* de-asserted vpxy_address_generator is stalled. RUN is ignored when LOAD is
--* asserted. The generated addresses are byte addresses in a Mio bank (2^14
--* bytes), represented on 14 bits.
--*
--* Important note: this module is a very delicate piece of control; it has been
--* validated by extensive simulations. It synthesizes on a LX330 Virtex 5
--* 5VLX330FF1760-2 target with a 200 MHz clock frequency and 225 LUTs. On a 65 nm
--* ASIC target, worst case corner, it synthesizes with a 450 MHz clock frequency
--* and 4000 square microns. Modify at your own risks. Do not use a modified
--* version before running the regression tests:
--* $ make vpxy_address_generator_sim.sim

library ieee;
use ieee.std_logic_1164.all;

library global_lib;

use ieee.numeric_std.all;
use global_lib.utils.all;

entity vpxy_address_generator is
  port(clk  : in  std_ulogic;
       srstn: in  std_ulogic;
       ce:    in  std_ulogic;
       load : in  std_ulogic;
       run  : in  std_ulogic;
       t    : in  u_unsigned(1 downto 0);
       b    : in  u_unsigned(13 downto 0);
       s    : in  std_ulogic;
       n    : in  u_unsigned(6 downto 0);
       m    : in  u_unsigned(7 downto 0);
       p    : in  u_unsigned(13 downto 0);
       w    : in  u_unsigned(1 downto 0);
       a0   : out u_unsigned(13 downto 0);
       a1   : out u_unsigned(13 downto 0);
       en0  : out std_ulogic_vector(0 to 7);
       en1  : out std_ulogic_vector(0 to 7));
end entity vpxy_address_generator;

architecture rtl of vpxy_address_generator is

  -- Signals related to management of periodic sequences (p parameter):
  -- p0 is the register for the p parameter
  -- pr is the register for the p-periodic counter
  signal p0, pr: u_unsigned(13 downto 0);
  -- Control signals
  signal freeze_pr_d, freeze_pr_q, freeze_pr, pr_eq0, pr_eq1, pr_eq2, pr_less_than_4: boolean;
  -- Inputs and outputs of the subtracter
  signal p_sub_in1, p_sub_out: u_unsigned(13 downto 0);
  signal p_sub_in2: natural range 1 to 2;

  -- Signals related to management of non-integer indices increments (m
  -- parameter):
  -- m0 is the register for the m parameter
  -- mr is the register for the m-periodic counter
  signal m0, mr: u_unsigned(7 downto 0);
  -- Control signals
  signal freeze_mr_d, freeze_mr_q, freeze_mr, mr_eq0, mr_eq1, mr_eq2, mr_less_than_4: boolean;
  -- Inputs and outputs of the subtracter
  signal m_sub_in1, m_sub_out: u_unsigned(7 downto 0);
  signal m_sub_in2: natural range 1 to 2;

  -- Signals related to components' indices:
  -- i0 is the register for the b parameter
  -- ir is the register for the current component index
  signal i0, ir: u_unsigned(13 downto 0);
  -- Control signal
  signal freeze_ir_d, freeze_ir_q, freeze_ir: boolean;
  -- Inputs and outputs of the first adder
  signal i_add_1_in1, i_add_1_in2, i_add_1_out: u_unsigned(13 downto 0);
  signal i_add_1_cin: natural range 0 to 1;
  -- Inputs and outputs of the second adder
  signal i_add_2_in1, i_add_2_in2, i_add_2_out: u_unsigned(13 downto 0);
  signal i_add_2_cin: natural range 0 to 1;

  -- Signals related to components' byte addresses
  signal a0_in, a1_in, a1_tmp: u_unsigned(13 downto 0);
  signal en0_in, en1_in : std_ulogic_vector(0 to 7);

  -- Control and parameter signals
  signal load_1: boolean; -- load delayed by one cycle
  signal tr: u_unsigned(1 downto 0); -- Register for t parameter
  signal sr: std_ulogic;           -- Register for s parameter
  signal nr: u_unsigned(6 downto 0); -- Register for n parameter
  signal wr: u_unsigned(1 downto 0); -- Register for w parameter

begin

  pr_less_than_4 <= or_reduce(pr(13 downto 2)) = '0';   -- pr is less than 4
  pr_eq0 <= pr_less_than_4 and (pr(1 downto 0) = "00"); -- pr equal 0
  pr_eq1 <= pr_less_than_4 and (pr(1 downto 0) = "01"); -- pr equal 1
  pr_eq2 <= pr_less_than_4 and (pr(1 downto 0) = "10"); -- pr equal 2

  -- p_sub_out=(pr==1)?p-1:pr-2
  p_sub_in1 <= p0 when pr_eq1 else pr;
  p_sub_in2 <= 1 when pr_eq1 else 2;
  p_sub_out <= p_sub_in1 - p_sub_in2;

  -- freeze_pr_d, freeze_pr_q and freeze_pr are used to freeze the pr register
  -- when p==0 or p==1.
  freeze_pr_d <= pr_eq0 or pr_eq1; -- pr<=1
  freeze_pr <= (load_1 and freeze_pr_d) or freeze_pr_q;

  -- p-periodic counter. pr cycles from p downto 1 and back to p, two values at
  -- a time. Examples:
  -- p=0 => pr=0,0,0,0,...
  -- p=1 => pr=1,1,1,1,...
  -- p=2 => pr=2,2,2,2,...
  -- p=3 => pr=3,1,2,3,1,2,3,1,2,3,...
  -- p=4 => pr=4,2,4,2,4,2,...
  -- p=5 => pr=5,3,1,4,2,5,3,1,4,...
  p_pr: process(clk)
  begin
    if rising_edge(clk) then
      if ce = '1' then
        if load = '1' then -- Load parameter p
          p0        <= p;
          pr        <= p;
          freeze_pr_q <= false;
        else
          if load_1 then
            freeze_pr_q <= freeze_pr_d;
          end if;
          if run = '1' and (not freeze_pr) then -- Regular run and pr not frozen
            if pr_eq2 then -- pr=2
              pr <= p0; -- Cycle and start new p-period at beginning (pr=p)
            else -- Continue current p-period
              pr <= p_sub_out; -- pr=(pr==1)?p-1:pr-2
            end if;
          end if;
        end if;
      end if;
    end if;
  end process p_pr;

  mr_less_than_4 <= or_reduce(mr(7 downto 2)) = '0';   -- mr less than 4
  mr_eq0 <= mr_less_than_4 and (mr(1 downto 0) = "00"); -- mr equal 0
  mr_eq1 <= mr_less_than_4 and (mr(1 downto 0) = "01"); -- mr equal 1
  mr_eq2 <= mr_less_than_4 and (mr(1 downto 0) = "10"); -- mr equal 2

  -- m_sub_out=(pr==1||mr==1)?m-1:mr-2
  m_sub_in1 <= m0 when pr_eq1 or mr_eq1 else mr;
  m_sub_in2 <= 1 when pr_eq1 or mr_eq1 else 2;
  m_sub_out <= m_sub_in1 - m_sub_in2;

  -- freeze_mr_d, freeze_mr_q and freeze_mr are used to freeze the mr register
  -- when m==0.
  freeze_mr_d <= mr_eq0; -- mr==0
  freeze_mr <= (load_1 and freeze_mr_d) or freeze_mr_q;
                                             
  -- m-periodic counter. mr cycles from m downto 1 and back to m, two values at
  -- a time. It is re-initialized when the p-periodic counter wraps. There are
  -- two re-initialisation cases: pr=p (even wrapping, mr is set to m) and
  -- pr=p-1 (odd wrapping, mr is set to m-1). Examples (with p=0):
  -- m=0 => mr=0,0,0,0,...
  -- m=1 => mr=1,1,1,1,...
  -- m=2 => mr=2,2,2,2,...
  -- m=3 => mr=3,1,2,3,1,2,3,1,2,3,...
  -- m=4 => mr=4,2,4,2,4,2,...
  -- m=5 => mr=5,3,1,4,2,5,3,1,4,...
  -- Examples (with p=6):
  -- p=6 => pr=6,4,2,6,4,2,6,4,2,6,...
  -- m=0 => mr=0,0,0,0,...
  -- m=1 => mr=1,1,1,1,...
  -- m=2 => mr=2,2,2,2,...
  -- m=3 => mr=3,1,2,3,1,2,3,1,2,3,...
  -- m=4 => mr=4,2,4,4,2,4,4,2,4,4,...
  -- m=5 => mr=5,3,1,5,3,1,5,3,1,5,...
  -- Examples (with p=5):
  -- p=5 => pr=5,3,1,4,2,5,3,1,4,2,...
  -- m=0 => mr=0,0,0,0,...
  -- m=1 => mr=1,1,1,1,...
  -- m=2 => mr=2,2,2,1,1,2,2,2...
  -- m=3 => mr=3,1,2,2,3,3,1,2,2,3,...
  -- m=4 => mr=4,2,4,3,1,4,2,4,3,1,...
  -- m=5 => mr=5,3,1,4,2,5,3,1,4,2,...
  m_pr: process(clk)
  begin
    if rising_edge(clk) then
      if ce = '1' then
        if load = '1' then -- Load parameter m
          m0        <= m;
          mr        <= m;
          freeze_mr_q <= false;
        else
          if load_1 then -- One clock cycle after loading parameters, if m<=2
            freeze_mr_q <= freeze_mr_d;
          end if;
          if run = '1' and (not freeze_mr) then -- Regular run and mr not frozen
            if pr_eq2 then -- pr=2
              mr <= m0; -- Cycle and start new p-period at beginning (mr=m)
            elsif pr_eq1 then -- pr==1
              mr <= m_sub_out; -- Cycle and start new p-period at next after beginning (mr=m-1)
            elsif mr_eq2 then -- mr==2
              mr <= m0; -- Cycle and start new m-period at beginning (mr=m)
            else -- Continue current m-period
              mr <= m_sub_out; -- mr=(mr==1)?m-1:mr-2
            end if;
          end if;
        end if;
      end if;
    end if;
  end process m_pr;

  -- Arithmetic operations for components' indices. The FEP user guide defines
  -- the index increment as:
  -- inc=(-1)^s*n       when m==0
  -- inc=(-1)^s*(n+1/m) when m>1 (note that m==1 is invalid)
  --
  -- The following two adders are used to compute two indices at a time, in an
  -- incremental way. The ir register always stores idx(2i) (where idx(0)=b is
  -- the first index and is stored in the i0 register). The wrapping around Mio
  -- banks and banks subsections is implemented in later stages. The computation
  -- of the idx(2i) and idx(2i+1) is done modulo 2^14, ignoring the t and w
  -- parameters. The content of the ir register is used to compute the first
  -- output byte address a0.
  --
  -- From ir=idx(2i) the first adder computes idx(2i+1)=idx(2i)+(-1)^s*(n+d)
  -- where d=0 or d=1 is used to implement the 1/m fractional part of the
  -- increment; d is set to 1 when the m-periodic counter wraps from 1 to m-1,
  -- that is, when the mr register equals 1 (n==1):
  -- mr:        3  1  2  3  1  2
  -- idx(2i):   0  2  5  8 10 13
  -- idx(2i+1): 1  4  6  9 12 14
  -- d:         0  1  0  0  1  0
  --
  -- When the p-periodic counter wraps from 1 to p-1, idx(2i+1)=idx(0)+(-1)^s*n
  -- is computed instead. Note that in this case, as m!=1, d==0. idx(2i+1) is
  -- used to compute the second output byte address a1. It is also used to
  -- re-initialize the ir register when the p-periodic counter wraps from 1 to
  -- p-1; when the p-periodic counter wraps from 2 to p, ir is re-initialized to
  -- i0=b.
  --
  -- All in all, the first adder computes one of the following operations
  -- (modulo 2^14):
  -- (b) +(n)       +0 = b + n    when pr==1 and sr==0
  -- (b) +(2^14-n-1)+1 = b - n    when pr==1 and sr==1
  -- (ir)+(n)       +0 = ir+ n    when pr!=1 and sr==0 and mr!=1
  -- (ir)+(2^14-n-1)+1 = ir- n    when pr!=1 and sr==1 and mr!=1
  -- (ir)+(n)       +1 = ir+(n+1) when pr!=1 and sr==0 and mr==1
  -- (ir)+(2^14-n-1)+0 = ir-(n+1) when pr!=1 and sr==1 and mr==1
  --
  -- First input of first adder: (pr==1)?b:ir
  i_add_1_in1 <= i0 when pr_eq1 else ir;
  -- Second input of first adder: (s==0)?n:2^14-n-1
  i_add_1_in2 <= "0000000" & nr when sr = '0' else "1111111" & (not nr);
  -- Input carry of first adder
  i_add_1_cin <= 0 when sr = '0' and pr_eq1 else
                 1 when sr = '1' and pr_eq1 else
                 0 when sr = '0' and (not mr_eq1) else
                 1 when sr = '1' and (not mr_eq1) else
                 1 when sr = '0' and mr_eq1 else
                 0;
  -- First adder
  i_add_1_out <= i_add_1_in1 + i_add_1_in2 + i_add_1_cin;

  -- The second adder is used to compute the next value to store in ir:
  -- idx(2i+2)=idx(2i)+(-1)^s*(2*n+e) where e=0 or e=1 is used to implement the
  -- 1/m fractional part of the increment; e is set to 1 when the m-periodic
  -- counter wraps from 1 to m-1 or from 2 to m, that is, when the mr register
  -- equals 1 or 2 (n==1):
  -- mr:        3  1  2  3  1  2
  -- idx(2i):   0  2  5  8 10 13
  -- idx(2i+2): 2  5  8 10 13 16
  -- e:         0  1  1  0  1  1
  -- Note that because m!=1 there is no need for e to take value 2: from idx(2i)
  -- to idx(2i+1) there is at most one skipped value.
  --
  -- All in all, the second adder computes one of the following operations
  -- (modulo 2^14):
  -- (ir)+(2n)       +1 = ir+(2n+1) when sr==0 and (mr==1 or mr==2)
  -- (ir)+(2^14-2n-1)+0 = ir-(2n+1) when sr==1 and (mr==1 or mr==2)
  -- (ir)+(2n)       +0 = ir+ 2n    when sr==0 and mr!=1 and mr!=2
  -- (ir)+(2^14-2n-1)+1 = ir- 2n    when sr==1 and mr!=1 and mr!=2
  --
  -- First input of second adder: ir
  i_add_2_in1 <= ir;
  -- Second input of second adder: (s==0)?2n:2^14-2n-1
  i_add_2_in2 <= "000000" & nr & '0' when sr = '0' else "111111" & (not nr) & '1';
  -- Input carry of second adder
  i_add_2_cin <= 1 when sr = '0' and (mr_eq1 or mr_eq2) else
                 0 when sr = '1' and (mr_eq1 or mr_eq2) else
                 1 when sr = '1' else
                 0;
  -- Second adder
  i_add_2_out <= i_add_2_in1 + i_add_2_in2 + i_add_2_cin;

  -- freeze_ir_d, freeze_ir_q and freeze_ir are used to freeze the ir register
  -- when p==1.
  freeze_ir_d <= pr_eq1; -- pr==1
  freeze_ir <= (load_1 and freeze_ir_d) or freeze_ir_q;

  -- Indices registers. i0 stores the base index and is set at load time to
  -- parameter b. ir stores idx(2i) and is also set at load time to parameter b.
  -- freeze_ir is a boolean flag used to freeze the ir register when p==1
  -- (periodic indices sequence with period==1).
  i_pr: process(clk)
  begin
    if rising_edge(clk) then
      if ce = '1' then
        if load = '1' then -- Load parameters
          i0        <= b;
          ir        <= b;
          freeze_ir_q <= false;
        else
          if load_1 then -- One clock cycle after loading parameters
            freeze_ir_q <= freeze_ir_d; -- Registers will be frozen to their initial values.
          end if;
          if run = '1' and (not freeze_ir) then -- If running and not frozen
            if pr_eq1 then -- If pr==1 (p-periodic counter wraps from 1 to p-1)
              ir <= i_add_1_out; -- ir=b+inc
            elsif pr_eq2 then -- If pr==2 (p-periodic counter wraps from 2 to p)
              ir <= i0; -- ir=b
            else
              ir <= i_add_2_out; -- ir=idx(2i+2)
            end if;
          end if;
        end if;
      end if;
    end if;
  end process i_pr;

  -- a0_in is an input of the a0 output register. It is the byte-address version
  -- of the ir index register. Depending on the byte-width of the vector's
  -- components (parameter t, stored in register tr), we apply a left shift by
  -- 0, 1 or 2 positions:
  a0_in <= ir when tr = "00" else
           ir(11 downto 0) & "00" when tr = "11" else
           ir(12 downto 0) & '0';

  -- Multiplexor between i0=b and the output of the first adder; when the
  -- p-periodic counter wraps from 1 to p-1 (pr==1), the input of the a1 output
  -- register depends on b. In all other situations it depends on the output of
  -- the first adder, that is, idx(2i)+inc:
  a1_tmp <= i0 when pr_eq1 else i_add_1_out;
  -- As a0_in, a1_in is an input of the a1 output register. It is a left-shifted
  -- version of the component's index, by 0, 1 or 2 positions, depending on tr.
  a1_in <= a1_tmp when tr = "00" else
           a1_tmp(11 downto 0) & "00" when tr = "11" else
           a1_tmp(12 downto 0) & '0';

  -- Output enables
  en_p: process(tr, a0_in, a1_in)
  begin
    en0_in <= (others => '0');
    en1_in <= (others => '0');
    if tr = "11" then
      case a0_in(3 downto 2) is
        when "00" =>
          en0_in(0 to 1) <= "11";
        when "01" =>
          en0_in(2 to 3) <= "11";
        when "10" =>
          en0_in(4 to 5) <= "11";
        when others =>
          en0_in(6 to 7) <= "11";
      end case;
      case a1_in(3 downto 2) is
        when "00" =>
          en1_in(0 to 1) <= "11";
        when "01" =>
          en1_in(2 to 3) <= "11";
        when "10" =>
          en1_in(4 to 5) <= "11";
        when others =>
          en1_in(6 to 7) <= "11";
      end case;
    else
      case a0_in(3 downto 1) is
        when "000" =>
          en0_in(0) <= '1';
        when "001" =>
          en0_in(1) <= '1';
        when "010" =>
          en0_in(2) <= '1';
        when "011" =>
          en0_in(3) <= '1';
        when "100" =>
          en0_in(4) <= '1';
        when "101" =>
          en0_in(5) <= '1';
        when "110" =>
          en0_in(6) <= '1';
        when others =>
          en0_in(7) <= '1';
      end case;
      case a1_in(3 downto 1) is
        when "000" =>
          en1_in(0) <= '1';
        when "001" =>
          en1_in(1) <= '1';
        when "010" =>
          en1_in(2) <= '1';
        when "011" =>
          en1_in(3) <= '1';
        when "100" =>
          en1_in(4) <= '1';
        when "101" =>
          en1_in(5) <= '1';
        when "110" =>
          en1_in(6) <= '1';
        when others =>
          en1_in(7) <= '1';
      end case;
    end if;
  end process en_p;

  -- Manages the a0 and a1 output registers; Applies the wrapping around Mio
  -- banks and banks subsections (parameter w). Also manages the data strobe out
  -- signal that indicates valid outputs.
  a_pr: process(clk)
  begin
    if rising_edge(clk) then
      if ce = '1' then
        if load_1 then -- One cycle after loading parameters
          a0  <= a0_in; -- Initialize output registers to base byte address
          a1  <= a0_in; -- (that is, byte address of first component).
          en0 <= en0_in;
          en1 <= en0_in;
        end if;
        if run = '1' then -- If running
        -- Depending on size of wrapping sections (parameter w, stored in
        -- register wr), overwrite more or less LSBs of a0 and a1 with values
        -- coming from a0_in and a1_in
          case wr is
            when "00" => a0(10 downto 0) <= a0_in(10 downto 0);
                         a1(10 downto 0) <= a1_in(10 downto 0);
            when "01" => a0(11 downto 0) <= a0_in(11 downto 0);
                         a1(11 downto 0) <= a1_in(11 downto 0);
            when "10" => a0(12 downto 0) <= a0_in(12 downto 0);
                         a1(12 downto 0) <= a1_in(12 downto 0);
            when others => a0 <= a0_in;
                           a1 <= a1_in;
          end case;
          en0 <= en0_in;
          en1 <= en1_in;
        end if;
      end if;
    end if;
  end process a_pr;

  -- Control and parameters storage
  ctrl_pr: process(clk)
  begin
    if rising_edge(clk) then
      if ce = '1' then
        load_1 <= false; -- By default we are not one cycle after parameters loading
        if load = '1' then -- If parameters loading
          load_1 <= true; -- One-cycle-delayed load
          tr     <= t; -- Store parameter t in register tr
          sr     <= s; -- Store parameter t in register sr
          nr     <= n; -- Store parameter t in register nr
          wr     <= w; -- Store parameter t in register wr
        end if;
      end if;
    end if;
  end process ctrl_pr;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
