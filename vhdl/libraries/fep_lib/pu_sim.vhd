--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for pu

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;

library global_lib;
use ieee.numeric_std.all;
use global_lib.global.all;
use global_lib.sim_utils.all;

library random_lib;
use random_lib.rnd.all;

use work.type_pkg.all;
use work.pu_pkg.all;
use work.fep_pkg.all;
use work.fep_sim_pkg.all;

entity pu_sim is
  generic(n: positive := 1000000;
          debug: boolean := false);
end entity pu_sim;

architecture sim of pu_sim is

  signal clk: std_ulogic;
  signal srstn: std_ulogic;
  signal ce: std_ulogic;
  signal dsi: std_ulogic;
  signal di: pu_in;
  signal do: pu_out;

  signal eos: boolean;

begin

  ipu: entity work.pu(rtl)
  generic map(debug  => debug)
  port map(clk  => clk,
           srstn  => srstn,
           ce  => ce,
           dsi => dsi,
           di => di,
           do => do);

  clock: process
  begin
    clk <= '0';
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
    if eos then wait; end if;
  end process clock;

  stim: process
    variable l: line;
    variable o, p: natural;
    variable pi: pu_in;
  begin
    srstn  <= '0';
    ce  <= '0';
    di <= pu_di_none;
    dsi <= '0';
    wait until rising_edge(clk);
    for i in 1 to n loop
      p := i mod (n / 100);
      if p = 0 then
        write(l, (i * 100) / n);
        write(l, string'("%"));
        writeline(output, l);
      end if;
      if int_rnd(1, 100) = 1 then
        srstn <= '0';
      else
        srstn <= '1';
      end if;
      if int_rnd(1, 100) = 1 then
        ce <= '0';
      else
        ce <= '1';
      end if;
      dsi <= std_ulogic_rnd;
      pi := pu_rnd;
      if pi.op = cwl or pi.op = cwli or pi.op = cwle then
        pi.ay(1) := '0';
      end if;
      di <= pi;
      wait until rising_edge(clk);
    end loop;
    dsi  <= '0';
    ce  <= '1';
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    eos <= true;
    report "Regression test passed";
    wait;
  end process stim;

  check: process(clk)
    type pu_in_vector is array(natural range <>) of pu_in;
    variable dsi_pipe: std_ulogic_vector(1 to pu_pipe_depth) := (others => '0');
    variable di_pipe: pu_in_vector(1 to pu_pipe_depth);
    variable do_ref: pu_out;
    variable l: line;
    variable ok: boolean;
  begin
    if rising_edge(clk) then
      if dsi_pipe(pu_pipe_depth) = '1' then
        do_ref := pu_ref(di_pipe(pu_pipe_depth));
        if (di_pipe(pu_pipe_depth).op = ft or di_pipe(pu_pipe_depth).op = ftl2 or di_pipe(pu_pipe_depth).op = ftl4) then
          ok := do.u = do_ref.u;
        else
          ok := do.v = do_ref.v;
        end if;
        if not ok then
          print(di_pipe(pu_pipe_depth));
          print(l, "PU returned:");
          writeline(output, l);
          print(do);
          print(l, "should be:");
          writeline(output, l);
          print(do_ref);
          assert false
          severity failure;
        end if;
      end if;
      if srstn = '0' then
        dsi_pipe := (others => '0');
      elsif ce = '1' then
        dsi_pipe := dsi & dsi_pipe(1 to pu_pipe_depth - 1);
        di_pipe := di & di_pipe(1 to pu_pipe_depth - 1);
      end if;
    end if;
  end process check;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
