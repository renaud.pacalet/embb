#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

gh-IGNORE	+= fep_lib.rams fep_lib.fep fep_lib.fep_no_rams fep_lib.fep_sim fep_lib.top
gh-IGNORE	+= fep_lib.pss_sim fep_lib.pss_mss

fep_lib.bf: \
	fep_lib.type_pkg

fep_lib.bf_sim: \
	random_lib.rnd \
	fep_lib.type_pkg \
	fep_lib.pu_pkg \
	fep_lib.bf

fep_lib.dsp: \
	global_lib.global \
	fep_lib.type_pkg \
	fep_lib.pu_pkg

fep_lib.dsp_sim: \
	global_lib.global \
	global_lib.sim_utils \
	random_lib.rnd \
	fep_lib.type_pkg \
	fep_lib.pu_pkg \
	fep_lib.dsp

fep_lib.fep: \
	global_lib.global\
	fep_lib.fep_pkg \
	fep_lib.fep_no_rams \
	fep_lib.rams

fep_lib.fep_no_rams: \
	global_lib.global\
	css_lib.css_pkg \
	css_lib.css \
	fep_lib.bitfield_pkg \
	fep_lib.fep_pkg \
	fep_lib.mss \
	fep_lib.pss

fep_lib.fep_pkg: \
	global_lib.global \
	global_lib.utils \
	random_lib.rnd \
	memories_lib.ram_pkg \
	fep_lib.bitfield_pkg \
	fep_lib.type_pkg \
	fep_lib.errors

fep_lib.fep_pkg_sim: \
	random_lib.rnd \
	fep_lib.type_pkg \
	fep_lib.fep_pkg

fep_lib.fep_sim_pkg: \
	global_lib.sim_utils \
	fep_lib.type_pkg

fep_lib.fep_sim: \
	global_lib.global \
	global_lib.hst_emulator \
	fep_lib.fep_pkg \
	fep_lib.bitfield_pkg \
	fep_lib.fep

fep_lib.ft_address_generator_sim: \
	random_lib.rnd \
	fep_lib.type_pkg \
	fep_lib.fep_pkg \
	fep_lib.ftx_address_generator \
	fep_lib.ftz_address_generator

fep_lib.ftx_address_generator: \
	fep_lib.type_pkg \
	fep_lib.fep_pkg

fep_lib.ftz_address_generator: \
	fep_lib.type_pkg \
	fep_lib.fep_pkg

fep_lib.pss: \
	global_lib.global \
	global_lib.utils \
	memories_lib.ram_pkg \
	fep_lib.bitfield_pkg \
	fep_lib.type_pkg \
	fep_lib.fep_pkg \
	fep_lib.pu_pkg \
	fep_lib.vpxy_address_generator \
	fep_lib.vpz_address_generator \
	fep_lib.ftx_address_generator \
	fep_lib.ftz_address_generator \
	fep_lib.transpose \
	fep_lib.twd_address_generator \
	fep_lib.pu \
	fep_lib.sma

fep_lib.pss_mss: \
	global_lib.global\
	fep_lib.bitfield_pkg \
	fep_lib.fep_pkg \
	fep_lib.mss \
	fep_lib.rams \
	fep_lib.pss

fep_lib.pss_sim: \
	global_lib.global \
	global_lib.css_emulator \
	memories_lib.ram_pkg \
	fep_lib.type_pkg \
	fep_lib.fep_pkg \
	fep_lib.mss \
	fep_lib.rams \
	fep_lib.pu \
	fep_lib.pss

fep_lib.mss: \
	global_lib.global \
	global_lib.utils \
	memories_lib.ram_pkg \
	fep_lib.type_pkg \
	fep_lib.fep_pkg

fep_lib.pu: \
	global_lib.global \
	global_lib.utils \
	fep_lib.type_pkg \
	fep_lib.pu_pkg \
	fep_lib.fep_pkg \
	fep_lib.dsp \
	fep_lib.bf

fep_lib.pu_pkg: \
	global_lib.utils \
	global_lib.sim_utils \
	random_lib.rnd \
	fep_lib.fep_sim_pkg \
	fep_lib.fep_pkg \
	fep_lib.type_pkg

fep_lib.pu_sim: \
	global_lib.global \
	global_lib.sim_utils \
	random_lib.rnd \
	fep_lib.fep_sim_pkg \
	fep_lib.type_pkg \
	fep_lib.pu_pkg \
	fep_lib.pu

fep_lib.rams: \
	memories_lib.ram_pkg \
	memories_lib.sdpram \
	memories_lib.tdpram \
	fep_lib.type_pkg \
	fep_lib.fep_pkg \
	fep_lib.twd

fep_lib.sma: \
	global_lib.utils \
	global_lib.sim_utils \
	fep_lib.type_pkg \
	fep_lib.fep_pkg

fep_lib.top: \
	global_lib.global \
	global_lib.utils \
	fep_lib.fep_pkg \
	fep_lib.fep

fep_lib.transpose: \
	global_lib.global \
	fep_lib.type_pkg \
	fep_lib.fep_pkg

fep_lib.twd: \
	global_lib.global \
	memories_lib.ram_pkg \
	fep_lib.type_pkg \
	fep_lib.fep_pkg \
	fep_lib.twd_pkg

fep_lib.twd_address_generator: \
	global_lib.utils \
	fep_lib.type_pkg \
	fep_lib.fep_pkg \
	fep_lib.pu_pkg

fep_lib.twd_address_generator_sim: \
	random_lib.rnd \
	fep_lib.type_pkg \
	fep_lib.fep_pkg \
	fep_lib.twd_address_generator

fep_lib.twd_pkg: \
	fep_lib.type_pkg

fep_lib.vpxy_address_generator: \
	global_lib.utils \
	fep_lib.type_pkg

fep_lib.vpxy_address_generator_sim: \
	global_lib.sim_utils \
	random_lib.rnd \
	fep_lib.vpxy_address_generator \
	fep_lib.type_pkg

fep_lib.vpz_address_generator: \
	global_lib.utils \
	fep_lib.type_pkg

fep_lib.vpz_address_generator_sim: \
	global_lib.sim_utils \
	random_lib.rnd \
	fep_lib.vpz_address_generator

# Default input and output pipeline depths of MSS.
FEP_LIB_N0	?= 2
FEP_LIB_N1	?= 2

# Simulations

fep_lib_csrc		:= $(hwprj_db_path.fep_lib)/C
# Name of command file for PSS simulation
fep_lib_c_cmdfile	= $(fep_lib_csrc)/c_fep.cmd
# Name of command file for full FEP simulation
fep_lib_cmdfile		= $(fep_lib_csrc)/fep.cmd
# Number of operations to simulate
FEP_LIB_CMDNUM		= 100
# Seven-bits mask of allowed operations: FT,CWL,MOV,CWS,CWM,CWP,CWA
FEP_LIB_OPERATIONS	= 4                                          
# Name of command file generator
FEP_LIB_CMDGEN		= mkcmdfile
fep_lib_cmdgen		= $(fep_lib_csrc)/$(FEP_LIB_CMDGEN)
# Options for command file generation
FEP_LIB_CMDGENFLAGS	= --tests=$(FEP_LIB_CMDNUM) --operations=$(FEP_LIB_OPERATIONS)
# Target technology
FEP_LIB_TARGET		?= 1
# Generic parameters for full FEP simulation
FEP_LIB_DEBUG		= false
FEP_LIB_VERBOSE		= false

ms-com.fep_lib ms-com.fep_lib%: MSCOMFLAGS += +acc=v -novopt -O0

ms-sim.fep_lib.bf_sim: MSSIMFLAGS += -Gdebug=$(FEP_LIB_DEBUG) -Gn=10000 -quiet -nostdout -logfile ms-sim.fep_lib.bf_sim.log
ms-sim.fep_lib.fep_sim: $(fep_lib_cmdfile)
ms-sim.fep_lib.fep_sim: MSSIMFLAGS += -Gcmdfile="$(fep_lib_cmdfile)" -Gdebug=$(FEP_LIB_DEBUG) -Gverbose=$(FEP_LIB_VERBOSE)
ms-sim.fep_lib.pss_sim: $(fep_lib_c_cmdfile)
ms-sim.fep_lib.pss_sim: MSSIMFLAGS += -Gcmdfile="$(fep_lib_c_cmdfile)"
$(addprefix ms-sim.fep_lib.,fep_sim pss_sim): MSSIMFLAGS += -Gn0=$(FEP_LIB_N0) -Gn1=$(FEP_LIB_N1)
$(addprefix ms-sim.fep_lib.,dsp_sim pu_sim pss_sim fep_sim): MSSIMFLAGS += -Gtarget=$(FEP_LIB_TARGET)
ms-sim.fep_lib.pu_sim: MSSIMFLAGS += -Gdebug=true
ms-sim.fep_lib.%: MSSIMFLAGS += -do 'run -all; quit'
ms-sim.fep_lib: $(addprefix ms-sim.fep_lib.,bf_sim dsp_sim fep_pkg_sim fep_sim pss_sim pu_sim vpxy_address_generator_sim vpz_address_generator_sim)
ms-sim: ms-sim.fep_lib

.PHONY: $(fep_lib_cmdgen)

$(fep_lib_c_cmdfile): $(fep_lib_cmdgen)
	$< --core $(FEP_LIB_CMDGENFLAGS) $@

$(fep_lib_cmdfile): $(fep_lib_cmdgen)
	$< $(FEP_LIB_CMDGENFLAGS) $@

$(fep_lib_cmdgen):
	$(MAKE) -C $(dir $@) $(notdir $@)

# Synthesis
$(addprefix pr-syn.fep_lib.,fep fep_no_rams pss pss_mss top): $(hwprj_db_path.fep_lib)/bitfield_pkg.vhd
$(addprefix pr-syn.fep_lib.,fep fep_no_rams top): $(hwprj_db_path.css_lib)/bitfield_pkg.vhd
pr-syn.fep_lib: $(addprefix pr-syn.fep_lib.,fep fep_no_rams pss pss_mss top)
pr-syn: pr-syn.fep_lib

$(addprefix rc-syn.fep_lib.,fep_no_rams pss): $(hwprj_db_path.fep_lib)/bitfield_pkg.vhd
$(addprefix rc-syn.fep_lib.,fep_no_rams): $(hwprj_db_path.css_lib)/bitfield_pkg.vhd
rc-syn.fep_lib: $(addprefix rc-syn.fep_lib.,fep_no_rams pss)
rc-syn: rc-syn.fep_lib

hw-clean: fep_lib_clean

fep_lib_clean:
	$(MAKE) -C $(fep_lib_csrc) clean
	rm -rf $(fep_lib_cmdfile) $(fep_lib_c_cmdfile)

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
