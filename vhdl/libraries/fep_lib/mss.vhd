--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief FEP's memory subsystem.
--*
--* The FEP's memory subsystem is made of 33 physical RAMs:
--* - UCR is a 1kx32 RAM (2 RAMB18).
--* - MIOrc (0<=r<4, 0<=c<8) are 4*8=32 1kx16 RAMs (1 RAMB18 each), organized in
--*   four rows and eight columns.
--* The mapping of these RAMs in the address space of their 4 clients (VCI, DMA,
--* UC and PSS) is the following:
--* - DMA and VCI addresses are double-words (64 bits) addresses. They access MIO
--* and UCR. They see MIO as a contiguous 8kx64 bits memory and they access it
--* through one read-or-write port only (port B). They are prevented from
--* accessing MIO whenever PSS uses port B of the sub-bank they target. VCI can
--* also be prevented by DMA which priority is higher. Addresses range from
--* 0x0000 to 0x1FFF. Addresses 0x0000 to 0x07FF correspond to the MIO first
--* bank. DMA and VCI see UCR as a 512x64 bits memory which addresses range from
--* 0x2000 to 0x21FF. They access UCR using two ports and are thus prevented from
--* accessing it whenever UC reads or write.
--* - PSS has access to MIO only. It sees MIO as a set of 4 banks, 1kx128 bits
--* each. It reads and writes through 2 ports per bank. Addresses are quad-words
--* (128 bits) addresses. In the each bank addresses range from 0x000 to 0x3FF.
--* Address 0x000 in the first bank corresponds to addresses 0x0000 (MSBs) and
--* 0x0001 (LSBs) of DMA and VCI.
--* - UC accesses UCR only, at addresses 0x0000 to 0x0FFF, 64 bits at a time, using both ports. Byte address 0x0000
--* corresponds to the MSB of DMA/VCI address 0x2000.
--*
--* PSS REQUESTS
--* PSS requests (highest priority) are always served. PSS has no access to UCR, it
--* reads and writes in MIOx and TMPx. In FT mode PSS uses both ports of a MIO
--* bank to read or write 128 bits at a time.
--*
--* UC REQUESTS
--*
--* UC requests (highest priority after PSS) are always served. UC address range
--* is limited to UCR area (4 k-bytes). UC accesses block RAMs 8 bytes at a
--* time. So, the write enables are used to write the target bytes only. The
--* address mapping on UC point of view is:
--* - 0x0000-0x0fff: UCR
--*
--* The out-of-range (oor) flag is raised whenever UC requests at other
--* addresses. When accessing UCR UC uses both ports of UCR.
--*
--* DMA REQUESTS
--*
--* DMA requests (highest priority after PSS and UC) are not always served.
--* They can be prevented by accesses to block RAMs by PSS or UC. DMA
--* accesses block RAMs 64 bits (8 bytes) at a time. The (byte) address mapping on
--* DMA point of view is:
--* - 0x00000-0x0ffff: Mio
--* - 0x10000-0x10fff: UCR
--*
--* When DMA accesses UCR it uses both ports (2 x 32 bits = 64 bits).
--* Port A is used to read/write the 32 MSBs at the even word address and
--* port B is used to read/write the 32 LSBs at the odd word address. When
--* accessing MIO it targets four consecutive block RAMs in a row and uses
--* port B of each. Out of range accesses raise an error during
--* simulation and set the "oor" field of mss2dma. DMA also provides
--* individual, active high, byte enables. This version uses these byte
--* enables when writting but it does not take them into account to reduce
--* the conflict situations: even when reading or writing a single byte, the
--* DMA request will be blocked until all the ports needed to read or write
--* the 8 bytes are available. Future versions could change this.
--*
--* VCI REQUESTS
--*
--* VCI accesses are exactly the same as DMA accesses. The only difference is
--* that VCI has the lowest priority.

-- pragma translate_off
use std.textio.all;
-- pragma translate_on

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use ieee.numeric_std.all;
use global_lib.utils.all;
use global_lib.global.all;

library memories_lib;
use memories_lib.ram_pkg.all;
-- defines the decode_we utility functions, the clients and ports enumerated
-- types and the record types to interface with the block RAMs

use work.type_pkg.all;
use work.fep_pkg.all;
-- defines the custom pss2mss_type and mss2pss_type types

entity mss is
  generic(n0: positive := 1;  --* Number of input pipeline registers in MSS, including input registers of RAMs
          n1: positive := 1); --* Number of output pipeline registers in MSS, including output registers of RAMs
  port(clk:      in  std_ulogic;
       -- Control subsystem interface (VCIInterface, DMA ans UC)
       css2mss:  in  css2mss_type;
       mss2css:  out mss2css_type;
       -- PSS interface
       pss2mss:   in  pss2mss_type;
       mss2pss:   out mss2pss_type;
       rams2mss: in  rams2mss_type;
       mss2rams: out mss2rams_type);
end entity mss;

architecture rtl of mss is

  constant sqrt2  : std_ulogic_vector(15 downto 0) := X"5A82";

  alias vci2mss is css2mss.vci2mss;
  alias mss2vci is mss2css.mss2vci;
  alias dma2mss is css2mss.dma2mss;
  alias mss2dma is mss2css.mss2dma;
  alias uc2mss  is css2mss.uc2mss;
  alias mss2uc  is mss2css.mss2uc;

  signal ucr_in: ucr_in_type; -- Input ports of the UCR RAM

  signal ucr_out: ucr_out_type; -- Output ports of the UCR RAM

  type mio_req is array(0 to 7) of in_port_1kx16;
  signal mio_in: mio_in_type; -- Input ports of the 32 MIO RAMs

  signal mio_out: mio_out_type; -- Output ports of the 32 MIO RAMs

  type banks_type is (ucr, mio); -- The 4 RAM sets

  -- The source of a 64 bits read by DMA (or VCI) can be:
  -- + ports A and B of the UCR RAM (r and c are ignored)
  -- + ports B of four consecutive MIO RAMs (pointed to by 4*c, 4*c+1, 4*c+2 and
  --   2*c+3) in a row (pointed to by r).
  type dma_vci_mux_type is record
    b: banks_type;
    r: natural range 0 to 3;
    c: natural range 0 to 1;
    be: word64_be_type;
  end record;
  type mux_type is record
    dma: dma_vci_mux_type;
    vci: dma_vci_mux_type;
    uc: word64_be_type;
  end record;
  type mux_pipe_type is array(1 to n0 + n1) of mux_type;
  signal mux_d, mux : mux_type;
  signal mux_pipe: mux_pipe_type;

  signal pss2mio: pss2mio_type;
  signal mio2pss: mio2pss_type;

  signal mss2rams_pipe: mss2rams_vector(1 to n0);
  signal rams2mss_pipe: rams2mss_vector(1 to n1);

begin

  pss2mio <= pss2mss.mio;

  -- The arbiter combinatorial process computes the different requests each
  -- client sends to each block RAM. It then enables only one client per port
  -- according to the fixed priority: PSS first, then UC, DMA and VCI. The
  -- arbiter also prepares the commands of the multiplexors that select block
  -- RAMs outputs to send to the clients.
  arbiter: process(css2mss, pss2mio)

    variable mio_in_v, pss_mio_req: mio_in_type;    -- Input ports of the 32 MIO RAMs
    variable dma_mio_req, vci_mio_req: mio_req;
    variable ucr_in_v: ucr_in_type;   -- Input ports of the UCR RAM

    variable n, col: natural range 0 to 7;
    -- Block RAM designator and MIO column designator
    variable row, dma_row, vci_row:  natural range 0 to 3;
    -- MIO row designator

    variable uc_ucr_access, dma_mio_access, dma_ucr_access, vci_mio_access, vci_ucr_access, uc_oor, dma_oor, vci_oor: boolean;

  begin

    -------------------------------------------------------
    ------------------ INITIALIZATION ---------------------
    -------------------------------------------------------

    for p in 0 to 1 loop -- for all ports (A and B)
      ucr_in_v(p) := in_port_1kx32_default; -- the UCR RAM
      -- Initialize the input ports to default (no request).
      for r in 0 to 3 loop -- for all MIO RAMs
        for c in 0 to 7 loop
          mio_in_v(r, c, p) := in_port_1kx16_default;
          -- Initialize the input ports to default (no request).
        end loop;
      end loop;
    end loop;

    uc_ucr_access := uc2mss.en = '1' and uc2mss.add(15 downto 12) = "0000";
    mss2uc.gnt <= (others => '1');  -- by default, grant UC's request
    mss2uc.oor <= '0';  -- by default we expect UC not to access out of range memory locations
    uc_oor := or_reduce(uc2mss.add(15 downto 12)) = '1';
    mux_d.uc <= (others => '0');

    dma_mio_access := dma2mss.en = '1' and dma2mss.add(13) = '0';
    dma_ucr_access := dma2mss.en = '1' and dma2mss.add(13) = '1';
    mss2dma.gnt <= (others => '0'); -- by default, don't grant DMA's request
    mss2dma.oor <= '0'; -- by default we expect DMA not to access out of range memory locations
    mux_d.dma <= (b => mio, r => 0, c => 0, be => (others => '0'));
    dma_row := to_integer(u_unsigned(dma2mss.add(12 downto 11)));
    dma_oor := (or_reduce(dma2mss.add(28 downto 14)) = '1') or ((dma2mss.add(13) = '1') and (or_reduce(dma2mss.add(12 downto 9)) = '1'));

    vci_mio_access := vci2mss.en = '1' and vci2mss.add(13) = '0';
    vci_ucr_access := vci2mss.en = '1' and vci2mss.add(13) = '1';
    mss2vci.gnt <= (others => '0'); -- by default, don't grant VCI's request
    mss2vci.oor <= '0'; -- by default we expect VCI not to access out of range memory locations
    mux_d.vci <= (b => mio, r => 0, c => 0, be => (others => '0'));
    vci_row := to_integer(u_unsigned(vci2mss.add(12 downto 11)));
    vci_oor := (or_reduce(vci2mss.add(28 downto 14)) = '1') or ((vci2mss.add(13) = '1') and (or_reduce(vci2mss.add(12 downto 9)) = '1'));

    -- Mio accesses
    -- PSS
    for r in 0 to 3 loop
      for p in 0 to 1 loop
        for c in 0 to 7 loop
          pss_mio_req(r, c, p).en    := pss2mio.en(r, c, p);
          if c = 0 then
            pss_mio_req(r, c, p).add   := pss2mio.add(r, 0, p);
          else
            pss_mio_req(r, c, p).add   := pss2mio.add(r, 1, p);
          end if;
          pss_mio_req(r, c, p).we    := band((not pss2mio.rnw(r)), pss2mio.be(c, p));
          pss_mio_req(r, c, p).wdata := pss2mio.wdata(p)(127 - 16 * c downto 112 - 16 * c);
        end loop;
      end loop;
    end loop;
    -- DMA
    col := to_integer(u_unsigned(dma2mss.add(0 downto 0)));
    for ch in 0 to 1 loop
      for cl in 0 to 3 loop
        if col = ch then
          dma_mio_req(4 * ch + cl).en    := '1';
        else
          dma_mio_req(4 * ch + cl).en    := '0';
        end if;
        dma_mio_req(4 * ch + cl).we    := band((not dma2mss.rnw), dma2mss.be(7 - cl * 2 downto 6 - cl * 2));
        dma_mio_req(4 * ch + cl).add   := dma2mss.add(10 downto 1);
        dma_mio_req(4 * ch + cl).wdata := dma2mss.wdata(63 - cl * 16 downto 48 - cl * 16);
        if dma2mss.rnw = '1' then
          mux_d.dma.be(7 - cl * 2 downto 6 - cl * 2) <= dma2mss.be(7 - cl * 2 downto 6 - cl * 2);
        end if;
      end loop;
    end loop;
    -- VCI
    col := to_integer(u_unsigned(vci2mss.add(0 downto 0)));
    for ch in 0 to 1 loop
      for cl in 0 to 3 loop
        if col = ch then
          vci_mio_req(4 * ch + cl).en    := '1';
        else
          vci_mio_req(4 * ch + cl).en    := '0';
        end if;
        vci_mio_req(4 * ch + cl).we    := band((not vci2mss.rnw), vci2mss.be(7 - cl * 2 downto 6 - cl * 2));
        vci_mio_req(4 * ch + cl).add   := vci2mss.add(10 downto 1);
        vci_mio_req(4 * ch + cl).wdata := vci2mss.wdata(63 - cl * 16 downto 48 - cl * 16);
      end loop;
    end loop;
    -- Requests selection
    for r in 0 to 3 loop
      if pss2mio.lock(r) = '1' then
        for c in 0 to 7 loop
          for p in 0 to 1 loop
            mio_in_v(r, c, p) := pss_mio_req(r, c, p);
          end loop;
        end loop;
      else
        if r = dma_row and dma_mio_access then -- MIO access
          col         := to_integer(u_unsigned(dma2mss.add(0 downto 0)));
          mux_d.dma   <= (b => mio, r => r, c => col, be => band(dma2mss.be, dma2mss.rnw));
          mss2dma.gnt <= (others => '1');
          for c in 0 to 7 loop
            mio_in_v(r, c, 1) := dma_mio_req(c);
          end loop;
        elsif r = vci_row and vci_mio_access then -- MIO access
          col         := to_integer(u_unsigned(vci2mss.add(0 downto 0)));
          mux_d.vci   <= (b => mio, r => r, c => col, be => band(vci2mss.be, vci2mss.rnw));
          mss2vci.gnt <= (others => '1');
          for c in 0 to 7 loop
            mio_in_v(r, c, 1) := vci_mio_req(c);
          end loop;
        end if;
      end if;
    end loop;

    -- UCR accesses
    if uc_ucr_access then -- if UC read/write channel enabled and access to the UCR area
      mss2uc.gnt        <= (others => '1'); -- grant the access
      -- Port A
      ucr_in_v(0).en    := '1';
      ucr_in_v(0).we    := band((not uc2mss.rnw), uc2mss.be(7 downto 4));
      ucr_in_v(0).add   := uc2mss.add(11 downto 3) & '0';
      ucr_in_v(0).wdata := uc2mss.wdata(63 downto 32);
      -- Port B
      ucr_in_v(1).en    := '1';
      ucr_in_v(1).we    := band((not uc2mss.rnw), uc2mss.be(3 downto 0));
      ucr_in_v(1).add   := uc2mss.add(11 downto 3) & '1';
      ucr_in_v(1).wdata := uc2mss.wdata(31 downto 0);
      if uc2mss.rnw = '1' then
        mux_d.uc <= uc2mss.be;
      end if;
    elsif dma_ucr_access then --UCR access
      mux_d.dma      <= (b => ucr, r => 0, c => 0, be => band(dma2mss.be, dma2mss.rnw));
      mss2dma.gnt    <= (others => '1');
      -- port a
      ucr_in_v(0).en    := '1';
      ucr_in_v(0).we    := band((not dma2mss.rnw), dma2mss.be(7 downto 4));
      ucr_in_v(0).wdata := dma2mss.wdata(63 downto 32);
      ucr_in_v(0).add   := dma2mss.add(8 downto 0) & '0';
      -- port b
      ucr_in_v(1).en    := '1';
      ucr_in_v(1).we    := band((not dma2mss.rnw), dma2mss.be(3 downto 0));
      ucr_in_v(1).wdata := dma2mss.wdata(31 downto 0);
      ucr_in_v(1).add   := dma2mss.add(8 downto 0) & '1';
    elsif vci_ucr_access then --UCR access
      mux_d.vci      <= (b => ucr, r => 0, c => 0, be => band(vci2mss.be, vci2mss.rnw));
      mss2vci.gnt    <= (others => '1');
      mux_d.vci      <= (b => ucr, r => 0, c => 0, be => band(vci2mss.be, vci2mss.rnw));
      -- Port A
      ucr_in_v(0).en    := '1';
      ucr_in_v(0).we    := band((not vci2mss.rnw), vci2mss.be(7 downto 4));
      ucr_in_v(0).wdata := vci2mss.wdata(63 downto 32);
      ucr_in_v(0).add   := vci2mss.add(8 downto 0) & '0';
      -- Port B
      ucr_in_v(1).en    := '1';
      ucr_in_v(1).we    := band((not vci2mss.rnw), vci2mss.be(3 downto 0));
      ucr_in_v(1).wdata := vci2mss.wdata(31 downto 0);
      ucr_in_v(1).add   := vci2mss.add(8 downto 0) & '1';
    end if;

    -- Out of range
    if uc_oor then
      mss2uc.oor <= '1';
      -- pragma translate_off
      assert uc2mss.en = '0'
        report "UC: out of range memory access (" & integer'image(to_integer(u_unsigned(uc2mss.add))) & ")"
        severity error;
      -- pragma translate_on
    end if;
    if dma_oor then
      mss2dma.oor <= '1';
      -- pragma translate_off
      assert dma2mss.en = '0'
        report "DMA: out of range memory access (" & integer'image(8 * to_integer(u_unsigned(dma2mss.add))) & ")"
        severity error;
      -- pragma translate_on
    end if;
    if vci_oor then
      mss2vci.oor <= '1';
      -- pragma translate_off
      assert vci2mss.en = '0'
        report "VCI: out of range memory access (" & integer'image(8 * to_integer(u_unsigned(vci2mss.add))) & ")"
        severity error;
      -- pragma translate_on
    end if;

    mio_in  <= mio_in_v;
    ucr_in  <= ucr_in_v;

  end process arbiter;

  mss2rams_pipe(1) <= (twd => pss2mss.twd, tmp => pss2mss.tmp, ucr => ucr_in, mio => mio_in);
  gn0: if n0 > 1 generate
    process(clk)
    begin
      if rising_edge(clk) then
        mss2rams_pipe(2 to n0) <= mss2rams_pipe(1 to n0 - 1);
      end if;
    end process;
  end generate gn0;
  rams2mss_pipe(1) <= rams2mss;
  gn1: if n1 > 1 generate
    process(clk)
    begin
      if rising_edge(clk) then
        rams2mss_pipe(2 to n1) <= rams2mss_pipe(1 to n1 - 1);
      end if;
    end process;
  end generate gn1;

  -- The regs process implements the sequential part:
  -- + registers to store the commands of the output multiplexers that select
  --   the "rdata" fields of UC, DMA and VCI and the read channels of PSS (mio"
  --   and "tmp").
  -- + input and output pipeline stages around RAMs (but last input and first output which are part of RAMs)
  regs: process(clk)
  begin

    if rising_edge(clk) then
      mux_pipe <= mux_d & mux_pipe(1 to n0 + n1 - 1);
    end if;
  end process regs;

  mss2rams <= mss2rams_pipe(n0);

  mux <= mux_pipe(n0 + n1);

  ucr_out <= rams2mss_pipe(n1).ucr;
  mio_out <= rams2mss_pipe(n1).mio;

  -- The muxes process implements the multiplexing of block RAM outputs
  muxes: process(mux, mio_out, ucr_out)
  begin

    -- PSS read data mux

    for r in 0 to 3 loop
      for c in 0 to 7 loop
        for p in 0 to 1 loop
          mio2pss(r, p)(127 - 16 * c downto 112 - 16 * c) <= mio_out(r, c, p);
        end loop;
      end loop;
    end loop;

     -- UC read data mux

    mss2uc.rdata  <= ucr_out(0) & ucr_out(1);
    mss2uc.be    <= mux.uc;

    -- DMA read data mux

    case mux.dma.b is
      when mio =>
        for c in 0 to 3 loop
          mss2dma.rdata(63 - c * 16 downto 48 - c * 16) <= mio_out(mux.dma.r, 4 * mux.dma.c + c, 1);
        end loop;
      when ucr =>
        mss2dma.rdata  <= ucr_out(0) & ucr_out(1);
    end case;
    mss2dma.be <= mux.dma.be;

    case mux.vci.b is
      when mio =>
        for c in 0 to 3 loop
          mss2vci.rdata(63 - c * 16 downto 48 - c * 16) <= mio_out(mux.vci.r, 4 * mux.vci.c + c, 1);
        end loop;
      when ucr =>
        mss2vci.rdata  <= ucr_out(0) & ucr_out(1);
    end case;
    mss2vci.be <= mux.vci.be;

  end process;

  mss2pss <= (mio => mio2pss, twd => rams2mss_pipe(n1).twd, tmp => rams2mss_pipe(n1).tmp);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
