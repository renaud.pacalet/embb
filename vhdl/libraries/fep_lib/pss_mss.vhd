--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Sub-part of FEP (PSS and MSS) for syntesis tests only.

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;
use ieee.numeric_std.all;

use work.fep_pkg.all;  -- defines the custom pss2mss_type and mss2pss_type types
use work.bitfield.all; -- defines bitfield_width and bitfield_padmask constants

entity pss_mss is
  generic(
    cmdsize:  positive := bitfield_width / 8; -- byte width of the command register
    n0:       positive;
    n1:       positive);
  port(clk:     in  std_ulogic;  -- master clock
       css2pss:  in  css2pss_type;
       param:   in  std_ulogic_vector(cmdsize * 8 - 1 downto 0);
       pss2css:  out pss2css_type;
       css2mss: in  css2mss_type;
       mss2css: out mss2css_type);
end entity pss_mss;

architecture rtl of pss_mss is

  signal pss2mss:  pss2mss_type;
  signal mss2pss:  mss2pss_type;
  signal rams2mss: rams2mss_type;
  signal mss2rams: mss2rams_type;

begin

  i_pss: entity work.pss(rtl)
  generic map(
    mss_pipeline_depth => n0 + n1)
  port map(
    clk    => clk,
    css2pss => css2pss,
    param  => param,
    pss2css => pss2css,
    pss2mss => pss2mss,
    mss2pss => mss2pss);

  i_mss: entity work.mss(rtl)
  generic map(
    n0 => n0,
    n1 => n1)
  port map(
    clk       => clk,
    css2mss   => css2mss,
    mss2css   => mss2css,
    pss2mss    => pss2mss,
    mss2pss    => mss2pss,
    rams2mss  => rams2mss,
    mss2rams  => mss2rams);

  i_rams: entity work.rams(rtl)
  port map(
    clk       => clk,
    rams2mss  => rams2mss,
    mss2rams  => mss2rams);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
