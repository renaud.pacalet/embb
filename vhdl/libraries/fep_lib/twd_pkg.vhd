--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Twiddle factors constant definition

library ieee;
use ieee.std_logic_1164.all;

use work.type_pkg.all;

package twd_pkg is

  constant twd_factors: v32_vector(0 to 511) := (
    X"7fff0000", X"7fff0032", X"7fff0064", X"7fff0096", X"7fff00c9", X"7fff00fb", X"7ffe012d", X"7ffe015f",
    X"7ffd0192", X"7ffc01c4", X"7ffc01f6", X"7ffb0228", X"7ffa025b", X"7ff9028d", X"7ff802bf", X"7ff702f1",
    X"7ff60324", X"7ff40356", X"7ff30388", X"7ff203ba", X"7ff003ed", X"7fee041f", X"7fed0451", X"7feb0483",
    X"7fe904b6", X"7fe704e8", X"7fe5051a", X"7fe3054c", X"7fe1057f", X"7fdf05b1", X"7fdd05e3", X"7fda0615",
    X"7fd80647", X"7fd6067a", X"7fd306ac", X"7fd006de", X"7fce0710", X"7fcb0742", X"7fc80775", X"7fc507a7",
    X"7fc207d9", X"7fbf080b", X"7fbc083d", X"7fb8086f", X"7fb508a2", X"7fb108d4", X"7fae0906", X"7faa0938",
    X"7fa7096a", X"7fa3099c", X"7f9f09ce", X"7f9b0a00", X"7f970a33", X"7f930a65", X"7f8f0a97", X"7f8b0ac9",
    X"7f870afb", X"7f820b2d", X"7f7e0b5f", X"7f790b91", X"7f750bc3", X"7f700bf5", X"7f6b0c27", X"7f670c59",
    X"7f620c8b", X"7f5d0cbd", X"7f580cef", X"7f530d21", X"7f4d0d53", X"7f480d85", X"7f430db7", X"7f3d0de9",
    X"7f380e1b", X"7f320e4d", X"7f2d0e7f", X"7f270eb1", X"7f210ee3", X"7f1b0f15", X"7f150f47", X"7f0f0f79",
    X"7f090fab", X"7f030fdd", X"7efd100e", X"7ef61040", X"7ef01072", X"7ee910a4", X"7ee310d6", X"7edc1108",
    X"7ed51139", X"7ecf116b", X"7ec8119d", X"7ec111cf", X"7eba1201", X"7eb31232", X"7eab1264", X"7ea41296",
    X"7e9d12c8", X"7e9512f9", X"7e8e132b", X"7e86135d", X"7e7f138e", X"7e7713c0", X"7e6f13f2", X"7e671423",
    X"7e5f1455", X"7e571487", X"7e4f14b8", X"7e4714ea", X"7e3f151b", X"7e37154d", X"7e2e157f", X"7e2615b0",
    X"7e1d15e2", X"7e141613", X"7e0c1645", X"7e031676", X"7dfa16a8", X"7df116d9", X"7de8170a", X"7ddf173c",
    X"7dd6176d", X"7dcd179f", X"7dc317d0", X"7dba1802", X"7db01833", X"7da71864", X"7d9d1896", X"7d9418c7",
    X"7d8a18f8", X"7d80192a", X"7d76195b", X"7d6c198c", X"7d6219bd", X"7d5819ef", X"7d4e1a20", X"7d431a51",
    X"7d391a82", X"7d2f1ab3", X"7d241ae4", X"7d191b16", X"7d0f1b47", X"7d041b78", X"7cf91ba9", X"7cee1bda",
    X"7ce31c0b", X"7cd81c3c", X"7ccd1c6d", X"7cc21c9e", X"7cb71ccf", X"7cab1d00", X"7ca01d31", X"7c941d62",
    X"7c891d93", X"7c7d1dc4", X"7c711df5", X"7c661e25", X"7c5a1e56", X"7c4e1e87", X"7c421eb8", X"7c361ee9",
    X"7c291f19", X"7c1d1f4a", X"7c111f7b", X"7c051fac", X"7bf81fdc", X"7beb200d", X"7bdf203e", X"7bd2206e",
    X"7bc5209f", X"7bb920d0", X"7bac2100", X"7b9f2131", X"7b922161", X"7b842192", X"7b7721c2", X"7b6a21f3",
    X"7b5d2223", X"7b4f2254", X"7b422284", X"7b3422b4", X"7b2622e5", X"7b192315", X"7b0b2345", X"7afd2376",
    X"7aef23a6", X"7ae123d6", X"7ad32407", X"7ac52437", X"7ab62467", X"7aa82497", X"7a9a24c7", X"7a8b24f7",
    X"7a7d2528", X"7a6e2558", X"7a5f2588", X"7a5025b8", X"7a4225e8", X"7a332618", X"7a242648", X"7a152678",
    X"7a0526a8", X"79f626d8", X"79e72707", X"79d82737", X"79c82767", X"79b92797", X"79a927c7", X"799927f6",
    X"798a2826", X"797a2856", X"796a2886", X"795a28b5", X"794a28e5", X"793a2915", X"792a2944", X"79192974",
    X"790929a3", X"78f929d3", X"78e82a02", X"78d82a32", X"78c72a61", X"78b62a91", X"78a62ac0", X"78952aef",
    X"78842b1f", X"78732b4e", X"78622b7d", X"78512bad", X"78402bdc", X"782e2c0b", X"781d2c3a", X"780c2c69",
    X"77fa2c98", X"77e92cc8", X"77d72cf7", X"77c52d26", X"77b42d55", X"77a22d84", X"77902db3", X"777e2de2",
    X"776c2e11", X"775a2e3f", X"77472e6e", X"77352e9d", X"77232ecc", X"77102efb", X"76fe2f29", X"76eb2f58",
    X"76d92f87", X"76c62fb5", X"76b32fe4", X"76a03013", X"768e3041", X"767b3070", X"7668309e", X"765430cd",
    X"764130fb", X"762e312a", X"761b3158", X"76073186", X"75f431b5", X"75e031e3", X"75cc3211", X"75b93240",
    X"75a5326e", X"7591329c", X"757d32ca", X"756932f8", X"75553326", X"75413354", X"752d3382", X"751933b0",
    X"750433de", X"74f0340c", X"74db343a", X"74c73468", X"74b23496", X"749e34c4", X"748934f2", X"7474351f",
    X"745f354d", X"744a357b", X"743535a8", X"742035d6", X"740b3604", X"73f63631", X"73e0365f", X"73cb368c",
    X"73b536ba", X"73a036e7", X"738a3714", X"73753742", X"735f376f", X"7349379c", X"733337ca", X"731d37f7",
    X"73073824", X"72f13851", X"72db387e", X"72c538ab", X"72af38d8", X"72983906", X"72823932", X"726b395f",
    X"7255398c", X"723e39b9", X"722739e6", X"72113a13", X"71fa3a40", X"71e33a6c", X"71cc3a99", X"71b53ac6",
    X"719e3af2", X"71863b1f", X"716f3b4c", X"71583b78", X"71413ba5", X"71293bd1", X"71123bfd", X"70fa3c2a",
    X"70e23c56", X"70cb3c83", X"70b33caf", X"709b3cdb", X"70833d07", X"706b3d33", X"70533d60", X"703b3d8c",
    X"70233db8", X"700a3de4", X"6ff23e10", X"6fda3e3c", X"6fc13e68", X"6fa93e93", X"6f903ebf", X"6f773eeb",
    X"6f5f3f17", X"6f463f43", X"6f2d3f6e", X"6f143f9a", X"6efb3fc5", X"6ee23ff1", X"6ec9401d", X"6eaf4048",
    X"6e964073", X"6e7d409f", X"6e6340ca", X"6e4a40f6", X"6e304121", X"6e17414c", X"6dfd4177", X"6de341a2",
    X"6dca41ce", X"6db041f9", X"6d964224", X"6d7c424f", X"6d62427a", X"6d4842a5", X"6d2d42d0", X"6d1342fa",
    X"6cf94325", X"6cde4350", X"6cc4437b", X"6ca943a5", X"6c8f43d0", X"6c7443fb", X"6c594425", X"6c3f4450",
    X"6c24447a", X"6c0944a5", X"6bee44cf", X"6bd344fa", X"6bb84524", X"6b9c454e", X"6b814578", X"6b6645a3",
    X"6b4a45cd", X"6b2f45f7", X"6b134621", X"6af8464b", X"6adc4675", X"6ac1469f", X"6aa546c9", X"6a8946f3",
    X"6a6d471c", X"6a514746", X"6a354770", X"6a19479a", X"69fd47c3", X"69e147ed", X"69c44816", X"69a84840",
    X"698c4869", X"696f4893", X"695348bc", X"693648e6", X"6919490f", X"68fd4938", X"68e04961", X"68c3498a",
    X"68a649b4", X"688949dd", X"686c4a06", X"684f4a2f", X"68324a58", X"68154a81", X"67f74aa9", X"67da4ad2",
    X"67bd4afb", X"679f4b24", X"67824b4c", X"67644b75", X"67464b9e", X"67294bc6", X"670b4bef", X"66ed4c17",
    X"66cf4c3f", X"66b14c68", X"66934c90", X"66754cb8", X"66574ce1", X"66394d09", X"661a4d31", X"65fc4d59",
    X"65dd4d81", X"65bf4da9", X"65a04dd1", X"65824df9", X"65634e21", X"65454e48", X"65264e70", X"65074e98",
    X"64e84ebf", X"64c94ee7", X"64aa4f0f", X"648b4f36", X"646c4f5e", X"644d4f85", X"642d4fac", X"640e4fd4",
    X"63ef4ffb", X"63cf5022", X"63b05049", X"63905070", X"63715097", X"635150bf", X"633150e5", X"6311510c",
    X"62f25133", X"62d2515a", X"62b25181", X"629251a8", X"627151ce", X"625151f5", X"6231521c", X"62115242",
    X"61f15269", X"61d0528f", X"61b052b5", X"618f52dc", X"616f5302", X"614e5328", X"612d534e", X"610d5375",
    X"60ec539b", X"60cb53c1", X"60aa53e7", X"6089540d", X"60685433", X"60475458", X"6026547e", X"600454a4",
    X"5fe354ca", X"5fc254ef", X"5fa05515", X"5f7f553a", X"5f5e5560", X"5f3c5585", X"5f1a55ab", X"5ef955d0",
    X"5ed755f5", X"5eb5561a", X"5e935640", X"5e715665", X"5e50568a", X"5e2d56af", X"5e0b56d4", X"5de956f9",
    X"5dc7571d", X"5da55742", X"5d835767", X"5d60578c", X"5d3e57b0", X"5d1b57d5", X"5cf957f9", X"5cd6581e",
    X"5cb45842", X"5c915867", X"5c6e588b", X"5c4b58af", X"5c2958d4", X"5c0658f8", X"5be3591c", X"5bc05940",
    X"5b9d5964", X"5b795988", X"5b5659ac", X"5b3359d0", X"5b1059f3", X"5aec5a17", X"5ac95a3b", X"5aa55a5e"
  );

end package twd_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
