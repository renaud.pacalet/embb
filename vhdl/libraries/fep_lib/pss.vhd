--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief FEP PSS top level

-- pragma translate_off
use std.textio.all;
-- pragma translate_on

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use ieee.numeric_std.all;
use global_lib.utils.all;
use global_lib.global.all;

--pragma translate_off
use global_lib.sim_utils.all;
--pragma translate_on

use work.bitfield.all;
use work.type_pkg.all;
use work.fep_pkg.all;
use work.pu_pkg.all;

library memories_lib;
use memories_lib.ram_pkg.all;

entity pss is
  generic(mss_pipeline_depth: positive range 2 to integer'high := 2); --* Number of pipeline stages in MSS, including input and output registers of RAMs
  port(clk:    in  std_ulogic;
       css2pss: in  css2pss_type;
       param:  in  std_ulogic_vector(cmdsize * 8 - 1 downto 0);
       pss2css: out pss2css_type;
       pss2mss: out pss2mss_type;
       mss2pss: in  mss2pss_type);
end entity pss;

architecture rtl of pss is

  constant debug_ftx: boolean := false;

  signal cmd, cmd_d: bitfield_type;
  signal srstn, ce, err, eoc, vpeoc, smaeoc: std_ulogic;
  signal ftnvp: boolean;
  signal vpry, vprl, vpwz, vpws, vpmul, vpint: boolean;
  signal dsipu: v2;
  signal smawdata: v128_vector(0 to 1);
  signal l2l: natural range 0 to 9;
  signal pss2mio, vppss2mio: pss2mio_type;
  signal mio2pss: mio2pss_type;

  -- alias for return status
  alias status is pss2css.status;
  alias exec is css2pss.exec;

  -- wait for read addresses
  type stage0_type is record
    en      : std_ulogic;
  end record;
  signal stage0a, stage0b : stage0_type;

  -- wait for MIO/TMP data
  type stage1_type is record
    en      : std_ulogic;
    last    : std_ulogic;
    t      : std_ulogic_vector(3 downto 0);
    mem     : memory_type;
    r      : std_ulogic;
  end record;

  type stage1_v is array (1 to mss_pipeline_depth) of stage1_type;

  signal stage1 : stage1_v;

  -- store data from MIO/TMP
  type stage2_type is record
    en      : std_ulogic;
    last    : std_ulogic;
    t      : std_ulogic_vector(3 downto 0);
    ft      : cpx50_array(1 downto 0, 3 downto 0);
  end record;

  signal stage2 : stage2_type;

  -- input transposition
  type stage3_type is record
    en      : std_ulogic;
    last    : std_ulogic;
    t      : std_ulogic_vector(3 downto 0);
  end record;

  signal stage3a, stage3b, stage3c : stage3_type;

  type stage4_type is record
    en      : std_ulogic;
   twdmod   : twiddle_modifier_vector(5 downto 0);
  end record;

  type stage4_v is array (1 to mss_pipeline_depth) of stage4_type;

  signal stage4 : stage4_v;

  -- pu processing
  type stage5_type is record
    en      : std_ulogic;
    last    : std_ulogic;
    t      : std_ulogic_vector(3 downto 0);
  end record;

  signal stage5a, stage5b, stage5c : stage5_type;

  -- output transposition
  type stage6_type is record
    en      : std_ulogic;
    last    : std_ulogic;
  end record;

  signal stage6a, stage6b, stage6c : stage6_type;

  type stage7_type is record
    en      : std_ulogic;
    data    : out_values_type;
  end record;

  signal stage7 : stage7_type;

  type run_type is record
    ftx    : std_ulogic;
    ftz    : std_ulogic;
    fttw   : std_ulogic;
    pu     : std_ulogic_vector(0 to 1);
  end record;

  signal run  : run_type;

  type bool_type is record
    ftx    : std_ulogic;
    ftz    : std_ulogic;
    fttw  : std_ulogic;
    pu    : std_ulogic;
  end record;

  signal eos, valid : bool_type;
  -- run address generators

  type addr_type is record
    ftx    : v10_vector(1 downto 0);
    ftz    : v10_vector(1 downto 0);
    wftx   : v10_vector(1 downto 0);
    wftz   : v10_vector(1 downto 0);
    fttw  : v10_vector(5 downto 0);
  end record;

  signal add : addr_type;
  -- addresses
  signal memx, memz : memory_type;
  -- memory selector

  signal load  : std_ulogic;
  signal eopv : extended_op_code_vector(1 downto 0);

-- processing unit dedicated signals

  signal ipu, ipu_ft : pu_in_v(1 downto 0);
  signal opu : pu_out_v(1 downto 0);

  signal ispow4  : boolean;
  -- pu command
  signal co50 : cpx50_array(1 downto 0, 3 downto 0);
  -- output data
  signal twdmod   : twiddle_modifier_vector(5 downto 0);

  -- transposing matrixes dedicated signals

  signal trsprdout : cpx50_array(1 downto 0, 3 downto 0);

  -- transposing mode
  signal t : std_ulogic_vector(3 downto 0);

  signal trspwrout      : cpx50_array(1 downto 0, 3 downto 0);
  signal lstage, rr, rw  : std_ulogic;
  signal rcnt, wcnt : std_ulogic_vector(10 downto 0);

  signal pss2twd: pss2twd_type;
  signal twd2pss: twd2pss_type;
  signal pss2tmp: pss2tmp_type;
  signal tmp2pss: v200_vector(0 to 1);

  signal vpcnt, vpcnt_next: u_unsigned(14 downto 0);
  signal vpcnt_eq0, vpcnt_eq1, vpcnt_eq2: boolean;
  signal iqx, iqy, iqz, iqs: natural range 0 to 3;
  signal ir: natural range 0 to 7;

  type vpstage01_d_type is record
    runx: std_ulogic;
    runy: std_ulogic;
  end record;

  signal vpstage01_d: vpstage01_d_type;

  type vpstage02_type is record
    addx, addy: u14_vector(0 to 1);
    enx, eny: vt8_vector(0 to 1); -- 2 ports, 8 columns
  end record;

  signal vpstage02: vpstage02_type;

  type vpstage03_type is record
    vpcl: vp_components_left;
    ax, ay: v4_vector(0 to 1);
  end record;
  constant vpstage03_none: vpstage03_type := (vpcl => zero, ax => (others => (others => '0')), ay => (others => (others => '0')));
  type vpstage03_vector is array(1 to mss_pipeline_depth) of vpstage03_type;
  constant vpstage03_vector_none: vpstage03_vector := (others => vpstage03_none);

  signal vpstage03_d, vpstage03: vpstage03_type;
  signal vpstage04: vpstage03_vector;

  type vpstage06_type is record
    vpcl: vp_components_left;
    ax, ay: v2_vector(0 to 1);
    x, y: v32_vector(0 to 1);
  end record;
  constant vpstage06_none: vpstage06_type := (vpcl => zero, ax => (others => (others => '0')), ay => (others => (others => '0')),
                                              x => (others => (others => '0')), y => (others => (others => '0')));

  signal vpstage06: vpstage06_type;

  type vpstage07_d_type is record
    vpcl: vp_components_left;
    add: v14_array(0 to 1, 0 to 1); -- First/second addresses, two ports
    extrapolate: v2;
    c1: u15_vector(0 to 1);
  end record;

  signal vpstage07_d: vpstage07_d_type;

  type vpstage07_type is record
    vpcl: vp_components_left;
    a: v4_vector(0 to 1); -- Two ports
    extrapolate: v2;
    c1: u15_vector(0 to 1);
  end record;
  constant vpstage07_none: vpstage07_type := (vpcl => zero, a => (others => (others => '0')), extrapolate => (others => '0'),
                                              c1 => (others => (others => '0')));
  type vpstage07_vector is array(1 to mss_pipeline_depth) of vpstage07_type;
  constant vpstage07_vector_none: vpstage07_vector := (others => vpstage07_none);

  signal vpstage07: vpstage07_type;
  signal vpstage08: vpstage07_vector;

  type vpstage10_type is record
    vpcl: vp_components_left;
    a: v2_vector(0 to 1); -- Two ports
    extrapolate: v2;
    c1: u15_vector(0 to 1);
    y: v32_vector(0 to 1);
  end record;
  constant vpstage10_none: vpstage10_type := (vpcl => zero, a => (others => (others => '0')), extrapolate => (others => '0'),
                                              c1 => (others => (others => '0')), y => (others => (others => '0')));

  signal vpstage10: vpstage10_type;

  type vpstage11_d_type is record
    vpcl: vp_components_left;
    dsi: std_ulogic_vector(0 to 1);
    ipu: pu_in_v(0 to 1);
  end record;

  signal vpstage11_d: vpstage11_d_type;

  signal vpstage11, vpstage12: vp_components_left;

  type vpstage13_type is record
    vpcl: vp_components_left;
    z: cpx66_vector(0 to 1);
  end record;

  signal vpstage13: vpstage13_type;

  type vpstage14_type is record
    vpcl: vp_components_left;
    runz: std_ulogic;
    z: cpx36_vector(0 to 1);
  end record;
  constant vpstage14_none: vpstage14_type := (vpcl => zero, runz => '0', z => (others => (others => (others => '0'))));

  signal vpstage14: vpstage14_type;

  type vpstage15_type is record
    vpcl: vp_components_left;
    be: v16_vector(0 to 1); -- byte write enables: 2 ports
    add: u14_vector(0 to 1);
    z: v32_vector(0 to 1);
  end record;

  signal vpstage15: vpstage15_type;

begin

  pss2mss.mio <= pss2mio;
  pss2mss.twd <= pss2twd;
  pss2mss.tmp <= pss2tmp;
  mio2pss <= mss2pss.mio;
  twd2pss <= mss2pss.twd;
  tmp2pss <= mss2pss.tmp;
  pss2css.data <= (others => '0'); -- FEP does not support reading PSS internal registers from CSS
  pss2css.eirq <= (others => '0'); -- FEP has zero extended interrupts

  -----------------------
  -- Vector operations --
  -----------------------

  iqx <= to_integer(u_unsigned(cmd.qx));
  iqy <= to_integer(u_unsigned(cmd.qy));
  iqz <= to_integer(u_unsigned(cmd.qz));
  iqs <= to_integer(u_unsigned(cmd.qs));
  ir  <= to_integer(u_unsigned(cmd.r));

  vpstage01_d.runx <= '1' when load = '1' or vpstage03_d.vpcl = more else
                     '0';
  vpstage01_d.runy <= '1' when load = '1' or (vpry and vpstage03_d.vpcl = more) else
                     '0';

  -- address generators
  vpx_addr: entity work.vpxy_address_generator(rtl)
  port map(
    clk   => clk,
    srstn => srstn,
    ce => ce,
    load  => exec,
    run   => vpstage01_d.runx,
    t     => u_unsigned(cmd_d.tx),
    b     => u_unsigned(cmd_d.bx),
    s     => cmd_d.sx,
    n     => u_unsigned(cmd_d.nx),
    m     => u_unsigned(cmd_d.mx),
    p     => u_unsigned(cmd_d.px),
    w     => u_unsigned(cmd_d.wx),
    a0    => vpstage02.addx(0),
    a1    => vpstage02.addx(1),
    en0   => vpstage02.enx(0),
    en1   => vpstage02.enx(1)
  );

  vpy_addr: entity work.vpxy_address_generator(rtl)
  port map(
    clk   => clk,
    srstn => srstn,
    ce => ce,
    load  => exec,
    run   => vpstage01_d.runy,
    t     => u_unsigned(cmd_d.ty),
    b     => u_unsigned(cmd_d.by),
    s     => cmd_d.sy,
    n     => u_unsigned(cmd_d.ny),
    m     => u_unsigned(cmd_d.my),
    p     => u_unsigned(cmd_d.py),
    w     => u_unsigned(cmd_d.wy),
    a0    => vpstage02.addy(0),
    a1    => vpstage02.addy(1),
    en0   => vpstage02.eny(0),
    en1   => vpstage02.eny(1)
  );

  vpstage03_d_p: process(ftnvp, vpcnt_eq0, vpcnt_eq1, vpcnt_eq2, vpstage02)
  begin
    vpstage03_d.vpcl <= zero;
    vpstage03_d.ax   <= (others => (others => '0'));
    vpstage03_d.ay   <= (others => (others => '0'));
    if not ftnvp then
      if vpcnt_eq0 then
        vpstage03_d.vpcl <= zero;
      elsif vpcnt_eq1 then
        vpstage03_d.vpcl <= one;
      elsif vpcnt_eq2 then
        vpstage03_d.vpcl <= two;
      else
        vpstage03_d.vpcl <= more;
      end if;
      for p in 0 to 1 loop
        vpstage03_d.ax(p) <= std_ulogic_vector(vpstage02.addx(p)(3 downto 0));
        vpstage03_d.ay(p) <= std_ulogic_vector(vpstage02.addy(p)(3 downto 0));
      end loop;
    end if;
  end process vpstage03_d_p;

  vpstage030405_p: process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        vpstage03.vpcl <= zero;
        for i in 1 to mss_pipeline_depth loop
          vpstage04(i).vpcl <= zero;
        end loop;
      elsif ce = '1' then
        vpstage03 <= vpstage03_d;
        vpstage04 <= vpstage03 & vpstage04(1 to mss_pipeline_depth - 1);
      end if;
    end if;
  end process vpstage030405_p;

  vpstage06_p: process(clk)
    variable tmp: v128;
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        vpstage06.vpcl <= zero;
      elsif ce = '1' then
        vpstage06 <= vpstage06_none;
        vpstage06.vpcl <= vpstage04(mss_pipeline_depth).vpcl;
        for p in 0 to 1 loop
          vpstage06.ax(p) <= vpstage04(mss_pipeline_depth).ax(p)(1 downto 0);
          vpstage06.ay(p) <= vpstage04(mss_pipeline_depth).ay(p)(1 downto 0);
        end loop;
        if not ftnvp then
          for p in 0 to 1 loop
            if vpstage04(mss_pipeline_depth).vpcl /= zero and (p = 0 or vpstage04(mss_pipeline_depth).vpcl /= one) then
              tmp := shift_left(mio2pss(iqx, p), to_integer(u_unsigned(vpstage04(mss_pipeline_depth).ax(p)(3 downto 2))) * 32);
              vpstage06.x(p)  <= tmp(127 downto 96);
              tmp := shift_left(mio2pss(iqy, p), to_integer(u_unsigned(vpstage04(mss_pipeline_depth).ay(p)(3 downto 2))) * 32);
              vpstage06.y(p)  <= tmp(127 downto 96);
            end if;
          end loop;
        end if;
      end if;
    end if;
  end process vpstage06_p;

  -- Y read requests for CWL
  vpstage07_d_p: process(vprl, vpstage06, cmd)
    variable luti: lut_info;
  begin
    vpstage07_d.vpcl        <= vpstage06.vpcl;
    vpstage07_d.add         <= (others => (others => (others => '0')));
    vpstage07_d.extrapolate <= (others => '0');
    vpstage07_d.c1          <= (others => (others => '0'));
    if vprl then
      for p in 0 to 1 loop
        if vpstage06.vpcl /= zero and (p = 0 or vpstage06.vpcl /= one) then
          luti := get_lut_info(vpstage06.x(p), vpstage06.ax(p), cmd);
          vpstage07_d.add(0, p)      <= luti.add;
          vpstage07_d.add(1, p)      <= luti.add2;
          vpstage07_d.extrapolate(p) <= luti.extrapolate;
          vpstage07_d.c1(p)          <= luti.c1;
        end if;
      end loop;
    end if;
  end process vpstage07_d_p;

  vpstage07_p: process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        vpstage07.vpcl <= zero;
      elsif ce = '1' then
        vpstage07.a(0)        <= vpstage07_d.add(0, 0)(3 downto 0);
        vpstage07.a(1)        <= vpstage07_d.add(0, 1)(3 downto 0);
        vpstage07.vpcl        <= vpstage07_d.vpcl;
        vpstage07.extrapolate <= vpstage07_d.extrapolate;
        vpstage07.c1          <= vpstage07_d.c1;
      end if;
    end if;
  end process vpstage07_p;

  vpstage080910_p: process(clk)
    variable tmp: v128;
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        for i in 1 to mss_pipeline_depth loop
          vpstage08(i).vpcl <= zero;
        end loop;
        vpstage10.vpcl <= zero;
      elsif ce = '1' then
        vpstage08 <= vpstage07_vector_none;
        vpstage10 <= vpstage10_none;
        if vprl then
          vpstage08 <= vpstage07 & vpstage08(1 to mss_pipeline_depth - 1);
          vpstage10.vpcl <= vpstage08(mss_pipeline_depth).vpcl;
          for p in 0 to 1 loop
            vpstage10.a(p)           <= vpstage08(mss_pipeline_depth).a(p)(1 downto 0);
            vpstage10.extrapolate(p) <= vpstage08(mss_pipeline_depth).extrapolate(p);
            vpstage10.c1(p)          <= vpstage08(mss_pipeline_depth).c1(p);
            tmp                      := mio2pss(iqy, p) rol (to_integer(u_unsigned(vpstage08(mss_pipeline_depth).a(p)(3 downto 1))) * 16);
            vpstage10.y(p)           <= tmp(127 downto 96);
          end loop;
        end if;
      end if;
    end if;
  end process vpstage080910_p;

  vpstage11_d_p: process(vpstage06, vpstage10, vprl, cmd, ftnvp)
    variable vpcl: vp_components_left;
    variable opl: std_ulogic_vector(cmd.op'length - 1 downto 0);
  begin
    opl := cmd.op;
    vpstage11_d.vpcl <= zero;
    vpstage11_d.ipu  <= (others => pu_di_none);
    vpstage11_d.dsi  <= (others => '0');
    if not ftnvp then
      if vprl then
        vpcl := vpstage10.vpcl;
      else
        vpcl := vpstage06.vpcl;
      end if;
      vpstage11_d.vpcl <= vpcl;
      for p in 0 to 1 loop
        if vpcl /= zero and (p = 0 or vpcl /= one) then
          case opl is
            when cwaop =>
              vpstage11_d.ipu(p).op <= cwa;
            when cwpop =>
              vpstage11_d.ipu(p).op <= cwp;
            when cwmop =>
              vpstage11_d.ipu(p).op <= cwm;
            when cwsop =>
              vpstage11_d.ipu(p).op <= cws;
            when movop =>
              vpstage11_d.ipu(p).op <= mov;
            when cwlop =>
              vpstage11_d.ipu(p).op <= cwl;
            when others => null;
          end case;
          vpstage11_d.ipu(p).tx  <= cmd.tx;
          vpstage11_d.ipu(p).ty  <= cmd.ty;
          vpstage11_d.ipu(p).vrx <= cmd.vrx;
          vpstage11_d.ipu(p).vix <= cmd.vix;
          vpstage11_d.ipu(p).vry <= cmd.vry;
          vpstage11_d.ipu(p).viy <= cmd.viy;
          vpstage11_d.ipu(p).dx  <= cmd.dx;
          vpstage11_d.ipu(p).dy  <= cmd.dy;
          if vprl then
            vpstage11_d.ipu(p).ay <= vpstage10.a(p);
            vpstage11_d.ipu(p).y <= vpstage10.y(p);
            if cmd.li = '1' then
              if vpstage10.extrapolate(p) = '1' then
                vpstage11_d.ipu(p).op <= cwle;
              else
                vpstage11_d.ipu(p).op <= cwli;
              end if;
              vpstage11_d.ipu(p).c1 <= vpstage10.c1(p);
            end if;
            vpstage11_d.dsi(p) <= '1';
          else
            vpstage11_d.ipu(p).ay <= vpstage06.ay(p)(1 downto 0);
            vpstage11_d.ipu(p).y  <= vpstage06.y(p);
            vpstage11_d.ipu(p).ax <= vpstage06.ax(p)(1 downto 0);
            vpstage11_d.ipu(p).x  <= vpstage06.x(p);
            vpstage11_d.dsi(p)    <= '1';
          end if;
        end if;
      end loop;
    end if;
  end process vpstage11_d_p;

  vpstage111213_p: process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        vpstage11      <= zero;
        vpstage12      <= zero;
        vpstage13.vpcl <= zero;
      elsif ce = '1' then
        vpstage11      <= vpstage11_d.vpcl;
        vpstage12      <= vpstage11;
        vpstage13.vpcl <= vpstage12;
      end if;
    end if;
  end process vpstage111213_p;

  vpstage13.z(0) <= opu(0).v when (not ftnvp) and vpstage13.vpcl /= zero else
                    (others => (others => '0'));
  vpstage13.z(1) <= opu(1).v when (not ftnvp) and vpstage13.vpcl /= zero and vpstage13.vpcl /= one else
                    (others => (others => '0'));

  i_sma: entity work.sma(rtl)
    port map(
      clk   => clk,
      srstn => srstn,
      ce => ce,
      ws    => vpws,
      mul   => vpmul,
      r     => ir,
      dsi   => vpstage13.vpcl,
      z     => vpstage13.z,
      wdata => smawdata,
      eoc   => smaeoc);

  vpstage14_p: process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        vpstage14.vpcl <= zero;
      elsif ce = '1' then
        vpstage14 <= vpstage14_none;
        vpstage14.vpcl <= vpstage13.vpcl;
        if vpwz and vpstage13.vpcl /= zero then
          vpstage14.runz  <= '1';
        end if;
        if vpwz and vpstage13.vpcl /= zero then
          vpstage14.z(0) <= output_conversion_1(vpstage13.z(0), cmd, vpmul);
          if vpstage13.vpcl /= one then
            vpstage14.z(1) <= output_conversion_1(vpstage13.z(1), cmd, vpmul);
          end if;
        end if;
      end if;
    end if;
  end process vpstage14_p;

  vpz_addr: entity work.vpz_address_generator(rtl)
  port map(
    clk   => clk,
    srstn => srstn,
    ce => ce,
    load  => exec,
    run   => vpstage14.runz,
    t     => u_unsigned(cmd_d.tz),
    b     => u_unsigned(cmd_d.bz),
    s     => cmd_d.sz,
    n     => u_unsigned(cmd_d.nz),
    m     => u_unsigned(cmd_d.mz),
    w     => u_unsigned(cmd_d.wz),
    a0    => vpstage15.add(0),
    a1    => vpstage15.add(1),
    be0   => vpstage15.be(0),
    be1   => vpstage15.be(1)
  );

  vpstage15_p: process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        vpstage15.vpcl <= zero;
      elsif ce = '1' then
        vpstage15.z <= (others => (others => '0'));
        vpstage15.vpcl <= vpstage14.vpcl;
        if vpstage14.vpcl /= zero then
          vpstage15.z(0)  <= output_conversion_2(vpstage14.z(0), cmd, vpint);
          if vpstage14.vpcl /= one then
            vpstage15.z(1)  <= output_conversion_2(vpstage14.z(1), cmd, vpint);
          end if;
        end if;
      end if;
    end if;
  end process vpstage15_p;

  vppss2mio_p: process(clk)
    variable tmp_add: v10;
    variable tmp: natural range 0 to 7;
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        vppss2mio.lock <= (others => '0');
        vppss2mio.en   <= (others => (others => (others => '0')));
      elsif ce = '1' then
        vppss2mio <= pss2mio_none;
        for r in 0 to 3 loop -- 4 rows
          -- X read requests
          if iqx = r then
            for p in 0 to 1 loop
              if vpstage03_d.vpcl /= zero and (p = 0 or vpstage03_d.vpcl /= one) then
                vppss2mio.lock(r) <= '1';
                tmp_add := std_ulogic_vector(vpstage02.addx(p)(13 downto 4));
                for c in 0 to 1 loop
                  vppss2mio.add(r, c, p) <= tmp_add;
                end loop;
                for c in 0 to 7 loop
                  vppss2mio.en(r, c, p) <= vpstage02.enx(p)(c);
                end loop;
              end if;
            end loop;
          end if;
        -- Y read requests for CWA and CWP
          if vpry and iqy = r then
            for p in 0 to 1 loop
              if vpstage03_d.vpcl /= zero and (p = 0 or vpstage03_d.vpcl /= one) then
                vppss2mio.lock(r) <= '1';
                tmp_add := std_ulogic_vector(vpstage02.addy(p)(13 downto 4));
                for c in 0 to 1 loop
                  vppss2mio.add(r, c, p) <= tmp_add;
                end loop;
                for c in 0 to 7 loop
                  vppss2mio.en(r, c, p) <= vpstage02.eny(p)(c);
                end loop;
              end if;
            end loop;
          end if;
        -- Y read requests for CWL
          if vprl and iqy = r then
            for p in 0 to 1 loop -- Two ports
              if vpstage07_d.vpcl /= zero and (p = 0 or vpstage07_d.vpcl /= one) then
                vppss2mio.lock(r) <= '1';
                tmp_add := vpstage07_d.add(0, p)(13 downto 4); -- First address
                tmp := to_integer(u_unsigned(vpstage07_d.add(0, p)(3 downto 1)));
                if tmp = 0 then
                  vppss2mio.add(r, 0, p) <= tmp_add;
                else
                  vppss2mio.add(r, 1, p) <= tmp_add;
                end if;
                vppss2mio.en(r, tmp, p) <= '1';
                if cmd.li = '1' then -- Interpolation - extrapolation
                  tmp_add := vpstage07_d.add(1, p)(13 downto 4); -- Second address
                  tmp := to_integer(u_unsigned(vpstage07_d.add(1, p)(3 downto 1)));
                  if tmp = 0 then
                    vppss2mio.add(r, 0, p) <= tmp_add;
                  else
                    vppss2mio.add(r, 1, p) <= tmp_add;
                  end if;
                  vppss2mio.en(r, tmp, p) <= '1';
                end if;
              end if;
            end loop;
          end if;
        -- Z write requests for vector operations
          if vpwz and iqz = r and vpstage15.vpcl /= zero then
            tmp_add := std_ulogic_vector(vpstage15.add(0)(13 downto 4));
            vppss2mio.lock(r) <= '1';
            vppss2mio.rnw(r) <= '0';
            for c in 0 to 1 loop
              if c = 0 then
                vppss2mio.add(r, 0, 0) <= tmp_add;
              else
                vppss2mio.add(r, 1, 0) <= tmp_add;
              end if;
            end loop;
            for c in 0 to 7 loop
              for b in 0 to 1 loop
                if vpstage15.be(0)(15 - 2 * c - b) = '1' then
                  vppss2mio.be(c, 0)(1 - b) <= '1';
                  vppss2mio.en(r, c, 0) <= '1';
                  vppss2mio.wdata(0)(127 - 16 * c - 8 * b downto 127 - 16 * c - 8 * b - 7) <= vpstage15.z(0)(31 - 16 * (c mod 2) - b * 8 downto 31 - 16 * (c mod 2) - b * 8 - 7);
                end if;
              end loop;
            end loop;
            tmp_add := std_ulogic_vector(vpstage15.add(1)(13 downto 4));
            for c in 0 to 1 loop
              if c = 0 then
                vppss2mio.add(r, 0, 1) <= tmp_add;
              else
                vppss2mio.add(r, 1, 1) <= tmp_add;
              end if;
            end loop;
            for c in 0 to 7 loop
              for b in 0 to 1 loop
                vppss2mio.be(c, 1)(1 - b) <= vpstage15.be(1)(15 - 2 * c - b);
                vppss2mio.wdata(1)(127 - 16 * c - 8 * b downto 127 - 16 * c - 8 * b - 7) <= vpstage15.z(1)(31 - 16 * (c mod 2) - b * 8 downto 31 - 16 * (c mod 2) - b * 8 - 7);
              end loop;
            end loop;
            if vpstage15.vpcl /= one then
              if tmp_add /= std_ulogic_vector(vpstage15.add(0)(13 downto 4)) then
                for c in 0 to 7 loop
                  for b in 0 to 1 loop
                    if vpstage15.be(1)(15 - 2 * c - b) = '1' then
                      vppss2mio.en(r, c, 1) <= '1';
                    end if;
                  end loop;
                end loop;
              else
                for c in 0 to 7 loop
                  for b in 0 to 1 loop
                    if vpstage15.be(1)(15 - 2 * c - b) = '1' then
                      vppss2mio.be(c, 0)(1 - b) <= '1';
                      vppss2mio.en(r, c, 0) <= '1';
                      vppss2mio.wdata(0)(127 - 16 * c - 8 * b downto 127 - 16 * c - 8 * b - 7) <= vpstage15.z(1)(31 - 16 * (c mod 2) - b * 8 downto 31 - 16 * (c mod 2) - b * 8 - 7);
                    end if;
                  end loop;
                end loop;
              end if;
            end if;
          end if;
        -- S write requests for vector operations
          if vpws and iqs = r and smaeoc = '1' then
            vppss2mio.lock(r) <= '1';
            for c in 0 to 7 loop
              vppss2mio.en(r, c, 0) <= '1';
              vppss2mio.be(c, 0) <= "11";
            end loop;
            for c in 0 to 3 loop
              vppss2mio.en(r, c, 1) <= '1';
              vppss2mio.be(c, 1) <= "11";
            end loop;
            vppss2mio.rnw(r) <= '0';
            for c in 0 to 1 loop
              vppss2mio.add(r, c, 0) <= cmd.bs & '0';
              vppss2mio.add(r, c, 1) <= cmd.bs & '1';
            end loop;
            vppss2mio.wdata <= smawdata;
          end if;
        end loop;
      end if;
    end if;
  end process vppss2mio_p;

  ------------------------
  -- Fourier transforms --
  ------------------------

  -- FFT read addresses generator
  ftx_addr: entity work.ftx_address_generator(rtl)
  port map(
    clk    => clk,
    srstn  => srstn,
    ce => ce,
    load   => load,
    run    => run.ftx,
    l      => cmd.l(12 downto 3),
    b      => cmd.bx(11 downto 3),
    eos    => eos.ftx,
    mem    => memx,
    r      => rr,
    t      => t,
    lstage => lstage,
    valid  => valid.ftx,
    a0     => add.ftx(0),
    a1     => add.ftx(1)
  );

  -- pragma translate_off
  gftx: if debug_ftx generate
    process
      variable l: line;
    begin
      wait until rising_edge(clk) and srstn = '1' and (load = '1' or valid.ftx = '1');
      if load = '1' then
        print(l, "run=");
        write(l, run.ftx);
        print(l, ", l=");
        write(l, cmd.l(12 downto 0));
        print(l, ", b=");
        write(l, cmd.bx);
        writeline(output, l);
      else
        if memx = mio then
          print(l, "memx=mio, ");
        else
          print(l, "memx=tmp, ");
        end if;
        print(l, "rr="); write(l, rr);
        print(l, ", t="); write(l, t);
        print(l, ", lstage="); write(l, lstage);
        print(l, ", a0="); write(l, to_integer(u_unsigned(add.ftx(0))));
        print(l, ", a1="); write(l, to_integer(u_unsigned(add.ftx(1))));
        writeline(output, l);
      end if;
    end process;
  end generate gftx;
  -- pragma translate_on

  ftx_addr_p: process(add.ftx, cmd)
  begin
    case cmd.wx is
      when "00" =>
        add.wftx(0) <= cmd.bx(11 downto 9) & add.ftx(0)(6 downto 0);
        add.wftx(1) <= cmd.bx(11 downto 9) & add.ftx(1)(6 downto 0);
      when "01" =>
        add.wftx(0) <= cmd.bx(11 downto 10) & add.ftx(0)(7 downto 0);
        add.wftx(1) <= cmd.bx(11 downto 10) & add.ftx(1)(7 downto 0);
      when "10" =>
        add.wftx(0) <= cmd.bx(11) & add.ftx(0)(8 downto 0);
        add.wftx(1) <= cmd.bx(11) & add.ftx(1)(8 downto 0);
      when others =>
        add.wftx(0) <= add.ftx(0);
        add.wftx(1) <= add.ftx(1);
    end case;
  end process ftx_addr_p;

  -- FFT write addresses generator
  ftz_addr: entity work.ftz_address_generator(rtl)
  port map(
    clk    => clk,
    srstn  => srstn,
    ce => ce,
    load   => load,
    run    => run.ftz,
    l      => cmd.l(12 downto 3),
    b      => cmd.bz(11 downto 3),
    eos    => eos.ftz,
    mem    => memz,
    lstage => open,
    r      => rw,
    t      => open,
    valid  => valid.ftz,
    a0     => add.ftz(0),
    a1     => add.ftz(1)
  );

  ftz_addr_p: process(add.ftz, cmd)
  begin
    case cmd.wz is
      when "00" =>
        add.wftz(0) <= cmd.bz(11 downto 9) & add.ftz(0)(6 downto 0);
        add.wftz(1) <= cmd.bz(11 downto 9) & add.ftz(1)(6 downto 0);
      when "01" =>
        add.wftz(0) <= cmd.bz(11 downto 10) & add.ftz(0)(7 downto 0);
        add.wftz(1) <= cmd.bz(11 downto 10) & add.ftz(1)(7 downto 0);
      when "10" =>
        add.wftz(0) <= cmd.bz(11) & add.ftz(0)(8 downto 0);
        add.wftz(1) <= cmd.bz(11) & add.ftz(1)(8 downto 0);
      when others =>
        add.wftz(0) <= add.ftz(0);
        add.wftz(1) <= add.ftz(1);
    end case;
  end process ftz_addr_p;

  -- FFT read twiddle addresses generator

  fttw_addr: entity work.twd_address_generator(rtl)
  port map(
    clk    => clk,
    srstn  => srstn,
    ce => ce,
    load   => load,
    run    => run.fttw,
    l      => cmd.l(12 downto 0),
    valid  => valid.fttw,
    twdmod => twdmod,
    add    => add.fttw
  );

  pu_g: for i in 0 to 1 generate
    ipu(i) <= ipu_ft(i) when ftnvp else
              vpstage11_d.ipu(i);
    dsipu(i) <= run.pu(i) when ftnvp else
                vpstage11_d.dsi(i);
    i_pu0: entity work.pu(rtl)
--pragma translate_off
    generic map(idx => i)
--pragma translate_on
    port map(
      clk   => clk,
      srstn => srstn,
      ce => ce,
      dsi   => dsipu(i),
      di    => ipu(i),
      do    => opu(i)
    );
  end generate pu_g;

  transp_rd: entity work.transpose(rtl)
  port map(
    clk   => clk,
    srstn => srstn,
    ce => ce,
    flush => load,
    en    => stage2.en,
    rnw   => '1',
    din   => stage2.ft,
    dout  => trsprdout,
    t     => stage2.t
  );

  transp_wr :  entity work.transpose(rtl)
  port map (
    clk   => clk,
    srstn => srstn,
    ce => ce,
    flush => load,
    en    => stage5c.en,
    rnw   => '0',
    din   => co50,
    dout  => trspwrout,
    t     => stage5c.t
  );

  ipu_ft_p: process(eopv, cmd, opu, trsprdout, twd2pss, stage4)
  begin
    ipu_ft <= (others => pu_di_none);
    for i in 0 to 1 loop
      ipu_ft(i).op <= eopv(i);
      ipu_ft(i).i  <= cmd.i;
      for j in 0 to 3 loop
        co50(i, j)  <= opu(i).u(j);
        ipu_ft(i).u(j) <= trsprdout(i, j);
      end loop;
    end loop;
    for j in 0 to 2 loop
      ipu_ft(0).t(j + 1).r <= resize(u_signed('0' & twd2pss(0, j)(31 downto 16)), 16);
      ipu_ft(0).t(j + 1).i <= resize(u_signed('0' & twd2pss(0, j)(15 downto 0 )), 16);
      ipu_ft(1).t(j + 1).r <= resize(u_signed('0' & twd2pss(1, j)(31 downto 16)), 16);
      ipu_ft(1).t(j + 1).i <= resize(u_signed('0' & twd2pss(1, j)(15 downto 0 )), 16);
      for i in 0 to 1 loop
        ipu_ft(i).tmod(j + 1)  <= stage4(mss_pipeline_depth).twdmod(i * 3 + j);
      end loop;
    end loop;
  end process ipu_ft_p;

  run.fttw    <= stage1(2).en;
  run.ftz     <= stage6c.en;
  run.ftx     <= stage0a.en;

  ipu_data: process(stage3c.en, ftnvp) --  processing unit input
  begin
    run.pu <= (others => '0');
    if ftnvp then
      run.pu <= (others => stage3c.en);
    end if;
  end process ipu_data;

  ipu_op: process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        eopv <= (others => cwa);
      elsif ce = '1' then
        if ftnvp then
          eopv <= (others => ft);
          if stage3b.en = '1' then
            if stage3b.last = '1' then
              if ispow4 then
                eopv <= (others => ftl4);
              else
                eopv <= (others => ftl2);
              end if;
            end if;
          end if;
        end if;
      end if;
    end if;
  end process ipu_op;

  pipe: process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        stage0b <= (en => '0');
        for i in 1 to mss_pipeline_depth loop
          stage1(i) <= (en => '0', last => '0', t => (others => '0'), mem => mio, r => '0');
          stage4(i) <= (en => '0', twdmod => twdmod);
        end loop;
        stage2.en <= '0';
        stage2.last <= '0';
        stage2.t <= (others => '0');
        stage3a <= (en => '0', last => '0', t => (others => '0'));
        stage3b <= (en => '0', last => '0', t => (others => '0'));
        stage3c <= (en => '0', last => '0', t => (others => '0'));
        stage5a <= (en => '0', last => '0', t => (others => '0'));
        stage5b <= (en => '0', last => '0', t => (others => '0'));
        stage5c <= (en => '0', last => '0', t => (others => '0'));
        stage6a <= (en => '0', last => '0');
        stage6b <= (en => '0', last => '0');
        stage6c <= (en => '0', last => '0');
        stage7.en <= '0';
      elsif ce = '1' then
      -- stage 0
        stage0b     <= stage0a;
      -- stage 1
        stage1(1).en    <= stage0b.en;
        stage1(1).last  <= lstage;
        stage1(1).t      <= t;
        stage1(1).mem   <= memx;
        stage1(1).r      <= rr;
        for i in 1 to mss_pipeline_depth - 1 loop
          stage1(i + 1)      <= stage1(i);
        end loop;
      -- stage 2
        if stage1(mss_pipeline_depth).mem = MIO then
          xmio_g : for i in 0 to 3 loop
            stage2.ft(0, i).r   <= resize(u_signed(mio2pss(iqx, 0)(127 - 32 * i downto 112 - 32 * i)), 25);
            stage2.ft(0, i).i   <= resize(u_signed(mio2pss(iqx, 0)(111 - 32 * i downto 96 - 32 * i)), 25);
            stage2.ft(1, i).r   <= resize(u_signed(mio2pss(iqx, 1)(127 - 32 * i downto 112 - 32 * i)), 25);
            stage2.ft(1, i).i   <= resize(u_signed(mio2pss(iqx, 1)(111 - 32 * i downto 96 - 32 * i)), 25);
          end loop;
        else
          xtmp_g : for i in 0 to 3 loop
            if stage1(mss_pipeline_depth).r = '0' then
              stage2.ft(0, i).r   <= s25(tmp2pss(0)((3 - i) * 50 + 49 downto (3 - i) * 50 + 25));
              stage2.ft(0, i).i   <= s25(tmp2pss(0)((3 - i) * 50 + 24 downto (3 - i) * 50));
              stage2.ft(1, i).r   <= s25(tmp2pss(1)((3 - i) * 50 + 49 downto (3 - i) * 50 + 25));
              stage2.ft(1, i).i   <= s25(tmp2pss(1)((3 - i) * 50 + 24 downto (3 - i) * 50));
            else
              stage2.ft(0, i).r   <= s25(tmp2pss(1)((3 - i) * 50 + 49 downto (3 - i) * 50 + 25));
              stage2.ft(0, i).i   <= s25(tmp2pss(1)((3 - i) * 50 + 24 downto (3 - i) * 50));
              stage2.ft(1, i).r   <= s25(tmp2pss(0)((3 - i) * 50 + 49 downto (3 - i) * 50 + 25));
              stage2.ft(1, i).i   <= s25(tmp2pss(0)((3 - i) * 50 + 24 downto (3 - i) * 50));
            end if;
          end loop;
        end if;
        stage2.en    <= stage1(mss_pipeline_depth).en;
        stage2.last  <= stage1(mss_pipeline_depth).last;
        stage2.t     <= stage1(mss_pipeline_depth).t;
      -- stage 3
        stage3a.en    <= stage2.en;
        stage3a.last  <= stage2.last;
        stage3a.t     <= stage2.t;
        stage3b       <= stage3a;
        stage3c       <= stage3b;
      -- stage 4
        stage4(1).en  <= valid.fttw;
        stage4(1).twdmod  <= twdmod;
        for i in 1 to mss_pipeline_depth - 1 loop
          stage4(i + 1)      <= stage4(i);
        end loop;
      -- stage 5
        stage5a.en    <= stage3c.en;
        stage5a.last  <= stage3c.last;
        stage5a.t      <= stage3c.t;
        stage5b      <= stage5a;
        stage5c      <= stage5b;
      -- stage 6
        stage6a.en     <= stage5c.en;
        stage6a.last   <= stage5c.last;
        stage6b       <= stage6a;
        stage6c       <= stage6b;
      -- stage 7
        stage7.en       <= stage6c.en;
      end if;
    end if;
  end process pipe;

  cmd_d <= bitfield_slice(param);
  vpcnt_next <= vpcnt - 2;
  ce <= css2pss.ce;
  srstn  <= '0' when css2pss.srstn = '0' or eoc = '1' or err = '1' else
            '1';

  ctrl_p : process(clk)
    variable rcntv, wcntv    : std_ulogic_vector(10 downto 0);
    variable verr: std_ulogic;
    variable vstatus: status_type;
    type state_type is (idle, starting, running, post);
    variable state: state_type;
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        vpeoc  <= '0';
        err    <= '0';
        status <= (others => '0');
        rcntv  := std_ulogic_vector(u_unsigned(rcnt) + 1);
        wcntv  := std_ulogic_vector(u_unsigned(wcnt) + 1);
        load       <= '0';
        cmd        <= cmd_none;
        wcnt       <= (others => '0');
        rcnt       <= (others => '0');
        stage0a.en <= '0';
        vpcnt      <= (others => '0');
        vpcnt_eq0  <= true;
        vpcnt_eq1  <= false;
        vpcnt_eq2  <= false;
        err        <= '0';
        status     <= (others => '0');
        ispow4     <= false;
        ftnvp      <= false;
        vpry       <= false;
        vprl       <= false;
        vpwz       <= false;
        vpws       <= false;
        vpmul      <= false;
        vpint      <= false;
        l2l        <= 0;
        state      := idle;
      elsif ce = '1' then
        vpeoc  <= '0';
        err    <= '0';
        status <= (others => '0');
        rcntv  := std_ulogic_vector(u_unsigned(rcnt) + 1);
        wcntv  := std_ulogic_vector(u_unsigned(wcnt) + 1);
        load <= '0';
        if exec = '1' then
          cmd   <= cmd_d;
          load  <= '1';
          rcntv := (others => '0');
          wcntv := (others => '0');
        end if;
        if load = '1' then
          wcnt <= (others => '0');
          rcnt <= (others => '0');
          if cmd.op = fftop then -- FFT mode
            stage0a.en <= '1';
            l2l        <= fep_log2(cmd.l(12 downto 3));
          else
            vpcnt     <= u_unsigned(cmd.l);
            vpcnt_eq0 <= (or_reduce(cmd.l) = '0');
            vpcnt_eq1 <= (or_reduce(cmd.l(14 downto 1) & (not cmd.l(0))) = '0');
            vpcnt_eq2 <= (or_reduce(cmd.l(14 downto 2) & (not cmd.l(1)) & cmd.l(0)) = '0');
          end if;
          check_param(cmd, verr, vstatus);
          err    <= verr;
          status <= vstatus;
          ispow4 <= is_pow4(cmd.l);
          ftnvp  <= cmd.op = fftop;
          vpry   <= cmd.op = cwaop or cmd.op = cwpop;
          vprl   <= cmd.op = cwlop;
          vpwz   <= cmd.op /= fftop and cmd.sma /= sma_s_only;
          vpws   <= cmd.op /= fftop and cmd.sma /= sma_z_only;
          vpmul  <= cmd.op = cwpop or cmd.op = cwmop or cmd.op = cwsop or cmd.op = cwlop;
          vpint  <= cmd.op /= fftop and cmd.tz(1) = '0';
        elsif vpcnt_next(14) = '1' then
          vpcnt     <= (others => '0');
          vpcnt_eq0 <= true;
          vpcnt_eq1 <= false;
          vpcnt_eq2 <= false;
        else
          vpcnt     <= vpcnt_next;
          vpcnt_eq0 <= (or_reduce(vpcnt_next) = '0');
          vpcnt_eq1 <= (or_reduce(vpcnt_next(14 downto 1) & (not vpcnt_next(0))) = '0');
          vpcnt_eq2 <= (or_reduce(vpcnt_next(14 downto 2) & (not vpcnt_next(1)) & vpcnt_next(0)) = '0');
        end if;
        if run.ftx = '1' then
          if rcntv(l2l) = '1' then
            rcntv      := (others => '0');
            stage0a.en <= '0';
          end if;
          rcnt <= rcntv;
        end if;
        if run.ftz = '1' then
          if wcntv(l2l) = '1' then
            wcntv := (others => '0');
            if stage6c.last = '0' then
              stage0a.en <= '1';
            end if;
          end if;
          wcnt <= wcntv;
        end if;
        if eos.ftz = '1' then
          stage0a.en <= '0';
        end if;
        if (not ftnvp) then -- Vector operations
          case state is
            when idle =>
              if exec = '1' then
                state := starting;
              end if;
            when starting =>
              case vpstage15.vpcl is
                when one | two => -- First and last output sample(s)
                  if vpws then
                    state := post;
                  else
                    state := idle;
                    vpeoc <= '1';
                  end if;
                when more => -- First two output samples
                  state := running;
                when others => -- Nothing yet
                  null;
              end case;
            when running =>
              if vpstage15.vpcl /= more then -- Last output sample(s)
                if vpws then
                  state := post;
                else
                  state := idle;
                  vpeoc <= '1';
                end if;
              end if;
            when post =>
              state := idle;
              vpeoc <= '1';
          end case;
        end if;
      end if;
    end if;
  end process ctrl_p;

  eoc        <= eos.ftz or err or vpeoc;
  pss2css.eoc <= eoc;
  pss2css.err <= err;

  output_processing : process(clk)
    variable v: out_values_type;
  begin
    if rising_edge(clk) then
      if ce = '1' then
        if stage6c.en = '1' then
          if ftnvp then -- DFT
            if stage6c.last = '1' then
              for n in 0 to 1 loop
                for j in 0 to 3 loop
                  v.z(n, j).r(15 downto 0)  := sat(shift_right(trspwrout(n, j).r, ir), 16);
                  v.z(n, j).i(15 downto 0)  := sat(shift_right(trspwrout(n, j).i, ir), 16);
                end loop;
              end loop;
            else
              v.z       := trspwrout;
            end if;
          end if;
          stage7.data <= v;
        end if;
      end if;
    end if;
  end process output_processing;

  -- FT
  -- X channel
  pss2tmp.enr     <= valid.ftx when memx = tmp else '0';
  pss2tmp.addr(0) <= add.ftx(0) when rr = '0' else add.ftx(1);
  pss2tmp.addr(1) <= add.ftx(1) when rr = '0' else add.ftx(0);

  -- Z channel
  pss2tmp.enw     <= valid.ftz when memz = tmp else '0';

  -- TMP
  pss2tmp.din(0) <= stage7.data.z(0, 0).r & stage7.data.z(0, 0).i & stage7.data.z(0, 1).r & stage7.data.z(0, 1).i &
                   stage7.data.z(0, 2).r & stage7.data.z(0, 2).i & stage7.data.z(0, 3).r & stage7.data.z(0, 3).i when rw = '0' else
                   stage7.data.z(1, 0).r & stage7.data.z(1, 0).i & stage7.data.z(1, 1).r & stage7.data.z(1, 1).i &
                   stage7.data.z(1, 2).r & stage7.data.z(1, 2).i & stage7.data.z(1, 3).r & stage7.data.z(1, 3).i;
  pss2tmp.din(1) <= stage7.data.z(0, 0).r & stage7.data.z(0, 0).i & stage7.data.z(0, 1).r & stage7.data.z(0, 1).i &
                   stage7.data.z(0, 2).r & stage7.data.z(0, 2).i & stage7.data.z(0, 3).r & stage7.data.z(0, 3).i when rw = '1' else
                   stage7.data.z(1, 0).r & stage7.data.z(1, 0).i & stage7.data.z(1, 1).r & stage7.data.z(1, 1).i &
                   stage7.data.z(1, 2).r & stage7.data.z(1, 2).i & stage7.data.z(1, 3).r & stage7.data.z(1, 3).i;

  pss2tmp.addw(0)    <= add.ftz(0) when rw = '0' else add.ftz(1);
  pss2tmp.addw(1)    <= add.ftz(1) when rw = '0' else add.ftz(0);

  pss2mio_p: process(ftnvp, stage7, valid, memx, iqx, add, memz, iqz, vppss2mio)
  begin
    pss2mio <= pss2mio_none;
    if ftnvp then
      for c in 0 to 3 loop
        for p in 0 to 1 loop
          pss2mio.wdata(p)(127 - 32 * c downto 112 - 32 * c) <= std_ulogic_vector(stage7.data.z(p, c).r(15 downto 0));
          pss2mio.wdata(p)(111 - 32 * c downto 96 - 32 * c) <= std_ulogic_vector(stage7.data.z(p, c).i(15 downto 0));
        end loop;
      end loop;
      if valid.ftx = '1' and memx = mio then
        for r in 0 to 3 loop
          if iqx = r then
            pss2mio.lock(r) <= '1';
            for p in 0 to 1 loop
              for c in 0 to 1 loop
                pss2mio.add(r, c, p) <= add.wftx(p);
              end loop;
              for c in 0 to 7 loop
                pss2mio.en(r, c, p) <= '1';
              end loop;
            end loop;
          end if;
        end loop;
      elsif valid.ftz = '1' and memz = mio then
        for r in 0 to 3 loop
          if iqz = r then
            pss2mio.lock(r) <= '1';
            pss2mio.rnw(r) <= '0';
            for p in 0 to 1 loop
              for c in 0 to 1 loop
                pss2mio.add(r, c, p) <= add.wftz(p);
              end loop;
              for c in 0 to 7 loop
                pss2mio.en(r, c, p) <= '1';
                pss2mio.be(c, p) <= (others => '1');
              end loop;
            end loop;
            if add.wftz(0) = add.wftz(1) then
              for c in 0 to 7 loop
                pss2mio.en(r, c, 0) <= '0';
              end loop;
            end if;
          end if;
        end loop;
      end if;
    else -- Vector operations
      pss2mio <= vppss2mio;
    end if;
  end process pss2mio_p;

  pss2twd.en    <= valid.fttw when ftnvp else '0';

  twd_g: for i in 0 to 2 generate
      pss2twd.add(0, i) <= add.fttw(i);
      pss2twd.add(1, i) <= add.fttw(3 + i);
  end generate;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
