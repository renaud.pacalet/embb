--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Prototype vpxy_address generator for FEP vector operations, read part
--*
--*  PLease see the FEP user guide for a complete description of the address
--* generation algorithm. This module is synchronous on the rising edge of its
--* clock CLK. Its reset SRSTN is synchronous and active low.
--* vpxy_address_generator samples the address generation parameters when its LOAD
--* input is asserted on the rising edge of CLK. On the following rising edge
--* of CLK, if its RUN input is asserted, it puts the two first byte
--* addresses on its two output busses A0 and A1. Every time RUN is
--* asserted vpxy_address_generator outputs the two next addresses. When RUN is
--* de-asserted vpxy_address_generator is stalled. RUN is ignored when LOAD is
--* asserted. The generated addresses are byte addresses in a Mio bank (2^14
--* bytes), represented on 14 bits. The DSO output is asserted when A0 and
--* A1 carry a valid pair of addresses.
--*
--* Important note: this module is a very delicate piece of control; it has been
--* validated by extensive simulations. It synthesizes on a LX330 Virtex 5
--* 5VLX330FF1760-2 target with a 200 MHz clock frequency and 225 LUTs. On a 65 nm
--* ASIC target, worst case corner, it synthesizes with a 450 MHz clock frequency
--* and 4000 square microns. Modify at your own risks. Do not use a modified
--* version before running the regression tests:
--* $ make vpxy_address_generator_sim.sim

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package type_pkg is

  type std_ulogic_array is array(natural range <>, natural range <>) of std_ulogic;
  type std_ulogic_cube is array(natural range <>, natural range <>, natural range <>) of std_ulogic;
  subtype v2 is std_ulogic_vector(1 downto 0);
  subtype v3 is std_ulogic_vector(2 downto 0);
  subtype v4 is std_ulogic_vector(3 downto 0);
  subtype vt4 is std_ulogic_vector(0 to 3);
  subtype v8 is std_ulogic_vector(7 downto 0);
  subtype v9 is std_ulogic_vector(8 downto 0);
  subtype v10 is std_ulogic_vector(9 downto 0);
  subtype v11 is std_ulogic_vector(10 downto 0);
  subtype v12 is std_ulogic_vector(11 downto 0);
  subtype v13 is std_ulogic_vector(12 downto 0);
  subtype v14 is std_ulogic_vector(13 downto 0);
  subtype v16 is std_ulogic_vector(15 downto 0);
  subtype v32 is std_ulogic_vector(31 downto 0);
  subtype v50 is std_ulogic_vector(49 downto 0);
  subtype v128 is std_ulogic_vector(127 downto 0);
  subtype v200 is std_ulogic_vector(199 downto 0);
  type v2_vector  is array(natural range <>) of v2;
  type v2_array  is array(natural range <>, natural range <>) of v2;
  type v3_vector  is array(natural range <>) of v3;
  type v4_vector  is array(natural range <>) of v4;
  type v4_array   is array(natural range <>, natural range <>) of v4;
  type v8_vector is array(natural range <>) of v8;
  type v10_vector is array(natural range <>) of v10;
  type v10_array   is array(natural range <>, natural range <>) of v10;
  type v10_cube is array(natural range <>, natural range <>, natural range <>) of v10;
  type v11_vector is array(natural range <>) of v11;
  type v14_vector is array(natural range <>) of v14;
  type v14_array   is array(natural range <>, natural range <>) of v14;
  type v16_vector is array(natural range <>) of v16;
  type v16_array   is array(natural range <>, natural range <>) of v16;
  type v16_cube is array(natural range <>, natural range <>, natural range <>) of v16;
  type v32_vector is array(natural range <>) of v32;
  type v32_array  is array(natural range <>, natural range <>) of v32;
  type v50_vector is array(natural range <>) of v50;
  type v50_array  is array(natural range <>, natural range <>) of v50;
  type v128_vector is array(natural range <>) of v128;
  type v128_array is array(natural range <>, natural range <>) of v128;
  type v200_vector is array(natural range <>) of v200;

  subtype vt8 is std_ulogic_vector(0 to 7);
  type vt8_vector is array(natural range <>) of vt8;

  subtype u2 is u_unsigned(1 downto 0); -- 2 bits u_unsigned
  subtype u4 is u_unsigned(3 downto 0); -- 4 bits u_unsigned
  subtype u7 is u_unsigned(6 downto 0); -- 7 bits u_unsigned
  subtype u8 is u_unsigned(7 downto 0); -- 8 bits u_unsigned
  subtype u9 is u_unsigned(8 downto 0); -- 8 bits u_unsigned
  subtype u10 is u_unsigned(9 downto 0); -- 10 bits u_unsigned
  subtype u11 is u_unsigned(10 downto 0); -- 11 bits u_unsigned
  type u11_vector is array (natural range <>) of u11; -- vector of u11
  subtype u12 is u_unsigned(11 downto 0); -- 12 bits u_unsigned
  subtype u13 is u_unsigned(12 downto 0); -- 13 bits u_unsigned
  subtype u14 is u_unsigned(13 downto 0); -- 15 bits u_unsigned
  type u14_vector is array (natural range <>) of u14; -- vector of u14
  subtype u15 is u_unsigned(14 downto 0); -- 15 bits u_unsigned
  type u15_vector is array(natural range <>) of u15; -- vector of u15
  subtype u16 is u_unsigned(15 downto 0); -- 16 bits u_unsigned
  type u16_vector is array(natural range <>) of u16; -- vector of u16
  subtype u32 is u_unsigned(31 downto 0); -- 32 bits u_unsigned
  subtype u128 is u_unsigned(127 downto 0); -- 128 bits u_unsigned

  subtype s14 is u_signed(13 downto 0);    -- 14 bits u_signed
  type s14_vector is array (natural range <>) of s14;
  subtype s15 is u_signed(14 downto 0);    -- 15 bits u_signed
  subtype s16 is u_signed(15 downto 0);    -- 16 bits u_signed
  type s16_vector is array (natural range <>) of s16;
  subtype s17 is u_signed(16 downto 0);    -- 17 bits u_signed
  subtype s18 is u_signed(17 downto 0);    -- 18 bits u_signed
  subtype s24 is u_signed(23 downto 0);    -- 24 bits u_signed
  subtype s25 is u_signed(24 downto 0);    -- 25 bits u_signed
  type s25_vector is array (natural range <>) of s25;
  subtype s32 is u_signed(31 downto 0);    -- 32 bits u_signed
  type s32_vector is array (natural range <>) of s32;
  type s32_array  is array (natural range <>, natural range <>) of s32;
  subtype s33 is u_signed(32 downto 0);    -- 33 bits u_signed
  type s33_vector is array (natural range <>) of s33;
  subtype s41 is u_signed(40 downto 0);    -- 41 bits u_signed
  subtype s42 is u_signed(41 downto 0);    -- 42 bits u_signed
  subtype s43 is u_signed(42 downto 0);    -- 43 bits u_signed
  subtype s47 is u_signed(46 downto 0);    -- 47 bits u_signed
  subtype s50 is u_signed(49 downto 0);    -- 50 bits u_signed
  type s50_vector is array(natural range <>) of s50;
  type s50_array  is array(natural range <>, natural range <>) of s50;
  subtype s64 is u_signed(63 downto 0);
  subtype s200 is u_signed(199 downto 0);
  type s200_vector is array(natural range <>) of s200;

  type cpx30 is record
    r, i: s15;
  end record;
  type cpx32 is record -- complex with 16 bits u_signed real and imaginary parts
    r, i: s16;
  end record;
  type cpx32_vector is array(natural range <>) of cpx32; -- vector of cpx32
  type cpx32_array  is array(natural range <>, natural range <>) of cpx32;
  type cpx34 is record
    r, i: s17;
  end record;
  type cpx34_vector is array(natural range <>) of cpx34;
  type cpx34_array  is array(natural range <>, natural range <>) of cpx34;
  type cpx36 is record
    r, i: s18;
  end record;
  type cpx36_vector is array(natural range <>) of cpx36;
  type cpx48 is record -- complex with 24 bits u_signed real and imaginary parts
    r, i: s24;
  end record;
  type cpx48_vector is array(natural range <>) of cpx48; -- vector of cpx48
  type cpx50 is record -- complex with 25 bits u_signed real and imaginary parts
    r, i: s25;
  end record;
  type cpx50_vector is array(natural range <>) of cpx50; -- vector of cpx50
  type cpx50_array  is array(natural range <>, natural range <>) of cpx50;
  type cpx66 is record -- complex with 33 bits u_signed real and imaginary parts
    r, i: s33;
  end record;
  type cpx66_vector is array(natural range <>) of cpx66; -- vector of cpx66
  type cpx66_array  is array(natural range <>, natural range <>) of cpx66;
  type cpx64 is record 
    r, i: s32;
  end record;
  type cpx94 is record
    r, i: s47;
  end record;

  type vp_components_left is (zero, one, two, more);

end package type_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
