--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief FEP's RAMs

library ieee;
use ieee.std_logic_1164.all;

library memories_lib;
use memories_lib.ram_pkg.all;

use work.type_pkg.all;
use work.fep_pkg.all;

entity rams is
  port(clk:      in  std_ulogic;
       mss2rams: in  mss2rams_type;
       rams2mss: out rams2mss_type);
end entity rams;

architecture rtl of rams is

begin

  ram_ucr: entity memories_lib.tdpram(arc)
    generic map(
-- pragma translate_off
                init_file     => "", -- initialization file
                si            => 0,  -- first address of initialization file
                instance_name => "ucr", -- Name of the RAM instance.
-- pragma translate_on
                registered => true,  -- control output registers
                na         => 10,    -- bit-width of address busses
                nb         => 4)     -- byte-width of data busses
    port map(clk   => clk,
             ena   => mss2rams.ucr(0).en,
             enb   => mss2rams.ucr(1).en,
             wea   => mss2rams.ucr(0).we,
             web   => mss2rams.ucr(1).we,
             adda  => mss2rams.ucr(0).add,
             addb  => mss2rams.ucr(1).add,
             dina  => mss2rams.ucr(0).wdata,
             dinb  => mss2rams.ucr(1).wdata,
             douta => rams2mss.ucr(0),
             doutb => rams2mss.ucr(1));

  g_mio_row: for r in 0 to 3 generate
    g_mio_col: for c in 0 to 7 generate
      ram_mio: entity memories_lib.tdpram(arc)
        generic map(
-- pragma translate_off
                    init_file     => "", -- initialization file
                    si            => 0,  -- first address of initialization file
                    instance_name => "mio" & integer'image(r) & integer'image(c),-- Name of the RAM instance.
-- pragma translate_on
                    registered => true,  -- control output registers
                    na         => 10,    -- bit-width of address busses
                    nb         => 2)     -- byte-width of data busses
        port map(clk => clk,
                 ena   => mss2rams.mio(r, c, 0).en,
                 enb   => mss2rams.mio(r, c, 1).en,
                 wea   => mss2rams.mio(r, c, 0).we,
                 web   => mss2rams.mio(r, c, 1).we,
                 adda  => mss2rams.mio(r, c, 0).add,
                 addb  => mss2rams.mio(r, c, 1).add,
                 dina  => mss2rams.mio(r, c, 0).wdata,
                 dinb  => mss2rams.mio(r, c, 1).wdata,
                 douta => rams2mss.mio(r, c, 0),
                 doutb => rams2mss.mio(r, c, 1));
    end generate g_mio_col;
  end generate g_mio_row;

  -- Twiddle factors memories
  twd_mems: entity work.twd(rtl)
    port map(clk => clk,
             pss2twd => mss2rams.twd,
             twd2pss => rams2mss.twd);

  -- TMP memories
  tmp_g: for n in 0 to 1 generate
    tmp_mems: entity memories_lib.sdpram(arc)
    generic map(
-- pragma translate_off
                init_file     => "", -- initialization file
                si            => 0,  -- first address of initialization file
                instance_name => "dpram" & integer'image(n), -- Name of the RAM instance.
-- pragma translate_on
                registered => true,  -- control output registers
                na         => 10,    -- bit-width of address busses
                nb         => 25)    -- bit-width of data busses
    port map(clk  => clk,
             enr  => mss2rams.tmp.enr,
             enw  => mss2rams.tmp.enw,
             we   => (others => '1'),
             addr => mss2rams.tmp.addr(n),
             addw => mss2rams.tmp.addw(n),
             din  => v200(mss2rams.tmp.din(n)),
             dout => rams2mss.tmp(n));
  end generate tmp_g;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
