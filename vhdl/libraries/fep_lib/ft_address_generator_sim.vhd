--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

use work.type_pkg.all;
use work.fep_pkg.all;

entity ft_address_generator_sim is
end entity ft_address_generator_sim;

architecture sim of ft_address_generator_sim is
  
  constant gb  : integer := 0;
  constant gq  : integer := 0;
  signal   gl  : integer := 256;

  signal clk, ce, load, es, run, rnw, srstn, valid : std_ulogic;
  signal eos    : boolean;
  signal lstage : std_ulogic;
  signal l      : v10;
  signal b      : v9;
  signal mem    : memory_type;
  signal a      : v10_vector(1 downto 0);
  signal twa    : pss2twd_addresses;

begin

  ag: entity work.ftz_address_generator(rtl)
  port map(
    clk    => clk,
    srstn  => srstn,
    ce     => ce,
    load   => load,
    run    => run,
    l      => l,
    b      => b,
    eos    => es,
    lstage => lstage,
    valid  => valid,
    mem    => mem,
    a0     => a(0),
    a1     => a(1)
  );

  clock: process
  begin
    clk <= '0';
    wait for 1 ns;
    clk <= '1';
    wait for 1 ns;
    if eos then
      wait;
    end if;
  end process clock;

  stim: process
  begin
    load  <= '0';
    run   <= '0';
    rnw   <= '0';
    srstn <= '0';
    ce    <= '0';
    b     <= (others => '0');
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    srstn <= '1';
    ce    <= '1';
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    load  <= '1';
    b    <= std_ulogic_vector(to_unsigned(gb, 9));
    l    <= std_ulogic_vector(to_unsigned(gl, 10));
    wait until rising_edge(clk);
    load  <= '0';
    run    <= '1';
    while es = '0' loop
      if es = '1' then 
        run    <= '0';
      end if;
      wait until rising_edge(clk);
    end loop;
    rnw      <= '1';
    load    <= '1';
    b      <= std_ulogic_vector(to_unsigned(gb, 9));
    l      <= std_ulogic_vector(to_unsigned(gl, 10));
    wait until rising_edge(clk);
    load  <= '0';
    run    <= '1';
    while es = '0' loop
      if es = '1' then 
        run    <= '0';
      end if;
      wait until rising_edge(clk);
    end loop;
    eos <= true;
    wait;
  end process stim;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
