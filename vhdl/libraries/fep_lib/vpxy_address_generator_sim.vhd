--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment vpxy_address_generator

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use ieee.numeric_std.all;

-- Utility package
package vpxy_address_generator_sim_pkg is

  -- Integer-based parameters
  type nparam is record
    t: natural range 0 to 3;
    b: natural range 0 to 2**14 - 1;
    s: natural range 0 to 1;
    n: natural range 0 to 127;
    m: natural range 0 to 255;
    p: natural range 0 to 2**14 - 1;
    w: natural range 0 to 3;
  end record;

  -- Vector-based parameters
  type sparam is record
    t: u_unsigned(1 downto 0);
    b: u_unsigned(13 downto 0);
    s: std_ulogic;
    n: u_unsigned(6 downto 0);
    m: u_unsigned(7 downto 0);
    p: u_unsigned(13 downto 0);
    w: u_unsigned(1 downto 0);
  end record;

  -- Conversion functions between nparam and sparam
  function n2s(n: nparam) return sparam;
  function s2n(s: sparam) return nparam;

  impure function nparam_rnd return nparam; -- Random generator of parameters
  -- The ad function computes a byte address according to the address generation
  -- algorithm. bw is the byte-width of components (depends on p.t) and se is
  -- the byte-length of the wrapping section (depends on w.t). i is the position
  -- of the requested byte address in the sequence, starting at i=0.
  function ad(p: nparam; bw: natural; se: natural; i: natural) return natural;
  -- Write "p / q" in l.
  procedure write(l: inout line; p: natural; q: natural);
  -- Write parameters p in l.
  procedure write(l: inout line; p: in nparam);
  -- Write "(idx,idx+1): (a0,a1)" in l
  procedure write_pair(l: inout line; idx: in natural; a0: in natural;
    a1: in natural; en0: std_ulogic_vector(0 to 7);
    en1: std_ulogic_vector(0 to 7));

end package vpxy_address_generator_sim_pkg;

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;

library random_lib;
use random_lib.rnd.all;

library global_lib;
use ieee.numeric_std.all;
use global_lib.sim_utils.all;

use work.vpxy_address_generator_sim_pkg.all;

entity vpxy_address_generator_sim is
  generic(nvec: natural := 1000; -- Number of random tests
          debug: natural := integer'high); -- Start verbose output at test #debug
end entity vpxy_address_generator_sim;

architecture sim of vpxy_address_generator_sim is
  
  signal clk, ce, load, run, srstn  :std_ulogic;
  signal eos: boolean;
  signal sp: sparam;
  signal a0:  u_unsigned(13 downto 0);
  signal a1:  u_unsigned(13 downto 0);
  signal en0: std_ulogic_vector(0 to 7);
  signal en1: std_ulogic_vector(0 to 7);
  signal dso:  std_ulogic;
 
begin

  ag: entity work.vpxy_address_generator(rtl)
    port map(clk   => clk,
             srstn => srstn,
             ce    => ce,
             load  => load,
             run   => run,
             t     => sp.t,
             b     => sp.b,
             s     => sp.s,
             n     => sp.n,
             m     => sp.m,
             p     => sp.p,
             w     => sp.w,
             a0    => a0,
             a1    => a1,
             en0   => en0,
             en1   => en1);

  clock: process
  begin
    clk <= '0';
    wait for 1 ns;
    clk <= '1';
    wait for 1 ns;
    if eos then
      wait;
    end if;
  end process clock;

  -- Randomize the run signal
  run_pr: process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        run <= '0';
      elsif ce = '1' then
        run <= std_ulogic_rnd;
      end if;
    end if;
  end process run_pr;

  stim: process
    variable np: nparam;
    variable cnt: natural;
    variable l: line;
  begin
    srstn <= '0';
    ce <= '0';
    load  <= '0';
    np := (0, 0, 0, 0, 0, 0, 0);
    sp <= n2s(np);
    cnt := 0;
    for i in 1 to 10 loop -- 10 cycles reset
      wait until rising_edge(clk);
    end loop;
    srstn <= '1';
    ce <= '1';
    for i in 1 to 10 loop -- 10 idle cycles
      wait until rising_edge(clk);
    end loop;
    -- Run directed tests (b=0, n,m,p<=7)
    write(l, string'("Directed tests:"));
    writeline(output, l);
    for t in 0 to 3 loop
      for s in 0 to 1 loop
        for n in 0 to 7 loop
          for m in 0 to 7 loop
            next when m = 1;
            for p in 0 to 7 loop
              for w in 0 to 1 loop
                cnt := cnt + 1;
                if cnt mod ((7 * 1024) / 100) = 0 then
                  progress(cnt, 7 * 1024);
                end if;
                for j in 1 to int_rnd(0, 10) loop -- 0 to 10 idle cycles
                  wait until rising_edge(clk);
                end loop;
                load <= '1';
                np := (t, 0, s, n, m, p, w);
                sp <= n2s(np);
                wait until rising_edge(clk);
                load  <= '0';
                for i in 1 to 100 loop -- 100 cycles
                  wait until rising_edge(clk);
                end loop;
              end loop;
            end loop;
          end loop;
        end loop;
      end loop;
    end loop;
    -- Run random tests
    cnt := 0;
    write(l, string'("Random tests:"));
    writeline(output, l);
    for i in 1 to nvec loop -- nvec random tests
      cnt := cnt + 1;
      if cnt mod (nvec / 100) = 0 then
        progress(cnt, nvec);
      end if;
      for j in 1 to int_rnd(0, 10) loop -- 0 to 10 idle cycles
        wait until rising_edge(clk);
      end loop;
      load <= '1';
      np := nparam_rnd;
      sp <= n2s(np);
      wait until rising_edge(clk);
      load  <= '0';
      for j in 1 to 10000 loop
        wait until rising_edge(clk);
      end loop;
    end loop;
    eos <= true; -- End of simulation
    report "Regression test passed";
    wait;
  end process stim;

  -- Verifier
  process(clk)
    variable l: line;
    variable np: nparam;
    variable bw, se: natural;
    variable idx, i: natural;
    variable u0, u1, v0, v1: natural;
    variable e0, e1: std_ulogic_vector(0 to 7);
  begin
    if rising_edge(clk) then
      if srstn = '0' then -- Reset
        np := (t => 0, b => 0, s => 0, n => 0, m => 0, p => 0, w => 0);
        bw := 1;
        se := 2**11;
        idx := 0;
        i := 0;
        u0 := 0;
        u1 := 0;
        v0 := 0;
        v1 := 0;
        dso <= '0';
      elsif ce = '1' then
        if load = '1' then -- Load parameters
          i := i + 1;
          np := s2n(sp);
          bw := np.t + 1; -- Components' byte-width
          if bw = 3 then bw := 2; end if;
          se := 2**(11 + np.w); -- Wrapping section byte-length
          idx := 0; -- Initialize indices counter
          if i >= debug then -- If debug print parameters
            write(l, np);
            writeline(output, l);
          end if;
          dso <= '0';
        elsif i > 0 then
          dso <= run;
          if dso = '1' then -- If valid output addresses
            u0 := to_integer(a0);
            u1 := to_integer(a1);
            v0 := ad(np, bw, se, idx); -- Compute reference first address
            v1 := ad(np, bw, se, idx + 1); -- Compute reference second address
            e0 := (others => '0');
            e0((v0 / 2) mod 8) := '1';
            e1 := (others => '0');
            e1((v1 / 2) mod 8) := '1';
            if np.t = 3 then
              e0 := e0 or ('0' & e0(0 to 6));
              e1 := e1 or ('0' & e1(0 to 6));
            end if;
            if u0 /= v0 or u1 /= v1 or en0 /= e0 or en1 /= e1 then -- If mismatch
              write(l, np); -- Print error information
              writeline(output, l);
              write_pair(l, idx, u0, u1, en0, en1);
              write(l, string'(" should be: "));
              write_pair(l, idx, v0, v1, e0, e1);
              assert false -- Crash simulation
              report l.all
              severity failure;
            end if;
            if i >= debug then -- Enter verbose debug section
              write_pair(l, idx, u0, u1, en0, en1); -- Print indices and output addresses
              writeline(output, l);
            end if;
            idx := idx + 2; -- Increment indices counter
          end if;
        end if;
      end if;
    end if;
  end process;

end architecture sim;

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

-- Utility package body
package body vpxy_address_generator_sim_pkg is

  function n2s(n: nparam) return sparam is
    variable res: sparam;
  begin
    res.t := to_unsigned(n.t, 2);
    res.b := to_unsigned(n.b, 14);
    if n.s = 0 then res.s := '0'; else res.s := '1'; end if;
    res.n := to_unsigned(n.n, 7);
    res.m := to_unsigned(n.m, 8);
    res.p := to_unsigned(n.p, 14);
    res.w := to_unsigned(n.w, 2);
    return res;
  end function n2s;

  function s2n(s: sparam) return nparam is
    variable res: nparam;
  begin
    res.t := to_integer(s.t);
    res.b := to_integer(s.b);
    if s.s = '0' then res.s := 0; else res.s := 1; end if;
    res.n := to_integer(s.n);
    res.m := to_integer(s.m);
    res.p := to_integer(s.p);
    res.w := to_integer(s.w);
    return res;
  end function s2n;

  function ad(p: nparam; bw: natural; se: natural; i: natural) return natural is
    variable u: integer;
    variable j, b0, b1: natural;
  begin
    b0 := (p.b * bw) mod 2**14;
    b1 := b0 mod se;
    b0 := b0 - b1;
    if p.p = 0 then
      j := i;
    else
      j := i mod p.p;
    end if;
    u := j * p.n;
    if p.m /= 0 then
      u := u + j / p.m;
    end if;
    if p.s = 1 then
      u := -u;
    end if;
    u := u * bw;
    u := b0 + ((b1 + u) mod se);
    return u;
  end function ad;

  procedure write(l: inout line; p: natural; q: natural) is
  begin
    write(l, p);
    write(l, string'("/"));
    write(l, q);
  end procedure write;

  procedure write(l: inout line; p: in nparam) is
  begin
    write(l, string'("t="));
    write(l, p.t);
    write(l, string'(", b="));
    write(l, p.b);
    write(l, string'(", s="));
    write(l, p.s);
    write(l, string'(", n="));
    write(l, p.n);
    write(l, string'(", m="));
    write(l, p.m);
    write(l, string'(", p="));
    write(l, p.p);
    write(l, string'(", w="));
    write(l, p.w);
  end procedure write;

  impure function nparam_rnd return nparam is
    variable p: nparam;
  begin
    p.t := int_rnd(0, 3);
    p.b := int_rnd(0, 2**14-1);
    p.s := int_rnd(0, 1);
    p.n := int_rnd(0, 127);
    p.m := int_rnd(0, 254);
    if p.m = 1 then p.m := p.m + 1; end if;
    p.p := int_rnd(0, 99);
    p.w := int_rnd(0, 3);
    return p;
  end function nparam_rnd;

  procedure write_pair(l: inout line; idx: in natural; a0: in natural;
    a1: in natural; en0: std_ulogic_vector(0 to 7);
    en1: std_ulogic_vector(0 to 7)) is
  begin
    write(l, string'("("));
    write(l, idx);
    write(l, string'(","));
    write(l, idx + 1);
    write(l, string'(") : ("));
    write(l, a0);
    write(l, string'(","));
    write(l, a1);
    write(l, string'(") ("));
    write(l, to_bitvector(en0));
    write(l, string'(","));
    write(l, to_bitvector(en1));
    write(l, string'(")"));
  end procedure write_pair;

end package body vpxy_address_generator_sim_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
