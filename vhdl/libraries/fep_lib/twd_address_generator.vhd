--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Addresses generator for Fourrier transform operations of FEP

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use ieee.numeric_std.all;
use global_lib.utils.all;

use work.type_pkg.all;
use work.fep_pkg.all;
use work.pu_pkg.all;

entity twd_address_generator is
  port(clk  : in  std_ulogic; -- clock
       srstn  : in  std_ulogic; -- reset
       ce:      in  std_ulogic; -- chip enable
       load  : in  std_ulogic; -- load parameters
       run  : in  std_ulogic; -- run addresses generator
       l  : in std_ulogic_vector(12 downto 0); -- length
       twdmod  : out twiddle_modifier_vector(5 downto 0); -- twiddle modifier values
       valid  : out std_ulogic; -- valid addresses
       add  : out v10_vector(5 downto 0));
end entity twd_address_generator;

architecture rtl of twd_address_generator is

  type u13_vector is array (natural range <>) of u_unsigned(12 downto 0);
  type u10_vector is array (natural range <>) of std_ulogic_vector(9  downto 0);
  type u23_vector is array (natural range <>) of std_ulogic_vector(22 downto 0);

  constant fepmaxsize  : natural  := 4096;
--  signal a         : u_unsigned(9 downto 0);
  signal ls          : std_ulogic_vector(12 downto 0);
  signal cnt         : std_ulogic_vector(9 downto 0);
  signal d         : natural range 0 to 12;
  signal d0         : natural range 0 to 9;
  signal i_s         : u13_vector(5 downto 0);
  signal val         : std_ulogic;
  signal cmsk : std_ulogic_vector(9  downto 0);

begin

  process(clk)
    variable i0, i1 : u13_vector(5 downto 0);
    variable tmp4    : u_unsigned(3 downto 0);
    variable cntv    : std_ulogic_vector(9 downto 0);
    variable c    : u10_vector(1 downto 0);
    variable tmp13:   u13;
    variable t    : u23_vector(1 downto 0);
    variable m    : twiddle_modifier_vector(5 downto 0);
  begin
    if clk'event and clk = '1' then
      for n in 0 to 5 loop
        i0(n)  := (others => '0');
        i1(n)  := (others => '0');
      end loop;
      for i in 0 to 1 loop
        c(i)  := (others => '0');
      end loop;
      if srstn = '0' then
        cnt   <= (others => '0');
        ls    <= (others => '0');
        tmp4   := (others => '0');
        cntv  := (others => '0');
        valid <= '0';
        d     <= 0;
        d0    <= 0;
        valid <= '0';
        val   <= '0';
        for n in 0 to 1 loop
          t(n)  := (others => '0');
        end loop;
        for n in 0 to 5 loop
          i_s(n)<= (others => '0');
        end loop;
        cmsk <= (others => '0');
        add  <= (others => (others => '0'));
      elsif ce = '1' then
        val   <= run;
        valid <= val;
        if load = '1' then
          cntv      := (others =>'0');
          ls      <= l(12 downto 0);
          d      <= fep_log2(l);
          d0      <= fep_log2(std_ulogic_vector(shift_right(u_unsigned(l),3)));
          tmp13 := shift_right(u_unsigned(l) - 1, 2);
          cmsk <= std_ulogic_vector(tmp13(9 downto 0));
        elsif run = '1' then
          cntv    := std_ulogic_vector(u_unsigned(cnt) + 1);
          if cntv(d0) = '1' and ls(1) = '0' and ls (0) = '0' then
            cntv      := (others =>'0');
            ls        <= std_ulogic_vector(shift_right(u_unsigned(ls), 2));
            d        <= d - 2;
            cmsk <= shift_right(cmsk, 2);
          end if;
        end if;
        cnt  <= cntv;
        tmp4  := (others => '0'); -- address
        for i in 0 to 1 loop -- address
          c(i)   := std_ulogic_vector(i + shift_left(u_unsigned(cnt), 1));
          t(i)   := (others => '0');
          t(i)(9 downto 0) := cmsk and c(i);
        end loop;
        i0(0) := fepmaxsize - resize(shift_right(shift_left(u_unsigned(t(0)), 12), d), 13); -- v0 * 4096/a
        i0(1) := fepmaxsize - resize(shift_right(shift_left(u_unsigned(t(0)), 13), d), 13); -- 2 * v0 * 4096/a
        i0(2) := fepmaxsize - resize(shift_right(shift_left(u_unsigned(t(0)), 13), d), 13) - resize(shift_right(shift_left(u_unsigned(t(0)), 12), d), 13); -- (2 + 1) * v0 * 4096/a
        i0(3) := fepmaxsize - resize(shift_right(shift_left(u_unsigned(t(1)), 12), d), 13); -- v1 * 4096/a
        i0(4) := fepmaxsize - resize(shift_right(shift_left(u_unsigned(t(1)), 13), d), 13); -- 2 * v1 * 4096/a
        i0(5) := fepmaxsize - resize(shift_right(shift_left(u_unsigned(t(1)), 13), d), 13) - resize(shift_right(shift_left(u_unsigned(t(1)), 12), d), 13); -- (2 + 1) * v1 * 4096/a
        for n in 0 to 5 loop
          i_s(n)  <=  i0(n);
          m(n)    := (false, false, false);
          tmp4    :=  i_s(n)(12 downto 9);
          if tmp4 >= "0001" then -- i > 511
            if tmp4 < "0010" then -- i < 1024
              i1(n)     := 1024 - i_s(n);
              m(n).swap := false;
              m(n).negr := false;
              m(n).negi := false;
            elsif tmp4 < "0011" then -- i < 1536
              i1(n)  := i_s(n) - 1024;
              m(n).swap := true;
              m(n).negr := true;
              m(n).negi := false;
            elsif tmp4 < "0100" then -- i < 2048
              i1(n)  := 2048 - i_s(n);
              m(n).swap := false;
              m(n).negr := true;
              m(n).negi := false;
            elsif tmp4 < "0101" then -- i < 2560
              i1(n)  := i_s(n) - 2048;
              m(n).swap := false;
              m(n).negr := true;
              m(n).negi := true;
            elsif tmp4 < "0110" then -- i < 3072
              i1(n)  := 3072 - i_s(n);
              m(n).swap := true;
              m(n).negr := true;
              m(n).negi := true;
            elsif tmp4 < "0111" then -- i < 3584
              i1(n)  := i_s(n) - 3072;
              m(n).swap := true;
              m(n).negr := false;
              m(n).negi := true;
            elsif tmp4 < "1000" then -- i < 4096
              i1(n)  := 4096 - i_s(n);
              m(n).swap := false;
              m(n).negr := false;
              m(n).negi := true;
            end if;
          end if;
        end loop;
        twdmod  <= m; -- twiddle modifiers
        for n in 0 to 5 loop
          add(n)  <= std_ulogic_vector(resize(i1(n), 10)); -- addresses
        end loop;
      end if;
    end if;
  end process;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
