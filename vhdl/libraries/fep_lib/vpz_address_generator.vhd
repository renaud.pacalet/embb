--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Prototype address generator for FEP vector operations, write part
--*
--*  PLease see the FEP user guide for a complete description of the address
--* generation algorithm. This module is synchronous on the rising edge of its
--* clock 'clk'. Its reset 'srstn' is synchronous and active low.
--* vpz_address_generator samples the address generation parameters when its 'load'
--* input is asserted on the rising edge of 'clk'. On the following rising edge
--* of 'clk', if its 'run' input is asserted, it puts the two first byte
--* addresses on its two output busses 'a0' and 'a1'. Every time 'run' is
--* asserted vpz_address_generator outputs the two next addresses. When 'run' is
--* de-asserted vpz_address_generator is stalled. 'run' is ignored when 'load' is
--* asserted. The generated addresses are byte addresses in a Mio bank (2^14
--* bytes), represented on 14 bits.
--*
--* Important note: this module is a very delicate piece of control; it has been
--* validated by extensive simulations. It synthesizes on a LX330 Virtex 5
--* 5VLX330FF1760-2 target with a 200 MHz clock frequency and 123 LUTs. On a 65 nm
--* ASIC target, worst case corner, it synthesizes with a 496 MHz clock frequency
--* and 2580 square microns. Modify at your own risks. Do not use a modified
--* version before running the regression tests:
--* $ make vpz_address_generator_sim.sim

library ieee;
use ieee.std_logic_1164.all;

library global_lib;

use ieee.numeric_std.all;
use global_lib.utils.all;

use work.type_pkg.all;

entity vpz_address_generator is
  port(clk  : in  std_ulogic;
       srstn: in  std_ulogic;
       ce:    in  std_ulogic;
       load : in  std_ulogic;
       run  : in  std_ulogic;
       t    : in  u2;
       b    : in  u14;
       s    : in  std_ulogic;
       n    : in  u7;
       m    : in  u8;
       w    : in  u2;
       a0   : out u14;
       a1   : out u14;
       be0  : out v16;
       be1  : out v16);
end entity vpz_address_generator;

architecture rtl of vpz_address_generator is

  -- Signals related to management of non-integer indices increments (m
  -- parameter):
  -- m0 is the register for the m parameter
  -- mr is the register for the m-periodic counter
  signal m0, mr: u8;
  -- Control signals
  signal freeze_mr_d, freeze_mr_q, freeze_mr, mr_eq0, mr_eq1, mr_eq2, mr_less_than_4: boolean;
  -- Inputs and outputs of the subtracter
  signal m_sub_in1, m_sub_out: u8;
  signal m_sub_in2: natural range 1 to 2;

  -- Signals related to components' indices:
  -- ir is the register for the current component index
  signal ir: u14;
  -- Inputs and outputs of the first adder
  signal i_add_1_in1, i_add_1_in2, i_add_1_out: u14;
  signal i_add_1_cin: natural range 0 to 1;
  -- Inputs and outputs of the second adder
  signal i_add_2_in1, i_add_2_in2, i_add_2_out: u14;
  signal i_add_2_cin: natural range 0 to 1;

  -- Signals related to components' byte addresses
  signal a_in: u14_vector(0 to 1);
  signal be_in : v16_vector(0 to 1);

  -- Control and parameter signals
  signal load_1: boolean; -- load delayed by one cycle
  signal tr: u2; -- Register for t parameter
  signal sr: std_ulogic;           -- Register for s parameter
  signal nr: u7; -- Register for n parameter
  signal wr: u2; -- Register for w parameter

begin

  mr_less_than_4 <= or_reduce(mr(7 downto 2)) = '0';   -- mr less than 4
  mr_eq0 <= mr_less_than_4 and (mr(1 downto 0) = "00"); -- mr equal 0
  mr_eq1 <= mr_less_than_4 and (mr(1 downto 0) = "01"); -- mr equal 1
  mr_eq2 <= mr_less_than_4 and (mr(1 downto 0) = "10"); -- mr equal 2

  -- m_sub_out=(mr==1)?m-1:mr-2
  m_sub_in1 <= m0 when mr_eq1 else mr;
  m_sub_in2 <= 1 when mr_eq1 else 2;
  m_sub_out <= m_sub_in1 - m_sub_in2;

  -- freeze_mr_d, freeze_mr_q and freeze_mr are used to freeze the mr register
  -- when m==0.
  freeze_mr_d <= mr_eq0; -- mr==0
  freeze_mr <= (load_1 and freeze_mr_d) or freeze_mr_q;
                                             
  -- m-periodic counter. mr cycles from m downto 1 and back to m, two values at
  -- a time. Examples:
  -- m=0 => mr=0,0,0,0,...
  -- m=1 => mr=1,1,1,1,...
  -- m=2 => mr=2,2,2,2,...
  -- m=3 => mr=3,1,2,3,1,2,3,1,2,3,...
  -- m=4 => mr=4,2,4,2,4,2,...
  -- m=5 => mr=5,3,1,4,2,5,3,1,4,...
  m_pr: process(clk)
  begin
    if rising_edge(clk) then
      if ce = '1' then
        if load = '1' then -- Load parameter m
          m0        <= m;
          mr        <= m;
          freeze_mr_q <= false;
        else
          if load_1 then -- One clock cycle after loading parameters
            freeze_mr_q <= freeze_mr_d;
          end if;
          if run = '1' and (not freeze_mr) then -- Regular run and mr not frozen
            if mr_eq2 then -- mr==2
              mr <= m0; -- Cycle and start new m-period at beginning (mr=m)
            else -- Continue current m-period
              mr <= m_sub_out; -- mr=(mr==1)?m-1:mr-2
            end if;
          end if;
        end if;
      end if;
    end if;
  end process m_pr;

  -- Arithmetic operations for components' indices. The FEP user guide defines
  -- the index increment as:
  -- inc=(-1)^s*n       when m==0
  -- inc=(-1)^s*(n+1/m) when m>1 (note that m==1 is invalid)
  --
  -- The following two adders are used to compute two indices at a time, in an
  -- incremental way. The ir register always stores idx(2i) (where idx(0)=b is
  -- the first index). The wrapping around Mio banks and banks subsections is
  -- implemented in later stages. The computation of the idx(2i) and idx(2i+1)
  -- is done modulo 2^14, ignoring the t and w parameters. The content of the ir
  -- register is used to compute the first output byte address a0.
  --
  -- From ir=idx(2i) the first adder computes idx(2i+1)=idx(2i)+(-1)^s*(n+d)
  -- where d=0 or d=1 is used to implement the 1/m fractional part of the
  -- increment; d is set to 1 when the m-periodic counter wraps from 1 to m-1,
  -- that is, when the mr register equals 1 (n==1):
  -- mr:        3  1  2  3  1  2
  -- idx(2i):   0  2  5  8 10 13
  -- idx(2i+1): 1  4  6  9 12 14
  -- d:         0  1  0  0  1  0
  --
  -- idx(2i+1) is used to compute the second output byte address a1.
  --
  -- All in all, the first adder computes one of the following operations
  -- (modulo 2^14):
  -- (ir)+(n)       +0 = ir+ n    when sr==0 and mr!=1
  -- (ir)+(2^14-n-1)+1 = ir- n    when sr==1 and mr!=1
  -- (ir)+(n)       +1 = ir+(n+1) when sr==0 and mr==1
  -- (ir)+(2^14-n-1)+0 = ir-(n+1) when sr==1 and mr==1
  --
  -- First input of first adder: b
  i_add_1_in1 <= ir;
  -- Second input of first adder: (s==0)?n:2^14-n-1
  i_add_1_in2 <= "0000000" & nr when sr = '0' else "1111111" & (not nr);
  -- Input carry of first adder
  i_add_1_cin <= 0 when sr = '0' and (not mr_eq1) else
                 1 when sr = '1' and (not mr_eq1) else
                 1 when sr = '0' and mr_eq1 else
                 0;
  -- First adder
  i_add_1_out <= i_add_1_in1 + i_add_1_in2 + i_add_1_cin;

  -- The second adder is used to compute the next value to store in ir:
  -- idx(2i+2)=idx(2i)+(-1)^s*(2*n+e) where e=0 or e=1 is used to implement the
  -- 1/m fractional part of the increment; e is set to 1 when the m-periodic
  -- counter wraps from 1 to m-1 or from 2 to m, that is, when the mr register
  -- equals 1 or 2 (n==1):
  -- mr:        3  1  2  3  1  2
  -- idx(2i):   0  2  5  8 10 13
  -- idx(2i+2): 2  5  8 10 13 16
  -- e:         0  1  1  0  1  1
  -- Note that because m!=1 there is no need for e to take value 2: from idx(2i)
  -- to idx(2i+1) there is at most one skipped value.
  --
  -- All in all, the second adder computes one of the following operations
  -- (modulo 2^14):
  -- (ir)+(2n)       +1 = ir+(2n+1) when sr==0 and (mr==1 or mr==2)
  -- (ir)+(2^14-2n-1)+0 = ir-(2n+1) when sr==1 and (mr==1 or mr==2)
  -- (ir)+(2n)       +0 = ir+ 2n    when sr==0 and mr!=1 and mr!=2
  -- (ir)+(2^14-2n-1)+1 = ir- 2n    when sr==1 and mr!=1 and mr!=2
  --
  -- First input of second adder: ir
  i_add_2_in1 <= ir;
  -- Second input of second adder: (s==0)?2n:2^14-2n-1
  i_add_2_in2 <= "000000" & nr & '0' when sr = '0' else "111111" & (not nr) & '1';
  -- Input carry of second adder
  i_add_2_cin <= 1 when sr = '0' and (mr_eq1 or mr_eq2) else
                 0 when sr = '1' and (mr_eq1 or mr_eq2) else
                 1 when sr = '1' else
                 0;
  -- Second adder
  i_add_2_out <= i_add_2_in1 + i_add_2_in2 + i_add_2_cin;

  -- Indices registers. ir stores idx(2i) and is also set at load time to
  -- parameter b.
  i_pr: process(clk)
  begin
    if rising_edge(clk) then
      if ce = '1' then
        if load = '1' then -- Load parameters
          ir        <= b;
        else
          if run = '1' then -- If running
            ir <= i_add_2_out; -- ir=idx(2i+2)
          end if;
        end if;
      end if;
    end if;
  end process i_pr;

  -- a_in(0) is an input of the a0 output register. It is the byte-address version
  -- of the ir index register. Depending on the byte-width of the vector's
  -- components (parameter t, stored in register tr), we apply a left shift by
  -- 0, 1 or 2 positions:
  a_in(0) <= ir when tr = "00" else
             ir(11 downto 0) & "00" when tr = "11" else
             ir(12 downto 0) & '0';

  -- As a_in(0), a_in(1) is an input of the a1 output register. It is a left-shifted
  -- version of the component's index, by 0, 1 or 2 positions, depending on tr.
  a_in(1) <= i_add_1_out when tr = "00" else
             i_add_1_out(11 downto 0) & "00" when tr = "11" else
             i_add_1_out(12 downto 0) & '0';

  -- Output write byte enables
  be_p: process(tr, a_in)
    variable t16: u16;
    variable t8: u8;
    variable t4: u4;
  begin
    for i in 0 to 1 loop
      be_in(i) <= (others => '0');
      t16 := decode(a_in(i)(3 downto 0));
      t8 := decode(a_in(i)(3 downto 1));
      t4 := decode(a_in(i)(3 downto 2));
      case tr is
        when "11" =>
          for j in 15 downto 0 loop
            be_in(i)(j) <= t4(j / 4);
          end loop;
        when "00" =>
          for j in 15 downto 0 loop
            be_in(i)(j) <= t16(j);
          end loop;
        when others =>
          for j in 15 downto 0 loop
            be_in(i)(j) <= t8(j / 2);
          end loop;
      end case;
    end loop;
  end process be_p;

  -- Manages the a0 and a1 output registers; Applies the wrapping around Mio
  -- banks and banks subsections (parameter w). Also manages the data strobe out
  -- signal that indicates valid outputs.
  a_pr: process(clk)
  begin
    if rising_edge(clk) then
      if load = '1' then
        a0  <= (others => '0');
        a1  <= (others => '0');
        be0 <= (others => '0');
        be1 <= (others => '0');
      elsif ce = '1' then
        if load_1 then -- One cycle after loading parameters
          a0  <= a_in(0); -- Initialize output registers to base byte address
          a1  <= a_in(0); -- (that is, byte address of first component).
          be0 <= be_in(0);
          be1 <= be_in(0);
        end if;
        if run = '1' then -- If running
        -- Depending on size of wrapping sections (parameter w, stored in
        -- register wr), overwrite more or less LSBs of a0 and a1 with values
        -- coming from a_in
          case wr is
            when "00" => a0(10 downto 0) <= a_in(0)(10 downto 0);
                         a1(10 downto 0) <= a_in(1)(10 downto 0);
            when "01" => a0(11 downto 0) <= a_in(0)(11 downto 0);
                         a1(11 downto 0) <= a_in(1)(11 downto 0);
            when "10" => a0(12 downto 0) <= a_in(0)(12 downto 0);
                         a1(12 downto 0) <= a_in(1)(12 downto 0);
            when others => a0 <= a_in(0);
                           a1 <= a_in(1);
          end case;
          be0 <= be_in(0);
          be1 <= be_in(1);
        end if;
      end if;
    end if;
  end process a_pr;

  -- Control and parameters storage
  ctrl_pr: process(clk)
  begin
    if rising_edge(clk) then
      if ce = '1' then
        load_1 <= false; -- By default we are not one cycle after parameters loading
        if load = '1' then -- If parameters loading
          load_1 <= true; -- One-cycle-delayed load
          tr     <= t; -- Store parameter t in register tr
          sr     <= s; -- Store parameter t in register sr
          nr     <= n; -- Store parameter t in register nr
          wr     <= w; -- Store parameter t in register wr
        end if;
      end if;
    end if;
  end process ctrl_pr;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
