--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Utility package for the FEP
--*
--* The fep_pkg package defines constants for the different address
--* used to interface the PSS and the memory subsystem.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- pragma translate_off
use std.textio.all;

library random_lib;
use random_lib.rnd.all;
-- pragma translate_on

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

library memories_lib;
use memories_lib.ram_pkg.all;

use work.bitfield.all;
use work.type_pkg.all;
use work.errors.all;

package fep_pkg is

  -- one PSS to TWD read channel (address only)
  type pss2twd_addresses is array(0 to 1, 0 to 2) of std_ulogic_vector(9 downto 0);
  -- 6 PSS to TWD read addresses
  type pss2twd_type is record
    en: std_ulogic; -- read enable
    add: pss2twd_addresses; -- addresses
  end record;
  constant pss2twd_none: pss2twd_type := (en => '0', add => (others => (others => (others => '0'))));
  -- one TWD to PSS read channel
  type twd2pss_type is array  (0 to 1, 0 to 2) of std_ulogic_vector(31 downto 0);

  -- FT TMP command interface
  type pss2tmp_type is record -- one PSS to MIO read/write channel
    enr   : std_ulogic; -- read enable channel
    enw   : std_ulogic; -- read enable channel
    addr  : v10_vector(0 to 1); -- read address 50-byte aligned
    addw  : v10_vector(0 to 1); -- write address 50-byte aligned
    din   : s200_vector(0 to 1); -- write data 8 * 50 bits
  end record;
  constant pss2tmp_none: pss2tmp_type := (enr => '0', enw => '0', addr => (others => (others => '0')), addw => (others => (others => '0')),
                                        din => (others => (others => '0')));
  subtype tmp2pss_type is v200_vector(0 to 1);

  type pss2mio_type is record
    lock: vt4; -- row locks: 4 rows
    en: std_ulogic_cube(0 to 3, 0 to 7, 0 to 1); -- enables: 4 rows x 8 columns x 2 ports
    rnw: vt4; -- read-not-write: 4 rows
    add: v10_cube(0 to 3, 0 to 1, 0 to 1); -- addresses: 4 rows x first(0)-others(1) columns x 2 ports
    be: v2_array(0 to 7, 0 to 1); -- byte write enables: 8 columns x 2 ports
    wdata: v128_vector(0 to 1); -- write date: 2 ports
  end record;
  constant pss2mio_none: pss2mio_type := (
    lock  => (others => '0'),
    en    => (others => (others => (others => '0'))),
    rnw   => (others => '1'),
    add   => (others => (others => (others => (others => '0')))),
    be    => (others => (others => (others => '0'))),
    wdata => (others => (others => '0')));

  subtype mio2pss_type is v128_array(0 to 3, 0 to 1); -- 4 rows x 2 ports

  type pss2mss_type is record
    mio: pss2mio_type;
    twd: pss2twd_type;
    tmp: pss2tmp_type;
  end record;
  constant pss2mss_none: pss2mss_type := (mio => pss2mio_none, twd => pss2twd_none, tmp => pss2tmp_none);

  type mss2pss_type is record
    mio: mio2pss_type;
    twd: twd2pss_type;
    tmp: tmp2pss_type;
  end record;

  type mio_in_type is array(0 to 3, 0 to 7, 0 to 1) of in_port_1kx16;
  type ucr_in_type is array(0 to 1) of in_port_1kx32;

  type mss2rams_type is record
    ucr: ucr_in_type;
    mio: mio_in_type;
    twd: pss2twd_type;
    tmp: pss2tmp_type;
  end record;
  type mss2rams_vector is array(natural range <>) of mss2rams_type;

  type mio_out_type is array(0 to 3, 0 to 7, 0 to 1) of word16;
  type ucr_out_type is array(0 to 1) of word32;

  type rams2mss_type is record
    ucr: ucr_out_type;
    mio: mio_out_type;
    twd: twd2pss_type;
    tmp: tmp2pss_type;
  end record;
  type rams2mss_vector is array(natural range <>) of rams2mss_type;

  procedure check_param(cmd: in bitfield_type; err: out std_ulogic; status: out status_type);

  -- pragma translate_off
  impure function check return boolean;
  -- pragma translate_on

  -- returns the log base 2 of l, assuming l is a power of 2
  function fep_log2(l: in std_ulogic_vector) return natural;

  type memory_type is (mio, tmp);

  constant cmdsize:  positive := bitfield_width/8;

  constant  int8_type: v2 := "00";
  constant int16_type: v2 := "01";
  constant cpx16_type: v2 := "10";
  constant cpx32_type: v2 := "11";

  constant sma_z_only: v2 := "01";
  constant sma_s_only: v2 := "10";
  constant sma_both: v2 := "11";

  constant param0: std_ulogic_vector(bitfield_width - 1 downto 0) := (others => '0');
  constant cmd_none: bitfield_type := bitfield_slice(param0);

  type pu_cmd_type is record
    op      : std_ulogic_vector(2 downto 0); -- operation
    last    : std_ulogic; -- fourrier transform last stage
    btfly2  : std_ulogic;
  end record;

  constant cwaop  : std_ulogic_vector(2 downto 0) := "000";
  constant cwpop  : std_ulogic_vector(2 downto 0) := "001";
  constant cwmop  : std_ulogic_vector(2 downto 0) := "010";
  constant cwsop  : std_ulogic_vector(2 downto 0) := "011";
  constant movop  : std_ulogic_vector(2 downto 0) := "100";
  constant cwlop  : std_ulogic_vector(2 downto 0) := "101";
  constant fftop  : std_ulogic_vector(2 downto 0) := "110";

  type out_values_type is record
    z        : cpx50_array(1 downto 0, 3 downto 0);
    sum        : cpx64;
    max        : cpx32;
    min        : cpx32;
    argmax    : cpx32;
    argmin    : cpx32;
  end record;

  type vp_values_type is record
    z        :  cpx66_vector(1 downto 0);
    sum        : cpx94;
    argmin    : cpx30;
    argmax    : cpx30;
    min        : cpx66;
    max        : cpx66;
  end record;

  function output_conversion_1(z: cpx66; cmd: bitfield_type; mul: boolean) return cpx36;
  function output_conversion_2(z: cpx36; cmd: bitfield_type; int: boolean) return v32;
  function sma_output_conversion(sum: cpx94; max: cpx66; argmax: cpx32; min: cpx66; argmin: cpx32; mul: boolean; r: natural range 0 to 7) return v128_vector;

  -- This function tests if v is a power of 4, knowing it is a power of 2
  function is_pow4(v: in std_ulogic_vector(14 downto 0)) return boolean;

  -- pragma translate_off
  function sat(l: integer; i: positive) return integer;
  -- pragma translate_on

  -- Saturates u_signed value l on i-bits, returning a i-bits u_signed result. l
  -- length must be larger or equal i. If l < -2^(i-1) returns -2^(i-1).
  -- If l > 2^(i-1)-1 returns 2^(i-1)-1. Else returns l(i-1 downto 0).
  function sat(l: u_signed; i: positive) return u_signed;

  type lut_info is record
    extrapolate: std_ulogic;
    add, add2: std_ulogic_vector(13 downto 0);
    c1: u15;
  end record;

  constant lut_info_none: lut_info := (
    extrapolate => '0',
    add => (others => '0'),
    add2 => (others => '0'),
    c1 => (others => '0'));

  function get_lut_info(x: v32; a: v2; cmd: bitfield_type) return lut_info;

end package fep_pkg;

package body fep_pkg is

  procedure check_param(cmd: in bitfield_type; err: out std_ulogic; status: out status_type) is
    variable ft, vp1, vp2, vp, qxeqqy, qxeqqz, qyeqqz, wz, p4, mxeq1, myeq1, mzeq1: boolean;
    variable il: natural range 0 to 2**15 - 1;
    variable ill: natural range 0 to 2**4 - 1;
  begin
    err := '0';
    status := (others => '0');
    ft := cmd.op = fftop;
    vp1 := cmd.op = cwmop or cmd.op = cwsop or cmd.op = movop;
    vp2 := cmd.op = cwaop or cmd.op = cwpop or cmd.op = cwlop;
    vp := vp1 or vp2;
    qxeqqy := cmd.qx = cmd.qy;
    qxeqqz := cmd.qx = cmd.qz;
    qyeqqz := cmd.qy = cmd.qz;
    wz := vp and cmd.sma /= sma_s_only;
    il := to_integer(u_unsigned(cmd.l));
    ill := to_integer(u_unsigned(cmd.ll));
    p4 := il = 8 or il = 16 or il = 32 or il = 64 or il = 128 or il = 256 or il = 512 or il = 1024 or il = 2048 or il = 4096;
    mxeq1 := cmd.mx = X"01";
    myeq1 := cmd.my = X"01";
    mzeq1 := cmd.mz = X"01";
    if (vp2 and qxeqqy) or (wz and (qxeqqz or (qyeqqz and vp2))) then
      err := '1';
      status(fep_bank_conflict) := '1';
-- pragma translate_off
      assert false
        report fep_invalid_base_index_message
        severity warning;
-- pragma translate_on
    end if;
    if ft and (cmd.bx(2 downto 0) /= "000" or cmd.bz(2 downto 0) /= "000") then
      err := '1';
      status(fep_invalid_base_index) := '1';
-- pragma translate_off
      assert false
        report fep_invalid_base_index_message
        severity warning;
-- pragma translate_on
    end if;
    if (vp and (il = 0 or il > 2**14)) or (ft and (not p4)) then
      err := '1';
      status(fep_invalid_length) := '1';
-- pragma translate_off
      assert false
        report fep_invalid_length_message
        severity warning;
-- pragma translate_on
    end if;
    if cmd.op = cwlop and (ill < 1 or ill > 14) then
      err := '1';
      status(fep_invalid_ll_value) := '1';
-- pragma translate_off
      assert false
        report fep_invalid_ll_value_message
        severity warning;
-- pragma translate_on
    end if;
    if cmd.op = cwlop and ((cmd.tx /= int8_type and cmd.tx /= int16_type) or (cmd.ty /= int8_type and cmd.ty /= int16_type)) then
      err := '1';
      status(fep_invalid_type) := '1';
-- pragma translate_off
      assert false
        report fep_invalid_type_message
        severity warning;
-- pragma translate_on
    end if;
    if vp and cmd.sma /= sma_z_only and cmd.sma /= sma_s_only and cmd.sma /= sma_both then
      err := '1';
      status(fep_invalid_sma_value) := '1';
-- pragma translate_off
      assert false
        report fep_invalid_sma_value_message
        severity warning;
-- pragma translate_on
    end if;
    if (not vp) and (not ft) then
      err := '1';
      status(fep_invalid_op_value) := '1';
-- pragma translate_off
      assert false
        report fep_invalid_op_value_message
        severity warning;
-- pragma translate_on
    end if;
    if (mxeq1 and vp) or (myeq1 and (cmd.op = cwaop or cmd.op = cwpop)) or (mzeq1 and wz) then
      err := '1';
      status(fep_invalid_m_value) := '1';
-- pragma translate_off
      assert false
        report fep_invalid_m_value_message
        severity warning;
-- pragma translate_on
    end if;
  end procedure check_param;

  -- pragma translate_off
  function check_param_check return boolean is
  begin
    return true;
    -- TBC
  end function check_param_check;
  -- pragma translate_on

  function fep_log2(l: in std_ulogic_vector) return natural is
    variable res: natural;
    constant n: natural := l'length;
    variable tmp: std_ulogic_vector(n - 1 downto 0) := l;
  begin
    res := 0;
    for i in 0 to n - 1 loop
      if tmp(i) = '1' then
        res := i;
      end if;
    end loop;
    return res;
  end function fep_log2;

  -- pragma translate_off
  impure function fep_log2_std_ulogic_vector_12_0_natural_check return boolean is
    variable fep_log2_l: std_ulogic_vector(255 downto 0);
    variable fep_log2_res: natural;
    variable l: line;
  begin
    fep_log2_res := fep_log2(fep_log2_l(-1 downto 0));
    if fep_log2_res /= 0 then
      write(l, string'("fep_log2: error"));
      writeline(output, l);
      return false;
    end if;
    for i in 0 to 255 loop
      fep_log2_l := (others => '0');
      for j in 0 to i loop
        fep_log2_l(j) := '1';
        fep_log2_res := fep_log2(fep_log2_l(i downto 0));
        if fep_log2_res /= j then
          write(l, string'("fep_log2: error"));
          writeline(output, l);
          return false;
        end if;
      end loop;
    end loop;
    return true;
  end function fep_log2_std_ulogic_vector_12_0_natural_check;
  -- pragma translate_on

  -- First stage of conversion of the raw output of a Processing Unit (PU)
  -- according the conversion-related parameter CMD.R and the kind of operation
  -- (with or without multiplications). For operations with multiplications
  -- (CWP, CWM, CWS, CWL) the raw PU result is first right-shifted by 15 positions.
  -- For all operations a right-shift by CMD.R positions takes place. The 18
  -- LSBs of the real and imaginary parts of the result form the output.
  function output_conversion_1(z: cpx66; cmd: bitfield_type; mul: boolean) return cpx36 is
    variable res: cpx36;
  begin
    if mul then
      res := (r => z.r(32 downto 15), i => z.i(32 downto 15));
    else
      res := (r => z.r(17 downto 0), i => z.i(17 downto 0));
    end if;
    if cmd.r(2) = '1' then
      res := (r => shift_right(res.r, 4), i => shift_right(res.i, 4));
    end if;
    if cmd.r(1) = '1' then
      res := (r => shift_right(res.r, 2), i => shift_right(res.i, 2));
    end if;
    if cmd.r(0) = '1' then
      res := (r => shift_right(res.r, 1), i => shift_right(res.i, 1));
    end if;
    return res;
  end function output_conversion_1;

  -- pragma translate_off
  impure function output_conversion_1_check return boolean is
    variable output_conversion_1_z, tmpz: cpx66;
    variable output_conversion_1_cmd: bitfield_type;
    variable output_conversion_1_mul: boolean;
    variable output_conversion_1_res, output_conversion_1_ref: cpx36;
    variable l: line;
  begin
    for r in 0 to 7 loop
      output_conversion_1_cmd.r := std_ulogic_vector(to_unsigned(r, 3));
      for z in 0 to 999 loop
        if int_rnd(0, 99) = 0 then
          output_conversion_1_z.r := to_signed(-2**16, 33);
        elsif int_rnd(0, 99) = 0 then
          output_conversion_1_z.r := to_signed(2**16, 33);
        else
          output_conversion_1_z.r := to_signed(int_rnd(-2**16+1, 2**16-1), 33);
        end if;
        if int_rnd(0, 99) = 0 then
          output_conversion_1_z.i := to_signed(-2**16, 33);
        elsif int_rnd(0, 99) = 0 then
          output_conversion_1_z.i := to_signed(2**16, 33);
        else
          output_conversion_1_z.i := to_signed(int_rnd(-2**16+1, 2**16-1), 33);
        end if;
        output_conversion_1_res := output_conversion_1(output_conversion_1_z, output_conversion_1_cmd, false);
        output_conversion_1_ref := (r => shift_right(output_conversion_1_z.r, r)(17 downto 0), i => shift_right(output_conversion_1_z.i, r)(17 downto 0));
        if output_conversion_1_res /= output_conversion_1_ref then
          write(l, string'("output_conversion_1: error"));
          writeline(output, l);
          return false;
        end if;
        if int_rnd(0, 99) = 0 then
          output_conversion_1_z.r := (32 => '1', others => '0');
        elsif int_rnd(0, 99) = 0 then
          output_conversion_1_z.r := (32 => '0', others => '1');
        else
          output_conversion_1_z.r := signed_rnd(33);
        end if;
        if int_rnd(0, 99) = 0 then
          output_conversion_1_z.i := (32 => '1', others => '0');
        elsif int_rnd(0, 99) = 0 then
          output_conversion_1_z.i := (32 => '0', others => '1');
        else
          output_conversion_1_z.i := signed_rnd(33);
        end if;
        output_conversion_1_res := output_conversion_1(output_conversion_1_z, output_conversion_1_cmd, true);
        output_conversion_1_ref := (r => shift_right(output_conversion_1_z.r, 15 + r)(17 downto 0), i => shift_right(output_conversion_1_z.i, 15 + r)(17 downto 0));
        if output_conversion_1_res /= output_conversion_1_ref then
          write(l, string'("output_conversion_1: error"));
          writeline(output, l);
          return false;
        end if;
      end loop;
    end loop;
    return true;
  end function output_conversion_1_check;
  -- pragma translate_on

  -- Second stage of conversion of the raw output of a Processing Unit (PU) according
  -- output type (CMD.TZ) and other conversion-related parameters (CMD.RI, CMD.ML).
  function output_conversion_2(z: cpx36; cmd: bitfield_type; int: boolean) return v32 is
    variable tmp36: cpx36 := z;
    variable tmp32: cpx32;
    variable res: v32;
  begin
    if int then
      if cmd.ri = '1' then
        tmp36.i := tmp36.r;
      else
        tmp36.r := tmp36.i;
      end if;
    end if;
    tmp32 := (r => sat(tmp36.r, 16), i => sat(tmp36.i, 16));
    case cmd.tz is
      when int8_type =>
        if cmd.ml = '1' then
          res(7 downto 0) := std_ulogic_vector(tmp32.r(15 downto 8));
        else
          res(7 downto 0) := std_ulogic_vector(sat(tmp32.r, 8));
        end if;
        res(31 downto 8) := res(7 downto 0) & res(7 downto 0) & res(7 downto 0);
      when int16_type =>
        res(15 downto 0) := std_ulogic_vector(tmp32.r);
        res(31 downto 16) := res(15 downto 0);
      when cpx16_type =>
        if cmd.ml = '1' then
          res(15 downto 0) := std_ulogic_vector(tmp32.r(15 downto 8)) & std_ulogic_vector(tmp32.i(15 downto 8));
        else
          res(15 downto 0) := std_ulogic_vector(sat(tmp32.r, 8)) & std_ulogic_vector(sat(tmp32.i, 8));
        end if;
        res(31 downto 16) := res(15 downto 0);
      when others =>
        res := std_ulogic_vector(tmp32.r) & std_ulogic_vector(tmp32.i);
    end case;
    return res;
  end function output_conversion_2;

  -- pragma translate_off
  impure function output_conversion_2_check return boolean is
    variable output_conversion_2_z, tmpz: cpx36;
    variable output_conversion_2_cmd: bitfield_type;
    variable output_conversion_2_int: boolean;
    variable output_conversion_2_res, output_conversion_2_ref: v32;
    variable tmpr, tmpi: integer;
    variable riml: std_ulogic_vector(0 to 1);
    variable l: line;
  begin
    for tz in 0 to 3 loop
      output_conversion_2_cmd.tz := std_ulogic_vector(to_unsigned(tz, 2));
      for rm in 0 to 3 loop
        riml := std_ulogic_vector(to_unsigned(rm, 2));
        output_conversion_2_cmd.ri := riml(0);
        output_conversion_2_cmd.ml := riml(1);
        for z in 0 to 99 loop
          if int_rnd(0, 99) = 0 then
            output_conversion_2_z.r := (17 => '1', others => '0');
          elsif int_rnd(0, 99) = 0 then
            output_conversion_2_z.r := (17 => '0', others => '1');
          else
            output_conversion_2_z.r := signed_rnd(18);
          end if;
          if int_rnd(0, 99) = 0 then
            output_conversion_2_z.i := (17 => '1', others => '0');
          elsif int_rnd(0, 99) = 0 then
            output_conversion_2_z.i := (17 => '0', others => '1');
          else
            output_conversion_2_z.i := signed_rnd(18);
          end if;
          output_conversion_2_res := output_conversion_2(output_conversion_2_z, output_conversion_2_cmd, output_conversion_2_cmd.tz(1) = '0');
          tmpr := to_integer(output_conversion_2_z.r);
          if tmpr < -2**15 then
            tmpr := -2**15;
          elsif tmpr >= 2**15 then
            tmpr := 2**15 - 1;
          end if;
          tmpi := to_integer(output_conversion_2_z.i);
          if tmpi < -2**15 then
            tmpi := -2**15;
          elsif tmpi >= 2**15 then
            tmpi := 2**15 - 1;
          end if;
          if output_conversion_2_cmd.tz = int8_type or output_conversion_2_cmd.tz = int16_type then
            if output_conversion_2_cmd.ri = '0' then
              tmpr := tmpi;
            else
              tmpi := tmpr;
            end if;
          end if;
          if output_conversion_2_cmd.tz = int8_type or output_conversion_2_cmd.tz = cpx16_type then
            if output_conversion_2_cmd.ml = '0' then
              if tmpr < -2**7 then
                tmpr := -2**7;
              elsif tmpr >= 2**7 then
                tmpr := 2**7 - 1;
              end if;
              if tmpi < -2**7 then
                tmpi := -2**7;
              elsif tmpi >= 2**7 then
                tmpi := 2**7 - 1;
              end if;
            else
              if tmpr >= 0 then
                tmpr := tmpr / 2**8;
              else
                tmpr := (tmpr - 2**8 + 1) / 2**8;
              end if;
              if tmpi >= 0 then
                tmpi := tmpi / 2**8;
              else
                tmpi := (tmpi - 2**8 + 1) / 2**8;
              end if;
            end if;
          end if;
          case output_conversion_2_cmd.tz is
            when int8_type =>
              output_conversion_2_ref(7 downto 0) := std_ulogic_vector(to_signed(tmpr, 8));
              output_conversion_2_ref(31 downto 8) := output_conversion_2_ref(7 downto 0) & output_conversion_2_ref(7 downto 0) & output_conversion_2_ref(7 downto 0);
              if output_conversion_2_res /= output_conversion_2_ref then
                write(l, string'("output_conversion_2: error"));
                writeline(output, l);
                return false;
              end if;
            when int16_type =>
              output_conversion_2_ref(15 downto 0) := std_ulogic_vector(to_signed(tmpr, 16));
              output_conversion_2_ref(31 downto 16) := output_conversion_2_ref(15 downto 0);
              if output_conversion_2_res /= output_conversion_2_ref then
                write(l, string'("output_conversion_2: error"));
                writeline(output, l);
                return false;
              end if;
            when cpx16_type =>
              output_conversion_2_ref(15 downto 8) := std_ulogic_vector(to_signed(tmpr, 8));
              output_conversion_2_ref(7 downto 0) := std_ulogic_vector(to_signed(tmpi, 8));
              output_conversion_2_ref(31 downto 16) := output_conversion_2_ref(15 downto 0);
              if output_conversion_2_res /= output_conversion_2_ref then
                write(l, string'("output_conversion_2: error"));
                writeline(output, l);
                return false;
              end if;
            when others =>
              output_conversion_2_ref(31 downto 16) := std_ulogic_vector(to_signed(tmpr, 16));
              output_conversion_2_ref(15 downto 0) := std_ulogic_vector(to_signed(tmpi, 16));
              if output_conversion_2_res /= output_conversion_2_ref then
                write(l, string'("output_conversion_2: error"));
                writeline(output, l);
                return false;
              end if;
          end case;
        end loop;
      end loop;
    end loop;
    return true;
  end function output_conversion_2_check;
  -- pragma translate_on

  function sma_output_conversion(sum: cpx94; max: cpx66; argmax: cpx32; min: cpx66; argmin: cpx32; mul: boolean; r: natural range 0 to 7) return v128_vector is
    variable res: v128_vector(0 to 1);
    variable sumv: cpx94;
    variable maxv, minv: cpx66;
    variable argmaxv, argminv: cpx32;
  begin
    res := (others => (others => '0'));
    sumv := sum;
    maxv := max;
    argmaxv := argmax;
    minv := min;
    argminv := argmin;
    if mul then
      sumv := (r => shift_right(sumv.r, 15), i => shift_right(sumv.i, 15));
      maxv := (r => shift_right(maxv.r, 15), i => shift_right(maxv.i, 15));
      minv := (r => shift_right(minv.r, 15), i => shift_right(minv.i, 15));
    end if;
    sumv := (r => shift_right(sumv.r, r), i => shift_right(sumv.i, r));
    maxv := (r => shift_right(maxv.r, r), i => shift_right(maxv.i, r));
    minv := (r => shift_right(minv.r, r), i => shift_right(minv.i, r));
    res(0)(127 downto 96) := std_ulogic_vector(sat(sumv.r, 32));
    res(0)(95 downto 64) := std_ulogic_vector(sat(sumv.i, 32));
    res(0)(63 downto 48) := std_ulogic_vector(sat(maxv.r, 16));
    res(0)(47 downto 32) := std_ulogic_vector(sat(maxv.i, 16));
    res(0)(31 downto 16) := std_ulogic_vector(sat(minv.r, 16));
    res(0)(15 downto 0) := std_ulogic_vector(sat(minv.i, 16));
    res(1)(127 downto 112) := std_ulogic_vector(argmaxv.r);
    res(1)(111 downto 96) := std_ulogic_vector(argmaxv.i);
    res(1)(95 downto 80) := std_ulogic_vector(argminv.r);
    res(1)(79 downto 64) := std_ulogic_vector(argminv.i);
    return res;
  end function sma_output_conversion;

  function is_pow4(v: in std_ulogic_vector(14 downto 0)) return boolean is
  begin
    return (v(1) or v(3) or v(5) or v(7) or v(9) or v(11) or v(13)) = '0';
  end function is_pow4;

  -- pragma translate_off
  impure function is_pow4_std_ulogic_vector_14_0_check return boolean is
    variable is_pow4_v: std_ulogic_vector(14 downto 0);
    variable is_pow4_res: boolean;
    variable l: line;
  begin
    for i in 0 to 14 loop
      is_pow4_v := (others => '0');
      is_pow4_v(i) := '1';
      is_pow4_res := is_pow4(is_pow4_v);
      if is_pow4_res /= (i mod 2 = 0) then
        write(l, string'("is_pow4: error"));
        writeline(output, l);
        return false;
      end if;
    end loop;
    return true;
  end function is_pow4_std_ulogic_vector_14_0_check;
  -- pragma translate_on

  function sat(l: u_signed; i: positive) return u_signed is
    constant n: natural := l'length;
    variable lv: u_signed(n - 1 downto 0) := l;
    variable res: u_signed(i - 1 downto 0);
  begin
    -- pragma translate_off
    assert i <= n
      report "sat: invalid parameters"
      severity failure;
    -- pragma translate_on
    if or_reduce(lv(n - 1 downto i - 1)) /= '0' and lv(n - 1) = '0' then -- l < -2^(i-1)
      res := (others => '1');
      res(i - 1) := '0'; -- res = -2^(i-1)
    elsif and_reduce(lv(n - 1 downto i - 1)) /= '1' and lv(n - 1) = '1' then -- l > 2^(i-1)-1
      res := (others => '0');
      res(i - 1) := '1'; -- res = 2^(i-1)-1
    else
      res := lv(i - 1 downto 0); -- res = l
    end if;
    return res;
  end function sat;

  -- pragma translate_off
  impure function sat_signed_positive_check return boolean is
    variable sat_l: u_signed(8 downto 1);
    variable sat_i: positive;
    variable sat_res: u_signed(8 downto 1);
    variable l: line;
  begin
    for i in 1 to 8 loop
      sat_i := i;
      for j in sat_i to 8 loop
        for k in -2**(j-1) to 2**(j-1)-1 loop
          sat_l(j downto 1) := to_signed(k, j);
          sat_res(sat_i downto 1) := sat(sat_l(j downto 1), sat_i);
          if sat_i = 1 then
            if sat_res(1) /= sat_l(j) then
              write(l, string'("sat: error (i="));
              write(l, sat_i);
              write(l, string'(",j="));
              write(l, j);
              write(l, string'(")"));
              writeline(output, l);
              return false;
            end if;
          elsif sat_i = j then
            if sat_res(sat_i downto 1) /= sat_l(sat_i downto 1) then
              write(l, string'("sat: error (i="));
              write(l, sat_i);
              write(l, string'(",j="));
              write(l, j);
              write(l, string'(")"));
              writeline(output, l);
              return false;
            end if;
          elsif or_reduce(sat_l(j downto sat_i)) /= '0' and sat_l(j) = '0' then
            if sat_res(sat_i) /= '0' or and_reduce(sat_res(sat_i - 1 downto 1)) /= '1' then
              write(l, string'("sat: error (i="));
              write(l, sat_i);
              write(l, string'(",j="));
              write(l, j);
              write(l, string'(")"));
              writeline(output, l);
              return false;
            end if;
          elsif and_reduce(sat_l(j downto sat_i)) /= '1' and sat_l(j) = '1' then
            if sat_res(sat_i) /= '1' or or_reduce(sat_res(sat_i - 1 downto 1)) /= '0' then
              write(l, string'("sat: error (i="));
              write(l, sat_i);
              write(l, string'(",j="));
              write(l, j);
              write(l, string'(")"));
              writeline(output, l);
              return false;
            end if;
          elsif sat_res(sat_i downto 1) /= sat_l(sat_i downto 1) then
              write(l, string'("sat: error (i="));
              write(l, sat_i);
              write(l, string'(",j="));
              write(l, j);
              write(l, string'(")"));
              writeline(output, l);
              return false;
          end if;
        end loop;
      end loop;
    end loop;
    return true;
  end function sat_signed_positive_check;
  -- pragma translate_on

  -- pragma translate_off
  function sat(l: integer; i: positive) return integer is
    variable res: integer;
  begin
    if l < -2**(i-1) then
      return -2**(i-1);
    elsif l > 2**(i-1)-1 then
      return 2**(i-1)-1;
    else
      return l;
    end if;
  end function sat;
  -- pragma translate_on

  function get_lut_info(x: v32; a: v2; cmd: bitfield_type) return lut_info is
    variable res: lut_info;
    variable xtmp: u_unsigned(31 downto 0);
    variable xint0, xint1: u_unsigned(31 downto 0);
    variable xfrac: u_unsigned(31 downto 0);
    variable by: u_unsigned(13 downto 0);
    variable ll: natural range 0 to 14;
    variable cs: v3;
  begin
    ll := to_integer(u_unsigned(cmd.ll));
    res := lut_info_none;
    cs := cmd.tx(0) & a;
    case cs is
      when "000" =>
        xtmp(15 downto 0) := u_unsigned(x(31 downto 24)) & x"00";
      when "001" =>
        xtmp(15 downto 0) := u_unsigned(x(23 downto 16)) & x"00";
      when "010" =>
        xtmp(15 downto 0) := u_unsigned(x(15 downto 8)) & x"00";
      when "011" =>
        xtmp(15 downto 0) := u_unsigned(x(7 downto 0)) & x"00";
      when "100" =>
        xtmp(15 downto 0) := u_unsigned(x(31 downto 16));
      when others =>
        xtmp(15 downto 0) := u_unsigned(x(15 downto 0));
    end case;
    if cmd.ls = '1' then
      xtmp(15) := not xtmp(15);
    end if;
    xtmp(31 downto 16) := X"0000";
    xint0 := shift_left(xtmp, ll);
    xtmp(31 downto 16) := X"ffff";
    xint1 := shift_left(xtmp, ll);
    xfrac := xint0;
    xint0 := shift_right(xint0, 16);
    xint1 := shift_right(xint1, 16);
    if cmd.li = '1' and and_reduce(xint1(13 downto 0)) = '1' then
      xint0(0) := '0';
      res.extrapolate := '1';
    else
      res.extrapolate := '0';
    end if;
    by := u_unsigned(cmd.by);
    if cmd.ty(0) = '1' then
      by := shift_left(by, 1);
      xint0 := shift_left(xint0, 1);
    end if;
    xint0 := xint0 + by;
    xint1 := xint0 + 2;
    case cmd.wy is
      when "00" =>
        xint0(13 downto 11) := by(13 downto 11);
        xint1(13 downto 11) := by(13 downto 11);
      when "01" =>
        xint0(13 downto 12) := by(13 downto 12);
        xint1(13 downto 12) := by(13 downto 12);
      when "10" =>
        xint0(13) := by(13);
        xint1(13) := by(13);
      when others => null;
    end case;
    res.add := std_ulogic_vector(xint0(13 downto 0));
    res.add2 := std_ulogic_vector(xint1(13 downto 0));
    res.c1 := xfrac(15 downto 1);
    return res;
  end function get_lut_info;

  -- pragma translate_off
  impure function get_lut_info_check return boolean is
    variable get_lut_info_x, tmpx: v32;
    variable get_lut_info_a: v2;
    variable get_lut_info_cmd: bitfield_type;
    variable get_lut_info_res, get_lut_info_ref: lut_info;
    variable tmp: std_ulogic_vector(0 to 11);
    variable ll, idx, a, a2, b0: natural;
    variable l: line;
  begin
    for i in 16#100# to 16#eff# loop
      tmp := std_ulogic_vector(to_unsigned(i, 12));
      get_lut_info_cmd.ll := tmp(0 to 3);
      ll := to_integer(u_unsigned(get_lut_info_cmd.ll));
      get_lut_info_cmd.tx(0) := tmp(4);
      get_lut_info_cmd.ls := tmp(5);
      get_lut_info_cmd.li := tmp(6);
      get_lut_info_cmd.ty(0) := tmp(7);
      get_lut_info_cmd.wy := tmp(8 to 9);
      get_lut_info_a := tmp(10 to 11);
      next when get_lut_info_cmd.tx(0) = '1' and get_lut_info_a(0) /= '0';
      for x in 0 to 9 loop
        get_lut_info_x := std_ulogic_vector_rnd(32);
        tmpx := shift_left(get_lut_info_x, to_integer(u_unsigned(get_lut_info_a)) * 8);
        tmpx := shift_right(tmpx, 16);
        if get_lut_info_cmd.tx(0) = '0' then
          tmpx(7 downto 0) := X"00";
        end if;
        if get_lut_info_cmd.ls = '1' then
          tmpx(15) := not tmpx(15);
        end if;
        idx := to_integer(u_unsigned(tmpx(15 downto 15 - ll + 1)));
        if get_lut_info_cmd.li = '1' and idx = 2**ll - 1 then
          idx := idx - 1;
          get_lut_info_ref.extrapolate := '1';
        else
          get_lut_info_ref.extrapolate := '0';
        end if;
        get_lut_info_ref.c1 := (others => '0');
        get_lut_info_ref.c1(14 downto ll - 1) := u_unsigned(tmpx(15 - ll downto 0));
        for b in 0 to 9 loop
          get_lut_info_cmd.by := std_ulogic_vector_rnd(14);
          get_lut_info_res := get_lut_info(get_lut_info_x, get_lut_info_a, get_lut_info_cmd);
          b0 := to_integer(u_unsigned(get_lut_info_cmd.by));
          a := idx + b0;
          if get_lut_info_cmd.ty(0) = '1' then
            a := 2 * a;
            b0 := 2 * b0;
          end if;
          a2 := a + 2;
          case get_lut_info_cmd.wy is
            when "00" => b0 := b0 - (b0 mod 2**11);
                         a := b0 + (a mod 2**11);
                         a2 := b0 + (a2 mod 2**11);
            when "01" => b0 := b0 - (b0 mod 2**12);
                         a := b0 + (a mod 2**12);
                         a2 := b0 + (a2 mod 2**12);
            when "10" => b0 := b0 - (b0 mod 2**13);
                         a := b0 + (a mod 2**13);
                         a2 := b0 + (a2 mod 2**13);
            when others => b0 := 0;
                           a := b0 + (a mod 2**14);
                           a2 := b0 + (a2 mod 2**14);
          end case;
          get_lut_info_ref.add := std_ulogic_vector(to_unsigned(a, 14));
          get_lut_info_ref.add2 := std_ulogic_vector(to_unsigned(a2, 14));
          if get_lut_info_res /= get_lut_info_ref then
            write(l, string'("get_lut_info:: error (i="));
            write(l, i);
            write(l, string'(")"));
            writeline(output, l);
            return false;
          end if;
        end loop;
      end loop;
    end loop;
    return true;
  end function get_lut_info_check;

  impure function check return boolean is
    variable good: boolean := true;
    variable l: line;
  begin
    write(l, string'("check_param_check..."));
    writeline(output, l);
    if not check_param_check then
      good := false;
      write(l, string'("FAILED"));
      writeline(output, l);
    else
      write(l, string'("PASSED"));
      writeline(output, l);
    end if;
    write(l, string'("fep_log2_std_ulogic_vector_12_0_natural_check..."));
    writeline(output, l);
    if not fep_log2_std_ulogic_vector_12_0_natural_check then
      good := false;
      write(l, string'("FAILED"));
      writeline(output, l);
    else
      write(l, string'("PASSED"));
      writeline(output, l);
    end if;
    write(l, string'("is_pow4_std_ulogic_vector_14_0_check..."));
    writeline(output, l);
    if not is_pow4_std_ulogic_vector_14_0_check then
      good := false;
      write(l, string'("FAILED"));
      writeline(output, l);
    else
      write(l, string'("PASSED"));
      writeline(output, l);
    end if;
--    write(l, string'("sat_signed_positive_check..."));
--    writeline(output, l);
--    if not sat_signed_positive_check then
--      good := false;
--      write(l, string'("FAILED"));
--      writeline(output, l);
--    else
--      write(l, string'("PASSED"));
--      writeline(output, l);
--    end if;
    write(l, string'("output_conversion_1_check..."));
    writeline(output, l);
    if not output_conversion_1_check then
      good := false;
      write(l, string'("FAILED"));
      writeline(output, l);
    else
      write(l, string'("PASSED"));
      writeline(output, l);
    end if;
    write(l, string'("output_conversion_2_check..."));
    writeline(output, l);
    if not output_conversion_2_check then
      good := false;
      write(l, string'("FAILED"));
      writeline(output, l);
    else
      write(l, string'("PASSED"));
      writeline(output, l);
    end if;
    write(l, string'("get_lut_info_check..."));
    writeline(output, l);
    if not get_lut_info_check then
      good := false;
      write(l, string'("FAILED"));
      writeline(output, l);
    else
      write(l, string'("PASSED"));
      writeline(output, l);
    end if;
    return good;
  end function check;
  -- pragma translate_on

end package body fep_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
