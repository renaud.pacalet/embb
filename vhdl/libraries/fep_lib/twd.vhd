--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Twiddle factors memories
--*
--*  The twiddle factors memories, TWD0, TWD1 and TWD2 are three 513x32 RAMs (1 RAMB36 each in Virtex V targets). Only the 512 first words are actually
--* implemented in RAMs. The 513th is wired to the constant value sqrt2 & sqrt2.

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use ieee.numeric_std.all;
use global_lib.global.all;

library memories_lib;
use memories_lib.ram_pkg.all;

use work.type_pkg.all;
use work.fep_pkg.all;
use work.twd_pkg.all;

entity twd is
  port(clk:    in  std_ulogic;
       -- PSS interface
       pss2twd: in  pss2twd_type;
       twd2pss: out twd2pss_type);
end entity twd;

architecture rtl of twd is

  -- When reading at address 512, sqrt2 & sqrt2 is returned
  constant sqrt2  : std_ulogic_vector(15 downto 0) := X"5A82";

  -- Flags indicating access to the 513th twiddle value
  type twd513_type is array(0 to 1, 0 to 2) of std_ulogic;
  signal twd513, twd513_q, twd513_d : twd513_type;

  type twd_in_type is array(0 to 2, 0 to 1) of in_port_1kx32;
  signal twd_in: twd_in_type; -- Input ports of the 3 TWD RAMs

  type twd_out_type is array(0 to 2, 0 to 1) of word32;
  signal twd_out: twd_out_type; -- Output ports of the 3 TWD RAMs

  type ramb_en is array(0 to 2, 0 to 1) of std_ulogic;
  signal enr: ramb_en;

  type ramb_dout is array(0 to 2, 0 to 1) of v32;
  signal doutr: ramb_dout;

begin

  -- Memories inputs
  process(pss2twd)
  begin
    for p in 0 to 1 loop -- for all ports (A and B)
      for n in 0 to 2 loop -- for all TWD RAMs
        -- Initialize the input ports to default (no request).
        twd_in(n, p) <= in_port_1kx32_default;
        twd513_d(p, n) <= '0'; -- No access to 513th twiddle factor
        if pss2twd.en = '1' then -- if TWD read channels enabled
          -- put read request on port of TWD block RAM
          twd513_d(p, n) <= pss2twd.add(p, n)(9);
          twd_in(n, p).en <= not pss2twd.add(p, n)(9);
          twd_in(n, p).add(8 downto 0) <= pss2twd.add(p, n)(8 downto 0);
        end if;
      end loop;
    end loop;
  end process;

  -- Propagate 513th twiddle access
  process(clk) -- 2 dffs
  begin
    if rising_edge(clk) then
      twd513	<= twd513_q;
      twd513_q	<= twd513_d;
    end if;
  end process;

  -- Multiplexing of block RAM outputs and sqrt2 & sqrt2 constant
  process(twd513, twd_out)
  begin
    for n in 0 to 2 loop -- for all TWD read channels
      for p in 0 to 1 loop
        if twd513(p, n) = '1' then
          twd2pss(p, n)  <= sqrt2 & sqrt2;
        else
          twd2pss(p, n)  <= twd_out(n, p);
        end if;
      end loop;
    end loop;
  end process;

  -------------------------------------------------------
  ------------------ BLOCK RAM INSTANCES ----------------
  -------------------------------------------------------
  ramb: process(clk)
    variable mem: v32_vector(0 to 511) := twd_factors;
  begin
    if rising_edge(clk) then
      for i in 0 to 2 loop
        for p in 0 to 1 loop
          enr(i, p) <= twd_in(i, p).en;
          if enr(i, p) = '1' then
            twd_out(i, p) <= doutr(i, p);
          end if;
          if twd_in(i, p).en = '1' then
            doutr(i, p) <= mem(to_integer(u_unsigned(twd_in(i, p).add)));
          end if;
        end loop;
      end loop;
    end if;
  end process ramb;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
