--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Top level of FEP dsp

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use ieee.numeric_std.all;
use global_lib.global.all;

library fep_lib;
use fep_lib.type_pkg.all;
use fep_lib.pu_pkg.all;

entity dsp is
  port(clk:   in  std_ulogic;
       srstn: in  std_ulogic;
       ce:    in  std_ulogic;
       dsi:   in  std_ulogic;
       di:    in  dsp_in;
       do:    out dsp_out);
end entity dsp;

architecture rtl of dsp is

  alias mode is di.mode;
  alias n0 is di.n0;
  alias n1 is di.n1;
  alias a0 is di.a0;
  alias a1 is di.a1;
  alias b0 is di.b0;
  alias b1 is di.b1;
  alias c0 is di.c0;
  alias c1 is di.c1;
  alias z0 is do.z0;
  alias z1 is do.z1;

  signal m0, m1: s41;
  signal mode_q: dsp_mode;
  signal n0_q, n1_q, adp, neg: boolean;
  signal a0_q, a1_q: s25;
  signal b0_q, b1_q: s16;
  signal c0_q, c1_q: s16;
  signal z0_d, z1_d: s43;
  signal z0_q, z1_q: s43;
  signal ad, op0, op1: s43;
  signal dsi1: std_ulogic;

begin

  m0 <= a0_q * b0_q;
  m1 <= a1_q * b1_q;
  ad <= op0 + op1 when adp else
        op0 - op1;

  process(mode_q, n0_q, n1_q, b1_q, c0_q, c1_q, m0, m1)
  begin
    op0 <= (others => '0');
    op1 <= (others => '0');
    adp <= true;
    case mode_q is
      when cwa_dsp =>
        op0(17 downto 0) <= resize(c1_q, 18);
        op1(17 downto 0) <= resize(b1_q, 18);
        adp <= not n1_q;
      when cwl_dsp =>
        op0 <= resize(m0, 43);
        op1 <= shift_left(resize(c0_q, 43), 15);
      when ft_dsp =>
        op0 <= resize(m0, 43);
        op1 <= resize(m1, 43);
        adp <= n0_q = n1_q;
      when others => null;
    end case;
  end process;

  process(mode_q, m0, m1, a0_q, b0_q, ad)
  begin
    z0_d <= (others => '0');
    z1_d <= (others => '0');
    case mode_q is
      when ftl2_dsp =>
        z0_d <= resize(m0, 43);
        z1_d <= resize(m1, 43);
      when ftl4_dsp =>
        z0_d(42 downto 18) <= a0_q;
      when mov_dsp =>
        z0_d(17 downto 0) <= resize(b0_q, 18);
      when others =>
        z0_d <= ad;
    end case;
  end process;

  process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        dsi1   <= '0';
      elsif ce = '1' then
        if dsi = '1' then
          mode_q <= mode;
          n0_q   <= n0;
          n1_q   <= n1;
          a0_q   <= a0;
          a1_q   <= a1;
          b0_q   <= b0;
          b1_q   <= b1;
          c0_q   <= c0;
          c1_q   <= c1;
        end if;
        dsi1 <= dsi;
        neg <= false;
        if dsi1 = '1' then
          if mode_q = cwa_dsp or mode_q = mov_dsp or mode_q = ft_dsp then
            neg <= n0_q;
          end if;
          z0_q <= z0_d;
          z1_q <= z1_d;
        end if;
      end if;
    end if;
  end process;

  z0 <= z0_q when not neg else
        -z0_q;
  z1 <= z1_q;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
