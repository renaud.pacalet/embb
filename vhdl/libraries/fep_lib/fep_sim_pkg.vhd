--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Utility package for the FEP simulation

use std.textio.all;

library ieee;
use ieee.numeric_std.all;

--pragma translate_off
library global_lib;
use global_lib.sim_utils.all;
--pragma translate_on

use work.type_pkg.all;

package fep_sim_pkg is

--pragma translate_off
  procedure print(val: in cpx32);
--pragma translate_on

end package fep_sim_pkg;

package body fep_sim_pkg is

--pragma translate_off
  procedure print(val: in cpx32) is
    variable l: line;
  begin
    write(l, string'("["));
    hwrite(l ,val.r);
    write(l, string'(","));
    hwrite(l ,val.i);
    write(l, string'("]"));
    writeline(output, l);
  end procedure print;
--pragma translate_on

end package body fep_sim_pkg;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
