--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for dsp

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use ieee.numeric_std.all;
use global_lib.global.all;
use global_lib.sim_utils.all;

library random_lib;
use random_lib.rnd.all;

use work.pu_pkg.all;
use work.type_pkg.all;

entity dsp_sim is
  generic(n: positive := 10000); -- Test vectors
end entity dsp_sim;

architecture sim of dsp_sim is

  signal clk: std_ulogic;
  signal srstn: std_ulogic;
  signal ce:    std_ulogic;
  signal dsi: std_ulogic;
  signal di: dsp_in;
  signal do: dsp_out;

  signal eos: boolean;

begin

  idsp: entity work.dsp
    port map(clk    => clk,
             srstn  => srstn,
             ce     => ce,
             dsi    => dsi,
             di     => di,
             do     => do);

  clock: process
  begin
    clk <= '0';
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
    if eos then wait; end if;
  end process clock;

  stim: process
    variable l: line;
    variable o, p: natural;
  begin
    srstn  <= '0';
    ce  <= '0';
    di <= dsp_di0;
    dsi <= '0';
    wait until rising_edge(clk);
    for i in 1 to n loop
      p := i mod (n / 100);
      if p = 0 then
        write(l, (i * 100) / n);
        write(l, string'("%"));
        writeline(output, l);
      end if;
      if int_rnd(1, 100) = 1 then
        srstn <= '0';
      else
        srstn <= '1';
      end if;
      if int_rnd(1, 100) = 1 then
        ce <= '0';
      else
        ce <= '1';
      end if;
      dsi <= std_ulogic_rnd;
      di <= dsp_rnd;
      wait until rising_edge(clk);
    end loop;
    dsi  <= '0';
    ce <= '1';
    for i in 1 to 10 loop
      wait until rising_edge(clk);
    end loop;
    eos <= true;
    report "Regression test passed";
    wait;
  end process stim;

  check: process(clk)
    variable tmp: s42;
    type dsp_in_vector is array(natural range <>) of dsp_in;
    variable dsi_pipe: std_ulogic_vector(dsp_pipe_depth downto 1) := (others => '0');
    variable di_pipe: dsp_in_vector(dsp_pipe_depth downto 1);
    variable do_ref: dsp_out;
    variable l: line;
    variable ok: boolean;
  begin
    if rising_edge(clk) then
      if dsi_pipe(1) = '1' then
        do_ref := dsp_ref(di_pipe(1));
        if di_pipe(1).mode = cwa_dsp or di_pipe(1).mode = mov_dsp then
          ok := do.z0(17 downto 0) = do_ref.z0(17 downto 0);
        elsif di_pipe(1).mode = cwl_dsp or di_pipe(1).mode = ft_dsp then
          ok := do.z0 = do_ref.z0;
        elsif di_pipe(1).mode = ftl2_dsp then
          ok := (do.z0 = do_ref.z0) and (do.z1 = do_ref.z1);
        else
          ok := do.z0(42 downto 18) = do_ref.z0(42 downto 18);
        end if;
        if not ok then
          print(di_pipe(1));
          print(l, "DSP returned:");
          writeline(output, l);
          print(do);
          print(l, "should be:");
          writeline(output, l);
          print(do_ref);
          assert false
          severity failure;
        end if;
      end if;
      if srstn = '0' then
        dsi_pipe := (others => '0');
      elsif ce = '1' then
        dsi_pipe := dsi & dsi_pipe(dsp_pipe_depth downto 2);
        di_pipe := di & di_pipe(dsp_pipe_depth downto 2);
      end if;
    end if;
  end process check;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
