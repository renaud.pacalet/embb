--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Utility package
--*
--*  Defines error codes and messages

library ieee;
use ieee.std_logic_1164.all;

package errors is

  --------------
  -- Error codes
  --------------
  --* Total number of error codes
  constant fep_num_error_codes:    natural                                 := 8;
  --* Invalid QX, QY, QZ combination
  constant fep_bank_conflict:      natural                                 := 0;
  --* FT operation with not multiple of 8 BX or BZ
  constant fep_invalid_base_index: natural                                 := 1;
  --* FT operation with L non power of 2 in 8-4096 range, or L=0 or L>2**14
  constant fep_invalid_length:     natural                                 := 2;
  --* CWL operation with LL=0 or LL> 14
  constant fep_invalid_ll_value:   natural                                 := 3;
  --* CWL operation with non integer TX or TY types
  constant fep_invalid_type:       natural                                 := 4;
  --* Invalid SMA value
  constant fep_invalid_sma_value:  natural                                 := 5;
  --* Invalid OP value
  constant fep_invalid_op_value:   natural                                 := 6;
  --* Address generator used with M=1 (MX, MY or MZ)
  constant fep_invalid_m_value:    natural                                 := 7;

-- pragma translate_off
  -----------------
  -- Error messages
  -----------------
  constant fep_bank_conflict_message:      string := "Invalid QX, QY, QZ combination";
  constant fep_invalid_base_index_message: string := "FT operation with not multiple of 8 BX or BZ";
  constant fep_invalid_length_message:     string := "FT operation with L non power of 2 in 8-4096 range, or L=0 or L>2**14";
  constant fep_invalid_ll_value_message:   string := "CWL operation with LL=0 or LL> 14";
  constant fep_invalid_type_message:       string := "CWL operation with non integer TX or TY types";
  constant fep_invalid_sma_value_message:  string := "Invalid SMA value";
  constant fep_invalid_op_value_message:   string := "Invalid OP value";
  constant fep_invalid_m_value_message:    string := "Address generator used with M=1 (MX, MY or MZ)";
-- pragma translate_on

end package errors;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
