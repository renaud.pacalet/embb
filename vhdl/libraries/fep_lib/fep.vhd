--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief FEP top level

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;

use work.fep_pkg.all;

entity fep is
  generic(
-- pragma translate_off
    debug:    boolean := false;  --* Print debug information
    verbose:  boolean := false;  --* Print more debug information
-- pragma translate_on
    n0:       positive := 1;  --* Number of input pipeline registers in MSS, including input registers of RAMs
    n1:       positive := 1); --* Number of output pipeline registers in MSS, including output registers of RAMs
  port(
    clk:      in  std_ulogic;  -- master clock
    srstn:    in  std_ulogic;  -- master reset
    ce:       in  std_ulogic;  -- chip enable
    hirq:     in  std_ulogic;  -- host input irq,
    uirq:     out std_ulogic;  -- UC output irq
    dirq:     out std_ulogic;  -- DMA output irq
    pirq:     out std_ulogic;  -- PSS output irq
    tvci_in:  in  vci_i2t_type;  -- Target AVCI input
    tvci_out: out vci_t2i_type; -- Target AVCI output
    ivci_in:  in  vci_t2i_type;  -- Initiator AVCI input
    ivci_out: out vci_i2t_type;  -- Initiator AVCI output
    gpo:      out std_ulogic_vector(63 downto 0)); -- General Purpose Output
end entity fep;

architecture rtl of fep is

  signal rams2mss: rams2mss_type;
  signal mss2rams: mss2rams_type;

begin

  i_fepnr: entity work.fep_no_rams(rtl)
  generic map(
-- pragma translate_off
    debug     => debug,
    verbose   => verbose,
-- pragma translate_on
    n0        => 1,
    n1        => 1)
  port map(
    clk      => clk,
    srstn    => srstn,
    ce       => ce,
    hirq     => hirq,
    uirq     => uirq,
    pirq     => pirq,
    dirq     => dirq,
    tvci_in  => tvci_in,
    tvci_out => tvci_out,
    ivci_in  => ivci_in,
    ivci_out => ivci_out,
    gpo      => gpo,
    rams2mss => rams2mss,
    mss2rams => mss2rams);

  i_rams: entity work.rams(rtl)
  port map(
    clk      => clk,
    rams2mss => rams2mss,
    mss2rams => mss2rams);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
