--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief FEP processing unit
--*
--* This module is synchronous on the rising edge of its clock CLK. Its
--* reset SRSTN is synchronous and active low. It implements most of the data
--* processing of the FEP. It samples its inputs DI on rising edges of the clock
--* where the reset is de-asserted and the DSI data strobe signal is asserted. If
--* such a rising edge of the clock is numbered #n, the results are output on the
--* DO output port at clock edge #n+2. Outputs can thus be sampled by the
--* environment at clock edge #n+3.
--* 
--* The DI input port is a record. The DI.OP field is an extended operation code
--* indicating which operation is to be performed on the other fields.
--*
--* A) Functional description:
--*
--* The different functions implemented by PU are the following (this description
--* is purely functional and does not necessarily reflect the actual
--* computations). All computations are u_signed, fully accurate, without rounding
--* nor truncation. Right shifts (>>) of a u_signed value are sign-extended shifts.
--* Conversions of a n-bits u_signed value in to a m>n -bits u_signed value preserve
--* the value.
--* 
--* 1) If DI.OP is in {CWA, CWP, CWM, CWS, MOV}:
--* 
--* 1.a) The DI.X 32-bits input is first modified according DI.TX, DI.AX,
--*      DI.VRX, DI.VIX and DI.DX to produce a CPX34 value XM; TMP1 and TMP2
--*      are two intermediate CPX32:
--* 
--*      DI.AX DI.TX                     TMP1.R                     TMP1.I
--*         00    00 DI.X(31 downto 24) & x"00" DI.X(31 downto 24) & x"00"
--*         00    01         DI.X(31 downto 16)         DI.X(31 downto 16)
--*         00    10 DI.X(31 downto 24) & x"00" DI.X(23 downto 16) & x"00"
--*         00    11         DI.X(31 downto 16)          DI.X(15 downto 0)
--*         01    00 DI.X(23 downto 16) & x"00" DI.X(23 downto 16) & x"00"
--*         01    01 forbidden
--*         01    10 forbidden
--*         01    11 forbidden
--*         10    00  DI.X(15 downto 8) & x"00"  DI.X(15 downto 8) & x"00"
--*         10    01          DI.X(15 downto 0)          DI.X(15 downto 0)
--*         10    10  DI.X(15 downto 8) & x"00"   DI.X(7 downto 0) & x"00"
--*         10    11 forbidden
--*         11    00   DI.X(7 downto 0) & x"00"   DI.X(7 downto 0) & x"00"
--*         11    01 forbidden
--*         11    10 forbidden
--*         11    11 forbidden
--*   
--*      DI.DX    TMP2.R    TMP2.I
--*          0    TMP1.R    TMP1.I
--*          1 TMP1.R>>8 TMP1.I>>8
--*   
--*      DI.VRX     XM.R
--*          00        0
--*          01  -TMP2.R
--*          10 |TMP2.R|
--*          11   TMP2.R
--*   
--*      DI.VIX     XM.I
--*          00        0
--*          01  -TMP2.I
--*          10 |TMP2.I|
--*          11   TMP2.I
--* 
--* 1.b) If DI.OP is in {CWA, CWP} the DI.Y 32-bits input is also modified by
--*      the same algorithm, according DI.TY, DI.AY, DI.VRY, DI.VIY and
--*      DI.DY to produce a CPX34 value YM.
--* 
--* 1.c) If DI.OP is CWS the DI.X 32-bits input is modified again by
--*      the same algorithm, according DI.TY, DI.AY, DI.VRY, DI.VIY and
--*      DI.DY to produce a CPX34 value YM.
--* 
--* 1.d) If DI.OP=CWA, the output DO.V is set to CPX66(XM+YM)
--* 
--* 1.e) If DI.OP is in {CWP, CWS}, the output DO.V is set to CPX66(XM*YM)
--* 
--* 1.f) If DI.OP=CWM, the output DO.V is set to
--*      CPX66((XM.R*XM.R+XM.I*XM.I)*(1+J))
--* 
--* 1.g) If DI.OP=MOV, the output DO.V is set to CPX66(XM)
--* 
--* 2) If DI.OP is in {CWL, CWLI, CWLE}, the Y 32 bits input carries Y0 and Y1, the two
--*    consecutive entries of the lookup table that will be used to compute the
--*    result. Y0 and Y1 must be adjacent in the Y word with Y0 on the left and
--*    Y1 on the right. If DI.TY="00" (int8), the MSB of Y0 can be bits 31 or 23
--*    of Y. If DI.TY="01" (INT16), the MSB of Y0 can only be bit 31 of Y.
--* 
--* 2.a) The DI.Y 32-bits input is first modified according DI.TY and DI.AY
--*      to produce two INT16 values Y0 and Y1:
--* 
--*      DI.AY DI.TY                         Y0                         Y1
--*          0    00 DI.Y(31 downto 24) & x"00" DI.Y(23 downto 16) & x"00"
--*          1    00 DI.Y(23 downto 16) & x"00"  DI.Y(15 downto 8) & x"00"
--*          0    01         DI.Y(31 downto 16)          DI.Y(15 downto 0)
--*          1 forbidden
--* 
--* 2.b) If DI.OP=CWL, the output DO.V is set to CPX66(Y0*2^15*(1+J))
--* 
--* 2.c) If DI.OP=CWLI (CWL with interpolation), the output DO.V is set to
--*      CPX66(Y0*2^15+DI.C1*(Y1-Y0))
--* 
--* 2.d) If DI.OP=CWLE (CWL with extrapolation), the output DO.V is set to
--*      CPX66(Y0*2^15+DI.C1*(Y1-Y0))
--* 
--* 3) If DI.OP is in {FT, FTL2, FTL4}:
--* 
--* 3.a) The DI.U input is a vector of 4 CPX50 values. It is first transformed
--*      into another vector of 4 CPX50 values, DIR.U, by swapping the components #1
--*      and #2 when DI.OP=FTL2 (last stage of a 2.4^n points FT) or by swapping
--*      the components #1 and #3 when (DI.OP=FT or DI.OP=FTL4) and DI.I='1'
--*      (any stage of an inverse FT but the last for a 2.4^n points IFT):
--* 
--*      DI.OP DI.I DIR.U(0) DIR.U(1) DIR.U(2) DIR.U(3)
--*         FT    0  DI.U(0)  DI.U(1)  DI.U(2)  DI.U(3)
--*       FTL2    0  DI.U(0)  DI.U(2)  DI.U(1)  DI.U(3)
--*       FTL4    0  DI.U(0)  DI.U(1)  DI.U(2)  DI.U(3)
--*         FT    1  DI.U(0)  DI.U(3)  DI.U(2)  DI.U(1)
--*       FTL2    1  DI.U(0)  DI.U(2)  DI.U(1)  DI.U(3)
--*       FTL4    1  DI.U(0)  DI.U(3)  DI.U(2)  DI.U(1)
--* 
--* 3.b) From DIR.U, two other vectors of 4 CPX50 values are then computed,
--*      corresponding to the 2 different FT butterflies; B2 is the butterfly of
--*      the last stage of a 2.4^n points FT; B4 is the butterfly of any stage of
--*      4^n points FTs and any stage but the last of a 2.4^n points FT:
--* 
--*                  B2(0)             B2(1)             B2(2)             B2(3)
--*      DIR.U(0)+DIR.U(2) DIR.U(0)-DIR.U(2) DIR.U(1)+DIR.U(3) DIR.U(1)-DIR.U(3)
--* 
--*            B4(0)         B4(1)       B4(2)         B4(3)
--*      B2(0)+B2(2) B2(1)-J.B2(3) B2(0)-B2(2) B2(1)+J.B2(3)
--* 
--*      Note: when DI.OP=FTL2, thanks to the swap of components #1 and #2, B2
--*      is indeed:
--* 
--*          B2(0)     B2(1)     B2(2)     B2(3)
--*      U(0)+U(1) U(0)-U(1) U(2)+U(3) U(2)-U(3)
--*
--*      and when (DI.OP=FT or DI.OP=FTL4) and DI.I='1', thanks to the swap of
--*      components #1 and #3, B4 is indeed:
--* 
--*            B4(0)         B4(1)       B4(2)         B4(3)
--*      B2(0)+B2(2) B2(1)+J.B2(3) B2(0)-B2(2) B2(1)-J.B2(3)
--* 
--* 3.c) If DI.OP=FT and DI.I='0' (regular FT stage), the three CPX32 twiddle
--*      factors DI.T(1), DI.T(2) and DI.T(3) are modified according their
--*      respective modifiers DI.TMOD(1), DI.TMOD(2) and DI.TMOD(3) to produce
--*      three CPX34 other twiddle factors T1, T2 and T3; TMP is a CPX32
--*      intermediate value:
--* 
--*      DI.TMOD(K).SWAP     TMP.R     TMP.I
--*                FALSE DI.T(K).R DI.T(K).I
--*                 TRUE DI.T(K).I DI.T(K).R
--* 
--*      DI.TMOD(K).NEGR   TK.R
--*                FALSE  TMP.R
--*                 TRUE -TMP.R 
--* 
--*      DI.TMOD(K).NEGI   TK.I
--*                FALSE  TMP.I
--*                 TRUE -TMP.I 
--* 
--*      If DI.OP=FT and DI.I='1' (regular inverse FT stage), the three CPX32
--*      twiddle factors DI.T(1), DI.T(2) and DI.T(3) are modified according their
--*      respective modifiers DI.TMOD(1), DI.TMOD(2) and DI.TMOD(3) to produce
--*      three CPX34 other twiddle factors T1, T2 and T3; TMP is a CPX32
--*      intermediate value:
--* 
--*      DI.TMOD(K).SWAP     TMP.R     TMP.I
--*                FALSE DI.T(K).R DI.T(K).I
--*                 TRUE DI.T(K).I DI.T(K).R
--* 
--*      DI.TMOD(K).NEGR   TK.R
--*                FALSE  TMP.R
--*                 TRUE -TMP.R 
--* 
--*      DI.TMOD(K).NEGI   TK.I
--*                FALSE -TMP.I
--*                 TRUE  TMP.I 
--*
--*      The DO.u output (vector of 4 CPX50 values) is then computed as:
--* 
--*      DO.U(0)=CPX50(B4(0)>>1)
--*      DO.U(1)=CPX50((B4(1)*T1)>>16)
--*      DO.U(2)=CPX50((B4(2)*T2)>>16)
--*      DO.U(3)=CPX50((B4(3)*T3)>>16)
--* 
--* 3.d) If DI.OP=FTL2 (last stage of a 2.4^n points FT), a INVSQRT2 constant is
--*      set to UINT16(2^15/SQRT(2)) and the DO.U output is computed as:
--* 
--*      DO.U(0)=CPX50((B2(0)*INVSQRT2)>>15)
--*      DO.U(1)=CPX50((B2(1)*INVSQRT2)>>15)
--*      DO.U(2)=CPX50((B2(2)*INVSQRT2)>>15)
--*      DO.U(3)=CPX50((B2(3)*INVSQRT2)>>15)
--* 
--* 3.e) If DI.OP=FTL4 (last stage of a 4^n points FT), the DO.U output is
--*      computed as:
--* 
--*      DO.U(0)=CPX50(B4(0)>>1)
--*      DO.U(1)=CPX50(B4(1)>>1)
--*      DO.U(2)=CPX50(B4(2)>>1)
--*      DO.U(3)=CPX50(B4(3)>>1)
--* 
--* B) Implementation:
--*
--* When DSI is asserted on a rising edge of the clock, the DI input is stored
--* in the first pipeline register DIR. During the following clock period, all
--* the type conversions, value modifications, butterflies computations take
--* place. The butterflies are computed by the combinatorial BF module. The
--* results of this first stage are fed into 6 identical DSP modules, each
--* containing two DSP48E units. These DSP modules are responsible for all the
--* arithmetic operations (but the butterflies and the Y1-Y0 computation for CWLI
--* and CWLE operations). The DSP modules are capable of negating some of their
--* inputs before computing. This feature is used to implement the twiddle factor
--* modifications and the V{RI}{XY} value modifications without extra arithmetic
--* units. The DSP modules contain 2 more pipeline registers.
--*
--* This module has been validated by extensive simulations. It synthesizes on a
--* LX330 Virtex 5 5VLX330FF1760-2 target with a 170 MHz clock frequency and
--* consumes 733 slices and 12 DSP48E blocks.
--*
--* Modify at your own risks. Do not use a modified version before running
--* the regression tests:
--* $ make pu_sim.sim

-- pragma translate_off
use std.textio.all;
-- pragma translate_on

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use ieee.numeric_std.all;
use global_lib.global.all;
use global_lib.utils.all;

--pragma translate_off
use global_lib.sim_utils.all;
--pragma translate_on

use work.type_pkg.all;
use work.fep_pkg.all;
use work.pu_pkg.all;

entity pu is
--pragma translate_off
  generic(
          debug: boolean := false;
          idx: natural range 0 to 1 := 0);
--pragma translate_on
  port(clk:   in  std_ulogic; --* master clock; PU is synchronous on the rising edge of clk
       srstn: in  std_ulogic; --* synchronous, active low, reset
       ce:    in  std_ulogic; --* synchronous, active high, chip enable
       dsi:   in  std_ulogic; --* active high data strobe input
       di:    in  pu_in;      --* data input
       do:    out pu_out);    --* data output
end entity pu;

architecture rtl of pu is

  -- Registered input data strobe
  signal dsir: std_ulogic;
  -- Extended operation code pipeline is used to track extended operation codes
  -- along the PU pipeline.
  signal op_pipe: extended_op_code_vector(1 to pu_pipe_depth);
  -- For CWL operations the output result is the y0 value that is computed in
  -- the first pipeline stage. This value must thus travel along all stages.
  signal y0_pipe: s16_vector(1 to pu_pipe_depth);
  -- For FT and FTL4 operations the output result do.u(0) is the (right shifted
  -- by one position) first output of the butterfly (bf4_out(0)). This value
  -- must thus travel along all stages.
  signal bf4_out0_pipe: cpx48_vector(1 to pu_pipe_depth);

  -- dir is the input register (first pipeline register).
  signal dir: pu_in;
  -- idft is true if dir.i = '1' (inverse FT)
  signal idft: boolean;
  -- tm is the modified di.t according to di.tmod
  signal tm: cpx32_vector(1 to 3);
  -- xm and ym are the two modified x and y values
  signal xm, ym: cpx32;
  -- negate flags for xm and ym real and imaginary parts
  signal negxr, negxi, negyr, negyi: boolean;
  -- y0 and y1 values used for CWL, CWLI and CWLE operations
  signal y0, y1, y01: s16;
  -- y1 minus y0, extended to 25 bits
  signal y1my0: s25;

  -- outputs of the BF module (the butterflies)
  signal bf2_out, bf4_out: cpx50_vector(0 to 3);

  -- data strobe inputs of the 6 DSP modules
  signal dsp_dsi: std_ulogic_vector(0 to 5);
  -- data inputs of the 6 DSP modules
  signal dsp_inputs: dsp_in_vector(0 to 5);
  -- data outputs of the 6 DSP modules
  signal dsp_outputs: dsp_out_vector(0 to 5);

  -- local versions of outputs
  signal dol: pu_out;
begin

  -- input register and pipeline registers
  pinput: process(clk)
  begin
    if rising_edge(clk) then
      if srstn = '0' then
        dsir <= '0';
      elsif ce = '1' then
        dir <= pu_di_none;
        dsir <= dsi;
        op_pipe(2 to pu_pipe_depth) <= op_pipe(1 to pu_pipe_depth - 1);
        y0_pipe(2 to pu_pipe_depth) <= y0_pipe(1 to pu_pipe_depth - 1);
        bf4_out0_pipe(2 to pu_pipe_depth) <= bf4_out0_pipe(1 to pu_pipe_depth - 1);
        if dsi = '1' then
          dir <= di;
          if di.op = ftl2 then
            dir.u <= di.u(0) & di.u(2) & di.u(1) & di.u(3);
          elsif (di.op = ft or di.op = ftl4) and di.i = '1' then
            dir.u <= di.u(0) & di.u(3) & di.u(2) & di.u(1);
          end if;
        end if;
      end if;
    end if;
  end process pinput;
  idft <= dir.i = '1';

  op_pipe(1) <= dir.op;

  -- modifications of input values during first pipeline stage
  pdimod: process(dir)
    variable tmp, tmp1: cpx32;      -- temporary variables
    variable nr, ni: boolean; -- temporary negate flags (real and imaginary parts)
    variable y0v, y1v: s16; -- temporary variables for y0 and y1 computations
  begin
    -- pragma translate_off
    -- Check operations versus input types and alignments. Not synthesizable.
    if dir.op /= ft and dir.op /= ftl2 and dir.op /= ftl4 then -- if not FT
      -- X components must be aligned in memory on addresses that are a multiple
      -- their byte-width
      assert (dir.tx = "11" and dir.ax = "00") or dir.tx = "00" or dir.ax(0) = '0'
        report "invalid X type - X alignment combination"
        severity warning;
      -- Y components must be aligned in memory on addresses that are a multiple
      -- their byte-width
      if dir.op = cwa or dir.op = cwp then
        assert (dir.ty = "11" and dir.ay = "00") or dir.ty = "00" or dir.ay(0) = '0'
          report "invalid Y type - Y alignment combination"
          severity warning;
      -- alignment rules change a bit for CWL, CWLI and CWLE operations
      elsif dir.op = cwl or dir.op = cwli or dir.op = cwle then
        if dir.ty = "01" and dir.ay(0) = '1' then
          assert false
            report "cwlcnv: invalid type-alignment combination"
            severity warning;
        end if;
      end if;
    end if;
    -- pragma translate_on
    tm <= dir.t; -- by default, no modification of t
    for i in 1 to 3 loop -- swap? real and imaginary parts of twiddle
      if dir.tmod(i).swap then
        tm(i) <= (r => dir.t(i).i, i => dir.t(i).r);
      end if;
    end loop;
    -- type conversion of X input
    tmp := xycnv(dir.x, dir.tx, dir.ax);
    tmp1 := tmp;
    -- value modification of X input
    xymod(tmp1, dir.vrx, dir.vix, dir.dx, tmp, nr, ni);
    xm <= tmp;
    negxr <= nr;
    negxi <= ni;
    -- type conversion of Y input
    if dir.op = cws then
      tmp := tmp1;
    else
      tmp := xycnv(dir.y, dir.ty, dir.ay);
    end if;
    -- value modification of Y input
    tmp1 := tmp;
    xymod(tmp1, dir.vry, dir.viy, dir.dy, tmp, nr, ni);
    ym <= tmp;
    negyr <= nr;
    negyi <= ni;
    -- computation of Y0 and Y1 values for CWL, CWLI and CWLE operations
    cwlcnv(dir.y, dir.ty(0), dir.ay(0), y0v, y1v);
    y0 <= y0v;
    y1 <= y1v;
  end process pdimod;

  -- the input of the y0_pipe
  y0_pipe(1) <= y0;
  -- the Y1-Y0 value used for CWLI and CWLE operations
  y1my0 <= resize(y1, 25) - resize(y0, 25);
  -- which of Y0 or Y1 is used as offset for CWLI and CWLE operations depend on the actual
  -- operation: Y0*2^15+C1.(Y1-Y0) in CWLI and Y1*2^15+C1.(Y1-Y0) in CWLE.
  y01 <= y0 when dir.op = cwli else
         y1;

  -- FT butterflies
  ibf: entity work.bf(rtl)
    port map(u  => dir.u,
             z2 => bf2_out,
             z4 => bf4_out);

  -- the input of the bf4_out0_pipe
  bf4_out0_pipe(1).r <= bf4_out(0).r(24 downto 1);
  bf4_out0_pipe(1).i <= bf4_out(0).i(24 downto 1);

  -- prepare inputs of the 6 DSP modules
  pdsp_inputs: process(idft, dsir, dir, tm, bf4_out, bf2_out, xm, ym, negxr, negxi,
    negyr, negyi, y01, y1my0)
  begin
    -- by default, all DSP modules are used on valid inputs
    dsp_dsi <= (others => dsir);
    for i in 0 to 2 loop -- by default, configure the DSP inputs for regular FT
      -- even DSP modules compute the real part (z0 output) of bf4_out(i+1)*tm(i+1)
      dsp_inputs(2 * i).mode <= ft_dsp; -- z0=(-1)^n0*a0*b0+(-1)^n1*a1*b1
      dsp_inputs(2 * i).n0 <= dir.tmod(i + 1).negr;     -- negate? real part of twiddle
      dsp_inputs(2 * i).n1 <= not (dir.tmod(i + 1).negi xor idft); -- negate? imaginary part of twiddle
      dsp_inputs(2 * i).a0 <= bf4_out(i + 1).r;
      dsp_inputs(2 * i).a1 <= bf4_out(i + 1).i;
      dsp_inputs(2 * i).b0 <= tm(i + 1).r;
      dsp_inputs(2 * i).b1 <= tm(i + 1).i;
      dsp_inputs(2 * i).c0 <= y01;  -- used for CWLI and CWLE
      dsp_inputs(2 * i).c1 <= ym.i; -- used for CWA
      -- odd DSP modules compute the imaginary part (z0 output) of bf4_out(i+1)*tm(i+1)
      dsp_inputs(2 * i + 1).mode <= ft_dsp; -- z0=(-1)^n0*a0*b0+(-1)^n1*a1*b1
      dsp_inputs(2 * i + 1).n0 <= dir.tmod(i + 1).negr; -- negate? real part of twiddle
      dsp_inputs(2 * i + 1).n1 <= dir.tmod(i + 1).negi xor idft; -- negate? imaginary part of twiddle
      dsp_inputs(2 * i + 1).a0   <= bf4_out(i + 1).i;
      dsp_inputs(2 * i + 1).a1   <= bf4_out(i + 1).r;
      dsp_inputs(2 * i + 1).b0   <= tm(i + 1).r;
      dsp_inputs(2 * i + 1).b1   <= tm(i + 1).i;
      dsp_inputs(2 * i + 1).c0   <= y01;  -- used for CWLI and CWLE
      dsp_inputs(2 * i + 1).c1   <= ym.i; -- used for CWA
    end loop;
    case dir.op is
      when ft => null; -- already done by the default above
      -- for FTL2 (last stage of a 2.4^n point FT), only the 4 first DSP modules
      -- are used. Each of them computes the real (z0 output) and imaginary
      -- parts (z1 output) of bf2_out(i)*invsqrt2
                   -- DSP modules 5 and 6 are disabled
      when ftl2 => dsp_dsi(4 to 5) <= (others => '0');
                   for i in 0 to 3 loop -- for 4 first DSP modules
                     dsp_inputs(i).mode <= ftl2_dsp; -- z0=a0*b0, z1=a1*b1
                     dsp_inputs(i).a0   <= bf2_out(i).r;
                     dsp_inputs(i).a1   <= bf2_out(i).i;
                     dsp_inputs(i).b0   <= invsqrt2;
                     dsp_inputs(i).b1   <= invsqrt2;
                   end loop;
      -- for FTL4 (last stage of a 4^n point FT), all DSP modules are used but
      -- only to delay their inputs by 2 clock cycles (no processing)
      when ftl4 => for i in 0 to 5 loop -- for all DSP modules
                     dsp_inputs(i).mode <= ftl4_dsp; -- z0(42 downto 18)=a0
                   end loop;
      -- for CWL (no inter- extra-polation), the DSP modules are all disabled
      when cwl => dsp_dsi <= (others => '0');
      -- for CWP or CWS, only the 2 last DSP modules are used
                        -- disable the 4 first DSP modules
      when cwp | cws => dsp_dsi(0 to 3) <= (others => '0');
                        -- DSP module #4 computes the real part (z0 output) of xm*ym
                        dsp_inputs(4).mode <= ft_dsp; -- z0=(-1)^n0*a0*b0+(-1)^n1*a1*b1
                        dsp_inputs(4).n0   <= negxr xor negyr;
                        dsp_inputs(4).n1   <= negxi xnor negyi;
                        dsp_inputs(4).a0   <= resize(xm.r, 25);
                        dsp_inputs(4).a1   <= resize(xm.i, 25);
                        dsp_inputs(4).b0   <= ym.r;
                        dsp_inputs(4).b1   <= ym.i;
                        -- DSP module #5 computes the imaginary part (z0 output) of xm*ym
                        dsp_inputs(5).mode <= ft_dsp; -- z0=(-1)^n0*a0*b0+(-1)^n1*a1*b1
                        dsp_inputs(5).n0   <= negxr xor negyi;
                        dsp_inputs(5).n1   <= negxi xor negyr;
                        dsp_inputs(5).a0   <= resize(xm.r, 25);
                        dsp_inputs(5).a1   <= resize(xm.i, 25);
                        dsp_inputs(5).b0   <= ym.i;
                        dsp_inputs(5).b1   <= ym.r;
      -- for CWA only the 2 last DSP modules are used
                  -- disable the 4 first DSP modules
      when cwa => dsp_dsi(0 to 3) <= (others => '0');
                  -- DSP module #4 computes the real part (z0 output) of xm+ym
                  dsp_inputs(4).mode <= cwa_dsp; -- z0(17 downto 0)=(-1)^n0*((-1)^n1*b1+c1)
                  dsp_inputs(4).n0   <= negyr;
                  dsp_inputs(4).n1   <= negxr xor negyr;
                  dsp_inputs(4).b1   <= xm.r;
                  dsp_inputs(4).c1   <= ym.r;
                  -- DSP module #5 computes the imaginary part (z0 output) of xm+ym
                  dsp_inputs(5).mode <= cwa_dsp; -- z0(17 downto 0)=(-1)^n0*((-1)^n1*b1+c1)
                  dsp_inputs(5).n0   <= negyi;
                  dsp_inputs(5).n1   <= negxi xor negyi;
                  dsp_inputs(5).b1   <= xm.i;
                  dsp_inputs(5).c1   <= ym.i;
      -- for CWM only the last DSP module is used
                  -- disable the 5 first DSP modules
      when cwm => dsp_dsi(0 to 4) <= (others => '0');
                  dsp_inputs(5).mode <= ft_dsp; -- z0=(-1)^n0*a0*b0+(-1)^n1*a1*b1
                  dsp_inputs(5).n0   <= false;
                  dsp_inputs(5).n1   <= false;
                  dsp_inputs(5).a0   <= resize(xm.r, 25);
                  dsp_inputs(5).a1   <= resize(xm.i, 25);
                  dsp_inputs(5).b0   <= xm.r;
                  dsp_inputs(5).b1   <= xm.i;
      -- for CWLI and CWLE only the last DSP module is used
                          -- disable the 5 first DSP modules
      when cwli | cwle => dsp_dsi(0 to 4) <= (others => '0');
                          dsp_inputs(5).mode <= cwl_dsp; -- z0=a0*b0+2^15*c0
                          dsp_inputs(5).a0   <= y1my0;
                          dsp_inputs(5).b0   <= u_signed(resize(dir.c1, 16));
                          dsp_inputs(5).c0   <= y01;
      -- for MOV only the 2 last DSP modules are used to optionally negate the
      -- real part (z0 output of DSP module #4) and/or the imaginary part (z0
      -- output of DSP module #5) of xm
                  -- disable the 4 first DSP modules
      when mov => dsp_dsi(0 to 3) <= (others => '0');
                  dsp_inputs(4).mode <= mov_dsp; -- z0(17 downto 0)=(-1)^n0*b0
                  dsp_inputs(4).n0   <= negxr;
                  dsp_inputs(4).b0   <= xm.r;
                  dsp_inputs(5).mode <= mov_dsp; -- z0(17 downto 0)=(-1)^n0*b0
                  dsp_inputs(5).n0   <= negxi;
                  dsp_inputs(5).b0   <= xm.i;
      when others => null;
    end case;
  end process pdsp_inputs;

  -- instantiate the 6 DSP modules
  gdsp: for i in 0 to 5 generate
    dsp: entity work.dsp
      port map(clk   => clk,
               srstn => srstn,
               ce    => ce,
               dsi   => dsp_dsi(i),
               di    => dsp_inputs(i),
               do    => dsp_outputs(i));
  end generate gdsp;

  -- output multiplexers to produce the do.u and do.v outputs
  poutput: process(op_pipe, y0_pipe, dsp_outputs, bf4_out0_pipe)
  begin
    -- by default the do.u output is set to that of a regular FT stage
    dol.u(0).r <= resize(bf4_out0_pipe(pu_pipe_depth).r, 25);
    dol.u(0).i <= resize(bf4_out0_pipe(pu_pipe_depth).i, 25);
    for i in 0 to 2 loop
      dol.u(i + 1).r <= dsp_outputs(2 * i).z0(40 downto 16);
      dol.u(i + 1).i <= dsp_outputs(2 * i + 1).z0(40 downto 16);
    end loop;
    -- by default, the do.v output is set to that of a CWP or CWS operation
    dol.v.r <= dsp_outputs(4).z0(32 downto 0);
    dol.v.i <= dsp_outputs(5).z0(32 downto 0);
    case op_pipe(pu_pipe_depth) is
      when cwa => dol.v.r <= resize(dsp_outputs(4).z0(17 downto 0), 33);
                  dol.v.i <= resize(dsp_outputs(5).z0(17 downto 0), 33);
      when mov => dol.v.r <= resize(dsp_outputs(4).z0(17 downto 0), 33);
                  dol.v.i <= resize(dsp_outputs(5).z0(17 downto 0), 33);
      when cwm => dol.v.r <= dsp_outputs(5).z0(32 downto 0);
                  dol.v.i <= dsp_outputs(5).z0(32 downto 0);
      when cwl => dol.v.r <= shift_left(resize(y0_pipe(pu_pipe_depth), 33), 15);
                  dol.v.i <= shift_left(resize(y0_pipe(pu_pipe_depth), 33), 15);
      when cwli | cwle => dol.v.r <= dsp_outputs(5).z0(32 downto 0);
                          dol.v.i <= dsp_outputs(5).z0(32 downto 0);
      when ftl2 => for i in 0 to 3 loop
                     dol.u(i).r <= dsp_outputs(i).z0(39 downto 15);
                     dol.u(i).i <= dsp_outputs(i).z1(39 downto 15);
                   end loop;
      when ftl4 => for i in 1 to 3 loop
                     dol.u(i).r <= resize(dsp_outputs(2 * i - 2).z0(42 downto 19), 25);
                     dol.u(i).i <= resize(dsp_outputs(2 * i - 1).z0(42 downto 19), 25);
                   end loop;
      when others => null;
    end case;
  end process poutput;

  do <= dol;

  -- pragma translate_off
  debug_g: if debug generate
    debug_p: process(clk)
      variable l: line;
      variable n1, n2: natural;
      variable tmp, tmp1: cpx32;
      variable nr, ni: boolean;
      variable y0v, y1v: s16;
      variable dsi_pipe: std_ulogic_vector(1 to pu_pipe_depth);
    begin
      if rising_edge(clk) then
        if srstn = '0' then
          n1 := 0;
          n2 := 0;
          dsi_pipe := (others => '0');
        elsif ce = '1' then
          if dsi = '1' then
            if di.op = cwa or di.op = cwp or di.op = cwm or di.op = cws or di.op = mov or di.op = cwl then
              write(l, string'("X0["));
              write(l, 2 * n1 + idx);
              write(l, string'("]="));
              hwrite(l, di.x);
              write(l, string'(" (AX="));
              write(l, di.ax);
              write(l, string'("), "));
              tmp := xycnv(di.x, di.tx, di.ax);
              tmp1 := tmp;
              xymod(tmp, di.vrx, di.vix, di.dx, tmp, nr, ni);
              write(l, string'("X2["));
              write(l, 2 * n1 + idx);
              write(l, string'("]=("));
              if nr then
                write(l, string'("-"));
              end if;
              hwrite(l, tmp.r);
              write(l, string'(","));
              if ni then
                write(l, string'("-"));
              end if;
              hwrite(l, tmp.i);
              write(l, string'(")"));
              writeline(output, l);
              if di.op = cwa or di.op = cwp or di.op = cwl then
                write(l, string'("Y0["));
                write(l, 2 * n1 + idx);
                write(l, string'("]="));
                hwrite(l, di.y);
                write(l, string'(" (AY="));
                write(l, di.ay);
                write(l, string'("), "));
                if di.op = cwl then
                  cwlcnv(di.y, di.ty(0), di.ay(0), y0v, y1v);
                  write(l, string'("Y0="));
                  hwrite(l, y0v);
                  write(l, string'(", "));
                  write(l, string'("Y1="));
                  hwrite(l, y1v);
                  writeline(output, l);
                else
                  if di.op = cws then
                    tmp := tmp1;
                  else
                    tmp := xycnv(di.y, di.ty, di.ay);
                  end if;
                  xymod(tmp, di.vry, di.viy, di.dy, tmp, nr, ni);
                  write(l, string'("Y2["));
                  write(l, 2 * n1 + idx);
                  write(l, string'("]=("));
                  if nr then
                    write(l, string'("-"));
                  end if;
                  hwrite(l, tmp.r);
                  write(l, string'(","));
                  if ni then
                    write(l, string'("-"));
                  end if;
                  hwrite(l, tmp.i);
                  write(l, string'(")"));
                  writeline(output, l);
                end if;
              end if;
            end if;
            n1 := n1 + 1;
          else
            n1 := 0;
          end if;
          if dsi_pipe(pu_pipe_depth) = '1' then
            if di.op = cwa or di.op = cwp or di.op = cwm or di.op = cws or di.op = mov or di.op = cwl then
              write(l, string'("Z["));
              write(l, 2 * n2 + idx);
              write(l, string'("]=("));
              hwrite(l, dol.v.r);
              write(l, string'(","));
              hwrite(l, dol.v.i);
              write(l, string'(")"));
              writeline(output, l);
              n2 := n2 + 1;
            else
              n2 := 0;
            end if;
          end if;
          dsi_pipe := dsi & dsi_pipe(1 to pu_pipe_depth - 1);
        end if;
      end if;
    end process debug_p;
  end generate debug_g;
  -- pragma translate_on

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
