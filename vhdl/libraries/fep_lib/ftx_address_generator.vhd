--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Addresses generator for Fourrier transform operations of FEP

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.fep_pkg.all;
use work.type_pkg.all;

entity ftx_address_generator is
  port(clk:    in  std_ulogic;  -- clock
       srstn:  in  std_ulogic;  -- reset
       ce:     in  std_ulogic;  -- chip enable
       load:   in  std_ulogic;  -- load parameters
       run:    in  std_ulogic;  -- run addresses generator
       l:      in  v10;         -- length of vector
       b:      in  v9;          -- base address in quarter
       eos:    out std_ulogic;  -- end of generation
       valid:  out std_ulogic;  -- valid addresses
       lstage: out std_ulogic;  -- last stage
       r:      out std_ulogic;  -- reverse output
       mem:    out memory_type; -- memory selector
       t:      out v4;          -- transposition command
       a0:     out v10;
       a1:     out v10);        -- conponent addresses
end entity ftx_address_generator;

architecture rtl of ftx_address_generator is

  constant fepmaxsize : natural := 4096;
  signal ls, l0       : u13;
  signal bs           : u14;
  signal cnt          : u11;
  signal es           : std_ulogic;
  signal mr           : memory_type;
  signal as           : u14_vector(1 downto 0);
  signal da           : natural range 0 to 9;

  function last_bit_reverse (l: u13; cnt: u11) return u12 is
    variable temp: u12 := (others => '0');
  begin
    if l(12) = '1' then -- 4096
      temp(3 downto 2)   := cnt(9 downto 8);
      temp(5 downto 4)   := cnt(7 downto 6);
      temp(7 downto 6)   := cnt(5 downto 4);
      temp(9 downto 8)   := cnt(3 downto 2);
      temp(11 downto 10) := cnt(1 downto 0);
    elsif l(11) = '1' then -- 2048
      temp(2)           := cnt(8);
      temp(4 downto 3)  := cnt(7 downto 6);
      temp(6 downto 5)  := cnt(5 downto 4);
      temp(8 downto 7)  := cnt(3 downto 2);
      temp(10 downto 9) := cnt(1 downto 0);
    elsif l(10) = '1' then -- 1024
      temp(3 downto 2)   := cnt(7 downto 6);
      temp(5 downto 4)   := cnt(5 downto 4);
      temp(7 downto 6)   := cnt(3 downto 2);
      temp(9 downto 8)   := cnt(1 downto 0);
      temp(11 downto 10) := cnt(9 downto 8);
    elsif l(9) = '1' then -- 512
      temp(2)           := cnt(6);
      temp(4 downto 3)  := cnt(5 downto 4);
      temp(6 downto 5)  := cnt(3 downto 2);
      temp(8 downto 7)  := cnt(1 downto 0);
      temp(10 downto 9) := cnt(8 downto 7);
    elsif l(8) = '1'  then -- 256
      temp(3 downto 2)   := cnt(5 downto 4);
      temp(5 downto 4)   := cnt(3 downto 2);
      temp(7 downto 6)   := cnt(1 downto 0);
      temp(9 downto 8)   := cnt(7 downto 6);
      temp(11 downto 10) := cnt(9 downto 8);
    elsif l(7) = '1' then -- 128
      temp(2)           := cnt(4);
      temp(4 downto 3)  := cnt(3 downto 2);
      temp(6 downto 5)  := cnt(1 downto 0);
      temp(8 downto 7)  := cnt(6 downto 5);
      temp(10 downto 9) := cnt(8 downto 7);
    elsif l(6) = '1' then -- 64
      temp(3 downto 2)   := cnt(3 downto 2);
      temp(5 downto 4)   := cnt(1 downto 0);
      temp(7 downto 6)   := cnt(5 downto 4);
      temp(9 downto 8)   := cnt(7 downto 6);
      temp(11 downto 10) := cnt(9 downto 8);
    elsif l(5) = '1' then -- 32
      temp(2)           := cnt(2);
      temp(4 downto 3)  := cnt(1 downto 0);
      temp(6 downto 5)  := cnt(4 downto 3);
      temp(8 downto 7)  := cnt(6 downto 5);
      temp(10 downto 9) := cnt(8 downto 7);
    else -- 16 or 8 or 4 or 2 or 1
      temp(3 downto 2)   := cnt(1 downto 0);
      temp(5 downto 4)   := cnt(3 downto 2);
      temp(7 downto 6)   := cnt(5 downto 4);
      temp(9 downto 8)   := cnt(7 downto 6);
      temp(11 downto 10) := cnt(9 downto 8);
    end if;
    return temp;
  end function last_bit_reverse;

  function bit_reverse(l: u13; cnt: u11) return u12 is
    variable temp: u12 := (others => '0');
  begin
    if l(12) = '1' then -- 4096
      temp(9 downto 2)   := cnt(9 downto 2);
      temp(11 downto 10) := cnt(1 downto 0);
    elsif l(11) = '1' then -- 2048
      temp(8 downto 2)  := cnt(8 downto 2);
      temp(10 downto 9) := cnt(1 downto 0);
    elsif l(10) = '1' then -- 1024
      temp(7 downto 2)   := cnt(7 downto 2);
      temp(9 downto 8)   := cnt(1 downto 0);
      temp(11 downto 10) := cnt(9 downto 8);
    elsif l(9) = '1' then -- 512
      temp(6 downto 2)  := cnt(6 downto 2);
      temp(8 downto 7)  := cnt(1 downto 0);
      temp(10 downto 9) := cnt(8 downto 7);
    elsif l(8) = '1'  then -- 256
      temp(5 downto 2)   := cnt(5 downto 2);
      temp(7 downto 6)   := cnt(1 downto 0);
      temp(9 downto 8)   := cnt(7 downto 6);
      temp(11 downto 10) := cnt(9 downto 8);
    elsif l(7) = '1' then -- 128
      temp(4 downto 2)  := cnt(4 downto 2);
      temp(6 downto 5)  := cnt(1 downto 0);
      temp(8 downto 7)  := cnt(6 downto 5);
      temp(10 downto 9) := cnt(8 downto 7);
    elsif l(6) = '1' then -- 64
      temp(3 downto 2)   := cnt(3 downto 2);
      temp(5 downto 4)   := cnt(1 downto 0);
      temp(7 downto 6)   := cnt(5 downto 4);
      temp(9 downto 8)   := cnt(7 downto 6);
      temp(11 downto 10) := cnt(9 downto 8);
    elsif l(5) = '1' then -- 32
      temp(2)           := cnt(2);
      temp(4 downto 3)  := cnt(1 downto 0);
      temp(6 downto 5)  := cnt(4 downto 3);
      temp(8 downto 7)  := cnt(6 downto 5);
      temp(10 downto 9) := cnt(8 downto 7);
    else -- 16 or 8 or 4 or 2 or 1
      temp(3 downto 2)   := cnt(1 downto 0);
      temp(5 downto 4)   := cnt(3 downto 2);
      temp(7 downto 6)   := cnt(5 downto 4);
      temp(9 downto 8)   := cnt(7 downto 6);
      temp(11 downto 10) := cnt(9 downto 8);
    end if;
    return temp;
  end function bit_reverse;

  function channel_selection(l0: u13; l: u13; cnt: u11) return std_ulogic is
    variable s: std_ulogic := '0';
  begin
    if l0(6) = '1' then -- DFT 64
      if l(4) = '1' then
        s := cnt(1);
      elsif l(2) = '1' then
        s := cnt(1);
      end if;
    elsif l0(8) = '1' then -- DFT 256
      if l(6) = '1' then
        s := cnt(3);
      elsif l(4) = '1' then
        s := cnt(1);
      elsif l(2) = '1' then
        s := cnt(3);
      end if;
    elsif l0(10) = '1' then -- DFT 256
      if l(8) = '1' then
        s := cnt(5);
      elsif l(6) = '1' then
        s := cnt(3);
      elsif l(4) = '1' then
        s := cnt(1);
      elsif l(2) = '1' then
        s := cnt(5);
      end if;
    elsif l0(12) = '1' then -- DFT 4096
      if l(10) = '1' then
        s := cnt(7);
      elsif l(8) = '1' then
        s := cnt(5);
      elsif l(6) = '1' then
        s := cnt(3);
      elsif l(4) = '1' then
        s := cnt(1);
      elsif l(2) = '1' then
        s := cnt(7);
      end if;
    elsif l0(5) = '1' then -- DFT 32
      if l(3) = '1' then
        s := cnt(0);
      elsif l(1) = '1' then
        s := cnt(1);
      end if;
    elsif l0(7) = '1' then -- DFT 128
      if l(5) = '1' then
        s := cnt(2);
      elsif l(3) = '1' then
        s := cnt(0);
      elsif l(1) = '1' then
        s := cnt(3);
      end if;
    elsif l0(9) = '1' then -- DFT 512
      if l(7) = '1' then
        s := cnt(4);
      elsif l(5) = '1' then
        s := cnt(2);
      elsif l(3) = '1' then
        s := cnt(0);
      elsif l(1) = '1' then
        s := cnt(5);
      end if;
    elsif l0(11) = '1' then -- DFT 512
      if l(9) = '1' then
        s := cnt(6);
      elsif l(7) = '1' then
        s := cnt(4);
      elsif l(5) = '1' then
        s := cnt(2);
      elsif l(3) = '1' then
        s := cnt(0);
      elsif l(1) = '1' then
        s := cnt(7);
      end if;
    end if;
    return s;
  end function channel_selection;

begin

  process(clk)
    variable cntv: u11;
    variable c:    u11_vector(1 downto 0);
    variable u:    u12;
  begin
    if rising_edge(clk) then
      cntv := cnt + 1;
      c := (others => (others => '0'));
      u := (others => '0');
      if srstn = '0' then
        cnt    <= (others => '0');
        ls     <= (others => '0');
        l0     <= (others => '0');
        bs     <= (others => '0');
        t      <= (others => '0');
        es     <= '0';
        r      <= '0';
        lstage <= '0';
        valid  <= '0';
        mr     <= MIO;
        da     <= 0;
        as     <= (others => (others => '0'));
      elsif ce = '1' then
        es     <= '0';
        lstage <= '0';
        valid  <= run;
        if es = '1' then
          valid <= '0';
        end if;
        if load = '1' then
          cnt  <= (others => '0');
          ls   <= u10(l) & "000";
          l0   <= u10(l) & "000";
          bs   <= "00" & u9(b) & "000";
          da   <= fep_log2(l);
          mr   <= MIO;
        elsif run = '1' then
          if ls(1) = '1' or ls(2) = '1' then
            lstage <= '1';
          end if;
          if cntv(da) = '1' then
            if ls(2) = '1' or ls(1) = '1' then
              es <= '1';
            end if;
            mr <= TMP;
            if mr = TMP then
              bs    <= bs + l0(12 downto 0);
            else
              bs    <= (others => '0');
            end if;
            ls    <= shift_right(ls, 2);
            cntv  := (others => '0');
          end if;
          cnt <= cntv;
        end if;
        for i in 0 to 1 loop -- address
          c(i)  := i + shift_left(cnt, 1);
          if ls(2) = '1'  or ls(1) = '1' then
            u  := last_bit_reverse(l0, c(i));
          else
            u  := bit_reverse(ls, c(i));
          end if;
          as(i) <= resize(u + bs, 14);
        end loop;
        mem   <= mr;
        r <= channel_selection(l0, ls,  cnt);
        if ls(1) = '1' then -- 2
          t    <= "0100";
          if l0(3) = '1' then
            t    <= "0101";
          end if;
        elsif ls(2) = '1' then -- 4
          t    <= "0100";
        elsif ls(3) = '1' then -- 8
          t    <= "1110";
        else
          t    <= "1100";
        end if;
      end if;
    end if;
  end process;

  a0  <= v10(as(0)(11 downto 2));
  a1  <= v10(as(1)(11 downto 2));
  eos  <= es;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
