--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for utility package of the FEP

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;

library random_lib;
use random_lib.rnd.all;

use work.type_pkg.all;
use work.fep_pkg.all;

entity fep_pkg_sim is
  generic(n: positive := 1000000);
end entity fep_pkg_sim;

architecture sim of fep_pkg_sim is

begin

  process
    variable l: line;
  begin
    if not check then
      write(l, string'("fep_pkg: error"));
      writeline(output, l);
      write(l, string'("Regression test FAILED"));
      writeline(output, l);
    else
      write(l, string'("Regression test PASSED"));
      writeline(output, l);
    end if;
    wait;
  end process;

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
