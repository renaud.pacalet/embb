--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief VCI ram 
--*
--* Details: VCI_RAM is a general purpose, embedded, 64-bits wide, memory. This VCI-compliant RAM is customizable through 3 generic parameters and an
--* initialization file:
--*
--* - The natural parameter NA is the log2 of the memory depth. The memory depth is thus 2^NA with a maximum of 2^17 (128k double words, that is 512 kBytes).
--*   Default: 10, that is 2^10 = 1k x 64.
--* 
--* - The string INIT_FILE parameter is either the empty string or the full path of an initialization file, relative to where the simulation is launched.
--*   Default: "" (empty string), that is no initialization file.
--* 
--* - The natural parameter SI is ignored when INIT_FILE="", else it is the double-word address of the first 64-bits words to initialize. Default: 0.
--*
--* The initialization files are ASCII files, containing at most 2^NA-SI hex values, one per 64 bits word to initialize, separated by spaces, tabs or ends of
--* line. The first value in init file 'INIT_FILE' initializes the word at address SI. If a file contains less that 2^NA-SI entries the remaining addresses are
--* uninitialized. An error is raised when a file contains more than 2^NA-SI entries, when an entry represents a value larger than 64 bits or when INIT_FILE is
--* not the empty string and SI>=2^NA or the corresponding file is not found or not readable.
--* 
--* Example of initialization file: if INIT_FILE="data.txt", NA=8, SI=5, and the file named data.txt contains:
--* 
--* 	FFFEFDFC
--* 	0
--* 	0101010101010101 AA
--* 
--* then, the 256 x 64 = 2k-bytes VCI_RAM is initialized as follows:
--* 
--* 	Byte address   Values
--* 	0x000-0x027	   Uninitialized
--* 	0x028-0x02B	   0x00
--* 	0x02C          0xFF
--* 	0x02D          0xFE
--* 	0x02E          0xFD
--* 	0x02F          0xFC
--* 	0x030-0x037	   0x00
--* 	0x038-0x03F    0x01
--* 	0x040-0x046	   0x00
--* 	0x047          0xAA
--* 	0x048-0x7FF	   Uninitialized
--* 
--* VCI_RAM being 64-bits wide, here is the same RAM content in double-words:
--* 
--* Byte addresses	 Values
--* 0x000		         Uninitialized
--* 0x008		         Uninitialized
--* 0x010		         Uninitialized
--* 0x018		         Uninitialized
--* 0x020		         Uninitialized
--* 0x028		         0x00000000FFFEFDFC
--* 0x030		         0x0000000000000000
--* 0x038		         0x0101010101010101
--* 0x040		         0x00000000000000AA
--* 0x048-0x7F8	     Uninitialized

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;

library memories_lib;
use memories_lib.ram_pkg.all;

entity vci_ram is
  generic(
-- pragma translate_off
          debug:     boolean := true;   --* Print debug information
          verbose:   boolean := false;  --* Print more debug information
          --* Name of initialization file if any. The initialization file is ASCII and contains u_unsigned hexadecimal values, one per address, in addresses
          --* increasing order, separated by spaces (spaces, tabs or CR).
          init_file: string := "";
          --* Address of the first word in RAM of the first data in initialization file. If this parameter is not 0, then some low addresses are uninitialized.
          si: natural := 0;
-- pragma translate_on
          na: natural range 0 to 17 := 10); --* Depth (log2 of number of 64-bits words) of memory
  port(
    clk:      in  std_ulogic;   --* master clock
    srstn:    in  std_ulogic;   --* master reset
    ce:       in  std_ulogic;   --* chip enable
    tvci_in:  in  vci_i2t_type; --* Target AVCI input
    tvci_out: out vci_t2i_type; --* Target AVCI output    
    ivci_in:  in  vci_t2i_type; --* Initiator AVCI input
    ivci_out: out vci_i2t_type; --* Initiator AVCI output
    dirq:     out std_ulogic  -- DMA output irq,
  );
end entity;

architecture rtl of vci_ram is
  
  signal rams2mss: word64;
  signal mss2rams: in_port_128kx64;

begin

  i_vnr: entity work.vci_ram_no_rams(rtl)
  generic map(na => na)
  port map(clk      => clk,
           srstn    => srstn,
           ce       => ce,
           dirq     => dirq,
           tvci_in  => tvci_in,
           tvci_out => tvci_out,
           ivci_in  => ivci_in,
           ivci_out => ivci_out,
           rams2mss => rams2mss,
           mss2rams => mss2rams);

  i_rams: entity work.rams(rtl)
  generic map(
-- pragma translate_off
    init_file => init_file,
    si        => si,
-- pragma translate_on
    na       => na
  )
  port map(clk      => clk,
           rams2mss => rams2mss,
           mss2rams => mss2rams);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
