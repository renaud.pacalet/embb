/*
 * Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
 * Copyright (C) - Telecom ParisTech
 * Contacts: contact-embb@telecom-paristech.fr
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */

#include <embb/memory.h>
#include <embb/context.h>
#include <embb/model_types.hh>
#include <embb/css.h>

#include <assert.h>
#include <cstdint>
#include <cinttypes>
#include <string.h>
#include <stdlib.h>
#include "utils.h"
#include "css_addresses.h"

// #define _GNU_SOURCE
#include <getopt.h>

/* Parse command line options, initialize file names. */
void parse_options (int argc, char **argv);

FILE *f;
FILE *fi;
int nt;
int na;
int nw;
int si;
int ni;
char *init_file;

CSS_FUNC_DECL

int main(int argc, char **argv)
{
  int i, j, k;
  uint8_t rnw, vnd, msk, tmp;
  uint32_t add, len;
  struct css_regs cr; // CSS registers

  parse_options (argc, argv);

  memset(&cr, 0, sizeof(struct css_regs));

  uint8_t data[1 << (na + 3)];

  do_srand(1);

  memset(data, 0, nw * 8);
  if(init_file != NULL) {
    fi = XFOPEN(init_file, "w");
    for(i = si; i < si + ni; i++) {
      for(j = 0; j < 8; j++) {
        data[8 * i + j] = do_rand() % 256;
      }
    }
    dump_buffer(fi, data + 8 * si, 8 * ni, 1, 0, 1, 8);
    fclose(fi);
  }

  fprintf(f, "# 10 cycles reset\n");
  fprintf(f, "RS 10\n");
  fprintf(f, "# Clear interrupt flags\n");
  fprintf(f, "VR %08" PRIx32 " 1 0 FF 0 0\n", CSS_IRQ_ADDR);
  fprintf(f, "# Set resets, chip enables and interrupt enables\n");
  css_set_drst(&cr, 1);  // De-assert DMA reset
  css_set_dce(&cr, 1);   // Chip-enable DMA
  css_set_d2hie(&cr, 1); // Enable DMA interrupts to host
  css_set_hie(&cr, 1);   // Enable interrupts to host
  css_set_lhirq(&cr, 1); // Level-triggered DMA interrupts to host
  fprintf(f, "VW %08" PRIx32 " 1 0 ff %016" PRIx64 "\n", CSS_CTRL_ADDR, cr.ctrl);
  fprintf(f, "# Initialize RAM\n");
  if(init_file != NULL) {
    if(si != 0) {
      fprintf(f, "DW 0 %u 0 ff\n", si);
      for(i = 0; i < si; i++)
        fprintf(f, "0\n");
      fprintf(f, "WD %u\n", si + 1000);
    }
    if(si + ni < nw) {
      fprintf(f, "DW %08" PRIx32 " %u 0 ff\n", 8 * (si + ni), nw - si - ni);
      for(i = 0; i < nw - si - ni; i++)
        fprintf(f, "0\n");
      fprintf(f, "WD %u\n", nw - si - ni + 1000);
    }
  }
  else {
    fprintf(f, "DW 0 %u 0 ff\n", nw);
    for(i = 0; i < nw; i++)
      fprintf(f, "0\n");
  }
  for(i = 0; i < nt; i++) {
    fprintf(f, "#################\n");
    fprintf(f, "# Test #%u\n", i + 1);
    fprintf(f, "#################\n");
    rnw = do_rand() % 2;
    vnd = do_rand() % 2;
    add = do_rand() % nw;
    len = do_rand() % (nw - add) + 1;
    msk = do_rand() % 256;
    if(rnw == 0) {
      if(vnd == 1)
        fprintf(f, "VW %08" PRIx32 " );
      else
        fprintf(f, "DW ");
    }
    else {
      if(vnd == 1)
        fprintf(f, "VR ");
      else
        fprintf(f, "DR ");
    }
    fprintf(f, "%08" PRIx32 " ", 8 * add);
    fprintf(f, "%u ", len);
    fprintf(f, "0 %02x\n", msk);
    if(rnw == 0) {
      for(j = add; j < add + len; j++) {
        for(k = 0; k < 8; k++) {
          tmp = do_rand() % 256;
          fprintf(f, "%02x", tmp);
          if(msk & (1 << (7 - k))) {
            data[j * 8 + k] = tmp;
          }
        }
        fprintf(f, "\n");
      }
    }
    else {
      for(j = add; j < add + len; j++) {
        for(k = 0; k < 8; k++) {
          fprintf(f, "%02x", data[j * 8 + k]);
        }
        fprintf(f, "\n");
      }
    }
    if(vnd == 0)
      fprintf(f, "WD ");
    else
      fprintf(f, "WV ");
    fprintf(f, "%u\n", nw + 1000);
    fprintf(f, "# End of test #%u\n\n", i + 1);
  }
  fclose(f);
  return 0;
}

const char *version_string = "\
mkcmdfile 0.1\n\
\n\
Copyright (C) 2010 Institut Telecom\n\
\n\
This software is governed by the CeCILL license under French law and\n\
abiding by the rules of distribution of free software.  You can  use,\n\
modify and/ or redistribute the software under the terms of the CeCILL\n\
license as circulated by CEA, CNRS and INRIA at the following URL\n\
http://www.cecill.info\n\
\n\
Written by Renaud Pacalet <renaud.pacalet@telecom-paristech.fr>.\n\
";

const char *usage_string = "\
mkcmdfile generate commands for VHDL functional simulation\n\
of a DSP unit or an IP core.\n\
\n\
Usage: mkcmdfile [OPTION]... [OUTPUTFILE]\n\
\n\
Mandatory arguments to long options are mandatory for short options too.\n\
If OUTPUTFILE is not specified stdout is used. On success the exit status is 0.\n\
\n\
Options:\n\
  -l, --log2len=NUM        log2 of memory depth; 0<=NUM<=17 (default=0)\n\
  -f, --init-file=STRING   name of initialization file if any (default=none)\n\
  -s, --start-init=NUM     address of first word to initialize if initialization file\n\
  -n, --num_init=NUM       number of words to initialize if initialization file\n\
  -t, --tests=NUM          number of tests to generate (default=1)\n\
  -v, --version\n\
        Output version information and exit\n\
  -h, --help\n\
        Display this help and exit\n\
\n\
Please report bugs to <renaud.pacalet@telecom-paristech.fr>.\n\
";

void
parse_options (int argc, char **argv) {
  int o, option_index;
  struct option long_options[] = {
    {"log2len", required_argument, NULL, 'l'},
    {"init-file", required_argument, NULL, 'i'},
    {"start-init", required_argument, NULL, 's'},
    {"num-init", required_argument, NULL, 'n'},
    {"tests", required_argument, NULL, 't'},
    {"version", no_argument, NULL, 'v'},
    {"help", no_argument, NULL, 'h'},
    {0, 0, 0, 0}
  };

  option_index = 0;
  nt = 1;
  na = 0;
  si = 0;
  ni = 0;
  init_file = NULL;
  while (1) {
    o = getopt_long (argc, argv, "l:i:s:n:t:vh", long_options, &option_index);
    if (o == -1) {
      break;
    }
    switch (o) {
      case 'l':
        na = atoi(optarg);
        break;
      case 'i':
        init_file = (char *)(XMALLOC(strlen(optarg) + 1));
        strcpy(init_file, optarg);
        break;
      case 's':
        si = atoi(optarg);
        break;
      case 'n':
        ni = atoi(optarg);
        break;
      case 't':
        nt = atoi(optarg);
        break;
      case 'v':
        fprintf (stderr, "%s", version_string);
        exit (0);
        break;
      default:
        fprintf (stderr, "%s", usage_string);
        exit (0);
        break;
    }
  }
  if(na < 0 || na > 17) {
    fprintf (stderr, "*** Invalid log2 of memory depth: %d\n", na);
    ERROR(-1, "%s", usage_string);
  }
  nw = 1 << na;
  if(init_file != NULL && (si < 0 || ni < 1 || si + ni >= nw)) {
    fprintf (stderr, "*** Invalid start-init / num-init combination: %d/%d (memory depth = %d)\n", si, ni, nw);
    ERROR(-1, "%s", usage_string);
  }
  if(nt < 1) {
    fprintf (stderr, "*** Invalid number of tests: %d\n", nt);
    ERROR(-1, "%s", usage_string);
  }
  f = stdout;
  if (optind < argc) {
    f = XFOPEN(argv[optind], "w");
    optind += 1;
  }
  if (optind < argc) {
    ERROR(-1, "%s", usage_string);
  }
}
