--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Memory access manager

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library global_lib;
use global_lib.global.all;
use global_lib.utils.all;

library memories_lib;
use memories_lib.ram_pkg.all;

entity mss is
  generic(na: natural range 0 to 17 := 10); --* Depth (log2 of number of 64-bits words) of memory
  port(clk:       in  std_ulogic;
       css2mss:   in  css2mss_type;
       mss2css:   out mss2css_type;
       mss2rams : out in_port_128kx64;
       rams2mss : in  word64);
end entity mss;

architecture rtl of mss is

  alias dma2mss is css2mss.dma2mss;
  alias mss2dma is mss2css.mss2dma;
  alias vci2mss is css2mss.vci2mss;
  alias mss2vci is mss2css.mss2vci;

begin

  mss2css.mss2uc <= mss2uc_none;

  --* The arbiter combinatorial process computes the different requests each client sends to each Block RAM. It then enables only one client according to the
  --* fixed priority: DMA (highest) and VCI (lowest).
  arbiter: process(css2mss)
  begin
    --+ by default, grant DMA request and no out-of-memory access
    mss2dma.gnt <= (others => '1');
    mss2dma.oor <= '0';
    --+ by default, don't grant request and no out-of-memory access
    mss2vci.gnt <= (others => '0');
    mss2vci.oor <= '0';
    --* by default no request
    mss2rams    <= in_port_128kx64_default;

    --* DMA requests are always served.
    if dma2mss.en = '1' then 
      mss2rams.en    <= '1';
      mss2rams.we    <= band((not dma2mss.rnw), dma2mss.be);
      mss2rams.wdata <= dma2mss.wdata;
      mss2rams.add   <= dma2mss.add(16 downto 0);
      mss2dma.gnt    <= dma2mss.be;
      if or_reduce(dma2mss.add(28 downto na)) /= '0' then
-- pragma translate_off
        assert false report "DMA: out of range memory access (" & integer'image(to_integer(u_unsigned(dma2mss.add))) & " >= " & integer'image(na) severity failure;
-- pragma translate_on
        mss2dma.oor <= '1';
      end if;
    elsif vci2mss.en = '1' then --* If no DMA request, VCI requests are always served.
      mss2rams.en    <= '1';
      mss2rams.we    <= band((not vci2mss.rnw), vci2mss.be);
      mss2rams.wdata <= vci2mss.wdata;
      mss2rams.add   <= vci2mss.add(16 downto 0);
      mss2vci.gnt    <= vci2mss.be;
      if or_reduce(vci2mss.add(28 downto na)) /= '0' then
-- pragma translate_off
        assert false report "VCI: out of range memory access (" & integer'image(to_integer(u_unsigned(vci2mss.add))) & " >= " & integer'image(na) severity failure;
-- pragma translate_on
        mss2vci.oor <= '1';
      end if;
    end if;

  end process arbiter;

  mss2dma.rdata <= rams2mss;
  mss2vci.rdata <= rams2mss;

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
