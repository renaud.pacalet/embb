--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Simulation environment for whole VCI_RAM

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;

entity vci_ram_sim is
  generic(cmdfile:   string  := "vci_ram.cmd";
          debug:     boolean := true;      --* Print debug information
          verbose:   boolean := false;     --* Print more debug information
          init_file: string  := "";
          si:        natural := 0;
          na:        natural := 10);
end entity vci_ram_sim;

architecture sim of vci_ram_sim is

  signal clk:      std_ulogic;
  signal srstn:    std_ulogic;
  signal ce:       std_ulogic;
  signal dirq:     std_ulogic;
  signal hst2tvci: vci_i2t_type;
  signal tvci2hst: vci_t2i_type;
  signal hst2ivci: vci_t2i_type;
  signal ivci2hst: vci_i2t_type;

begin

  i_vr: entity work.vci_ram(rtl)
  generic map(debug     => debug,
              verbose   => verbose,
              init_file => init_file,
              si        => si,
              na        => na)
  port map(clk      => clk,
           srstn    => srstn,
           ce       => ce,
           dirq     => dirq,
           tvci_in  => hst2tvci,
           tvci_out => tvci2hst,
           ivci_in  => hst2ivci,
           ivci_out => ivci2hst);

  i_hst: entity global_lib.hst_emulator(arc)
	generic map(cmdfile   => cmdfile,
              n_pa_regs => 0)
  port map(clk       => clk,
           srstn     => srstn,
           ce        => ce,
           hirq      => open,
           dirq      => dirq,
           pirq      => '0',
           uirq      => '0',
           eirq      => (others => '0'),
           hst2tvci  => hst2tvci,
           tvci2hst  => tvci2hst,
           hst2ivci  => hst2ivci,
           ivci2hst  => ivci2hst,
           gpodvalid => open,
           gpod      => open,
           gpovvalid => open,
           gpov      => open,
           eos       => open);

end architecture sim;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
