--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief RAMS instantiation

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;

library memories_lib;
use memories_lib.ram_pkg.all;

entity rams is
  generic(
-- pragma translate_off
          --* Name of initialization file if any. The initialization file is ASCII and contains u_unsigned hexadecimal values, one per address, in addresses
          --* increasing order, separated by spaces (spaces, tabs or CR).
          init_file: string := "";
          --* Address of the first word in RAM of the first data in initialization file. If this parameter is not 0, then some low addresses are uninitialized.
          si: natural := 0;
-- pragma translate_on
          na: natural range 0 to 17 := 10); --* Depth (log2 of number of 64-bits words) of memory
  port(clk:       in  std_ulogic;
       mss2rams : in  in_port_128kx64;
       rams2mss : out word64);
end entity rams;

architecture rtl of rams is

begin
  
  ram_i: entity memories_lib.spram(arc)
    generic map(
-- pragma translate_off
                init_file     => init_file,
                si            => si,
                instance_name => "VCI_RAM",
-- pragma translate_on
                registered    => true,
                na            => na,
                nb            => 8)
  port map(
    clk  => clk,
    en   => mss2rams.en,
    we   => mss2rams.we,
    add  => mss2rams.add(na - 1 downto 0),
    din  => mss2rams.wdata,
    dout => rams2mss);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
