#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

proc usage {} {
    puts "\
usage: precision <options> \[-shell\] -file  <script> -fileargs \"<helper>\"
  <options>:  precision command line options
  <script>:   TCL synthesis script
  <helper>:   helper TCL script"
}

if { [llength $argv] == 1 } {
    set HELPER [lindex $argv 0]
} else {
    usage
    exit -1
}

# Source helper script
source ${HELPER}

regexp {([^\.]+)\.([^\.]+).*} $TARGET dummy LIB PUNIT

# Source myself to get rid of annoying echo of TCL commands
if ![ info exists already_sourced ] {
    set already_sourced 1
    source pr-syn.${TARGET}.tcl
}

# Toplevel entity name
set topentity $PUNIT

# Parameters that influence the synthesis flow and their default values
array set synthesis_options {
      addio                         false
      auto_resource_allocation_ram  true
      bottom_up_flow                false
      compile_for_timing            true
      dsp_across_hier               true
      edif                          true
      family                        Zynq
      generics                      { { n0 1 } { n1 1 } }
      hdl                           vhdl_2008
      ignore_ram_rw_collision       true
      input_delay                   2500
      manufacturer                  Xilinx
      max_fanout                    16
      max_fanout_strategy           AUTO
      output_delay                  2500
      part                          7Z020CLG484
      period                        5000
      resource_sharing              false
      speed                         1
}

set_results_dir .

# Read HDL files
load_vhdl

# Apply synthesis options
setup_design xcv7
setup_design -vivado
setup_design -addio=$synthesis_options(addio)
setup_design -auto_resource_allocation_ram=$synthesis_options(auto_resource_allocation_ram)
setup_design -bottom_up_flow=$synthesis_options(bottom_up_flow)
setup_design -compile_for_timing=$synthesis_options(compile_for_timing)
setup_design -design=$topentity
setup_design -dsp_across_hier=$synthesis_options(dsp_across_hier)
setup_design -edif=$synthesis_options(edif)
setup_design -manufacturer=$synthesis_options(manufacturer) -family=$synthesis_options(family) -part=$synthesis_options(part) -speed=$synthesis_options(speed)
if { [ llength $synthesis_options(generics) ] != 0 } {
    setup_design -overrides $synthesis_options(generics)
}
setup_design -frequency=[ expr 1000000 / $synthesis_options(period) ]
setup_design -hdl=$synthesis_options(hdl)
setup_design -ignore_ram_rw_collision=$synthesis_options(ignore_ram_rw_collision)
setup_design -input_delay=[ expr $synthesis_options(input_delay) / 1000 ]
setup_design -max_fanout=$synthesis_options(max_fanout)
setup_design -max_fanout_strategy=$synthesis_options(max_fanout_strategy)
setup_design -output_delay=[ expr $synthesis_options(output_delay) / 1000 ]
setup_design -resource_sharing=$synthesis_options(resource_sharing)

# Reports
puts "--------------------------------------------------------------------------------"
puts "Input files:"
puts "--------------------------------------------------------------------------------"
report_input_file_list
puts "--------------------------------------------------------------------------------"
puts "Output files:"
puts "--------------------------------------------------------------------------------"
report_output_file_list
puts "--------------------------------------------------------------------------------"

# Compile
compile

# Reports
puts "--------------------------------------------------------------------------------"
puts "Constraints:"
puts "--------------------------------------------------------------------------------"
report_constraints
puts "--------------------------------------------------------------------------------"

synthesize

# Area report
puts "--------------------------------------------------------------------------------"
puts "Area report:"
puts "--------------------------------------------------------------------------------"
report_area

# Timing report
puts "--------------------------------------------------------------------------------"
puts "Timing report:"
puts "--------------------------------------------------------------------------------"
report_timing

# Messages
set rundir [pwd]
puts ""
puts "\[PRECISION\]: done"
puts "  area report:       $rundir/${topentity}_area.rep"
puts "  timing report:     $rundir/${topentity}_timing.rep"
puts "  verilog netlist:   $rundir/${topentity}.v"

if {[string length "$I"]==0} {
    close_results_dir
    quit
}

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
