--
-- Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
-- Copyright (C) - Telecom ParisTech
-- Contacts: contact-embb@telecom-paristech.fr
-- 
-- Embb is governed by the CeCILL license under French law and abiding by the rules
-- of distribution of free software. You can use, modify and/ or redistribute the
-- software under the terms of the CeCILL license. You should have received a copy
-- of the CeCILL license along with this program; if not, you can access it online
-- at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
--

--* @brief Top level without RAMs

library ieee;
use ieee.std_logic_1164.all;

library global_lib;
use global_lib.global.all;

library css_lib;

library memories_lib;
use memories_lib.ram_pkg.all;

entity vci_ram_no_rams is
  generic(
-- pragma translate_off
    debug:     boolean               := true;  --* Print debug information
    verbose:   boolean               := false; --* Print more debug information
-- pragma translate_on
    na:        natural range 0 to 17 := 10     --* Depth (log2 of number of 64-bits words) of memory
  );
  port(clk:      in  std_ulogic;       --* master clock
       srstn:    in  std_ulogic;       --* master reset
       ce:       in  std_ulogic;       --* chip enable
       dirq:     out std_ulogic;  -- DMA output irq,
       tvci_in:  in  vci_i2t_type;     --* Target AVCI input
       tvci_out: out vci_t2i_type;     --* Target AVCI output
       ivci_in:  in  vci_t2i_type;     --* Initiator AVCI input
       ivci_out: out vci_i2t_type;     --* Initiator AVCI output
       rams2mss: in  word64;           --* Inputs from external RAMs
       mss2rams: out in_port_128kx64); --* Outputs to external RAMs
end entity vci_ram_no_rams;

architecture rtl of vci_ram_no_rams is
  
  signal css2mss:  css2mss_type;
  signal mss2css:  mss2css_type;

begin

  i_css: entity css_lib.css(rtl)
  generic map(
-- pragma translate_off
    debug     => debug,
    verbose   => verbose,
    name      => "VCI RAM",
-- pragma translate_on
    n_pa_regs => 0,
    pa_rmask  => "",
    pa_wmask  => "",
    with_pss   => false,
    with_uc   => false
  )
  port map(clk      => clk,
           srstn    => srstn,
           ce       => ce,
           hirq     => '0',
           uirq     => open,
           pirq     => open,
           dirq     => dirq,
           eirq     => open,
           pss2css   => pss2css_none,
           css2pss   => open,
           param    => open,
           mss2css  => mss2css,
           css2mss  => css2mss,
           tvci_in  => tvci_in,
           tvci_out => tvci_out,
           ivci_in  => ivci_in,
           ivci_out => ivci_out);

  i_mss: entity work.mss(rtl)
    generic map(na => na)
    port map(clk       => clk,
             css2mss   => css2mss,
             mss2css   => mss2css,
             rams2mss  => rams2mss,
             mss2rams  => mss2rams);

end architecture rtl;

-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
