#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

gh-IGNORE	+= vci_ram_lib.vci_ram_no_rams vci_ram_lib.vci_ram vci_ram_lib.top vci_ram_lib.vci_ram_sim

vci_ram_lib.mss: \
	global_lib.global \
	global_lib.utils \
	memories_lib.ram_pkg

vci_ram_lib.rams: \
	global_lib.global \
	memories_lib.ram_pkg \
	memories_lib.spram

vci_ram_lib.top: \
	global_lib.global \
	global_lib.utils \
	vci_ram_lib.vci_ram

vci_ram_lib.vci_ram_no_rams: \
	global_lib.global \
	memories_lib.ram_pkg \
	css_lib.css \
	vci_ram_lib.mss

vci_ram_lib.vci_ram: \
	global_lib.global \
	memories_lib.ram_pkg \
	vci_ram_lib.rams \
	vci_ram_lib.vci_ram_no_rams

vci_ram_lib.vci_ram_sim: \
	global_lib.global \
	global_lib.hst_emulator \
	vci_ram_lib.vci_ram

# Simulations

vci_ram_lib_csrc	:= $(hwprj_db_path.vci_ram_lib)/C
vci_ram_lib_cmdfile	:= $(vci_ram_lib_csrc)/vci_ram.cmd
VCI_RAM_LIB_NT		:= 1000
VCI_RAM_LIB_NA		:= 7
VCI_RAM_LIB_INITFILE	:= data.txt
VCI_RAM_LIB_SI		:= 11
VCI_RAM_LIB_NI		:= 100
VCI_RAM_LIB_CMDGEN	:= mkcmdfile
vci_ram_lib_cmdgen	:= $(vci_ram_lib_csrc)/$(VCI_RAM_LIB_CMDGEN)
VCI_RAM_LIB_CMDGENFLAGS	:= --log2len=$(VCI_RAM_LIB_NA) --init-file="$(VCI_RAM_LIB_INITFILE)" --start-init=$(VCI_RAM_LIB_SI) --num-init=$(VCI_RAM_LIB_NI) --tests=$(VCI_RAM_LIB_NT)

ms-sim.vci_ram_lib.%: MSSIMFLAGS += -do 'run -all; quit' -t ps
ms-sim.vci_ram_lib.vci_ram_sim: $(vci_ram_lib_cmdfile)
ms-sim.vci_ram_lib.vci_ram_sim: MSSIMFLAGS += -Gcmdfile="$(vci_ram_lib_cmdfile)" -Ginit_file="$(VCI_RAM_LIB_INITFILE)" -Gsi=$(VCI_RAM_LIB_SI) -Gna=$(VCI_RAM_LIB_NA) -Gdebug=true -Gverbose=false
ms-sim.vci_ram_lib: ms-sim.vci_ram_lib.vci_ram_sim
ms-sim: ms-sim.vci_ram_lib

.PHONY: $(vci_ram_lib_cmdgen)

$(vci_ram_lib_cmdfile): $(vci_ram_lib_cmdgen)
	$(vci_ram_lib_cmdgen) $(VCI_RAM_LIB_CMDGENFLAGS) $@

$(vci_ram_lib_cmdgen):
	$(MAKE) -C $(dir $@) $(notdir $@)

# Synthesis

$(addprefix pr-syn.vci_ram_lib.,vci_ram top): $(hwprj_db_path.css_lib)/bitfield_pkg.vhd

hw-clean: vci_ram_lib_clean

vci_ram_lib_clean:
	$(MAKE) -C $(vci_ram_lib_csrc) clean
	rm -rf $(vci_ram_lib_cmdfile) $(VCI_RAM_LIB_INITFILE)

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
