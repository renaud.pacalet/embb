#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
# 
# Embb is governed by the CeCILL license under French law and abiding by the rules
# of distribution of free software. You can use, modify and/ or redistribute the
# software under the terms of the CeCILL license. You should have received a copy
# of the CeCILL license along with this program; if not, you can access it online
# at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#

IGNORE		+= leon3_coprocessor spectra_sparc_lib

# Bitfields
tooldir		:= $(abspath $(hwprj)/..)
bitfielddir	:= $(rootdir)/../bitfields
BFGEN		:= $(tooldir)/bfgen/src/bfgen
BFGENFLAGS	:= --endian=little name=bitfield --backend=vhdl2

$(hwprj_db_path.crossbar_lib)/bitfield_pkg.vhd: BFGENFLAGS := --endian=little name=bitfield --backend=vhdl

# $(1): lib
# $(2): path
define BITFIELD_rules
$(1).bitfield_pkg: $$(hwprj_db_path.$(1))/bitfield_pkg.vhd
$$(hwprj_db_path.$(1))/bitfield_pkg.vhd: $$(bitfielddir)/$(1).bitfield
	$(Q)$(ECHO) '[BFGEN] $$(BFGENFLAGS) $$< -> $$@' && \
	$$(BFGEN) $$(BFGENFLAGS) --input=$$< --output=$$@
endef
$(foreach l,$(hwprj_db_lib),$(if $(wildcard $(bitfielddir)/$(l).bitfield),$(eval $(call BITFIELD_rules,$(l)))))

ms-reg.%: ms-sim.%
	@[ -f $(msbuilddir)/$<.log ] && grep -io 'non regression test passed' $(msbuilddir)/$<.log || exit 1

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
