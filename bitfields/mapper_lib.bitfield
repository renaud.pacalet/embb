#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#


name: mapper
longname: Mapper
address: 0x000fff00
align: 64
bound: 32

%r0
  width: 64
  %%lenm1
    position: 0
    width: 12
    longname: LENgth Minus 1
    default: 0
    doc: Input frame is lenm1 + 1 symbols long

  %%__padding__
    width: 4

  %%lba
    width: 10
    position: 16
    default: 0
    longname: Look-up table Base Address
    doc: Word address of first entry of look-up-table in LUT. 

  %%__padding__
    width: 6

  %%oba
    width: 12
    position: 32
    default: 0
    longname: Output Base Address
    doc: Word address of first output point of output frame in DO. 
  
  %%__padding__
    width: 4

  %%iba
    width: 16
    position: 48
    default: 0
    longname: Input Base Address
    doc: Bit address of first symbol of input frame in DI. 

%r1
  width: 64

  %%mult
    width: 32
    position: 0
    default: 0
    longname: MULTiplier
    doc: Coefficient the output points are optionally multiplied by prior storage in DO

  %%men
    width: 1
    position: 32
    default: 0
    longname: Multiplier ENable
    doc: When set, enables the optional multiplication of the output points by MULT prior storage in DO

  %%sym
    width: 1
    position: 33
    default: 0
    longname: SYMmetrical mode
    doc: When set, input symbols are split in two halves and each half addresses LUT independently.

  %%__padding__
    width: 6

  %%bpsm1
    width: 4
    position: 40
    default: 0
    longname: Bits Per Symbol Minus 1
    doc: Symbols are bpsm1 + 1 bits wide

  %%__padding__
    width: 4

  %%m
    width: 8
    position: 48
    default: 0
    doc: Fractionnal part of output addresses increment (m!=0 => 1/m, m==0 => 0)

  %%n
    width: 7
    position: 56
    default: 0
    doc: Integer part of output addresses increment

  %%s
    width: 1
    position: 63
    default: 0
    longname: Sign
    doc: Sign of output addresses increment
