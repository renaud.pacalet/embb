#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#


name: css
longname: Control Sub-System
address: 0x000fff00
bound: 64
align: 64

%dcfg: 64
  longname: Dma ConFiG
  doc: DMA configuration register, flags, byte enables and transfer byte length.
  address: 0x80

  %%dls: 0-0
    longname: Dma Local Source
    doc: When set, the source of the DMA transfer is MSS and the source address is a byte offset relative to the base address of the DSP unit.
    doc: When unset, the source is an external memory location and the source address is a system byte address. Ignored when DCS is set.
    default: 0
    0: External
    1: Local

  %%dld: 1-1
    longname: Dma Local Destination
    doc: Same as DLS but for destination address.
    default: 0
    0: External
    1: Local

  %%dfs: 2-2
    longname: Dma Fixed Source double-word
    doc: When set, the source address wraps around an 8-bytes boundary, else it is self-incremented. Ignored when DCS is set.
    default: 0
    0: Incremental
    1: Fixed

  %%dfd: 3-3
    longname: Dma Fixed Destination double-word
    doc: Same as DFS but for destination address.
    default: 0
    0: Incremental
    1: Fixed

  %%dcs: 4-4
    longname: Dma Constant Source data
    doc: When set, the source data is the content of the CST register; DSRC, DLS and DFS are ignored. Used to initialize memory areas.
    default: 0
    0: Disabled
    1: Enabled

  %%__padding__
    width: 3

  %%dbe: 8-15
    longname: Dma Byte Enable
    doc: Byte enable for write operations. Each bit enables (1) or disables (0) the writing of a byte of the 64-bits data at destination.
    doc: Bit #15 corresponds to the leftmost byte (63 downto 56) of written data while bit #8 corresponds to the rightmost (7 downto 0).
    doc: This byte enable indicator is double-word aligned: bit #15 always correspond to a byte which address is a multiple of 8.

  %%__padding__
    width: 16

  %%dlenm1: 32-63
    longname: Dma transfer LENght Minus 1
    doc: Transfer length in bytes, minus one, as if DBE=0xFF. The length includes bytes that are actually write-protected by unset DBE bits: when DFD=0, for
    doc: instance, LENM1=A2-A1 where A1 is the byte address of the first written byte if DBE=0xFF and A2 is the byte address of the last written byte if
    doc: DBE=0xFF.

%dcst: 64
  longname: Dma ConSTant data value
  doc: Constant value used to initialize memory areas. Source data of the transfer when DCS=1. Ignored when DCS=0.
  address: 0x88

  %%dcstl: 0-31
    longname: Dma ConSTant data value low
    doc: Constant value used to initialize memory areas. Source data of the transfer when DCS=1. Ignored when DCS=0.

  %%dcsth: 32-63
    longname: Dma ConSTant data value high

%dadd: 64
  longname: Dma ADDresses
  doc: Source and destination addresses of DMA transfer.
  address: 0x90

  %%dsrc: 0-31
    longname: Dma SouRCe address
    doc: DMA transfer source byte address. Byte offset relative to base address of DSP unit if DLS=1 (local source),
    doc: else system byte address. Ignored when DCS is set.
  %%ddst: 32-63
    longname: Dma DeSTination address
    doc: DMA transfer destination byte address. Byte offset relative to base address of DSP unit if DLD=1 (local destination),
    doc: else system byte address.

%dgost: 64
  longname: Dma Go and STatus
  doc: DMA transfer starts upon write. Written data is ignored. Reading returns DMA status.
  address: 0x98
  direction: r

  %%dst: 0-23
    longname: Dma return STatus
    doc: Set by DMA at the end of a DMA transfer [TBC]. FIFO.

  %%derr: 24-24
    longname: Dma ERRor
    1: Error
    0: No error
    doc: Set by DMA at the end of a DMA transfer to indicate an error condition during the transfer. FIFO.

  %%dval: 25-25
    longname: Dma VALid status and error
    1: Valid
    0: Non valid
    doc: Indicate that the return status and error flag have been set and not yet read by host or UC.

  %%__padding__
    width: 38

%dgoreg: 64
  longname: Dma GO
  address: union
  doc: DMA transfer starts. Written data is ignored
  direction: w

  %%dgo: 0-0
    doc: Write this bit to start DMA.

  %%__padding__
    width: 63

%pgost: 64
  longname: Pss GO and STatus
  doc: PSS processing starts upon write. Written data is ignored. Reading returns PSS status and data.
  address: 0xA0
  direction: r

  %%pst: 0-23
    longname: Pss return STatus
    doc: DSP unit-dependant. Set by PSS at the end of a processing. Usually zero if no error, else error code. Two-stages FIFO.

  %%perr: 24-24
    longname: Pss ERRor
    1: Error
    0: No error
    doc: Set by PSS at the end of a processing to indicate an error condition. Two-stages FIFO.

  %%pval: 25-25
    longname: Pss VALid status and error
    1: Valid
    0: Non valid
    doc: Indicate that the return status and error flag have been set and not yet read by host or UC. Two-stages FIFO.

  %%__padding__
    width: 6

  %%pdata: 32-63
    longname: Pss read DATA
    doc: DSP unit-dependant. Set by PSS at the end of a processing. Usually zero if unused in DSP unit, else some data result of a command. Two-stages FIFO.

%pgoreg: 64
  longname: Pss GO and STatus
  doc: PSS processing starts upon write. Written data is ignored
  address: union
  direction: w

  %%pgo: 0-0
    doc: Write this bit to start PSS.

  %%__padding__
    width: 63

%ctrl: 64
  longname: ConTRoL register
  doc: Resets, chip enables, interrupt enables, UC debug mode,...
  address: 0xA8
  %%urst: 0-0
    longname: Uc ReSeT
    doc: Micro-controller synchronous, active low reset.

  %%prst: 1-1
    longname: Pss ReSeT
    doc: PSS processing core synchronous, active low reset.

  %%drst: 2-2
    longname: Dma ReSeT
    doc: DMA engine synchronous, active low reset.

  %%uce: 3-3
    longname: Uc Chip Enable
    doc: Micro-controller active high chip enable.

  %%pce: 4-4
    longname: Pss Chip Enable
    doc: PSS processing core active high chip enable.

  %%dce: 5-5
    longname: Dma Chip Enable
    doc: DMA engine active high chip enable.

  %%udbg: 6-6
    longname: Uc DeBuG
    doc: UC debug mode flag. When set, UC runs in step-by-step mode.

  %%__padding__
    width: 1

  %%u2hie: 8-8
    longname: Uc to Host Interrupt Enable
    doc: Micro-controller to host system interrupt enable. When unset, interrupts raised by UC are not signalled to host system.

  %%p2hie: 9-9
    longname: Pss to Host Interrupt Enable
    doc: PSS processing core to host system interrupt enable. When unset, PSS ends of computation are not signalled to host system.

  %%d2hie: 10-10
    longname: Dma to Host Interrupt Enable
    doc: DMA engine to host system interrupt enable. When unset, DMA ends of transfer are not signalled to host system.

  %%e2hie: 11-11
    longname: Extended interrupts to Host Interrupt Enable
    doc: Extended interrupts to host system interrupt enable. When unset, extended interrupts are not signalled to host system.

  %%hie: 12-12
    longname: Host Interrupt Enable
    doc: Global interrupt enable for host system. When unset none of the UC, PSS, DMA or extended interrupts are signalled to host system,
    doc: even if individually enabled.

  %%__padding__
    width: 3

  %%p2uie: 16-16
    longname: Pss to Uc Interrupt Enable
    doc: PSS processing core to micro-controller interrupt enable. When unset, PSS ends of computation are not signalled to UC.

  %%d2uie: 17-17
    longname: Dma to Uc Interrupt Enable
    doc: DMA engine to micro-controller interrupt enable. When unset, DMA ends of transfer are not signalled to UC.

  %%h2uie: 18-18
    longname: Host to Uc Interrupt Enable
    doc: Host system to micro-controller interrupt enable. When unset, interrupts raised by host system are not signalled to UC.

  %%e2uie: 19-19
    longname: Extended interrupts to Uc Interrupt Enable
    doc: Extended interrupts to micro-controller interrupt enable. When unset, extended interrupts are not signalled to UC.

  %%uie: 20-20
    longname: Uc Interrupt Enable
    doc: Global interrupt enable for micro-controller. When unset none of the PSS, DMA, host system or extended interrupts are signalled to UC,
    doc: even if individually enabled.

  %%__padding__
    width: 3

  %%lhirq: 24-24
    longname: Level-triggered Host Interrupt ReQuests
    doc: Selects level- (when set) or edge-triggered (when unset) UC, PSS and DMA interrupt requests to host.

  %%luirq: 25-25
    longname: Level-triggered Uc Interrupt ReQuests
    doc: Selects level- (when set) or edge-triggered (when unset) PSS, DMA and host system interrupt requests to UC.

  %%leirq: 26-26
    longname: Level-triggered Extended Interrupt ReQuests
    doc: Selects level- (when set) or edge-triggered (when unset) extended interrupt requests to host system and UC.

  %%__padding__
    width: 5

  %%eie: 32-63
    longname: Extended Interrupt Enables
    doc: Enables of the (up to) 32 DSP-unit dependent extended interrupts.

%irq: 64
  longname: Interrupt ReQuests
  doc: Interrupt flags. When set, they indicate a pending interrupt. If the corresponding interrupt enable is set along with the UC
  doc: (or host system) global interrupt enable, the interrupt is forwarded to UC (or host system).
  address: 0xB0
  %%u2hirq: 0-0
    longname: Uc to Host Interrupt ReQuest
    doc: Micro-controller to host system interrupt status. Set by UC upon writing a 1 (set-only).
    doc: If interrupts to host are level-triggered, de-asserted by host with clear-on-read.
    direction: r

  %%p2hirq: 1-1
    longname: Pss to Host Interrupt ReQuest
    doc: PSS processing core to host system interrupt status. Set by PSS when ending a processing. 
    doc: Used only when interrupts to host are edge-triggered.
    direction: r

  %%d2hirq: 2-2
    longname: Dma to Host Interrupt ReQuest
    doc: DMA engine to host system interrupt status. Set by DMA when ending a transfer.
    doc: Used only when interrupts to host are edge-triggered.
    direction: r

  %%__padding__
    width: 1

  %%dbsy: 4-4
    longname: Dma BuSY
    doc: When set indicates that DMA is currently running. Read only.
    1: Busy
    0: Idle
    direction: r

  %%drqp: 5-5
    longname: Dma ReQuest Pending
    1: Request pending
    0: Idle
    doc: Indicates that a DMA request is already pending and will be sent to DMA after completion of the current transfer. Read only.
    direction: r

  %%__padding__
    width: 2

  %%p2uirq: 8-8
    longname: Pss to Uc Interrupt ReQuest
    doc: PSS processing core to micro-controller interrupt status. Set by PSS when ending a processing.
    doc: De-asserted by UC with clear-on-read.
    direction: r

  %%d2uirq: 9-9
    longname: Dma to Uc Interrupt ReQuest
    doc: DMA engine to micro-controller interrupt status. Set by DMA when ending a transfer.
    doc: De-asserted by UC with clear-on-read.
    direction: r

  %%h2uirq: 10-10
    longname: Host to Uc Interrupt ReQuest
    doc: Host system to micro-controller interrupt status. Set by host system upon writing a 1 (set-only) or raising HIRQ input.
    doc: De-asserted by UC with clear-on-read.

  %%__padding__
    width: 1

  %%pbsy: 12-12
    longname: Pss BuSY
    1: Busy
    0: Idle
    doc: When set indicates that PSS is currently running. Read only.
    direction: r

  %%prqp: 13-13
    longname: Pss ReQuest Pending
    1: Request pending
    0: Idle
    doc: Indicates that a PSS request is already pending and will be sent to PSS after completion of current processing. Read only.
    direction: r

  %%__padding__
    width: 18

  %%eirq: 32-63
    longname: Extended Interrupt ReQuests
    doc: Up to 32 DSP-unit dependent extended interrupts. Signalling defined by lhirq. Set by PSS. Clear-on-read when level-triggered.
    direction: r

%uregs: 64
  longname: Uc internal REGisterS
  doc: Read-only, host system-only register. Corresponds to the internal registers of UC.
  address: 0xB8
  direction: r

  %%upc: 0-15
    longname: Uc Program Counter

  %%usp: 16-23
    longname: Uc Stack Pointer

  %%ua: 24-31
    longname: Uc Accumulator

  %%ux: 32-39
    longname: Uc X register

  %%uy: 40-47
    longname: Uc Y register

  %%uc: 48-48
    longname: Uc Carry flag

  %%uz: 49-49
    longname: Uc Zero flag

  %%ui: 50-50
    longname: Uc Interrupt disable flag

  %%ud: 51-51
    longname: Uc Decimal mode flag

  %%ub: 52-52
    longname: Uc Break flag

  %%__padding__
    width: 1

  %%uv: 54-54
    longname: Uc oVerflow flag

  %%un: 55-55
    longname: Uc Negative flag

  %%__padding__
    width: 8

%uprphs: 64
  longname: Uc PeRiPHerals' registers
  doc: Interface registers of UC peripherals.
  address: 0xF0
  access_width: 8

  %%ufpd: 0-7
    longname: Uc Fast Path Destination index (0 to 31)
    doc: The index of the first 64 bits register target of a FastPath transfer

  %%ufpl: 8-15
    longname: Uc Fast Path Length (1 to 32)
    doc: Number of 64 bits registers of FastPath transfer

  %%ufpo: 16-23
    longname: Uc Fast Path source Offset (0 to 255)
    doc: Double-word offset in UCR of a FastPath transfer.

  %%ufpb: 24-31
    longname: Uc Fast Path source Base address (0 to |UCR|/2048)
    doc: Double-word base address in UCR of a FastPath transfer

  %%usig: 32-63
    longname: Uc SIGnalling
    doc: UIRQ is raised when UC writes the least significant byte. HIRQ is raised when host writes the least significant byte. Used for signalling with value
    doc: passing between host and DSP unit. Can also be used for signalling between DSP units.

%uvects: 64
  longname: Uc VECTorS
  doc: The UC address space 0xFFF8-0xFFFF is mapped to this register. 0xFFF8 (bits 56-63) is the EOS virtual register (used to end simulations). 0xFFF9
  doc: (bits 48-55) is the TTY virtual register (used for printing during simulations). The 3 other 16-bits fields are the byte address of UC IRQ, startup
  doc: and NMI routines.
  address: 0xF8

  %%uirqv: 0-15
    longname: Uc IRQ Vector
    doc: 16-bits byte address of UC IRQ subroutine. 8-15 is the least significant byte.

  %%urstv: 16-31
    longname: Uc ReSeT Vector
    doc: 16-bits byte address of UC startup routine. 24-31 is the least significant byte.

  %%unmiv: 32-47
    longname: Uc NMI Vector
    doc: 16-bits byte address of UC NMI subroutine. 40-47 is the least significant byte.

  %%tty: 48-55
    longname: TTY virtual register
    doc: During simulations, when a character is written in TTY, the character is sent to the simulator's standard output.

  %%eos: 56-63
    longname: End Of Simulation virtual register
    doc: During simulations, when EOS is written, the simulation ends gracefully and the written value is printed to the simulator's standard output.
