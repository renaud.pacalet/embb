#
# Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
# Copyright (C) - Telecom ParisTech
# Contacts: contact-embb@telecom-paristech.fr
#
# This file must be used under the terms of the CeCILL.
# This source file is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at
# http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
#


name: adaif_hreg
longname: ADAIF_HREG
address: 0x0
bound: 32
align: 32

%wptrt0
  width: 32
  %%wptrt0
    width: 11
    longname: Write PoinTeR Tx0
    doc: Write pointer of TX0 FIFO

  %%__padding__
    width: 21

%wptrr0
  width: 32
  %%wptrr0
    width: 11
    longname: Write PoinTeR Rx0
    doc: Write pointer of RX0 FIFO

  %%__padding__
    width: 21

%wptrt1
  width: 32
  %%wptrt1
    width: 11
    longname: Write PoinTeR Tx1
    doc: Write pointer of TX1 FIFO

  %%__padding__
    width: 21

%wptrr1
  width: 32
  %%wptrr1
    width: 11
    longname: Write PoinTeR Rx1
    doc: Write pointer of RX1 FIFO

  %%__padding__
    width: 21

%wptrt2
  width: 32
  %%wptrt2
    width: 11
    longname: Write PoinTeR Tx2
    doc: Write pointer of TX2 FIFO

  %%__padding__
    width: 21

%wptrr2
  width: 32
  %%wptrr2
    width: 11
    longname: Write PoinTeR Rx2
    doc: Write pointer of RX2 FIFO

  %%__padding__
    width: 21

%wptrt3
  width: 32
  %%wptrt3
    width: 11
    longname: Write PoinTeR Tx3
    doc: Write pointer of TX3 FIFO

  %%__padding__
    width: 21

%wptrr3
  width: 32
  %%wptrr3
    width: 11
    longname: Write PoinTeR Rx3
    doc: Write pointer of RX3 FIFO

  %%__padding__
    width: 21

%cntt0
  width: 32
  %%cntt0
    width: 32
    longname: CouNTer Tx0
    doc: Free running counter of TX0 interface

%cntr0
  width: 32
  %%cntr0
    width: 32
    longname: CouNTer Rx0
    doc: Free running counter of RX0 interface

%cntt1
  width: 32
  %%cntt1
    width: 32
    longname: CouNTer Tx1
    doc: Free running counter of TX1 interface

%cntr1
  width: 32
  %%cntr1
    width: 32
    longname: CouNTer Rx1
    doc: Free running counter of RX1 interface

%cntt2
  width: 32
  %%cntt2
    width: 32
    longname: CouNTer Tx2
    doc: Free running counter of TX2 interface

%cntr2
  width: 32
  %%cntr2
    width: 32
    longname: CouNTer Rx2
    doc: Free running counter of RX2 interface

%cntt3
  width: 32
  %%cntt3
    width: 32
    longname: CouNTer Tx3
    doc: Free running counter of TX3 interface

%cntr3
  width: 32
  %%cntr3
    width: 32
    longname: CouNTer Rx3
    doc: Free running counter of RX3 interface

%lvlt0
  width: 32
  %%lvlt0
    width: 12
    longname: LeVeL Tx0
    doc: Filling level of TX0 FIFO

  %%__padding__
    width: 4

  %%uirqt0
    width: 1
    longname: Underflow Interrupt ReQuest Tx0
    doc: Interrupt flag signalling underflow of TX0 FIFO

  %%oirqt0
    width: 1
    longname: Overflow Interrupt ReQuest Tx0
    doc: Interrupt flag signalling overflow of TX0 FIFO

  %%mirqt0
    width: 1
    longname: Mid Interrupt ReQuest Tx0
    doc: Interrupt flag signalling that TX0 FIFO went half-empty

  %%heft0
    width: 1
    longname: Half Empty/Full Tx0
    doc: Status flag indicating that TX0 FIFO is half-empty or less

  %%et0
    width: 1
    longname: Empty Tx0
    doc: Status flag indicating that TX0 FIFO is empty

  %%ft0
    width: 1
    longname: Full Tx0
    doc: Status flag indicating that TX0 FIFO is full

  %%__padding__
    width: 10

%lvlr0
  width: 32
  %%lvlr0
    width: 12
    longname: LeVeL Tx0
    doc: Filling level of TX0 FIFO

  %%__padding__
    width: 4

  %%uirqr0
    width: 1
    longname: Underflow Interrupt ReQuest Rx0
    doc: Interrupt flag signalling underflow of RX0 FIFO

  %%oirqr0
    width: 1
    longname: Overflow Interrupt ReQuest Rx0
    doc: Interrupt flag signalling overflow of RX0 FIFO

  %%mirqr0
    width: 1
    longname: Mid Interrupt ReQuest Rx0
    doc: Interrupt flag signalling that RX0 FIFO went half-full

  %%hefr0
    width: 1
    longname: Half Empty/Full Rx0
    doc: Status flag indicating that RX0 FIFO is half-full or more

  %%er0
    width: 1
    longname: Empty Rx0
    doc: Status flag indicating that RX0 FIFO is empty

  %%fr0
    width: 1
    longname: Full Rx0
    doc: Status flag indicating that RX0 FIFO is full

  %%__padding__
    width: 10

%lvlt1
  width: 32
  %%lvlt1
    width: 12
    longname: LeVeL Tx1
    doc: Filling level of TX1 FIFO

  %%__padding__
    width: 4

  %%uirqt1
    width: 1
    longname: Underflow Interrupt ReQuest Tx1
    doc: Interrupt flag signalling underflow of TX1 FIFO

  %%oirqt1
    width: 1
    longname: Overflow Interrupt ReQuest Tx1
    doc: Interrupt flag signalling overflow of TX1 FIFO

  %%mirqt1
    width: 1
    longname: Mid Interrupt ReQuest Tx1
    doc: Interrupt flag signalling that TX1 FIFO went half-empty

  %%heft1
    width: 1
    longname: Half Empty/Full Tx1
    doc: Status flag indicating that TX1 FIFO is half-empty or less

  %%et1
    width: 1
    longname: Empty Tx1
    doc: Status flag indicating that TX1 FIFO is empty

  %%ft1
    width: 1
    longname: Full Tx1
    doc: Status flag indicating that TX1 FIFO is full

  %%__padding__
    width: 10

%lvlr1
  width: 32
  %%lvlr1
    width: 12
    longname: LeVeL Tx1
    doc: Filling level of TX1 FIFO

  %%__padding__
    width: 4

  %%uirqr1
    width: 1
    longname: Underflow Interrupt ReQuest Rx1
    doc: Interrupt flag signalling underflow of RX1 FIFO

  %%oirqr1
    width: 1
    longname: Overflow Interrupt ReQuest Rx1
    doc: Interrupt flag signalling overflow of RX1 FIFO

  %%mirqr1
    width: 1
    longname: Mid Interrupt ReQuest Rx1
    doc: Interrupt flag signalling that RX1 FIFO went half-full

  %%hefr1
    width: 1
    longname: Half Empty/Full Rx1
    doc: Status flag indicating that RX1 FIFO is half-full or more

  %%er1
    width: 1
    longname: Empty Rx1
    doc: Status flag indicating that RX1 FIFO is empty

  %%fr1
    width: 1
    longname: Full Rx1
    doc: Status flag indicating that RX1 FIFO is full

  %%__padding__
    width: 10

%lvlt2
  width: 32
  %%lvlt2
    width: 12
    longname: LeVeL Tx2
    doc: Filling level of TX2 FIFO

  %%__padding__
    width: 4

  %%uirqt2
    width: 1
    longname: Underflow Interrupt ReQuest Tx2
    doc: Interrupt flag signalling underflow of TX2 FIFO

  %%oirqt2
    width: 1
    longname: Overflow Interrupt ReQuest Tx2
    doc: Interrupt flag signalling overflow of TX2 FIFO

  %%mirqt2
    width: 1
    longname: Mid Interrupt ReQuest Tx2
    doc: Interrupt flag signalling that TX2 FIFO went half-empty

  %%heft2
    width: 1
    longname: Half Empty/Full Tx2
    doc: Status flag indicating that TX2 FIFO is half-empty or less

  %%et2
    width: 1
    longname: Empty Tx2
    doc: Status flag indicating that TX2 FIFO is empty

  %%ft2
    width: 1
    longname: Full Tx2
    doc: Status flag indicating that TX2 FIFO is full

  %%__padding__
    width: 10

%lvlr2
  width: 32
  %%lvlr2
    width: 12
    longname: LeVeL Tx2
    doc: Filling level of TX2 FIFO

  %%__padding__
    width: 4

  %%uirqr2
    width: 1
    longname: Underflow Interrupt ReQuest Rx2
    doc: Interrupt flag signalling underflow of RX2 FIFO

  %%oirqr2
    width: 1
    longname: Overflow Interrupt ReQuest Rx2
    doc: Interrupt flag signalling overflow of RX2 FIFO

  %%mirqr2
    width: 1
    longname: Mid Interrupt ReQuest Rx2
    doc: Interrupt flag signalling that RX2 FIFO went half-full

  %%hefr2
    width: 1
    longname: Half Empty/Full Rx2
    doc: Status flag indicating that RX2 FIFO is half-full or more

  %%er2
    width: 1
    longname: Empty Rx2
    doc: Status flag indicating that RX2 FIFO is empty

  %%fr2
    width: 1
    longname: Full Rx2
    doc: Status flag indicating that RX2 FIFO is full

  %%__padding__
    width: 10

%lvlt3
  width: 32
  %%lvlt3
    width: 12
    longname: LeVeL Tx3
    doc: Filling level of TX3 FIFO

  %%__padding__
    width: 4

  %%uirqt3
    width: 1
    longname: Underflow Interrupt ReQuest Tx3
    doc: Interrupt flag signalling underflow of TX3 FIFO

  %%oirqt3
    width: 1
    longname: Overflow Interrupt ReQuest Tx3
    doc: Interrupt flag signalling overflow of TX3 FIFO

  %%mirqt3
    width: 1
    longname: Mid Interrupt ReQuest Tx3
    doc: Interrupt flag signalling that TX3 FIFO went half-empty

  %%heft3
    width: 1
    longname: Half Empty/Full Tx3
    doc: Status flag indicating that TX3 FIFO is half-empty or less

  %%et3
    width: 1
    longname: Empty Tx3
    doc: Status flag indicating that TX3 FIFO is empty

  %%ft3
    width: 1
    longname: Full Tx3
    doc: Status flag indicating that TX3 FIFO is full

  %%__padding__
    width: 10

%lvlr3
  width: 32
  %%lvlr3
    width: 12
    longname: LeVeL Tx3
    doc: Filling level of TX3 FIFO

  %%__padding__
    width: 4

  %%uirqr3
    width: 1
    longname: Underflow Interrupt ReQuest Rx3
    doc: Interrupt flag signalling underflow of RX3 FIFO

  %%oirqr3
    width: 1
    longname: Overflow Interrupt ReQuest Rx3
    doc: Interrupt flag signalling overflow of RX3 FIFO

  %%mirqr3
    width: 1
    longname: Mid Interrupt ReQuest Rx3
    doc: Interrupt flag signalling that RX3 FIFO went half-full

  %%hefr3
    width: 1
    longname: Half Empty/Full Rx3
    doc: Status flag indicating that RX3 FIFO is half-full or more

  %%er3
    width: 1
    longname: Empty Rx3
    doc: Status flag indicating that RX3 FIFO is empty

  %%fr3
    width: 1
    longname: Full Rx3
    doc: Status flag indicating that RX3 FIFO is full

  %%__padding__
    width: 10

