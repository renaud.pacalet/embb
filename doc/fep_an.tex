%
% Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
% Copyright (C) - Telecom ParisTech
% Contacts: contact-embb@telecom-paristech.fr
% 
% Embb is governed by the CeCILL license under French law and abiding by the rules
% of distribution of free software. You can use, modify and/ or redistribute the
% software under the terms of the CeCILL license. You should have received a copy
% of the CeCILL license along with this program; if not, you can access it online
% at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
%

This chapter lists use-cases or applications of the functionalities provided by Front-End Processor (FEP) for different air-interfaces. It can be used as a
starting point to design applications using FEP.

\section{Conventions and notations}

The conventions and notations of the FEP user guide are used but with the following differences:

\begin{itemize}
  \item Uppercase letters denote frequency domain data, either scalar or vectors while lowercase letters denote time domain data.
  \item The product by a scalar $a$ is denoted $a\cdot X$, $aX$ or $a\times X$.
\end{itemize}

\section{Use for Time and Frequency Synchronization \label{sec:fep_tfsynch}}

\subsection{Time Synchronization}

Timing synchronization is typically achieved using a sliding window correlation with a known pilot waveform transmitted from a synchronization source. For
example, in 802.11a/g/p systems\cite{spec:dot11}, the so-called {\em long training sequence (LTS)} is used. The structure of this signal is chosen to offer a
compromise between efficient time synchronization and carrier frequency offset estimation. In LTE \cite{spec:lte211}, the so-called {\em primary synchronization
signal (PSS)} is used for the same purpose.

In the FEP, the time offset estimation process can be efficiently implemented using an overlapping FFT-based correlator, followed by non-coherent peak
detection. Let $p(n), 0\leq n<L_p$ be the reference pilot signal used for time synchronization, and $L_p$ its length in samples. For 802.11a/g/p $L_p$ is 160
samples, while in LTE $L_p$ is 128 samples (assuming down sampling to 6 resource blocks during initial acquisition). Let $r(n)$ be the complex-baseband received
signal and define the detection problem as:
\begin{equation*}
  r(n) = e^{j2\pi n\Delta f_{\mathrm{coarse}}}p(n)*h(n-\Delta n) + z(n)
\end{equation*}
where $\Delta f_{\mathrm{coarse}}$ is an unknown frequency offset to be estimated subsequently to $\Delta n$. Here the effect of the channel is unknown, which
infers a non-coherent detection rule. Typically, even the statistical characterization of $h[n]$ is assumed unknown, aside from an underlying assumption on its
effective time duration (time delay spread).  Define signal segments of dimension $N_p$ samples:
\begin{equation*}
  r_k=\left[r(kO_p),r(kO_p+1),\cdots,r(kO_p+N_p-1)\right]
\end{equation*}
where $N_p$ can be chosen to be $2^{1+\lfloor\log_2L_p\rfloor}$, that is 256 for both 802.11a/g/p and LTE. $O_p$ would be the overlap factor $N_p-L_p$.  This
choice of overlap guarantees that the signal component at delay $\Delta n$ falls entirely in only one segment $r_k$. Now define:
\begin{equation*}
  R_k={DFT}_{N_p}(r_k)
\end{equation*}
to be the DFT of $r_k$. The correlation operation would be performed as:
\begin{equation*}
  q_k={IDFT}_{N_p}(R_k\times P^*)
\end{equation*}
where $P=\mathrm{DFT}_{N_p}(p')$ and
\begin{equation*}
  p'=\left[p(0),p(1),\cdots,p(L_p-1),\underbrace{0,\cdots,0}_{O_p\;\;\mathrm{times}}\right].
\end{equation*}
The statistics for detection of $\Delta n$ are:
\begin{equation*}
  q_{k,\mathrm{max}} = \max(q_k)
\end{equation*} %\note[RP]{2011-05-09: shouldn't it be $\max(\lvert q_k\rvert^2)$ instead of $\max(q_k)$?}
and
\begin{equation*}
  \Delta\tilde{n} = \argmax(q_k)
\end{equation*} %\note[RP]{2011-05-09: shouldn't it be $\argmax(\lvert q_k\rvert^2)$ instead of $\argmax(q_k)$?}
Detection of $\Delta n$ would typically be controlled by a threshold on $q_{k,\mathrm{max}}$ derived from the average received energy, $\lambda(E_k)$, where
$E_k$ could be computed as $E_k = \alpha E_{k-1} + (1-\alpha)E(r_{k})$, where $\alpha$ controls the memory of the energy computation. One possible rule (not
necessarily optimal) would be to choose $\Delta\tilde{n}$ when $q_{k,\mathrm{max}}>\lambda(E_k)$ and $q_{k,\mathrm{max}}>q_{k-1,\mathrm{max}}$ and
$q_{k,\mathrm{max}}>q_{k+1,\mathrm{max}}$.

\subsection{Coarse Frequency Offset Estimation}

Once time synchronization is achieved (hypothetically) in segment $k$, estimation of $\Delta f_{\mathrm{coarse}}$ can be attempted using $R_k$. One possible
method for this estimation would be to compute the following statistics based on $R_k$:
\begin{equation*}
  q_{k+} = \frac{1}{\sqrt{N_p}}(R_k\times P_{+}^*)\odot\psi(\Delta\tilde{n})
\end{equation*}
and
\begin{equation*}
  q_{k-} = \frac{1}{\sqrt{N_p}}(R_k\times P_{-}^*)\odot\psi(\Delta\tilde{n})
\end{equation*}
where $\psi(x)[n]=e^{j\frac{2\pi}{N_p}nx}$. The $P_{+}$ and $P_{-}$ are computed as:
\begin{equation*}
  P_{+}={DFT}_{N_p}(p_{+})
\end{equation*}
and
\begin{equation*}
  P_{-}={DFT}_{N_p}(p_{-})
\end{equation*}
where $p_{+}(n)=p(n)e^{j2\pi n\Delta f_{\max}}$ and $p_{-}(n)=p(n)e^{-j2\pi n\Delta f_{\max}}$. $P_{+},P_{-}$ and $\psi(x)$ can be computed off-line.  $\Delta
f_{\max}$ is a parameter which represents the maximal frequency offset that a receiver should expect (it can be derived from the standards specification of the
system). The statistic $|q_{k+}|^2$ roughly represents the amount of signal energy at a frequency offset of $\Delta f_{\max}$ from the carrier frequency in the
direction of the pilot waveform. Based on the three statistics $q_{k,\max}$, $q_{k+}$ and $q_{k-}$ a simple binomial interpolation function can be used to
estimate the carrier frequency offset $\Delta\tilde{f}$. The maximum of the binomial is used as an estimate of the frequency offset. After a bit of algebra,
this estimate is given by:
\begin{equation*}
  \Delta\tilde{f}_{\mathrm{coarse}} = \frac{|q_{k-}|^2-|q_{k+}|^2}{2(|q_{k-}|^2+|q_{k+}|^2-2q_{k,\max})}\Delta f_{\max}
\end{equation*}
This is motivated by the fact that:
\begin{equation*}
  q(u) = \left.p(n)e^{j2\pi n\Delta f_{\mathrm{coarse}}}*p^*(-n)e^{-j2\pi un}\right|_{n=\Delta\tilde{n}}
\end{equation*}
is a convex-$\bigcap$ function.

\section{Use in 802.11a/g/p Systems}

Here we consider a candidate set of operations for use of the FEP specifically for 802.11a/g/p systems. Specific details for all parameters and signal
descriptions in this section can be found in \cite{spec:dot11}.

\subsection{Channel Estimation}

Channel estimation is achieved using two pilot OFDM symbols (long training symbol or LTS) each with 64 information samples following 32 prefix samples, at the
beginning of a SIGNAL/DATA burst. For simplicity, consider the case of using a single OFDM symbol for channel estimation, $r_{\mathrm{LTS}}$ which is
transformed as $R_{\mathrm{LTS}}=DFT_{64}(r_{\mathrm{LTS}})$ and given by:
\begin{equation*}
  R_{\mathrm{LTS}} = \left(H\times P_{\mathrm{LTS}}\right)+Z
\end{equation*}
where $Z$ is additive noise, $H$ is an unknown channel to be estimated and $P_{\mathrm{LTS}}$ is the frequency-domain representation of the 802.11 long training
symbol. The least-squares estimate of $H$ under no assumption of prior knowledge on $H$ (i.e. even no knowledge of the time delay spread) is:
\begin{equation*}
  \hat{H} = R_{\mathrm{LTS}}\times P_{\mathrm{LTS}}^*
\end{equation*}
This estimate can be improved (if required) through additional smoothing/interpolation functions, by taking into account the typical duration (and shape) of the
channel response.

\subsection{Carrier Phase Offset Estimation}

For residual frequency-offset estimation, the problem amounts to phase estimation. The received signal for symbol $n$ in the SIGNAL/DATA burst is given by:
\begin{equation*}
  R_n = \left(e^{j \phi_n} H \times X_n\right)+Z_n
\end{equation*}
where $\phi_n$ is the phase offset induced by the residual frequency offset and phase noise. We assume that the channel $H$ is unchanged with respect to its
state during the preamble period. $X_n$ and $Z_n$ are the signal and noise components respectively. Prior to data detection, estimation of $e^{\phi_n}$ must be
achieved using additional pilot symbols interleaved in $X_n$. In 802.11a/g/p, 4 pilot carriers are used out of 64 carrier positions (in positions -21,-7,7,21 in
physical frequency DFT ordering) and assume pseudo-random values from the set $\{+1,-1\}$ (i.e. BPSK modulated). The phase offset with respect to the symbol
with which channel estimation was performed can be estimated as:
\begin{equation*}
  A(H) e^{-j\hat{\phi_n}} = \frac{1}{4} \sum_{i=-21,-7,7,21} (P_{\mathrm{LTS}}\times X_n)[i](R_{\mathrm{LTS}}^{*}\times R_{n})[i]
\end{equation*}
%\note[RP]{2011-05-09: shouldn't it be $P_{\mathrm{LTS}}^*\times X_n$ instead of $P_{\mathrm{LTS}}\times X_n$?}
where $A(H)$ is a scalar representing the amplitude of the product, its value is dependent on $H$ and given by:
\begin{equation*}
  A(H) = \frac{1}{4}\sum_{i=-21,-7,7,21}\left(|H[i]|^2 + H^{*}[i]Z_n[i]+H^*[i]Z[i]+Z^*[i]Z_n[i]\right)
\end{equation*}
This unknown term can be approximated by:
\begin{equation*}
  \hat{A}(H) = \frac{1}{4}\sum_{i=-21,-7,7,21}|\hat{H}[i]|^2
\end{equation*}

\subsection{Channel Compensation (Equalization)}

Both $A(H) e^{-j\hat{\phi_n}}$ and $\hat{A}(H)$ will be used in conjunction with the channel estimate $\hat{H}$ to provide sufficient statistics for data
detection (in the FEP or in another dedicated co-processor). One target for data detection is to produce a ``equalised'' signal:
\begin{equation*}
  R_{d,n} = (1+\epsilon)\times X_n+Z_n^{out}
\end{equation*}
where $\epsilon[i]\ll 1$ represents the error due to phase and channel estimation, and should be almost negligible. This can be accomplished by the following
operations:
\begin{equation*}
  R_{d,n} = \frac{A(H)e^{-j\hat{\phi_{n}}}}{\hat{A}(H)}\cdot\frac{\hat{H^{*}} \times R_n}{\hat{H}\times\hat{H}^{*}}
\end{equation*}
This statistic in conjunction with the channel estimate $\hat{H}$ is sufficient for detection (LLR generation). Both should be the outputs of the FEP if
generation of the LLRs are done in an outside co-processor (detector). Note that the component-wise division of a complex vector by a real vector is not yet
supported by FEP. The division by $\hat{H}\times\hat{H}^{*}$ (which is a real vector) requires first a component-wise lookup operation to approximate the
component-wise inverse, followed by a component-wise product.

\subsection{802.11a/g/p Data Detection with FEP}

Thanks to the input value modifiers, the FEP can compute $\abs(x)$, component-wise absolute value of both the real and imaginary components of vector $x$. FEP
can thus be used for data detection in the case of Gray-coded QAM modulation (all known systems!). Consider first the case of QPSK modulation. Gray coding
ensures that the real and imaginary components can be completely decoupled in the detection process. Here we use the statistic:
\begin{equation*}
  R_{d,n} = \frac{A(H)e^{-j\hat{\phi_{n}}}}{\hat{A}(H)}\cdot(\hat{H^{*}} \times R_n) = \left((1\times\epsilon)\times |H|^2\times X_n\right)+Z_n^{out}
\end{equation*}
which is sufficient for detection (channel decoding). In the case of 16QAM modulation $R_{d,n}$ is sufficient for two out of four bits, the exact bits depend on
the mapping used. The remaining two bits are obtained as:
\begin{equation*}
  R_{d2,n} = \abs(R_{d,n})-\frac{2}{\sqrt{10}}\cdot(\hat{H}\times\hat{H}^*)
\end{equation*}
which is approximately sufficient for detection. In the case of 64QAM modulation $R_{d,n}$ is sufficient for two out of four bits, and again the exact bits
depend on the mapping used. The next two bits are obtained as:
\begin{equation*}
  R_{d2,n} = \abs(R_{d,n})-\frac{4}{\sqrt{42}}\cdot(\hat{H}\times\hat{H}^*)
\end{equation*}
and the remaining two bits as:
\begin{equation*}
  R_{d3,n} = \abs(R_{d2,n})-\frac{2}{\sqrt{42}}\cdot(\hat{H}\times\hat{H}^*)
\end{equation*}

\section{Use in LTE}

Detailed descriptions for all LTE parameters and configurations described in this section can be found in \cite{spec:lte211}.

\subsection{Primary Synchronization in UE}

Primary synchronization in LTE can make use of the methods described in Section \ref{sec:fep_tfsynch} using the {\em primary synchronization signal}.

\subsection{Secondary Synchronization in UE}

This is for later ...

\subsection{Downlink Channel Estimation}

Downlink channel estimation for the basic control (PBCH, PDCCH) and downlink shared channels (PDSCH) in LTE is achieved using the so-called {\em Cell-specific
Reference Signals (RS)}. An LTE subframe consists of either 12 (extended prefix mode) or 14 (normal prefix mode) OFDM symbols spanning 1ms, 4 of which contain
pilot or reference elements in the case of 1 or 2 transmit antennas. 6 symbols contain pilot elements in the case of 4 transmit antennas.  For brevity, we
consider the case of 2 transmit antennas in this document. The OFDM symbols containing pilots are in positions 0,3,6,9 in the case of 12 symbol subframes and in
positions 0,4,7,11 in the case of 14 symbol subframes ((see Figure 6.1.10.2-1 in \cite{spec:lte211}). The resource elements containing pilot carriers are evenly
spaced in the OFDM symbol as described by the set $\{k:k\bmod 6=(\nu+\nu_{\mathrm{shift}})\bmod 6\}$ (see Section 6.1.10.2 in \cite{spec:lte211}), where $\nu$
takes on the values ${0,3}$ depending on the transmit antenna index and OFDM symbol number. If $\nu=0$ on antenna 0 then $\nu=3$ on antenna 1 and vice versa,
which guarantees that transmit antennas use orthogonal pilot signals. $\nu_{\mathrm{shift}}$ is a cell-specific frequency shift, which is typically chosen so
that adjacent cell sites use orthogonal pilot signals.

Channel estimation is a two-step process. Upon receiving the pilot-containing OFDM symbols in symbol epoch $l$ on all receiver antennas, the channel elements in
the pilot positions are estimated as:
\begin{multline*}
  \hat{H}_{l,i,j}[6k+(\nu_{l,i}+\nu_{\mathrm{shift}})\bmod 6] = \\
    R_{l,j}[6k+(\nu_{l,i}+\nu_{\mathrm{shift}})\bmod 6]\times RS_{l,i}^*[6k+(\nu_{l,i}+\nu_{\mathrm{shift}})\bmod 6], \\
    k=0,1,\cdots,2N_{\mathrm{PRB}}-1
  \end{multline*}
where $N_{\mathrm{PRB}}$ is the number of {\rm physical resource blocks}, a parameter which depends on the system bandwidth. For 20MHz bandwidth
$N_{\mathrm{PRB}}=100$. $i$ represents the transmit antenna index and $j$ the receive antenna index. For a $2\times 2$ system, this operation is repeated 4
times with the appropriate values of $\nu_{l,i}$ and $RS_{l,i}$ for each $R_{l,0}$ and $R_{l,1}$. Note that $RS_{l,i}$ contains the reference elements for all
shifts and not only the ones used for the current cell. This is imposed by the constraint on the common addressing of the operands of the subband componentwise
product. For each of the partial channel estimates, frequency interpolation is performed to fill in the missing components. The simplest method is based on
linear interpolation where the 5 channel elements in positions between two RS elements would be given by:
\begin{multline*}
  \hat{H}_{l,i,j}[f+6k+(\nu_{l,i}+\nu_{\mathrm{shift}})\bmod 6] = \\
    \left(1-\frac{f}{6}\right)\hat{H}_{l,i,j}[6k+(\nu_{l,i}+\nu_{\mathrm{shift}})\bmod 6]+
      \left(\frac{f}{6}\right)\hat{H}_{l,i,j}[6(k+1)+(\nu_{l,i}+\nu_{\mathrm{shift}})\bmod 6], \\
    0<f<6
\end{multline*}
This would be implemented as component-wise products and adds with an extensive use of the FEP address generators (periodic access over vectors of interpolation
coefficients, oversampling over channel estimates, \ldots):
\begin{align*}
  \forall\ m&=6k+(\nu_{l,i}+\nu_{\mathrm{shift}})\bmod 6, \\
  \hat{H}_{l,i,j}[m+1..m+5]&=\hat{H}_{l,i,j}[m]\times \begin{pmatrix}5/6&2/3&1/2&1/3&1/6\end{pmatrix} + \\
                   &\;\;\hat{H}_{l,i,j}[m+6]\times \begin{pmatrix}1/6&1/3&1/2&2/3&5/6\end{pmatrix}
\end{align*}
Care must be taken at the extremities where extrapolation must be performed and around the DC resource element where the interpolation is over 7 positions
instead of 6. The basic operation remains the same. Once the entire channel estimate for the pilot-bearing symbol is complete, channels in between the current
symbol and previous pilot-bearing symbol can be found by temporal interpolation. The channel response including the residual phase offset after coarse frequency
estimation and correction can be closely approximated as:
\begin{equation*}
  H_{l,i,k}=e^{j2\pi l\Delta f_{\mathrm{fine}}}H_{l,i,k}'\approx (1+2\pi l\Delta f_{\mathrm{fine}})H_{l,i,k}',
\end{equation*}
under the important assumption that the residual frequency offset after coarse frequency correction and any Doppler frequency shift due to mobile objects,
$\Delta f_{\mathrm{fine}}$, is small compared to the OFDM carrier spacing (15 kHz in LTE). This linear variation of the channel response between pilot-bearing
symbols can again be easily be estimated by linear interpolation between the two channel estimates which include the phase offsets and can be achieved, for the
case of normal prefix in symbols $l=1$,2 and 3 as:
\begin{equation*}
  \hat{H}_{l,i,k} = \left(1-\frac{l}{4}\right)\times\hat{H}_{0,i,k}+\left(\frac{l}{4}\right)\times\hat{H}_{4,i,k}
\end{equation*}
in symbols $l=5$,6 as:
\begin{equation*}
  \hat{H}_{l,i,k} = \left(1-\frac{l-4}{3}\right)\times\hat{H}_{4,i,k}+\left(\frac{l-4}{3}\right)\times\hat{H}_{7,i,k}
\end{equation*}
in symbols $l=8$,9 and 10 as:
\begin{equation*}
  \hat{H}_{l,i,k} = \left(1-\frac{l-8}{4}\right)\times\hat{H}_{7,i,k}+\left(\frac{l-8}{4}\right)\times\hat{H}_{11,i,k}
\end{equation*}
and in symbols $l=12$,and 13 as:
\begin{equation*}
  \hat{H}_{l,i,k} = \left(1-\frac{l-12}{3}\right)\times\hat{H}_{11,i,k}+\left(\frac{l-12}{3}\right)\times\hat{H}_{0,i,k}.
\end{equation*}
The operation is similar for the case of extended prefix. Temporal interpolation methods such as this will yield unsatisfactory performance if $\Delta
f_{\mathrm{fine}}$ is too significant, especially for high spectral-efficiency transmission (high MCS). More sophisticated techniques would have to be
considered. $\Delta f_{\mathrm{fine}}$ on the order of 50 Hz should be sufficient even for high spectral-efficiency transmission.

\subsection{UE Measurements}

The measurements performed by the UE in LTE make use of the channel estimates and raw signal samples. These operations will be detailed later, but essentially
require:
\begin{itemize}
  \item Wideband energy computation from channel estimates. This is required to compute wideband received signal strength indicators (RSSI) and wideband
    signal-to-noise ratios (SNR).
  \item Subband energy computations from channel estimates. This is required to compute subband SNRs for channel-quality indicators (CQI).
  \item Subband energy computations from received signal vectors during known blank periods to estimate noise levels.
  \item Component-wise products and dot-products for precoding-matrix indicator (PMI) computation.
\end{itemize}

\subsection{Timing drift adjustment}

Timing adjustment is required in order to track the drift induced by slight offsets in sampling frequency at the UE with respect to that used to generate the
incoming signal. This can be performed periodically based on the time-domain representation of the channel estimates. Tracking loops could make use of the
statistic:
\begin{equation*}
  \Delta\tilde{t} = \argmax\left(IDFT_{N_\mathrm{RE}}(\hat{H}_{l,i,j})\right)
\end{equation*}
which is the peak location of the dominant path in the impulse response.  $N_\mathrm{RE}$ is the number of carriers or resource elements of the OFDM symbols.
For robustness through transmit and/or receive diversity, this could be done using the channel response which is strongest among the set of transmit/receive
antenna pairs.

\subsection{SIMO Channel Compensation and Data Detection \label{sec:fep_SIMOlte}}

Consider now the channel compensation (equalization) process in LTE for single-antenna transmission, or the so-called {\em Transmission Mode 1}. This procedure
could be applied to all downlink physical channels (PBCH, PCFICH/PDCCH and PDSCH). We assume a receiver with $M$ antennas and received signals:
\begin{equation*}
  R_{n,\mathrm{j}} = \left(H_{j} \times X_n\right)+Z_{n,j}, j=0,1,\cdots,M-1
\end{equation*}
The following statistic is sufficient for data detection in the case of QPSK transmission since LTE employs Gray-coding:
\begin{equation*}
  R_{d,n} = \sum_{j=0}^{M-1}\hat{H_j^{*}} \times R_{n,j} = \left((1+\epsilon)\times \sum_{j=0}^{M-1}|H_{j}|^2\right)\times X_n+Z_n^{out}
\end{equation*}
The real and imaginary components of $R_{d,n}$ can be passed directly to the deinterleaver prior to channel decoding. Similarly to the case in 802.11a/g/p 16QAM
data can be detected first using $R_{d,n}$ for two of the bits and using:
\begin{equation*}
  R_{d2,n} = \abs(R_{d,n})-\frac{2}{\sqrt{10}}\left(\sum_{j=0}^{M-1} \hat{H_j^{*}}\times\hat{H_j^{*}}\right)
\end{equation*}
for the remaining two bits, using the $\abs(\cdot)$ operator. For 64QAM data the two statistics in addition to $R_{d,n}$ would be:
\begin{equation*}
  R_{d2,n} = \abs(R_{d,n})-\frac{4}{\sqrt{42}}\left(\sum_{j=0}^{M-1} \hat{H_j}\times\hat{H_j^{*}}\right)
\end{equation*}
and
\begin{equation*}
  R_{d3,n} = \abs(R_{d2,n})-\frac{2}{\sqrt{42}}\left(\sum_{j=0}^{M-1} \hat{H_j}\times\hat{H_j^{*}}\right)
\end{equation*}
Note that these operations require the presence of an $\abs(\cdot)$ operator. In the event that a generic QAM detector is used outside the FEP, the statistic:
\begin{equation*}
  R_{d,n} = \frac{\sum_{j=0}^{M-1}\hat{H_j^{*}}\times R_{n,j}}{\sum_{j=0}^{M-1} \hat{H_j}\times\hat{H_j^{*}}} = \left((1+\epsilon)\times X_n\right)+Z_n^{out}
\end{equation*}
along with $\left(\sum_{j=0}^{M-1} \hat{H_j}\times\hat{H_j^{*}}\right)$ would be passed to the detector. Again, the component-wise division by a real vector
would be approximated by a component-wise lookup followed by a component-wise product.

\subsection{MISO and MIMO Channel Compensation- Transmit Diversity\label{sec:fep_chcomptdlte}}

Dual antenna transmit diversity in LTE makes use of a space-frequency code based on Alamouti's original space-time code. It is used in all transmission modes
for control information (PBCH,PDCCH), and for PDSCH in {\em Transmission Mode 2}. It is the fallback solution for all transmission modes on the PDSCH when
performance enhancements of more complex transmission techniques do not prove to be beneficial because of channel conditions or type of traffic. The receiver in
the UE must employ a linear combination of resource elements of the received OFDM symbol. For OFDM symbols not containing the reference signals these are always
adjacent elements, whereas in the symbols containing reference signals, the reference resource elements must be skipped, and the linear combination sometimes
straddles the reference element. For the first case, we first compute the statistics:
\begin{equation*}
  R_{d,n,i} = \sum_{j=0}^{M-1}\hat{H_{i,j}^{*}} \times R_{n,j}, i=0,1
\end{equation*}
And thanks to the advanced addresses generators, the following operations are performed as two consecutive operations:
\begin{align*}
  R_{d,n}[2k]   &= R_{d,n,0}[2k]+R_{d,n,1}^*[2k+1] \\
  R_{d,n}[2k+1] &= R_{d,n,0}[2k+1]-R_{d,n,1}^*[2k]
\end{align*}
Data detection is then performed in a similar manner to the process described in Section \ref{sec:fep_SIMOlte}.

In the second case, a slightly more sophisticated use of the addresses generators is required. In some cases, copy ($\MOV$) operations skipping the reference
elements may be needed before the linear combination.

\subsection{MISO and MIMO Channel Compensation- Single-layer Precoding}

This channel compensation is used by the UE when configured in {\em Transmission Mode 6}. We compute the statistics $R_{d,n,i}$ as in Section
\ref{sec:fep_chcomptdlte}. For contiguous groups of resource blocks known to the UE, the UE performs the linear combination:
\begin{equation*}
  R_{d,n,g} = R_{d,n,0,g}+\left(R_{d,n,1,g}\times q_g\right)
\end{equation*}
where $g$ indicates the group of contiguous resource blocks, and $q_g\in\{1,-1,j,-j\}$ is the known precoding constant for group $g$.

\subsection{Dual-stream Channel Compensation}

This channel compensation is used by the UE when configured in either {\em Transmission Mode 4} (point-to-point MIMO) or {\em Transmission Mode 5} (Multiuser
MIMO). Later ...

\section{Use in OFDM Systems}

\subsection{Single Antenna Receiver}

\begin{figure}[htbp!]
  \begin{center}
    \scalebox{0.5}{\input{build/fep_ofdm-fig.pdf_t}}
    \caption{OFDM receiver parameters}
    \label{fig:fep_OFDM_receiver_parameters}
  \end{center}
\end{figure}

The received signal in case of single antenna is given by:
\begin{equation}
  R_n = e^{j \phi} H \times X_n + Z_n
\end{equation}
where $H, X, \text{and}\ Z$ represent Channel Response, Data-transmitted, and noise respectively. The data transmitted is assumed to be QAM-constellation, with
variance equal to 1.

The signal fed to detector at the end of FEP operations should look like:
\begin{equation}
  R = X_n + Z_n^{out}
\end{equation}
i.e. FEP operations should extract the transmitted signal from the convolution with the channel response and noise. This requires to calculate phase estimate $
\hat{e^{j\phi}} $ and channel estimate $\hat{H}$.

The two basic pilot arrangement schemes used in OFDM systems are illustrated in Figure \ref{fig:fep_pilots}. The first one, block-type pilot channel estimation,
is performed by inserting pilot tones into all subcarriers of OFDM symbols within a specific period. The second one, diffused pilot symbols or comb-type pilot
channel estimation, is performed by inserting pilot tones into certain subcarriers of each OFDM symbol, where the interpolation is needed to estimate the
conditions of data subcarriers. The strategies of these two basic types are analyzed in the next sections.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.5}{\input{build/fep_pilots-fig.pdf_t}}
    \caption{Two Basic Types of Pilot Arrangement in OFDM Systems}
    \label{fig:fep_pilots}
  \end{center}
\end{figure}

\subsubsection{Case: Diffused Pilots Symbols}

First, case of diffused pilots for phase off-set compensation is elaborated which is used in 802.11x, 802.16 etc., and the operations performed are listed
below.

Channel Estimation is the first task and that is performed by Least-Square method using the FEP addresses generators for sub-sampling.  Component-wise-product
of Pilot symbols and received signal for these pilot symbols results in channel estimate:
\begin{equation}
  \hat{H} = P \times R_p
\end{equation}
For phase estimation, Maximum Likelihood Estimation (MLE) methodology is utilized. The FEP addresses generators can accommodate that by a dot-product of
reference symbol conjugate and received symbols (providing an approximate phase estimate), and is given by: %\note[RP]{2011-05-09: all this seams out-of-date, from now to the end of this chapter. To be checked and reworked}
\begin{equation}
  A(H) e^{-j\hat{\phi_n}} = \frac{1}{N_p} \sum_{i=0}^{N_p-1} R_{p,i}^{*}R_{n,i}
\end{equation}

$ R_{p,i} $, and $ R_{n,i}$ represent the DFT of reference and received symbol respectively. $A(H)$ is a scalar representing the amplitude of the product, its
value is dependent on H.
\begin{eqnarray*}
                R_{p,i} & = & H_i X_{p,i} + Z_{p,i} \\
                R_{n,i} & = & e^{j \phi} H_i X_{n,i} + Z_{n,i} \\
  R_{p,i}^{*} . R_{n,i} & = & \frac{1}{N_p} \sum_{i=0}^{N_p-1} R_{p,i}^{*}R_{n,i} |H_i|^{2} + Z_{\phi,i} = A(H) e^{-j\hat{\phi_n}} \\
                   A(H) & = & \sum_{i \in pilot-positions} |H_i|^2
\end{eqnarray*}
So to get exact phase estimation, we need to divide the resultant dot-product by $A(H)$, which can be calculated by the Energy calculation block. One option is
to divide before proceeding further, or to do it at the end. We keep this quantity and divide at the end.

%Next step is to divide the dot-product resultant by Energy-calculation resultant.
%\begin{equation}
% e^{-j\hat{\phi_n}} = \frac{R_{p,i}^{*} . R_{n,i}}{A(H)}
%\end{equation}
%The division by scalar is performed by component-wise division module, which is capable of dividing a vector by a scalar quantity.

\noindent The estimated channel and phase offset along with received signl $R_n$ are used in Matched filter receiver technique, and is given by:
\begin{eqnarray*}
  R^{'} & = & A(H) e^{-j\hat{\phi_{n}}} \hat{H^{*}} \odot R_n \\
        & = & A(H) e^{-j (\phi - \hat{\phi_{n}})} |H|^{2} \odot X_i + A(H) e^{-j\hat{\phi_{n}}} \hat{H^{*}} \odot Z_i
\end{eqnarray*}

The operations used are Component-wise product between vectores for $\hat{H^{*}} \odot R_n$, and then resultant is multiplied by $A(H) e^{-j\hat{\phi_{n}}}$
using component-wise product (vector by scalar).

%\begin{equation}
% R^{'} = \frac{e^{-j\hat{\phi_{n}}} e^{-j\angle H}}{|H|^2} \odot R_n
%\end{equation}

\noindent $e^{-j (\phi - \hat{\phi_{n}})}$ is termed as phase-error and it is approximately equal to one. The above equation reveals, to get back the desired
signal of equation (23), we need to divide the above resultant $R^{'}$ by $A(H)$ and by $|H|^{2}$. The operation performed is as follows:
\begin{equation}
  R^{''} = \frac{R^{'}}{A(H)} \div |\hat{H}|^{2}
\end{equation}

%\begin{equation}
% R^{''} = \frac{1}{N} \sum_{i=0}^{N_p-1} \frac{R_{p,i}^{*}}{||H||^2} R_{n,i} [ \hat{H^{*}} \odot R_n] \div |\hat{H}|^{2}
%\end{equation}

%\begin{equation}
% R^{''} = \frac{1}{N||H||^2} \sum_{i=0}^{N_p-1} R_{p,i}^{*} R_{n,i} [ \hat{H^{*}} \div |\hat{H}|^{2} \odot R_n ]
%\end{equation}

%The term $ \frac{1}{N||H||^2} \sum_{i=0}^{N_p-1} R_{p,i}^{*} R_{n,i}$ in equation(18) represents precisely the phase estimate $e^{-j\phi}$, and over all what we have at the end can be written as:
\begin{equation}
  R^{''} = e^{-j\hat{\hat{\phi}}} X_n + Z_n^{out}
\end{equation}
where $e^{-j\hat{\hat{\phi}}}$ represents the phase-error, and should be almost negligible.

%\begin{equation}
% R_p^{'} = \frac{R_p}{\sum_{i=0}^{N-1}R_{p,i}} = \frac{R_p}{||\hat{H}||}
%\end{equation}

\textbf{Summary}
\normalsize
Following is the set of operation in-order, that can be used as pseudo-code for sequence (single thread) in micro-processor $6502$:
\begin{enumerate}
  \item Compute DFT of received signals (both pilot, data symbols) using DFT module.
  \item Compute $\hat{H}$, use component-wise-product macro-block.
  \item Compute $A(H) e^{-j\hat{\phi_n}}$ using Dot-Product function. The result of this operation is stored as Q2.14 (to accomodate a full-scale channel
    response) using saturation and truncation.
  \item Compute $ R^{'} = A(H) e^{-j\hat{\phi_{n}}} \hat{H^{*}} \odot R_n $, first use component-wise-product of two vectors functionality ($\hat{H^{*}} \odot
    R_n$), result is stored in Q1.15 format. Next use this result and $A(H) e^{-j\hat{\phi_n}}$ in component-wise-product by a scalar module, saving the output
    in Q3.29 format.
  \item Compute $A(H)$ using Energy Calculationn block.
  \item Compute $R^{''}$: First a division by a real vector ($A(H)$) module is used on previous result i.e. $ R^{'} $. Then the Component-wise-division between
    vectors module is used with resultant of first division and $|\hat{H}|^{2}$ being the operands.
\end{enumerate}

The parameters transmitted to detector are of $8$ bits, the truncation and/or saturation of final result depends on the modulation scheme used.

\subsubsection{Case: Whole Pilots Symbols}

usage UMTS, LTE like systems ....

\subsubsection{Interpolation schemes for Pilot symbols}

\subsection{Multiple Antenna Receiver}

Here we consider the case of single stream. The received signal can be represented in generic format:
\begin{equation}
  R_{i,m} = e^{j \phi_{i,m}} H_m \odot X_i + Z_{i,m}
\end{equation}
where $i$ represent the transmit antenna index while $m$ is receive antennas index. $X$, $H$, $Z$, and $R$ are the conventional notations for data symbols
transmitted, channel response, noise added, and the received signal.

\noindent Channel estimation, carrier phase offset correction, and data detection are calculated in the same fashion as for single-antenna case and are
elaborated here:

\noindent Component-wise-product of Pilot symbols and received signal for these pilot symbols results in channel estimate for the respective receive antenna,
\begin{equation}
  \hat{H_m} = P^{*} \odot R_{P,m}
\end{equation}

Similarly, phase can be estimated by:
\begin{equation}
  e^{j\hat{\phi}_{i,m}} = \frac{\frac{1}{N_{p}} \sum_{n\in N_P} R_{p,m,n}^{*} R_{i,m,n}} {| {\frac{1}{N_{p}} \sum_{n\in N_P} R_{p,m,n}^{*} R_{i,m,n}} |}
\end{equation}

Following the pattern of previous section (Single-Antenna case), data symbols sent can be retrieved as follows:
\begin{equation}
  R_i^{'} = \frac{1}{M} \sum_{m} e^{-j(\phi_{i,m} - \hat{\phi_{i,m}})} \hat{H_m^{*}} \odot R_{i,m}
\end{equation}
\begin{equation}
  R_i^{'} = \frac{1}{M} \sum_{m} e^{-j(\phi_{i,m} - \hat{\phi_{i,m}})} |H_m|^{2} \odot X_i + H_m^{*} Z_i
\end{equation}
\begin{equation}
  R_i^{'} = (\frac{1}{M} \sum_{m} e^{-j(\phi_{i,m} - \hat{\phi_{i,m}})} |H_m|^{2}) \odot X_i + (\frac{1}{M} \sum_{m} H_m^{*}) \odot Z_i
\end{equation}
The channel estimate, and phase estimate are known, macro-block 'component-wise-product' is in-place as well. To get back the data-symbols transmitted, we need
to get-rid of $|H_m|^{2}$. This can be achieved in two ways, one is to take maximum of all channel-estimates and use that to take care of $|H_m|^{2}$. The other
possibility is to have a component-wise division with respective channel estimate. Both are illustrated here:
\begin{equation}
  R_i^{''} = \frac{1}{|H_{max}|^{2}} R_i^{'} = (\frac{1}{M} \sum_{m} e^{-j(\phi_{i,m} - \hat{\phi_{i,m}})} \frac{|H_m|^{2}}{|H_{max}|^{2}}) \odot X_i +
             (\frac{1}{M} \sum_{m} \frac{H_m^{*}}{|H_{max}|^{2}}) \odot Z_i
\end{equation}
where $H_{max} = max_{n,m} |H_{m,n}|^{2}$, and $\frac{1}{M} \sum_{m}
e^{-j(\phi_{i,m} - \hat{\phi_{i,m}})} \frac{|H_m|^{2}}{|H_{max}|^{2}}$ is
inside unit-circle.
\begin{equation}
  R_i^{''} = (\frac{R_i^{'}}{\sqrt{\frac{1}{M} \sum |H_m|^2}})
\end{equation}

\section{Use in WCDMA systems}

The analysis is based on RAKE receiver architectures. A receiver technique which uses several baseband correlators to individually process several signal
multipath components. The correlator out-puts are combined to achieve improved communications reliability and performance \cite{cdma_online}.

\noindent The wave at the transmitter can be written as:
\begin{equation}
  S_i(n) = \sum_k S_k \psi_k(n-Mk)
\end{equation}

where $i$ is code/user index, $S_k$ represents the symbols (which can be QPSK or 16-QAM). $\psi_k$ is spreading sequence at time-k, ans is usually
QPSK-sequence. $M$ is spreading factor, and its range is $1 -512$.

\noindent Channel response and noise are added to signal when it arrives at receiver, and is given by:
\begin{equation}
  r(n) = \sum S_i(k) h(n-Lk) + Z(n)	
\end{equation}

$L$ is receiver up-sampling factor, and its value is either 4 or 8 in case of RAKE receiver.

\noindent Now to retrieve the data symbols, RAKE receiver operations begin with an initial channel estimate $\hat{h(n)}$, which infact represents fingers. A
finger $f$ in RAKE receiver is termed as pair of amplitude and delay $F =
(\hat{h_i}, n_i) i \in [0, F-1]$ (F is total number of fingers).
\begin{equation}
  \hat{r_{k,i}} = \sum_{f=0}^{F-1} h_f^{*} <\psi_{(k,i)} (0 : M-1), r(Mk + n_i : (M+1)k + n_i) >
\end{equation}

The above equation looks quite similar to that of OFDM case, and the same methodology can be applied here. The up-sampling factor is countered by
Division-by-real macro-block of FEP.

% vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
