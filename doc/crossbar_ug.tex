%
% Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
% Copyright (C) - Telecom ParisTech
% Contacts: contact-embb@telecom-paristech.fr
% 
% Embb is governed by the CeCILL license under French law and abiding by the rules
% of distribution of free software. You can use, modify and/ or redistribute the
% software under the terms of the CeCILL license. You should have received a copy
% of the CeCILL license along with this program; if not, you can access it online
% at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
%

\section{Plug and play support}

The crossbar of the \Embb processor embeds hardware support for plug and play, that is, a way for a software application or an operating system to scan the
DSP part and discover its characteristics:

\begin{itemize}
  \item identification of attached peripherals (initiators and targets),
  \item address mapping of targets,
  \item interrupts routing.
\end{itemize}

These information are stored in a ROM embedded in the crossbar at byte offset 0. At system start-up, the operating system can scan the ROM and automatically
detect which peripherals are present on the platform, how they are configured and where they are located in the address space (targets). The crossbar ROM
contains four tables as shown in figure \ref{fig:rom}. 

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.5}{\input{build/crossbar_rom-fig.pdf_t}}
    \label{fig:rom}
    \caption{Crossbar plug and play ROM}
  \end{center}
\end{figure}

The first table contains only two entries: NPER and NIRQ. NPER (8 bits, $0\le \NPER\le255$) is the number of instantiated peripherals in the DSP part and NIRQ
(16 bits, $0\le NIRQ\le1024$) is the total number of interrupts that can be fired by the DSP part. Table \ref{table:CrossbarROMTable1} lists the fields of the
first table. Byte offsets are from the base address of the DSP part in the global address map\footnote{Reminder: \Embb is big-endian}.

\begin{table}[h]
  \begin{center}
    {\small
    \begin{tabular}{llll}
      \toprule
      \textbf{Name} & \textbf{Bit-width} & \textbf{Byte offset} & \textbf{Description}\\
      \midrule
      \textbf{NPER} & 8                  & 1                    & Number of peripherals \\
      \textbf{NIRQ} & 10                 & 2                    & Total number of interrupts\\
      \bottomrule
    \end{tabular}
    \caption{First table entry}
    \label{table:CrossbarROMTable1}
    }
  \end{center}
\end{table}

The second table contains NPER 64-bits entries and is dedicated to identification of the instantiated peripherals and their address mapping. Each entry is
associated to one peripheral instance. VID, DID, VER and DNIRQ are four 8-bit fields that respectively provide vendor identifier, device identifier, device
version and number of output interrupts of each peripheral.

Table \ref{table:CrossbarROMTable2} lists the fields of the $i^{th}$ entry ($0\le i<\NPER$).

\begin{table}[h]
  \begin{center}
    {\small
    \begin{tabular}{llll}
      \toprule
      \textbf{Name}  & \textbf{Bit-width} & \textbf{Byte offset} & \textbf{Description}\\
      \midrule
      \textbf{VID}   & 8                  & $4+8i$        & Vendor ID \\
      \textbf{VER}   & 8                  & $4+8i+1$      & Device version\\
      \textbf{DID}   & 10                 & $4+8i+2$      & Device ID\\
      \textbf{DNIRQ} & 8                  & $4+8i+4$      & Number of output interrupts\\
      \textbf{ADDR}  & 12                 & $4+8i+5$      & Address\\
      \textbf{MSK}   & 12                 & $4+8i+6$      & Mask\\
      \bottomrule
    \end{tabular}
    \caption{Second table entry}
    \label{table:CrossbarROMTable2}
    }
  \end{center}
\end{table}

Each peripheral type is identified by three fields: the vendor ID, the device ID, and the device version number. The vendor ID is a unique number assigned to a
device vendor. The device ID is a unique number assigned by a vendor to a specific device. The version number is used to distinguish different versions of a
given device. Tables \ref{table:vendors} and \ref{table:devices} list the currently assigned vendor and device IDs, respectively. Vendor ID 0 and device ID 0
are reserved. Important note: some peripherals are available in different configurations (e.g. with or without DMA engine, local micro-controller or processing
core). These configurations can be very different from a programmer's perspective. Without or with DMA engine, for instance, decides whether the peripheral is a
target only or also has its own initiator interface. The different configurations of the same peripheral shall be assigned different device IDs. Table
\ref{table:devices} illustrates a simple way of assigning device IDs: the three least significant bits are used as with/without flags for a DMA engine ($d\in {0,1}$), a local
micro-controller ($u\in {0,1}$) and a processing core ($c\in {0,1}$) respectively.

\begin{table}[h]
  \begin{center}
    {\small
    \begin{tabular}{lll}
      \toprule
      VENDOR            & ID\\
      \midrule
      TELECOM PARISTECH & 1\\
      EURECOM           & 2 \\
      \bottomrule
    \end{tabular}
    \caption{Currently assigned vendor IDs}
    \label{table:vendors}
    }
  \end{center}
\end{table}
 
\begin{table}[h]
  \begin{center}
    {\small
    \begin{tabular}{llllll}
      \toprule
      DEVICE              & with DMA & with UC & with core & ID\\
      \midrule                                 
      CROSSBAR            &       NO &      NO &        NO &  8\\
      VCI2DSP BRIDGE      &       NO &      NO &        NO & 16\\
      FRONT END PROCESSOR &      $d$ &     $u$ &       $c$ & $24+4d+2u +c$\\
      PRE PROCESSOR       &      $d$ &     $u$ &       $c$ & $32+4d+2u +c$\\
      INTERLEAVER         &      $d$ &     $u$ &       $c$ & $40+4d+2u +c$\\
      MAPPER              &      $d$ &     $u$ &       $c$ & $48+4d+2u +c$\\
      RAM                 &      $d$ &      NO &        NO & $56+4d$\\
      CHANNEL DECODER     &      $d$ &     $u$ &       $c$ & $64+4d+2u +c$\\
      AGRF CONTROLLER     &       NO &      NO &        NO & 72\\
      SPI CONTROLLER      &       NO &      NO &        NO & 80\\
      GPIO                &       NO &      NO &        NO & 88\\
      ADAIFEM1            &      $d$ &     $u$ &       $c$ & $96+4d+2u +c$\\
      \bottomrule
    \end{tabular}
    \caption{Currently assigned device IDs}
    \label{table:devices}
    }
  \end{center}
\end{table}

If the peripheral instance has a target interface, its address space is defined by the two 12-bit fields ADDR and MASK. Otherwise these fields are set to 0. The
32-bit address VADDR is mapped to the target peripheral instance satisfying the following equation:

\begin{equation*}
  \left(VADDR[31:20] \band MASK\right) = ADDR
\end{equation*}

The third table is dedicated to interrupts. Each of the NPER peripherals drives interrupt lines. The number of output interrupts of a peripheral instance is
defined by the DNIRQ field of its entry in the second table. Altogether, these NIRQ interrupt lines form the complete set of interrupts flowing from the
DSP part to the control system. The third table contains NIRQ entries and binds each of these NIRQ interrupts to a specific output interrupt of a specific
peripheral instance. Interrupts are numbered from 0 to $\NIRQ-1$. Each entry of the table is 32-bit long and consists in two 16-bit fields IDX and N. The first
entry binds interrupt \#0, the second binds interrupt \#1 and the last binds interrupt \#$\NIRQ-1$. The IDX field identifies the peripheral instance that fires
the interrupt. IDX is the index in the second table of the bound peripheral instance. Most peripherals drive several interrupt lines, locally numbered from 0 to
$\DNIRQ-1$. The N field ($0\le N<\DNIRQ$) identifies the particular output interrupt from the peripheral instance. Table \ref{table:CrossbarROMTable3} lists the
fields of the $i^{th}$ entry ($0\le i<\NIRQ$).

\begin{table}[h]
  \begin{center}
    {\small
    \begin{tabular}{llll}
      \toprule
      \textbf{Name} & \textbf{Bit-width} & \textbf{Byte offset}         & \textbf{Description}\\
      \midrule
      \textbf{IDX}  & 16                 & $4+8\NPER+4i$   & index of peripheral instance in second table\\
      \textbf{N}    & 16                 & $4+8\NPER+4i+2$ & peripheral output interrupt number \\
      \bottomrule
    \end{tabular}
    \caption{Third table entry}
    \label{table:CrossbarROMTable3}
    }
  \end{center}
\end{table}

The fourth table details the wiring of the crossbar. The \Embb crossbar is a partial one. Some links between initiators and targets do not make sense for the
target applications and are missing. Each and every existing link is represented by one 16 bits entry of the table, composed of IIDX, an initiator peripheral
index (8 bits, $0\le IIDX<\NPER$) and TIDX, a target peripheral index (8 bits, $0\le TIDX<\NPER$). IIDX and TIDX are the indices in the second table of the
peripheral instances. Table \ref{table:CrossbarROMTable4} lists the fields of the $i^{th}$ entry.

\begin{table}[h]
  \begin{center}
    {\small
    \begin{tabular}{lllp{4cm}}
      \toprule
      \textbf{Name} & \textbf{Bit-width} & \textbf{Byte offset}                      & \textbf{Description}\\
      \midrule
      \textbf{IIDX} & 8                  & $4+8\NPER+4\NIRQ+2i$   & index of initiator peripheral instance in second table\\
      \textbf{TIDX} & 8                  & $4+8\NPER+4\NIRQ+2i+1$ & index of target peripheral instance in second table \\
      \bottomrule
    \end{tabular}
    \caption{Fourth table entry}
    \label{table:CrossbarROMTable4}
    }
  \end{center}
\end{table}

The end of the fourth table is indicated by a last entry with $IIDX=TIDX=0$. This last entry does not correspond to an existing crossbar link (a link between
the initiator and target interfaces of the same peripheral would not make sense). All in all the length 


\section{Interrupt management}

The crossbar of the DSP part embeds a local hardware interrupt controller. This interrupt controller takes as input the NIRQ intterrupt lines from 
DSP peripherals and outputs either an edge-triggered or level triggered interrupt with line IRQ to the main interrupt controller. The hardware mechanism 
with NIRQ = 4 is shown in figure \ref{fig:irqctrl}.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{1.0}{\input{build/crossbar_irqctrl-fig.pdf_t}}
    \label{fig:irqctrl}
    \caption{Crossbar interrupt controller}
  \end{center}
\end{figure}


The interrupt controller receives the NIRQ interrupts from DSP peripherals. Edge-triggered interrupts are not supported so input interrupts must be
level-triggered. Each interrupt can be independently masked according to its associated bit in register IRQMSK. When a peripheral raises an interrupt line, the
controller first sets the bit corresponding to the line in register R0. A FFS ("Find First Set bit") operation is called on each cycle and returns the index of the
higthest priority interrupt, that is the bit in R0 with the lowest index value. This index value is stored in field IRQI of register STATUS. Interrupts are indexed 
from value 0 to value NIRQ - 1. Another bit, the field IRQV of register STATUS indicates that an interrupt is valid and must be processed. When the controller is
configured to provide edge-triggered interrupts, the IRQ signal will be raised only when all bits from R0 are unset and an input interrupt line is high. When the
controller is configured to provide level-triggered interrupts, the output interrupt line will stay high while R0 is different from 0, that is while a input interrupt
line is set.
The interrupt triggering mode of the crossbar can be configured with field IRQTM of register CONFIG.


\section{Register description}


The parameters configuring the crossbar are passed through one 64 bits configuration register \texttt{config}. The organization of this register is depicted on figure \ref{fig:crossbar_config_layout}.

\bfFigCrossbarConfig

Table \ref{tab:crossbar_config_fields} lists the crossbar parameters, their type, name and short description.

\begin{center}
  \small
\bfTableCrossbarConfig
\end{center}

The register \texttt{status} storing crossbar output interrupts information is depicted on figure  \ref{fig:crossbar_status_layout}.

\bfFigCrossbarStatus

Table \ref{tab:crossbar_status_fields} lists the crossbar interrupts information, their type, name and short description.

\bfTableCrossbarStatus

Four additionnal registers are embedded in crossbar :\texttt{rstn}, \texttt{ce}, \texttt{hirq} and \texttt{irqmask}. For \texttt{rstn}, \texttt{ce} and \texttt{hirq} registers, each bit in register is associated to one peripheral.
Because $0\le \NPER\le255$, these three registers are 256-bit long. The first register \texttt{rstn} is used to reset peripherals. Writing 0 to a bit in this register resets the corresponding peripheral. Writing 1 to a bit in this register unresets the corresponding peripheral.
The second register \texttt{ce} is used to enable or disable peripherals. Writing 1 to a bit in this register enables the corresponding peripheral. Writing 0 to a bit in this register disables the corresponding peripheral.
The third register \texttt{hirq} is used to raise interrupts to peripherals. Writing 1 to a bit in this register raises a edge-triggered interrupt to the corresponding peripheral. The bit will stay high during only one cycle.
Because each platform can be configured differently, that is the number of peripherals and their types can be different between two platforms, the association between a peripheral and an index in these register 
can not be statically established. At platform building, each peripheral is associated with an instance number corresponding to the instance entry index in the ROM table 2 (table 
\ref{table:CrossbarROMTable2}).This index associated to each instance is defined by user and is known by the software application. The bit associated to a peripheral is these three first registers is indexed by
this instance number. The register \texttt{irqmsk} is used to mask interrupts from peripherals. Each bit is associated to one interrupt of the platform. Because, $0\le NIRQ\le1024$, this register is 1024-bit long.
the association between a bit index in this register and the corresponding interruption is done with ROM table 3 (table \ref{table:CrossbarROMTable3}). In this table, each peripheral interruption is bound to a line
of the interrupt controller. The bit associated to a interruption in IRQMSK register is indexed by this line number.


\section{Memory mapping}


The crossbar memory mapping is shown in figure \ref{fig:crossbar_memory}. Adresses are relative to crossbar address. 

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.8}{\input{build/crossbar_memory_map-fig.pdf_t}}
    \label{fig:crossbar_memory}
    \caption{Crossbar memory mapping}
  \end{center}
\end{figure}


% vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
