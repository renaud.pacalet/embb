%
% Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
% Copyright (C) - Telecom ParisTech
% Contacts: contact-embb@telecom-paristech.fr
% 
% Embb is governed by the CeCILL license under French law and abiding by the rules
% of distribution of free software. You can use, modify and/ or redistribute the
% software under the terms of the CeCILL license. You should have received a copy
% of the CeCILL license along with this program; if not, you can access it online
% at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
%

\section{Introduction}

This chapter provides a detailed description of the architecture of the mapper (MAPPER).

\section{The pipeline}

\begin{figure}[htbp]
  \begin{center}
    \rotatebox{90}{\scalebox{0.75}{\input{build/mapper_pss-fig.pdf_t}}}
    \caption{MAPPER pipeline}
    \label{fig:mapper_pss}
  \end{center}
\end{figure}

%% \section{Memory subsystem}
%% 
%% The memory subsystem of the mapper is divided in three regions, DI, LUT and DO.
%% Each region is built from several physical RAMs as depicted by figure
%% \ref{fig:memorymap}. 
%% 
%% \begin{figure}[htbp]
%%   \begin{center}
%%     \scalebox{0.7}{\input{build/memorymap-fig.pdf_t}}
%%     \caption{The two address schemes}
%%     \label{fig:memorymap}
%%   \end{center}
%% \end{figure}
%% 
%% The DMA engine and the VCI interface (VCI) access the local memory buffers
%% through a 64 bits wide interface while the local micro-controller (UC) uses a 8
%% bits wide interface. Processing Sub-System (PSS) uses 3 dedicated interfaces. The arbitration
%% policy for memory accesses is based on fixed priority: PSS > UC > DMA > VCI. PSS
%% reads only in DI and LUT and writes only in DO. DMA, VCI and UC read and write
%% in the three regions.
%% 
%% DI is built from four 1k x 16, single port SRAMs, $DI_0$, $DI_1$, $DI_2$ and
%% $DI_1$.  When accessing DI, PSS reads in one of the four SRAMs. The three other
%% SRAMs are free for a UC access. DMA and VCI read or write in the four SRAMs
%% simultaneously so they can be prevented from accessing DI by PSS and/or UC. As
%% the worst case use of DI by PSS is five accesses every eight clock cycles
%% (QAM1024), it is still possible to parallelize processing and I/O operations in
%% DI.
%% 
%% DO is built from four 1k x 32, single port SRAMs, $DO_0$, $DO_1$, $DO_2$ and
%% $DO_3$. PSS writes in DO every clock cycle, in one of the four SRAMs only. The
%% three other SRAMs are free for a UC access. DMA and VCI read or write in two of
%% the SRAMs simultaneously so they can be prevented from accessing DI by PSS and/or
%% UC.
%% 
%% LUT is built from two 1k x 32, single port SRAMs, $LUT_0$ and $LUT_1$. PSS reads
%% every clock cycle in one of the two. DMA and VCI read or write in the two SRAMs
%% simultaneously so they are prevented from accessing DO during processing by PSS
%% and/or UC access.
%% 
%% DI, DO and LUT form a continuous memory area in the global memory map of the
%% system. In a $n$-bytes word, we number bytes from 0 for the leftmost to $n-1$
%% for the rightmost. We denote $M[a](b)$ the byte $b$ of word at address $a$ in
%% SRAM $M$. Then,
%% $\forall\ (a,i,j), 0\mathsf{x}000\le a<0\mathsf{x}400, 0\le i\le 3, 0\le j\le 1$,
%% the byte offsets from mapper's base address in the system are:
%% 
%% \begin{eqnarray*}
%%   DI_i[a](b) & \Rightarrow & @ = a\times 8 + i\times 2 + b, 0\le b\le 1 \\
%%              &             & (0\mathsf{x}0000 \le @ < 0\mathsf{x}2000) \\
%%   DO_j[a](b) & \Rightarrow & @ = 0\mathsf{x}2000+a\times 8 + j\times 4 + b,
%%     0\le b\le 3 \\
%%              &             & (0\mathsf{x}2000 \le @ < 0\mathsf{x}4000) \\
%%   DO_{2+j}[a](b) & \Rightarrow & @ = 0\mathsf{x}4000+a\times 8 + j\times 4 + b,
%%     0\le b\le 3 \\
%%              &             & (0\mathsf{x}4000 \le @ < 0\mathsf{x}6000) \\
%%   LUT_j[a](b) & \Rightarrow & @ = 0\mathsf{x}6000+a\times 8 + j\times 4 + b,
%%     0\le b\le 3 \\
%%              &             & (0\mathsf{x}6000 \le @ < 0\mathsf{x}8000)
%% \end{eqnarray*}
%% 
%% This address scheme is the one used by DMA, VCI and UC. DMA and VCI accesses are
%% 64 bits (8 bytes) wide. UC accesses are 8 bits wide. PSS uses a different address
%% scheme through 3 independent channels, one for DI, the second for DO and the
%% third for LUT. PSS reads only in DI, 16 bits at a time. It writes only in DO, 32
%% bits at a time and it reads only in LUT, 32 bits at a time. The addresses for
%% these three channels are:
%% 
%% \begin{eqnarray*}
%%   DI_i[a] & \Rightarrow & @_{DI} = a\times 4 + i \\
%%           &             & (0\mathsf{x}000 \le @_{DI} < 0\mathsf{x}1000) \\
%%   DO_j[a] & \Rightarrow & @_{DO} = a\times 2 + j \\
%%           &             & (0\mathsf{x}000 \le @_{DO} < 0\mathsf{x}800) \\
%%   DO_{2+j}[a] & \Rightarrow & @_{DO} = 0\mathsf{x}800+a\times 2 + j \\
%%           &             & (0\mathsf{x}800 \le @_{DO} < 0\mathsf{x}1000) \\
%%   LUT_j[a] & \Rightarrow & @_{LUT} = a\times 2 + j \\
%%           &             & (0\mathsf{x}000 \le @_{LUT} < 0\mathsf{x}800)
%% \end{eqnarray*}
%% 
%% Figure \ref{fig:memorymap} summarizes these two address schemes.
%% 
%% \section{Architecture}
%% 
%% The mapper is designed in such a way that the critical paths are minimized and
%% the throughput is one output point per clock cycle. As a consequence, one
%% 16-bits word is read from the DI input buffer every 16 cycles for BPSK
%% modulations (one bit per symbol, $bpsm1=0$), every 8 cycles for QPSK, \ldots,
%% every two cycles for QAM-256 and five times every 8 cycles for QAM-1024. In the
%% worst case (QAM-1024) 3 cycles over 8 can be used for I/O operations in the DI
%% buffer through the DMA engine, the VCI or the 8 bits UC. The latency of read
%% accesses in DI and LUT is two cycles (because of the input and output registers
%% of the SRAMs). Every input of the PSS is registered as early as possible and
%% every output is the output of a register. All in all, a mapping operation takes
%% one cycle per output point, plus a few extra cycles for initialization [TODO:
%% provide accurate figures].
%% 
%% Figure \ref{fig:architecture} provides and overview of the mapper's pipeline
%% structure.
%% 
%% \begin{figure}[htbp]
%%   \begin{center}
%%     \scalebox{0.65}{\input{build/pss-fig.pdf_t}}
%%     \caption{Architecture of mapper's pipeline}
%%     \label{fig:architecture}
%%   \end{center}
%% \end{figure}

% vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
