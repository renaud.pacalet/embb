%
% Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
% Copyright (C) - Telecom ParisTech
% Contacts: contact-embb@telecom-paristech.fr
% 
% Embb is governed by the CeCILL license under French law and abiding by the rules
% of distribution of free software. You can use, modify and/ or redistribute the
% software under the terms of the CeCILL license. You should have received a copy
% of the CeCILL license along with this program; if not, you can access it online
% at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
%

\section{introduction}

This chapter describes the hardware architecture of DU Shell. Section \ref{sec:dus_ds_ovw} provides a general overview of DU Shell. Sections \ref{sec:dus_ds_mss},
\ref{sec:dus_ds_css} and \ref{sec:dus_ds_pss} present the 3 components of DU Shell:

\begin{itemize}
  \item The Processing Sub-System (PSS), a custom processing unit.
  \item The Memory Sub-System (MSS), a local storage facility.
  \item The Control Sub-System (CSS), responsible for interfacing with the host system and acting as a controller of the whole DSP unit.
\end{itemize}

\section{General overview\label{sec:dus_ds_ovw}}

All DSP units of \Embb share the same generic hardware architecture: the DU Shell. This architecture comprises 3 components: the Processing
Sub-System (PSS), the Memory Sub-System (MSS) and the Control Sub-System (CSS). MSS and PSS are DSP unit-specific but some of their interfaces are standardized and they
must implement common protocols and behaviors that will be detailed in their respective sections. CSS is a generic module, common to all DSP units. Each DSP
unit is an isochronous domain in which all operations are synchronized on the rising edge of a single clock. Different DSP units may have different clocks.
Figure \ref{fig:dus_du_shell} represents the DU Shell architecture.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.75}{\input{build/dus_du_shell-fig.pdf_t}}
    \caption{The DU Shell generic architecture}
    \label{fig:dus_du_shell}
  \end{center}
\end{figure}

CSS is the gateway to the host system. It interfaces with the system interconnect: an Advanced Virtual Component Interface (AVCI) -compliant partial crossbar.
AVCI\cite{vci} is a point-to-point communication standard specified by the Virtual Socket Interface Alliance (VSIA) consortium\cite{vsia}. As most
point-to-point communication standards AVCI is asymmetrical: one end is a slave (target in AVCI language) and the other is a master (initiator). Targets receive
read or write requests from initiators, acknowledge the requests, send responses in return and receive response acknowledges from the initiator. Requests and
responses are decoupled, allowing initiators to send a request before receiving the response to the previous one. Moreover, requests are tagged with a set of
identifiers and the corresponding responses carry copies of these identifiers; they can thus be sent back in a different order than the requests. AVCI is called
a split and out-of-order protocol. The AVCI standard states that initiators must maintain their requests until they are acknowledged by the target.
Symmetrically, targets must maintain their responses until they are acknowledged by the initiator. A request operation completes when the request indicator
(CMDVAL) and the request acknowledge indicator (CMDACK) are both asserted on a rising edge of the system clock. Targets are thus allowed to slow down the
requests stream by de-asserting CMDACK and so inserting wait states whenever they are not ready to process. The same applies for responses with the RSPVAL and
RSPACK indicators. Initiators can also slow down the responses stream by de-asserting RSPACK. Figure \ref{fig:dus_avci_protocol} illustrates the AVCI protocol. Note
that, unlike several other protocols, write requests are followed by responses, just like read requests; these responses do not carry any data but they carry
all the other responses fields, like for instance, the request identifiers.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.75}{\input{build/dus_avci_protocol-fig.pdf_t}}
    \caption{The AVCI protocol}
    \label{fig:dus_avci_protocol}
  \end{center}
\end{figure}

CSS embeds 3 sub-components plus some glue logic and AVCI requests-responses FIFOs:

\begin{itemize}
  \item A control unit containing a set of control and status registers (CTRL)
	\item A Direct Memory Access engine (DMA)
	\item An 8-bits micro-controller and its peripherals (UC)
\end{itemize}

CSS has two AVCI interfaces: one target (T-AVCI) and one initiator (I-AVCI). On T-AVCI it receives read and write requests to the Memory Sub-System or to CTRL
internal registers. While this interface can be used by the main CPU of \Embb to upload data samples in MSS and to download processing results,
this is rather inefficient. T-AVCI should be used mainly for read / write operations in the control and status registers, that is, to drive the DSP unit and to
get information about its current state. Data input-output operations should instead be performed through I-AVCI which is handled by DMA. Once programmed
through T-AVCI, DMA can perform data transfers between MSS and other memory banks in the system, or move data inside MSS, without involving the main CPU.

UC can be used to chain sequences of operations (data transfers with DMA or processing by PSS) without involving the main CPU. Its program and data memory is
contained in MSS.

CTRL contains some control logic and a set of control and status registers used to drive and to monitor operations in the DSP unit:

\begin{itemize}
  \item Program and launch a DMA transfer
  \item Program and launch a processing by PSS
  \item Start / stop UC
  \item Configure the chip enables, resets and interrupt enables of PSS, DMA and UC
  \item Get the current status of PSS or DMA
  \item Read and clear interrupt flags
\end{itemize}

Most of these registers are identical from a DSP unit to another, with one exception: the register containing the processing parameters for PSS operations is
specific to each DSP unit. The bit-width of this register is always a multiple of 64. As its width varies from one DSP unit to another, a generic parameter is
used to specify the bit-width of this register: $width=cmdsize\times64$.

The control and status registers can be accessed either by UC or by the main CPU (through T-AVCI). Some registers are associated a particular behaviour on read
or write. Section \ref{sec:dus_ds_css} details all CSS registers and their role.

MSS is a local storage facility. It contains the program and data memory of UC. This 2 k-Bytes memory is accessed by UC through an 8-bits interface. It can also
be accessed through the 64-bits interfaces of DMA or directly from the T-AVCI. On access conflicts the priority is fixed: UC, DMA, T-AVCI, in decreasing
priority order. Accesses by low priority clients are delayed until higher priority requests are served. A typical UC use case begins with UC in disabled state;
the main CPU programs a DMA transfer (or, less efficiently, performs a series of load-store operations through T-AVCI) to load a binary image (program and data)
in the UC memory; UC is then waken up by a proper write operation in a CSS configuration register. UC jumps at a pre-defined address in its memory space and
starts running its own program. The binary image can be changed by disabling UC, programming a DMA transfer and enabling UC again. Note that PSS cannot access UC
memory.

MSS also contains the memories used by PSS for its own data processing tasks. PSS accesses them through custom interfaces. Some of these memories can be private
and accessible only by PSS. Others are regularly mapped in the system address space and can be accessed either by DMA or through the T-AVCI. UC cannot access
these other memories directly: its memory space is limited to its own program and data memory, plus the control and status registers in CTRL. However, UC can
program a local to local DMA transfer to move some data between one of these memories (assuming it is not an PSS-private one) and its own memory. Access
conflicts between PSS, DMA and T-AVCI are handled with fixed priorities: PSS, DMA, T-AVCI, in decreasing priority order. Accesses by low priority clients are
delayed until higher priority requests are served. The interfaces between MSS and DMA and between MSS and T-AVCI are identical, 64-bits wide, standard
interfaces. MSS is synchronous and its pipeline depth is configurable through two generic parameters: \texttt{n0} is the number of input register stages on
front of the actual RAM blocks and \texttt{n1} is the number of output register stages after the actual RAM blocks. The minimum value for \texttt{n0} and
\texttt{n1} is 1; the minimum pipeline depth of MSS is thus 2. This depth is configurable and can be set to higher values for performance reasons. In DSP units
with complex and large MSS, the floor plan is usually constrained by the arrangement of the RAM blocks. If the RAM blocks are distributed over a large area of
the silicon die, the interconnection delays become dominant and can dramatically increase the critical paths. Adding input and/or output pipeline stages can
significantly improve the maximum reachable frequency because the synthesis / place and route tools can take benefit of them to distribute the logic and routing
interconnection delays among several stages. Most modern CAD tools can efficiently handle this \emph{re-timing} operation. Of course, the PSS design must take
this pipeline depth into account. The CSS also takes this into account, thanks to its two corresponding generic parameters: \texttt{n0} and \texttt{n1}. Figure
\ref{fig:dus_mss} represents a typical MSS and figure \ref{fig:dus_mss_timing} illustrates the timing of MSS read - write operations with $n_0=1$ and $n_1=1$
(minimum pipeline depth).


\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.75}{\input{build/dus_mss-fig.pdf_t}}
    \caption{MSS typical architecture}
    \label{fig:dus_mss}
  \end{center}
\end{figure}

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.75}{\input{build/dus_mss_timing-fig.pdf_t}}
    \caption{MSS timing}
    \label{fig:dus_mss_timing}
  \end{center}
\end{figure}

Because the internal arbitration and multiplexing logics are directly connected to the MSS primary inputs and outputs, it is important, when designing MSS and
PSS, to present read - write requests as early as possible in the clock cycle and to register MSS outputs as soon as possible. Paying attention to these
recommendations helps shortening the combinatorial paths between MSS and PSS and increasing the reachable clock frequency. In case long critical paths involving
the MSS appear, it is advised to first shorten the combinational logic inside PSS on the paths to and from MSS, then to add input and/or output pipeline stages
inside MSS by increasing the \texttt{n0} and/or \texttt{n1} generic parameters of MSS and CSS. Once the desired clock frequency is reached, PSS must then be
reworked and adapted to the new depth. Important note: increasing the pipeline depth of MSS beyond what is strictly required should be avoided as it uselessly
increases the silicon area, the power consumption and also degrades the performance of UC.

Designing MSS is one of the tasks a DSP unit designer has to perform. Whatever the specificities of the DSP unit and its PSS, MSS must contain the UC memory and
must implement the 3 standard interfaces with UC, DMA and T-AVCI. Section \ref{sec:dus_ds_mss} provides more details and examples about MSS.

PSS is the processing heart of each DSP unit. As MSS, it is different from one DSP unit to another. PSS has a standard interface with CSS through which it
receives execution requests and processing parameters. In return, when a processing completes, it sends back to CSS an End-Of-Computation (EOC) signal, an error
flag, a data result and a status information. CSS guarantees that execution requests are never sent to PSS when PSS is busy. The processing parameters are
available at the CSS to PSS interface only during the clock cycle of an execution request. It is the PSS designer's responsibility to store the processing
parameters internally if needed. Error cases, data results and the status information returned by PSS are PSS-specific. PSS also interfaces with MSS through a set
of custom channels. On PSS's point of view, MSS is a set of independent memory areas with different widths, depths and access rights; some can be read-only,
others write-only and others read-write.  Designing PSS is the other, usually most complex, task of a DSP unit designer. Again, whatever the DSP unit
specificities, PSS must implement the standard interface with CSS and the associated protocol illustrated by figure \ref{fig:dus_css_pss_protocol} and it must
take into account the MSS timing characteristics.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.65}{\input{build/dus_css_pss_protocol-fig.pdf_t}}
    \caption{CSS - PSS communication protocol}
    \label{fig:dus_css_pss_protocol}
  \end{center}
\end{figure}

\section{Memory Sub-System (MSS)\label{sec:dus_ds_mss}}

% The Memory Sub-System (MSS), a local memory area mapped in the global system memory map. MSS can be accessed directly from the main controller CPU of
% \Embb through regular load and store operations. It can also be accessed through DMA for local to distant, distant to local or local to local data
% transfers. MSS also contains the memory used by the local 8-bits micro-controller for both its data and instructions.  Finally, PSS accesses MSS directly through
% dedicated custom channels for its operations.

\section{Control Sub-System (CSS)\label{sec:dus_ds_css}}

% The Control Sub-System is a generic device used in every DSP unit of \Embb. It interfaces with the host system (through the crossbar) and with
% the two other sub-components of the DSP unit.

\subsection{The Direct Memory Access engine (DMA)\label{sec:dus_ds_dma}}

\subsection{The 8-bits micro-controller (UC)\label{sec:dus_ds_uc}}

\subsection{The control unit (CTRL)\label{sec:dus_ds_ctrl}}

CTRL contains all control and status registers of CSS. Most of them are regular read-write registers, some others are virtual registers triggering a particular
action when written or read. CTRL has 5 different interfaces which will be detailed first

\subsubsection{CTRL - T-AVCI interface}

CTRL interfaces with T-AVCI through a pair of AVCI requests and response FIFOs. This interface carries only 64-bits wide read-write requests, without the
request identifiers nor the other subtleties of the AVCI protocol (the details of the AVCI protocol are handled by the FIFOs and some extra glue logic). It is
the interface used by the main CPU to access the control and status registers of the DSP unit.

\begin{table}[htpb]
  \begin{center}
    \begin{tabular}{|c|c|c|p{7cm}|}
      \toprule
      Name & Bit-width & Source & Description \\
      \midrule
      \texttt{en} & 1 & T-AVCI & When asserted, signals an active request \\
      \texttt{rnw} & 1 & T-AVCI & Read-not-write; when asserted signals a read request, else a write request \\
      \texttt{be} & 8 & T-AVCI & Individual write byte enables; bytes are written only if the corresponding bit is asserted; the leftmost bit of \texttt{be}
      corresponds to the leftmost byte of \texttt{wdata}, the rightmost bit to the rightmost byte \\
      \texttt{add} & 5 & T-AVCI & 64-bits double-word address of the target register \\
      \texttt{wdata} & 64 & T-AVCI & Write data \\
      \texttt{oor} & 1 & CTRL & Out-of-range access; when asserted signals an invalid read or write request, that is, at an unmapped address, or a read request
      to a write-only register, or a write request to a read-only register \\
      \texttt{rdata} & 64 & CTRL & Read data \\
      \bottomrule
    \end{tabular}
    \caption{Interface between CTRL and T-AVCI}
    \label{tab:dus_ctrl_t_avci_if}
  \end{center}
\end{table}

Read-write accesses to CTRL registers are performed in a single clock cycle: write operations complete on the first rising edge of the clock on which
\texttt{en} is asserted and read data are available on \texttt{rdata} before the first rising edge of the clock on which \texttt{en} is asserted.

Some internal registers are less than 64-bits wide or have some bits unmapped. Reading or writing in such registers is not an error case and \texttt{oor} is not
raised but the returned value of the corresponding unmapped bits in \texttt{rdata} is unspecified when reading and the corresponding bits of \texttt{wdata} are
ignored when writing. Software designers should not make any assumption about these read values nor the effect of the written values. When writing such reserved
bits, it is advised to write zeros because future extensions could make use of them but, when possible, their zero value will lead to the current behavior.

\subsubsection{CTRL - DMA interface}

DMA is driven through a dedicated interface with CTRL. This interface comprises a signal used to request a DMA transfer and the full set of parameters for the
transfer. Upon completion of the transfer DMA returns an End-Of-Transfer signal, an error flag and a status indicator.

\begin{table}[htpb]
  \begin{center}
    \begin{tabular}{|c|c|c|p{7cm}|}
      \toprule
      Name & Bit-width & Source & Description \\
      \midrule
      \texttt{exec} & 1 & CTRL & When asserted on a rising edge of clock, requests a DMA transfer; \texttt{exec} is never asserted when DMA is busy; all
      parameters specifying the DMA transfer are valid on the same rising edge of clock only, they must be registered internally by DMA if needed \\
      \texttt{ls} & 1 & CTRL & When asserted indicates that the source of the transfer is the local MSS \\
      \texttt{ld} & 1 & CTRL & When asserted indicates that the destination of the transfer is the local MSS \\
      \texttt{fs} & 1 & CTRL & When asserted indicates that the source address is constant \\
      \texttt{fd} & 1 & CTRL & When asserted indicates that the destination address is constant \\
      \texttt{cs} & 1 & CTRL & When asserted indicates that the source data is the constant value specified by \texttt{cst} \\
      \texttt{be} & 8 & CTRL & Individual write byte enables; on each 64-bits aligned write access performed by DMA, bytes are written only if the corresponding
      bit is asserted; the leftmost bit of \texttt{be} corresponds to the leftmost byte of the written double-word, the rightmost bit to the rightmost byte \\
      \bottomrule
    \end{tabular}
    \caption{Interface between CTRL and DMA}
    \label{tab:dus_ctrl_dma_if}
  \end{center}
\end{table}

\subsubsection{CTRL - UC interface}

An interface with UC through which UC accesses the CTRL internal registers with regular 8-bits read-write operations.

\subsubsection{CTRL - PSS interface}

An interface with PSS which is very similar to that with DMA: a signal used to request a processing, the full set of processing parameters (which bit-width
depends on the CMDSIZE generic parameter) and, from PSS to CTRL, an End-Of-Computation signal, an error flag, a data result and a status indicator.

\subsubsection{CTRL - host system interface}

An interface with the host system with 3 output and one input interrupt lines.

\section{Processing Core (PSS)\label{sec:dus_ds_pss}}

PSS is the processing unit. It receives its start orders and processing parameters from CSS. It reads and writes data samples in MSS., dedicated to a specific
class of digital signal processing. PSSs differ from one DSP unit to the other.  The interfaces of PSS are voluntarily very simple: the main purpose of the DU
Shell is to decouple communication, storage and processing issues in order to simplify the hardware design and validation tasks. Thanks to its simplified
interfaces, designing PSS is no more complex than necessary and the designer can concentrate on her actual data processing problem rather than dealing with
complex communication protocols and memory organization. One of two interfaces of PSS is its control interface with the Control Sub-System (CSS).

% vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
