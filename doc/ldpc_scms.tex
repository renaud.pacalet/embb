%
% Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
% Copyright (C) - Telecom ParisTech
% Contacts: contact-embb@telecom-paristech.fr
% 
% Embb is governed by the CeCILL license under French law and abiding by the rules
% of distribution of free software. You can use, modify and/ or redistribute the
% software under the terms of the CeCILL license. You should have received a copy
% of the CeCILL license along with this program; if not, you can access it online
% at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
%

The original Self-Corrected Min-Sum (SCMS) decoding algorithm for LDPC codes, as proposed in~\cite{savin2008self}, is depicted on algorithm~\ref{alg:scms-original}, with the following notations:

\begin{itemize}
    \item $\Tg$: Tanner graph of LDPC code
    \item $n \in \{1,2,\ldots,N\}$: Variable Nodes (VN) of $\Tg$
    \item $m \in \{1,2,\ldots,M\}$: Check Nodes (CN) of $\Tg$
    \item $\Tg(n)$: set of neighbor CNs of VN $n$
    \item $\Tg(m)$: set of neighbor VNs of CN $m$
    \item $\bm{x}$: vector
    \item $x_i$: component $i$ of vector $\bm{x}$
    \item $\prio_n$: a priori information (LLR) on bit $n$ from channel observation
    \item $\post_n$: a posteriori information (LLR) on bit $n$
    \item $\bm{\prio}$: vector of a priori information (LLR)
    \item $\bm{\post}$: vector of a posteriori information (LLR)
    \item $\alpha_{m,n}$: VN-to-CN message from VN $n$ to CN $m$
    \item $\beta_{m,n}$: CN-to-VN message from CN $m$ to VN $n$
    \item $\sign(x)\in\{-1,+1\}$: sign of $x$ ($\sign(0)=+1$).
    \item $s.x$: field $x$ of data structure $s$
    \item $\mathcal{G}\setminus g$: set $\mathcal{G}$ without element $g$ ($\{g'\in\mathcal{G},\ g'\neq g\}$)
\end{itemize}

\begin{algorithm}
    \small
    \begin{algorithmic}[1]
        \For{$n = 0$ to $N-1$} \Comment\ For all columns
            \State\ $\prio_n \leftarrow \ln(\Pr(x_n = 0 | channel)/\Pr(x_n = 1 | channel))$ \Comment\ LLRs
            \For{$m \in \Tg(n)$}\ \Comment\ For all active rows in column
                \State\ $\alpha_{m,n}^{(0)} \leftarrow \prio_n$ \Comment\ Initialization
            \EndFor%
        \EndFor%
        \For{$l = 1$ to $l_{max}$} \Comment\ Iterate
            \For{$m = 0$ to $M-1$} \Comment\ For all rows
                \For{$n \in \Tg(m)$} \Comment\ For all active columns in row
                    \State\ $\beta_{m,n}^{(l)} \leftarrow \left(\prod\limits_{n'\in\Tg(m)\setminus n}\sign\left(\alpha_{m,n'}^{(l-1)}\right)\right)\times\min\limits_{n'\in\Tg(m)\setminus n}\left(\lvert\alpha_{m,n'}^{(l-1)}\rvert\right)$
                \EndFor%
            \EndFor%
            \For{$n = 0$ to $N-1$} \Comment\ For all columns
				\State\ $\post_n^{(l)} \leftarrow \prio_n + \sum\limits_{m\in\Tg(n)}\beta_{m,n}^{(l)}$\label{alg:scms-original-posterior-update}
                \For{$m \in \Tg(n)$} \Comment\ For all active rows in column
                    \State\ $\alpha_{m,n}^{(l)} \leftarrow \post_n^{(l)} - \beta_{m,n}^{(l)}$
                    \If{$\sign\left(\alpha_{m,n}^{(l)}\right)\neq\sign\left(\alpha_{m,n}^{(l-1)}\right)\band\alpha_{m,n}^{(l-1)}\neq 0$}
                        \State\ $\alpha_{m,n}^{(l)} \leftarrow 0$ \Comment\ Erasure
                    \EndIf%
                \EndFor%
            \EndFor%
        \EndFor%
    \end{algorithmic}
    \caption{The original SCMS algorithm}\label{alg:scms-original}
\end{algorithm}

Algorithm~\ref{alg:scms-original} can be slightly modified to use only posterior information ($\post_n$), initialized with prior information ($\prio_n$) before the first iteration. Line~\ref{alg:scms-original-posterior-update} becomes:
\begin{align*}
	\post_n^{(l)} \leftarrow \post_n^{(l-1)} + \sum\limits_{m\in\Tg(n)}\beta_{m,n}^{(l)}
\end{align*}
with $\post_n^{(0)}=\prio_n$. The impact on decoding performance is negigible but the savings in terms of storage space is significant.

Algorithm~\ref{alg:scms-original} can also be transformed in an equivalent algorithm where the CN-to-VN ($\beta$) and VN-to-CN ($\alpha$) message computations are swapped and their initializations are changed. Algorithm~\ref{alg:scms-modified} illustrates these two modifications.

\begin{algorithm}
    \small
    \begin{algorithmic}[1]
        \For{$n = 0$ to $N-1$} \Comment\ For all columns
			\State\ $\post_n^{(0)} \leftarrow \ln(\Pr(x_n = 0 | channel)/\Pr(x_n = 1 | channel))$ \Comment\ LLRs
            \For{$m \in \Tg(n)$} \Comment\ For all active rows in column
                \State\ $\alpha_{m,n}^{(0)} \leftarrow 0$ \Comment\ Initialization
                \State\ $\beta_{m,n}^{(0)} \leftarrow 0$ \Comment\ Initialization
            \EndFor%
        \EndFor%
        \For{$l = 1$ to $l_{max}$} \Comment\ Iterate
            \For{$m = 0$ to $M-1$} \Comment\ For all rows
                \For{$n \in \Tg(m)$} \Comment\ For all active columns in row
					\State\ $\alpha_{m,n}^{(l)} \leftarrow \post_n^{(l-1)} - \beta_{m,n}^{(l-1)}$
                    \If{$\sign(\alpha_{m,n}^{(l)}) \neq \sign(\alpha_{m,n}^{(l-1)}) \band \alpha_{m,n}^{(l-1)} \neq 0$}
                        \State\ $\alpha_{m,n}^{(l)} \leftarrow 0$ \Comment\ Erase message
                    \EndIf%
                \EndFor%
                \For{$n \in \Tg(m)$} \Comment\ For all active columns in row
                    \State\ $\beta_{m,n}^{(l)} \leftarrow \left(\prod\limits_{n'\in\Tg(m)\setminus n}\sign\left(\alpha_{m,n'}^{(l-1)}\right)\right)\times\min\limits_{n'\in\Tg(m)\setminus n}\left(\lvert\alpha_{m,n'}^{(l-1)}\rvert\right)$
                \EndFor%
            \EndFor%
            \For{$n = 0$ to $N-1$} \Comment\ For all columns
				\State\ $\post_n^{(l)} \leftarrow \post_n^{(l-1)} + \sum\limits_{m\in\Tg(n)}\beta_{m,n}^{(l)}$
            \EndFor%
        \EndFor%
    \end{algorithmic}
    \caption{The modified SCMS algorithm}\label{alg:scms-modified}
\end{algorithm}

The layered schedule proposed in~\cite{mansour2006turbo} speeds-up the propagation of the posterior information and reduces the average number of iterations. Algorithm~\ref{alg:scms-modified-layered} depicts this proposal applied to the modified SCMS algorithm~\ref{alg:scms-modified}.

\begin{algorithm}
    \small
    \begin{algorithmic}[1]
        \For{$n = 0$ to $N-1$} \Comment\ For all columns
            \State\ $\post_n^{(0)} \leftarrow \ln(\Pr(x_n = 0 | channel)/\Pr(x_n = 1 | channel))$ \Comment\ LLRs
            \For{$m \in \Tg(n)$} \Comment\ For all active rows in column
                \State\ $\alpha_{m,n}^{(0)} \leftarrow 0$ \Comment\ Initialization
                \State\ $\beta_{m,n}^{(0)} \leftarrow 0$ \Comment\ Initialization
            \EndFor%
        \EndFor%
        \For{$l = 1$ to $l_{max}$} \Comment\ Iterate
            \For{$m = 0$ to $M-1$} \Comment\ For all rows
                \For{$n \in \Tg(m)$} \Comment\ For all active columns in row
					\State\ $\alpha_{m,n}^{(l)} \leftarrow \post_n^{(l-1)} - \beta_{m,n}^{(l-1)}$
                    \If{$\sign(\alpha_{m,n}^{(l)}) \neq \sign(\alpha_{m,n}^{(l-1)}) \band \alpha_{m,n}^{(l-1)} \neq 0$}
                        \State\ $\alpha_{m,n}^{(l)} \leftarrow 0$ \Comment\ Erase message
                    \EndIf%
                \EndFor%
                \For{$n \in \Tg(m)$} \Comment\ For all active columns in row
                    \State\ $\beta_{m,n}^{(l)} \leftarrow \left(\prod\limits_{n'\in\Tg(m)\setminus n}\sign\left(\alpha_{m,n'}^{(l-1)}\right)\right)\times\min\limits_{n'\in\Tg(m)\setminus n}\left(\lvert\alpha_{m,n'}^{(l-1)}\rvert\right)$
					\State\ $\post_n^{(l)} \leftarrow \post_n^{(l-1)} + \sum\limits_{m\in\Tg(n)}\beta_{m,n}^{(l)}$
                \EndFor%
            \EndFor%
        \EndFor%
    \end{algorithmic}
    \caption{The modified and layered SCMS algorithm}\label{alg:scms-modified-layered}
\end{algorithm}

With the layered schedule (algorithm~\ref{alg:scms-modified-layered}), the posterior information $\post_n^{(l)}$ must be stored. The $\beta_{m,n}^{(l)}$ (CN-to-VN messages) depend entirely on limited information about the set of the $\alpha_{m,n}^{(l)}$ (argmin, first and second minimum of absolute values, signs, product of signs). The SCMS algorithm also requires that the signs of the $\alpha_{m,n}^{(l)}$ are stored, along with flags indicating that they have been erased. All in all, for each row $m$, the set of $\alpha_{m,n}^{(l)}$ can be represented in the compact form $r_m^{(l)} = \{m_1,m_2,i,\bm{\alpha^s},sp,\bm{\alpha^e}\}$:

\begin{itemize}
    \item $m_1=\min\limits_{n\in\Tg(m)}\lvert\alpha_{m,n}^{(l)}\rvert=$ minimum of absolute values
    \item $i=\argmin\limits_{n\in\Tg(m)}\lvert\alpha_{m,n}^{(l)}\rvert=$ index of $m_1$
    \item $m_2=\min\limits_{n\in\Tg(m)\setminus i}\lvert\alpha_{m,n}^{(l)}\rvert=$ second minimum of absolute values
    \item $\bm{\alpha^s}: \alpha^s_n=\sign(\alpha_{m,n}^{(l)})=$ vector of signs
    \item $sp=\prod\limits_{n\in\Tg(m)}\sign(\alpha_{m,n}^{(l)})=$ product of signs
    \item $\bm{\alpha^e}: \alpha^e_n=true$ if $\alpha_{m,n}^{(l)}$ erased = vector of erasure flags
\end{itemize}

Using this compact form, $\beta_{m,n}^{(l)}$ can be computed from $r_m^{(l)}$ as shown on algorithm~\ref{alg:scms-r2b}.

\begin{algorithm}
    \small
    \begin{algorithmic}[1]
        \If{$n=r_m^{(l)}.i$}
            \State\ $\beta_{m,n}^{(l)} \leftarrow r_m^{(l)}.\alpha^s_n \times r_m^{(l)}.sp \times r_m^{(l)}.m_2$
        \Else%
            \State\ $\beta_{m,n}^{(l)} \leftarrow r_m^{(l)}.\alpha^s_n \times r_m^{(l)}.sp \times r_m^{(l)}.m_1$
        \EndIf%
    \end{algorithmic}
    \caption{Computing $\beta_{m,n}^{(l)}$ from $r_m^{(l)}$ compact data structure}\label{alg:scms-r2b}
\end{algorithm}

The SCMS algorithm with layered schedule can be re-formulated as shown on algorithm~\ref{alg:scms-optimized} where we denote $\beta(r_m^{(l)},n)$ the computation of $\beta_{m,n}^{(l)}$ from $r_m^{(l)}$ and $n$ according algorithm~\ref{alg:scms-r2b}.

\begin{algorithm}
    \small
    \begin{algorithmic}[1]
        \For{$n = 0$ to $N-1$} \Comment\ For all columns
            \State\ $\post_n^{(0)} \leftarrow \ln(\Pr(x_n = 0 | channel)/\Pr(x_n = 1 | channel))$ \Comment\ LLRs
        \EndFor%
        \For{$m = 0$ to $M-1$} \Comment\ For all rows
            \State\ $r_m^{(0)} = \{0,0,0,\bm{+1},+1,\bm{true}\}$\label{alg:scms-optimized:r-init0}
        \EndFor%
        \For{$l = 1$ to $l_{max}$}
            \For{$m = 0$ to $M-1$}
                \State\ $r_m^{(l)} \leftarrow \{+\infty,+\infty,0,\bm{+1},+1,\bm{false}\}$\label{alg:scms-optimized:r-init}
                \For{$n \in \Tg(m)$}\label{alg:scms-optimized:phase-1-begin}
                    \State\ $\alpha \leftarrow \post_n^{(l-1)} - \beta(r_m^{(l-1)},n)$\label{alg:scms-optimized:a-new}
					\If{$\sign(\alpha) \neq r_m^{(l-1)}.\alpha^s_n \band\ (\bnot r_m^{(l-1)}.\alpha^e_n)$}\label{alg:scms-optimized:erase-begin}
                        \State\ $\alpha \leftarrow 0$
                        \State\ $r_m^{(l)}.\alpha^e_n \leftarrow true$
                    \EndIf\label{alg:scms-optimized:erase-end}
                    \State\ $r_m^{(l)}.\alpha^s_n \leftarrow \sign(\alpha)$\label{alg:scms-optimized:r-new-begin}
                    \State\ $r_m^{(l)}.sp \leftarrow r_m^{(l)}.sp\times\sign(\alpha)$
                    \If{$\lvert\alpha\rvert < r_m^{(l)}.m_1$}
                        \State\ $r_m^{(l)}.m_2 \leftarrow r_m^{(l)}.m_1$
                        \State\ $r_m^{(l)}.m_1 \leftarrow \lvert\alpha\rvert$
                        \State\ $r_m^{(l)}.i \leftarrow n$
                    \ElsIf{$\lvert\alpha\rvert < r_m^{(l)}.m_2$}
                        \State\ $r_m^{(l)}.m_2 \leftarrow \lvert\alpha\rvert$
                    \EndIf\label{alg:scms-optimized:r-new-end}
                \EndFor\label{alg:scms-optimized:phase-1-end}
                \For{$n \in \Tg(m)$}\label{alg:scms-optimized:g-begin}\label{alg:scms-optimized:phase-2-begin}
                    \State\ $\post_n^{(l)} \leftarrow \post_n^{(l-1)} + \beta(r_m^{(l)},n)$\label{alg:scms-optimized:g-new}
                \EndFor\label{alg:scms-optimized:g-end}\label{alg:scms-optimized:phase-2-end}
            \EndFor%
        \EndFor%
    \end{algorithmic}
    \caption{The memory-optimized SCMS algorithm with layered schedule}\label{alg:scms-optimized}
\end{algorithm}

% vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
