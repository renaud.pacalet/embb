%
% Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
% Copyright (C) - Telecom ParisTech
% Contacts: contact-embb@telecom-paristech.fr
% 
% Embb is governed by the CeCILL license under French law and abiding by the rules
% of distribution of free software. You can use, modify and/ or redistribute the
% software under the terms of the CeCILL license. You should have received a copy
% of the CeCILL license along with this program; if not, you can access it online
% at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
%

\section*{Revision history}

\begin{table}[htbp]
  \begin{center}
    \begin{tabular}{lll}
      \toprule
      \textbf{Date} & \textbf{Version} & \textbf{Revision} \\
      \midrule
      2011-05-31    & 1.0              & Initial release. \\
      2011-07-07    & 1.1              & Complete user guide. \\
      2011-07-18    & 1.2              & Improved user guide. \\
      2011-08-17    & 1.3              & Updated specifications (sym/asym modes). \\
      2011-09-13    & 1.4              & Reformatting, update of error codes. \\
      2012-04-23    & 1.5              & Rework of memory organization. \\
      \bottomrule
    \end{tabular}
    \caption{Revision history}
    \label{tab:mapper_revisions}
  \end{center}
\end{table}

\section*{Introduction}

This chapter presents the functional view of the Mapper processor (MAPPER). It completely and accurately defines the supported operations. The reader interested
in using MAPPER from a functional perspective will find the bit-accurate description of the computations, the local memory organization, the parameters used to
drive MAPPER operations and the related C-language API. Some partial information on MAPPER internals and performance are also provided but the technical
datasheet is a better place to look at for those interested in these topics.

Section \ref{sec:mapper_notations} introduces the conventions and notations. Section \ref{sec:mapper_overview} is an overview of the supported operations. Section
\ref{sec:mapper_mio} presents the local memory and its organization. MAPPER operations are presented in section \ref{sec:mapper_function}. Section \ref{sec:mapper_ctrl}
summarizes the MAPPER parameters, provides the description of the parameter registers, lists the error cases and the corresponding error codes.

The Application Programming Interface is documented in the separate \emph{libembb reference manual}.

\section{Conventions and notations}\label{sec:mapper_notations}

The following conventions and notations are used:

\begin{itemize}
  \item All data types are built on top of real numbers. Real numbers are approximated by integers (fixed point representation). \texttt{intn} is the type of
    the $n$-bits signed integers, represented in 2's complement, in the $[-2^{n-1},2^{n-1}-1]$ range. \texttt{uintn} are $n$-bits unsigned integers is
    the$[0,2^n-1]$ range. Complex numbers are built on top of real numbers. \texttt{cpx2n} are complex numbers which real and imaginary parts are \texttt{intn}
    (\texttt{cpx32} is thus the type of complex number with \texttt{int16} real and imaginary parts). \texttt{ucpx2n} are complex numbers which real and
    imaginary parts are \texttt{uintn}.
  \item When referring to the parameters that specify which processing is to be performed, \texttt{param} is the name of the parameter and $param$ is its value.
    The type of the parameters is either \texttt{intn}, \texttt{uintn} or \texttt{cpx2n} for some $n$.
  \item Vectors are one dimensional arrays of scalar numbers, characterized by the type and number of components. In a \texttt{intn}, $l$-components, vector
    $X$, all components are \texttt{intn}, $X[0]$ is the first component and $X[l-1]$ is the last.
  \item Vectors names are uppercase ($V$). Scalar names are lowercase ($s$).
  \item In a $n$-bits word $d$, bits are numbered from $d_{n-1}$ for the leftmost to $d_0$ for the rightmost. When $d$ represents an integer, $d_{n-1}$ is the
    Most Significant Bit (MSB) and $d_0$ is the Least Significant Bit (LSB).
  \item For any \texttt{intn} or \texttt{uintn} $d$, any natural number $p$, and any two integers $d_{min}\le d_{max}$,
    \begin{itemize}
      \item $uintn(d)$ is the cast of the $n$-bits word $d$ to \texttt{uintn} type, that is, the \texttt{uintn} value that has the same $n$-bits binary
        representation as $d$.
      \item $intn(d)$ is the cast of the $n$-bits word $d$ to \texttt{intn} type, that is, the \texttt{intn} value that has the same $n$-bits binary
        representation as $d$.
      \item $(d\bmod p)$ is the smallest natural integer such that $d=k\times p+(d\bmod p)$ for some integer $k$.
      \item $\sat(d,d_{min},d_{max})$ is the saturated value of $d$ in range $[d_{min},d_{max}]$:
	      \begin{eqnarray*}
          d<d_{min}&\Rightarrow&\sat(d,d_{min},d_{max})=d_{min}\\
          d_{min}\le d\le d_{max}&\Rightarrow&\sat(d,d_{min},d_{max})=d\\
          d_{max}<d&\Rightarrow&\sat(d,d_{min},d_{max})=d_{max}
        \end{eqnarray*}
      \item $\lsh(d,p)$ is the left-shifted value of $d$ by $p$ positions. The $p$ leftmost bits of $d$ are lost. The $p$ bits on the right are 0.
    \end{itemize}
  \item For any \texttt{intn} $d$ and any natural number $p$,
    \begin{itemize}
      \item $\sat(d,p)=\sat(d,-2^{p-1},2^{p-1}-1)$.
      \item $\rsh(d,p)$ is the sign-extended, right-shifted value of $d$ by $p$ positions. The $p$ rightmost bits of $d$ are lost. The $p$ leftmost bits of
        $\rsh(d,p)$ are all equal to the MSB of $d$ (which preserves the sign in 2's complement representation).
      \item $\resize(d,p)$ is a $p$-bits number. If $p\le n$ the $n-p$ leftmost bits of $d$ are dropped. If $n<p$, $p-n$ bits are concatenated to the left of
        $d$; the $p-n$ new bits are all equal to the MSB of $d$ (which preserves the sign and the value in 2's complement representation).
    \end{itemize}
  \item For any \texttt{uintn} $d$ and any natural number $p$,
    \begin{itemize}
      \item $\sat(d,p)=\sat(d,0,2^p-1)$.
      \item $\rsh(d,p)$ is the right-shifted value of $d$ by $p$ positions. The $p$ rightmost bits of $d$ are lost. The $p$ leftmost bits of $\rsh(d,p)$ are all
        0.
      \item $\resize(d,p)$ is a $p$-bits number. If $p\le n$ the $n-p$ leftmost bits of $d$ are dropped. If $n<p$, $p-n$ zero bits are concatenated to the left
        of $d$.
    \end{itemize}
  \item For any real number $x$, $\lfloor x\rfloor$ is the largest integer not greater than $x$ and $\lceil x\rceil$ is the smallest integer not less than $x$.
  \item For any complex number $a$,
    \begin{itemize}
      \item $j=\sqrt{-1}$
      \item $a= \Re(a)+j\Im(a)$: $\Re(a)$ ($\Im(a)$) is the real (imaginary) part of $a$
    \end{itemize}
  \item For any \texttt{cpx2n} or \texttt{ucpx2n} $a$, any two integers $d_{min}\le d_{max}$ and any natural number $p$,
    \begin{itemize}
      \item $\sat(a,d_{min},d_{max})=\sat(\Re(a),d_{min},d_{max})+j\sat(\Im(a),d_{min},d_{max})$
      \item $\sat(a,p)=\sat(\Re(a),p)+j\sat(\Im(a),p)$
      \item $\rsh(a,p)=\rsh(\Re(a),p)+j\rsh(\Im(a),p)$
      \item $\lsh(a,p)=\lsh(\Re(a),p)+j\lsh(\Im(a),p)$
      \item $\resize(a,p)=\resize(\Re(a),p)+j\resize(\Im(a),p)$
    \end{itemize}
  \end{itemize}

\section{General overview}\label{sec:mapper_overview}

MAPPER transforms a frame of input symbols into a frame of \texttt{cpx32} complex numbers representing points of a 2D constellation diagram. MAPPER scans the
input frame, one symbol at a time, and produces one output \texttt{cpx32} per input symbol. Each input symbol is considered an unsigned integer and is used as
an address in a lookup table containing the \texttt{cpx32} complex numbers. MAPPER reads the input symbols in a dedicated 8k-bytes input memory -- $DI$ -- and
stores the output \texttt{cpx32} in another dedicated 16k-bytes output memory -- $DO$. The lookup tables are stored in a third 4k-bytes memory -- $LUT$.
Optionally, a programmable complex scaling factor can be applied to the constellation. This feature can be used to rotate and/or apply an homothecy.

\section{Local memory organization}\label{sec:mapper_mio}

$DI$ is organized as a 4k $\times 16$ bits memory. The bit-width of the input symbols is programmable from one (BPSK modulation) to 16 bits per symbol
(QAM-65536). They are concatenated together to form an input frame. The first (leftmost) bit of the first symbol can fall at any position in any 16-bits word of
$DI$. Same for the last (rightmost) bit of the last symbol. Figure \ref{fig:mapper_7symbolsx6bits} shows an input frame of 7 symbols, 6 bits each (QAM-64), starting at
bit 14 of half-word \texttt{0x0004} in $DI$ and ending with bit 5 of half-word \texttt{0x0006}.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.30}{\input{build/mapper_7symbolsx6bits-fig.pdf_t}}
    \caption{A 7 symbols input frame, 6 bits per symbol, in DI}
    \label{fig:mapper_7symbolsx6bits}
  \end{center}
\end{figure}

Entries in $LUT$ and output constellation points are \texttt{cpx32} complex numbers. $LUT$ and $DO$ are thus organized as 1k $\times32$ and 4k $\times32$
memories, respectively. The real (imaginary) part is the left (right) half of the 32 bits words. Real and imaginary parts have their most significant bit on the
left. Figure \ref{fig:mapper_cpx16-16} represents a constellation point $p=x+j\times y$ as stored in $LUT$ and $DO$.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.5}{\input{build/mapper_cpx16-16-fig.pdf_t}}
    \caption{A constellation point $p=x+j\times y$ as stored in $LUT$ or $DO$}
    \label{fig:mapper_cpx16-16}
  \end{center}
\end{figure}

A fourth memory is also present in MAPPER's memory sub-system: $UCR$, the 4-kBytes program and data memory of the local micro-controller. From a global system
address map point of view, the byte addresses of the memories is depicted by figure \ref{fig:mapper_address_map}. The addresses are relative to the MAPPER's
base address in the host system.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.65}{\input{build/mapper_address_map-fig.pdf_t}}
    \caption{Address map of memories, system view}
    \label{fig:mapper_address_map}
  \end{center}
\end{figure}

\section{Functional description}\label{sec:mapper_function}

The \texttt{uint4} parameter \texttt{bpsm1} defines the bit-width $w$ of the input symbols: $w=bpsm1+1$. $bpsm1$ ranges from 0 (one bit per symbol, BPSK
modulations) to 15 (16 bits per symbol, QAM-65536).

The \texttt{uint1} parameter \texttt{sym} selects one of the two possible operating modes: asymmetrical ($sym=0$) or symmetrical ($sym=1$). In asymmetrical mode
the whole input symbol is considered as an address in the lookup table, pointing to the corresponding output constellation point. The basic asymmetrical mapping
process is depicted in figure \ref{fig:mapper_asym_mapping_basic} and in algorithm \ref{alg:mapper_asym_mapping_basic}.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.75}{\input{build/mapper_asym_mapping_basic-fig.pdf_t}}
    \caption{The basic asymmetrical mapping process}
    \label{fig:mapper_asym_mapping_basic}
  \end{center}
\end{figure}

\begin{algorithm}
  \caption{The basic asymmetrical mapping process}
  \label{alg:mapper_asym_mapping_basic}
  \begin{algorithmic}[1]
    \For{$i = 0$ to $n-1$}
      \State $s \Leftarrow \text{\texttt{read\_next\_symbol}}(DI)$
      \State $DO[i] \Leftarrow LUT[s]$
    \EndFor
  \end{algorithmic}
\end{algorithm}

As the $LUT$ memory is 1k $\times32$, it contains, at most, 1024 constellation points. In asymmetrical mode the bit-width of the input symbols must thus be less
or equal 10 ($0\le bpsm1\le9\Rightarrow1\le w\le10$) in order to fit the $2^w$-entries lookup table in the $LUT$ memory. A $w$-bits input symbol
$s=s_{w-1}s_{w-2}\ldots s_0$ ($1\le w\le 10$) is considered a natural value $v=\texttt{uintw}(s)$ between zero and $2^w-1$. This natural value is used as an
offset in the look-up table; the pointed value in the table is the output point corresponding to the input symbol. The size of the look-up table is $2^w\le1024$
entries.

{\bf Important note}: an error is raised if $sym=0\ \band\ bpsm1>9$, that is, $sym=0\ \band\ w>10$.

In symmetrical mode ($sym=1$) the bit-width $w$ of the input symbols must be even. Each symbol is split in two halves; the left half is used to address the real
part of the lookup table in $LUT$ while the right half is used to independently address the imaginary part. The two parts are then concatenated to form the
corresponding output point. This mode is depicted in figure \ref{fig:mapper_sym_mapping_basic} and in algorithm \ref{alg:mapper_sym_mapping_basic}.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.75}{\input{build/mapper_sym_mapping_basic-fig.pdf_t}}
    \caption{The basic symmetrical mapping process}
    \label{fig:mapper_sym_mapping_basic}
  \end{center}
\end{figure}

\begin{algorithm}
  \caption{The basic symmetrical mapping process}
  \label{alg:mapper_sym_mapping_basic}
  \begin{algorithmic}[1]
    \For{$i = 0$ to $n-1$}
      \State $s \Leftarrow \text{\texttt{read\_next\_symbol}}(DI)$
      \State $sr \Leftarrow \rsh(s,w/2)$
      \State $si \Leftarrow \resize(s,w/2)$
      \State $DO[i] \Leftarrow \Re(LUT[sr])+j\times\Im(LUT[si])$
    \EndFor
  \end{algorithmic}
\end{algorithm}

{\bf Important note}: an error is raised if $sym=1\ \band\ w\bmod2=1$.

Table \ref{tab:mapper_valid_configurations} summarizes the valid configurations of \texttt{sym} and \texttt{bpsm1}. It also gives the number of lookup table
entries for each valid configuration.

\begin{table}
  \begin{center}
    \begin{tabular}{rrrr}
      \toprule
      $bpsm1$ & $w$ & $sym$ &  Lookup table entries \\
      \midrule
            0 &   1 &     0 &    2 \\
            1 &   2 &     0 &    4 \\
            1 &   2 &     1 &    2 \\
            2 &   3 &     0 &    8 \\
            3 &   4 &     0 &   16 \\
            3 &   4 &     1 &    4 \\
            4 &   5 &     0 &   32 \\
            5 &   6 &     0 &   64 \\
            5 &   6 &     1 &    8 \\
            6 &   7 &     0 &  128 \\
            7 &   8 &     0 &  256 \\
            7 &   8 &     1 &   16 \\
            8 &   9 &     0 &  512 \\
            9 &  10 &     0 & 1024 \\
            9 &  10 &     1 &   32 \\
           11 &  12 &     1 &   64 \\
           13 &  14 &     1 &  128 \\
           15 &  16 &     1 &  256 \\
      \bottomrule
    \end{tabular}
    \caption{Valid \texttt{sym} - \texttt{bpsm1} configurations}
    \label{tab:mapper_valid_configurations}
  \end{center}
\end{table}

The \texttt{uint12} parameter \texttt{lenm1} defines the number $n$ of symbols in the input frame: $n=lenm1+1$. The maximum input frame length is thus 4096
symbols which is also the maximum number of output points that can be stored in the $DO$ memory.

The \texttt{uint16} parameter \texttt{iba} is the index of the first bit of the input frame in $DI$. Bit index 0 corresponds to the first bit (leftmost bit or
bit number 15) of the first half-word (half-word address \texttt{0x0000}). Bit index $2^{16}-1$ corresponds to the last bit (rightmost bit or bit number 0) of
the last half-word (half-word address \texttt{0x0fff}). Bit number $0\le b\le15$ of half-word at address $0\le a<2^{12}$ has a bit index $i=16\times a+15-b$.

{\bf Important note}: during the mapping process bit indices wrap around $DI$ boundaries. After reaching the last bit index, if more bits are needed to complete
the current symbol or to read more symbols, they are read from the beginning of $DI$.

The \texttt{uint10} parameter \texttt{lba} is the base word address of the look up table in $LUT$.

{\bf Important note}: an error is raised if the combination of \texttt{lba} and the largest encountered symbol's value $\max$ leads to an out of range memory
access in $LUT$, that is, if:
\begin{equation*}
  \max+lba>=2^{10}
\end{equation*}

Optionally, and prior being written in $DO$, the output point can be rotated and scaled by multiplying it by a \texttt{cpx32} complex coefficient. When set, the
\texttt{uint1} parameter \texttt{men} enables this feature. The \texttt{cpx32} parameter \texttt{mult} is the multiplier. \texttt{mult} is ignored when $men=0$.
Else, its bit-wise representation is as depicted by figure \ref{fig:mapper_cpx16-16}

When this feature is enabled, the lookup table entries are multiplied by $mult$ in full accuracy. The result is then divided by $2^{15}$ and saturated to the
range of \texttt{cpx32} values:
\begin{equation*}
  DO[i] \Leftarrow \sat(\rsh(mult\times LUT[s], 15), 16)
\end{equation*}

\subsection{Addressing scheme on output}

When writing the output points in $DO$, the mapper applies a programmable addressing scheme defined by 4 parameters: \texttt{oba} (\texttt{uint12}), \texttt{s}
(\texttt{uint1}), \texttt{n} (\texttt{uint7}) and \texttt{m} (\texttt{uint8}).

\texttt{oba} is the base word address of the output frame, that is, the word address of the first output point in $DO$.

\texttt{n} and \texttt{m} form a non-integer increment between two consecutive write addresses in $DO$. The value of the increment is:
\begin{equation*}
  inc = \begin{cases}
    n+1/m & \text{ if $m\neq0$} \\
    n     & \text{ if $m=0$}
  \end{cases}
\end{equation*}
This increment being non necessarily integer, addresses can take non-integer values. Non-integer addresses are rounded to the nearest smaller integer prior
addressing $DO$. With increment values less than 1 ($n=0$) some addresses are written several times (which is probably a useless feature in most situations)
while with values larger than 1 some addresses are skipped. A value $n=m=0$ repeatedly writes at the same address.

\texttt{s} can be used to write the output frame in decreasing order. As when reading in $DI$, the addresses generator wraps around $DO$ boundaries. The
following equations summarize the addressing scheme and define $a(i)$, the address of the $i^{th}$ written point:

\begin{align*}
  inc &= \begin{cases}
    n+1/m & \text{ if $m\neq0$} \\
    n     & \text{ if $m=0$}
  \end{cases} \\
  a(i) &= (oba + (-1)^s\times\lfloor i\times inc\rfloor)\bmod 2^{12}
\end{align*}

Table \ref{tab:mapper_addresses_sequences} gives several examples of sequences of word addresses. The first example illustrates a backward sequence. The 3 next
examples show how non-integer increments can be used to: skip every third index, repeat every index twice and skip every sixth index. The first and last
examples illustrate the wrapping mechanism around the $DO$ boundaries.

\begin{table}
  \begin{center}
    \begin{tabular}{rrrrrr}
      \toprule
                   & Ex. \#1 & Ex. \#2 & Ex. \#3 & Ex. \#4 & Ex. \#5 \\
      \midrule
      \texttt{oba} &       5 &       0 &      0 &      0 &      4073 \\
      \texttt{s}   &       1 &       0 &      0 &      0 &         0 \\
      \texttt{n}   &       3 &       1 &      0 &      1 &        11 \\
      \texttt{m}   &       0 &       2 &      2 &      5 &         3 \\
      \midrule
      $a_0$        &       5 &       0 &      0 &      0 &      4073 \\
      $a_1$        &       2 &       1 &      0 &      1 &      4084 \\
      $a_2$        &    4095 &       3 &      1 &      2 &      4095 \\
      $a_3$        &    4092 &       4 &      1 &      3 &        11 \\
      $a_4$        &    4089 &       6 &      2 &      4 &        22 \\
      $a_5$        &    4086 &       7 &      2 &      6 &        33 \\
      $a_6$        &    4083 &       9 &      3 &      7 &        45 \\
      \bottomrule
    \end{tabular}
    \caption{Examples of addresses sequences}
    \label{tab:mapper_addresses_sequences}
  \end{center}
\end{table}

\section{MAPPER control interfaces}\label{sec:mapper_ctrl}

The parameters defining the operations are passed to MAPPER through two 64 bits parameter registers \texttt{r0} and \texttt{r1}, which map is represented on
figure \ref{fig:mapper_ug_regmap} (addresses are relative to the base address of the DPS unit in the global address map). The organization of these registers is
depicted on figures \ref{fig:mapper_r0_layout} and \ref{fig:mapper_r1_layout}.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.75}{\input{build/mapper_ug_regmap-fig.pdf_t}}
    \caption{Map of MAPPER parameter registers}
    \label{fig:mapper_ug_regmap}
  \end{center}
\end{figure}

\bfFigMapperRZero
\bfFigMapperROne

\pagebreak

Table \ref{tab:mapper_fields} lists the MAPPER parameters, their type, name and short description.

\begin{center}
  \small
  \bfFieldsTableMapper
\end{center}

Some parameter values or combinations of parameter values are invalid. When MAPPER detects such a situation it raises an error, aborts the operation and returns
an error code. The error conditions and the corresponding error codes are summarized in table \ref{tab:mapper_errors}.

\begin{table}[htbp]
  \begin{center}
    \begin{tabular}{*{2}{l}}
      \toprule
      \multirow{2}{*}{Code} & Name \\
      \cmidrule(r){2-2}
                            & Condition \\
      \midrule
      \multirow{4}{*}{1}    & \texttt{MAPPER\_INVALID\_SYM\_BPSM1} \\
      \cmidrule(r){2-2}
                            & $sym=0\ \band\ bpsm1>9$ \\
                            & $\bor$ \\
                            & $sym=1\ \band\ bpsm1\bmod2=0$ \\
      \midrule
      \multirow{2}{*}{2}    & \texttt{MAPPER\_LUT\_OUT\_OF\_RANGE} \\
      \cmidrule(r){2-2}
                            & $\max_{0\le i<=lenm1}(\text{\texttt{symbol}}[i])+lba>=2^{11}$ \\
      \bottomrule
    \end{tabular}
    \caption{MAPPER error conditions and codes}
    \label{tab:mapper_errors}
  \end{center}
\end{table}

% vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
