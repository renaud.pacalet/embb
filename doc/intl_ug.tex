%
% Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
% Copyright (C) - Telecom ParisTech
% Contacts: contact-embb@telecom-paristech.fr
% 
% Embb is governed by the CeCILL license under French law and abiding by the rules
% of distribution of free software. You can use, modify and/ or redistribute the
% software under the terms of the CeCILL license. You should have received a copy
% of the CeCILL license along with this program; if not, you can access it online
% at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
%

\section*{Revision history}

\begin{table}[htbp]
  \begin{center}
    \begin{tabular}{lll}
      \toprule
      \textbf{Date} & \textbf{Version} & \textbf{Revision} \\
      \midrule
      2011-06-21    & 1.0              & Initial release. \\
      2011-06-28    & 1.1              & User guide complete. \\
      2011-09-13    & 1.4              & Reformatting, update of error codes. \\
      2012-01-24    & 1.5              & Minor bug fixes (figure \ref{fig:intl_interleaving_pbi} and corresponding equation). \\
      2012-04-23    & 1.6              & Added micro-controller memory. \\
      \bottomrule
    \end{tabular}
    \caption{Revision history}
    \label{tab:intl_revisions}
  \end{center}
\end{table}

\section*{Introduction}

This chapter presents the functional view of the General Purpose Interleaver (INTL). It completely and accurately defines the supported operations. The reader
interested in using INTL from a functional perspective will find the bit-accurate description of the computations, the local memory organization, the parameters
used to drive INTL operations and the related C-language API.  Some partial information on INTL internals and performance are also provided but the technical
datasheet is a better place to look at for those interested in these topics.

Section \ref{sec:intl_notations} introduces the conventions and notations.  Section \ref{sec:intl_overview} is an overview of the supported operations.  Section
\ref{sec:intl_mio} presents the local memory and its organization. INTL operations are presented in section \ref{sec:intl_function}. Section \ref{sec:intl_ctrl}
summarizes the INTL parameters, provides the description of the parameter registers, lists the error cases and the corresponding error codes.

\section{Conventions and notations}\label{sec:intl_notations}

The following conventions and notations are used:

\begin{itemize}
  \item All data types are built on top of natural integers. \texttt{uintn} is the type of the $n$-bits unsigned integers is the $[0,2^n-1]$ range.
  \item When referring to the parameters that specify which processing is to be performed, \texttt{param} is the name of the parameter and $param$ is its value.
    The type of the parameters is \texttt{uintn} for some $n$.
  \item Vectors are one dimensional arrays of natural numbers, characterized by the type and number of components. In a \texttt{uintn}, $l$-components, vector
    $X$, all components are \texttt{uintn}, $X[0]$ is the first component and $X[l-1]$ is the last.
  \item Vectors names are upper-case ($V$). Scalar names are lower-case ($s$).
  \item In a $n$-bits word $d$, bits are numbered from $d_0$ for the leftmost to $d_{n-1}$ for the rightmost. When $d$ represents a natural integer, $d_0$ is
    the Most Significant Bit (MSB) and $d_{n-1}$ is the Least Significant Bit (LSB).
  \item If $d$ is a natural number and $p$ a natural number different from 0:
    \begin{itemize}
      \item $d/p=\frac{d}{p}$ is the quotient of the integer division of $d$ by $p$. It is a natural number.
      \item $0\le d\bmod p<p$ is the remainder of the integer division of $d$ by $p$. It is a natural number.
      \item $d=p\times(d/p)+(d\bmod p)$.
    \end{itemize}
\end{itemize}

\section{General overview}\label{sec:intl_overview}

The INTL implements interleaving and de-interleaving of sequences of data samples, that is, data samples permutations. There are two main solutions to the
permutation problem. Both of them usually consist in reading the input samples from an input memory and writing them in an output memory; if the sequence of
addresses used to read is different from the one used to write, the output samples are stored in a different order than in the input memory; they are permuted.
\begin{enumerate}
  \item In the first family of solutions, dedicated automaton are used to generate the sequence of read and write addresses. One of the two sequences is
    frequently the natural order while the other is different.
  \item In the second family the sequences of addresses are tabulated and stored in a third memory. Again, one of the two sequences is frequently the natural
    order and is produced by a simple counter while the other is different and tabulated.
\end{enumerate}
Both solutions have their pros and cons. The first one usually leads to smaller silicon areas in hardware or to smaller memory footprints in software. But the
second one is much more flexible and can implement virtually any permutation while the other is constrained by the hard-wired automata generating the sequences
of addresses. 

As INTL is general purpose it pertains to the second family. The permutation function is defined by a permutation table stored in a dedicated memory. This is
not the most efficient way to implement a given structured permutation but it allows to define any kind of permutation, even a completely random one. INTL reads
the input samples in a permuted order, according to the permutation table, and writes them back in the natural order, according a simple counter.  Each entry of
the permutation table is thus an offset in the input sequence of samples. In INTL, there is no difference between interleaving and de-interleaving. Both
operations are seen as a table-based permutation.

INTL reads the input samples in a dedicated input memory -- $X$ -- and stores them back in another dedicated output memory -- $Y$: $Y[i] = X[\pi(i)]$, where
$X[i]$ is the $i^{th}$ input sample, $Y[i]$ is the $i^{th}$ output sample and $\pi$ is the permutation function. In the simplest, basic mode, the input and
output samples are 8 bits samples. The maximum length of an input or output sequence is $2^{16}=65536$ samples. $X$ and $Y$ are thus two 64k-bytes memories. The
permutation function $\pi$ is defined as a table stored in a third dedicated memory -- $P$: $P[i]=\pi(i)$ where where $P[i]$ is the $i^{th}$ entry in the $P$
memory. Each entry of the $P$ memory is 16 bits wide, which is the minimum to address the whole 64k samples input memory $X$. The maximum length of a
permutation is $2^{16}=65536$ entries. $P$ is thus a 64k half-words (128k-bytes) memory. Still in the simplest, basic mode, the length of the output sequence is
equal to the length of the permutation table. The \texttt{uint16} parameter \texttt{lenm1} defines the length minus one of the permutation table ($0 \le
lenm1\le 65535$).

Remark: the term {\em permutation} is misleading because it could be interpreted in its mathematical sense. INTL can be used to implement mathematical
permutations but it can also implement other transformations of an input sequence of data samples. Puncturing, for instance, can be implemented by not using
some input indices in the permutation table. The corresponding input samples will then not be copied in the output memory. The processing is thus not really a
mathematical permutation but a punctured one, that is, a mathematical permutation followed by the removal of some samples from the output sequence. Similarly,
repetitions can be implemented by repeating the same index several times. There again the processing is not a pure permutation but a mathematical permutation
preceded by the duplication of some samples of the input sequence. Of course, puncturing and repetitions can be combined. In the following, when the term {\em
permutation} is used, it must be understood as a processing of INTL, not in the mathematical sense.

The basic mode is depicted in figure \ref{fig:intl_interleaving_basic} and in algorithm \ref{alg:intl_interleaving_basic}.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.75}{\input{build/intl_interleaving_basic-fig.pdf_t}}
    \caption{The basic permutation process}
    \label{fig:intl_interleaving_basic}
  \end{center}
\end{figure}

\begin{algorithm}
  \caption{The basic permutation process}
  \label{alg:intl_interleaving_basic}
  \begin{algorithmic}[1]
    \For{$i = 0$ to $lenm1$}
      \State $Y[i] \Leftarrow X[P[i]]$
    \EndFor
  \end{algorithmic}
\end{algorithm}

Several other INTL parameters are used to alter this basic behaviour and will be described in section \ref{sec:intl_function}. Before entering the detailed
functional description it is important to understand the local memory organization.

\section{Local memory organization}\label{sec:intl_mio}

The local memory is a memory space dedicated to INTL operations. Before being permuted, an input sequence of data samples must first be stored in this local
memory, along with a permutation table. The result of the processing is also stored in this local memory, from which it can be read out. In the following the
local memory is named $\mio$.

$\mio$ is a 256k-bytes memory, split in three disjointed memories $X$, $Y$ and $P$. $X$ and $Y$ are two 64k-bytes memories and store the input, respectively the
output, sequences of data samples. $P$ stores permutation tables and is a 128k-bytes memory.

A fourth memory is also present in INTL's memory sub-system: $UCR$, the 4-kBytes program and data memory of the local micro-controller. Figure
\ref{fig:intl_mio} depicts $\mio$ and indicates the byte addresses of the different memory areas, relative to the base address of $\mio$ in the global system
memory map.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.75}{\input{build/intl_mio-fig.pdf_t}}
    \caption{The $\mio$ organization and address map}
    \label{fig:intl_mio}
  \end{center}
\end{figure}

\section{Functional description}\label{sec:intl_function}

\subsection{Length of permutation table}

The \texttt{uint16} parameter \texttt{lenm1} defines the length minus one of the permutation table ($0 \le \texttt{lenm1}\le 65535$).

\subsection{Samples bit-width}

The data samples can be less than 8 bits. The \texttt{uint3} parameter \texttt{widm1} defines the bit-width minus one of the data samples. The samples are
right-aligned in a byte: the useful bits are the $widm1+1$ rightmost ones.  The $7-widm1$ leftmost bits of the input samples are ignored and the $7-widm1$
leftmost bits of the output samples are forced to zero. Figure \ref{fig:intl_interleaving_sample_size} illustrates this feature.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{1.0}{\input{build/intl_interleaving_sample_size-fig.pdf_t}}
    \caption{Controlling the bit-width of data samples}
    \label{fig:intl_interleaving_sample_size}
  \end{center}
\end{figure}

\subsection{Packed, single-bit, input samples}

When the data samples are single-bit samples ($widm1 = 0$), the \texttt{uint1} parameter \texttt{pbi} can be set to 1 to indicate that each byte of the $X$
input memory holds 8 different samples, one per bit. The first sample in a byte (the one with the lowest sequence index) is the leftmost one. The entries of the
permutation table in $P$ become bit-offsets: the 13 MSBs hold the byte-offset in the $X$ input memory and the 3 LSBs hold the bit-offset of the input sample in
the selected byte:
\begin{equation*}
  \forall\ 0\le i\le lenm1,\ Y[i]_7=X[P[i]/8]_{P[i]\bmod8}, Y[i]_{0\le j\le6}=0
\end{equation*}

{\bf Important note}: an error is raised if $widm1 \neq 0 \band pbi=1$.

When $widm1=0 \band pbi=0$ each byte of $X$ or $Y$ hold a single data sample in its LSB; the 7 MSBs are ignored in $X$ and forced to zero in $Y$. Figure
\ref{fig:intl_interleaving_pbi} illustrates the differences between the packed and basic modes.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.75}{\input{build/intl_interleaving_pbi-fig.pdf_t}}
    \caption{Basic and packed input mode}
    \label{fig:intl_interleaving_pbi}
  \end{center}
\end{figure}

\subsection{Packed, single-bit output samples}

When the data samples are single-bit samples ($widm1 = 0$), the \texttt{uint1} parameter \texttt{pbo} can be set to 1 to indicate that each byte of the $Y$
output memory holds 8 different samples, one per bit. The first sample in a byte (the one with the lowest sequence index) is the leftmost one:
\begin{equation*}
  \forall\ 0\le i\le lenm1,\ Y[i/8]_{i\bmod8}=X[P[i]]_7
\end{equation*}

{\bf Important note}: if the first or last byte of the output sequence is not fully used, the unused bits are unmodified.

\texttt{pbo} and \texttt{pbi} are independent and both can be set:
\begin{equation*}
  \forall\ 0\le i\le lenm1,\ Y[i/8]_{i\bmod8}=X[P[i]/8]_{P[i]\bmod8}
\end{equation*}

{\bf Important note}: an error is raised if $widm1\neq0\band pbo=1$.

\subsection{Forcing output samples}

When set to 1, the \texttt{uint1} parameter \texttt{fe} enables a mode in which data samples of the output sequence can be regular copies of input samples or
can be forced to one or zero. Two programmable values in the permutation table are given a special meaning: \texttt{fz} and \texttt{fo}. When the \texttt{fz}
entry is encountered, it is not used to read an input sample from the input memory. Instead, a zeroed output sample is generated. If \texttt{pbo} is set, this
will force one bit on a output byte to 0. Else, a \texttt{0x00} byte will be stored in the output memory. Similarly, when \texttt{fo} is encountered, a all ones
output sample is generated.  If \texttt{pbo} is set, this will force one bit on a output byte to 1. Else, the output sample generated will have its $widm1+1$
rightmost bits set and its $7-widm1$ leftmost bits unset.

{\bf Important note}: an error is raised if $fe=1 \band fz=fo$.

\subsection{Skipping output samples}

When set to 1, the \texttt{uint1} parameter \texttt{se} enables a mode in which locations of the output memory can be skipped instead of being overwritten by an
input sample. One programmable value in the permutation table is given a special meaning: \texttt{sv}. When the \texttt{sv} offset is encountered, the offset is
not used to read an input sample from the input memory. Instead, the counter of output samples is incremented without storing an output sample in the output
memory. When \texttt{pbo} is set to true, the corresponding skipped bit in the output byte is left unchanged. Else, the corresponding byte in the output memory
is left unchanged. Note: this option can be useful to implement large permutation functions as a succession of several smaller permutations.

{\bf Important note}: an error is raised if $fe=1\ \band se=1 \band\ (sv=fo\ \bor sv=fz)$.

\subsection{Handling of repeated input samples}

The rate matching algorithm used in transmission, or some other transmission steps, sometimes repeat some of the samples to transmit, before or after
interleaving the whole frame of samples. The received frame may thus contains several copies of the same input sample. Because of transmission errors, these
different copies can carry different values. The \texttt{uint1} parameter \texttt{re} is used to indicate that the repeated input samples will be handled
differently from the others. When \texttt{re} is set to 1, only the 15 LSBs of the entries of the permutation table are used as offsets in the input memory.  As
a consequence, the offset range is reduced to the $[0\cdots32767]$ range instead of $[0\cdots65535]$ for the basic mode. The MSB of each entry is a {\em repeat
flag} defining {\em repeat sequences} in the permutation table. A repeat sequence starts with an entry which repeat flag is set and ends with the first
following entry which repeat flag is unset. The offsets in a repeat sequence correspond to the different copies of the same repeated input sample. For each
repeat sequence, whatever its length, one single output sample is written in the output memory. Depending on the \texttt{arm} parameter (see below), this output
sample can be the average of the copies of the repeated input sample or the copy corresponding to the last entry of the repeat sequences. Note: the shortest
repeat sequence contains two entries, one which repeat flag is set and a second which repeat flag is unset. A single entry with unset repeat flag is not
considered a repeat sequence, it points to a regular non-repeated sample.  Figure \ref{fig:intl_interleaving_repeat} illustrates the repeat mode.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{1.0}{\input{build/intl_interleaving_repeat-fig.pdf_t}}
    \caption{The repeat sequences}
    \label{fig:intl_interleaving_repeat}
  \end{center}
\end{figure}

The \texttt{uint1} parameter \texttt{arm} is used to enable or disable the {\em average} repeat mode. When $re=1\band arm=0$ ({\em last} repeat mode) all the
copies of a repeated input sample are skipped but the last one which is written as is in the output memory (the hatched copy in figure
\ref{fig:intl_interleaving_repeat}). The skipped entries are those which repeat flag is set in the repeat sequence and the copied one is the last one, which
repeat flag is unset. When $re=1\band arm=1$, all the copies of a repeated input sample are summed together. At the end of the sequence, the sum is averaged by
integer division by the sequence length and the result is written in the output memory. When $re=1\band arm=1$ the only supported lengths of repeat sequences
range from 2 to 8 samples long.

{\bf Important note}: an error is raised if $re=1\band fe=1$ and a repeat sequence contains one of the two special values \texttt{fz} or \texttt{fo}.

{\bf Important note}: an error is raised if $re=1\band se=1$ and a repeat sequence contains the special value \texttt{sv}.

{\bf Important note}: an error is raised if $re=1$ and the last entry in the permutation table has its repeat flag set, that is, if the last repeat sequence
does not end properly with an entry which repeat flag is unset.

{\bf Important note}: an error is raised if $re=1\band arm=1$ and a repeat sequence is more than 8 samples long. This limitation does not hold for repeat mode
{\em last}.

Note: \texttt{arm} is ignored when $re=0$ is unset.

\subsection{Start indices}

The starting byte address of the input sequence in input memory $X$ is defined by the \texttt{uint16} parameter \texttt{iof}. When $pbi=1$ (packed binary
inputs), the \texttt{uint3} parameter \texttt{biof} is also used as the bit offset of the first sample in $X[iof]$. \texttt{biof} is ignored when $pbi=0$.  If
we denote $U$ the sequence of input samples, then:
\begin{align*}
  a&=\begin{cases}
    iof              & \text{ if $pbi=0$} \\
    8\times iof+biof & \text{ if $pbi=1$}
  \end{cases} \\
  \forall 0\le i\le lenm1,\ U[i]&=\begin{cases}
    X[a+i]                                    & \text{ if $pbi=0$} \\
    X\left[\frac{a+i}{8}\right]_{(a+i)\bmod8} & \text{ if $pbi=1$}
  \end{cases}
\end{align*}

{\bf Important note}: an error is raised on out of range access in the input memory $X$.

Similarly, the starting byte address of the output sequence in output memory $Y$ is defined by the \texttt{uint16} parameter \texttt{oof}. When $pbo=1$ (packed
binary outputs), the \texttt{uint3} parameter \texttt{boof} is also used as the bit offset of the first sample in $Y[oof]$. \texttt{boof} is ignored when
$pbo=0$.  If we denote $V$ the sequence of output samples, then:
\begin{align*}
  a&=\begin{cases}
    oof              & \text{ if $pbo=0$} \\
    8\times oof+boof & \text{ if $pbo=1$}
  \end{cases} \\
  \forall 0\le i\le lenm1,\ V[i]&=\begin{cases}
    Y[a+i]                                    & \text{ if $pbo=0$} \\
    Y\left[\frac{a+i}{8}\right]_{(a+i)\bmod8} & \text{ if $pbo=1$}
  \end{cases}
\end{align*}

{\bf Important note}: an error is raised on out of range access in the output memory $Y$.

The \texttt{uint16} parameter \texttt{pof} is the (half-word) address of the first entry of the permutation table in the $P$ memory.

{\bf Important note}: an error is raised on out of range access in the memory $P$ of permutation tables.

\section{INTL control interfaces}\label{sec:intl_ctrl}

The parameters defining the operations are passed to INTL through three 64 bits parameter registers \texttt{r0}, \texttt{r1} and \texttt{r2}, which map is
represented on figure \ref{fig:intl_ug_regmap} (addresses are relative to the base address of the DPS unit in the global address map). The organization of these
registers is depicted on figures \ref{fig:intl_r0_layout}, \ref{fig:intl_r1_layout} and \ref{fig:intl_r2_layout}.

\begin{figure}[htbp]
  \begin{center}
    \scalebox{0.75}{\input{build/intl_ug_regmap-fig.pdf_t}}
    \caption{Map of INTL parameter registers}
    \label{fig:intl_ug_regmap}
  \end{center}
\end{figure}

\bfFigIntlRZero
\bfFigIntlROne
\bfFigIntlRTwo

\pagebreak

Table \ref{tab:intl_fields} lists the INTL parameters, their type, name and short description.

\begin{center}
  \small
  \bfFieldsTableIntl
\end{center}

Some parameter values or combinations of parameter values are invalid. When INTL detects such a situation it raises an error, aborts the operation and returns
an error code. The portion of the output memory in which the result was to be stored is left in an undefined state. The error conditions and the corresponding
error codes are summarized in table \ref{tab:intl_errors}.

\begin{table}[htbp]
  \begin{center}
    \begin{tabular}{*{2}{l}}
      \toprule
      \multirow{2}{*}{Code} & Name \\
      \cmidrule(r){2-2}
                            & Condition \\
      \midrule
      \multirow{2}{*}{1}    & \texttt{INTL\_NON\_BINARY\_PACKED\_INPUTS} \\
      \cmidrule(r){2-2}
                            & $pbi=1\band widm1\neq0$ \\
      \midrule
      \multirow{2}{*}{2}    & \texttt{INTL\_NON\_BINARY\_PACKED\_OUTPUTS} \\
      \cmidrule(r){2-2}
                            & $pbo=1\band widm1\neq0$ \\
      \midrule
      \multirow{2}{*}{4}    & \texttt{INTL\_FORCE\_ONE\_EQUAL\_FORCE\_ZERO} \\
      \cmidrule(r){2-2}
                            & $fe=1\band fz=fo$ \\
      \midrule
      \multirow{2}{*}{8}    & \texttt{INTL\_FORCE\_EQUAL\_SKIP} \\
      \cmidrule(r){2-2}
                            & $fe=1\band se=1\band (sv=fz\bor sv=fo)$ \\
      \midrule
      \multirow{2}{*}{16}   & \texttt{INTL\_REPEAT\_OVERFLOW} \\
      \cmidrule(r){2-2}
                            & $re=1\band arm=1\band \text{ length of repeat sequence } >8$ \\
      \midrule
      \multirow{2}{*}{32}   & \texttt{INTL\_FORCE\_WHILE\_REPEATING} \\
      \cmidrule(r){2-2}
                            & $fe=1\band re=1\band$ encountered $fz$ or $fo$ in repeat sequence \\
      \midrule
      \multirow{2}{*}{64}   & \texttt{INTL\_SKIP\_WHILE\_REPEATING} \\
      \cmidrule(r){2-2}
                            & $se=1\band re=1\band$ encountered $sv$ in repeat sequence \\
      \midrule
      \multirow{2}{*}{128}  & \texttt{INTL\_END\_OF\_TABLE\_WHILE\_REPEATING} \\
      \cmidrule(r){2-2}
                            & $re=1\band$ encountered end of permutation table in repeat sequence \\
      \midrule
      \multirow{2}{*}{256}  & \texttt{INTL\_X\_OUT\_OF\_RANGE} \\
      \cmidrule(r){2-2}
                            & out of range access in $X$ memory \\
      \midrule
      \multirow{2}{*}{512}  & \texttt{INTL\_P\_OUT\_OF\_RANGE} \\
      \cmidrule(r){2-2}
                            & out of range access in $P$ memory \\
      \midrule
      \multirow{2}{*}{1024} & \texttt{INTL\_Y\_OUT\_OF\_RANGE} \\
      \cmidrule(r){2-2}
                            & out of range access in $Y$ memory \\
      \bottomrule
    \end{tabular}
    \caption{INTL error conditions and codes}
    \label{tab:intl_errors}
  \end{center}
\end{table}

% vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
