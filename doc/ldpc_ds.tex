%
% Embb ( http://embb.telecom-paristech.fr/ ) - This file is part of Embb
% Copyright (C) - Telecom ParisTech
% Contacts: contact-embb@telecom-paristech.fr
% 
% Embb is governed by the CeCILL license under French law and abiding by the rules
% of distribution of free software. You can use, modify and/ or redistribute the
% software under the terms of the CeCILL license. You should have received a copy
% of the CeCILL license along with this program; if not, you can access it online
% at http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
%

\section{Algorithm}

\input{ldpc_scms}

\section{Architecture}

With structured quasi-cyclic parity check matrices a first level of parallelism can be exploited to speed-up the decoding: all rows of a block-row can be processed in parallel because the active variable nodes do not overlap between rows. In algorithm~\ref{alg:scms-optimized} it means that several iterations of the loop over the rows can be processed in parallel. For 3GPP New Radio LDPC codes, for instance, up to 384 rows can be processed simultaneously. In \ldpc\ the processing of a row is allocated to one of the Processing Elements (PEs). The number of PEs is 384, the largest lifting factor of all supported standard codes (3GPP NR). When the lifting factor of the current code is 384 all PEs are used simultaneously to process the 384 rows of a block-row in parallel. When the lifting factor is less than 384, some PEs are not used.

\ldpc\ also embeds a shared memory (\texttt{p\_ram}) to store the posterior Log Likelihood Ratios (LLRs). This memory is initialized with the \textit{a priori} information ($\forall\ 0\le n<N,\ \post_n^{(0)}=\prio_n$) from channel observation before the first decoding iteration.

As can been seen in algorithm~\ref{alg:scms-optimized}, during iteration $l$, the processing of a row is split in two phases:

\begin{itemize}
	\item In the first phase (phase 1, lines~\ref{alg:scms-optimized:phase-1-begin} to~\ref{alg:scms-optimized:phase-1-end}) the previous posterior LLRs ($\post_n{(l-1)}$) are read from \texttt{p\_ram}, the compact representation $r_m^{(l-1)}$ of the previous Variable Node (VN) to Check Node (CN) messages ($\alpha_{m,n}^{(l-1)}$) is read from an internal memory (\texttt{r\_ram}), and they are used to compute (and possibly erase) the new VN to CN messages ($\alpha_{m,n}^{(l)}$). Only their compact representation $r_m^{(l)}$ is stored back in the \texttt{r\_ram} internal memory. The previous posterior LLRs ($\post_n{(l-1)}$) are also stored in another internal temporary memory (\texttt{g\_ram}) because they are needed for the second phase (this reduces the bandwidth with \texttt{p\_ram}, which is critical, at the cost of a small internal memory).
	\item In the second phase (phase 2, lines~\ref{alg:scms-optimized:phase-2-begin} to~\ref{alg:scms-optimized:phase-2-end}) the new compact representation $r_m^{(l)}$ of the VN to CN messages computed during phase 1, and the previous posterior LLRs ($\post_n^{(l-1)}$) stored in \texttt{r\_ram} are used to compute the new posterior messages ($\post_n^{(l)}$) which are stored back in \texttt{p\_ram}.
\end{itemize}
 
These two phases are not independent: the second cannot start before the first one ends. With some codes, however, the first phase of a bloc-row can be processed in parallel with the second phase of the previous block-row, because they do not overlap (they have no VNs in common). It is the case with the 3GPP New Radio LDPC codes. \ldpc\ implements this second level of parallelism: when two consecutive block-rows are orthogonal the first phase of the second block-row starts immediately after the first phase of the first block-row. Else, the first phase of the second block-row is delayed until the end of the second phase of the first block-row.

\subsection{The processing pipeline}

Figure~\ref{fig:ldpc_pipe} represents the processing pipeline of \ldpc.\@

\begin{figure}[htbp]
	\begin{center}
		\scalebox{0.75}{\input{build/ldpc_pipe-fig.pdf_t}}
        \caption{The processing pipeline}\label{fig:ldpc_pipe}
	\end{center}
\end{figure}

\subsection{The Processing Element (PE)}

The $n^{th}$ PE is the computing unit responsible of the processing of the $n^{th}$ rows of all block-rows of the parity check matrix. It implements algorithm~\ref{alg:scms-optimized}. PE runs as many iterations as needed. During each iteration it processes its rows, one after the other. When possible, it runs its two processing phases in parallel, on two consecutive rows.

PE contains two internal memories:

\begin{itemize}
    \item \texttt{r\_ram} in which it stores the $r_m^{(l)}$ compact representations of the $\alpha_{m,n}^{(l)}$ VN-to-CN messages corresponding of all its rows.
    \item \texttt{g\_ram} in which it temporarily stores the $\post_n^{(l)}$ posterior messages for the currently processed row.
\end{itemize}

\subsubsection{Processing phase 1}

At the beginning of phase 1, PE initializes the new $r_m^{(l)}$ with $\{+\infty,+\infty,0,\bm{+1},+1,\bm{false}\}$ in register \texttt{r\_new} (line~\ref{alg:scms-optimized:r-init} of algorithm~\ref{alg:scms-optimized}).

PE constantly reads the same previous $r_m^{(l-1)}$ value from its internal \texttt{r\_ram} memory. During the first iteration it uses value $r_m^{(l-1)} = \{0,0,0,\bm{+1},+1,\bm{true}\}$, instead (line~\ref{alg:scms-optimized:r-init0}).

PE receives the posterior messages $\post_n^{(l-1)}$ from the external posterior memory \texttt{p\_ram}, one per clock cycle. From $r_m^{(l-1)}$ and $\post_n^{(l-1)}$ it reconstructs $\beta_{m,n}^{(l-1)}$ using algorithm~\ref{alg:scms-r2b} and computes the new $\alpha_{m,n}^{(l)} = \post_n^{(l-1)} - \beta_{m,n}^{(l-1)}$ (line~\ref{alg:scms-optimized:a-new}). It also applies the erasure (lines~\ref{alg:scms-optimized:erase-begin} to~\ref{alg:scms-optimized:erase-end}), and updates the new $r_m^{(l)}$ in register \texttt{r\_new} (lines~\ref{alg:scms-optimized:r-new-begin} to~\ref{alg:scms-optimized:r-new-end}). Each $\post_n^{(l-1)}$ received from \texttt{p\_ram} is also stored in the \texttt{g\_ram} local memory.

At the end of phase 1, PE writes back the new $r_m^{(l)}$ from register \texttt{r\_new} to its internal \texttt{r\_ram} memory, overwriting the old $r_m^{(l-1)}$. The previous $\post_n^{(l-1)}$ are available in the \texttt{g\_ram} local memory, ready for use during phase 2.

\subsubsection{Processing phase 2}

During phase 2 (lines~\ref{alg:scms-optimized:g-begin} to~\ref{alg:scms-optimized:g-end}), PE reads back the $\post_n^{(l-1)}$ from its \texttt{g\_ram} local memory, one per clock cycle. Simultaneously, it reconstructs the $\beta_{m,n}^{(l)}$ from the new $r_m^{(l)}$ still in register \texttt{r\_new} using algorithm~\ref{alg:scms-r2b}, computes the new $\post_n^{(l)} = \post_n^{(l-1)} + \beta_{m,n}^{(l)}$ (line~\ref{alg:scms-optimized:g-new}) and sends it to the external \texttt{p\_ram} memory where it is stored, overwriting the old $\post_n^{(l-1)}$.

\subsubsection{PE block diagram}

Figure~\ref{fig:ldpc_pe_arc} represents the internal architecture of PE.\@

\begin{figure}[htbp]
	\begin{center}
		\scalebox{0.75}{\input{build/ldpc_pe_arc-fig.pdf_t}}
        \caption{The Processing Element (PE)}\label{fig:ldpc_pe_arc}
	\end{center}
\end{figure}

\subsubsection{PE scheduling}

Figure~\ref{fig:ldpc_pe_sched} represents the scheduling of PE operations, plus posterior memory read-write addresses. The wait state cycles (phase 0) between two rows is due to the latency of the registered posterior memory.

\begin{figure}
    \centering
    \scriptsize
    \begin{turn}{0}
        \begin{tikztimingtable}
            {clk}                   & C ; CC           ; CC           ; CC           ; CC           ; CC             ; CC             ; CC             ; 2S ; CC                 ; CC                 ; CC                 ; CC                 ; CC             ; CC             ; 2S ; CC                 ; CC                 ; CC                 ; CC           ; CC           ; CC             ; CC             \\
            & 43S \\                                                                                                                                                               
            {stage5.fi}             & L ; 2L           ; 2L           ; 2L           ; 2L           ; 2H             ; 2H             ; 2H             ; 2S ; 2H                 ; 2H                 ; 2H                 ; 2H                 ; 2H             ; 2H             ; 2S ; 2H                 ; 2H                 ; 2H                 ; 2H           ; 2H           ; 2H             ; 2H             \\
            {stage5.fgir}           & L ; 2L           ; 2L           ; 2L           ; 2L           ; 2H             ; 2L             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2L                 ; 2L                 ; 2H             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2L                 ; 2L           ; 2L           ; 2H             ; 2L             \\
            {stage5.phase}          & X ; 2X           ; 2X           ; 2X           ; 2X           ; 6D{1}          ;                ;                ; 2S ; 8D{1}              ;                    ;                    ;                    ; 4D{2}          ;                ; 2S ; 6D{2}              ;                    ;                    ; 4D{0}        ;              ; 4D{1}          ; \\
            {stage5.n}              & X ; 2X           ; 2X           ; 2X           ; 2X           ; 2D{0}          ; 2D{1}          ; 2D{2}          ; 2S ; 2D{$d-4$}          ; 2D{$d-3$}          ; 2D{$d-2$}          ; 2D{$d-1$}          ; 2D{0}          ; 2D{1}          ; 2S ; 2D{$d-3$}          ; 2D{$d-2$}          ; 2D{$d-1$}          ; 2X           ; 2X           ; 2D{0}          ; 2D{1}          \\
            {stage5.gi}             & X ; 2X           ; 2X           ; 2X           ; 2X           ; 2D{$\post_0$}  ; 2D{$\post_1$}  ; 2D{$\post_2$}  ; 2S ; 2D{$\post_{d-3}$}  ; 2D{$\post_{d-2}$}  ; 2D{$\post_{d-2}$}  ; 2D{$\post_{d-1}$}  ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2X           ; 2X           ; 2D{$\post_0$}  ; 2D{$\post_1$}  \\
            {stage5.g\_ram\_we} & L ; 2L           ; 2L           ; 2L           ; 2L           ; 2H             ; 2H             ; 2H             ; 2S ; 2H                 ; 2H                 ; 2H                 ; 2H                 ; 2L             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2L                 ; 2L           ; 2L           ; 2H             ; 2H             \\
            {stage5.g\_ram\_wa} & X ; 2X           ; 2X           ; 2X           ; 2X           ; 2D{0}          ; 2D{1}          ; 2D{2}          ; 2S ; 2D{$d-3$}          ; 2D{$d-2$}          ; 2D{$d-2$}          ; 2D{$d-1$}          ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2X           ; 2X           ; 2D{0}          ; 2D{1}          \\
            {stage5.g\_ram\_ra} & X ; 2X           ; 2X           ; 2X           ; 2X           ; 2X             ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2D{0}              ; 2D{1}          ; 2D{2}          ; 2S ; 2D{$d-2$}          ; 2D{$d-1$}          ; 2X                 ; 2X           ; 2X           ; 2X             ; 2X             \\
            {pe.g\_ram\_di}     & X ; 2X           ; 2X           ; 2X           ; 2X           ; 2D{$\alpha_0$} ; 2D{$\alpha_1$} ; 2D{$\alpha_2$} ; 2S ; 2D{$\alpha_{d-3}$} ; 2D{$\alpha_{d-2}$} ; 2D{$\alpha_{d-2}$} ; 2D{$\alpha_{d-1}$} ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2X           ; 2X           ; 2D{$\alpha_0$} ; 2D{$\alpha_1$} \\
            {pe.g\_ram\_do}     & X ; 2X           ; 2X           ; 2X           ; 2X           ; 2X             ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2X                 ; 2D{$\alpha_0$} ; 2D{$\alpha_1$} ; 2S ; 2D{$\alpha_{d-3}$} ; 2D{$\alpha_{d-2}$} ; 2D{$\alpha_{d-1}$} ; 2X           ; 2X           ; 2X             ; 2X             \\
            {stage5.r\_ram\_we}  & L ; 2L           ; 2L           ; 2L           ; 2L           ; 2L             ; 2L             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2L                 ; 2H                 ; 2L             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2L                 ; 2L           ; 2L           ; 2L             ; 2L             \\
            {stage5.r\_ram\_wa}  & X ; 2X           ; 2X           ; 2X           ; 2X           ; 2X             ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2D{$m$}            ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2X           ; 2X           ; 2X             ; 2X             \\
            {stage5.r\_ram\_ra}  & X ; 2X           ; 2X           ; 2X           ; 8D{$m$}      ;                ;                ;                ; 2S ; 6D{$m$}            ;                    ;                    ; 2X                 ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2X           ; 6D{$m+1$}    ;                ; \\
            {pe.r\_ram\_di}      & X ; 2X           ; 2X           ; 2X           ; 2X           ; 2X             ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2D{$r_m'$}         ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2X           ; 2X           ; 2X             ; 2X             \\
            {pe.r\_ram\_do}      & X ; 2X           ; 2X           ; 2X           ; 2X           ; 6D{$r_m$}      ;                ;                ; 2S ; 8D{$r_m$}          ;                    ;                    ;                    ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2X           ; 2X           ; 4D{$r_{m+1}$}  ; \\
            {stage5.go}             & X ; 2X           ; 2X           ; 2X           ; 2X           ; 2X             ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2X                 ; 2D{$\post_0$}  ; 2D{$\post_1$}  ; 2S ; 2D{$\post_{d-3}$}  ; 2D{$\post_{d-2}$}  ; 2D{$\post_{d-1}$}  ; 2X           ; 2X           ; 2X             ; 2X             \\
            & 43S \\                                                                                                                                                               
            {stage4.fi}             & L ; 2L           ; 2L           ; 2L           ; 2L           ; 2H             ; 2H             ; 2H             ; 2S ; 2H                 ; 2H                 ; 2H                 ; 2H                 ; 2H             ; 2H             ; 2S ; 2H                 ; 2H                 ; 2H                 ; 2H           ; 2H           ; 2H             ; 2H             \\
            {stage4.fgir}           & L ; 2L           ; 2L           ; 2L           ; 2L           ; 2H             ; 2L             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2L                 ; 2L                 ; 2H             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2L                 ; 2L           ; 2L           ; 2H             ; 2L             \\
            {stage4.lgir}           & L ; 2L           ; 2L           ; 2L           ; 2L           ; 2L             ; 2L             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2L                 ; 2H                 ; 2L             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2H                 ; 2L           ; 2L           ; 2L             ; 2L             \\
            {stage4.phase}          & X ; 2X           ; 2X           ; 2X           ; 2X           ; 6D{1}          ;                ;                ; 2S ; 8D{1}              ;                    ;                    ;                    ; 4D{2}          ;                ; 2S ; 6D{2}              ;                    ;                    ; 4D{0}        ;              ; 4D{1}          ; \\
            {stage4.n}              & X ; 2X           ; 2X           ; 2X           ; 2X           ; 2D{0}          ; 2D{1}          ; 2D{2}          ; 2S ; 2D{$d-4$}          ; 2D{$d-3$}          ; 2D{$d-2$}          ; 2D{$d-1$}          ; 2D{0}          ; 2D{1}          ; 2S ; 2D{$d-3$}          ; 2D{$d-2$}          ; 2D{$d-1$}          ; 2X           ; 2X           ; 2D{0}          ; 2D{1}          \\
            & 43S \\                                                                                                                                                               
            {stage3.fi}             & L ; 2L           ; 2L           ; 2L           ; 2H           ; 2H             ; 2H             ; 2H             ; 2S ; 2H                 ; 2H                 ; 2H                 ; 2H                 ; 2H             ; 2H             ; 2S ; 2H                 ; 2H                 ; 2H                 ; 2H           ; 2H           ; 2H             ; 2H             \\
            {stage3.r}              & X ; 2X           ; 2X           ; 2X           ; 8D{$m$}      ;                ;                ;                ; 2S ; 12D{$m$}           ;                    ;                    ;                    ;                ;                ; 2S ; 4D{$m$}            ;                    ; 2X                 ; 2X           ; 6D{$m+1$}    ;                ; \\
            {stage3.fgir}           & L ; 2L           ; 2L           ; 2L           ; 2H           ; 2L             ; 2L             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2L                 ; 2H                 ; 2L             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2L                 ; 2L           ; 2H           ; 2L             ; 2L             \\
            {stage3.lgir}           & L ; 2L           ; 2L           ; 2L           ; 2L           ; 2L             ; 2L             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2H                 ; 2L                 ; 2L             ; 2L             ; 2S ; 2L                 ; 2H                 ; 2L                 ; 2L           ; 2L           ; 2L             ; 2L             \\
            {stage3.phase}          & X ; 2X           ; 2X           ; 2X           ; 8D{1}        ;                ;                ;                ; 2S ; 6D{1}              ;                    ;                    ; 6D{2}              ;                ;                ; 2S ; 4D{2}              ;                    ; 4D{0}              ;              ; 6D{1}        ;                ; \\
            {stage3.n}              & X ; 2X           ; 2X           ; 2X           ; 2D{0}        ; 2D{1}          ; 2D{2}          ; 2D{3}          ; 2S ; 2D{$d-3$}          ; 2D{$d-2$}          ; 2D{$d-1$}          ; 2D{0}              ; 2D{1}          ; 2D{$d-3$}      ; 2S ; 2D{$d-2$}          ; 2D{$d-1$}          ; 2X                 ; 2X           ; 2D{0}        ; 2D{1}          ; 2D{2}          \\
            {stage3.gamma\_ram\_we} & L ; 2L           ; 2L           ; 2L           ; 2L           ; 2L             ; 2L             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2L                 ; 2L                 ; 2H             ; 2H             ; 2S ; 2H                 ; 2H                 ; 2H                 ; 2L           ; 2L           ; 2L             ; 2L             \\
            {stage3.gamma\_ram\_wa} & X ; 2X           ; 2X           ; 2X           ; 2X           ; 2X             ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2X                 ; 2D{$a_0$}      ; 2D{$a_1$}      ; 2S ; 2D{$a_{d-3}$}      ; 2D{$a_{d-2}$}      ; 2D{$a_{d-1}$}      ; 2X           ; 2X           ; 2X             ; 2X             \\
            {stage3.gamma\_ram\_do} & X ; 2X           ; 2X           ; 2X           ; 2X           ; 2D{$\post_0$}  ; 2D{$\post_1$}  ; 2D{$\post_2$}  ; 2S ; 2D{$\post_{d-3}$}  ; 2D{$\post_{d-2}$}  ; 2D{$\post_{d-2}$}  ; 2D{$\post_{d-1}$}  ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2X           ; 2X           ; 2D{$\post_0$}  ; 2D{$\post_1$}  \\
            {stage3.gamma\_ram\_ra} & X ; 2X           ; 2X           ; 2D{$a_{0}$}  ; 2D{$a_{1}$}  ; 2D{$a_{2}$}    ; 2D{$a_{3}$}    ; 2D{$a_{4}$}    ; 2S ; 2D{$a_{d-2}$}      ; 2D{$a_{d-1}$}      ; 2X                 ; 2X                 ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2D{$a_{0}$}  ; 2D{$a_{1}$}  ; 2D{$a_{2}$}    ; 2D{$a_{3}$}    \\
            {stage3.gamma\_ram\_di} & X ; 2X           ; 2X           ; 2X           ; 2X           ; 2X             ; 2X             ; 2X             ; 2S ; 2X                 ; 2X                 ; 2X                 ; 2X                 ; 2D{$\post_0$}  ; 2D{$\post_1$}  ; 2S ; 2D{$\post_{d-3}$}  ; 2D{$\post_{d-2}$}  ; 2D{$\post_{d-1}$}  ; 2X           ; 2X           ; 2X             ; 2X             \\
            & 43S \\                                                                                                                                                               
            {stage2.fi}             & L ; 2L           ; 2L           ; 2H           ; 2H           ; 2H             ; 2H             ; 2H             ; 2S ; 2H                 ; 2H                 ; 2H                 ; 2H                 ; 2H             ; 2H             ; 2S ; 2H                 ; 2H                 ; 2H                 ; 2H           ; 2H           ; 2H             ; 2H             \\
            {stage2.fr}             & L ; 2L           ; 2L           ; 2H           ; 2H           ; 2H             ; 2H             ; 2H             ; 2S ; 2H                 ; 2H                 ; 2H                 ; 2H                 ; 2H             ; 2H             ; 2S ; 2H                 ; 2L                 ; 2L                 ; 2L           ; 2L           ; 2L             ; 2L             \\
            {stage2.fgir}           & L ; 2L           ; 2L           ; 2H           ; 2L           ; 2L             ; 2L             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2H                 ; 2L                 ; 2L             ; 2L             ; 2S ; 2L                 ; 2L                 ; 2L                 ; 2H           ; 2L           ; 2L             ; 2L             \\
            {stage2.lgir}           & L ; 2L           ; 2L           ; 2L           ; 2L           ; 2L             ; 2L             ; 2L             ; 2S ; 2L                 ; 2H                 ; 2L                 ; 2L                 ; 2L             ; 2L             ; 2S ; 2H                 ; 2L                 ; 2L                 ; 2L           ; 2L           ; 2L             ; 2L             \\
            {stage2.phase}          & X ; 2X           ; 2X           ; 10D{1}       ;              ;                ;                ;                ; 2S ; 4D{1}              ;                    ; 8D{2}              ;                    ;                ;                ; 2S ; 2D{2}              ; 4D{0}              ;                    ; 8D{1}        ;              ;                ;                \\
            {stage2.g}              & X ; 2X           ; 2X           ; 10D{0}       ;              ;                ;                ;                ; 2S ; 12D{0}             ;                    ;                    ;                    ;                ;                ; 2S ; 2D{0}              ; 2X                 ; 2X                 ; 8D{1}        ;              ;                ;                \\
            {stage2.d}              & X ; 2X           ; 2X           ; 10D{$d_0$}   ;              ;                ;                ;                ; 2S ; 12D{$d_0$}         ;                    ;                    ;                    ;                ;                ; 2S ; 2D{$d_0$}          ; 2X                 ; 2X                 ; 8D{$d_1$}    ;              ;                ;                \\
            {stage2.bq}             & X ; 2X           ; 2X           ; 2D{$bq_{0}$} ; 2D{$bq_{1}$} ; 2D{$bq_{2}$}   ; 2D{$bq_{3}$}   ; 2D{$bq_{4}$}   ; 2S ; 2D{$bq_{d-2}$}     ; 2D{$bq_{d-1}$}     ; 2X                 ; 2X                 ; 2D{$bq_{0}$}   ; 2D{$bq_{1}$}   ; 2S ; 2D{$bq_{d-3}$}     ; 2D{$bq_{d-2}$}     ; 2D{$bq_{d-1}$}     ; 2D{$bq_{0}$} ; 2D{$bq_{1}$} ; 2D{$bq_{2}$}   ; 2D{$bq_{3}$}   \\
            & 43S \\                                                                                                                                                               
            {stage1.iter}           & X ; 2X           ; 12D{0}       ;              ;              ;                ;                ;                ; 2S ; 12D{0}             ;                    ;                    ;                    ;                ;                ; 2S ; 14D{0}             ;                    ;                    ;              ;              ;                ;                \\
            {stage1.br}             & X ; 2X           ; 12D{0}       ;              ;              ;                ;                ;                ; 2S ; 12D{0}             ;                    ;                    ;                    ;                ;                ; 2S ; 14D{0}             ;                    ;                    ;              ;              ;                ;                \\
            {stage1.g}              & X ; 2X           ; 12D{0}       ;              ;              ;                ;                ;                ; 2S ; 12D{0}             ;                    ;                    ;                    ;                ;                ; 2S ; 14D{0}             ;                    ;                    ;              ;              ;                ;                \\
            {stage1.phase}          & X ; 2X           ; 12D{1}       ;              ;              ;                ;                ;                ; 2S ; 2D{1}              ; 4D{0}              ;                    ; 6D{2}              ;                ;                ; 2S ; 4D{2}              ;                    ; 10D{1}             ;              ;              ;                ;                \\
            {stage1.n}              & X ; 2X           ; 2D{0}        ; 2D{1}        ; 2D{2}        ; 2D{3}          ; 2D{4}          ; 2D{5}          ; 2S ; 2D{$d-1$}          ; 2D{0}              ; 2D{1}              ; 2D{0}              ; 2D{1}          ; 2D{2}          ; 2S ; 2D{$d-2$}          ; 2D{$d-1$}          ; 2D{0}              ; 2D{1}        ; 2D{2}        ; 2D{3}          ; 2D{4}          \\
            {stage1.rom\_adds}      & X ; 2X           ; 2D{0}        ; 2D{1}        ; 2D{2}        ; 2D{3}          ; 2D{4}          ; 2D{5}          ; 2S ; 2D{$d-1$}          ; 2X                 ; 2X                 ; 2D{0}              ; 2D{1}          ; 2D{2}          ; 2S ; 2D{$d-2$}          ; 2D{$d-1$}          ; 2D{0}              ; 2D{1}        ; 2D{2}        ; 2D{3}          ; 2D{4}          \\
            \begin{extracode}
                \begin{pgfonlayer}{background}
                    \begin{scope}[semitransparent,semithick]
                        \vertlines[red]{1,3,...,43}
                    \end{scope}
                \end{pgfonlayer}
            \end{extracode}
        \end{tikztimingtable}
    \end{turn}
    \caption{The scheduling of PE}\label{fig:ldpc_pe_sched}
\end{figure}


% vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0:
